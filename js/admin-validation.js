$(document).unbind().on('click','.set_top',function(){
		var boat_id=this.id;
		var get_status=$(this).attr('data-ref');
		var type = $(this).attr('rel');
		$.ajax({
			url    :base_url+'ajax/set_top_destination',
			data   :"get_id="+boat_id+"&get_status="+get_status+"&type="+type,
			success:function(result){
					if(result=="done")
					{
						alert("Record(s) updated successfully !");
						window.location.reload(true);
						return false;
					}
					if(result=="error")
					{
						alert('Error : please try again some time');
						window.location.reload(true);
						return false;
					}
				}	
		});
});

$(document).on('change','select[name="maincategory_id"]',function(){
	var category_id = $(this).val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: base_url+"ajax/getCategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#category_id').html(result);
			
		}
	});
});

/* set front product */
$(document).on('click','.set_front_product',function(){
	
		var boat_id=this.id;
		var get_status=$(this).attr('data-ref');
	  
		$.ajax({
			url:base_url+'ajax/set_front',
			data: "boat_id="+boat_id+"&get_status="+get_status,
			success:function(result){
					if(result=="done")
					{
						alert("Record(s) updated successfully !");
						window.location.reload(true);
						return false;
					}
					if(result=="error")
					{
						alert('Error : please try again some time');
						window.location.reload(true);
						return false;
					}
					if(result=='limit')
					{
						alert('Error : you have only set the 3 boat on front side !');
						window.location.reload(true);
						return false;
					}
				}	
				
		});
});
$(document).ready(function(){
	
	$('#add-image').click(function(){
		flag=1
		$('.pimg').each(function(e){
		  var pdf_id=$(this).attr('id','product_image_'+e);
		  var id_check=$(this).attr('id');
		 
		   if($('#'+id_check).val()=='')
		   {
			    $('#error_product_image').css('margin-left','230px');	
				$('#error_product_image').show();
				$('#error_product_image').fadeIn(3000);
				document.getElementById('error_product_image').innerHTML="The Image uploaded is required.";
				setTimeout(function(){
				 $('#error_product_image').fadeOut('slow');
				},3000);3
				flag=0;
				return false;
		   }
		   var chkpdf = $('#'+id_check).val().split(".");
		   var extension = chkpdf[1];
		   
		   if(extension!='jpg' && extension!='JPG' && extension!='png' && extension!='PNG' && extension!='jpeg' && extension!='JPEG' && extension!='gif' && extension!='GIF')	
		   {
			 $('#error_product_image1').css('margin-left','230px')
			 $('#error_product_image1').show();
			 $('#error_product_image1').fadeIn(3000);	
			 document.getElementById('error_product_image1').innerHTML="The file type you are attempting to upload is not allowed.";
			 setTimeout(function(){
				$('#product_image').css('border-color','#dddfe0');
				$('#error_product_image1').fadeOut('slow');
			 },3000);
			 flag=0;
			 if($('.class-add').length>1)
			  {$('#imageappend.class-add:last').last().remove();}
			  else
			  {$("#imageappend.class-add").find("input").val("");}
			 return false;
		   }
		})
		if(flag==1)
		{
			$('#imageappend').clone().appendTo('#append');
			$("#imageappend.class-add:last").find("input").val("");
			$('#error_product_image').fadeOut('slow');
		}
	});
	
	
	$('#remove-image').click(function(){
	  if($('.class-add').length>1)
	  {$('#imageappend.class-add:last').last().remove();}
	  else
	  {$("#imageappend.class-add").find("input").val("");}
	});	
	
	
	<!-- validation Send Newsletter -->
	$('#btn_send_newsletter').click(function(){
		var news_title=document.getElementById('news_title');
		var chk_bx=document.getElementsByName('check_email[]');
		
		var chk_len=chk_bx.length;
		var flag=0; 
		for(i=0;i<chk_len;i++)
		{
			if(chk_bx[i].checked)
			{
				flag=1;
				break;
			}
			else
			{
				flag=0;
			}
		}
		
		if(news_title.value=="")
		{
			$('#err_news_title').show();
			$('#err_news_title').fadeIn(3000);	
			document.getElementById('err_news_title').innerHTML="The title field is required.";
			news_title.focus();
			setTimeout(function(){
				$('#news_title').css('border-color','#dddfe0');
				$('#err_news_title').fadeOut('slow');
								
			},3000);
			return false;
		}
		 if(flag==0)
		{
		  $('#err_news_title').show();
			$('#err_news_title').fadeIn(3000);	
			document.getElementById('err_news_title').innerHTML="Please Select Subscribers.";
			news_title.focus();
			setTimeout(function(){
				$('#news_title').css('border-color','#dddfe0');
				$('#err_news_title').fadeOut('slow');
								
			},3000);
			return false;
		}
	});
	
	<!-- validation Add newsletter -->
	$('#btn_newsletter').click(function(){
		var news_description=document.getElementById('news_description');	
		var description1 = CKEDITOR.instances['news_description'].getData().replace(/<[^>]*>/gi, '');
		if(!description1.length)
		{
			$('#err_news_description').show();
			$('#err_news_description').fadeIn(3000);	
			document.getElementById('err_news_description').innerHTML="The Description field is required.";
			setTimeout(function(){
				$('#description1').css('border-color','#dddfe0');
				$('#err_news_description').fadeOut('slow');
								
			},3000);
			return false;
		}		
	});
	
	<!-- validationUpdate newsletter -->
	$('#btn_updatenews').click(function(){
		var news_description=document.getElementById('news_description');	
		var description1 = CKEDITOR.instances['news_description'].getData().replace(/<[^>]*>/gi, '');
		if(!description1.length)
		{
			$('#err_news_description').show();
			$('#err_news_description').fadeIn(3000);	
			document.getElementById('err_news_description').innerHTML="The Description field is required.";
			setTimeout(function(){
				$('#description1').css('border-color','#dddfe0');
				$('#err_news_description').fadeOut('slow');
								
			},3000);
			return false;
		}		
	});
	
	
	
	
	$('#remove-price').click(function(){
	  if($('.class-add-price').length>1)
	  {$('#priceappend.class-add-price:last').last().remove();}
	  else
	  {$("#priceappend.class-add-price").find("input").val("");}
	});	
	
	
	$('#remove-video').click(function(){
	  if($('.class-add-video').length>1)
	  {$('#videoappend.class-add-video:last').last().remove();}
	  else
	  {$("#videoappend.class-add-video").find("input").val("");}
	});	
	
	
	
	
	
});
//delete newsletter
function deletconfirm()
{
	if(confirm('Are you sure to delete this record ?'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function multipledeletconfirm(frmname)
{

	var checked=$("input[name='checkbox_del[]']:checked").length;
	if(checked<1)
    alert('No records selected for deletion.');
	else
	{
 		if(confirm('Are you sure to delete these records ?'))
  		{
  			$('#'+frmname).submit();
  			return true;
  		}
  	    else
  		{
  			return false;
  		}
	}
}

//delete mulitiple manage newsletter
function deletesendnewsletter()
{
	var checked=$("input[name='check_email[]']:checked").length;
	if(checked<1)
    alert('No records selected for deletion.');
	else
	{
 		if(confirm('Are you sure to delete these records ?'))
  		{
  			$('#frm-send-newsletter').submit();
  			return true;
  		}
  	    else
  		{
  			return false;
  		}
	}
}


