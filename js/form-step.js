$(document).on('click','.chk_click',function(){
	  var form_family_member= $(this).attr('rel');
      if($(this).is(':checked'))
	  {
		$('#'+form_family_member).show();
	  }
	  else
	  {
		$('#'+form_family_member).hide();
	  } 	  
});

$(document).on('click','.clsday',function(){
      if($(this).is(':checked'))
	  {
		  $('#'+$(this).val()).show();
	  }
	  else
	  {
		  $('#'+$(this).val()).hide();
	  }
 });


$(function() {
 $('#accommodationsform').on('submit',function(){
	 $('.error').remove();
	 if($('#accom_user_name').val()=='')
	 {
		$('input[id="accom_user_name"]').after('<label class="error" for="review_desc">This field is required.</label>');
		return false; 
	 }
	 else if($('#accom_position').val()=='')
	 {
		$('input[id="accom_position"]').after('<label class="error" for="review_desc">This field is required.</label>');
		return false; 
	 }
	 else if($('#accom_email').val()=='')
	 {
		$('input[id="accom_email"]').after('<label class="error" for="review_desc">This field is required.</label>');
		return false; 
	 }
	 else if($('#accom_mobile').val()=='')
	 {
		$('input[id="accom_mobile"]').after('<label class="error" for="review_desc">This field is required.</label>');
		return false; 
	 }
 });	
 $('#transportationform').on('submit',function(){
				 if($('#transportation_available_time').val()=='')
				 {
					$('input[id="transportation_available_time"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_pickuptime').val()=='')
				 {
					$('input[id="transportation_pickuptime"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_pickup_location').val()=='')
				 {
					$('input[id="transportation_pickup_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_pickup_remarks').val()=='')
				 {
					$('input[id="transportation_pickup_remarks"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_drop_location').val()=='')
				 {
					$('input[id="transportation_drop_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_drop_remark').val()=='')
				 {
					$('input[id="transportation_drop_remark"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 });	
 $('#eventform').on('submit',function(){
				 if($('#event_user_name').val()=='')
				 {
					$('input[id="event_user_name"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#event_position').val()=='')
				 {
					$('input[id="event_position"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#event_email').val()=='')
				 {
					$('input[id="event_email"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#event_mobile').val()=='')
				 {
					$('input[id="event_mobile"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 });	
 $('#couponform').on('submit',function(){
				 if($('#coupon_user_name').val()=='')
				 {
					$('input[id="coupon_user_name"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#coupon_person_postion').val()=='')
				 {
					$('input[id="coupon_person_postion"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#coupon_email').val()=='')
				 {
					$('input[id="coupon_email"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#coupon_mobile').val()=='')
				 {
					$('input[id="coupon_mobile"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			  });	
 $('#recreationform').on('submit',function(){
				/* if($('#form_available_from_date_recreation').val()=='')
				 {
					$('input[id="form_available_from_date_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_availalbe_to_date_recreation').val()=='')
				 {
					$('input[id="form_availalbe_to_date_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else*/ if($('#form_departure_recreation').val()=='')
				 {
					$('input[id="form_departure_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_address_recreation').val()=='')
				 {
					$('input[id="form_departure_address_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_time_recreation').val()=='')
				 {
					$('input[id="form_departure_time_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 });	
 $('#adventureform').on('submit',function(){
					/* if($('#form_available_from_date_adventure').val()=='')
					 {
						$('input[id="form_available_from_date_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false; 
					 }
					 else if($('#form_availalbe_to_date_adventure').val()=='')
					 {
						$('input[id="form_availalbe_to_date_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false; 
					 }
					 else*/ if($('#form_departure_adventure').val()=='')
					 {
						$('input[id="form_departure_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false; 
					 }
					 else if($('#form_departure_address_adventure').val()=='')
					 {
						$('input[id="form_departure_address_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false; 
					 }
					 else if($('#form_departure_time_adventure').val()=='')
					 {
						$('input[id="form_departure_time_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false; 
					 }
				 });
 $('#toursform').on('submit',function(){
				
			 });				  			 			 
 
/*	
 var current_cls=0;
 $('.progressbar li').each(function(e){
		 if($(this).hasClass('current'))
		 {
		   $('fieldset').hide();
		   $('fieldset').eq(e).show();	 
		   current_cls=e;
		   for(i=0;e>i;i++)
		   {
			 $("#progressbar li").eq(i+1).addClass("active");   
		   }
		 }	
	 });
*/
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var parent_id;
$(document).on('click',".next",function(){
	     var currentId=$(this).parent().attr('class');
		 $('.error').remove();
		 if(currentId=='progressbaraccom')
		 {
			 if($(this).parent().attr('id')=='accom1')
			 {
				 if($('#accom_title').val()=='')
				 {
					  $('input[id="accom_title"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#accom_title').focus();
					  return false;
				 }
				 if($('#checkupdate_accoum').val()!='update')
				 {
					  if($('#accom_image1').val()=='')
					  {
						  $('input[id="accom_image1"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  $('#accom_image1').focus();
						  return false;
					  }
				 }
				 else if($('#accom_category_id').val()=='')
				 {
					  $('select[id="accom_category_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_subcategory_id').val()=='')
				 {
					  $('select[id="accom_subcategory_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_location').val()=='')
				 {
					  $('textarea[id="accom_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_description').val()=='')
				 {
					  $('textarea[id="accom_description"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='accom2')
			 {
				 if($('#accom_bed_rooms').val()=='')
				 {
					  $('input[id="accom_bed_rooms"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_beds').val()=='')
				 {
					  $('input[id="accom_beds"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_bathrooms').val()=='')
				 {
					  $('input[id="accom_bathrooms"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_swimming_pool').val()=='')
				 {
					  $('input[id="accom_swimming_pool"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_play_yards').val()=='')
				 {
					  $('input[id="accom_play_yards"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_balcony').val()=='')
				 {
					  $('input[id="accom_balcony"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_garden').val()=='')
				 {
					  $('input[id="accom_garden"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_roof').val()=='')
				 {
					  $('input[id="accom_roof"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='accom3')
			 {	
			   
			   if($('#accom_tv_yes').is(':visible'))
			   {
				 if($('#accom_tv_number').val()=='')
				 {
					  $('input[id="accom_tv_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_playstation_yes').is(':visible'))
			   {
				 if($('#accom_playstation_number').val()=='')
				 {
					  $('input[id="accom_playstation_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_wifi_yes').is(':visible'))
			   {
				 if($('#accom_wifi_number').val()=='')
				 {
					  $('input[id="accom_wifi_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_dvd_yes').is(':visible'))
			   {
				 if($('#accom_dvd_number').val()=='')
				 {
					  $('input[id="accom_dvd_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_gym_yes').is(':visible'))
			   {
				 if($('#accom_gym_number').val()=='')
				 {
					  $('input[id="accom_gym_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_kitchen_yes').is(':visible'))
			   {
				 if($('#accom_kitchen_number').val()=='')
				 {
					  $('input[id="accom_kitchen_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_washing_machine_yes').is(':visible'))
			   {
				  if($('#accom_washing_machine_number').val()=='')
				  {
					$('input[id="accom_washing_machine_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				  }
			   }
			 }
			 if($(this).parent().attr('id')=='accom4')
			 {
				 
				 if($('#accom_check_in').val()=='')
				 {
					$('input[id="accom_check_in"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
				 else if($('#accom_check_out').val()=='')
				 {
					$('input[id="accom_check_out"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#accom_guest_max').val()=='')
				 {
					$('input[id="accom_guest_max"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 else if($(this).parent().attr('id')=='accom8')
			 {
				 if($('#accom_location_address_accom').val()=='')
				 {
					$('input[id="accom_location_address_accom"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
			 }
			 
			 /*if($(this).parent().attr('id')=='accom5')
			 {
				 
				 if($('#accom_gustes_allowed').val()=='')
				 {
					$('textarea[id="accom_gustes_allowed"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
			} */
			 if($(this).parent().attr('id')=='accom5')
			 {
				 if($('#accom_price').val()=='')
				 {
					$('input[id="accom_price"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
				 if($('#accom_discount_on').is(':visible'))
				 {
					 if($('#accom_precentage_discount').val()=='')
					 {
						$('input[id="accom_precentage_discount"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
			/*	 if($('#accom_bird_discount').val()=='')
				 {
					$('input[id="accom_bird_discount"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }*/
				 if($('#accom_cancel_policy_yes').is(':visible'))
				 {
					if($('#accom_cancel_policy_price').val()=='')
					{
					  $('input[id="accom_cancel_policy_price"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false; 	
					}
				 }
			}
		 }
		 if(currentId=='progressbartours')
		 {
			 if($(this).parent().attr('id')=='tours1')
			 {
				 if($('#form_title_tours').val()=='')
				 {
					  $('input[id="form_title_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#form_title_tours').focus();
					  return false;
				 }
				 if($('#checkupdate_tours').val()!='update')
				 {
					 if($('#form_image_tours').val()=='')
					 {
						  $('input[id="form_image_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  return false;
					 }
				 }
				 else if($('#categoryID_tours').val()=='')
				 {
					  $('select[id="categoryID_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#subcategoryID_tours').val()=='')
				 {
					  $('select[id="subcategoryID_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_duration_tours').val()=='')
				 {
					  $('input[id="form_duration_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_desc_tours').val()=='')
				 {
					  $('textarea[id="form_desc_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_why_you_tours').val()=='')
				 {
					  $('textarea[id="form_why_you_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_address_tours').val()=='')
				 {
					  $('textarea[id="form_address_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			}
			 if($(this).parent().attr('id')=='tours2')
			 {
                 if($('#form_name_tours').val()=='')
				 {
					$('input[id="form_name_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_position_tours').val()=='')
				 {
					$('input[id="form_position_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_email_tours').val()=='')
				 {
					$('input[id="form_email_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_mobile_tours').val()=='')
				 {
					$('input[id="form_mobile_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			}
			 if($(this).parent().attr('id')=='tours3')
			 {
				 if($('#form_gender_tours').val()=='')
				 {
					$('select[id="form_gender_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_age_tours').val()=='')
				 {
					$('input[id="form_age_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_weight_tours').val()=='')
				 {
					$('input[id="form_weight_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_what_allowed_tours').val()=='')
				 {
					$('textarea[id="form_what_allowed_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_not_allowed_tours').val()=='')
				 {
					$('input[id="form_not_allowed_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_additional_info_tours').val()=='')
				 {
					$('input[id="form_additional_info_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 
			 }
			 if($(this).parent().attr('id')=='tours7')
			 {
				 if($('#form_location_tours').val()=='')
				 {
					$('input[id="form_location_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 if($(this).parent().attr('id')=='tours4')
			 {
				 if($('#form_price_tours').val()=='')
				 {
					$('input[id="form_price_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_price_tours').val()=='')
				 {
					$('input[id="form_child_price_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_age_tours').val()=='')
				 {
					$('input[id="form_child_age_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_grp_price_tours').val()=='')
				 {
					$('input[id="form_grp_price_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 if($('#form_refundable_tours_yes').is(':visible'))
				 {
					 if($('#form_cancel_before_tours').val()=='')
					 {
						$('input[id="form_cancel_before_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
					 else if($('#form_not_cancel_tours').val()=='')
					 {
						$('input[id="form_not_cancel_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
			 }
			 if($(this).parent().attr('id')=='tours5')
			 {
				 if($('#form_departure_tours').val()=='')
				 {
					$('textarea[id="form_departure_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 /*else if($('#form_departure_address_tours').val()=='')
				 {
					$('textarea[id="form_departure_address_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_time_tours').val()=='')
				 {
					$('input[id="form_departure_time_tours"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }*/
			 }
		  }
		 if(currentId=='progressbarpackage')
         {
			 if($(this).parent().attr('id')=='package1')
			 {
				 if($('#form_title_package').val()=='')
				 {
					  $('input[id="form_title_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#form_title_package').focus();
					  return false;
				 }
				 if($('#checkupdate_package').val()!='update')
				 {
					 if($('#form_image_package').val()=='')
					 {
						  $('input[id="form_image_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  return false;
					 }
				 }
				 else if($('#categoryID_package').val()=='')
				 {
					  $('select[id="categoryID_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#subcategoryID_package').val()=='')
				 {
					  $('select[id="subcategoryID_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_duration_package').val()=='')
				 {
					  $('input[id="form_duration_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_desc_package').val()=='')
				 {
					  $('textarea[id="form_desc_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_why_you_package').val()=='')
				 {
					  $('textarea[id="form_why_you_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_address_package').val()=='')
				 {
					  $('textarea[id="form_address_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			}
			 if($(this).parent().attr('id')=='package2')
			 {
                 if($('#form_name_package').val()=='')
				 {
					$('input[id="form_name_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_position_package').val()=='')
				 {
					$('input[id="form_position_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_email_package').val()=='')
				 {
					$('input[id="form_email_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_mobile_package').val()=='')
				 {
					$('input[id="form_mobile_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			}
			 if($(this).parent().attr('id')=='package3')
			 {
				 if($('#form_gender_package').val()=='')
				 {
					$('select[id="form_gender_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_age_package').val()=='')
				 {
					$('input[id="form_age_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_weight_package').val()=='')
				 {
					$('input[id="form_weight_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_what_allowed_package').val()=='')
				 {
					$('textarea[id="form_what_allowed_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_not_allowed_package').val()=='')
				 {
					$('input[id="form_not_allowed_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_additional_info_package').val()=='')
				 {
					$('input[id="form_additional_info_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 
			 }
			 if($(this).parent().attr('id')=='package7')
			 {
				 if($('#form_location_package').val()=='')
				 {
					$('input[id="form_location_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 if($(this).parent().attr('id')=='package4')
			 {
				 if($('#form_price_package').val()=='')
				 {
					$('input[id="form_price_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_price_package').val()=='')
				 {
					$('input[id="form_child_price_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_age_package').val()=='')
				 {
					$('input[id="form_child_age_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_grp_price_package').val()=='')
				 {
					$('input[id="form_grp_price_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 if($('#form_refundable_package_yes').is(':visible'))
				 {
					 if($('#form_cancel_before_package').val()=='')
					 {
						$('input[id="form_cancel_before_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
					 else if($('#form_not_cancel_package').val()=='')
					 {
						$('input[id="form_not_cancel_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
			 }
			 if($(this).parent().attr('id')=='package5')
			 {
				 if($('#form_departure_package').val()=='')
				 {
					$('textarea[id="form_departure_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_address_package').val()=='')
				 {
					$('textarea[id="form_departure_address_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_time_package').val()=='')
				 {
					$('input[id="form_departure_time_package"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
		  } 
		 if(currentId=='progressbaradventure')
		 {
			 if($(this).parent().attr('id')=='adventure1')
			 {
				 if($('#form_title_adventure').val()=='')
				 {
					  $('input[id="form_title_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#form_title_adventure').focus();
					  return false;
				 }
				 if($('#checkupdate_adventure').val()!='update')
				 {
					 if($('#form_image_adventure').val()=='')
					 {
						  $('input[id="form_image_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  return false;
					 }
				 }
				 else  if($('#categoryID_adventure').val()=='')
				 {
					  $('select[id="categoryID_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#subcategoryID_adventure').val()=='')
				 {
					  $('select[id="subcategoryID_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_duration_adventure').val()=='')
				 {
					  $('input[id="form_duration_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_desc_adventure').val()=='')
				 {
					  $('textarea[id="form_desc_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_why_you_adventure').val()=='')
				 {
					  $('textarea[id="form_why_you_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_address_adventure').val()=='')
				 {
					  $('textarea[id="form_address_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='adventure2')
			 {
				 if($('#form_name_adventure').val()=='')
				 {
					$('input[id="form_name_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_position_adventure').val()=='')
				 {
					$('input[id="form_position_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_email_adventure').val()=='')
				 {
					$('input[id="form_email_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_mobile_adventure').val()=='')
				 {
					$('input[id="form_mobile_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			  }
			 if($(this).parent().attr('id')=='adventure3')
			 {
				 if($('#form_gender_adventure').val()=='')
				 {
					$('select[id="form_gender_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_age_adventure').val()=='')
				 {
					$('input[id="form_age_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_weight_adventure').val()=='')
				 {
					$('input[id="form_weight_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_what_allowed_adventure').val()=='')
				 {
					$('textarea[id="form_what_allowed_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_not_allowed_adventure').val()=='')
				 {
					$('textarea[id="form_not_allowed_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_additional_info_adventure').val()=='')
				 {
					$('textarea[id="form_additional_info_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				
				
			 }
			 if($(this).parent().attr('id')=='adventure8')
			 {
				  if($('#form_location_adventure').val()=='')
				 {
					$('input[id="form_location_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 if($(this).parent().attr('id')=='adventure4')
			 {
				 if($('#form_price_adventure').val()=='')
				 {
					$('input[id="form_price_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_price_adventure').val()=='')
				 {
					$('input[id="form_child_price_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_age_adventure').val()=='')
				 {
					$('input[id="form_child_age_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_grp_price_adventure').val()=='')
				 {
					$('input[id="form_grp_price_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 if($('#form_refundable_adventure_yes').is(':visible'))
				 {
					 if($('#form_cancel_before_adventure').val()=='')
					 {
						$('input[id="form_cancel_before_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
					 else if($('#form_not_cancel_adventure').val()=='')
					 {
						$('input[id="form_not_cancel_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
			 }
			
			 if($(this).parent().attr('id')=='adventure5')
			 {
				 if($('#form_departure_adventure').val()=='')
				 {
					$('textarea[id="form_departure_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_address_adventure').val()=='')
				 {
					$('textarea[id="form_departure_address_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_time_adventure').val()=='')
				 {
					$('input[id="form_departure_time_adventure"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
		 }
		 if(currentId=='progressbarrecreation')
		 {
			 if($(this).parent().attr('id')=='recreation1')
			 {
				 if($('#form_title_recreation').val()=='')
				 {
					  $('input[id="form_title_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#form_title_recreation').focus();
					  return false;
				 }
				 if($('#checkupdat_recreationse').val()!='update')
				 {
					 if($('#form_image_recreation').val()=='')
					 {
						  $('input[id="form_image_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  return false;
					 }
				 }
				 else if($('#categoryID_recreation').val()=='')
				 {
					  $('select[id="categoryID_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#subcategoryID_recreation').val()=='')
				 {
					  $('select[id="subcategoryID_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_duration_recreation').val()=='')
				 {
					  $('input[id="form_duration_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_desc_recreation').val()=='')
				 {
					  $('textarea[id="form_desc_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_why_you_recreation').val()=='')
				 {
					  $('textarea[id="form_why_you_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#form_address_recreation').val()=='')
				 {
					  $('textarea[id="form_address_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='recreation2')
			 {
                 if($('#form_name_recreation').val()=='')
				 {
					$('input[id="form_name_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_position_recreation').val()=='')
				 {
					$('input[id="form_position_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_email_recreation').val()=='')
				 {
					$('input[id="form_email_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_email_recreation').val()=='')
				 {
					$('input[id="form_email_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 if($(this).parent().attr('id')=='recreation3')
			 {
				
				 if($('#form_gender_recreation').val()=='')
				 {
					$('select[id="form_gender_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_age_recreation').val()=='')
				 {
					$('input[id="form_age_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_weight_recreation').val()=='')
				 {
					$('input[id="form_weight_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_what_allowed_recreation').val()=='')
				 {
					$('textarea[id="form_what_allowed_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_not_allowed_recreation').val()=='')
				 {
					$('input[id="form_not_allowed_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_additional_info').val()=='')
				 {
					$('input[id="form_additional_info"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 
			 }
			 if($(this).parent().attr('id')=='recreation7')
			 {
				if($('#form_location_recreation').val()=='')
				 {
					$('input[id="form_location_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 if($(this).parent().attr('id')=='recreation4')
			 {
				 if($('#form_price_recreation').val()=='')
				 {
					$('input[id="form_price_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_price_recreation').val()=='')
				 {
					$('input[id="form_child_price_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_child_age_recreation').val()=='')
				 {
					$('input[id="form_child_age_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_grp_price_recreation').val()=='')
				 {
					$('input[id="form_grp_price_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 if($('#form_refundable_recreation_yes').is(':visible'))
				 {
					 if($('#form_cancel_before_recreation').val()=='')
					 {
						$('input[id="form_cancel_before_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
					 else if($('#form_not_cancel_recreation').val()=='')
					 {
						$('input[id="form_not_cancel_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
			 }
			 if($(this).parent().attr('id')=='recreation5')
			 {
				 if($('#form_departure_recreation').val()=='')
				 {
					$('textarea[id="form_departure_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_address_recreation').val()=='')
				 {
					$('textarea[id="form_departure_address_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#form_departure_time_recreation').val()=='')
				 {
					$('input[id="form_departure_time_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 } 
		 }
		 if(currentId=='progressbarcoupon')
		 {
			 if($(this).parent().attr('id')=='coupon1')
			 {
				 if($('#coupon_title').val()=='')
				 {
					  $('input[id="coupon_title"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#coupon_title').focus();
					  return false;
				 }
				 if($('#checkupdate_coupon').val()!='update')
				 {
					 if($('#coupon_image').val()=='')
					 {
						 $('input[id="coupon_image"]').after('<label class="error" for="review_desc">This field is required.</label>');
						 $('#coupon_image').focus();
						 return false; 
					 }
				 }
				 else if($('#coupon_category_id').val()=='')
				 {
					  $('select[id="coupon_category_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#coupon_subcategory_id').val()=='')
				 {
					  $('select[id="coupon_subcategory_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#coupon_location').val()=='')
				 { 
					  $('textarea[id="coupon_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#coupon_address').val()=='')
				 {
					  $('textarea[id="coupon_address"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#coupon_description').val()=='')
				 {
					  $('textarea[id="coupon_description"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='coupon2')
			 {
				 if($('#coupon_gender').val()=='')
				 {
				   $('select[id="coupon_gender"]').after('<label class="error" for="review_desc">This field is required.</label>');
				    return false;
				 } 
				 else if($('#age').val()=='')
				 {
					  $('input[id="age"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#weight').val()=='')
				 {
					  $('input[id="weight"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#coupon_what_allowed').val()=='')
				 {
					  $('input[id="coupon_what_allowed"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#coupon_not_allowed').val()=='')
				 {
					  $('input[id="coupon_not_allowed"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#additiona_info').val()=='')
				 {
					  $('textarea[id="additiona_info"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='coupon3')
			 {
				 if($('#price').val()=='')
				 {
					  $('input[id="price"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 } 
				 else if($('#price_unit').val()=='')
				 {
					  $('select[id="price_unit"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 } 
			 }
			 if($(this).parent().attr('id')=='coupon4')
			 {
				 
				 if($('#cancel_policy_yes').is(':visible'))
				 {
					 if($('#cancel_policy_price').val()=='')
					 {
						$('input[id="cancel_policy_price"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
					 else if($('#cancel_policy_yes').val()=='')
					 {
						$('input[id="form_not_cancel_recreation"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
			 
			 }
			 if($(this).parent().attr('id')=='coupon6')
			 {
				 if($('#coupon_user_name').val()=='')
				 {
					$('input[id="coupon_user_name"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#coupon_person_postion').val()=='')
				 {
					$('input[id="coupon_person_postion"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#coupon_email').val()=='')
				 {
					$('input[id="coupon_email"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#coupon_mobile').val()=='')
				 {
					$('input[id="coupon_mobile"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			  }
		 }
		 if(currentId=='progressbarevent')
		 {
			 if($(this).parent().attr('id')=='event1')
			 {
				 if($('#event_title').val()=='')
				 {
					  $('input[id="event_title"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#event_title').focus();
					  return false;
				 }
				 if($('#checkupdate_event').val()!='update')
				 {
					if($('#event_image').val()=='')
					 {
						  $('input[id="event_image"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  $('#event_title').focus();
						  return false;
					 }
				 }
				 else if($('#event_category_id').val()=='')
				 {
					  $('select[id="event_category_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#event_subcategory_id').val()=='')
				 {
					  $('select[id="event_subcategory_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#event_duration').val()=='')
				 {
					  $('input[id="coupon_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#event_description').val()=='')
				 {
					  $('textarea[id="event_description"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($(this).parent().attr('id')=='event2')
			 {
				 if($('#event_address').val()=='')
				 {
				   $('textarea[id="event_address"]').after('<label class="error" for="review_desc">This field is required.</label>');
				    return false;
				 } 
				 /*else if($('#age').val()=='')
				 {
					  $('input[id="age"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }*/
				
			 }
			 if($(this).parent().attr('id')=='event4')
			 {
				 if($('#event_user_name').val()=='')
				 {
					$('input[id="event_user_name"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#event_position').val()=='')
				 {
					$('input[id="event_position"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#event_email').val()=='')
				 {
					$('input[id="event_email"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#event_mobile').val()=='')
				 {
					$('input[id="event_mobile"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
		 }
		 if(currentId=='progressbartransportation')
		 {
			if($(this).parent().attr('id')=='transportation1')
			{
				 if($('#transportation_title').val()=='')
				 {
					  $('input[id="transportation_title"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#transportation_title').focus();
					  return false;
				 }
				 if($('#checkupdate_transportation').val()!='update')
				 {
					 if($('#transportation_image').val()=='')
					 {
						  $('input[id="transportation_image"]').after('<label class="error" for="review_desc">This field is required.</label>');
						  return false;
					 }
				 }
				 else  if($('#transportation_category_id').val()=='')
				 {
					  $('select[id="transportation_category_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#transportation_subcategory_id').val()=='')
				 {
					  $('select[id="transportation_subcategory_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#transportation_description').val()=='')
				 {
					  $('textarea[id="transportation_description"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			if($(this).parent().attr('id')=='transportation2')
			{
				 if($('#transportation_car_size').val()=='')
				 {
				    $('input[id="transportation_car_size"]').after('<label class="error" for="review_desc">This field is required.</label>');
				    return false;
				 } 
				 else if($('#transportation_car_model').val()=='')
				 {
					  $('input[id="transportation_car_model"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#transportation_car_brand').val()=='')
				 {
					  $('input[id="transportation_car_brand"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#transportation_car').val()=='')
				 {
					  $('input[id="transportation_car"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }	
				 else if($('#transportation_max_passanger').val()=='')
				 {
					  $('input[id="transportation_max_passanger"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }	
			 }
			if($(this).parent().attr('id')=='transportation4')
			{
				 if($('#transportation_available_time').val()=='')
				 {
					$('input[id="transportation_available_time"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_pickuptime').val()=='')
				 {
					$('input[id="transportation_pickuptime"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_pickup_location').val()=='')
				 {
					$('input[id="transportation_pickup_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_pickup_remarks').val()=='')
				 {
					$('input[id="transportation_pickup_remarks"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_drop_location').val()=='')
				 {
					$('input[id="transportation_drop_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#transportation_drop_remark').val()=='')
				 {
					$('input[id="transportation_drop_remark"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
		 }
		 if(animating) return false;
		 animating = true;
		 current_fs = $(this).parent();
		 next_fs = $(this).parent().next();
		 parent_id= $(this).closest(".active").attr("id"); 
		//alert($('#'+currentId).find("fieldset").index(next_fs));
		 //activate next step on progressbar using the index of next_fs
		 //
		 $("#"+currentId+" li").removeClass("current");
		 $("#"+currentId+" li").eq($('#'+parent_id).find('fieldset').index(next_fs)).addClass("active");
		 $("#"+currentId+" li").eq($('#'+parent_id).find('fieldset').index(next_fs)).addClass("current");
		 //show the next fieldset
		 next_fs.show(); 
		 //hide the current fieldset with style
		 current_fs.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50)+"%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({'transform': 'scale('+scale+')'});
				next_fs.css({'left': left, 'opacity': opacity});
			}, 
			duration: 0, 
			complete: function(){
				current_fs.hide();
				animating = false;
			}, 
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	
});
$(".previous").click(function(){
	var currentId=$(this).parent().attr('class');
	if(animating) return false;
	animating = true;
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
    parent_id= $(this).closest(".active").attr("id"); 
	//de-activate current step on progressbar
	$("#"+currentId+" li").removeClass("current");
	$("#"+currentId+" li").eq($('#'+parent_id).find('fieldset').index(current_fs)).removeClass("active");
	$("#"+currentId+" li").eq($('#'+parent_id).find('fieldset').index(previous_fs)).addClass("current");
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 0, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});
});
$(document).on('click','.deletelink',function(){
	var type=$(this).attr('rel');
    var delete_id= $(this).attr('data-id');
	var current =$(this);
	  $.ajax({
				type: "POST",
				data: { type : type, delete_id : delete_id},
				url: siteurl+"owner/deleteimage/",
				beforeSend: function(){
					  current.closest('.col-md-3').css('opacity','0.1');
					},
				success: function(result) {
					current.closest('.col-md-3').css('opacity','1');
					if(result=='success')
					{
					  current.closest('.col-md-3').remove();
					}
				  }
		    }); 
  
  });