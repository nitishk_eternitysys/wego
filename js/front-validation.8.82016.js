$(document).on('click','.editclass',function(){
    var accom_id=$(this).attr('rel');
    var funname= $(this).attr('data-action');
	var divName= $(this).attr('data-id');
      $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"editform/"+funname+"/",
				beforeSend: function(){
					  $('#'+divName).css('opacity','0.1');
					  $('#'+divName).html('Processing.....');
					},
				success: function(result) {
					$('.commancls').hide();
					$('#'+divName).show();
					$('#'+divName).css('opacity','1');
					$('#'+divName).html(result)
				 }
		    }); 

});
$(document).on('click','.addcls',function(){
    var accom_id=$(this).attr('rel');
	var manageId=$(this).parent().parent().parent().prev().attr('id');
	var parentID=$(this).parent().parent().parent().parent().attr('id');
	if(accom_id==manageId)
	{
	   $('#'+parentID).find('.commancls').hide();
	   $('#'+accom_id).show();	
	}
	else
	{
	   var funname= $(this).attr('data-action');
	   var divName= $(this).attr('data-id');
       $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"editform/"+funname+"/",
				beforeSend: function(){
					  $('#'+divName).css('opacity','0.1');
					  $('#'+divName).html('Processing.....');
					},
				success: function(result) {
				    $('#'+parentID).find('.commancls').hide();
					$('#'+divName).show();
					$('#'+divName).css('opacity','1');
					$('#'+divName).html(result)
				 }
		    }); 	
	}
   
 });
$(document).on('click','.removeaccom',function(){
	if(confirm("Are you sure you want to delete this records?"))
	{
	   var accom_id=$(this).attr('rel');
       $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"ajax/removeaccom/",
				beforeSend: function(){
					  $('#remove_'+accom_id).css('opacity','0.1');
					},
				success: function(result) {
					if(result=='success')
					{
					  $('#remove_'+accom_id).remove();  	
					}
				 }
		    }); 
	}
	else
	{
	   return false;
	}
});



$(document).on('change','#accom_category_id',function(){
	       var category_id = $('#accom_category_id').val();
	         $.ajax({
						type: "POST",
						data: {category_id:category_id},
						url: siteurl+"ajax/getSubcategory/",
						beforeSend: function(){
								},
						success: function(result) {
							$('#accom_subcategory_id').html(result);
							
						}
					});
});
$(document).on('change','#categoryID_tours',function(){
	var category_id = $('#categoryID_tours').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_tours').html(result);
			
		}
	});
});
$(document).on('change','#categoryID_adventure',function(){
	var category_id = $('#categoryID_adventure').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_adventure').html(result);
			
		}
	});
});
$(document).on('change','#categoryID_recreation',function(){
	var category_id = $('#categoryID_recreation').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_recreation').html(result);
			
		}
	});
});


$(document).on('click','.amenities',function(){
	var inputname =$(this).find('input').attr('name');
	var inputnameId =$(this).find('input').attr('id');
	//alert(inputname);
	$('input[name="'+inputname+'"]').removeAttr('checked');
	$("#"+inputnameId).attr('checked', 'checked');
	var name=$("[name='"+inputname+"'][checked]").attr("value");
    if(name=='yes')
	{
		$('#'+inputname+'_yes').show();
	}
	else
	{
	   $('#'+inputname+'_yes').hide();	
	   $('#'+inputname+'_yes').find('input').val('');	
	}
 });
 $(document).on('click','.discount',function(){
	    var inputname =$(this).find('input').attr('name');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
		if(name=='on')
		{
			$('#'+inputname+'_on').show();
		}
		else
		{
		   $('#'+inputname+'_on').hide();	
		}
 });
$(document).on('click','.cancellation',function(){
	    var inputname =$(this).find('input').attr('name');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
		if(name=='yes')
		{
			$('#'+inputname+'_yes').show();
		}
		else
		{
		   $('#'+inputname+'_yes').hide();	
		}
});




function OnlyNumericKeys(e) {
	 var key; 
	 if (navigator.appName == "Microsoft Internet Explorer") key = window.event.keyCode; else key = e.which; 
	 if((key!=13) && (key !=8) && (key!=0)&& (key!=46)) { 
	   if (key<48 || key>57) { 
	     if (navigator.appName == "Microsoft Internet Explorer")
		    window.event.keyCode=0; else e.preventDefault();
	      } 
	    } 
	  }

$(document).on('change','#country',function(){
   var id = $(this).val();
    $.ajax({
						type: "POST",
						data: ({countryID: id}),
						url: siteurl+"ajax/selectState/"+id,
						beforeSend: function(){
								},
						success: function(result) {
							$('#state').html(result);
							
						}
        			});
                return false;
});
$('#loginfrm_user_rating').validate({
	 submitHandler: function(form) {
		    $('#btn_login').html('Processing....');
		     $.ajax({
						type: "POST",
						data: $('#loginfrm_user_rating').serialize(),
						url: siteurl+"ajax/login/",
						beforeSend: function(){
								},
						success: function(result) {
							 $('#btn_login').html('LOGIN <i class="fa fa-sign-in"></i>');
							 if(result=='success')
							 {
							   location.reload();
							 }
							 else
							 {
								$('#alerterror').show()
								$('#alertsuccess').hide()
								$('#alerterror').html(result);  
							 }
							
						}
        			});
                return false;
			 },	  
	   });


$('#reviewform').validate({
	 submitHandler: function(form) {
		     $('#btn_review').html('Processing....');
		     $.ajax({
						type: "POST",
						data: $('#reviewform').serialize(),
						url: siteurl+"ajax/rating/",
						beforeSend: function(){
								},
						success: function(result) {
							 $('#btn_review').html('Submit');
							 if(result=='success')
							 {
								$('#review_title').val('');
								$('#review_desc').val(''); 
							    $('#alertsuccess').show()
								$('#alerterror').hide()
								$('#alertsuccess').html('Thanks for your ratings!');   
							 }
							 else
							 {
								$('#alerterror').show()
								$('#alertsuccess').hide()
								$('#alerterror').html(result);   
							 }
							 $('#review_title').val('');
							 $('#review_desc').val('');
						}
        			});
                return false;
			 },	  
	   });

$(document).on('click','.addwishlist',function(){
    var product_id = $(this).attr('rel');
    var user_id = $(this).attr('data-id');
    $.ajax({
			type: "POST",
			url: siteurl+"ajax/wishlist/"+user_id+"/"+product_id,
			beforeSend: function(){
					},
			success: function(result) {
				 if(result=='ok')
				 {
				   alert('Boat added to wishlist!');
				   $(this).removeAttr('class');
				 }
				 if(result=='already')
				 {
					alert('Boat already added on wishlist !');
					return false;
				 }
				 if(result=='error')
				 {
					alert('Boat adding wishlist error !');
					return false; 
				 }
			}
         });
});
$(document).ready(function()
{
	//chk email address already exist or not
	$('.chk_email_address').blur(function()
	{
		
		var emailaddress=jQuery('#user_email').val();
		var functionname=jQuery('#chkclass').val();
		var filter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if(emailaddress=="" || !filter.test(emailaddress))
		{
			 document.getElementById('user_email').value='';
			 jQuery('#user_email').css('border-color', 'red');
			 jQuery('#user_email').attr("placeholder",ENTER_EMAIL);
			 jQuery('#user_email').keyup(function () { jQuery('#user_email').css('border-color', ''); });
		}
		else
		{
			var param={email:emailaddress};
			jQuery.ajax({
			url:site_url+'ajax/cheakemail/',
			type:'POST',
			data:param,
			success:function(res)
			{ 
				if(res=="exist")
				{
					jQuery('#user_email1').fadeIn("slow");	
					jQuery('#user_email').addClass('myerror');
					jQuery("#user_email1").css('color','red');
					jQuery("#user_email1").html(EMAIL + ALREADY_EXIST);
					jQuery("#user_email1").css("font-size",14);
					jQuery("#btn_user_registration").attr("disabled",true);
				}
				else
				{
					jQuery('#user_email1').fadeIn("slow");	
					jQuery("#user_email1").css('color','green');
					jQuery("#user_email1").html(EMAIL + AVAILABLE);
					jQuery("#user_email1").css("font-size",14);
					jQuery('#user_email1').fadeOut(2000);
					jQuery('#user_email').removeClass('myerror');
					jQuery("#btn_user_registration").removeAttr("disabled");
				}
				return true;
			}
	});
			
		}
	});	
	$('#add-image').click(function(){
		flag=1
		$('.pimg').each(function(e){
		  var pdf_id=$(this).attr('id','product_image_'+e);
		  var id_check=$(this).attr('id');
		 
		   if($('#'+id_check).val()=='')
		   {
			    $('#error_product_image').css('margin-left','230px');	
				$('#error_product_image').show();
				$('#error_product_image').fadeIn(3000);
				document.getElementById('error_product_image').innerHTML="The Image uploaded is required.";
				setTimeout(function(){
				 $('#error_product_image').fadeOut('slow');
				},3000);3
				flag=0;
				return false;
		   }
		   var chkpdf = $('#'+id_check).val().split(".");
		   var extension = chkpdf[1];
		   
		   if(extension!='jpg' && extension!='JPG' && extension!='png' && extension!='PNG' && extension!='jpeg' && extension!='JPEG' && extension!='gif' && extension!='GIF')	
		   {
			 $('#error_product_image1').css('margin-left','230px')
			 $('#error_product_image1').show();
			 $('#error_product_image1').fadeIn(3000);	
			 document.getElementById('error_product_image1').innerHTML="The file type you are attempting to upload is not allowed.";
			 setTimeout(function(){
				$('#product_image').css('border-color','#dddfe0');
				$('#error_product_image1').fadeOut('slow');
			 },3000);
			 flag=0;
			 if($('.class-add').length>1)
			  {$('#imageappend.class-add:last').last().remove();}
			  else
			  {$("#imageappend.class-add").find("input").val("");}
			 return false;
		   }
		})
		if(flag==1)
		{
			$('#imageappend').clone().appendTo('#append');
			$("#imageappend.class-add:last").find("input").val("");
			$('#error_product_image').fadeOut('slow');
		}
	});
	$('#remove-image').click(function(){
	  if($('.class-add').length>1)
	  {$('#imageappend.class-add:last').last().remove();}
	  else
	  {$("#imageappend.class-add").find("input").val("");}
	});	
	$('#remove-price').click(function(){
	  if($('.class-add-price').length>1)
	  {$('#priceappend.class-add-price:last').last().remove();}
	  else
	  {$("#priceappend.class-add-price").find("input").val("");}
	});	
	$('#remove-video').click(function(){
	  if($('.class-add-video').length>1)
	  {$('#videoappend.class-add-video:last').last().remove();}
	  else
	  {$("#videoappend.class-add-video").find("input").val("");}
	});	
	//registration validation
	$('#btn_user_registration').click(function()
	{
		var gender=$('#gender').val(); 
		var first_name=$('#first_name').val(); 
		var last_name=$('#last_name').val(); 
		var user_email=$('#user_email').val(); 
		var password=$('#password').val(); 
		var confirm_pw=$('#confirm_pw').val();
		var filter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var checked=$("input[name='gender']:checked").length;
		
		if(checked==0)
		{
			
			 $('#gendererr').fadeIn("slow");	
			 $('#gendererr').html(SELECT_GENDER);
			 $('#gendererr').keyup(function () { $('#gendererr').removeClass('myerror'); });
			 $('#gendererr').html(SELECT_GENDER);
			 $('#gendererr').fadeOut(2000);
			 return false;
		} 
		else if(first_name=="")
		{
			  $('#first_name1').fadeIn("slow");
			 $('#first_name').addClass('myerror');
			 $('#first_name1').html(REG_ENTER_FIRST_NM);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#first_name').keyup(function () { $('#first_name').removeClass('myerror'); });
			 $('#first_name1').fadeOut(2000);
			 $('#first_name').focus();
			 return false;
		} 
		else if(last_name=="")
		{
			 $('#last_name1').fadeIn("slow");
			 $('#last_name').addClass('myerror');
			 $('#last_name1').html(REG_ENTER_LAST_NM);
			 $('#last_name').keyup(function () { $('#last_name').removeClass('myerror'); });
			 $('#last_name1').fadeOut(2000);
			 $('#last_name').focus();
			 return false;
		} 
		else if(user_email=="" || !filter.test(user_email))
		{
			 $('#user_email1').fadeIn("slow");
			 $('#user_email').addClass('myerror');
			 $('#user_email1').html(INVALID_EMAIL);
			 $('#user_email').keyup(function () { $('#user_email').removeClass('myerror'); });
			 $('#user_email1').fadeOut(2000);
			 $('#user_email').focus();
			 return false;
		} 
		else if(password=="")
		{
			 $('#password1').fadeIn("slow");
			 $('#password').addClass('myerror');
			 $('#password1').html(ENTER_PW);
			 $('#password').keyup(function () { jQuery('#password').removeClass('myerror'); });
			 $('#password1').fadeOut(2000);
			 $('#password').focus();
			 return false;
		}
		else if(password.length < 6)
		{
			 $('#password1').fadeIn("slow");
			 $('#password').addClass('myerror');
			 $('#password1').html(ERR_MIN_LENTH);
			 $('#password').keyup(function () { jQuery('#password').removeClass('myerror'); });
			 $('#password1').fadeOut(2000);
			 $('#password').focus();
			 return false;
		}
		else if(confirm_pw=="")
		{
			 $('#confirm_pw1').fadeIn("slow");
			 $('#confirm_pw').addClass('myerror');
			 $('#confirm_pw1').html(CONFIRM_PW);
			 $('#confirm_pw').keyup(function () { jQuery('#confirm_pw').removeClass('myerror'); });
			 $('#confirm_pw1').fadeOut(2000);
			 $('#confirm_pw').focus();
			 return false;
		}
		else if(password!=confirm_pw)
		{
			 $('#confirm_pw1').fadeIn("slow");
			 $('#confirm_pw').val('');
			 $('#confirm_pw').addClass('myerror');
			 $('#confirm_pw1').html(INCORRECT_PW_CONFIMATION);
			 $('#confirm_pw').keyup(function () { jQuery('#confirm_pw').removeClass('myerror'); });
			 $('#confirm_pw1').fadeOut(2000);
			 $('#confirm_pw').focus();
			 return false;
		}
	});
	//login function validation
	$('#btn_user_login').click(function()
	{
		var email_address=$('#email_address').val(); 
		var login_password=$('#login_password').val(); 
		
		if(email_address=="")
		{
			 jQuery('#email_address').addClass('myerror');
			 jQuery('#email_address').attr("placeholder",ENTER_EMAIL);
			 jQuery('#email_address').keyup(function () { jQuery('#email_address').removeClass('myerror'); });
			 jQuery('#email_address').focus();
			 return false;
		} 
		else if(login_password=="")
		{
			 jQuery('#login_password').addClass('myerror');
			 jQuery('#login_password').attr("placeholder",ENTER_PW);
			 jQuery('#login_password').keyup(function () { jQuery('#login_password').removeClass('myerror'); });
			 jQuery('#login_password').focus();
			 return false;
		} 
	});
	//login function validation
	$('#btn_merchant_login').click(function()
	{
		var email_address=$('#email_address').val(); 
		var login_password=$('#login_password').val(); 
		
		if(email_address=="")
		{
			 jQuery('#email_address').addClass('myerror');
			 jQuery('#email_address').attr("placeholder",ENTER_EMAIL);
			 jQuery('#email_address').keyup(function () { jQuery('#email_address').removeClass('myerror'); });
			 jQuery('#email_address').focus();
			 return false;
		} 
		else if(login_password=="")
		{
			 jQuery('#login_password').addClass('myerror');
			 jQuery('#login_password').attr("placeholder",ENTER_PW);
			 jQuery('#login_password').keyup(function () { jQuery('#login_password').removeClass('myerror'); });
			 jQuery('#login_password').focus();
			 return false;
		} 
	});
	//forget password validation
	$('#btn_forget_password').click(function()
	{
		var email_address=$('#email_address').val(); 
		
		if(email_address=="")
		{
			 jQuery('#email_address').addClass('myerror');
			 jQuery('#email_address').attr("placeholder",ENTER_EMAIL);
			 jQuery('#email_address').keyup(function () { jQuery('#email_address').removeClass('myerror'); });
			 jQuery('#email_address').focus();
			 return false;
		} 
	});
	//change password validation
	$('#btn_change_pw').click(function()
	{
		var new_pw=$('#new_pw').val(); 
		var con_pw=$('#con_pw').val(); 
		
		if(new_pw=="")
		{
			 jQuery('#new_pw').addClass('myerror');
			 jQuery('#new_pw').attr("placeholder",ENTER_PW);
			 jQuery('#new_pw').keyup(function () { jQuery('#new_pw').removeClass('myerror'); });
			 jQuery('#new_pw').focus();
			 return false;
		}
		else if(con_pw=="")
		{
			 jQuery('#con_pw').addClass('myerror');
			 jQuery('#con_pw').attr("placeholder",CONFIRM_PW);
			 jQuery('#con_pw').keyup(function () { jQuery('#con_pw').removeClass('myerror'); });
			 jQuery('#con_pw').focus();
			 return false;
		}
		else if(new_pw!=con_pw)
		{
			 $('#con_pw').val('');
			 jQuery('#con_pw').addClass('myerror');
			 jQuery('#con_pw').attr("placeholder",INCORRECT_PW_CONFIMATION);
			 jQuery('#con_pw').keyup(function () { jQuery('#con_pw').removeClass('myerror'); });
			 jQuery('#con_pw').focus();
			 return false;
		} 
	});
	//change password validation
	$('#update_merchant_password').click(function()
	{
		var new_pw=$('#new_pw').val(); 
		var con_pw=$('#con_pw').val(); 
		
		if(new_pw=="")
		{
			 jQuery('#new_pw').addClass('myerror');
			 jQuery('#new_pw').attr("placeholder",ENTER_PW);
			 jQuery('#new_pw').keyup(function () { jQuery('#new_pw').removeClass('myerror'); });
			 jQuery('#new_pw').focus();
			 return false;
		}
		else if(con_pw=="")
		{
			 jQuery('#con_pw').addClass('myerror');
			 jQuery('#con_pw').attr("placeholder",CONFIRM_PW);
			 jQuery('#con_pw').keyup(function () { jQuery('#con_pw').removeClass('myerror'); });
			 jQuery('#con_pw').focus();
			 return false;
		}
		else if(new_pw!=con_pw)
		{
			 $('#con_pw').val('');
			 jQuery('#con_pw').addClass('myerror');
			 jQuery('#con_pw').attr("placeholder",INCORRECT_PW_CONFIMATION);
			 jQuery('#con_pw').keyup(function () { jQuery('#con_pw').removeClass('myerror'); });
			 jQuery('#con_pw').focus();
			 return false;
		} 
	});
	//merchant registratio validation
	$('#btn_merchant_registration').click(function()
	{
		var merchant_comp=$('#merchant_comp').val(); 
		var password=$('#password').val(); 
		var confirm_password=$('#confirm_password').val(); 
		var address=$('#address').val(); 
		var city=$('#city').val();
		var website=$('#website').val();
		var ph_no=$('#ph_no').val();
		var post_code=$('#post_code').val();
		var business_act=$('#business_act').val();
		var business_desc=$('#business_desc').val();
		var bank_details=$('#bank_details').val();
		var filter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var filterweb=/(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
		var checked=$("input[name='gender']:checked").length;
		
		if(merchant_comp=="")
		{
			  $('#merchant_comp1').fadeIn("slow");
			 $('#merchant_comp').addClass('myerror');
			 $('#merchant_comp1').html(REG_ENTER_FIRST_NM);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#merchant_comp').keyup(function () { $('#merchant_comp').removeClass('myerror'); });
			 $('#merchant_comp1').fadeOut(2000);
			 $('#merchant_comp').focus();
			 return false;
		} 
		else if(password=="")
		{
			 $('#password1').fadeIn("slow");
			 $('#password').addClass('myerror');
			 $('#password1').html(ENTER_PW);
			 $('#password').keyup(function () { jQuery('#password').removeClass('myerror'); });
			 $('#password1').fadeOut(2000);
			 $('#password').focus();
			 return false;
		}
		else if(password.length < 6)
		{
			 $('#password1').fadeIn("slow");
			 $('#password').addClass('myerror');
			 $('#password1').html(ERR_MIN_LENTH);
			 $('#password').keyup(function () { jQuery('#password').removeClass('myerror'); });
			 $('#password1').fadeOut(2000);
			 $('#password').focus();
			 return false;
		}
		else if(confirm_password=="")
		{
			 $('#confirm_password1').fadeIn("slow");
			 $('#confirm_password').addClass('myerror');
			 $('#confirm_password1').html(CONFIRM_PW);
			 $('#confirm_password').keyup(function () { jQuery('#confirm_password').removeClass('myerror'); });
			 $('#confirm_password1').fadeOut(2000);
			 $('#confirm_password').focus();
			 return false;
		}
		else if(password!=confirm_password)
		{
			 $('#confirm_password1').fadeIn("slow");
			 $('#confirm_password').val('');
			 $('#confirm_password').addClass('myerror');
			 $('#confirm_password1').html(INCORRECT_PW_CONFIMATION);
			 $('#confirm_password').keyup(function () { jQuery('#confirm_password').removeClass('myerror'); });
			 $('#confirm_password1').fadeOut(2000);
			 $('#confirm_password').focus();
			 return false;
		} 
		else if(address=="")
		{
			 $('#address1').fadeIn("slow");
			 $('#address').addClass('myerror');
			 $('#address1').html(ENTER_ADDRESS);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#address').keyup(function () { $('#address').removeClass('myerror'); });
			 $('#address1').fadeOut(2000);
			 $('#address').focus();
			 return false;
		} 
		else if(post_code=="" || isNaN(post_code))
		{
			  $('#post_code1').fadeIn("slow");
			 $('#post_code').addClass('myerror');
			 $('#post_code1').html(ENTER_POSTCODE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#post_code').keyup(function () { $('#post_code').removeClass('myerror'); });
			 $('#post_code1').fadeOut(2000);
			 $('#post_code').focus();
			 return false;
		} 
		else if(city=="")
		{
			 $('#city1').fadeIn("slow");
			 $('#city').addClass('myerror');
			 $('#city1').html(ENTER_CITY);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#city').keyup(function () { $('#city').removeClass('myerror'); });
			 $('#city1').fadeOut(2000);
			 $('#city').focus();
			 return false;
		} 
	
	else if(website=="" || !filterweb.test(website))
		{
			  $('#website1').fadeIn("slow");
			 $('#website').addClass('myerror');
			 $('#website1').html(ENTER_WEBSITE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#website').keyup(function () { $('#website').removeClass('myerror'); });
			 $('#website1').fadeOut(2000);
			 $('#website').focus();
			 return false;
		} 
		else if(ph_no=="" || isNaN(ph_no))
		{
			  $('#ph_no1').fadeIn("slow");
			 $('#ph_no').addClass('myerror');
			 $('#ph_no1').html(ENTER_PHONE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#ph_no').keyup(function () { $('#ph_no').removeClass('myerror'); });
			 $('#ph_no1').fadeOut(2000);
			 $('#ph_no').focus();
			 return false;
		} 
	else if(business_act=="")
		{
			  $('#business_act1').fadeIn("slow");
			 $('#business_act').addClass('myerror');
			 $('#business_act1').html(ENTER_BUSINESS_ACTIVITY);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#business_act').keyup(function () { $('#business_act').removeClass('myerror'); });
			 $('#business_act1').fadeOut(2000);
			 $('#business_act').focus();
			 return false;
		} 
		else if(business_desc=="")
		{
			  $('#business_desc1').fadeIn("slow");
			 $('#business_desc').addClass('myerror');
			 $('#business_desc1').html(ENTER_BUSINESS_DESC);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#business_desc').keyup(function () { $('#business_desc').removeClass('myerror'); });
			 $('#business_desc1').fadeOut(2000);
			 $('#business_desc').focus();
			 return false;
		} 
		else if(bank_details=="")
		{
			  $('#bank_details1').fadeIn("slow");
			 $('#bank_details').addClass('myerror');
			 $('#bank_details1').html(ENTER_BANK_DETAILS);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#bank_details').keyup(function () { $('#bank_details').removeClass('myerror'); });
			 $('#bank_details1').fadeOut(2000);
			 $('#bank_details').focus();
			 return false;
		} 
	});
	$('#update_merchant_profile').click(function()
	{
		var merchant_comp=$('#merchant_comp').val(); 
		var contact_email=$('#contact_email').val(); 
		var address=$('#address').val(); 
		var city=$('#city').val();
		var website=$('#website').val();
		var ph_no=$('#ph_no').val();
		var post_code=$('#post_code').val();
		var business_act=$('#business_act').val();
		var business_desc=$('#business_desc').val();
		var bank_details=$('#bank_details').val();
		var filter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var filterweb=/(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
		var checked=$("input[name='gender']:checked").length;
		
		if(merchant_comp=="")
		{
			  $('#merchant_comp1').fadeIn("slow");
			 $('#merchant_comp').addClass('myerror');
			 $('#merchant_comp1').html(REG_ENTER_FIRST_NM);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#merchant_comp').keyup(function () { $('#merchant_comp').removeClass('myerror'); });
			 $('#merchant_comp1').fadeOut(2000);
			 $('#merchant_comp').focus();
			 return false;
		} 
		else if(contact_email=="" || !filter.test(contact_email))
		{
			 $('#contact_email1').fadeIn("slow");
			 $('#contact_email').addClass('myerror');
			 $('#contact_email1').html(INVALID_EMAIL);
			 $('#contact_email').keyup(function () { $('#contact_email').removeClass('myerror'); });
			 $('#contact_email1').fadeOut(2000);
			 $('#contact_email').focus();
			 return false;
		}
		else if(address=="")
		{
			 $('#address1').fadeIn("slow");
			 $('#address').addClass('myerror');
			 $('#address1').html(ENTER_ADDRESS);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#address').keyup(function () { $('#address').removeClass('myerror'); });
			 $('#address1').fadeOut(2000);
			 $('#address').focus();
			 return false;
		} 
		else if(post_code=="" || isNaN(post_code))
		{
			  $('#post_code1').fadeIn("slow");
			 $('#post_code').addClass('myerror');
			 $('#post_code1').html(ENTER_POSTCODE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#post_code').keyup(function () { $('#post_code').removeClass('myerror'); });
			 $('#post_code1').fadeOut(2000);
			 $('#post_code').focus();
			 return false;
		} 
		else if(city=="")
		{
			 $('#city1').fadeIn("slow");
			 $('#city').addClass('myerror');
			 $('#city1').html(ENTER_CITY);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#city').keyup(function () { $('#city').removeClass('myerror'); });
			 $('#city1').fadeOut(2000);
			 $('#city').focus();
			 return false;
		} 
	
	else if(website=="" || !filterweb.test(website))
		{
			  $('#website1').fadeIn("slow");
			 $('#website').addClass('myerror');
			 $('#website1').html(ENTER_WEBSITE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#website').keyup(function () { $('#website').removeClass('myerror'); });
			 $('#website1').fadeOut(2000);
			 $('#website').focus();
			 return false;
		} 
		else if(ph_no=="" || isNaN(ph_no))
		{
			  $('#ph_no1').fadeIn("slow");
			 $('#ph_no').addClass('myerror');
			 $('#ph_no1').html(ENTER_PHONE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#ph_no').keyup(function () { $('#ph_no').removeClass('myerror'); });
			 $('#ph_no1').fadeOut(2000);
			 $('#ph_no').focus();
			 return false;
		} 
	else if(business_act=="")
		{
			  $('#business_act1').fadeIn("slow");
			 $('#business_act').addClass('myerror');
			 $('#business_act1').html(ENTER_BUSINESS_ACTIVITY);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#business_act').keyup(function () { $('#business_act').removeClass('myerror'); });
			 $('#business_act1').fadeOut(2000);
			 $('#business_act').focus();
			 return false;
		} 
		else if(business_desc=="")
		{
			  $('#business_desc1').fadeIn("slow");
			 $('#business_desc').addClass('myerror');
			 $('#business_desc1').html(ENTER_BUSINESS_DESC);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#business_desc').keyup(function () { $('#business_desc').removeClass('myerror'); });
			 $('#business_desc1').fadeOut(2000);
			 $('#business_desc').focus();
			 return false;
		} 
		else if(bank_details=="")
		{
			  $('#bank_details1').fadeIn("slow");
			 $('#bank_details').addClass('myerror');
			 $('#bank_details1').html(ENTER_BANK_DETAILS);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#bank_details').keyup(function () { $('#bank_details').removeClass('myerror'); });
			 $('#bank_details1').fadeOut(2000);
			 $('#bank_details').focus();
			 return false;
		} 
	});
	//Enquiry sending validations
	$('#submit_query').click(function()
	{
		var contact_name=$('#contact_name').val(); 
		var email=$('#email').val(); 
		var phone_number=$('#phone_number').val(); 
		var subject=$('#subject').val(); 
		var description=$('#description').val();
		var filter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if(contact_name=="")
		{
			  $('#contact_name_error').fadeIn("slow");
			 $('#contact_name').addClass('myerror');
			 $('#contact_name_error').html(REG_ENTER_FIRST_NM);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#contact_name').keyup(function () { $('#contact_name').removeClass('myerror'); });
			 $('#contact_name_error').fadeOut(2000);
			 $('#contact_name').focus();
			 return false;
		} 
		else if(email=="" || !filter.test(email) )
		{
			 $('#email_error').fadeIn("slow");
			 $('#email').addClass('myerror');
			 $('#email_error').html(INVALID_EMAIL);
			 $('#email').keyup(function () { $('#email').removeClass('myerror'); });
			 $('#email_error').fadeOut(2000);
			 $('#email').focus();
			 return false;
		}
		if(phone_number=="")
		{
			  $('#phone_number_error').fadeIn("slow");
			 $('#phone_number').addClass('myerror');
			 $('#phone_number_error').html(ENTER_PHONE);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#phone_number').keyup(function () { $('#phone_number').removeClass('myerror'); });
			 $('#phone_number_error').fadeOut(2000);
			 $('#phone_number').focus();
			 return false;
		} 
		if(subject=="")
		{
			  $('#subject_error').fadeIn("slow");
			 $('#subject').addClass('myerror');
			 $('#subject_error').html(REG_ENTER_FIRST_NM);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#subject').keyup(function () { $('#subject').removeClass('myerror'); });
			 $('#subject_error').fadeOut(2000);
			 $('#subject').focus();
			 return false;
		} 
		if(description=="")
		{
			  $('#description_error').fadeIn("slow");
			 $('#description').addClass('myerror');
			 $('#description_error').html(REG_ENTER_FIRST_NM);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#description').keyup(function () { $('#description').removeClass('myerror'); });
			 $('#description_error').fadeOut(2000);
			 $('#description').focus();
			 return false;
		} 
		
	});
	//register for email alert
	$('#btn_add_for_email').click(function()
	{
			var first_name_header=$('#first_name_header').val(); 
			var email_header=$('#email_header').val(); 
			var select_cat_header=$('#select_cat_header').val(); 
			var alert_type_header=$('#alert_type_header').val(); 
			var filter=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		
		if(first_name_header=="")
		{
			  $('#first_name_header_err').fadeIn("slow");
			 $('#first_name_header').addClass('myerror');
			 $('#first_name_header_err').html(ERR_NECCESSORY_FIELD);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#first_name_header').keyup(function () { $('#first_name_header').removeClass('myerror'); });
			 $('#first_name_header_err').fadeOut(2000);
			 $('#first_name_header').focus();
			 return false;
		} 
		else if(email_header=="" || !filter.test(email_header))
		{
			  $('#email_header_err').fadeIn("slow");
			 $('#email_header').addClass('myerror');
			 $('#email_header_err').html(INVALID_EMAIL);
			 $('#email_header').change(function () { $('#email_header').removeClass('myerror'); });
			 $('#email_header_err').fadeOut(2000);
			 $('#email_header').focus();
			 return false;
		} 
		else 	if(select_cat_header=="")
		{
			  $('#select_cat_header1').fadeIn("slow");
			 $('#select_cat_header').addClass('myerror');
			 $('#select_cat_header1').html(ERR_NECCESSORY_FIELD);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#select_cat_header').change(function(){  $('#select_cat_header').removeClass('myerror');   });
			 $('#select_cat_header1').fadeOut(2000);
			 $('#first_name_header').focus();
			 return false;
		} 
		else 	if(alert_type_header=="")
		{
			  $('#alert_type_header_err').fadeIn("slow");
			 $('#alert_type_header').addClass('myerror');
			 $('#alert_type_header_err').html(ERR_NECCESSORY_FIELD);//attr("placeholder",REG_ENTER_FIRST_NM);
			 $('#alert_type_header').change(function(){  $('#alert_type_header').removeClass('myerror');   });
			  $('#alert_type_header_err').fadeOut(2000);
			 $('#alert_type_header').focus();
			 return false;
		} 
		else {
			var param={first_name:first_name_header,email:email_header,cat_id:select_cat_header,type:alert_type_header};
			$.ajax({
					
					url: site_url+'ajax/subscribeforemailalert/',
					type:'POST',
					data:param,
					success:function(res)
					{ 
					 	if(res=='ok')
						{	$('#headererror').css('display','none');
							$('#headersuccess').css('display','block');
							$('#headersuccess').html(SUCC_INSERT);
						
						}
						else if(res=='nook')
						{	
							$('#headersuccess').css('display','none');
							$('#headererror').css('display','block');
							$('#headererror').html(ERR_INSERT);
							
						}
						else if(res=='already')
						{	
							$('#headersuccess').css('display','none');
							$('#headererror').css('display','block');
							$('#headererror').html(RECORD_EXIST);
						}
							
							$('#first_name_header').val(''); 
							$('#email_header').val(''); 
							$("#select_cat_header").val([]);
							$("#alert_type_header").val([]);
						}
				});	
				}
	});

});