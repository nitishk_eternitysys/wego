$(document).on('click','.editclass',function(){
	var hideDiv = $(this).closest('.active').attr('id');
	var accom_id=$(this).attr('rel');
    var funname= $(this).attr('data-action');
	var divName= $(this).attr('data-id');
      $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"editform/"+funname+"/",
				beforeSend: function(){
					  $('#'+divName).css('opacity','0.1');
					  $('#'+divName).html('Processing.....');
					},
				success: function(result) {
					$('#'+hideDiv).find('.commancls').hide();
					$('#'+divName).show();
					$('#'+divName).css('opacity','1');
					$('#'+divName).html(result)
				 }
		    }); 

});
$(document).on('click','.addcls',function(){
    var accom_id=$(this).attr('rel');
	var manageId=$(this).parent().parent().parent().prev().attr('id');
	var parentID=$(this).parent().parent().parent().parent().attr('id');
	if(accom_id==manageId)
	{
	   $('#'+parentID).find('.commancls').hide();
	   $('#'+accom_id).show();	
	}
	else
	{
	   var funname= $(this).attr('data-action');
	   var divName= $(this).attr('data-id');
       $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"editform/"+funname+"/",
				beforeSend: function(){
					  $('#'+divName).css('opacity','0.1');
					  $('#'+divName).html('Processing.....');
					},
				success: function(result) {
				    $('#'+parentID).find('.commancls').hide();
					$('#'+divName).show();
					$('#'+divName).css('opacity','1');
					$('#'+divName).html(result)
				 }
		    }); 	
	}
   
 });
$(document).on('click','.removeaccom',function(){
	if(confirm("Are you sure you want to delete this records?"))
	{
	   var $this=$(this); 	
	   var type = $(this).attr('rel')
	   var accom_id=$(this).attr('data-id');
       $.ajax({
				type: "POST",
				data: { common_id : accom_id,type : type},
				url: siteurl+"ajax/removeaccom/",
				beforeSend: function(){
					  $this.closest('.item-entry').css('opacity','0.1');
					},
				success: function(result) {
					if(result=='success')
					{
					   $this.closest('.item-entry').remove();  	
					}
				 }
		    }); 
	}
	else
	{
	   return false;
	}
});
$(document).on('change','#accom_category_id',function(){
	       var category_id = $('#accom_category_id').val();
	         $.ajax({
						type: "POST",
						data: {category_id:category_id},
						url: siteurl+"ajax/getSubcategory/",
						beforeSend: function(){
								},
						success: function(result) {
							$('#accom_subcategory_id').html(result);
							
						}
					});
});
$(document).on('change','#categoryID_tours',function(){
	var category_id = $('#categoryID_tours').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_tours').html(result);
			
		}
	});
});
$(document).on('change','#categoryID_adventure',function(){
	var category_id = $('#categoryID_adventure').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_adventure').html(result);
			
		}
	});
});
$(document).on('change','#categoryID_recreation',function(){
	var category_id = $('#categoryID_recreation').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_recreation').html(result);
			
		}
	});
});
$(document).on('change','#transportation_category_id',function(){
	var category_id = $('#transportation_category_id').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#transportation_subcategory_id').html(result);
			
		}
	});
});
$(document).on('change','#event_category_id',function(){
	var category_id = $('#event_category_id').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#event_subcategory_id').html(result);
			
		}
	});
});
$(document).on('change','#coupon_category_id',function(){
	var category_id = $('#coupon_category_id').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategory_id').html(result);
			
		}
	});
});




$(document).on('click','.amenities',function(){
	var inputname =$(this).find('input').attr('name');
	var inputnameId =$(this).find('input').attr('id');
	//alert(inputname);
	$('input[name="'+inputname+'"]').removeAttr('checked');
	$("#"+inputnameId).attr('checked', 'checked');
	var name=$("[name='"+inputname+"'][checked]").attr("value");
    if(name=='yes')
	{
		$('#'+inputname+'_yes').show();
	}
	else
	{
	   $('#'+inputname+'_yes').hide();	
	   $('#'+inputname+'_yes').find('input').val('');	
	}
 });
 $(document).on('click','.discount',function(){
	    var inputname =$(this).find('input').attr('name');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
		if(name=='on')
		{
			$('#'+inputname+'_on').show();
		}
		else
		{
		   $('#'+inputname+'_on').hide();	
		}
 });
$(document).on('click','.cancellation',function(){
	    var inputname =$(this).find('input').attr('name');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
		if(name=='yes')
		{
			$('#'+inputname+'_yes').show();
		}
		else
		{
		   $('#'+inputname+'_yes').hide();	
		}
});




function OnlyNumericKeys(e) {
	 var key; 
	 if (navigator.appName == "Microsoft Internet Explorer") key = window.event.keyCode; else key = e.which; 
	 if((key!=13) && (key !=8) && (key!=0)&& (key!=46)) { 
	   if (key<48 || key>57) { 
	     if (navigator.appName == "Microsoft Internet Explorer")
		    window.event.keyCode=0; else e.preventDefault();
	      } 
	    } 
	  }

$(document).on('change','#country',function(){
   var id = $(this).val();
    $.ajax({
						type: "POST",
						data: ({countryID: id}),
						url: siteurl+"ajax/selectState/"+id,
						beforeSend: function(){
								},
						success: function(result) {
							$('#state').html(result);
							
						}
        			});
                return false;
});
$('#loginfrm_user_rating').validate({
	 submitHandler: function(form) {
		    $('#btn_login').html('Processing....');
		     $.ajax({
						type: "POST",
						data: $('#loginfrm_user_rating').serialize(),
						url: siteurl+"ajax/login/",
						beforeSend: function(){
								},
						success: function(result) {
							 $('#btn_login').html('LOGIN <i class="fa fa-sign-in"></i>');
							 if(result=='success')
							 {
							   location.reload();
							 }
							 else
							 {
								$('#alerterror').show()
								$('#alertsuccess').hide()
								$('#alerterror').html(result);  
							 }
							
						}
        			});
                return false;
			 },	  
	   });


$('#reviewform').validate({
	 submitHandler: function(form) {
		     $('#btn_review').html('Processing....');
		     $.ajax({
						type: "POST",
						data: $('#reviewform').serialize(),
						url: siteurl+"ajax/rating/",
						beforeSend: function(){
								},
						success: function(result) {
							 $('#btn_review').html('Submit');
							 if(result=='success')
							 {
								$('#review_title').val('');
								$('#review_desc').val(''); 
							    $('#alertsuccess').show()
								$('#alerterror').hide()
								$('#alertsuccess').html('Thanks for your ratings!');   
							 }
							 else
							 {
								$('#alerterror').show()
								$('#alertsuccess').hide()
								$('#alerterror').html(result);   
							 }
							 $('#review_title').val('');
							 $('#review_desc').val('');
						}
        			});
                return false;
			 },	  
	   });

$(document).on('click','.addwishlist',function(){
    var product_id = $(this).attr('rel');
    var user_id = $(this).attr('data-id');
    $.ajax({
			type: "POST",
			url: siteurl+"ajax/wishlist/"+user_id+"/"+product_id,
			beforeSend: function(){
					},
			success: function(result) {
				 if(result=='ok')
				 {
				   alert('Boat added to wishlist!');
				   $(this).removeAttr('class');
				 }
				 if(result=='already')
				 {
					alert('Boat already added on wishlist !');
					return false;
				 }
				 if(result=='error')
				 {
					alert('Boat adding wishlist error !');
					return false; 
				 }
			}
         });
});