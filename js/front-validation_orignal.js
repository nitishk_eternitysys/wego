$(document).on('click','.bookingapprove',function(){
	   var attributAction = $(this).attr('href');
	   var modelID    = $(this).attr('rel');
	   $('#'+modelID).modal('show');
	   return false;
});
$(document).on('click','.btn_approve_view',function(){
	   var attributAction = $(this).attr('href');
	   var modelID    = $(this).attr('rel');
	   $('#'+modelID).modal('show');
	   return false;
});
$(document).on('click','.btn_cancel_view',function(){
	   var attributAction = $(this).attr('href');
	   var modelID    = $(this).attr('rel');
	   $('#'+modelID).modal('show');
	   return false;
});
$(document).on('click','.btn_cancel',function(){
	   var attributAction = $(this).attr('href');
	   var modelID    = $(this).attr('rel');
	   $('#'+modelID).modal('show');
	   return false;
});
$(document).on('click','.savebtnapprove',function(){
      var currentId= $(this).closest('form').attr('id');
	  var modelID = $('#modal-'+currentId);
	  var acturl=$(this).closest('form').attr('action');
	  var textValue = $(this).closest('form').find('textarea').val();
	   $.ajax({
				type: "POST",
				data: {textValue:textValue},
				url : acturl,
				beforeSend: function(){ 
				 modelID.closest('.item-entry').css('opacity','0.3');
				},
				success: function(result) {
				   modelID.closest('.item-entry').css('opacity','1');
				   var tabID=$('.tab-content').find('.active').attr('id');
				   $("#"+tabID).load(location.href + " #"+tabID+">*", "");
				   modelID.modal('hide');
				   //$('#accom_subcategory_id').html(result);
				 }
		  }); 
	   return false;

});
$(document).on('click','.savebtn',function(){
	  var currentId= $(this).closest('form').attr('id');
	  var modelID = $('#modal-'+currentId);
	  var acturl=$(this).closest('form').attr('action');
	  var textValue = $(this).closest('form').find('textarea').val();
	   $.ajax({
				type: "POST",
				data: {textValue:textValue},
				url : acturl,
				beforeSend: function(){ 
				 modelID.closest('.item-entry').css('opacity','0.3');
				},
				success: function(result) {
				   modelID.closest('.item-entry').css('opacity','1');
				   var tabID=$('.tab-content').find('.active').attr('id');
				   $("#"+tabID).load(location.href + " #"+tabID+">*", "");
				   modelID.modal('hide');
				   //$('#accom_subcategory_id').html(result);
				 }
		  }); 
	   return false;
});
$(document).on('click','.btnapprove',function(){
	var currentId= $(this);
	var tabID = $(this).attr('rel'); 
	var check_url=$(this).attr('href');
	var category_id = '0';
    $.ajax({
				type: "POST",
				data: {category_id:category_id},
				url : check_url,
				beforeSend: function(){ 
				  currentId.closest('.item-entry').css('opacity','0.3');
				},
				success: function(result) {
				   currentId.closest('.item-entry').css('opacity','1');
				   $("#"+tabID).load(location.href + " #"+tabID+">*", ""); 
				   //$('#accom_subcategory_id').html(result);
				 }
		  }); 
     return false ;
});
$(document).on('click','.addcheck',function(){
    	$('.alert').fadeOut(300);
	   var type = $(this).attr('rel');
	   var currentID=$(this).parent().parent().parent().attr('id');
	   var fieldsetID=$(this).closest('fieldset').attr('id'); 
	   var getlen=$('#'+currentID).find('.col-xs-10').length;
	   if(getlen< 5)
	   {
		  $(this).closest('.col-xs-10').after('<div class="col-xs-10"><div class="col-xs-4"></div><div class="col-xs-2"><input type="text" name="form_from_time_'+currentID+'[]" id="form_from_time_'+type+''+currentID+'_'+getlen+'" placeholder="From time" class="form_availalbe_from_time_'+type+'" /></div> <div class="col-xs-2"><input type="text" name="form_to_time_'+currentID+'[]" id="form_to_time_'+type+''+currentID+'_'+getlen+'" placeholder="To time" class="form_availalbe_to_time_'+type+'" /> </div><div class="col-xs-2"><input type="text" name="form_day_duration_'+currentID+'[]" id="form_duration_'+type+''+currentID+'_'+getlen+'" placeholder="Duration" /></div><div class="col-xs-2"  style="margin-top:-20px;"><a href="javascript:void(0);" class="removeavailability">Remove</a></div></div></div>');
	  jQuery('.form_availalbe_from_time_'+type+'').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function(ct){	
			   this.setOptions({
				 maxDate:jQuery('.form_availalbe_to_time_'+type+'').val()?jQuery('.form_availalbe_to_time_'+type+'').val():false
			   })
			  },
		 });
		jQuery('.form_availalbe_to_time_'+type+'').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_'+type+'').val()?jQuery('.form_available_from_time_'+type+'').val():false
			   })
			  },
		   });
	   }
	   else
	   {
		   alert('You have add availabilty time less than five');
	   }
  });
$(document).on('click','.removeavailability',function(){
	  $('.alert').fadeOut(300);
	  $(this).closest('.col-xs-10').remove();
   });

$(document).on('click','.editclass',function(){
	var hideDiv = $(this).closest('.active').attr('id');
	var accom_id=$(this).attr('rel');
    var funname= $(this).attr('data-action');
	var divName= $(this).attr('data-id');
	var checkLoder=$(this).closest('.active').attr('id');
      $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"editform/"+funname+"/",
				beforeSend: function(){
					  $('#'+checkLoder).find('.user-waitt').show();
					  $('#'+divName).css('opacity','0.1');
					  $('#'+divName).html('Processing.....');
					},
				success: function(result) {
					$('#'+checkLoder).find('.user-waitt').hide();
					$('#'+hideDiv).find('.commancls').hide();
					$('#'+divName).show();
					$('#'+divName).css('opacity','1');
					$('#'+divName).html(result)
				 }
		    }); 

});
$(document).on('click','.addcls',function(){
	$('.alert').fadeOut(300);
    var accom_id=$(this).attr('rel');
	var manageId=$(this).parent().parent().parent().prev().attr('id');
	var parentID=$(this).parent().parent().parent().parent().attr('id');
	var checkLoder=$(this).closest('.active').attr('id');
	if(accom_id==manageId)
	{
	   $('#'+parentID).find('.commancls').hide();
	   $('#'+accom_id).show();	
	}
	else
	{
	   var funname= $(this).attr('data-action');
	   var divName= $(this).attr('data-id');
       $.ajax({
				type: "POST",
				data: { accom_id : accom_id },
				url: siteurl+"editform/"+funname+"/",
				beforeSend: function(){
					  $('#'+checkLoder).find('.user-waitt').show();
					  $('#'+divName).css('opacity','0.1');
					  $('#'+divName).html('Processing.....');
					},
				success: function(result) {
					$('#'+checkLoder).find('.user-waitt').hide();
				    $('#'+parentID).find('.commancls').hide();
					$('#'+divName).show();
					$('#'+divName).css('opacity','1');
					$('#'+divName).html(result)
				 }
		    }); 	
	}
   
 });
$(document).on('click','.removeaccom',function(){
	$('.alert').fadeOut(300);
	if(confirm("Are you sure you want to delete this records?"))
	{
	   var $this=$(this); 	
	   var type = $(this).attr('rel')
	   var accom_id=$(this).attr('data-id');
       $.ajax({
				type: "POST",
				data: { common_id : accom_id,type : type},
				url: siteurl+"ajax/removeaccom/",
				beforeSend: function(){
					  $this.closest('.item-entry').css('opacity','0.1');
					},
				success: function(result) {
					if(result=='success')
					{
					   $this.closest('.item-entry').remove();  	
					}
				 }
		    }); 
	}
	else
	{
	   return false;
	}
});
$(document).on('change','#accom_category_id',function(){
	
	       var category_id = $('#accom_category_id').val();
	         $.ajax({
						type: "POST",
						data: {category_id:category_id},
						url: siteurl+"ajax/getSubcategory/",
						beforeSend: function(){
								},
						success: function(result) {
							$('#accom_subcategory_id').html(result);
							
						}
					});
});
$(document).on('change','#categoryID_tours',function(){
	
	var category_id = $('#categoryID_tours').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_tours').html(result);
			
		}
	});
});
$(document).on('change','#categoryID_things_to_do',function(){
	var category_id = $('#categoryID_things_to_do').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getMaincategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#categoryID_tours').html(result);
		}
	});
});
$(document).on('change','#categoryID_adventure',function(){
	var category_id = $('#categoryID_adventure').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_adventure').html(result);
			
		}
	});
});
$(document).on('change','#categoryID_recreation',function(){
	
	var category_id = $('#categoryID_recreation').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategoryID_recreation').html(result);
			
		}
	});
});
$(document).on('change','#transportation_category_id',function(){
	
	var category_id = $('#transportation_category_id').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#transportation_subcategory_id').html(result);
			
		}
	});
});
$(document).on('change','#event_category_id',function(){
	
	var category_id = $('#event_category_id').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#event_subcategory_id').html(result);
			
		}
	});
});
$(document).on('change','#coupon_category_id',function(){
	
	var category_id = $('#coupon_category_id').val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategory_id').html(result);
			
		}
	});
});


$(document).on('change','input[name="form_availalbe_for"]',function(){
	
	var inputname =$(this).attr('name');
	var inputnameId =$(this).attr('id');
	var inputrel =$(this).attr('rel');
	//alert(inputrel);
	$('input[name="'+inputname+'"]').removeAttr('checked');
	$('input[name="form_availalbe_for"]').attr('checked', false);
	$(this).attr('checked', true);
	var name=$("[name='"+inputname+"'][checked]").attr("value");
	
    if($(this).val()=='daily')
	{
		
		$('#daily_'+inputrel).show();
		$('#weekly_'+inputrel).hide();
	}
	else
	{
		$('#daily_'+inputrel).hide();
	   $('#weekly_'+inputrel).show();	
	}
 });

$(document).on('click','.amenities',function(){
	$('.alert').fadeOut(300);
	var inputname =$(this).find('input').attr('name');
	var inputnameId =$(this).find('input').attr('id');
	//alert(inputname);
	$('input[name="'+inputname+'"]').removeAttr('checked');
	$("#"+inputnameId).attr('checked', 'checked');
	var name=$("[name='"+inputname+"'][checked]").attr("value");
    if(name=='yes')
	{
		$('#'+inputname+'_yes').show();
	}
	else
	{
	   $('#'+inputname+'_yes').hide();	
	   $('#'+inputname+'_yes').find('input').val('');	
	}
 });
 $(document).on('click','.amenitiesform',function(){
	 $('.alert').fadeOut(300);
    var inputname =$(this).find('input').attr('name');
	var inputnameId =$(this).find('input').attr('id');
	var relId=$(this).find('input').attr('rel');
	//alert(inputname);
	$('input[name="'+inputname+'"]').removeAttr('checked');
	$("#"+inputnameId).attr('checked', 'checked');
	var name=$("[name='"+inputname+"'][checked]").attr("value");
    if(name=='yes')
	{
		$('#'+relId+'_yes').show();
	}
	else
	{
	   $('#'+relId+'_yes').hide();	
	   $('#'+relId+'_yes').find('input').val('');	
	}
 });
 $(document).on('click','.discount',function(){
	 $('.alert').fadeOut(300);
	    var inputname =$(this).find('input').attr('name');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
		if(name=='on')
		{
			$('#'+inputname+'_on').show();
		}
		else
		{
		   $('#'+inputname+'_on').hide();	
		}
 });
  $(document).on('click','.discountthree',function(){
	 $('.alert').fadeOut(300);
	    var inputname =$(this).find('input').attr('class');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
	    
		
			$('#'+inputname+'_on').toggle();
		
 });
$(document).on('click','.cancellation',function(){
	   $('.alert').fadeOut(300);
	    var inputname =$(this).find('input').attr('name');
		var inputnameId =$(this).find('input').attr('id');
		//alert(inputname);
		$('input[name="'+inputname+'"]').removeAttr('checked');
		$("#"+inputnameId).attr('checked', 'checked');
		var name=$("[name='"+inputname+"'][checked]").attr("value");
		if(name=='yes')
		{
			$('#'+inputname+'_yes').show();
		}
		else
		{
		   $('#'+inputname+'_yes').hide();	
		}
});




function OnlyNumericKeys(e) {
	 var key; 
	 if (navigator.appName == "Microsoft Internet Explorer") key = window.event.keyCode; else key = e.which; 
	 if((key!=13) && (key !=8) && (key!=0)&& (key!=46)) { 
	   if (key<48 || key>57) { 
	     if (navigator.appName == "Microsoft Internet Explorer")
		    window.event.keyCode=0; else e.preventDefault();
	      } 
	    } 
	  }

$(document).on('change','#country',function(){
	$('.alert').fadeOut(300);
   var id = $(this).val();
    $.ajax({
						type: "POST",
						data: ({countryID: id}),
						url: siteurl+"ajax/selectState/"+id,
						beforeSend: function(){
								},
						success: function(result) {
							$('#state').html(result);
							
						}
        			});
                return false;
});
$('#loginfrm_user_rating').validate({
	 submitHandler: function(form) {
		    $('#btn_login').html('Processing....');
		     $.ajax({
						type: "POST",
						data: $('#loginfrm_user_rating').serialize(),
						url: siteurl+"ajax/login/",
						beforeSend: function(){
								},
						success: function(result) {
							 $('#btn_login').html('LOGIN <i class="fa fa-sign-in"></i>');
							 if(result=='success')
							 {
							   location.reload();
							 }
							 else
							 {
								$('#alerterror').show()
								$('#alertsuccess').hide()
								$('#alerterror').html(result);  
							 }
							
						}
        			});
                return false;
			 },	  
	   });


$('#reviewform').validate({

	 submitHandler: function(form) {
		     $('#btn_review').html('Processing....');
		     $.ajax({
						type: "POST",
						data: $('#reviewform').serialize(),
						url: siteurl+"ajax/rating/",
						beforeSend: function(){
								},
						success: function(result) {
							 $('#btn_review').html('Submit');
							 if(result=='success')
							 {
								$('#review_title').val('');
								$('#review_desc').val(''); 
							    $('#alertsuccess').show()
								$('#alerterror').hide()
								$('#alertsuccess').html('Thanks for your ratings!');   
							 }
							 else
							 {
								$('#alerterror').show()
								$('#alertsuccess').hide()
								$('#alerterror').html(result);   
							 }
							 $('#review_title').val('');
							 $('#review_desc').val('');
						}
        			});
                return false;
			 },	  
	   });

$(document).on('click','.addwishlist',function(){

    var product_id = $(this).attr('rel');
    var user_id = $(this).attr('data-id');
    $.ajax({
			type: "POST",
			url: siteurl+"ajax/wishlist/"+user_id+"/"+product_id,
			beforeSend: function(){
					},
			success: function(result) {
				 if(result=='ok')
				 {
				   alert('Boat added to wishlist!');
				   $(this).removeAttr('class');
				 }
				 if(result=='already')
				 {
					alert('Boat already added on wishlist !');
					return false;
				 }
				 if(result=='error')
				 {
					alert('Boat adding wishlist error !');
					return false; 
				 }
			}
         });
});

$(document).on('click','#searchTab li a',function(){
	var category_id = $(this).attr('rel');
	
	if(category_id=='')
	{
		category_id='1';
	}
	$('.selectpicker').selectpicker('val', '');
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getCategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#category'+category_id).html(result);
			$('.selectpicker').selectpicker('refresh');
			
		}
	});
});


$(document).on('change','input[name="mainCat"]',function(){

	var category_id = $(this).attr('rel');
	
	$('.selectpicker').selectpicker('val', '');
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getCategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#category2').html(result);
			$('.selectpicker').selectpicker('refresh');
			
		}
	});
});
$(document).on('change','select[name="user_country"]',function(){
	var $this=$(this);
	var country_name = $(this).val();
	$.ajax({
		type: "POST",
		data: {country_name:country_name},
		url: siteurl+"ajax/getCity/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#user_city').html(result);
		}
	});
});

$(document).on('change','select[name="category"]',function(){
	var $this=$(this);
	var rel=$(this).attr('rel');
	var category_id = $(this).val();
	$.ajax({
		type: "POST",
		data: {category_id:category_id},
		url: siteurl+"ajax/getSubcategory/",
		beforeSend: function(){
				},
		success: function(result) {
			$('#subcategory'+rel).html(result);
			$('.selectpicker').selectpicker('refresh');
			
		}
	});
});
$(document).on('click','.view-switcher a',function(){
	var rel=$(this).attr('rel');
	$('#view').val(rel);
	search();
});
$(document).on('change','.sort select.selectpicker',function(){
	$('.selectpicker').selectpicker('refresh');
	search();
});

function search(){
    
    var role = $('#role').val();
	var url = siteurl + "search/"+role+"?"+$('#queryString').val();
	if(role=='tours' || role=='adventure' || role=='recreations')
	{
		var url = siteurl + "search/tours?"+$('#queryString').val();
	}
	var category = $('#category').val();
	var subcategory = $('#subcategory').val();
	var minPrice = $('#minPrice').val();
	var maxPrice = $('#maxPrice').val();
	var viewHtml=$('#view').val();
	// For sorting
	var order_price = $('#order_price').val();
	var order_star = $('#order_star').val();
	var order_user = $('#order_user').val();
	var order_name = $('#order_name').val();
	

    $('#searchResult').html('<div style="text-align:center;"><img src="'+siteurl+'/front/images/loader.gif" width="200" height="200"></div>');
   
    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: {
			view:viewHtml,
			minPrice:minPrice,
			maxPrice:maxPrice,
			category:category,
			subcategory:subcategory,
			order_price:order_price,
			order_star:order_star,
			order_user:order_user,
			order_name:order_name,
            role: role
        },
        success: function(data) {
          $('#searchResult').html('');
          $('#searchResult').html(data.html);
		  $('#searchHTML').html('');
          $('#searchHTML').html(data.searchHtml);
		  $('#count').html(data.count);
		  $( "input[name='adult']" ).spinner({
			min: 1
			});	
			$( "input[name='child']" ).spinner( {
				min: 1
			});
			
        }
    });
}
function updateTextAreaCat() {
	var allVals = [];
	$('#categoryview :checked').each(function () {
		allVals.push($(this).val());
	});
	$('#category').val(allVals);
}
function updateTextAreaSubCat() {
	var allValsc = [];
	$('#subcategoryview :checked').each(function () {
		allValsc.push($(this).val());
	});
	$('#subcategory').val(allValsc);
	
}
$(document).on('click','#categoryview input',function(){
	$('#categoryview input').click(updateTextAreaCat);
	updateTextAreaCat();
	search();
});
$(document).on('click','#subcategoryview input',function(){
	$('#subcategoryview input').click(updateTextAreaSubCat);
	updateTextAreaSubCat();
	search();
});
$(document).ready(function(e) {
    $('#searchTab li.active a').trigger('click');
});