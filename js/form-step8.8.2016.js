$(function() {
 $('#accommodationsform').on('submit',function(){
	$('.error').remove();
	if($("fieldset").index($('.submit').parent())=='7')
	{
		 if($('#accom_user_name').val()=='')
		 {
			$('input[id="accom_user_name"]').after('<label class="error" for="review_desc">This field is required.</label>');
			return false; 
		 }
		 else if($('#accom_position').val()=='')
		 {
			$('input[id="accom_position"]').after('<label class="error" for="review_desc">This field is required.</label>');
			return false; 
		 }
		 else if($('#accom_email').val()=='')
		 {
			$('input[id="accom_email"]').after('<label class="error" for="review_desc">This field is required.</label>');
			return false; 
		 }
		 else if($('#accom_mobile').val()=='')
		 {
			$('input[id="accom_mobile"]').after('<label class="error" for="review_desc">This field is required.</label>');
			return false; 
		 }
	}
  });	
	
 var current_cls=0;
 $('.progressbar li').each(function(e){
		 if($(this).hasClass('current'))
		 {
		   $('fieldset').hide();
		   $('fieldset').eq(e).show();	 
		   current_cls=e;
		   for(i=0;e>i;i++)
		   {
			 $("#progressbar li").eq(i+1).addClass("active");   
		   }
		 }
	 });
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
$(".next").click(function(){
	     var currentId=$(this).parent().attr('class');
         $('.error').remove();
		 if(currentId=='progressbar')
		 {
			 if($("fieldset").index($(this).parent())=='0')
			 {
				 if($('#accom_title').val()=='')
				 {
					  $('input[id="accom_title"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  $('#accom_title').focus();
					  return false;
				 }
				 else if($('#accom_category_id').val()=='')
				 {
					  $('select[id="accom_category_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_subcategory_id').val()=='')
				 {
					  $('select[id="accom_subcategory_id"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_location').val()=='')
				 {
					  $('textarea[id="accom_location"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_description').val()=='')
				 {
					  $('textarea[id="accom_description"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($("fieldset").index($(this).parent())=='1')
			 {
				 if($('#accom_bed_rooms').val()=='')
				 {
					  $('input[id="accom_bed_rooms"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_beds').val()=='')
				 {
					  $('input[id="accom_beds"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_bathrooms').val()=='')
				 {
					  $('input[id="accom_bathrooms"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_swimming_pool').val()=='')
				 {
					  $('input[id="accom_swimming_pool"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_play_yards').val()=='')
				 {
					  $('input[id="accom_play_yards"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_balcony').val()=='')
				 {
					  $('input[id="accom_balcony"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_garden').val()=='')
				 {
					  $('input[id="accom_garden"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
				 else if($('#accom_roof').val()=='')
				 {
					  $('input[id="accom_roof"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			 }
			 if($("fieldset").index($(this).parent())=='2')
			 {	
			   
			   if($('#accom_tv_yes').is(':visible'))
			   {
				 if($('#accom_tv_number').val()=='')
				 {
					  $('input[id="accom_tv_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_playstation_yes').is(':visible'))
			   {
				 if($('#accom_playstation_number').val()=='')
				 {
					  $('input[id="accom_playstation_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_wifi_yes').is(':visible'))
			   {
				 if($('#accom_wifi_number').val()=='')
				 {
					  $('input[id="accom_wifi_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_dvd_yes').is(':visible'))
			   {
				 if($('#accom_dvd_number').val()=='')
				 {
					  $('input[id="accom_dvd_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_gym_yes').is(':visible'))
			   {
				 if($('#accom_gym_number').val()=='')
				 {
					  $('input[id="accom_gym_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_kitchen_yes').is(':visible'))
			   {
				 if($('#accom_kitchen_number').val()=='')
				 {
					  $('input[id="accom_kitchen_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false;
				 }
			   }
			   if($('#accom_washing_machine_yes').is(':visible'))
			   {
				  if($('#accom_washing_machine_number').val()=='')
				  {
					$('input[id="accom_washing_machine_number"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				  }
			   }
			 }
			 if($("fieldset").index($(this).parent())=='3')
			 {
				 
				 if($('#accom_check_in').val()=='')
				 {
					$('input[id="accom_check_in"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
				 else if($('#accom_check_out').val()=='')
				 {
					$('input[id="accom_check_out"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
				 else if($('#accom_guest_max').val()=='')
				 {
					$('input[id="accom_guest_max"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			 }
			 if($("fieldset").index($(this).parent())=='4')
			 {
				 
				 if($('#accom_gustes_allowed').val()=='')
				 {
					$('textarea[id="accom_gustes_allowed"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
			}
			 if($("fieldset").index($(this).parent())=='5')
			 {
				 if($('#accom_price').val()=='')
				 {
					$('input[id="accom_price"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false;
				 }
				 if($('#accom_discount_on').is(':visible'))
				 {
					 if($('#accom_precentage_discount').val()=='')
					 {
						$('input[id="accom_precentage_discount"]').after('<label class="error" for="review_desc">This field is required.</label>');
						return false;
					 } 
				 }
				 if($('#accom_bird_discount').val()=='')
				 {
					$('input[id="accom_bird_discount"]').after('<label class="error" for="review_desc">This field is required.</label>');
					return false; 
				 }
			}
			 if($("fieldset").index($(this).parent())=='6')
			 {
				if($('#accom_cancel_policy_yes').is(':visible'))
				{
					if($('#accom_cancel_policy_price').val()=='')
					{
					  $('input[id="accom_cancel_policy_price"]').after('<label class="error" for="review_desc">This field is required.</label>');
					  return false; 	
					}
				}
			}
		 }
		 if(currentId=='progressbar')
		 {
			 
		 }
	  	 if(animating) return false;
		 animating = true;
		 current_fs = $(this).parent();
		 next_fs = $(this).parent().next();
		 //activate next step on progressbar using the index of next_fs
		 $("#"+currentId+" li").removeClass("current");
		 $("#"+currentId+" li").eq($("fieldset").index(next_fs)).addClass("active");
		 $("#"+currentId+" li").eq($("fieldset").index(next_fs)).addClass("current");
		 //show the next fieldset
		 next_fs.show(); 
		 //hide the current fieldset with style
		 current_fs.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50)+"%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({'transform': 'scale('+scale+')'});
				next_fs.css({'left': left, 'opacity': opacity});
			}, 
			duration: 800, 
			complete: function(){
				current_fs.hide();
				animating = false;
			}, 
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	
});
$(".previous").click(function(){
	var currentId=$(this).parent().attr('class');
	if(animating) return false;
	animating = true;
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	//de-activate current step on progressbar
	$("#"+currentId+" li").removeClass("current");
	$("#"+currentId+" li").eq($("fieldset").index(current_fs)).removeClass("active");
	$("#"+currentId+" li").eq($("fieldset").index(previous_fs)).addClass("current");
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});
});