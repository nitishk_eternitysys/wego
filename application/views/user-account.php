<!-- START: USER PROFILE -->
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3><?php echo $this->lang->line('Welcome');?><?php echo $fetch_array[0]['user_name']; ?></h3>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#profile-overview" class="text-center"><i class="fa fa-bolt"></i> <span><?php echo $this->lang->line('Overview');?></span></a></li>
						
						<li><a data-toggle="tab" href="#profile" class="text-center"><i class="fa fa-user"></i> <span><?php echo $this->lang->line('Profile');?></span></a></li>
						<li><a data-toggle="tab" href="#booking" class="text-center"><i class="fa fa-history"></i> <span><?php echo $this->lang->line('Bookings');?></span></a></li>	
                        <li><a data-toggle="tab" href="#storyboard" class="text-center"><i class="fa fa-edit"></i> <span><?php echo $this->lang->line('story_board');?></span></a></li>
						<!--<li><a data-toggle="tab" href="#cards" class="text-center"><i class="fa fa-credit-card"></i> <span>My Cards</span></a></li>
						<li><a data-toggle="tab" href="#complaint" class="text-center"><i class="fa fa-edit"></i> <span>Complaints</span></a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
					<div id="profile-overview" class="tab-pane fade in active">
						<div class="col-md-6">
							<div class="brief-info">
								<div class="col-md-2 col-sm-2 clear-padding">
									<img src="assets/images/user1.jpg" alt="cruise">
								</div>
								<div class="col-md-10 col-sm-10">
									<h3><?php echo ucfirst($fetch_array[0]['user_name']); ?></h3>
									<h5><i class="fa fa-envelope-o"></i><?php echo $fetch_array[0]['user_email']; ?></h5>
									<h5><i class="fa fa-phone"></i>+<?php echo $fetch_array[0]['user_mobile']; ?></h5>
									<h5><i class="fa fa-map-marker"></i><?php echo $fetch_array[0]['user_address']; ?></h5>
								</div>
								<div class="clearfix"></div>
								<div class="brief-info-footer">
									<a data-toggle="tab" href="#profile" class="text-center" id="profileDiv"><i class="fa fa-edit"></i><?php echo $this->lang->line('Edit_Profile');?></a>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="most-recent-booking">
								<h4><?php echo $this->lang->line('Recent_Booking');?></h4>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i><?php echo $this->lang->line('New_York');?><i class="fa fa-long-arrow-right"></i><?php echo $this->lang->line('New_Delhi');?></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i><?php echo $this->lang->line('Confirmed');?></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#"><?php echo $this->lang->line('Detail');?></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i> <?php echo $this->lang->line('Grand_Lilly');?><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i><?php echo $this->lang->line('Confirmed');?></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#"><?php echo $this->lang->line('Detail');?></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i><?php echo $this->lang->line('Wonderful_Europe');?></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i><?php echo $this->lang->line('failed');?></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#"><?php echo $this->lang->line('Detail');?></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i><?php echo $this->lang->line('New_York');?><i class="fa fa-long-arrow-right"></i><?php echo $this->lang->line('New_Delhi');?></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i><?php echo $this->lang->line('Confirmed');?></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#"><?php echo $this->lang->line('Detail');?></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i> <?php echo $this->lang->line('Grand_Lilly');?><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i><?php echo $this->lang->line('Confirmed');?></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#"><?php echo $this->lang->line('Detail');?></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i><?php echo $this->lang->line('Wonderful_Europe');?></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i><?php echo $this->lang->line('failed');?></p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#"><?php echo $this->lang->line('Detail');?></a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-profile-offer">
								<h4><?php echo $this->lang->line('Offers_For_You');?></h4>
								<div class="offer-body">
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p><?php echo $this->lang->line('20_OFF');?></p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p> <?php echo $this->lang->line('OFF_on_flights');?><a href="#"><?php echo $this->lang->line('Book_Now');?></a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p><?php echo $this->lang->line('30_OFF');?></p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p> <?php echo $this->lang->line('30_OFF_on');?><a href="#"><?php echo $this->lang->line('Book_Now');?></a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p><?php echo $this->lang->line('10_OFF');?></p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p><?php echo $this->lang->line('10_OFF_on');?> <a href="#"><?php echo $this->lang->line('Book_Now');?></a></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="user-notification">
									<h4><?php echo $this->lang->line('Notification');?></h4>
									<div class="notification-body">
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> <?php echo $this->lang->line('Domestic_Flights');?><span class="pull-right"><?php echo $this->lang->line('1m_ago');?></span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i>  <?php echo $this->lang->line('Cashback_on');?><span class="pull-right"><?php echo $this->lang->line('1h_ago');?></span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bolt"></i>  <?php echo $this->lang->line('off_on_all');?><span class="pull-right"><?php echo $this->lang->line('08h_ago');?></span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-sun-o"></i> <?php echo $this->lang->line('New_Year');?> <span class="pull-right">1d ago<?php echo $this->lang->line('Welcome');?></span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> <?php echo $this->lang->line('Domestic_Flights');?> <span class="pull-right"><?php echo $this->lang->line('1m_ago');?></span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> <?php echo $this->lang->line('20_Cashback');?> <span class="pull-right"><?php echo $this->lang->line('1h_ago');?></span></p>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div id="booking" class="tab-pane fade in">
					   <div class="col-md-12">
                           <div class="text-center" role="tabpanel">
                                <!-- BEGIN: CATEGORY TAB -->
                                <ul id="hotDeal" role="tablist" class="nav nav-tabs">
                                    <li class="active text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab1" href="#tab1">
                                            <i class="fa fa-bed"></i> 
                                            <span><?php echo $this->lang->line('Accommodations');?></span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" rThing to doole="tab" aria-controls="tab2" href="#tab2">
                                            <i class="fa fa-plane"></i> 
                                            <span><?php echo $this->lang->line('Thing_to_do');?></span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab3" href="#tab3">
                                            <i class="fa fa-suitcase"></i> 
                                            <span><?php echo $this->lang->line('Packages');?></span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab4" href="#tab4">
                                            <i class="fa fa-taxi"></i> 
                                            <span><?php echo $this->lang->line('Transportation');?></span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab5" href="#tab5">
                                            <i class="fa fa-taxi"></i> 
                                            <span><?php echo $this->lang->line('Events');?></span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab6" href="#tab6">
                                            <i class="fa fa-taxi"></i> 
                                            <span><?php echo $this->lang->line('Coupons');?></span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- END: CATEGORY TAB -->
                                <div class="clearfix"></div>
                                <!-- BEGIN: TAB PANELS -->
                                <div class="tab-content">
                                    <!-- BEGIN: FLIGHT SEARCH -->
                                     <div id="tab1" class="tab-pane active fade in" >
                                     <?php
                                    $this->db->join('tbl_accommodations_master','tbl_accommodations_master.accom_id=tbl_transaction_master.booking_form_id');
                                    $this->db->select('tbl_transaction_master.*,tbl_accommodations_master.accom_title AS title,tbl_accommodations_master.accom_image AS image,tbl_accommodations_master.accom_cancel_policy');
								     $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'accom'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span> <?php echo $this->lang->line('Order_ID');?><?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php  } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong><?php echo $this->lang->line('Booking_Type');?></strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong><?php echo $this->lang->line('Payment_by');?></strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <?php 
													if($order['accom_cancel_policy']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Cancel');?></a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel"><?php echo $this->lang->line('Cancel_Reason');?></h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('Close');?></button>
                                                                    <button class="btn btn-primary savebtn"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 }
													 else if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Cancel_View');?></a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel"><?php echo $this->lang->line('Cancel_Reason');?></h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Approve_View');?></a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel"><?php echo $this->lang->line('Note');?></h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													} ?>
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong><?php echo $this->lang->line('Order_Date');?></strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong><?php echo $this->lang->line('Order_Total');?></strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download><?php echo $this->lang->line('Attachment');?></a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>"><?php echo $this->lang->line('View_details');?></a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning"><?php echo $this->lang->line('No_Booking_Available');?></div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                    </div>
                                     <div id="tab2" class="tab-pane fade" >
                                     <?php
									 $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image,tbl_three_forms.form_refundable');
									 $this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_transaction_master.booking_form_id');
									 $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type !='=>'packages'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span><?php echo $this->lang->line('Order_ID');?> <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong><?php echo $this->lang->line('Booking_Type');?></strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong><?php echo $this->lang->line('Payment_by');?></strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                     <?php 
													if($order['form_refundable']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Cancel');?></a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel"><?php echo $this->lang->line('Cancel_Reason');?></h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('Close');?></button>
                                                                    <button class="btn btn-primary savebtn"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 } }
													 if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Cancel_View');?></a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel"><?php echo $this->lang->line('Cancel_Reason');?></h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Approve_View');?></a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel"><?php echo $this->lang->line('Note');?></h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													 ?>
                                                  </div>
                                                <div class="item-footer">
                                                    <p><strong><?php echo $this->lang->line('Order_Date');?></strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong><?php echo $this->lang->line('Order_Total');?></strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download><?php echo $this->lang->line('Attachment');?></a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>"><?php echo $this->lang->line('View_details');?></a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning"><?php echo $this->lang->line('No_Booking_Available');?></div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab3" class="tab-pane fade" >
                                     <?php
									  $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image,tbl_three_forms.form_refundable');
									  $this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_transaction_master.booking_form_id');
									  $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'packages'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span><?php echo $this->lang->line('Order_ID');?> <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong><?php echo $this->lang->line('Booking_Type');?></strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong><?php echo $this->lang->line('Payment_by');?></strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <?php 
													if($order['form_refundable']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Cancel');?></a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel"><?php echo $this->lang->line('Cancel_Reason');?></h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo $this->lang->line('Close');?></button>
                                                                    <button class="btn btn-primary savebtn"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 } }
													 if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Cancel_View');?></a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel"><?php echo $this->lang->line('Cancel_Reason');?></h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" ><?php echo $this->lang->line('Approve_View');?></a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel"><?php echo $this->lang->line('Note');?></h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													 ?>
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong><?php echo $this->lang->line('Order_Date');?></strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong><?php echo $this->lang->line('Order_Total');?></strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download><?php echo $this->lang->line('Attachment');?></a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>"><?php echo $this->lang->line('View_details');?></a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else{ ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning"><?php echo $this->lang->line('No_Booking_Available');?></div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab4" class="tab-pane fade" >
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning"><?php echo $this->lang->line('No_Booking_Available');?></div>
                                                </div>
                                            </div>
                                         </div>
                                      </div>
                                     <div id="tab5" class="tab-pane fade" >
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning"><?php echo $this->lang->line('No_Booking_Available');?></div>
                                                </div>
                                            </div>
                                         </div>
                                      </div>
                                     <div id="tab6" class="tab-pane fade" >
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning"><?php echo $this->lang->line('No_Booking_Available');?></div>
                                                </div>
                                            </div>
                                         </div>
                                      </div>  
                                </div>
                           </div>
			           </div>
					</div>
					<div id="profile" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4><?php echo $this->lang->line('Personal_Information');?></h4>
								<div class="user-info-body">
									 <form method="post" id="userProfile" enctype="multipart/form-data">
										<div class="col-md-12">
											<label><?php echo $this->lang->line('User_Name');?></label>
											<input name="user_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true"  value="<?php echo $fetch_array[0]['user_name']; ?>">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Email_ID');?></label>
											 <input name="user_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" value="<?php echo $fetch_array[0]['user_email']; ?>" data-rule-email="true" readonly>
										</div>
										<div class="col-md-12">
									   <label><?php echo $this->lang->line('Birth_day');?></label>
									   <input name="user_birth_day" type="text" class="form-control" placeholder="Birth day" data-rule-required="true" value="<?php echo $fetch_array[0]['user_birth_day']; ?>"  >	</div>
									   <div class="col-md-12">
										<label><?php echo $this->lang->line('Gender');?></label>
									      <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="male" <?php if($fetch_array[0]['user_gender']=='male'){echo 'checked="checked"'; } ?> > <?php echo $this->lang->line('male');?>
                                          <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="female" <?php if($fetch_array[0]['user_gender']=='female'){echo 'checked="checked"'; } ?> ><?php echo $this->lang->line('Female');?>
										</div>
										<div class="col-md-12">
                                         <label><?php echo $this->lang->line('Mobile');?></label>
                                         <input name="user_mobile" type="text" class="form-control" placeholder="mobile" data-rule-required="true" value="<?php echo $fetch_array[0]['user_mobile']; ?>" >
										</div>	
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Address');?></label>
											<input name="user_address" type="text" class="form-control" placeholder="Address" data-rule-required="true" value="<?php echo $fetch_array[0]['user_address']; ?>" >
										</div>
                                        <div class="col-md-12">
											<label><?php echo $this->lang->line('Country');?></label>
										    <input name="user_country" type="text" class="form-control" placeholder="Country" data-rule-required="true" value="<?php echo $fetch_array[0]['user_country']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('City');?></label>
										  <input name="user_city" type="text" class="form-control" placeholder="City" data-rule-required="true" value="<?php echo $fetch_array[0]['user_city']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Id_Proof');?></label>
										  <input name="user_id_info" type="file"  placeholder="City"   >
                                          <a href="<?php echo base_url().'uploads/profile/'.$fetch_array[0]['user_id_info'] ?>" download><?php echo $this->lang->line('Download');?></a>
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('payment_method');?></label>
										  <input name="user_payment_method" type="text" class="form-control" placeholder="payment method " data-rule-required="true" value="<?php echo $fetch_array[0]['user_payment_method']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Preferred_currency');?></label>
										  <input name="user_preferred_currency" type="text" class="form-control" placeholder="Preferred currency" data-rule-required="true" value="<?php echo $fetch_array[0]['user_preferred_currency']; ?>"  >
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Language');?></label>
										   <input name="user_preferred_language" type="text" class="form-control" placeholder="Language" data-rule-required="true" value="<?php echo $fetch_array[0]['user_preferred_language']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Website');?></label>
										  <input name="user_website_here" type="text" class="form-control" placeholder="Website" data-rule-required="true" value="<?php echo $fetch_array[0]['user_website_here']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Interests');?></label>
										  <textarea name="user_interests" id="user_interests" class="form-control" data-rule-required="true"><?php echo $fetch_array[0]['user_interests']; ?></textarea>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <button type="submit"  name="btn_profile" id="btn_profile"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <a href="<?php echo base_url('user/profile'); ?>"><?php echo $this->lang->line('CANCEL');?></a>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4><?php echo $this->lang->line('Change_Password');?></h4>
								<div class="change-password-body">
									<form >
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Old_Password');?></label>
											<input type="password" placeholder="Old Password" class="form-control" name="old-password">
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('New_Password');?></label>
											<input type="password" placeholder="New Password" class="form-control" name="new-password">
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Confirm_Password');?></label>
											<input type="password" placeholder="Confirm Password" class="form-control" name="confirm-password">
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#flight-preference" aria-expanded="false" aria-controls="flight-preference">
										<i class="fa fa-plane"></i> <?php echo $this->lang->line('Flight_Preference');?><span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="flight-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Price_Range');?></label>
											<select class="form-control" name="flight-price-range">
												<option><?php echo $this->lang->line('Upto_199');?></option>
												<option><?php echo $this->lang->line('Upto_250');?></option>
												<option><?php echo $this->lang->line('Upto_499');?></option>
												<option><?php echo $this->lang->line('Upto_599');?></option>
												<option><?php echo $this->lang->line('Upto_1000');?></option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Food_Preference');?></label>
											<select class="form-control" name="flight-food">
												<option><?php echo $this->lang->line('Indian');?></option>
												<option><?php echo $this->lang->line('Chineese');?></option>
												<option><?php echo $this->lang->line('Sea_Food');?></option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Airline');?></label>
											<select class="form-control" name="flight-airline">
												<option><?php echo $this->lang->line('Indigo');?></option>
												<option><?php echo $this->lang->line('Vistara');?></option>
												<option><?php echo $this->lang->line('Spicejet');?></option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#hotel-preference" aria-expanded="false" aria-controls="hotel-preference">
										<i class="fa fa-bed"></i>  <?php echo $this->lang->line('Hotel_Preference');?><span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="hotel-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Price_Range');?></label>
											<select class="form-control" name="hotel-price-range">
												<option><?php echo $this->lang->line('Upto_199');?></option>
												<option><?php echo $this->lang->line('Upto_250');?></option>
												<option><?php echo $this->lang->line('Upto_499');?></option>
												<option><?php echo $this->lang->line('Upto_599');?></option>
												<option><?php echo $this->lang->line('Upto_1000');?></option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Food_Preference');?></label>
											<select class="form-control" name="hotel-food">
												<option><?php echo $this->lang->line('Indian');?></option>
												<option><?php echo $this->lang->line('Chineese');?></option>
												<option><?php echo $this->lang->line('Sea_Food');?></option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Facilities');?></label>
											<select class="form-control" name="hotel-facilities">
												<option><?php echo $this->lang->line('WiFi');?></option>
												<option><?php echo $this->lang->line('Bar');?></option>
												<option><?php echo $this->lang->line('Restaurant');?></option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label><?php echo $this->lang->line('Rating');?></label>
											<select class="form-control" name="hotel-facilities">
												<option>5</option>
												<option>4</option>
												<option>3</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
                    
					<div id="storyboard" class="tab-pane fade in">
						<div class="col-md-12">
                            <p class="item-content"><a href="<?php echo base_url(); ?>user/addStory"><?php echo $this->lang->line('Add');?></a></p>    
                            <?php if(count($fetch_story)>0){ 
								foreach($fetch_story as $story){
							$fetch_story_img=$this->master_model->getRecords('tbl_story_images',array('storyID'=>$story['storyID']));
							?>                    
							<div class="item-entry">
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="<?php echo base_url(); ?>uploads/story/<?php echo $fetch_story_img[0]['story_images']; ?>" alt="<?php echo $story['story_title']; ?>">
										</div>
										<div class="col-md-7 col-sm-7">
											<h4><?php echo $story['story_title']; ?></h4>
											<p><?php echo substr($story['story_description'],0,200); ?></p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="<?php echo base_url(); ?>user/storyremove/<?php echo base64_encode($story['storyID']); ?>"><?php echo $this->lang->line('Remove');?></a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong><?php echo $this->lang->line('Category');?></strong> <?php echo $story['category_name_'.$this->session->userdata('lang')]; ?> <a href="<?php echo base_url(); ?>user/editStory/<?php echo base64_encode($story['storyID']); ?>"><?php echo $this->lang->line('Edit');?></a></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<?php } } ?>
							
						</div>
					</div>
					<div id="cards" class="tab-pane fade in">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="card-entry">
									<div class="pull-right">
										<p><a href="#"><i class="fa fa-pencil"></i></a><a href="#"><i class="fa fa-times"></i></a></p>
									</div>
									<h3>XXXX XXXX XXXX 1234</h3>
									<p><?php echo $this->lang->line('Valid_Thru');?></p>
									<div class="clearfix"></div>
									<div class="card-type">
										<p><?php echo $this->lang->line('Name_On_Card');?></p>
										<div class="pull-left">
											<h3><?php echo $this->lang->line('Lenore');?></h3>
										</div>
										<div class="pull-right">
											<img src="assets/images/card/mastercard.png" alt="cruise">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-entry">
									<div class="pull-right">
										<p><a href="#"><i class="fa fa-pencil"></i></a><a href="#"><i class="fa fa-times"></i></a></p>
									</div>
									<h3>XXXX XXXX XXXX 2345</h3>
									<p><?php echo $this->lang->line('Valid_Thru');?></p>
									<div class="clearfix"></div>
									<div class="card-type">
										<p><?php echo $this->lang->line('Name_On_Card');?></p>
										<div class="pull-left">
											<h3><?php echo $this->lang->line('Lenore');?></h3>
										</div>
										<div class="pull-right">
											<img src="assets/images/card/visa.png" alt="cruise">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-6">
								<div class="card-entry primary-card">
									<div class="pull-left">
										<span><?php echo $this->lang->line('Primary');?></span>
									</div>
									<div class="pull-right">
										<p><a href="#"><i class="fa fa-pencil"></i></a><a href="#"><i class="fa fa-times"></i></a></p>
									</div>
									<div class="clearfix"></div>
									<h3>XXXX XXXX XXXX 1234</h3>
									<p><?php echo $this->lang->line('Valid_Thru');?></p>
									<div class="clearfix"></div>
									<div class="card-type">
										<p><?php echo $this->lang->line('Name_On_Card');?></p>
										<div class="pull-left">
											<h3><?php echo $this->lang->line('Lenore');?></h3>
										</div>
										<div class="pull-right">
											<img src="assets/images/card/mastercard.png" alt="cruise">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="user-add-card">
									<form >
										<input class="form-control" name="card-number" type="text" required placeholder="Card Number">
										<input class="form-control" name="cardholder-name" type="text" required placeholder="Cardholder Name">
										<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
											<input class="form-control" name="valid-month" type="text" required placeholder="Expiry Month">
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											<input class="form-control" name="valid-year" type="text" required placeholder="Expiry Year">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4 clear-padding">
											<input class="form-control" name="cvv" type="password" required placeholder="CVV">
										</div>
										<div class="clearfix"></div>
										<div>
											 <button type="submit"><?php echo $this->lang->line('ADD_CARD');?></button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div id="complaint" class="tab-pane fade in">
						<div class="col-md-12">
							<div class="recent-complaint">
								<h3><?php echo $this->lang->line('Service_Requests');?></h3>
								<div class="complaint-tabs">
									<ul class="nav nav-tabs">
										<li class="active"><a data-toggle="tab" href="#active-complaint" class="text-center"><i class="fa fa-bolt"></i> <?php echo $this->lang->line('Active');?></a></li>
										<li><a data-toggle="tab" href="#resolved-complaint" class="text-center"><i class="fa fa-history"></i> <?php echo $this->lang->line('Resolved');?></a></li>	
									</ul>
								</div>
								<div class="tab-content">
									<div id="active-complaint" class="tab-pane fade in active">
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_123456');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_113443');?></span><?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_113443');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_123456');?></span><?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_113443');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_113443');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
									</div>
									<div id="resolved-complaint" class="tab-pane fade in">
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_123456');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_113443');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_113443');?></span> <?php echo $this->lang->line('My_last_booking');?></a></p>
										<p><a href="#"><span><?php echo $this->lang->line('Ticket_123456');?></span><?php echo $this->lang->line('My_last_booking');?></a></p>
									</div>
								</div>
								<h3><?php echo $this->lang->line('New_Requests');?></h3>
								<div class="submit-complaint">
									<form >
										<div class="col-md-6 col-sm-6 col-xs-6">
											<label><?php echo $this->lang->line('Category');?></label>
											<select class="form-control" name="category">
												<option><?php echo $this->lang->line('Flight');?></option>
												<option><?php echo $this->lang->line('Hotell');?></option>
												<option><?php echo $this->lang->line('Cruise');?></option>
												<option><?php echo $this->lang->line('Holiday');?></option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											<label><?php echo $this->lang->line('Sub_Category');?></label>
											<select class="form-control" name="sub-category">
												<option><?php echo $this->lang->line('Flight');?></option>
												<option><?php echo $this->lang->line('Hotell');?></option>
												<option><?php echo $this->lang->line('Cruise');?></option>
												<option><?php echo $this->lang->line('Holiday');?></option>
											</select>
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Booking_ID');?></label>
											<input type="text" class="form-control" name="booking-id" placeholder="e.g. CR12567">
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Subject');?></label>
											<input type="text" class="form-control" name="subject" placeholder="Problem Subject">
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Problem_Description');?></label>
											<textarea class="form-control" rows="5" name="problem" placeholder="Your Issue"></textarea>
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit"><?php echo $this->lang->line('SUBMIT_REQUEST');?></button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END: USER PROFILE -->
<!-- END: USER PROFILE -->
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		 jQuery('.alert').show(); 
		 jQuery('#userstory').validate({});
         jQuery('#passwordForm').validate({});
		 jQuery('#userProfile').validate({});
	});
</script>