<?php //echo '<pre>';print_r($searchData);echo '</pre>'; ?>
<input type="hidden" value="<?php echo $this->uri->segment(3); ?>" name="role" id="role" />

<input type="hidden" value="list" name="view" id="view" />
<input type="hidden" id="minPrice" name="minPrice" readonly value="">
<input type="hidden" id="maxPrice" name="maxPrice" readonly value="">
<input type="hidden" id="queryString" name="queryString" readonly value="<?php echo $_SERVER['QUERY_STRING']; ?>">
<!-- START: MODIFY SEARCH -->
	<div class="row modify-search modify-holiday">
		<div class="container">
			<form >
            <input type="hidden" value="<?php echo $_GET['category']; ?>" name="category" id="category" />
			<input type="hidden" value="<?php echo $_GET['subcategory']; ?>" name="subcategory" id="subcategory" />
				<div id="searchHTML"></div>
				<div class="col-md-2 col-sm-6 col-xs-9">
					<div class="form-gp">
						<button type="submit" class="modify-search-button btn transition-effect text-center">MODIFY SEARCH</button>
					</div>
				</div>
			</form>
		</div>
	</div>
<!-- END: MODIFY SEARCH -->

<!-- START: LISTING AREA-->
    <div class="row">
        <div class="container">
            <!-- START: FILTER AREA -->
            <div class="col-md-3 clear-padding ">
                <div class="main-footer" style="padding: 1px 0 !important;">
                   <div class="links">
                   <ul>
                    <li <?php if($this->uri->segment(3)=='accommodations'){ echo "class='active'"; }?>><a href="<?php echo base_url().'search/result/accommodations?category=&subcategory=&checkin=&checkout=';?>">Accoumadation</a> </li>
                    <li <?php if($this->uri->segment(3)=='tours'){ echo "class='active'"; }?>><a href="<?php echo base_url().'search/result/tours?category=&subcategory=&destination=&adult=1&child=1';?>" >Tours</a></li>
                    <li <?php if($this->uri->segment(3)=='adventure'){ echo "class='active'"; }?>><a href="<?php echo base_url().'search/result/adventure?category=&subcategory=&destination=&adult=1&child=1';?>" >Adventure</a></li>
                    <li <?php if($this->uri->segment(3)=='recreations'){ echo "class='active'"; }?>><a href="<?php echo base_url().'search/result/recreations?category=&subcategory=&destination=&adult=1&child=1';?>" >Recreations </a></li>
                  </ul>
                   
                </div>
                </div>
                <div style="clear:both;"></div>
                <div class="filter-head text-center">
                    <h4><span id="count"><?php if(isset($searchData)){ echo count($searchData); } ?></span> Result Found Matching Your Search.</h4>
                </div>
                <div class="filter-area">
                    <div class="price-filter filter">
                        <h5><i class="fa fa-usd"></i> Price</h5>
                           <p>
                            <label></label>
                            <input type="text" id="amount" name="amount" readonly value="">
                           </p> 
                        <div id="price-range"></div>
                    </div>
                    <div class="star-filter filter" id="categoryview">
                        <h5><i class="fa fa-list"></i> Category</h5>
                        <ul>
                        <?php 
						$this->db->join('tbl_maincategory_master','tbl_maincategory_master.category_id=tbl_category_master.maincategory_id');
						$Cat=$this->master_model->getRecords('tbl_category_master',array('tbl_category_master.category_status'=>'active','tbl_category_master.parent_id'=>'0','tbl_maincategory_master.category_slug'=>$this->uri->segment(3)),'tbl_category_master.*');
							if(count($Cat)>0){
								foreach($Cat as $c){
						?>
                            <li><input type="checkbox" name="sortCategory" id="sortCategory" value="<?php echo $c['category_id']; ?>" <?php if($c['category_id']==$_GET['category']){ echo 'checked="checked"';} ?>> <?php echo $c['category_name_eng']; ?></li>
                            <?php } } ?>
                        </ul>
                    </div>
                    <div class="location-filter filter" id="subcategoryview">
                        <h5><i class="fa fa-list"></i> Subcategory</h5>
                        <ul>
                            <?php 
						$this->db->join('tbl_maincategory_master','tbl_maincategory_master.category_id=tbl_category_master.maincategory_id');
						$Cat=$this->master_model->getRecords('tbl_category_master',array('tbl_category_master.category_status'=>'active','tbl_category_master.parent_id !='=>'0','tbl_maincategory_master.category_slug'=>$this->uri->segment(3)),'tbl_category_master.*');
							if(count($Cat)>0){
								foreach($Cat as $c){
						?>
                            <li><input type="checkbox" name="sortSubcategory" id="sortSubcategory" value="<?php echo $c['category_id']; ?>" <?php if($c['category_id']==$_GET['subcategory']){ echo 'checked="checked"';} ?>> <?php echo $c['category_name_eng']; ?></li>
                            <?php } } ?>
                        </ul>
                    </div>
                    
                </div>
            </div>
            <!-- END: FILTER AREA -->
            <!-- START: INDIVIDUAL LISTING AREA -->
            <div class="col-md-9 holidays-listing">
                <!-- START: SORT AREA -->
                <div class="sort-area col-sm-10">
                    <div class="col-md-3 col-sm-3 col-xs-6 sort">
                        <select class="selectpicker" name="order_price" id="order_price">
                            <option value="">Price</option>
                            <option value="asc"> Low to High</option>
                            <option value="desc"> High to Low</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 sort">
                        <select class="selectpicker" name="order_star" id="order_star">
                            <option value="">Star Rating</option>
                            <option value="asc"> Low to High</option>
                            <option value="desc"> High to Low</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 sort">
                        <select class="selectpicker" name="order_user" id="order_user">
                            <option value="">User Rating</option>
                            <option value="asc"> Low to High</option>
                            <option value="desc"> High to Low</option>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6 sort">
                        <select class="selectpicker" name="order_name" id="order_name">
                            <option value="">Name</option>
                            <option value="asc"> Ascending</option>
                            <option value="desc"> Descending</option>
                        </select>
                    </div>
                </div>
                <!-- END: SORT AREA -->
                            <div class="clearfix visible-xs-block"></div>
                <div class="col-sm-2 view-switcher">
                    <div class="pull-right">
                        <a href="javascript:void(0);" title="Grid View" rel="grid">
                            <i class="fa fa-th-large"></i>
                        </a>
                        <a href="javascript:void(0);" title="List View" rel="list">
                            <i class="fa fa-list"></i>
                        </a>
                        <?php if($this->uri->segment(3)=='tours' || $this->uri->segment(3)=='adventure' || $this->uri->segment(3)=='recreations' || $this->uri->segment(3)=='accommodations') {?>
                        <a href="javascript:void(0);" title="Map View" rel="map">
                            <i class="fa fa-map-marker"></i>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <!-- START: HOTEL LIST VIEW -->
                <div class="switchable col-md-12 clear-padding">
                <div id="searchResult"></div>
                <?php /*?><?php if($searchData){ 
						foreach($searchData as $res){?>
                    <div  class="hotel-list-view">
                        <div class="wrapper">
                            <div class="col-md-4 col-sm-6 switch-img clear-padding">
                                <img src="<?php echo base_url(); ?>uploads/forms/<?php echo $res['form_image']; ?>" alt="<?php echo $res['form_title']; ?>">
                            </div>
                            <div class="col-md-6 col-sm-6 hotel-info">
                                <div>
                                    <div class="hotel-header">
                                        <h5><?php echo $res['form_title']; ?> <span><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star-o colored"></i></span></h5>
                                        <p><i class="fa fa-map-marker"></i><?php echo $res['form_address']; ?> <i class="fa fa-phone"></i><?php echo $res['form_mobile']; ?></p>
                                    </div>
                                    <div class="hotel-facility">
                                        <p><i class="fa fa-plane" title="Flight Included"></i><i class="fa fa-bed" title="Luxury Hotel"></i><i class="fa fa-taxi" title="Transportation"></i><i class="fa fa-beer" title="Bar"></i><i class="fa fa-cutlery" title="Restaurant"></i></p>
                                    </div>
                                    <div class="hotel-desc">
                                        <p><?php echo $res['form_desc']; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix visible-sm-block"></div>
                            <div class="col-md-2 rating-price-box text-center clear-padding">
                                <div class="rating-box">
                                    <div class="tripadvisor-rating">
                                        <img src="<?php echo base_url(); ?>assets/images/tripadvisor.png" alt="cruise"><span>4.5/5.0</span>
                                    </div>
                                    <div class="user-rating">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                                        <span>128 Guest Reviews.</span>
                                    </div>
                                </div>
                                <div class="room-book-box">
                                    <div class="price">
                                        <h5>$<?php echo $res['form_price']; ?>/Person</h5>
                                    </div>
                                    <div class="book">
                                        <a href="#">BOOK</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix visible-sm-block"></div>
                <?php } } else { ?>
                No record(s) found.
                <?php } ?><?php */?>
                
                    <!-- END: HOTEL LIST VIEW -->
                
                <!-- START: PAGINATION -->
                <!--<div class="bottom-pagination">
                    <nav class="pull-right">
                        <ul class="pagination pagination-lg">
                            <li><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                            <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">2 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">3 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">4 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">5 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">6 <span class="sr-only">(current)</span></a></li>
                            <li><a href="#" aria-label="Previous"><span aria-hidden="true">&#187;</span></a></li>
                        </ul>
                    </nav>
                </div>-->
                <!-- END: PAGINATION -->
            </div>
            <!-- END: INDIVIDUAL LISTING AREA -->
        </div>
    </div>
    <!-- END: LISTING AREA -->
    </div>


<script>

	/* Price Range Slider */
	  
	$(function() {
		search();
		
		"use strict";
		$( "#price-range" ).slider({
		  range: true,
		  min: 0,
		  max: 1000,
		  values: [ 0, 1000 ],
		  slide: function( event, ui ) {
			$( "#amount" ).val( "$ " + ui.values[ 0 ] + " - $ " + ui.values[ 1 ] );
			$( "#minPrice" ).val(ui.values[ 0 ]);
			$( "#maxPrice" ).val(ui.values[ 1 ]);
		  },
		  change: function(event, ui) {
			 search();
		  }
		});
		$( "#minPrice" ).val($( "#price-range" ).slider( "values", 0 ));
		$( "#maxPrice" ).val($( "#price-range" ).slider( "values", 1 ));
		$( "#amount" ).val( "$ " + $( "#price-range" ).slider( "values", 0 ) +
		  " - $ " + $( "#price-range" ).slider( "values", 1 ) );
	  });
</script>