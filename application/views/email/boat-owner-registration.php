<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Emailer</title>
</head>

<body style="background-color:#fff; margin:0px; padding:0px;">
<table cellpadding="0" cellspacing="0" style="width:650px; background-color:#f8f8f8;">
	<tr style="background-color:#0066CC; height:40px;">
		<td align="center"><a href="<?php echo base_url(); ?>">Rehla Ticket</a></td>
	</tr>
	<tr>
		<td style="padding:30px 10px;">Hello <?php echo $firstname;?>,</td>
	</tr>
	<tr>
		<td style="padding:0 10px;">Congratulation! you are successfully registered.</td>
	</tr>
    <tr>
		<td style="padding:0 10px;">Following are your details :</td>
	</tr>
	<tr>
		<td style="padding:0px 10px;">Name : <?php echo $firstname.' '.$lastname;?></td>
        <td style="padding:0px 10px;"></td>
	</tr>
    <tr>
		<td style="padding:0px 10px;">Email : <?php echo $email;?></td>
        <td style="padding:0px 10px;"></td>
	</tr>
    <tr>
		<td style="padding:0px 10px;">Password : <?php echo $password;?></td>
        <td style="padding:0px 10px;"></td>
	</tr>
	<tr>
		<td style="padding:30px 10px;">Please <a href="<?php echo base_url(); ?>owner/verify/<?php echo base64_encode($userId);?>" style="color:#cea070;">Click here</a> for email verification.</td>
	</tr>
	<tr>
		<td style="padding:0 10px 30px 10px;">Regards,<br>Rehla Ticket</td>
	</tr>
	<tr style="background-color:#0066CC; height:60px;">
		<td style="text-align:center; padding:10px 10px; color:#fff; font-size:12px; line-height:20px;"><a href="<?php echo base_url(); ?>" style="color:#cea070; text-decoration:none;">rehlaticket.com</a><br>
		</td>
	</tr>
</table>
</body>
</html>
