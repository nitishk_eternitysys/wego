<!-- START: USER PROFILE -->
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3><?php echo $this->lang->line('Welcome');?><?php echo $fetch_array[0]['user_name']; ?></h3>
			</div>
			
			<div class="col-md-12 col-sm-12">
				<div class="tab-content">
					<div id="booking" class="tab-pane fade in active">
						
						<div class="col-md-12">
                        <?php 
								if($order[0]['type']=='Tours' || $order[0]['type']=='Adventure' || $order[0]['type']=='Recreation')
								{
									$this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_transaction_master.booking_form_id');
									$this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image');
								    $tours=$this->master_model->getRecords('tbl_transaction_master',array('tbl_three_forms.hosts_id'=>$this->session->userdata('userID')));
								}
								else
								{
									$this->db->join('tbl_accommodations_master','tbl_accommodations_master.accom_id=tbl_transaction_master.booking_form_id');
									$this->db->select('tbl_transaction_master.*,tbl_accommodations_master.accom_title AS title,tbl_accommodations_master.accom_image AS image');
								    $tours=$this->master_model->getRecords('tbl_transaction_master',array('tbl_accommodations_master.hosts_id'=>$this->session->userdata('userID')));
								}
							
						?>
							<div class="item-entry">
								<span><?php echo $this->lang->line('Order_ID');?> <?php echo $tours[0]['orderID']; ?></span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
                                        	<?php if($tours[0]['image']!=''){ 
													if($order[0]['type']=='Tours' || $order[0]['type']=='Adventure' || $order[0]['type']=='Recreation'){
											?>
                                                <img src="<?php echo $this->master_model->resize($tours[0]['image'],500,350,'uploads/forms/');?>" alt="<?php echo $tours[0]['title']; ?>">
                                             <?php } else {?>
                                             	<img src="<?php echo $this->master_model->resize($tours[0]['image'],500,350,'uploads/accom/');?>" alt="<?php echo $tours[0]['title']; ?>">
                                             <?php } } else { ?>
                                                <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $tours[0]['title']; ?>">
                                            <?php } ?>
										</div>
										<div class="col-md-4 col-sm-4">
											<h4><?php echo ucfirst($tours[0]['title']); ?></h4>
											<p><strong><?php echo $this->lang->line('Booking_Type');?></strong>: <?php echo ucfirst($order[0]['booking_type']); ?> </p>
											<p><strong><?php echo $this->lang->line('Payment_by');?></strong>: <?php echo ucfirst($order[0]['payType']); ?></p>
										</div>
										<div class="col-md-3 col-sm-3">
                                        <?php if($order[0]['status']=='pending'){ ?>
											<p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order[0]['status']); ?></p>
                                            <?php } else if($order[0]['status']=='approve'){ ?>
                                            <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order[0]['status']); ?></p>
                                            <?php } else if($order[0]['status']=='cancel'){ ?>
                                            <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order[0]['status']); ?></p>
                                            <?php } else {} ?>
										</div>
                                        
										<div class="col-md-3 col-sm-3">
                                        	<p><a href="#"><?php echo $this->lang->line('Cancel');?></a></p>
										</div>
                                        
									</div>
									<div class="item-footer">
										<p><strong><?php echo $this->lang->line('Order_Date');?></strong> <?php echo date('d M Y',strtotime($order[0]['tranDate'])); ?><strong><?php echo $this->lang->line('Order_Total');?></strong> $<?php echo $order[0]['tranAmount']; ?> <?php if($order[0]['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order[0]['doc']; ?>" download><?php echo $this->lang->line('Attachment');?></a></strong><?php } ?></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
            
          <div class="col-md-12 col-sm-12">
            <div class="most-recent-booking">
                <h4><?php echo $this->lang->line('Passangers_list');?></h4>
                
                <div class="field-entry">
                	<div class="col-md-1 col-sm-1 col-xs-1 clear-padding">
                        <p><strong><?php echo $this->lang->line('No');?></strong></p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                        <p><strong><?php echo $this->lang->line('Passenger_Name');?></strong></p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                        <p><strong><?php echo $this->lang->line('Gender');?></strong></p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 clear-padding">
                        <p><strong><?php echo $this->lang->line('Age');?></strong></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <?php 
					$passangers=$this->master_model->getRecords('tbl_passenger_list',array('tmp_booking_id'=>$order[0]['booking_id']));
					if(count($passangers)>0){
						$i=1;
						foreach($passangers as $pass){
				 ?>
                <div class="field-entry">
                	<div class="col-md-1 col-sm-1 col-xs-1 clear-padding">
                        <p><strong><?php echo $i; ?>.</strong></p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                        <p><?php echo ucfirst($pass['passenger_name']); ?></p>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                        <p><?php echo ucfirst($pass['passenger_gender']); ?></p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 clear-padding">
                        <p><?php echo $pass['passanger_age']; ?></p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php $i++; } } ?>
            </div>
          </div>
            
		</div>
	</div>
<!-- END: USER PROFILE -->
<!-- END: USER PROFILE -->