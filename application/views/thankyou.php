<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3><?php echo $this->lang->line('THANK_YOU');?></h3>
			<h4 class="thank"><i class="fa fa-thumbs-o-up"></i> <?php echo $this->lang->line('Your_Booking_Confirm');?></h4>
		</div>
	</div>
	<!-- END: PAGE TITLE -->

	<!-- START: BOOKING DETAILS -->
	<div class="row">
		<div class="container clear-padding">
			<div>
				<div class="col-md-8 col-sm-8">
					<div class=" confirmation-detail">
						<h3><?php echo $this->lang->line('Booking_Details');?></h3>
						<table class="table">
							<tr>
								<td><?php echo $this->lang->line('Booking_ID');?></td>
								<td><?php echo $details[0]['orderID'] ?></td>
							</tr>
							<?php if( $details[0]['payType']=='paypal'){ ?>
							<tr>
								<td><?php echo $this->lang->line('Transaction_ID');?></td>
								<td><?php echo $details[0]['transactionID'] ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td><?php echo $this->lang->line('Booked');?></td>
								<td><?php echo $boatDetails[0]['title'] ?></td>
							</tr>
							<tr>
								<td><?php echo $this->lang->line('Purchased_Amount');?></td>
								<td>$<?php echo $details[0]['tranAmount'] ?></td>
							</tr>
							<tr>
								<td><?php echo $this->lang->line('Purchased_Date');?></td>
								<td><?php echo date('d M Y H:i A',strtotime($details[0]['tranDate'])) ?></td>
							</tr>
							<tr>
								<td><?php echo $this->lang->line('Payer_Name');?></td>
								<td><?php echo $this->session->userdata('userFName'); ?></td>
							</tr>
							<tr>
								<td><?php echo $this->lang->line('Email');?></td>
								<td><?php echo $this->session->userdata('userEmail'); ?></td>
							</tr>

						</table>
						<p><?php echo $this->lang->line('You_can_check');?></p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 booking-sidebar">
					<div class="sidebar-item contact-box">
						<h4><i class="fa fa-phone"></i><?php echo $this->lang->line('Need_Help');?></h4>
						<div class="sidebar-body text-center">
							<p><?php echo $this->lang->line('Call_us');?></p>
							<h2>+91 1234567890</h2>
						</div>
					</div>
					<!--<div class="sidebar-item rec-box">
						<h4><i class="fa fa-bullhorn"></i>Suggestions For You</h4>
						<div class="sidebar-body">
							<table class="table">
								<tr>
									<td><i class="fa fa-suitcase"></i> Holidays</td>
									<td><a href="#">172 holidays. Starting from $142</a></td>
								</tr>
								<tr>
									<td><i class="fa fa-bed"></i> Hotel</td>
									<td><a href="#">150 deals. Starting from $199</a></td>
								</tr>
								<tr>
									<td><i class="fa fa-bed"></i> Hotels</td>
									<td><a href="#">172 hotels. Starting from $142</a></td>
								</tr>
								<tr>
									<td><i class="fa fa-suitcase"></i> Holidays</td>
									<td><a href="#">150 deals. Starting from $199</a></td>
								</tr>
							</table>
						</div>
					</div>-->
				</div>
			</div>
		</div>
	</div>
	<!-- END: BOOKING DETAILS -->
