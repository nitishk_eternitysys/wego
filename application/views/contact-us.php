	<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3><?php echo $this->lang->line('CONTACT_US');?></h3>
			<h4 class="thank"><?php echo $this->lang->line('Get_Connected');?></h4>
		</div>
	</div>
	<!-- END: PAGE TITLE -->
	<!-- START: CONTACT-US -->
	<div class="row contact-address">
		<div class="container clear-padding">
          <?php $adminContact=$this->master_model->getRecords('tbl_admin_login',array('id'=>'1')); ?>
			<div class="text-center">
				<h2><?php echo $this->lang->line('GET_IN_TOUCH');?></h2>
				<h5><?php echo $this->lang->line('Lorem_Ipsum');?><br> <?php echo $this->lang->line('standard_dummy');?></h5>
				<div class="space"></div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-map-marker"></i>
					<p><?php echo $adminContact[0]['admin_address']; ?></p>
				</div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-envelope-o"></i>
					<p><a href="mailto:<?php echo $adminContact[0]['admin_email']; ?>"><?php echo $adminContact[0]['admin_email']; ?></a></p>
				</div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-phone"></i>
					<p><?php echo $adminContact[0]['admin_phone']; ?></p>
				</div>
			</div>
		</div>
	</div>
	<!-- END: CONTACT-US -->
<div class="row flex-row">
			<div class="col-md-6 col-sm-6 flex-item clear-padding">
				<iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJG1usnet4BTkRzQqb_Ys-JOg&amp;key=AIzaSyB6hgZM-ruUqTPVUjXGUR-vv7WRqc4MXjY" class="contact-map"></iframe>
			</div>
			<div class="col-md-6 col-sm-6 contact-form flex-item">
				<div class="col-md-12">
					<h2><?php echo $this->lang->line('DROP_A_MESSAGE');?></h2>
					<h5><?php echo $this->lang->line('Drop_Us_a_Message');?></h5>
				</div>
				<form>
					<div class="col-md-6 col-sm-6">
						<input type="text" placeholder="Your Name" class="form-control" required name="name">
					</div>
					<div class="col-md-6 col-sm-6">
						<input type="email" placeholder="Your Email" class="form-control" required name="email">
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
						<input type="text" placeholder="Message Title" class="form-control" required name="message-title">
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
						<textarea placeholder="Your Message" id="comment" rows="5" class="form-control"></textarea>
					</div>
					<div class="clearfix"></div>
					<div class="text-center">
						<button class="btn btn-default submit-review" type="submit"><?php echo $this->lang->line('SEND_YOUR_MESSAGE');?></button>
					</div>
				</form>
			</div>
	</div>