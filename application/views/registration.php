 <style >
.reqcls{color:red;}

/*form styles*/
.msform {
margin:30px auto 0px;
text-align: center;
position: relative;
}
/*
#msform fieldset {
background: white;
border: 0 none;
border-radius: 3px;
box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
padding: 20px 30px;
box-sizing: border-box;
width: 80%;
margin: 0 10%;
position: absolute;
}
*/
/*Hide all except first fieldset*/
.msform fieldset:not(:first-of-type) {
display: none;
}
/*inputs*/
.msform input, #msform textarea {
margin-bottom: 10px;
width: 100%;
/*
padding: 15px;
border: 1px solid #ccc;
border-radius: 3px;
box-sizing: border-box;
color: #2C3E50;
font-size: 13px;
*/
}
/*buttons*/
.msform .action-button {
width: 100px;
background: #009bd7;
margin: 10px;
font-weight: bold;
border: 0 none;
border-radius: 1px;
cursor: pointer;
padding: 10px 5px;
color:#fff;
}
.msform .action-button:hover, .msform .action-button:focus {
background: #00adef;
}
/*headings*/
.fs-title {
font-size: 15px;
text-transform: uppercase;
color: #2C3E50;
margin:10px 0 0;
}
.fs-subtitle {
font-weight: normal;
font-size: 13px;
color: #666;
margin:8px 0 15px;
}
/*progressbar*/
.progressbar {
margin-bottom: 30px;
overflow: hidden;
padding:0px;
counter-reset: step;
}
.progressbar li {
list-style-type: none;
color: #555;
text-transform: uppercase;
font-size:12px;
width: 33.33%;
float: left;
position: relative;
}
.progressbar li:before {
content: counter(step);
counter-increment: step;
width:40px;
height:40px;
line-height: 36px;
display: block;
font-size:20px;
color: #333;
background: #e6e6e6;
border-radius:30px;
margin: 0 auto 5px auto;
position: relative;
z-index: 2;
}
/*progressbar connectors*/
.progressbar li:after {
content: '';
width: 100%;
height: 2px;
background: #ccc;
position: absolute;
left: -50%;
top: 20px;
z-index: 1; /*put it behind the numbers*/
}
.progressbar li:first-child:after {
/*connector not needed before the first step*/
content: none;
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
.progressbar li.active:before, .progressbar li.active:after {
background: #009bd7;
color: white;
}    
label{float: left;}
</style>
<script type="application/javascript">
$(document).ready(function(){
  var dateToday = new Date();
  var yrRange = (dateToday.getFullYear() - 100)+ ":" + dateToday.getFullYear();
 jQuery('#registration_user').validate({});
 jQuery('#loginfrm_user').validate({});
 jQuery('#registration_owner').validate({});
 jQuery('#loginfrm_owner').validate({});
 jQuery("#user_birth_day").datepicker({ dateFormat: "yy-mm-dd",changeYear: true,changeMonth: true,  maxDate: '0', yearRange:yrRange, });
});
</script>
<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3><?php echo $this->lang->line('LOGIN_REGISTER');?></h3>
			<h4 class="thank"><?php echo $this->lang->line('Manage_Your_Account');?></h4>
		</div>
	</div>
	<!-- END: PAGE TITLE -->
    <!-- START: LOGIN/REGISTER -->
     <div class="login-box-img">
	<div class="container login-row">
    <div class="col-md-12 element-tab">
        <ul class="nav nav-tabs tab-style1">
            <li <?php if($this->router->fetch_class()=='home'){echo 'class="active"'; } ?> ><a data-toggle="tab" href="#tab1"><?php echo $this->lang->line('User');?></a></li>
            <li <?php if($this->router->fetch_class()=='owner'){echo 'class="active"'; } ?> ><a data-toggle="tab" href="#tab2"><?php echo $this->lang->line('Hosts_Provider');?></a></li>
        </ul>
        <div class="tab-content">
            <div id="tab1" class="tab-pane fade <?php if($this->router->fetch_class()=='home'){echo ' in active';} ?>">
                <div class="row">
                 <div class="login-min">
                <div class="col-sm-12 login-form">
				<?php 
                if($error!=''){  ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php } 
                if($this->session->flashdata('success')!=''){?>	
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                 <h4><?php echo $this->lang->line('Sign_Up');?></h4>
                 <form method="post" id="registration_user" action="<?php echo base_url().'home/registration/'; ?>" class="msform" enctype="multipart/form-data">
						<!-- progressbar -->
                        <ul id="progressbar" class="progressbar">
                            <li class="active"><?php echo $this->lang->line('Account_Setup');?></li>
                            <li><?php echo $this->lang->line('Personal_Details');?></li>
                            <li><?php echo $this->lang->line('Payment_Information');?></li>
                        </ul>
                         <?php if($error_reg!=''){  ?>
                        <div class="alert alert-danger"><?php echo $error_reg; ?></div>
                        <?php } 
                        if($this->session->flashdata('success_user_reg')!=''){?>	
                        <div class="alert alert-success"><?php echo $this->session->flashdata('success_user_reg'); ?></div>
                        <?php } ?>
						<!-- fieldsets -->
                        <fieldset>
                        <h2 class="fs-title"><?php echo $this->lang->line('Create_your_account');?></h2>
                           <h3 class="fs-subtitle"><?php echo $this->lang->line('User_Info');?></h3>
                            <label>Full name <span class='reqcls'>*</span></label>
                            <div class="input-group">
                            <input name="user_name" type="text" id="user_name" class="form-control" placeholder="Full Name" data-rule-required="true"  data-rule-alpha='true'>
                           </div>
                           <label>Email <span class='reqcls'>*</span></label>
                           <div class="input-group">
                            <input name="user_email" type="text" id="user_email" onblur="checkEmailDup(this.value);" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                            <span class="err" id="err_user_email" style="color:red;"></span>
                           </div>
                           <label>Password <span class='reqcls'>*</span></label>
                           <div class="input-group">
                             <input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password" data-rule-required="true" data-rule-minlength="6">
                           </div>	
                           <label>Confirm Password <span class='reqcls'>*</span></label>
                           <div class="input-group">
                                <input name="confirm-password" id="password_confirm" type="password" class="form-control" placeholder="Retype Password" data-rule-required="true" data-rule-minlength="6" data-rule-equalto="#user_password">
                               
                           </div>	
                          <input type="button" name="next" class="next action-button" value="Next" />
                        </fieldset>
                        <fieldset>
                        <h2 class="fs-title"><?php echo $this->lang->line('Personal_Details');?></h2>
                        <h3 class="fs-subtitle"></h3>
                        <label>DOB<span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_birth_day" id="user_birth_day" type="text" class="form-control" placeholder="DOB" data-rule-required="true" readonly="true">
                        </div>
                       <label>Gender <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="male" checked="checked" > <?php echo $this->lang->line('male');?>
                            <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="female" ><?php echo $this->lang->line('Female');?>
                        </div>
                       <label>Mobile Number <span class='reqcls'>*</span></label>
                        <div class="input-group">
                         <input name="user_mobile" id="user_mobile" type="text" class="form-control" placeholder="Mobile Number" data-rule-required="true" data-rule-minlength="7" data-rule-maxlength="16" onkeypress="return OnlyNumericKeys(event);" >
                        </div>
                        <label>Country <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <select class="form-control" data-rule-required="true" name="user_country" id="user_country">
                            <option value=""><?php echo $this->lang->line('Select_country');?></option>
                             <?php
                							 $this->db->group_by('country');
                							 $country=$this->master_model->getRecords('tbl_cities_master');
                							 if(count($country))
                							 {
                								foreach($country  as $coun)
                								{
                								?>
                                                 <option value="<?php echo $coun['country']; ?>"><?php echo $coun['country']; ?></option>
                                               <?php
                							   }
                							 }
                							?>
                           </select>
                           <!--<input name="user_country" type="text" class="form-control" placeholder="Country" data-rule-required="true" >-->
                        </div>
                        <label>City <span class='reqcls'>*</span></label>
                        <div class="input-group">
                           <select class="form-control" data-rule-required="true" name="user_city" id="user_city">
                             <option value=""><?php echo $this->lang->line('Select_City');?></option>
                           </select>
                        </div>
                        <label>ID Proof <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_id_info" type="file" id="user_id_info"  data-rule-required="true" accept="image/*" >
                            <div class="clr"></div>
                            <span><?php echo $this->lang->line('please_upload');?></span>
                        </div>
                        <input type="button" name="previous" class="previous action-button" value="Previous" />
                        <input type="button" name="next" class="next action-button" value="Next" />
                        </fieldset>
                        <fieldset>
                        <h2 class="fs-title"></h2>
                        <h3 class="fs-subtitle"><?php echo $this->lang->line('Payment_Information');?></h3>
                         <label> payment method <span class='reqcls'>*</span></label>
                        <div class="input-group">
                          <select class="form-control" data-rule-required="true" name="user_payment_method" id="user_payment_method">
                            <option value=""><?php echo $this->lang->line('Select_Payment');?></option>
                            <option value="Paypal"><?php echo $this->lang->line('Paypal');?></option>
                            <option value="Bank"><?php echo $this->lang->line('Bank');?></option>
                          </select>
                          <!--<input name="" type="text" class="form-control" placeholder="payment method " data-rule-required="true" >-->
                        </div>
                          <label>Preferred currency <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <select class="form-control" data-rule-required="true" name="user_preferred_currency" id="user_preferred_currency">
                            <option value=""><?php echo $this->lang->line('Select_currency');?></option>
                             <?php
							 $lang=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
							 if(count($lang))
							 {
								foreach($lang as $row)
								{
								?>
                                 <option value="<?php echo $row['currency_id']; ?>"><?php echo $row['currency_'.$this->session->userdata('lang')]; ?></option>
                               <?php
							   }
							 }
							?>
                           </select>
                          <!--<input name="user_preferred_currency" type="text" class="form-control" placeholder="Preferred currency" data-rule-required="true" >-->
                            
                        </div>
                         <label>Language <span class='reqcls'>*</span></label>
                        <div class="input-group">
                           <select class="form-control" data-rule-required="true" name="user_preferred_language" id="user_preferred_language">
                            <option value=""><?php echo $this->lang->line('Select_Language');?></option>
                             <?php
							 $lang=$this->master_model->getRecords('tbl_language_master',array('language_status'=>'active'));
							 if(count($lang))
							 {
								foreach($lang as $row)
								{
								?>
                                 <option value="<?php echo $row['language_id']; ?>"><?php echo $row['language_'.$this->session->userdata('lang')]; ?></option>
                               <?php
							   }
							 }
							?>
                           </select>
                              
                            <!--<input name="user_preferred_language" type="text" class="form-control" placeholder="Language" data-rule-required="true" >-->
                           
                        </div>
                         <label>Website <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_website_here" id="user_website_here" type="text" class="form-control" placeholder="Website" data-rule-required="true" >
                           
                        </div>
                       <label>Interests <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <textarea name="user_interests" id="user_interests" class="form-control" data-rule-required="true" placeholder='Add your Interests'></textarea></div>
                             <?php echo $this->lang->line('I_agree_To');?><a href="<?php echo base_url().'home/page/2';?>" target="new"><?php echo $this->lang->line('Terms_Conditions');?></a> <input name="tc" type="checkbox" required>
                          <input type="button" name="previous" class="previous action-button" value="Previous" />
                          <button type="submit action-button" name="btn_register" id="btn_register" value="1"><?php echo $this->lang->line('REGISTER_ME');?><i class="fa fa-edit"></i></button>
                        </fieldset>
				   </form>    
                 </div>
                 <div class="clearfix"></div>
                </div>
                </div>
            </div>
            <div id="tab2" class="tab-pane fade <?php if($this->router->fetch_class()=='owner'){echo ' in active'; } ?> ">
               <div class="row">
                  <div class="login-min"> 
                    <div class="col-sm-12 login-form">
						  <?php if($this->session->flashdata('error_owner_reg')){  ?>
                            <div class="alert alert-danger"><?php echo $this->session->flashdata('error_owner_reg'); ?></div>
                            <?php } 
                            if($this->session->flashdata('success_owner_reg')!=''){?>	
                            <div class="alert alert-success"><?php echo $this->session->flashdata('success_owner_reg'); ?></div>
                            <?php } ?>
                         <h4><?php echo $this->lang->line('Sign_Up');?></h4>
                         <form method="post" id="registration_owner" enctype="multipart/form-data" action="<?php echo base_url().'owner/regitration/'; ?>" class="msform">
                                <!-- progressbar -->
                                <ul id="progressbar" class="progressbar">
                                    <li class="active"><?php echo $this->lang->line('Account_Setup');?></li>
                                    <li><?php echo $this->lang->line('Personal_Details');?></li>
                                    <li><?php echo $this->lang->line('Payment_Information');?></li>
                                </ul>
                                <?php if($error_reg!=''){  ?>
                                <div class="alert alert-danger"><?php echo $error_reg; ?></div>
                                <?php }if($this->session->flashdata('success_user_reg')!=''){?>	
                                <div class="alert alert-success"><?php echo $this->session->flashdata('success_user_reg'); ?></div>
                                <?php } ?>
                                <!-- fieldsets -->
                                <fieldset>
                                <h2 class="fs-title"><?php echo $this->lang->line('Create_your_account');?></h2>
                                   <h3 class="fs-subtitle"><?php echo $this->lang->line('User_Info');?></h3>
                                    <label>Full Name <span class='reqcls'>*</span></label>
                                    <div class="input-group">
                                        <input name="hosts_name" id="hosts_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true" data-rule-lettersonly="true">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                    </div>
                                    <label>Company Name <span class='reqcls'>*</span></label>
                                    <div class="input-group"><input name="hosts_company" id="hosts_company" type="text" class="form-control" placeholder="Company name" data-rule-required="true"><span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span> </div>
                                    <label>Types  <span class='reqcls'>*</span></label>
                                    <div class="input-group">
                                              <select multiple="multiple" class="form-control" data-rule-required="true" style="height:auto !important;" name="hosts_mainategory_id[]">
                                                  <?php if(count($categories)>0){ 
                                                        foreach($categories as $cat){?>
                                                <option value="<?php echo $cat['category_id'] ?>"><?php echo $cat['category_name_'.$this->session->userdata('lang')] ?></option>
                                                <?php } } ?>
                                        </select>
                                    </div>
                                    <label>Email <span class='reqcls'>*</span></label>
                                    <div class="input-group">
                                        <input name="hosts_email" id="hosts_email" type="text" class="form-control" placeholder="Email" onblur="checkHostDup(this.value);" data-rule-required="true" data-rule-email="true"><span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                       
                                        </div>
                                         <span style="color:red;" id="err_host_email"></span>
                                    <label>Password <span class='reqcls'>*</span></label>
                                    <div class="input-group">
                                     <input name="hosts_password" id="hosts_password" type="password" class="form-control" placeholder="Password" data-rule-required="true" data-rule-minlength="6"><span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                                   </div>	
                                    <label>Confirm Password <span class='reqcls'>*</span></label>
                                    <div class="input-group">
                                        <input name="confirm-password" id="password_confirm1" type="password" class="form-control" placeholder="Retype Password"data-rule-required="true" data-rule-minlength="6" data-rule-equalto="#hosts_password">
                                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                                    </div>
                                    <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                <h2 class="fs-title"><?php echo $this->lang->line('Personal_Details');?></h2>
                                <h3 class="fs-subtitle"></h3>
                                <!--
                                <label>Contact Number <span class='reqcls'>*</span></label>
                                <div class="input-group">
                                    <input name="hosts_contact" id="hosts_contact" type="text" class="form-control" placeholder="Phone number" data-rule-required="true">
                                    <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                                </div>-->
                                 <input name="hosts_contact" id="hosts_contact" type="hidden" class="form-control" placeholder="Phone number">
                                
                                 <label>Description <span class='reqcls'>*</span></label>
                                <div class="input-group">
                                    <textarea name="hosts_description" id="hosts_description" class="form-control" data-rule-required="true"  style="height:auto !important;" placeholder="Description"></textarea>
                                   <span style="color:red;" id="err_hosts_description"></span> 
                                </div>
                                 <label>Address <span class='reqcls'>*</span></label>
                                <div class="input-group">
                                    <textarea name="hosts_address" id="hosts_address" class="form-control" data-rule-required="true" style="height:auto !important;" placeholder="Address"></textarea>
                                </div>
                                 <label>Website URL</label>
                                <div class="input-group">
                                    <input name="hosts_website" id="hosts_website" type="text" class="form-control" placeholder="Website" data-rule-url="true">
                                    <span class="input-group-addon"><i class="fa fa-globe fa-fw"></i></span>
                                </div>
                                <label>Mobile Number</label>
                                <div class="input-group">
                                    <input name="hosts_mobile" id="hosts_mobile" type="text" class="form-control" placeholder="Mobile Number" data-rule-minlength="7" data-rule-maxlength="16" data-rule-number="true">
                                    <span class="input-group-addon"><i class="fa fa-mobile fa-fw"></i></span>
                                </div>
                               <label>Position</label>
                                <div class="input-group">
                                    <input name="hosts_position" id="hosts_position" type="text" class="form-control" placeholder="Position">
                                    <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                                </div>
                               
                                <input type="button" name="previous" class="previous action-button" value="Previous" />
                                <input type="button" name="next" class="next action-button" value="Next" />
                                </fieldset>
                                <fieldset>
                                <h2 class="fs-title"></h2>
                                <h3 class="fs-subtitle"><?php echo $this->lang->line('Payment_Information');?></h3>
                                <label>Preferred Payment<span class='reqcls'>*</span></label>
                                <div class="input-group">
                                    <select class="form-control" data-rule-required="true" name="hosts_preferred_payment">
                                        <option value=""><?php echo $this->lang->line('Select_Payment');?></option>
                                        <option value="Paypal"><?php echo $this->lang->line('Paypal');?></option>
                                        <option value="Bank"><?php echo $this->lang->line('Bank');?></option>
                                    </select>
                                </div>
                               <label>Preferred Currency<span class='reqcls'>*</span></label>
                                <div class="input-group">
                                    <select class="form-control" data-rule-required="true" name="hosts_preferred_currency">
                                        <option value=""><?php echo $this->lang->line('Select_currency');?></option>
                                          <?php
                                         $lang=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
                                         if(count($lang))
                                         {
                                            foreach($lang as $row)
                                            {
                                            ?>
                                             <option value="<?php echo $row['currency_id']; ?>"><?php echo $row['currency_'.$this->session->userdata('lang')]; ?></option>
                                           <?php
                                           }
                                         }
                                        ?>
                                    </select>
                                </div>
                                <input name="tc" type="checkbox" required>  <?php echo $this->lang->line('I_agree_To');?><a href="<?php echo base_url().'home/page/2';?>" target="new"><?php echo $this->lang->line('Terms_Conditions');?></a>
                                <input type="button" name="previous" class="previous action-button" value="Previous" />
                                <button type="submit action-button"  name="btn_register_owner" id="btn_register_owner" value="1"> <?php echo $this->lang->line('REGISTER_ME');?><i class="fa fa-edit"></i></button>
                               </fieldset>
                           </form>    
                	</div>
                    <div class="clearfix"></div>     
                 </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
   </div>
 </div>
     <div class="clearfix"></div>  
    </div>
<!-- jQuery --> 
<!-- jQuery easing plugin -->  
<script src="front/js/jquery.easing.min.js" type="text/javascript"></script> 
<script>
/*=======Working on Seema Jadhav(07-March-2017)====*/
var base_url = '<?php echo base_url();?>';

function checkEmailDup(email_id)
{
    
      if(email_id!="") 
      {


          $.ajax({
            url: base_url+'ajax/check_email_duplication',
            data: {
               email: email_id
            },
            type: 'POST',
            dataType: 'json',
            success: function(data) {

              if($.trim(data.msg)=='exist')
              {

                  $('#err_user_email').html('This email ID is already exist.')
                  $('#user_email').on('keyup',function(){ $('#err_user_email').html(''); });
                  $('#user_email').focus();
                  return false;
              }
              else if($.trim(data.msg)=='empty')
              {

                  $('#err_user_email').html('Email is required.')
                  $('#user_email').on('keyup',function(){ $('#err_user_email').html(''); });
                  $('#user_email').focus();
                  return false;
              }
              else{ return true; }

            }
              
            
         });
      }
}

/*===============Host=========*/

function checkHostDup(email_id)
{
    
      if(email_id!="") 
      {


          $.ajax({
            url: base_url+'ajax/check_host_duplication',
            data: {
               email: email_id
            },
            type: 'POST',
            dataType: 'json',
            success: function(data) {

              if($.trim(data.msg)=='exist')
              {

                  $('#err_host_email').html('This email ID is already exist.')
                  $('#host_email').on('keyup',function(){ $('#err_host_email').html(''); });
                  $('#host_email').focus();
                  return false;
              }
              else if($.trim(data.msg)=='empty')
              {

                  $('#err_host_email').html('Email is required.')
                  $('#host_email').on('keyup',function(){ $('#err_host_email').html(''); });
                  $('#host_email').focus();
                  return false;
              }
              else{ return true; }

            }
              
            
         });
      }
}

$(function()
{

  $('#hosts_description').unbind('keyup change input paste').bind('keyup change input paste',function(e){
      var $this = $(this);
      var val = $this.val();
      var valLength = val.length;
      var maxCount = 60;
      if(valLength>maxCount){
          $('#err_hosts_description').html('Sorry,No more description is allowed only 60 character are allowed.');
      }
  }); 
});  
/*==========End=================================*/
$(function() {
jQuery.validator.addMethod("alpha", function(value, element) { 
	  return this.optional(element) || /^([a-zA-Z]+\s)*[a-zA-Z]+$/.test(value)
  // just ascii letters
},"Letters only please");
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
$(".next").click(function(){
 if($('#tab2').hasClass('active in'))
 {	
    var formID='registration_owner';
 }
 else
 {
   var formID='registration_user';
 }
 if($('#'+formID).valid())
 {
		
		if(animating) return false;
		animating = true;
		current_fs = $(this).parent();
		next_fs = $(this).parent().next();
		//activate next step on progressbar using the index of next_fs
		$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
		//show the next fieldset
		next_fs.show(); 
		//hide the current fieldset with style
		current_fs.animate({opacity: 0}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.2;
				//2. bring next_fs from the right(50%)
				left = (now * 50)+"%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({'transform': 'scale('+scale+')'});
				next_fs.css({'left': left, 'opacity': opacity});
			}, 
			duration: 0, 
			complete: function(){
				current_fs.hide();
				animating = false;
			}, 
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	} 
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 0, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
})

});
</script>