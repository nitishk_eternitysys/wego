<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/admin-validation.js"></script>
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
       <h1><i class="fa fa-book"></i>Menu</h1>
       <h4>Menu List</h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active">Manage Menu</li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<form name="frm-manage-newsletter" id="frm-manage-newsletter" action="<?php echo base_url().'superadmin/menu/manage/';?>"  method="post">
<div class="row">
    <div class="col-md-12">
        <div class="box">
         <?php 
                      if($this->session->flashdata('error')!=''){  ?>
                        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                    <?php } 
                      if($this->session->flashdata('success')!=''){?>	
                        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                    <?php } ?>
            <div class="box-title">
                <h3><i class="fa fa-table"></i> Manage Menu</h3>
                <div class="box-tool">
                  
                </div>
            </div>
            <div class="box-content">
                <div class="btn-toolbar pull-right clearfix">
                    <div class="btn-group">
                        <a class="btn btn-circle show-tooltip" title="Add Category" href="<?php echo base_url().'superadmin/menu/add/';?>"><i class="fa fa-plus"></i></a>
                       <button type="submit" name="multiple_delete" id="multiple_delete"  class="btn btn-circle btn-fill btn-bordered btn-primary" onclick="javascript : return deletconfirm();" ><i class="fa fa-trash-o"></i></button>
                    </div>
                    
                    <div class="btn-group">
                        <a class="btn btn-circle show-tooltip" title="Refresh" href="<?php echo base_url().'superadmin/menu/manage/'; ?>"><i class="fa fa-repeat"></i></a>
                    </div>
                    
                    
                </div>
                <br/><br/>
                <div class="clearfix"></div>
                <div class="table-responsive" style="border:0">
                 
                    <table class="table table-advance" <?php if(count($fetch_menu)>0){?> id="table1" <?php } ?>>
                       <thead>
                            <tr>
                               <th style="width:18px">
                                <input type="checkbox" name="mult_change" id="mult_change" value="delete" /></th>
                               <th>Menu English</th>
                               <th>Menu Arabic</th>
                               <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                         <?php 
						 if(count($fetch_menu)>0)
						 {
						   foreach($fetch_menu as $d) 
						   {
						 ?>
                             <tr>
                                <td style="width:18px"><input type="checkbox" name="checkbox_del[]" id="checkbox_del[]" value="<?php echo $d['menu_id']; ?>"/></td>
                                
                                <td><?php echo stripslashes($d['menu_name_eng']); ?></td>
                                <td><?php echo stripslashes($d['menu_name_arb']); ?></td>
                                <td>
                                <a  href="<?php echo base_url().'superadmin/menu/update/'.$d['menu_id'];?>" title="Click here for Edit"><i class="fa fa-edit"  style="font-size: 18px;"></i>
                                </a>
                                <a href="<?php echo base_url().'superadmin/menu/delete/'.$d['menu_id'];?>" onclick="javascript : return deletconfirm();" title="Click here for Delete" ><i class="fa fa-trash-o"  style="font-size: 18px;"></i></a>
                                </td>
                             </tr>
                        <?php 
						   }
						 }
						 else
						 { ?>
                        <tr>
                          <td colspan="6"><div class="alert alert-danger" style="text-align:center;">No Data Found.</div></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<!-- END Main Content -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/data-tables/bootstrap3/dataTables.bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>