 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap-fileupload/bootstrap-fileupload.css" />
<!-- BEGIN Page Title -->
<div class="page-title">
    <div style="clear:both !important;">
        <h1><i class="fa fa-book"></i><?php echo $pageLable; ?></h1>
        <h4>Edit <?php echo $pageLable; ?></h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li>
        	<a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>">Manage <?php echo $pageLable; ?></a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active">Edit <?php echo $pageLable; ?> </li>
    </ul>
</div>
<style type="text/css">
  .star{ color:red; }
</style>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i>Edit Host Information</h3>
                <div class="box-tool">
                    <a  class="show-tooltip" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/';?>" title="Back"><i class="fa fa-chevron-up"></i></a>
                   <!-- <a data-action="close" href="#"><i class="fa fa-times"></i></a>-->
                </div>
            </div>
            <div class="box-content">
              <form method="post" class="form-horizontal" action="<?php echo base_url();?>superadmin/host/edit/<?php echo $this->uri->segment(4);?>" id="validation-form" enctype="multipart/form-data">

                   <div class="form-group">
                    <div class="col-sm-12">
				          	<?php 
                      if($error!=''){  ?>
                        <div class="alert alert-danger"><?php echo $error; ?></div>
                    <?php } 
                      if($this->session->flashdata('success')!=''){?>	
                        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                    <?php } ?>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Full Name <span class="star">*</span></label>
                    <div class="col-md-6">
                       <input type="text" class="form-control" name="full_name" id="full_name" placeholder="Full Name" 
                       value="<?php echo $host_arr[0]['hosts_name'];?>" data-rule-required="true" />
                      <div class="error_msg" id="error_full_name"><?php echo form_error('full_name'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Email <span class="star">*</span></label>
                      <div class="col-md-6">
                         <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $host_arr[0]['hosts_email'];?>" data-rule-required="true" readonly="true" />
                       </div>
                   </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Company Name <span class="star">*</span></label>
                      <div class="col-md-6">
                         <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Category Name in english" value="<?php echo $host_arr[0]['hosts_company'];?>" data-rule-required="true" />
                         <?php echo form_error('company_name'); ?>
                         <div class="error_msg" id="error_company_name" style="display:none;"></div>
                      </div>
                   </div>
                   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Profile Image</label>
                      <div class="col-sm-9 col-md-6 controls">
                         <div class="fileupload fileupload-new" data-provides="fileupload">
                            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                              <?php if(isset($host_arr[0]['host_profile']) && file_exists('uploads/profile/'.$host_arr[0]['host_profile']))
                              { ?>
                                <img src="<?php echo base_url(); ?>uploads/profile/<?php echo $host_arr[0]['host_profile'];?>" alt="" />

                              <?php }else { ?>
                                
                                <img src="<?php echo base_url(); ?>images/noimage.jpg" alt="" style="width: 200px; height: 150px;"/>
                             <?php }?>
                            </div>
                            <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                          
                            <div>
                               <span class="btn btn-file">
                               <span class="fileupload-new">Select image</span>
                               <span class="fileupload-exists">Change</span>
                               <input type="file" class="default" name="profile_image" id="profile_image"  accept="image/*" /> 
                               <input type="hidden" class="default" name="old_profile_image" id="old_profile_image" value="<?php echo $host_arr[0]['host_profile'];?>"/>
                               </span>

                               <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                           <div class="clearfix"></div> 
                         </div>
                         <span class="label label-important">NOTE!</span>
                         <span>Please select only png,jpg,gif file extensions.</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Types <span class="star">*</span></label>
                      <div class="col-md-6">
                         <select id="mainategory_id" multiple="multiple" data-rule-required="true" name="mainategory_id[]" class="form-control">
                           <?php $main_cat_arr = array();
                            $main_cat_arr = explode(',',$host_arr[0]['hosts_mainategory_id'].',');
                           if(count($categories)>0){ 
                             foreach($categories as $cat)
                              {
                                ?>
                                <option <?php if(in_array($cat['category_id'], $main_cat_arr)) { echo'selected="selected"'; } ?> value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name_eng'] ?></option>
                                <?php } } ?>
                         </select>
                       </div>
                   </div>
                 
                   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Description </label>
                      <div class="col-md-6">
                         <textarea class="form-control" name="description" id="description" placeholder="Description" value="" data-rule-required="true" ><?php echo $host_arr[0]['hosts_description'];?></textarea>
                         <div class="error_msg"><?php echo form_error('description'); ?></div>
                       </div>
                   </div> 
                   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Address <span class="star">*</span></label>
                      <div class="col-md-6">
                         <textarea class="form-control" name="address" id="address" placeholder="Address" value="" data-rule-required="true" ><?php echo $host_arr[0]['hosts_address'];?></textarea>
                         <div class="error_msg" id="error_company_name"><?php echo form_error('address'); ?></div>
                       </div>
                   </div> 
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Website <span class="star">*</span></label>
                      <div class="col-md-6">
                         <input type="text" class="form-control" name="website" id="website" placeholder="Website" value="<?php echo $host_arr[0]['hosts_website'];?>" data-rule-required="true" data-rule-url="true">
                        <div class="error_msg"> <?php echo form_error('website'); ?></div>
                       </div>
                   </div>  
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Contact Number <span class="star">*</span></label>
                      <div class="col-md-6">
                         <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact Number" value="<?php echo $host_arr[0]['hosts_contact'];?>" data-rule-required="true"/>
                         <div class="error_msg"><?php echo form_error('contact_no'); ?></div>
                       </div>
                   </div>   
                    <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Position <span class="star">*</span></label>
                      <div class="col-md-6">
                         <input type="text" class="form-control" name="position" id="position" placeholder="Position" data-rule-required="true" value="<?php echo $host_arr[0]['hosts_position'];?>" />
                         <div class="error_msg"><?php echo form_error('position'); ?></div>
                       </div>
                   </div>    
                   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Mobile Number <span class="star">*</span></label>
                      <div class="col-md-6">
                         <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="Mobile Number" value="<?php echo $host_arr[0]['hosts_mobile'];?>" data-rule-required="true"/>
                         <div class="error_msg"><?php echo form_error('mobile_no'); ?></div>
                       </div>
                   </div>     
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Preferred Payment <span class="star">*</span></label>
                      <div class="col-md-6">
                         <select name="payment_type" id="payment_type" class="form-control">
                          <option value="">-Select Payment-</option>
                          <option <?php if($host_arr[0]['hosts_preferred_payment']=='Paypal') { echo'selected="selected"'; } ?> value="Paypal">Paypal</option>
                          <option <?php if($host_arr[0]['hosts_preferred_payment']=='Bank') { echo'selected="selected"'; } ?> value="Bank">Bank</option>
                         </select>
                         <div class="error_msg"><?php echo form_error('payment_type'); ?></div>
                       </div>
                   </div>      
                   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Preferred Currency <span class="star">*</span></label>
                      <div class="col-md-6">
                         <select name="currency_type" id="currency_type" class="form-control">
                          <option value="">-Select Currency-</option>
                          <?php $lang=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
                           if(count($lang)>0)
                           {
                            foreach($lang as $row)
                            {
                            ?>
                                             <option <?php if($host_arr[0]['hosts_preferred_currency']==$row['currency_id']) { echo'selected="selected"'; } ?> value="<?php echo $row['currency_id']; ?>"><?php echo $row['currency_eng']; ?></option>
                                           <?php
                             }
                           }
                          ?>
                         </select>
                         <div class="error_msg"><?php echo form_error('currency_type'); ?></div>
                       </div>
                   </div>       
                   <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Submit" class="btn btn-primary" name="btn_edit_host" id="btn_edit_host">
                     </div>
                   </div>
               </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
