 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/bootstrap-fileupload/bootstrap-fileupload.css" />
                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Account Setting</h1>
                        <h4>Account Setting</h4>
                    </div>
                </div>
                <!-- END Page Title -->
                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo base_url(); ?>superadmin/admin/dashboard/">Home</a>
                            <span class="divider"><i class="fa fa-angle-right"></i></span>
                        </li>
                        <li class="active">Account Setting</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->
                <!-- BEGIN Main Content -->
                

                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-pink">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Edit Profile</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                            <div class="form-group">
								<?php 
                                if($error!=''){  ?>
                                <div class="alert alert-danger"><?php echo $error; ?></div>
                                <?php } 
                                if($success!=''){?>	
                                <div class="alert alert-success"><?php echo $success; ?></div>
                                <?php } ?>
                            </div>
                                <form  id="validation-form" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <div class="form-group">
                                      <label class="col-sm-3 col-lg-2 control-label">Image Upload</label>
                                      <div class="col-sm-9 col-md-10 controls">
                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                               <img src="<?php echo base_url(); ?>uploads/admin/<?php echo  $result[0]['admin_img'] ;?>" alt="" />
                                            </div>
                                            <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                            <div>
                                               <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                               <span class="fileupload-exists">Change</span>
                                               <input type="file" class="default" name="file_upload" id="file_upload"  accept="image/*"	/>
                                               <input type="hidden" class="default" name="old_image" id="old_image" value="<?php echo  $result[0]['admin_img'] ;?>"/>
                                               </span>
                                               <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                         </div>
                                         <span class="label label-important">NOTE!</span>
                                         <span>Attached image img-thumbnail is supported in Latest Firefox, Chrome, Opera, Safari and Internet Explorer 10 only</span>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                    
                                        <label class="col-sm-3 col-lg-2 control-label">Username<span>*</span></label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="<?php echo $result[0]['admin_username'] ?>" class="form-control" name="username" id="username" data-rule-required="true" />
                                            <!--<span class="help-inline"><a href="#">Request new ?</a></span>-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Email<span>*</span></label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="<?php echo $result[0]['admin_email'] ?>" class="form-control" name="email" id="email" data-rule-required="true" data-rule-email="true"/>
                                        </div>
                                    </div>
                                  
                                  <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Phone<span>*</span></label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="<?php echo $result[0]['admin_phone'] ?>" class="form-control" name="phone" id="phone" data-rule-required="true" data-rule-number="true"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Fax<span>*</span></label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <input type="text" value="<?php echo $result[0]['admin_fax'] ?>" class="form-control" name="fax" id="fax" data-rule-required="true" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 col-lg-2 control-label">Address<span>*</span></label>
                                        <div class="col-sm-9 col-lg-10 controls">
                                            <textarea  class="form-control" name="address" id="address" data-rule-required="true"/><?php echo $result[0]['admin_address'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                            <button type="submit" class="btn btn-primary" name="btn_account" id="btn_account">Submit</button>
                                            <button type="reset" class="btn">Cancel</button>
                                        </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-orange">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Change Password</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                            <div class="form-group">
								<?php 
                                if($error1!=''){  ?>
                                <div class="alert alert-danger"><?php echo $error1; ?></div>
                                <?php } 
                                if($success1!=''){?>	
                                <div class="alert alert-success"><?php echo $success1; ?></div>
                                <?php } ?>
                            </div>
                                <form method="post" class="form-horizontal" id="validation-form-password">
                                    <div class="form-group">
                                        <label class="col-sm-4 col-md-5 control-label">Current password<span>*</span></label>
                                        <div class="col-sm-8 col-md-7 controls">
                                            <input type="password" class="form-control" name="current_pass" id="current_pass" data-rule-minlength="6" data-rule-required="true" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-md-5 control-label">New password<span>*</span></label>
                                        <div class="col-sm-8 col-md-7 controls">
                                            <input type="password" class="form-control" data-rule-minlength="6" data-rule-required="true" name="new_pass" id="new_pass" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 col-md-5 control-label">Re-type new password<span>*</span></label>
                                        <div class="col-sm-8 col-md-7 controls">
                                            <input type="password" class="form-control" data-rule-minlength="6" data-rule-required="true" name="confirm_pass" id="confirm_pass" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-5">
                                            <button type="submit" class="btn btn-primary" name="btn_password" id="btn_password">Submit</button>
                                            <button type="button" class="btn">Cancel</button>
                                        </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box box-red">
                            <div class="box-title">
                                <h3><i class="fa fa-file"></i> Site Status</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                            <div class="form-group">
								<?php 
                                if($error2!=''){  ?>
                                <div class="alert alert-danger"><?php echo $error2; ?></div>
                                <?php } 
                                if($success2!=''){?>	
                                <div class="alert alert-success"><?php echo $success2; ?></div>
                                <?php } ?>
                            </div>
                                <form method="post" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="col-sm-3 col-lg-2 control-label">Site Status</label>
                                       <div class="col-sm-9 col-lg-10 controls">
                                          <label class="radio">
                                              <input type="radio" name="site_status" value="1" <?php if($status[0]['site_status']=='1') { echo 'checked="checked"'; } ?>  /> Online
                                          </label>
                                          <label class="radio">
                                              <input type="radio" name="site_status" value="0" <?php if($status[0]['site_status']=='0') { echo 'checked="checked"'; } ?>/> Offline
                                          </label>  
                                          
                                       </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-5">
                                            <button type="submit" class="btn btn-primary" name="btn_status" id="btn_status">Submit</button>
                                            <button type="button" class="btn">Cancel</button>
                                        </div>
                                   </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	 $('#validation-form-password').validate();
	});
</script>
