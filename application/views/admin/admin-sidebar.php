<!-- BEGIN Sidebar -->
<div id="sidebar" class="navbar-collapse collapse"> 
  <!--BEGIN Navlist -->
  <ul class="nav nav-list">
    <!-- BEGIN Search Form -->
    <li>
      <form target="#" method="GET" class="search-form">
        <span class="search-pan">
        <button type="submit"> <i class="fa fa-search"></i> </button>
        <input type="text" name="search" placeholder="Search ..." autocomplete="off" />
        </span>
      </form>
    </li>
    <!-- END Search Form -->
    <li <?php if($this->router->fetch_method()=='dashboard'){?> class="active" <?php }?>> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>"> <i class="fa fa-dashboard"></i> <span>Dashboard</span> </a> </li>
    <li <?php if($this->router->fetch_method()=='accountsetting' || $this->uri->segment(3)=='accountsetting' || $this->uri->segment(3)=='sociallink'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Account settings</span> <b class="arrow fa <?php if($this->router->fetch_method()=='accountsetting' ){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_method()=='accountsetting' || $this->uri->segment(3)=='accountsetting' || $this->uri->segment(3)=='sociallink'){?> style="display:block;" <?php }?>>
        <li <?php if($this->uri->segment(3)=='accountsetting'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/admin/accountsetting'; ?>">Setting</a></li>
        <li <?php if($this->uri->segment(3)=='sociallink'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/admin/sociallink'; ?>">Social Links</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='category'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Category</span> <b class="arrow fa <?php if($this->router->fetch_class()=='category'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='category'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='category/addCategory'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/category/addCategory/'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='category/manageCategory'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/category/manageCategory/'; ?>">Manage</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='subcategory'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>sub-category</span> <b class="arrow fa <?php if($this->router->fetch_class()=='subcategory'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='subcategory'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='subcategory/addsubcategory'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/subcategory/add/'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='subcategory/managesubcategory'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/subcategory/manage/'; ?>">Manage</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='testimonial'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Testimonial</span> <b class="arrow fa <?php if($this->router->fetch_class()=='testimonial'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='testimonial'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='testimonial/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/testimonial/add/'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='testimonial/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/testimonial/manage/'; ?>">Manage</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='whychooseus'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Why choose Us</span> <b class="arrow fa <?php if($this->router->fetch_class()=='whychooseus'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='whychooseus'){?> style="display:block;" <?php }?>>
        <!--<li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='testimonial/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/testimonial/add/'; ?>">Add</a></li>-->
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='testimonial/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/whychooseus/manage/'; ?>">Manage</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_method()=='frontpages' || $this->uri->segment(3)=='managefrontpage' || $this->uri->segment(3)=='addfrontpage' || $this->uri->segment(3)=='frontupdate'){?> class="active" <?php }?>> <a href="<?php echo base_url().'superadmin/frontpages/managefrontpage/'; ?>" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Front Pages</span> </a> </li>
    
    <li <?php if($this->router->fetch_class()=='menu'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Menu</span> <b class="arrow fa <?php if($this->router->fetch_class()=='menu'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='menu'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='menu/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/menu/add'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='menu/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/menu/manage/'; ?>">Manage</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='submenu'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Sub Menu</span> <b class="arrow fa <?php if($this->router->fetch_class()=='submenu'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='submenu'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='submenu/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/submenu/add'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='submenu/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/submenu/manage/'; ?>">Manage</a></li>
      </ul>
    </li>
   
    <li <?php if($this->router->fetch_class()=='user'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>User</span> <b class="arrow fa <?php if($this->router->fetch_class()=='user'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='user'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='user/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/user/manage/'; ?>">Manage User</a></li>
      </ul>
    </li>
   <li <?php if($this->router->fetch_class()=='owner'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Hosts Provider</span> <b class="arrow fa <?php if($this->router->fetch_class()=='owner'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='owner'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='host/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/host/manage/'; ?>">Manage Host/Provider</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='accommodations'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Accommodations</span> <b class="arrow fa <?php if($this->router->fetch_class()=='accommodations'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='accommodations'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='accommodations/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/accommodations/manage/'; ?>">Manage Accommodations</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='tours'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Tours</span> <b class="arrow fa <?php if($this->router->fetch_class()=='tours'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='tours'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='tours/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/tours/manage/'; ?>">Manage Tours</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='recreation'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Recreation</span> <b class="arrow fa <?php if($this->router->fetch_class()=='recreation'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='recreation'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='recreation/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/recreation/manage/'; ?>">Manage Recreation</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='adventure'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Adventure</span> <b class="arrow fa <?php if($this->router->fetch_class()=='adventure'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='adventure'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='adventure/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/adventure/manage/'; ?>">Manage Adventure</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='packages'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Packages</span> <b class="arrow fa <?php if($this->router->fetch_class()=='packages'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='packages'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='packages/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/packages/manage/'; ?>">Manage Packages</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='event'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Event</span> <b class="arrow fa <?php if($this->router->fetch_class()=='event'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='event'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='event/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/event/manage/'; ?>">Manage Event</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='coupon'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Coupon</span> <b class="arrow fa <?php if($this->router->fetch_class()=='coupon'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='coupon'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='coupon/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/coupon/manage/'; ?>">Manage Coupon</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='transportation'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Transportation</span> <b class="arrow fa <?php if($this->router->fetch_class()=='transportation'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='transportation'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='transportation/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/transportation/manage/'; ?>">Manage Transportation</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='facilities'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Allowed</span> <b class="arrow fa <?php if($this->router->fetch_class()=='facilities'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='facilities'){?> style="display:block;" <?php }?>>
         <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='facilities/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/facilities/manage/'; ?>">Allowed</a></li>
         <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='facilities/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/facilities/add/'; ?>">Add Allowed</a></li>
    
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='requirement'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Requirement</span> <b class="arrow fa <?php if($this->router->fetch_class()=='requirement'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='requirement'){?> style="display:block;" <?php }?>>
         <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='requirement/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/requirement/manage/'; ?>">Requirement</a></li>
         <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='requirement/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/requirement/add/'; ?>">Add Requirement</a></li>
     </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='included'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Included</span> <b class="arrow fa <?php if($this->router->fetch_class()=='included'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='included'){?> style="display:block;" <?php }?>>
         <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='included/manage'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/included/manage/'; ?>">Included</a></li>
         <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='included/add'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/included/add/'; ?>">Add Included</a></li>
      </ul>
    </li>
    <li <?php if($this->router->fetch_class()=='storycategory'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Story Category</span> <b class="arrow fa <?php if($this->router->fetch_class()=='storycategory'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='storycategory'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='storycategory/addStoryCategory'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/storycategory/addStoryCategory/'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='storycategory/manageStorycategory'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/storycategory/manageStorycategory/'; ?>">Manage</a></li>
      </ul>
    </li>
    
    <li <?php if($this->router->fetch_class()=='storyboard'){?> class="active" <?php }?>> <a href="#" class="dropdown-toggle" > <i class="fa fa-file-o"></i> <span>Story Board</span> <b class="arrow fa <?php if($this->router->fetch_class()=='manageStoryboard'){echo 'fa-angle-down';}else{echo 'fa-angle-right';} ?>"></b> </a>
      <ul class="submenu" <?php if($this->router->fetch_class()=='storyboard'){?> style="display:block;" <?php }?>>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='storyboard/addStoryBoard'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/storyboard/addStoryBoard/'; ?>">Add</a></li>
        <li <?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='storyboard/manageStoryboard'){?> class="active" <?php }?>><a href="<?php echo base_url().'superadmin/storyboard/manageStoryboard/'; ?>">Manage</a></li>
      </ul>
    </li>
  </ul>
  <!-- END Navlist --> 
  <!-- BEGIN Sidebar Collapse Button -->
  <div id="sidebar-collapse" class="visible-lg"> <i class="fa fa-angle-double-left"></i> </div>
  <!-- END Sidebar Collapse Button --> 
</div>

<!-- END Sidebar -->