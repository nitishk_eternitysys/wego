<!-- START: USER PROFILE -->
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3><?php echo $this->lang->line('Welcome');?> <?php echo $fetch_array[0]['user_name']; ?></h3>
			</div>
			
			<div class="col-md-12 col-sm-12">
				<div class="tab-content">
					<div id="booking" class="tab-pane fade in active">
						
						<div class="col-md-12">
							<div class="user-personal-info">
								<h4><?php echo $this->lang->line('Add_Story');?></h4>
								<div class="user-info-body">
									 <form method="post" id="userProfile" enctype="multipart/form-data" action="<?php echo base_url(); ?>user/addStory">
                                     <div class="col-md-12 col-sm-12">
											<label><?php echo $this->lang->line('Category');?></label>
											<select class="form-control" name="categoryID" data-rule-required="true">
												<option value=""><?php echo $this->lang->line('Select_Category');?></option>
												<?php if(count($fetch_category)>0){ 
														foreach($fetch_category as $cat){
												?>
                                                <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name_'.$this->session->userdata('lang')]; ?></option>
                                                <?php } } ?>
											</select>
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Story_Title');?></label>
											<input name="story_title" type="text" class="form-control" placeholder="Story Title" data-rule-required="true">
										</div>
										<div class="clearfix"></div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Description');?></label>
										  <textarea name="story_description" id="story_description" class="form-control" data-rule-required="true"></textarea>
										</div>
										<div class="clearfix"></div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Images');?></label>
										  <input style="padding:0px;" type="file" name="story_images[]" id="story_images" multiple placeholder="Image" data-rule-required="true" />
										</div>
                                        <div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <button type="submit"  name="btn_story" id="btn_story" value="1"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <a href="<?php echo base_url('user/profile'); ?>"><?php echo $this->lang->line('CANCEL');?></a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
            
          
            
		</div>
	</div>
<!-- END: USER PROFILE -->
<!-- END: USER PROFILE -->
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		 jQuery('#userProfile').validate({});
	});
</script>