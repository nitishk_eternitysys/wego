<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>
<!-- START: PAGE TITLE -->
<div class="row page-title page-title1">
	<div class="container clear-padding text-center">
		<h3><?php echo $fetch_array_details[0]['event_title']; ?></h3>
		<h5>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		</h5>
		<p><i class="fa fa-car"></i> <?php echo $fetch_array_details[0]['event_location']; ?></p>
	</div>
</div>
<!-- END: PAGE TITLE -->
<!-- START: ROOM GALLERY -->
<?php $image_array=$this->master_model->getRecords('tbl_event_images',array('event_id'=>$fetch_array_details[0]['event_id'])); ?>
<div class="row hotel-detail">
	<div class="container">
		<div class="main-content col-md-8">
			<div id="room-gallery" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php
						if(count($image_array)){
							$i=0;
						foreach($image_array as $row)
						{
						?>
						<li data-target="#room-gallery" data-slide-to="<?php echo $i; ?>" <?php if ($i==0): ?> class="active" <?php endif; ?>></li>
										 <?php
						$i++;}
						} ?>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php if(count($image_array)){
						$i=0;
					foreach($image_array as $row)
					{ ?>
					<div class="item <?php if ($i==0): ?>active<?php endif; ?>">
						<img src="<?php echo base_url().'uploads/event/'.$row['event_images_name']; ?>" alt="<?php echo $row['event_images_name']; ?>" />
					</div>
				<?php	$i++;}
					} ?>
				</div>
				<a class="left carousel-control" href="#room-gallery" role="button" data-slide="prev">
					<span class="fa fa-chevron-left" aria-hidden="true"></span>
					<span class="sr-only"><?php echo $this->lang->line('Previous');?></span>
				</a>
				<a class="right carousel-control" href="#room-gallery" role="button" data-slide="next">
					<span class="fa fa-chevron-right" aria-hidden="true"></span>
					<span class="sr-only"><?php echo $this->lang->line('Next');?></span>
				</a>
			</div>

			<div class="room-complete-detail">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#overview"><i class="fa fa-bolt"></i> <span><?php echo $this->lang->line('Overview');?></span></a></li>
					<li><a data-toggle="tab" href="#review"><i class="fa fa-comments"></i> <span><?php echo $this->lang->line('Reviews');?></span></a></li>
					<li><a data-toggle="tab" href="#write-review"><i class="fa fa-edit"></i> <span><?php echo $this->lang->line('Write_Review');?></span></a></li>
				</ul>
				<div class="tab-content">
					<div id="overview" class="tab-pane active in fade">
						<h4 class="tab-heading"><?php echo $this->lang->line('Brief_Description_of');?> <?php echo $fetch_array_details[0]['event_title']; ?></h4>
						<p><?php echo $fetch_array_details[0]['event_description']; ?></p>
					</div>
					<div id="review" class="tab-pane fade">
						<h4 class="tab-heading"><?php echo $this->lang->line('REVIEWS');?></h4>
						<div class="review-header">
							<div class="col-md-6 col-sm6 text-center">
								<h2>4.0/5.0</h2>
								<h5><?php echo $this->lang->line('Based');?></h5>
							</div>
							<div class="col-md-6 col-sm-6">
								<table class="table">
									<tr>
										<td><?php echo $this->lang->line('Value_for_Money');?></td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
									<tr>
										<td><?php echo $this->lang->line('Atmosphere_in_hotel');?></td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
									<tr>
										<td><?php echo $this->lang->line('Quality_of_food');?></td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</td>
									</tr>
									<tr>
										<td><?php echo $this->lang->line('Staff_behaviour');?></td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
									<tr>
										<td><?php echo $this->lang->line('Restaurant_Quality');?></td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
							<div class="guest-review">
								<div class="individual-review dark-review">
									<h4> <?php echo $this->lang->line('Best_Place');?><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
									<p><?php echo $this->lang->line('simply_dummy');?></p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user1.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span><?php echo $this->lang->line('Lenore');?></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review">
									<h4> <?php echo $this->lang->line('Best_Place');?><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></h4>
									<p><?php echo $this->lang->line('simply_dummy');?></p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span><?php echo $this->lang->line('Lenore');?></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review dark-review">
									<h4><?php echo $this->lang->line('Best_Place');?> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
									<p><?php echo $this->lang->line('simply_dummy');?></p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user1.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span><?php echo $this->lang->line('Lenore');?></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review">
									<h4><?php echo $this->lang->line('Best_Place');?><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></h4>
									<p><?php echo $this->lang->line('simply_dummy');?></p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span><?php echo $this->lang->line('Lenore');?></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review dark-review">
									<h4><?php echo $this->lang->line('Best_Place');?> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
									<p><?php echo $this->lang->line('simply_dummy');?></p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user1.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span><?php echo $this->lang->line('Lenore');?></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="load-more text-center">
									<a href="#"><?php echo $this->lang->line('Load_More');?></a>
								</div>
							</div>
						</div>
					</div>
					<div id="write-review" class="tab-pane fade">
						<h4 class="tab-heading"><?php echo $this->lang->line('Write_A_Review');?></h4>
						<form >
							<label><?php echo $this->lang->line('Review_Title');?></label>
							<input type="text" class="form-control" name="review-titile" required>
							<label for="comment"><?php echo $this->lang->line('Comment');?></label>
							<textarea class="form-control" rows="5" id="comment"></textarea>
							<label><?php echo $this->lang->line('Value');?></label>
							<input type="text" class="form-control" name="value-for-money">
							<label><?php echo $this->lang->line('Hotel');?></label>
							<input type="text" class="form-control" name="atmosphere">
							<label><?php echo $this->lang->line('Staff');?></label>
							<input type="text" class="form-control" name="staff-beh">
							<label><?php echo $this->lang->line('Food');?></label>
							<input type="text" class="form-control" name="food-quality">
							<label><?php echo $this->lang->line('Rooms');?></label>
							<input type="text" class="form-control" name="room">
							<div class="text-center">
								<button type="submit" class="btn btn-default submit-review"><?php echo $this->lang->line('Submit');?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 hotel-detail-sidebar">
			<div class="col-md-12 sidebar-wrapper clear-padding">
				<div class="contact sidebar-item">
					<h4><i class="fa fa-bookmark"></i> <?php echo $this->lang->line('Package_Summary');?></h4>
					<div class="sidebar-item-body">
						<h5><i class="fa fa-heart"></i><?php echo $this->lang->line('Title');?></h5>
						<p><?php echo $fetch_array_details[0]['event_title']; ?></p>
						<h5><i class="fa fa-calendar"></i><?php echo $this->lang->line('Time');?></h5>
						<p><?php echo $fetch_array_details[0]['event_duration']; ?></p>
						<h5><i class="fa fa-map-marker"></i><?php echo $this->lang->line('Location');?></h5>
						<p><?php echo $fetch_array_details[0]['event_location']; ?></p>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: ROOM GALLERY -->
<script type="text/javascript">
var map;
var markersArray = [];
		function initialize() {
				var myLatlng = new google.maps.LatLng(<?php echo $fetch_array_details[0]['accom_latitude']; ?>,<?php echo $fetch_array_details[0]['accom_longitude']; ?>);
				var myOptions = {
						zoom:7,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map"), myOptions);

				var marker = new google.maps.Marker({
							position: new google.maps.LatLng(<?php echo $fetch_array_details[0]['accom_latitude']; ?>,<?php echo $fetch_array_details[0]['accom_longitude']; ?>),
							map: map
					 });
					 markersArray.push(marker);
}

window.onload = function () { initialize() };
</script>
