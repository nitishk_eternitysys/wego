<link rel="stylesheet" href="<?php echo base_url(); ?>assets/prettyPhoto/css/prettyPhoto.css">
<!-- BEGIN Page Title -->
<div class="page-title">
  <div>
    <h1><i class="fa fa-book"></i><?php echo $pageLable;?></h1>
    <h4><?php echo $pageLable;?></h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li><a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>"><?php echo 'Manage coupon';?></a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
    <li class="active"><?php echo $pageLable;?></li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class='col-md-12'>
    <div class="box">
        <div class="box-title">
            <h3><i class="fa fa-bars"></i> <?php echo $arr_details[0]['coupon_title'];?></h3>	
            <div class="box-tool">
              <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
             <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">coupon Title </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_title'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Provider Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['hosts_name'];?>
                  </div>
               </div>
               <div class="form-group">
                 <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['category_id'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">Category Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($categoryName[0]['category_name_eng'])){echo $categoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>   
               <div class="form-group">
               <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['subcategory_id'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">SubCategory Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     :  <?php if(isset($subcategoryName[0]['category_name_eng'])){echo $subcategoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Location </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_location']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Description </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_description']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">longitude </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_longitude']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">latitude  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_latitude']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Address</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_address']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Name</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_user_name']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Position</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_person_postion']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Email</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_email']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Mobile</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_mobile']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Gender</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['gender']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Age</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['age']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">weight</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['weight']; ?>
                  </div>
               </div>
              
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">what allowed</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_what_allowed']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">not allowed</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['coupon_not_allowed']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">additiona info</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['additiona_info']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['price']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">price unit</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['price_unit']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">cancel policy</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['cancel_policy']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['cancel_policy']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">cancel policy price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['cancel_policy_price']; ?>
                  </div>
               </div>
               <?php } ?>
              
              </form>
          </div>
     </div>
  </div>      
</div>
<!-- END Main Content -->