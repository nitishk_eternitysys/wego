<!-- START: BOOKING TAB -->
<div class="row">
		<div class="container clear-padding">
			<div class="tab-content">
            	<div id="review-booking" class="tab-pane fade in active">
					<div class="col-md-8 col-sm-8">
                      <div class="">
							<h3><?php echo $this->lang->line('Booking_time');?></h3>
							<div class="booking-form">
                                <div class="col-md-12 col-sm-12">
									 <form method="post" name="form_booking" id="form_booking">
                                       <?php 
									    echo '<strong>'.date('d-m-Y',strtotime($fetch_array_details[0]['form_from_time'])).'</strong>'.'<br/>';
									    if(count($fetch_array_details)){ 
									     foreach($fetch_array_details as $row)
										 {
									    ?>
										   <label> <input type="radio" name="availability" id="availability<?php echo $row['availableID']; ?>" value="<?php echo $date.' '.date('H:i:s',strtotime($row['form_from_time'])).' - '.$date.' '.date('H:i:s',strtotime($row['form_to_time'])); ?>"> <?php echo date('H:i:s',strtotime($row['form_from_time'])).' - '.date('H:i:s',strtotime($row['form_to_time'])); ?> </label> <div class="clearboth"></div>
                                        <?php 
										 }
										} ?>
                                        <div class="error"></div>
                                        <button type="submit" name="btn_booking" id="btn_booking" value="1"><?php echo $this->lang->line('Booking');?></button>
									</form>
								</div>
                            </div>
						</div>
                     </div>
				</div>
            </div>
		</div>
	</div>
<!-- END: BOOKING TAB -->
<script type="application/javascript">
$(document).ready(function(){
	   $('#form_booking').submit(function(){
			if($('input[name=availability]:checked').length<=0)
			{
			   $('.error').html("Please select the radio button");
			   return false;
			}
		});
});
</script>