<!-- START: USER PROFILE -->
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3><?php echo $this->lang->line('Welcome');?> <?php echo $fetch_array[0]['user_name']; ?></h3>
			</div>
			
			<div class="col-md-12 col-sm-12">
				<div class="tab-content">
					<div id="booking" class="tab-pane fade in active">
						
						<div class="col-md-12">
							<div class="user-personal-info">
								<h4><?php echo $this->lang->line('Edit_Story');?></h4>
								<div class="user-info-body">
									 <form method="post" id="userProfile" enctype="multipart/form-data">
                                     <div class="col-md-12 col-sm-12">
											<label><?php echo $this->lang->line('Category');?></label>
											<select class="form-control" name="categoryID" data-rule-required="true">
												<option value=""><?php echo $this->lang->line('Select_Category');?></option>
												<?php if(count($fetch_category)>0){ 
														foreach($fetch_category as $cat){
												?>
                                                <option value="<?php echo $cat['category_id']; ?>" <?php if($fetch_story[0]['categoryID']==$cat['category_id']){ ?> selected="selected" <?php } ?>><?php echo $cat['category_name_'.$this->session->userdata('lang')]; ?></option>
                                                <?php } } ?>
											</select>
										</div>
										<div class="col-md-12">
											<label><?php echo $this->lang->line('Story_Title');?></label>
											<input name="story_title" type="text" class="form-control" placeholder="Story Title" data-rule-required="true" value="<?php echo $fetch_story[0]['story_title']; ?>">
										</div>
										<div class="clearfix"></div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Description');?></label>
										  <textarea name="story_description" id="story_description" class="form-control" data-rule-required="true"><?php echo $fetch_story[0]['story_description']; ?></textarea>
										</div>
										<div class="clearfix"></div>
                                        <div class="col-md-12">
										  <label><?php echo $this->lang->line('Images');?></label>
										  <input style="padding:0px;" type="file" name="story_images[]" id="story_images" multiple placeholder="Image" />
										</div>
                                        <div class="clearfix"></div>
                                          <div class="image-set gallery-box">
                                              <?php 
                                              if(count($fetch_story_img))
                                              {
                                                  foreach($fetch_story_img as $row)
                                                  {
                                            ?>    <div class="col-md-3 col-sm-4">
                                                    <div class="image-wrapper">
                                                      <img src="<?php  echo base_url().'uploads/story/'.$row['story_images']; ?>" width="175" height="144">
                                                      <div class="img-caption">
                                                        <div class="link">
                                                            <a href="javascript:void(0);" class="deletelinkstory" rel="story" data-id="<?php echo $row['storyImgID'];?>"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>      
                                                    </div>
                                                 </div>
                                              <?php
                                                  }
                                              }
                                              ?>
                                             </div>
                                        <div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <button type="submit"  name="btn_story" id="btn_story"><?php echo $this->lang->line('SAVE_CHANGES');?></button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <a href="<?php echo base_url('user/profile'); ?>"><?php echo $this->lang->line('CANCEL');?></a>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
            
          
            
		</div>
	</div>
<!-- END: USER PROFILE -->
<!-- END: USER PROFILE -->
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		 jQuery('#userProfile').validate({});
		 $(document).on('click','.deletelinkstory',function(){
			var type=$(this).attr('rel');
			var delete_id= $(this).attr('data-id');
			var current =$(this);
			  $.ajax({
						type: "POST",
						data: { type : type, delete_id : delete_id},
						url: siteurl+"user/deleteimage/",
						beforeSend: function(){
							  current.closest('.col-md-3').css('opacity','0.1');
							},
						success: function(result) {
							current.closest('.col-md-3').css('opacity','1');
							if(result=='success')
							{
							  current.closest('.col-md-3').remove();
							}
						  }
					}); 
		  
		  });
	});
</script>