<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3><?php echo $this->lang->line('payment_Cancel');?></h3>
			<h4 class="thank"><i class="fa fa-thumbs-o-down"></i><?php echo $this->lang->line('Sorry_your');?></h4>
			<!--<span><i class="fa fa-plane"></i> New York <i class="fa fa-long-arrow-right"></i> New Delhi <i class="fa fa-calendar"></i> SAT, 22 JUL</span>-->
		</div>
	</div>
	<!-- END: PAGE TITLE -->
	
	<!-- START: BOOKING DETAILS -->
	<div class="row">
		<div class="container clear-padding">
			<div>
				<div class="col-md-8 col-sm-8">
					<div class=" confirmation-detail">
						<h3><?php echo $this->lang->line('Booking_Cancelled');?></h3>
						<p><?php echo $this->lang->line('Sorry_your');?></p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 booking-sidebar">
					<div class="sidebar-item contact-box">
						<h4><i class="fa fa-phone"></i><?php echo $this->lang->line('Need_Help');?></h4>
						<div class="sidebar-body text-center">
							<p><?php echo $this->lang->line('Call_us');?></p>
							<h2>+91 1234567890</h2>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- END: BOOKING DETAILS -->
