<!-- START: PAGE TITLE -->
<div class="row page-title">
    <div class="container clear-padding text-center">
        <h3><?php echo $fetch_story[0]['story_title']; ?></h3>
        <p><i class="fa fa-map-marker"></i><?php echo $fetch_story[0]['category_name_'.$this->session->userdata('lang')]; ?></p>
    </div>
</div>
<!-- END: PAGE TITLE -->
<!-- START: ROOM GALLERY -->
<?php $image_array=$this->master_model->getRecords('tbl_story_images',array('storyID'=>$fetch_story[0]['storyID'])); ?>
<div class="row hotel-detail">
		<div class="container">
			<div class="product-brief-info">
				<div class="col-md-8 clear-padding">
					<div id="slider" class="flexslider">
						<ul class="slides">
                            <?php
							if(count($image_array)){
							 foreach($image_array as $row)
							 {
							?>
							 <li><img src="<?php echo base_url().'uploads/story/'.$row['story_images']; ?>" alt="<?php echo $row['story_images']; ?>" /></li>
                           <?php
							 }
						   } ?>
						</ul>
					</div>
					<div id="carousel" class="flexslider">
						<ul class="slides">
						<?php
							if(count($image_array)){
							 foreach($image_array as $row)
							 {
							?>
							 <li><img src="<?php echo base_url().'uploads/story/'.$row['story_images']; ?>" alt="<?php echo $row['story_images']; ?>" /></li>
                           <?php
							 }
						   } ?>
						</ul>
					</div>
				</div>
				<div class="col-md-4 package-detail-sidebar">
                      <div class="col-md-12 sidebar-wrapper clear-padding">
                                <div class="package-summary sidebar-item">
                                    <h4><i class="fa fa-bookmark"></i> Story Summary</h4>
                                    <div class="package-summary-body">
                                        <h5><i class="fa fa-heart"></i>Title</h5>
                                        <p><?php echo $fetch_story[0]['story_title']; ?></p>
                                        <h5><i class="fa fa-user"></i>User</h5>
                                        <?php 
					if($fetch_story[0]['userID']!='0')
					{
						$user=$this->master_model->getRecords('tbl_user_master',array('user_id'=>$fetch_story[0]['userID']),'user_name');
						$userName=$user[0]['user_name'];
					}
					else
					{
						$userName='Admin';
					}
										?>
                                        <p><?php echo $userName; ?></p>
                                        <h5><i class="fa fa-calendar"></i>Date</h5>
                                        <p><?php echo date('d M Y',strtotime($fetch_story[0]['dateAdded'])); ?></p>
                                    </div>
                                    
                                </div>
                            </div>
				</div>
			</div>
		</div>
</div>
<div class="row product-complete-info">
		<div class="container">
			<div class="col-md-8 main-content">
				<div class="room-complete-detail custom-tabs">
					<ul class="nav nav-tabs">
						<li class="col-md-2 col-sm-2 col-xs-2 active text-center"><a data-toggle="tab" href="#overview"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
						
					</ul>
					<div class="tab-content">
						<div id="overview" class="tab-pane fadein active in">
							<h4 class="tab-heading">About <?php echo $fetch_story[0]['story_title']; ?></h4>
							<p><?php echo $fetch_story[0]['story_description']; ?></p>
                              
						</div>
						
					</div>
				</div>
			</div>
			
		</div>
	</div>
<!-- END: ROOM GALLERY -->

