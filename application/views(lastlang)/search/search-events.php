<div class="col-md-4 col-sm-6 search-col-padding">
    <label>Types</label>
    <select class="form-control"  name="category" id="category6" rel="6"  >
            <option value=""><?php echo 'Select category'; ?></option>
            <?php
			$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'6','parent_id'=>'0','category_status'=>'active')); 
                    if(count($category_accom)>0)
                    {
                        foreach($category_accom as $row)
                        {
							$selected='';
							if(isset($_GET['category']))
							{
								if($row['category_id']==$_GET['category'])
								{
									$selected="selected='selected'";
								}
							}
							?>
                             <option value="<?php echo $row['category_id']; ?>" <?php echo $selected;?>><?php echo $row['category_name_eng']; ?></option>
                            <?php
						}
					}
			?>
     </select>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="destination" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['destination']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
    </div>
</div>