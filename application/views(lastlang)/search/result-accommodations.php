<script type="text/javascript" src="<?php echo base_url('js/jquery.quick.pagination.js') ?>"></script>
<style type="text/css">
.marker-style {
  border: 3px solid #1396e2;
  text-align: center;
  width: 34px;
  height: 34px;
  border-radius: 50%;
  margin-left: -17px !important;
  margin-top: -46px !important;
}
.marker-style img {
  position: absolute !important;
  top: -1px !important;
  bottom: 0px !important;
  right: 0px;
  left: 0px;
  margin: auto !important;
}
.cluster > div::before {
    background-color: #1396e2;
    border-radius: 50%;
    bottom: 0;
    content: "";
    height: 31px;
    left: 0;
    margin: auto;
    position: absolute;
    right: 0;
    top: 0;
    width: 31px;
    z-index: -1;
}
.cluster > div {
    color: #fff !important;
    text-align: center !important;
    z-index: 3;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
        $(".paginationsearch").quickPagination({pageSize:"10"});
		<?php if($view=='map'){  ?>
		initialize();
		<?php } ?>
});
var mapStyles = [{featureType:'water',elementType:'all',stylers:[{hue:'#d7ebef'},{saturation:-5},{lightness:54},{visibility:'on'}]},{featureType:'landscape',elementType:'all',stylers:[{hue:'#eceae6'},{saturation:-49},{lightness:22},{visibility:'on'}]},{featureType:'poi.park',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-81},{lightness:34},{visibility:'on'}]},{featureType:'poi.medical',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-80},{lightness:-2},{visibility:'on'}]},{featureType:'poi.school',elementType:'all',stylers:[{hue:'#c8c6c3'},{saturation:-91},{lightness:-7},{visibility:'on'}]},{featureType:'landscape.natural',elementType:'all',stylers:[{hue:'#c8c6c3'},{saturation:-71},{lightness:-18},{visibility:'on'}]},{featureType:'road.highway',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-92},{lightness:60},{visibility:'on'}]},{featureType:'poi',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-81},{lightness:34},{visibility:'on'}]},{featureType:'road.arterial',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-92},{lightness:37},{visibility:'on'}]},{featureType:'transit',elementType:'geometry',stylers:[{hue:'#c8c6c3'},{saturation:4},{lightness:10},{visibility:'on'}]}];
function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
		styles: mapStyles
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
	<?php $i=1; if($searchData){
		foreach($searchData as $res){?>
	['<?php echo $res['accom_title']; ?>', <?php echo $res['accom_location_map']; ?>]<?php if(count($searchData)!=$i) { ?>,<?php }?>
	<?php $i++; } } ?>	
    ];
                        
    // Info Window Content
    var infoWindowContent = [
	<?php $i=1; if($searchData){
		foreach($searchData as $res){?>
        ['<div class="info_content">' +
        '<h3><?php echo $res['accom_title']; ?></h3>' +
        '<p><?php echo $res['accom_location_address']; ?></p>' + '</div>']<?php if(count($searchData)!=$i) { ?>,<?php }?>
		<?php $i++; } } ?>
       
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
	var markersArr = [];
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
			icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + ([i+1]) + '|FF776B|000000',
			//labelContent: 'terte',
			labelAnchor: new google.maps.Point(50, 0),
			labelClass: "marker-style"
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
		markersArr.push(marker);

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

	var clusterStyles = [
		{
			url: '<?php echo base_url(); ?>/images/cluster.png',
			height: 37,
			width: 37
		}
        ];
        var markerCluster = new MarkerClusterer(map, markersArr, {styles: clusterStyles, maxZoom: 15});
    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
   /* var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(7);
        google.maps.event.removeListener(boundsListener);
    });*/
    
}
</script>
<div id="searchpagination" class="paginationsearch">
<?php 
if($view=='list'){
if($searchData){ 
		foreach($searchData as $res){
			$host= $this->master_model->getRecords('tbl_hosts_master',array('hosts_id'=>$res['hosts_id']));
			?>
	<div  class="hotel-list-view">
       <?php if($host[0]['hosts_verify']=='yes'){ ?>
        <div class="certified-box"><img src="<?php echo base_url();?>front/images/certified.png" alt=""></div>
        <?php } ?>
		<div class="wrapper">
			<div class="col-md-4 col-sm-6 switch-img clear-padding">
            	<?php if($res['accom_image']!=''){ ?>
				<img src="<?php echo $this->master_model->resize($res['accom_image'],500,350,'uploads/accom/');?>" alt="<?php echo $res['accom_title']; ?>">
                <?php } else { ?>
                <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $res['accom_title']; ?>">
                <?php } ?>
			</div>
			<div class="col-md-6 col-sm-6 hotel-info">
				<div>
					<div class="hotel-header">
						<h5><?php echo $res['accom_title'.$this->session->userdata('form_lang').'']; ?> <span>
                         <?php 
						  $rating=$this->master_model->rating($res['accom_id']);
						  echo $rating['star'];
						  ?>
                        </span></h5>
						<p><i class="fa fa-map-marker"></i><?php echo $res['accom_location_address']; ?></p>
					</div>
				    <div class="hotel-desc">
						<p><?php echo substr($res['accom_description'.$this->session->userdata('form_lang').''],0,70); ?></p>
					</div>
                    <div class="ammenties-2">
                       <p>
                        	<?php if($res['accom_swimming_pool']>0){ ?>
                            <i class="fa fa-life-ring"  style="background-color:#00adef;" title="Swimming pool"></i>
                            <?php }else{ ?>
							<i class="fa fa-life-ring" title="Swimming pool"></i>
							<?php }?>
                            <?php if($res['accom_tv']=='yes'){ ?>
                            <i class="fa fa-desktop" style="background-color:#00adef;" title="Television"></i>
                            <?php }else{?>
							 <i class="fa fa-desktop" title="Television"></i>
							<?php	} ?>
                            <?php if($res['accom_playstation']=='yes'){ ?>
                            <i class="fa fa-gamepad" style="background-color:#00adef;" title="Playstation"></i>
                            <?php }else{ ?>
                            <i class="fa fa-gamepad" title="Playstation"></i>
                            <?php } ?>
                            <?php if($res['accom_wifi']=='yes'){ ?>
                            <i class="fa fa-wifi"  style="background-color:#00adef;" title="Wifi"></i>
                            <?php }else{ ?>
                             <i class="fa fa-wifi" title="Wifi"></i>
                            <?php } ?>
                            <?php if($res['accom_dvd']=='yes'){ ?>
                            <i class="fa fa-dot-circle-o" style="background-color:#00adef;" title="DVD"></i>
                            <?php }else{ ?>
                             <i class="fa fa-dot-circle-o" title="DVD"></i>
                            <?php } ?>
                            <?php if($res['accom_gym']=='yes'){ ?>
                            <i class="fa fa-male" title="gym"></i>
                            <?php }else{ ?>
                            <i class="fa fa-male" style="background-color:#00adef;" title="gym"></i>
                            <?php } ?>
                        </p>
                    </div>
				</div>
			</div>
			<div class="clearfix visible-sm-block"></div>
			<div class="col-md-2 rating-price-box text-center clear-padding">
				<div class="rating-box">
					<div class="tripadvisor-rating">
						<img src="<?php echo base_url(); ?>assets/images/tripadvisor.png" alt="cruise"><span>4.5/5.0</span>
					</div>
					<div class="user-rating">
						 <?php 
						 $rating=$this->master_model->rating($res['accom_id']);
						  echo $rating['star'];
						  ?>
						<span><?php echo $rating['avg']; ?>  Reviews.</span>
					</div>
				</div>
				<div class="room-book-box">
					<div class="price">
						<h5>$<?php echo $res['accom_price']; ?>/Person</h5>
					</div>
					<div class="book">
						<a href="<?php echo base_url().'details/accodetails/'.base64_encode($res['accom_id']);?>">BOOK</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix visible-sm-block"></div>
<?php } } else { ?>
No record(s) found.
<?php } ?>
<?php } else if($view=='grid'){
	if($searchData){ 
		foreach($searchData as $res){
	 ?>
	<div class="col-md-4 col-sm-6">
				<div class="holiday-grid-view">
					<div class="holiday-header-wrapper">
						<div class="holiday-header">
							<div class="holiday-img">
							<?php if($res['accom_image']!=''){ ?>
                            <img src="<?php echo $this->master_model->resize($res['accom_image'],500,350,'uploads/accom/');?>" alt="<?php echo $res['accom_title']; ?>">
                            <?php } else { ?>
                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $res['accom_title']; ?>">
                            <?php } ?>
							</div>
							<div class="holiday-price">
								<h4>$<?php echo $res['accom_price']; ?></h4>
								<h5>/Person</h5>
							</div>
							<div class="holiday-title">
								<h3><?php echo $res['accom_title'.$this->session->userdata('form_lang').'']; ?></h3>
								<h5><?php echo $res['accom_location']; ?></h5>
							</div>
						</div>
					</div>
					<div class="holiday-details">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Inclusion</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p>
						    <?php if($res['accom_swimming_pool']>0){ ?>
                            <i class="fa fa-life-ring"  style="color:#00adef;" title="Swimming pool"></i>
                            <?php }else{ ?>
							<i class="fa fa-life-ring" title="Swimming pool"></i>
							<?php }?>
                            <?php if($res['accom_tv']=='yes'){ ?>
                            <i class="fa fa-desktop" style="color:#00adef;" title="Television"></i>
                            <?php }else{?>
							 <i class="fa fa-desktop" title="Television"></i>
							<?php	} ?>
                            <?php if($res['accom_playstation']=='yes'){ ?>
                            <i class="fa fa-gamepad" style="color:#00adef;" title="Playstation"></i>
                            <?php }else{ ?>
                            <i class="fa fa-gamepad" title="Playstation"></i>
                            <?php } ?>
                            <?php if($res['accom_wifi']=='yes'){ ?>
                            <i class="fa fa-wifi"  style="color:#00adef;" title="Wifi"></i>
                            <?php }else{ ?>
                             <i class="fa fa-wifi" title="Wifi"></i>
                            <?php } ?>
                            <?php if($res['accom_dvd']=='yes'){ ?>
                            <i class="fa fa-dot-circle-o" style="color:#00adef;" title="DVD"></i>
                            <?php }else{ ?>
                             <i class="fa fa-dot-circle-o" title="DVD"></i>
                            <?php } ?>
                            <?php if($res['accom_gym']=='yes'){ ?>
                            <i class="fa fa-male" title="gym"></i>
                            <?php }else{ ?>
                            <i class="fa fa-male" style="color:#00adef;" title="gym"></i>
                            <?php } ?>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Highlight</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p class="sm-text"><?php echo $res['accom_location']; ?></p>
						</div>
					</div>
					<div class="holiday-footer text-center">
						<div class="col-md-8 col-sm-8 col-xs-8 view-detail">
							<a href="<?php echo base_url().'details/accodetails/'.base64_encode($res['accom_id']);?>">VIEW DETAILS</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 social">
							<i class="fa fa-heart-o"></i>
							<i class="fa fa-share-alt"></i>
						</div>
					</div>
				</div>
			</div>
            <?php } } ?>
<?php } else if($view=='map'){ ?>
<div  class="hotel-list-view">
    <div class="wrapper">
        <div id="gmap" style="width:100%;height:500px;"></div>
    </div>
</div>
<?php } ?>
</div>