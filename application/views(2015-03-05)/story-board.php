<!-- BEGIN: PAGE TITLE SECTION -->
<section>
	<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3>Story Board</h3>
			<h4 class="thank">Latest Stories</h4>
		</div>
	</div>
	<!-- END: PAGE TITLE -->
</section>
<!-- END: PAGE TITLE SECTION -->

<!-- BEGIN: CONTENT SECTION -->
<section>	
	<!-- START: POST LISTING -->
	<div class="row">
		<div class="container clear-padding">
			<div>
				<div class="col-md-8 col-sm-8">
                 <?php if($fetch_story){ 
						foreach($fetch_story as $res){
							$fetch_story_img=$this->master_model->getRecords('tbl_story_images',array('storyID'=>$res['storyID']));
							if($res['userID']!='0')
						   {
							   $user=$this->master_model->getRecords('tbl_user_master',array('user_id'=>$res['userID']),'user_name');
							   $userName=$user[0]['user_name'];
						   }
						   else
						   {
							   $userName='Admin';
						   }
							?>
					<div class="post-wrapper">
                    
						<img src="<?php echo $this->master_model->resize($fetch_story_img[0]['story_images'],800,500,'uploads/story/');?>" alt="<?php echo $res['story_title']; ?>">
						<div class="post-body">
							<h3><?php echo $res['story_title']; ?></h3>
							<p><i class="fa fa-tags"></i><a href="#"><?php echo $res['category_name_'.$this->session->userdata('lang')]; ?></a></p>
							<p><?php echo substr($res['story_description'],0,350).' ...'; ?></p>
						</div>
						<div class="post-footer">
							<div class="col-md-6 col-sm-6 col-xs-6">
								<h5><?php echo date('d M Y',strtotime($res['dateAdded'])); ?> By <a href="#"><?php echo $userName; ?></a></h5>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 read-more clear-padding">
								<a href="<?php echo base_url(); ?>home/storyDetails/<?php echo base64_encode($res['storyID']); ?>" class="pull-right">READ MORE</a>
							</div>
						</div>
					</div>
                    <?php } } else { ?>
                    <div class="text-center">
						No record(s) found.
					</div>
                <?php } ?>
					
					
				</div>
				<div class="col-md-4 col-sm-4 booking-sidebar">
					<div class="sidebar-item post-cat">
						<h4><i class="fa fa-bookmark"></i>Categories</h4>
						<div class="sidebar-body">
							<ul class="list-group">
                            <?php if(count($fetch_category)>0){ 
									foreach($fetch_category as $cat){
										$res=$this->master_model->getRecordCount('tbl_story_board',array('categoryID'=>$cat['category_id'],'story_status'=>'active'));
							?>
								<li class="list-group-item <?php if(base64_encode($cat['category_id'])==$this->uri->segment(3)){ echo 'active'; } ?>">
									<span class="badge"><?php echo $res; ?></span>
									<a href="<?php echo base_url(); ?>home/storyBoard/<?php echo base64_encode($cat['category_id']); ?>"><?php echo ucfirst($cat['category_name_'.$this->session->userdata('lang')]); ?></a>
								</li>
								<?php } } ?> 
                                <li class="list-group-item"><a href="<?php echo base_url(); ?>home/storyBoard/">Clear Filter</a></li>
							</ul>
						</div>
					</div>
					<div class="sidebar-item sidebar-subscribe">
						<h4><i class="fa fa-paper-plane"></i>Subscribe</h4>
						<div class="sidebar-body">
							<p>Subscribe to our monthly newletter. We promise we will never send spam.</p>
							<div class="text-center">
								<form >
									<input class="form-control" type="email" required placeholder="Enter Yout Email">
									<button type="submit"><i class="fa fa-paper-plane"></i> Subscribe</button>
								</form>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
	<!-- END: POST LISTING -->
</section>
<!-- END: CONTENT SECTION -->
