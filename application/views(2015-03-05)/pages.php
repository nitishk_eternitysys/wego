<section>
<!-- START: PAGE TITLE -->
<div class="row page-title">
    <div class="container clear-padding text-center flight-title">
        <h3><?php echo $fetch_array[0]['front_page_name_'.$this->session->userdata('lang')] ?></h3>
        <!--<h4>A Few Words About Us</h4>-->
    </div>
</div>
<!-- END: PAGE TITLE -->
</section>
<!-- BEGIN: CONTENT SECTION -->	
<section>
<!-- START: ABOUT-US -->
<div class="row about-intro">
		<div class="container clear-padding">
		  <?php echo $fetch_array[0]['front_page_description_'.$this->session->userdata('lang')] ?>
		</div>
	</div>
<!-- END: ABOUT-US -->
</section>
