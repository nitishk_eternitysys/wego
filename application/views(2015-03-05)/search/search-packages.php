<div class="col-md-3 col-sm-6 search-col-padding">
    <label>Departure</label>
    <div class="input-group">
        <input type="text" value="<?php echo $_GET['location']; ?>" placeholder="E.g. London" class="form-control" name="location" id="packagelocation">
        <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
    </div>
    <div id="packageresults"></div>
</div>
<div class="col-md-3 col-sm-6 search-col-padding">
    <label>From Date </label>
    <div class="input-group">
        <input type="text" value="<?php if(isset($_GET['checkin']) && $_GET['checkin']!=''){ echo date('Y-m-d',strtotime($_GET['checkin'])); } ?>"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkin" id="checkin_package">
        <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
    </div>
</div>
<div class="col-md-3 col-sm-6 search-col-padding">
    <label>To Date </label>
    <div class="input-group">
        <input type="text" placeholder="DD/MM/YYYY" class="form-control hasDatepicker" value="<?php if(isset($_GET['checkout']) && $_GET['checkout']!=''){ echo date('Y-m-d',strtotime($_GET['checkout'])); } ?>" name="checkout" id="checkout_package">
        <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-3 col-sm-6 search-col-padding">
    <label>Types</label>
    <div class="input-group">
        <select class="selectpicker" name="category" id="category9" rel="9" >
             <?php
			$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'9','parent_id'=>'0','category_status'=>'active')); 
                    if(count($category_accom)>0)
                    {
                        foreach($category_accom as $row)
                        {
							$selected='';
							if(isset($_GET['category']))
							{
								if($row['category_id']==$_GET['category'])
								{
									$selected="selected='selected'";
								}
							}
							?>
                             <option value="<?php echo $row['category_id']; ?>" <?php echo $selected;?>><?php echo $row['category_name_eng']; ?></option>
                            <?php
						}
					}
			?>
        </select>
    </div>
</div>
<!--<div class="col-md-1 col-sm-6 search-col-padding">
    <label>Adult</label><br>
    <span class="ui-spinner">
    <input class="form-control" value="1" name="adult" id="adult_count" aria-valuemin="1" aria-valuenow="1" autocomplete="off" role="spinbutton" readonly></span>
</div>
<div class="col-md-1 col-sm-6 search-col-padding">
    <label>Child</label><br>
    <input type="text" class="form-control" value="1" name="child" id="child_count" >
</div>-->
<script type="text/javascript">
jQuery(document).ready(function(e) {
     jQuery('#checkin_package').datetimepicker({
	  format:'d-m-Y',
	  minDate:'2017-01-26',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		maxDate:jQuery('#checkin_package').val()?jQuery('#checkout_package').val():false
	   })
	  },
 });
	jQuery('#checkout_package').datetimepicker({
	  format:'d-m-Y',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		minDate:jQuery('#checkin_package').val()?jQuery('#checkin_package').val():false
	   })
	  },
	});
});

</script>