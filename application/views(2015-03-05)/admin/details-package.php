<link rel="stylesheet" href="<?php echo base_url(); ?>assets/prettyPhoto/css/prettyPhoto.css">
<!-- BEGIN Page Title -->
<div class="page-title">
  <div>
    <h1><i class="fa fa-book"></i><?php echo $pageLable;?></h1>
    <h4><?php echo $pageLable;?></h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li><a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>"><?php echo 'Manage Boat';?></a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
    <li class="active"><?php echo $pageLable;?></li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class='col-md-12'>
    <div class="box">
        <div class="box-title">
            <h3><i class="fa fa-bars"></i> <?php echo $arr_details[0]['boat_name'];?></h3>	
            <div class="box-tool">
              <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
             <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_name'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Owner Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['owner_first_name'].' '.$arr_details[0]['owner_last_name'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Adult Qty </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['adult_qty'])){echo $arr_details[0]['adult_qty']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Child Qty </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['child_qty'])){echo $arr_details[0]['child_qty']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Child Price </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['child_price'])){echo $arr_details[0]['child_price']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Adult Price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['adult_price'])){echo $arr_details[0]['adult_price']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Location </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['boat_location'])){echo $arr_details[0]['boat_location']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat To Location</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['boat_location_to'])){echo $arr_details[0]['boat_location_to']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">From Date</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['from_date'])){echo $arr_details[0]['from_date']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">To Date</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['to_date'])){echo $arr_details[0]['to_date']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Short Desc</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['boat_short_desc'])){echo $arr_details[0]['boat_short_desc']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Description</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['boat_description'])){echo $arr_details[0]['boat_description']; } ?>
                  </div>
                </div>
              </form>
          </div>
     </div>
  </div>      
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-gray">
      <div class="box-title">
        <h3><i class="fa fa-file"></i> Boat Image</h3>
       
      </div>
      <div class="box-content">
        <form class="form-horizontal" enctype="multipart/form-data" method="post">
          <ul class="gallery">
             <?php 
			 $getImage=$this->master_model->getRecords('tbl_boat_image',array('package_id'=>$arr_details[0]['package_id'])); 
			 if(count($getImage))
			 {
			   foreach($getImage as $row)
			   {	  
			 ?>
                  <li>
                    <div>
                        <img src="<?php echo base_url().'uploads/boats/'.$row['boat_image_name']; ?>" alt="" width="200" height="200" />
                        <i></i>
                    </div>
                  </li>
            <?php
			   }
			 }
			 ?>
          </ul>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- END Main Content -->