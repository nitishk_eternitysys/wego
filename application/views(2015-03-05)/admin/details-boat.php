<link rel="stylesheet" href="<?php echo base_url(); ?>assets/prettyPhoto/css/prettyPhoto.css">
<!-- BEGIN Page Title -->
<div class="page-title">
  <div>
    <h1><i class="fa fa-book"></i><?php echo $pageLable;?></h1>
    <h4><?php echo $pageLable;?></h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li><a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>"><?php echo 'Manage Boat';?></a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
    <li class="active"><?php echo $pageLable;?></li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class='col-md-12'>
    <div class="box">
        <div class="box-title">
            <h3><i class="fa fa-bars"></i> <?php echo $arr_details[0]['boat_name'];?></h3>	
            <div class="box-tool">
              <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
             <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_name'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Owner Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['owner_first_name'].' '.$arr_details[0]['owner_last_name'];?>
                  </div>
               </div>
               <div class="form-group">
                <?php $fetchtype=$this->master_model->getRecords('tbl_boattype_master',array('boattype_id'=>$arr_details[0]['boat_type_id'])) ?>
                  <label class="col-sm-3 col-lg-2 control-label">Boat Type  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($arr_details[0]['boattype_name_eng'])){echo $arr_details[0]['boattype_name_eng'];} ?>
                  </div>
               </div>   
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Model </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_model']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat size  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_size']; ?>
                  </div>
               </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Capacity </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_capacity']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Utilities </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_utilities']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat License No </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_license_no']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Boat Harbor </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['boat_harbor']; ?>
                  </div>
               </div>
                <div class="form-group">
                 <?php $fetchcabin=$this->master_model->getRecords('tbl_cabins_master',array('cabins_id'=>$arr_details[0]['boat_cabins_id'])) ?>
                  <label class="col-sm-3 col-lg-2 control-label">Boat Cabins </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($fetchcabin[0]['cabins_name_eng'])){echo $fetchcabin[0]['cabins_name_eng'];} ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Crew Type</label>
                  <?php $fetchcrew=$this->master_model->getRecords('tbl_typecrew_master',array('crew_id'=>$arr_details[0]['boat_crewtype_id'])) ?>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($fetchcrew[0]['crew_name_arb'])){echo $fetchcrew[0]['crew_name_arb']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Category Name</label>
                  <?php $fetchcat=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['boat_category_id'])) ?>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($fetchcat[0]['category_name_eng'])){echo $fetchcat[0]['category_name_eng']; } ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Sub-Category Name</label>
                  <?php $fetchsubcat=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['boat_subcategory_id'])) ?>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($fetchsubcat[0]['category_name_eng'])){echo $fetchsubcat[0]['category_name_eng']; } ?>
                  </div>
                </div>
              </form>
          </div>
     </div>
  </div>      
</div>
<!-- END Main Content -->