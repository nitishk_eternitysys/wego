<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Owner extends CI_Controller {
	
	public function __construct()
    {
	   parent::__construct();
	   	
	   if($this->session->userdata('lang')=='')
	   {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	 
	}
	public function regitration()
	{
	  $this->load->library('upload');	
      $data['error']='';		
	  $data['pagetitle']='Regitration owner'; 
	  $this->load->model('email_sending');
	  if(isset($_POST['btn_register']))
	  {
		  $this->form_validation->set_rules('owner_first_name','First Name','required|xss_clean');
		  $this->form_validation->set_rules('company_name','Company Name','required|xss_clean');
		  $this->form_validation->set_rules('owner_email','Email','required|xss_clean|is_unique[tbl_boat_owner.owner_email]');
		  $this->form_validation->set_rules('password','Password','required|xss_clean');
		  $this->form_validation->set_rules('confirm-password','Confirm Password','required|xss_clean');
		  $this->form_validation->set_rules('owner_national_id_number','National Id number','required|xss_clean');
		  $this->form_validation->set_rules('owner_country','Country','required|xss_clean');
		  $this->form_validation->set_rules('owner_city','City','required|xss_clean');
		  $this->form_validation->set_rules('paypal_id','Paypal','required|xss_clean');
		  $this->form_validation->set_rules('bank_name','Bank Name','required|xss_clean');
		  $this->form_validation->set_rules('iban_number','IBAN number','required|xss_clean');
		  if($this->form_validation->run())
		  {
			  $owner_first_name=$this->input->post('owner_first_name',true);
			  $company_name=$this->input->post('company_name',true);
			  $owner_email=$this->input->post('owner_email',true);
			  $password=$this->input->post('password',true);
			  $company_name=$this->input->post('company_name',true);
			  $owner_national_id_number = $this->input->post('owner_national_id_number',true);
			  $owner_country = $this->input->post('owner_country',true);
			  $owner_city = $this->input->post('owner_city',true);
			  $paypal_id = $this->input->post('paypal_id',true);
			  $bank_name = $this->input->post('bank_name',true);
			  $iban_number = $this->input->post('iban_number',true);
			  $today=date('Y-m-d h:i:s');
			    $owner_upload_license='';
				$config=array('upload_path'=>'uploads/license/',
					          'allowed_types'=>'*',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				if($_FILES['owner_upload_license']['name']!='')
				{
					if($this->upload->do_upload('owner_upload_license'))
					{
					  $dt=$this->upload->data();
					  $owner_upload_license=$dt['file_name'];
					}
					else
					{
					  $this->session->set_flashdata('error_owner_reg',$this->upload->display_errors());
					  redirect(base_url().'owner/regitration');
					}
				}
			    if($owner_upload_license!='' && $owner_logo!='')	
			    {
				   $input_array=array('owner_first_name'=>$owner_first_name,'company_name'=>$company_name,'owner_email'=>$owner_email,'owner_password'=>$password,'owner_national_id_number'=>$owner_national_id_number,'owner_country'=>$owner_country,'owner_city'=>$owner_city,'paypal_id'=>$paypal_id,'owner_upload_license'=>$owner_upload_license,'bank_name'=>$bank_name,'iban_number'=>$iban_number);
				   if($owner_id=$this->master_model->insertRecord('tbl_boat_owner',$input_array,true))
				   {
						
						$whr=array('id'=>'1');
						$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
						$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$owner_email,'subject'=>'Boat owner Successfully Registered','view'=>'boat-owner-registration');
						$other_info=array('firstname'=>$owner_first_name,'email'=>$owner_email,'lastname'=>$owner_last_name,'password'=>$password,'userId'=>$owner_id);
						$this->email_sending->sendmail($info_arr,$other_info);
						
						$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
						$other_info1=array('firstname'=>$owner_first_name,'email'=>$owner_email,'lastname'=>$owner_last_name);
						$this->email_sending->sendmail($info_arr1,$other_info1);
						
						$this->session->set_flashdata('success_owner_reg','You are registered successfully.');
						redirect(base_url().'owner/regitration');
					}
				   else
				   {
					 $this->session->set_flashdata('error_owner_reg','Oops ! something wrong. Please try sometimes leter.');
					 redirect(base_url().'owner/regitration');
				   }
			    }
		   }
		  else
		  {
			$this->session->set_flashdata('error_owner_reg',$this->form_validation->error_string());
			redirect(base_url().'owner/regitration');
		  }
	  }
	  $data['middle_content']='login';
	  $this->load->view('common-file',$data);	
	}
	public function login()
	{
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	 
      $data['error']='';	
	  $data['pagetitle']='Owner login'; 
	  if(isset($_POST['btn_login']))
	  {
		    $this->form_validation->set_rules('email','Email','required|xss_clean');
		    $this->form_validation->set_rules('password','Password','required|xss_clean');
			if($this->form_validation->run())
			{
			   $email=$this->input->post('email',true);
			   $password=$this->input->post('password',true);
			   $valid=$this->master_model->getRecords('tbl_boat_owner',array('owner_email'=>$email,'owner_password'=>$password));
				if(count($valid)>0)
				{
					if($valid[0]['owner_email_verify']!='no')
					{
						if($valid[0]['owner_status']!='block')
						{
							$user_data=array('userID'=>$valid[0]['owner_id'],
											 'userEmail'=>$valid[0]['owner_email'],
											 'userFName'=>$valid[0]['owner_first_name'],
											 'userLName'=>$valid[0]['owner_last_name'],
											 'userType'=>'owner'
											  );
							$this->session->set_userdata($user_data);
							redirect(base_url('owner/profile'));
						}
						else
						{
						  $this->session->set_flashdata('error_owner_login','Sorry! you are block by admin.');
						  redirect(base_url().'owner/regitration');
						}
					}
					else
					{
					  $this->session->set_flashdata('error_owner_login','Sorry! You are not verify. Please verify your email id.');
					  redirect(base_url().'owner/regitration');
					}
				}
				else
				{
					$this->session->set_flashdata('error_owner_login','Invalid credentials.');
					redirect(base_url().'owner/regitration');
				}
			}
			else
			{
				$this->session->set_flashdata('error_owner_login',$this->form_validation->error_string());
				redirect(base_url().'owner/regitration');
			}
	  }
	  //$data['middle_content']='owner-login';
	  $data['middle_content']='login';
	  $this->load->view('common-file',$data);	
	}
	public function verify()
	{
		$owner_id=base64_decode($this->uri->segment(3));
		if($owner_id)
		{
			$input_array=array('owner_email_verify'=>'yes');
			$this->master_model->updateRecord('tbl_boat_owner',$input_array,array('owner_id'=>$owner_id)); 
			$this->session->set_flashdata('success','Thank you. you are verified.');
			redirect(base_url().'owner/login');
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function profile()
	{
		if($this->session->userdata('userID')=='')
	   {redirect(base_url());}
	  $this->load->library('upload');		
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Owner account'; 
	  $data['error']='';
	  if(isset($_POST['btn_profile']))
	  {
		  $this->form_validation->set_rules('owner_first_name','First Name','required|xss_clean');
		  $this->form_validation->set_rules('owner_last_name','Last Name','required|xss_clean');
		  $this->form_validation->set_rules('company_name','company name','required|xss_clean');
		  if($this->form_validation->run())
		  {
			  $owner_first_name=$this->input->post('owner_first_name',true);
			  $owner_last_name=$this->input->post('owner_last_name',true);
			  $owner_email=$this->input->post('owner_email',true);
			  $password=$this->input->post('password',true);
			  $company_name=$this->input->post('company_name',true);
			  $owner_upload_license=$this->input->post('oldlicense');
			  $owner_logo=$this->input->post('oldlogo');
			  $owner_contact_personal=$this->input->post('owner_contact_personal');
			  $owner_mobile=$this->input->post('owner_mobile');
			  $owner_telphone=$this->input->post('owner_telphone');
			  $owner_fax=$this->input->post('owner_fax');
			  $owner_branch_1=$this->input->post('owner_branch_1');
			  $owner_branch_2=$this->input->post('owner_branch_2');
			  $owner_branch_3=$this->input->post('owner_branch_3');
			  $owner_address=$this->input->post('owner_address'); 
					$config=array('upload_path'=>'uploads/logo/',
								  'allowed_types'=>'jpg|jpeg|gif|png',
								  'file_name'=>rand(1,9999),'max_size'=>0);
					$this->upload->initialize($config);
					if($_FILES['owner_logo']['name']!='')
					{
						if($this->upload->do_upload('owner_logo'))
						{
						  $dt=$this->upload->data();
						  $owner_logo=$dt['file_name'];
						}
						else
						{
						  $data['error']=$this->upload->display_errors();
						}
					}
					$config=array('upload_path'=>'uploads/license/',
								  'allowed_types'=>'*',
								  'file_name'=>rand(1,9999),'max_size'=>0);
					$this->upload->initialize($config);
					if($_FILES['owner_upload_license']['name']!='')
					{
						if($this->upload->do_upload('owner_upload_license'))
						{
						  $dt=$this->upload->data();
						  $owner_upload_license=$dt['file_name'];
						}
						else
						{
						  $data['error']=$this->upload->display_errors();
						}
					}
			        $input_array=array('owner_first_name'=>$owner_first_name,'owner_last_name'=>$owner_last_name,'owner_company_name'=>$company_name,'owner_upload_license'=>$owner_upload_license,'owner_logo'=>$owner_logo,'owner_contact_personal'=>$owner_contact_personal,'owner_mobile'=>$owner_mobile,'owner_telphone'=>$owner_telphone,'owner_fax'=>$owner_fax,'owner_branch_1'=>$owner_branch_1,'owner_branch_2'=>$owner_branch_2,'owner_branch_3'=>$owner_branch_3,'owner_address'=>$owner_address);
				   if($this->master_model->updateRecord('tbl_boat_owner',$input_array,array('owner_id'=>$this->session->userdata('userID'))))
				   {
						$this->session->set_flashdata('success','records update successfully !');
						redirect(base_url().'owner/profile');
				   } 
				   else
				   {
						$data['error']='Oops ! something wrong. Please try sometimes leter.';
				   }
		   }
		   else
		   {
			 $data['error']=$this->form_validation->error_string();
		   }
	  }
	  if(isset($_POST['btn_addBoat']))
	  {
		  $this->form_validation->set_rules('boat_name','Boat Name','required|xss_clean');
		  $this->form_validation->set_rules('boat_type_id','Boat Type','required|xss_clean');
		  $this->form_validation->set_rules('boat_model','Boat Model','required|xss_clean');
		  if($this->form_validation->run())
		  {
			  $boat_name=$this->input->post('boat_name',true);
			  $boat_type_id=$this->input->post('boat_type_id',true);
			  $boat_price=$this->input->post('boat_price',true);
			 
			  if(count($_POST['facilities']))
			  {$facilities=@implode(',',$_POST['facilities']);}
			  else
			  {$facilities='';}
			  $boat_model=$this->input->post('boat_model',true);
			  $boat_size=$this->input->post('boat_size',true);
			  $boat_capacity=$this->input->post('boat_capacity',true);
			  $boat_utilities=$this->input->post('boat_utilities',true);
			  $boat_license_no=$this->input->post('boat_license_no',true);
			  $boat_harbor=$this->input->post('boat_harbor');
			  $boat_cabins_id=$this->input->post('boat_cabins_id');
			  $boat_crewtype_id=$this->input->post('boat_crewtype_id');
			  $boat_category_id=$this->input->post('boat_category_id');
			  $boat_subcategory_id=$this->input->post('boat_subcategory_id');
			
			  $input_array=array('boat_name'=>$boat_name,'boat_type_id'=>$boat_type_id,'boat_model'=>$boat_model,'boat_size'=>$boat_size,'boat_capacity'=>$boat_capacity,'boat_utilities'=>$boat_utilities,'boat_license_no'=>$boat_license_no,'boat_harbor'=>$boat_harbor,'boat_cabins_id'=>$boat_cabins_id,'boat_crewtype_id'=>$boat_crewtype_id,'boat_category_id'=>$boat_category_id,'boat_subcategory_id'=>$boat_subcategory_id,'boat_owner_id'=>$this->session->userdata('userID'),'facilities'=>$facilities,'added_date'=>date('Y-m-d h:i:s'));
			   if($this->master_model->insertRecord('tbl_boat_master',$input_array))
			   {
					$this->session->set_flashdata('success','Boat Added successfully !');
					redirect(base_url().'owner/profile');
			   } 
			   else
			   {
					$data['error']='Oops ! something wrong. Please try sometimes leter.';
			   }
		   }
		   else
		   {
			 $data['error']=$this->form_validation->error_string();
		   }
	  }
	  if(isset($_POST['btn_addpackage']))
	  {
		  
		     $this->form_validation->set_rules('package_title','Package Title','required|xss_clean');
		     $this->form_validation->set_rules('boat_id','Boat Name','required|xss_clean');
		     if($this->form_validation->run())
			 {
				   $package_title=$this->input->post('package_title');
				   $boat_id=$this->input->post('boat_id');
				   $adult_qty=$this->input->post('adult_qty',true);
				   $child_price=$this->input->post('child_price',true);
				   $child_qty=$this->input->post('child_qty',true);
				   $boat_price=$this->input->post('boat_price',true);
				  
				   $boat_location=$this->input->post('boat_location',true);
				   $boat_location_to=$this->input->post('boat_location_to',true);
				   $from_date=date('Y-m-d',strtotime($this->input->post('from_date',true)));
				   $to_date=date('Y-m-d',strtotime($this->input->post('to_date',true)));
				   $boat_short_desc=$this->input->post('boat_short_desc');
				   $boat_description=$this->input->post('boat_description');

				   $is_deal=$this->input->post('is_deal');
				   if($is_deal!='')
				   {$is_deal='yes';}    
                                    
				   $input_array=array('package_title'=>$package_title,'boat_id'=>$boat_id,'boat_location'=>$boat_location,'boat_short_desc'=>$boat_short_desc,'boat_description'=>$boat_description,'boat_owner_id'=>$this->session->userdata('userID'),'adult_qty'=>$adult_qty,'child_price'=>$child_price,'child_qty'=>$child_qty,'adult_price'=>$boat_price,'boat_location_to'=>$boat_location_to,'from_date'=>$from_date,'to_date'=>$to_date,'added_date'=>date('Y-m-d h:i:s'),'is_deal'=>$is_deal);
				   if($this->master_model->insertRecord('tbl_boatpackage_master',$input_array))
				   {
						$package_id=$this->db->insert_id();
						$files = $_FILES;
						if(isset($files['main_image']['name']) && count($files['main_image']['name'])>0 && $files['main_image']['name'][0]!='')
						{
								$gallry_img=count($files['main_image']['name']); 
								$config=array('upload_path'=>'uploads/boats/', 
											  'allowed_types'=>'jpg|jpeg|gif|png',
											  'file_name'=>rand(1,9999)
											  );
								$this->upload->initialize($config);
								$flag=1; 		
								for($i=0;$i<$gallry_img;$i++)
								{  
									if($files['main_image']['name'][$i]!='')
									{
										$_FILES['main_image']['name']	= rand(0,999999).str_replace(' ','',$files['main_image']['name'][$i]);
										$_FILES['main_image']['type']	= $files['main_image']['type'][$i];
										$_FILES['main_image']['tmp_name']= $files['main_image']['tmp_name'][$i];
										$_FILES['main_image']['error']	= $files['main_image']['error'][$i];
										$_FILES['main_image']['size']	= $files['main_image']['size'][$i];
										if($this->upload->do_upload('main_image'))
										{
											$dt=$this->upload->data();
											$file_name=$dt['file_name'];
											$this->master_model->createThumb($file_name,'uploads/boats/','uploads/boats/thumb',270,200,FALSE);
											$this->master_model->createThumb($file_name,'uploads/boats/','uploads/boats/front',640,620,FALSE);
											$this->master_model->insertRecord('tbl_boat_image',array('package_id'=>$package_id,'boat_id'=>$boat_id,'boat_image_name'=>$file_name));
									   }
									}
									if($i==0)
									{
									  $this->master_model->updateRecord('tbl_boat_image',array('show_front'=>'yes'), array('package_id'=>$package_id));   
									}
								}
						   }
						$this->session->set_flashdata('success','Boat Package Added successfully !');
						redirect(base_url().'owner/profile');
				   } 
				   else
				   {
						$data['error']='Oops ! something wrong. Please try sometimes leter.';
				   }
		      }
	  }
	 
	  $data['fetch_array']=$this->master_model->getRecords('tbl_boat_owner',array('owner_id'=>$this->session->userdata('userID')));
	  $data['boat_types']=$this->master_model->getRecords('tbl_boattype_master',array('boattype_status'=>'active'));
	  $data['cabins']=$this->master_model->getRecords('tbl_cabins_master',array('cabins_status'=>'active'));
	  $data['crew_types']=$this->master_model->getRecords('tbl_typecrew_master',array('crew_status'=>'active'));
	  $data['categories']=$this->master_model->getRecords('tbl_category_master',array('category_status'=>'1','parent_id'=>'0'));
	  $this->db->join('tbl_boattype_master','tbl_boattype_master.boattype_id=tbl_boat_master.boat_type_id');
	  $data['fetch_boats']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boat_master.boat_owner_id'=>$this->session->userdata('userID')),'',array('boat_id'=>'desc'));
	  
	  $this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.package_id=tbl_banking_temp.trpackage_id');
	  $this->db->join('tbl_boat_master','tbl_boat_master.boat_id=tbl_boatpackage_master.boat_id');
	  $data['fetch_banking']=$this->master_model->getRecords('tbl_banking_temp',array('tbl_banking_temp.status'=>'pending','tbl_boat_master.boat_owner_id'=>$this->session->userdata('userID')),'',array('tbl_banking_temp.tranDate'=>'desc')); 
	  
	  $this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.package_id=tbl_transaction_master.trpackage_id');
	  $this->db->join('tbl_boat_master','tbl_boat_master.boat_id=tbl_boatpackage_master.boat_id');
	  $data['fetch_booking']=$this->master_model->getRecords('tbl_transaction_master',array('tbl_boat_master.boat_owner_id'=>$this->session->userdata('userID')),'',array('tbl_transaction_master.tranDate'=>'desc')); 
	  
	  $data['middle_content']='owner-account';
	  $this->load->view('common-file',$data);		
	}
	public function deleteBoat($boat_id)
	{
		$boat_id=base64_decode($boat_id);
		if($boat_id)
		{
			if($this->master_model->deleteRecord('tbl_boat_master','boat_id',$boat_id)) 
			{
				//$this->master_model->deleteRecord('tbl_boat_image','boat_id',$boat_id);
				$this->session->set_flashdata('success','Record deleted successfully.');
				redirect(base_url().'owner/profile/');
			}
			else
			{
				$this->session->set_flashdata('error','Error while deleting record.'); 
				redirect(base_url().'owner/profile');
			}
		}
		else
		{
			redirect(base_url().'owner/profile');
		}
	}
	
	public function editBoat($boat_id)
	{
		if($this->session->userdata('userID')=='')
	   {redirect(base_url());}
		$boat_id=base64_decode($boat_id);
		if($boat_id)
		{
		  $this->load->library('upload');		
		  if($this->session->userdata('lang')=='')
		  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		  $data['pagetitle']='Edit Boat'; 
		  $data['error']='';
		  if(isset($_POST['btn_editBoat']))
	      {
		   $this->form_validation->set_rules('boat_name','Boat Name','required|xss_clean');
		   $this->form_validation->set_rules('boat_type_id','Boat Type','required|xss_clean');
		   $this->form_validation->set_rules('boat_model','Boat Model','required|xss_clean');
		   if($this->form_validation->run())
		   {
				  $boat_name=$this->input->post('boat_name',true);
				  $boat_type_id=$this->input->post('boat_type_id',true);
				  $boat_price=$this->input->post('boat_price',true);
				 
				  if(count($_POST['facilities']))
				  {$facilities=@implode(',',$_POST['facilities']);}
				  else
				  {$facilities='';}
				 
				  
				  $boat_model=$this->input->post('boat_model',true);
				  $boat_size=$this->input->post('boat_size',true);
				  $boat_capacity=$this->input->post('boat_capacity',true);
				  $boat_utilities=$this->input->post('boat_utilities',true);
				  $boat_license_no=$this->input->post('boat_license_no',true);
				  $boat_harbor=$this->input->post('boat_harbor');
				  $boat_cabins_id=$this->input->post('boat_cabins_id');
				  $boat_crewtype_id=$this->input->post('boat_crewtype_id');
				  $boat_category_id=$this->input->post('boat_category_id');
				  $boat_subcategory_id=$this->input->post('boat_subcategory_id');
                                  			
	             
		      $input_array=array('boat_name'=>$boat_name,'boat_type_id'=>$boat_type_id,'boat_model'=>$boat_model,'boat_size'=>$boat_size,'boat_capacity'=>$boat_capacity,'boat_utilities'=>$boat_utilities,'boat_license_no'=>$boat_license_no,'boat_harbor'=>$boat_harbor,'boat_cabins_id'=>$boat_cabins_id,'boat_crewtype_id'=>$boat_crewtype_id,'boat_category_id'=>$boat_category_id,'boat_subcategory_id'=>$boat_subcategory_id,'boat_owner_id'=>$this->session->userdata('userID'),'facilities'=>$facilities,'added_date'=>date('Y-m-d h:i:s'));
				   if($this->master_model->updateRecord('tbl_boat_master',$input_array,array('boat_id'=>$boat_id)))
				   {
					    $this->session->set_flashdata('success','Boat updated successfully !');
						redirect(base_url().'owner/editBoat/'.base64_encode($boat_id));
				   } 
				   else
				   {
						$data['error']='Oops ! something wrong. Please try sometimes leter.';
				   }
		   }
		   else
		   {
			 $data['error']=$this->form_validation->error_string();
		   }
	  }
		  $data['fetch_array']=$this->master_model->getRecords('tbl_boat_owner',array('owner_id'=>$this->session->userdata('userID')));
		  $data['fetch_boats']=$this->master_model->getRecords('tbl_boat_master',array('boat_owner_id'=>$this->session->userdata('userID'),'boat_id'=>$boat_id));
		  $data['boat_types']=$this->master_model->getRecords('tbl_boattype_master',array('boattype_status'=>'active'));
		  $data['cabins']=$this->master_model->getRecords('tbl_cabins_master',array('cabins_status'=>'active'));
		  $data['crew_types']=$this->master_model->getRecords('tbl_typecrew_master',array('crew_status'=>'active'));
		  $data['categories']=$this->master_model->getRecords('tbl_category_master',array('category_status'=>'1','parent_id'=>'0'));
		  $data['boatImage']=$this->master_model->getRecords('tbl_boat_image',array('boat_id'=>$boat_id));
		  $data['middle_content']='edit-boat';
		  $this->load->view('common-file',$data);
		}
		else
		{
			redirect(base_url().'owner/profile');
		}
	}
	public function editpackage($package_id='')
	{
		
		if($this->session->userdata('userID')=='')
	   {redirect(base_url());}
		$package_id=base64_decode($package_id);
		if($package_id)
		{
		  $this->load->library('upload');		
		  if($this->session->userdata('lang')=='')
		  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		  $data['pagetitle']='Edit Boat'; 
		  $data['error']='';
		  if(isset($_POST['btn_editBoat']))
	      {
		     $this->form_validation->set_rules('package_title','Package Title','required|xss_clean');
		     $this->form_validation->set_rules('boat_id','Boat Name','required|xss_clean');
		     if($this->form_validation->run())
			 {
				   $package_title=$this->input->post('package_title');
				   $boat_id=$this->input->post('boat_id');
				   $adult_qty=$this->input->post('adult_qty',true);
				   $child_price=$this->input->post('child_price',true);
				   $child_qty=$this->input->post('child_qty',true);
				   $adult_price =$this->input->post('adult_price',true);
				   
				   $boat_location=$this->input->post('boat_location',true);
				   $boat_location_to=$this->input->post('boat_location_to',true);
				   $from_date=date('Y-m-d',strtotime($this->input->post('from_date',true)));
				   $to_date=date('Y-m-d',strtotime($this->input->post('to_date',true)));
				   $boat_short_desc=$this->input->post('boat_short_desc');
				   $boat_description=$this->input->post('boat_description');
				   
				   $is_deal = $this->input->post('is_deal');
				   if($is_deal=='yes')
				   {$is_deal='yes';}
				   else
				   {$is_deal='no';}
				   $input_array=array('package_title'=>$package_title,'boat_id'=>$boat_id,'boat_location'=>$boat_location,'boat_short_desc'=>$boat_short_desc,'boat_description'=>$boat_description,'boat_owner_id'=>$this->session->userdata('userID'),'adult_qty'=>$adult_qty,'child_price'=>$child_price,'child_qty'=>$child_qty,'boat_location_to'=>$boat_location_to,'adult_price'=>$adult_price,'from_date'=>$from_date,'to_date'=>$to_date,'added_date'=>date('Y-m-d h:i:s'),'is_deal'=>$is_deal);
				   if($this->master_model->updateRecord('tbl_boatpackage_master',$input_array,array('package_id'=>$package_id)))
				   {
					    $files = $_FILES;
						if(isset($files['main_image']['name']) && count($files['main_image']['name'])>0 && $files['main_image']['name'][0]!='')
						{
								$gallry_img=count($files['main_image']['name']); 
								$config=array('upload_path'=>'uploads/boats/', 
											  'allowed_types'=>'jpg|jpeg|gif|png',
											  'file_name'=>rand(1,9999)
											  );
								$this->upload->initialize($config);
								$flag=1; 		
								for($i=0;$i<$gallry_img;$i++)
								{  
									if($files['main_image']['name'][$i]!='')
									{
										$_FILES['main_image']['name']	= rand(0,999999).str_replace(' ','',$files['main_image']['name'][$i]);
										$_FILES['main_image']['type']	= $files['main_image']['type'][$i];
										$_FILES['main_image']['tmp_name']= $files['main_image']['tmp_name'][$i];
										$_FILES['main_image']['error']	= $files['main_image']['error'][$i];
										$_FILES['main_image']['size']	= $files['main_image']['size'][$i];
										if($this->upload->do_upload('main_image'))
										{
											$dt=$this->upload->data();
											$file_name=$dt['file_name'];
											$this->master_model->createThumb($file_name,'uploads/boats/','uploads/boats/thumb',270,200,FALSE);
											$this->master_model->createThumb($file_name,'uploads/boats/','uploads/boats/front',640,620,FALSE);
											$this->master_model->insertRecord('tbl_boat_image',array('package_id'=>$package_id,'boat_id'=>$boat_id,'boat_image_name'=>$file_name));
									   }
									}
								}
						   }
						$this->session->set_flashdata('success','Boat updated successfully !');
						redirect(base_url().'owner/editpackage/'.base64_encode($package_id));
				   } 
				   else
				   {
						$data['error']='Oops ! something wrong. Please try sometimes leter.';
				   }
		   }
		     else
		     {
			  $data['error']=$this->form_validation->error_string();
		     }
	      }
		  $data['fetch_boats']=$this->master_model->getRecords('tbl_boatpackage_master',array('boat_owner_id'=>$this->session->userdata('userID'),'package_id'=>$package_id ));
		  $data['boatImage']=$this->master_model->getRecords('tbl_boat_image',array('package_id'=>$package_id));
		  $data['middle_content']='edit-package';
		  $this->load->view('common-file',$data);
		}
		else
		{
			redirect(base_url().'owner/profile');
		}
	
	}
	
	public function acceptPayment($btID)
	{
		$btID=base64_decode($btID);
		if($btID)
		{
			$banking=$this->master_model->getRecords('tbl_banking_temp',array('tbl_banking_temp.btID'=>$btID)); 
			
			 $insert_array=array(
					'tranAmount'=>$banking[0]['tranAmount'],
					'trpackage_id'=>$banking[0]['trpackage_id'],
					'truserID'=>$banking[0]['truserID'],
					'shipping_firstname'=>$banking[0]['shipping_firstname'],
					'shipping_lastname'=>$banking[0]['shipping_lastname'],
					'shipping_contactNumber'=>$banking[0]['shipping_contactNumber'],
					'note'=>$banking[0]['note'],
					'qty'=>$banking[0]['qty'],
					'tranDate'=>$banking[0]['tranDate'],
					'transactionID'=>uniqid(),
					'tranStatus'=>'Completed'
				);
			if($this->db->insert('tbl_transaction_master',$insert_array,true))
			{
				$this->master_model->updateRecord('tbl_banking_temp',array('status'=>'complete'),array('btID'=>$btID));
				$this->session->set_flashdata('success','Bank details accepted');
				redirect(base_url().'owner/profile');
			}
			else
			{
				$this->session->set_flashdata('error','Oops! something is wrong.');
				redirect(base_url().'owner/profile');
			}
			
		}
		else
		{
			redirect(base_url().'owner/profile');
		}
	}
	
	public function declinePayment($btID)
	{
		$btID=base64_decode($btID);
		if($btID)
		{//decline
			$banking=$this->master_model->getRecords('tbl_banking_temp',array('tbl_banking_temp.btID'=>$btID)); 
			if($this->master_model->updateRecord('tbl_banking_temp',array('status'=>'decline'),array('btID'=>$btID))) 
			{
				$this->db->where('package_id',$banking[0]['trpackage_id']);
				$this->db->set('adult_qty','adult_qty+'.$banking[0]['qty'],FALSE);
				$this->db->set('child_price','child_price+'.$banking[0]['child_qty'],FALSE);
				$this->db->update('tbl_boatpackage_master');
				$this->session->set_flashdata('success','Payment Decline successfully.');
				redirect(base_url().'owner/profile/');
			}
			else
			{
				$this->session->set_flashdata('error','Error while Decline record.'); 
				redirect(base_url().'owner/profile');
			}
		}
		else
		{
			redirect(base_url().'owner/profile');
		}
	}
	public function deletepakage()
	{
		
		$pakage_id=base64_decode($pakage_id);
		if($pakage_id)
		{
			if($this->master_model->deleteRecord('tbl_boatpackage_master','package_id',$package_id)) 
			{
				$this->master_model->deleteRecord('tbl_boat_image','package_id',$package_id );
				$this->session->set_flashdata('success','Record deleted successfully.');
				redirect(base_url().'owner/profile/');
			}
			else
			{
				$this->session->set_flashdata('error','Error while deleting record.'); 
				redirect(base_url().'owner/profile');
			}
		}
		else
		{
			redirect(base_url().'owner/profile');
		}
	}
}