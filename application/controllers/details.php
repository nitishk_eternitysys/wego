<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Details extends CI_Controller {
	public function accodetails($accom_id='')
	{
	  $accom_id=base64_decode($accom_id);
	  $data['pagetitle']='Accommodation details';
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$accom_id));
	  $data['middle_content']='accom-details';
	  $this->load->view('common-file',$data);
	}
	public function transDetails($transportation_id)
	{
	  $transportation_id=base64_decode($transportation_id);
	  $data['pagetitle']='Transportation Details';
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_transportation_master',array('transportation_id'=>$transportation_id));
	  $data['middle_content']='transportation-details';
	  $this->load->view('common-file',$data);
	}
	public function toursDetails($formsID,$type)
	{
	  $formsID=base64_decode($formsID);
	  $data['pagetitle']='Tours details';
	  if(isset($_POST['btn_booking']))
	  {
		 $availability = $_POST['availability'];
		 $arrayInfo=array('availability_check'=>$availability,'formID'=>$formsID);
		 $this->session->set_userdata($arrayInfo);
		 redirect(base_url().'booking/tours/'.base64_encode($formsID).'/');
	  }
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
	  $data['middle_content']='tours-details';
	  $this->load->view('common-file',$data);
	}
	public function eventDetails($event_id)
	{
	  $event_id=base64_decode($event_id);
	  $data['pagetitle']='Event details';
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_event_master',array('event_id'=>$event_id));
	  $data['middle_content']='event-details';
	  $this->load->view('common-file',$data);
	}
	public function tourstime($days,$event_id,$type,$date)
	{
	   $event_id=base64_decode($event_id);
	   $data['pagetitle']='Event details';
	   $data['fetch_array_details']=$this->master_model->getRecords('tbl_available_days',array('day'=>$days,'DATE(form_from_time)'=>''.$date.'','formID'=>$event_id,'LCASE(available_form_type)'=>strtolower($type)));
	   $data['date']= $date;
	  // print_r($data['fetch_array_details']);
	   $this->load->view('schedule',$data);
	}
}
