<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Search extends CI_Controller {
	public function result($type)
	{

			
		  if($this->session->userdata('lang')=='')
		  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		  $data['pagetitle']='Search';
		  $data['middle_content']='search';
		  $this->load->view('common-file',$data);
	}

	function accommodations()
	{
		$data['searchData']=array();
		$role = $this->input->post("role");
		$minPrice = $this->input->post("minPrice");
		$maxPrice = $this->input->post("maxPrice");
		$category = $this->input->post("category");
        $subcategory = $this->input->post("subcategory");
		$view = $this->input->post("view");
		$data['view']=$view;
		
		//sort
		$order_price = $this->input->post("order_price");
		$order_star = $this->input->post("order_star");
		$order_user = $this->input->post("order_user");
		$order_name = $this->input->post("order_name");
		$checkin = $this->input->get("checkin");
		$checkout = $this->input->get("checkout");
		$name = trim($this->input->get("name"));
		$rooms = $this->input->get("rooms");
		$members = $this->input->get("members");
		$adult = $this->input->get("adult");
		$child = $this->input->get("child");
		
		$result=array();
		
		if($category!='') {
			$categories=explode(',',$category);
			$catcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($categories as $cat)
			{
				if($catcounter==1)
				{$whereParam.="tbl_accommodations_master.accom_category_id='".$cat."'";}
				else
				{$whereParam.=" OR tbl_accommodations_master.accom_category_id='".$cat."'";}
				$catcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($subcategory!='') {
			$subcategories=explode(',',$subcategory);
			$subcatcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($subcategories as $subcat)
			{
				if($subcatcounter==1)
				{$whereParam.="tbl_accommodations_master.accom_subcategory_id='".$subcat."'";}
				else
				{$whereParam.=" OR tbl_accommodations_master.accom_subcategory_id='".$subcat."'";}
				$subcatcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($name!='')
		{
			$this->db->or_like(array('tbl_accommodations_master.accom_title' => $name, 'accom_location_address' => $name));
		}
		if($rooms!='')
		{
			$this->db->where('accom_bed_rooms <=',$rooms);
		}
		if($members!='')
		{
			$this->db->where('accom_guest_max <=',$members);
		}
		if($checkin!='')
		{
			$this->db->where('DATE(tbl_accommodations_master.accom_check_in)', date('Y-m-d',strtotime($checkin)));
		}
		if($checkout!='')
		{
			$this->db->where('DATE(tbl_accommodations_master.accom_check_out)', date('Y-m-d',strtotime($checkout)));
		}
		
		if($minPrice!='' && $maxPrice!='' )
		{
		  $this->db->where('tbl_accommodations_master.accom_price >=', $minPrice);
		  $this->db->where('tbl_accommodations_master.accom_price <=', $maxPrice);
		}
		if($order_price)
		{
		  $this->db->order_by('tbl_accommodations_master.accom_price',$order_price);
		}

		if($order_name)
		{
		 $this->db->order_by('tbl_accommodations_master.accom_title',$order_name);
		}
		$this->db->where('DATE(tbl_accommodations_master.accom_check_out) >=',date('Y-m-d'));
		$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_accommodations_master.hosts_id');
		$data['searchData']=$this->master_model->getRecords('tbl_accommodations_master',array('tbl_accommodations_master.accom_status'=>'active'));
		
		$resultHtml=$this->load->view('search/result-accommodations',$data, true);
		$searchHtml=$this->load->view('search/search-accommodations',$data, true);
		echo json_encode(array("html"=>$resultHtml,"count"=>count($data['searchData']),"searchHtml"=>$searchHtml));
		exit;
	}
	
	function thingstodo()
	{


		$data['searchData']=array();
		$role = $this->input->post("role");
		$minPrice = $this->input->post("minPrice");
		$maxPrice = $this->input->post("maxPrice");
		$category = $this->input->post("category");
        $subcategory = $this->input->post("subcategory");
		$view = $this->input->post("view");
		$data['view']=$view;
		
		//sort
		$order_price = $this->input->post("order_price");
		$order_star = $this->input->post("order_star");
		$order_user = $this->input->post("order_user");
		$order_name = $this->input->post("order_name");
		
		$location = $this->input->get("location");
		$adult = $this->input->get("adult");
		$child = $this->input->get("child");
		$checkin = $this->input->get("checkin");
		$checkout = $this->input->get("checkout");
		$mainCat = $this->input->get("mainCat");
		
		$result=array();
		$whereParam="";
		$order_by='';
		$this->db->_protect_identifiers=false;
		if($category!='') 
		{
			$categories=explode(',',$category);
			$catcounter=1;
			$whereParam.=" AND (";
			foreach($categories as $cat)
			{
				if($catcounter==1)
				{$whereParam.="tbl_three_forms.categoryID='".$cat."'";}
				else
				{$whereParam.=" OR tbl_three_forms.categoryID='".$cat."'";}
				$catcounter++;
			}
			$whereParam.=")";
			
		}
		if($subcategory!='') {
			$subcategories=explode(',',$subcategory);
			$subcatcounter=1;
			$whereParam="";
			$whereParam.=" AND(";
			foreach($subcategories as $subcat)
			{
				if($subcatcounter==1)
				{$whereParam.="tbl_three_forms.subcategoryID='".$subcat."'";}
				else
				{$whereParam.=" OR tbl_three_forms.subcategoryID='".$subcat."'";}
				$subcatcounter++;
			}
			$whereParam.=")";
		}

	
		
		if($minPrice!='' && $maxPrice!='' )
		{
			$whereParam.=' AND tbl_three_forms.form_price >='.$minPrice;
			$whereParam.=' AND tbl_three_forms.form_price <='.$maxPrice;
		}
		if($order_price)
		{
			$comma='';
			if($order_name)
			{$comma=',';}
			$order_by.=' tbl_three_forms.form_price '.$order_price.''.$comma.'';
		}
		if($order_name)
		{
			$order_by.=' tbl_three_forms.form_title '.$order_name.'';
		}
		if($checkin!='')
		{
			$whereParam.=' AND DATE(tbl_three_forms.form_available_from_date)="'.date('Y-m-d',strtotime($checkin)).'"';
		}
		if($checkout!='')
		{
			$whereParam.=' AND DATE(tbl_three_forms.form_availalbe_to_date)="'.date('Y-m-d',strtotime($checkout)).'"';
		}
		
		if($mainCat=='tours')
		{
			$whereParam.=' AND tbl_three_forms.form_type="Tours"';
		}
		if($mainCat=='adventure')
		{
			$whereParam.=' AND tbl_three_forms.form_type="Adventure"';
		}
		if($mainCat=='recreations')
		{
			$whereParam.=' AND tbl_three_forms.form_type="Recreation"';
		}
        $order_by_pass='';
		if($order_by!='')
		{
		  $order_by_pass=" ORDER BY ".$order_by."";
		}
        $query_str="";
        $query_str .= "SELECT * FROM (tbl_three_forms) JOIN tbl_hosts_master ON tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id WHERE";
		if($location!='')
		{

			$query_str .= " tbl_three_forms.form_location_address LIKE '%".$location."%'";	
			$query_str .= " AND";
		}

		$query_str .=" DATE(tbl_three_forms.form_availalbe_to_date) >= '".date('Y-m-d')."' AND `tbl_three_forms`.`form_status` =  'active' AND DATE(tbl_three_forms.form_end_date) >='".date('Y-m-d')."' AND tbl_three_forms.form_type!='packages' ".$whereParam." GROUP BY tbl_three_forms.formsID ".$order_by_pass."";

		//echo $query_str;exit;

		$query = $this->db->query($query_str);

		$data['searchData']= $query->result_array();


		/*$query='(SELECT formID FROM tbl_available_days ORDER BY availableID DESC LIMIT 1)=tbl_three_forms.formsID';
		$this->db->order_by('tbl_available_days.form_from_time','DESC');
		$this->db->where('DATE(tbl_three_forms.form_availalbe_to_date) >=',date('Y-m-d'));
		$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
		$this->db->join('tbl_available_days',$query);
		$this->db->group_by('tbl_available_days.formID');
		$data['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active'));
		*/

		 $resultHtml=$this->load->view('search/result-tours',$data, true);
		 $searchHtml=$this->load->view('search/search-tours',$data, true);
		 echo json_encode(array("html"=>$resultHtml,"count"=>count($data['searchData']),"searchHtml"=>$searchHtml));exit;
	
	}
	function packages()
	{
	   
		$data['searchData']=array();
		$role = $this->input->post("role");
		$minPrice = $this->input->post("minPrice");
		$maxPrice = $this->input->post("maxPrice");
		$category = $this->input->post("category");
        $subcategory = $this->input->post("subcategory");
		$view = $this->input->post("view");
		$data['view']=$view;
		
		//sort
		$order_price = $this->input->post("order_price");
		$order_star = $this->input->post("order_star");
		$order_user = $this->input->post("order_user");
		$order_name = $this->input->post("order_name");
		
		$location = $this->input->get("location");
		$adult = $this->input->get("adult");
		$child = $this->input->get("child");
		$checkin = $this->input->get("checkin");
		$checkout = $this->input->get("checkout");
		$mainCat = $this->input->get("mainCat");
		
		$result=array();
		
		if($category!='') {
			$categories=explode(',',$category);
			$catcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($categories as $cat)
			{
				if($catcounter==1)
				{$whereParam.="tbl_three_forms.categoryID='".$cat."'";}
				else
				{$whereParam.=" OR tbl_three_forms.categoryID='".$cat."'";}
				$catcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($subcategory!='') {
			$subcategories=explode(',',$subcategory);
			$subcatcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($subcategories as $subcat)
			{
				if($subcatcounter==1)
				{$whereParam.="tbl_three_forms.subcategoryID='".$subcat."'";}
				else
				{$whereParam.=" OR tbl_three_forms.subcategoryID='".$subcat."'";}
				$subcatcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($location!='')
		{
			$this->db->like('tbl_three_forms.form_departure', $location);
		}
		
		if($minPrice!='' && $maxPrice!='' )
		{
			$this->db->where('tbl_three_forms.form_price >=', $minPrice);
			$this->db->where('tbl_three_forms.form_price <=', $maxPrice);
		}
		if($order_price)
		{
			$this->db->order_by('tbl_three_forms.form_price',$order_price);
		}
		if($order_name)
		{
			$this->db->order_by('tbl_three_forms.form_title',$order_name);
		}
		if($checkin!='')
		{
			$this->db->where('DATE(tbl_three_forms.form_available_from_date)', date('Y-m-d',strtotime($checkin)));
		}
		if($checkout!='')
		{
			$this->db->where('DATE(tbl_three_forms.form_availalbe_to_date)', date('Y-m-d',strtotime($checkout)));
		}
		
		
		$this->db->where('tbl_three_forms.main_categoryID','9');
		$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
		$data['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active'));
		//echo $this->db->last_query();exit;
		$resultHtml=$this->load->view('search/result-tours',$data, true);
		$searchHtml=$this->load->view('search/search-packages',$data, true);
		
		echo json_encode(array("html"=>$resultHtml,"count"=>count($data['searchData']),"searchHtml"=>$searchHtml));exit;
	
		
	}
	function tours()
	{
		$data['searchData']=array();
		$role = $this->input->post("role");
		$minPrice = $this->input->post("minPrice");
		$maxPrice = $this->input->post("maxPrice");
		$category = $this->input->post("category");
        $subcategory = $this->input->post("subcategory");
		$view = $this->input->post("view");
		$data['view']=$view;
		
		//sort
		$order_price = $this->input->post("order_price");
		$order_star = $this->input->post("order_star");
		$order_user = $this->input->post("order_user");
		$order_name = $this->input->post("order_name");
		
		$destination = $this->input->get("destination");
		$adult = $this->input->get("adult");
		$child = $this->input->get("child");
		
		$result=array();
		
		if($category!='') {
			$categories=explode(',',$category);
			$catcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($categories as $cat)
			{
				if($catcounter==1)
				{$whereParam.="tbl_three_forms.categoryID='".$cat."'";}
				else
				{$whereParam.=" OR tbl_three_forms.categoryID='".$cat."'";}
				$catcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($subcategory!='') {
			$subcategories=explode(',',$subcategory);
			$subcatcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($subcategories as $subcat)
			{
				if($subcatcounter==1)
				{$whereParam.="tbl_three_forms.subcategoryID='".$subcat."'";}
				else
				{$whereParam.=" OR tbl_three_forms.subcategoryID='".$subcat."'";}
				$subcatcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($destination!='')
		{
			$this->db->where('tbl_three_forms.form_departure_address', $destination);
		}
		
		if($minPrice!='' && $maxPrice!='' )
		{
			$this->db->where('tbl_three_forms.form_price >=', $minPrice);
			$this->db->where('tbl_three_forms.form_price <=', $maxPrice);
		}
		if($order_price)
		{
			$this->db->order_by('tbl_three_forms.form_price',$order_price);
		}
		if($order_name)
		{
			$this->db->order_by('tbl_three_forms.form_title',$order_name);
		}
		
		if($role=='tours')
		{
			$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
			$data['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active','tbl_three_forms.form_type'=>'Tours'));
		}
		if($role=='adventure')
		{
			$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
			$data['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active','tbl_three_forms.form_type'=>'Adventure'));
		}
		if($role=='recreations')
		{
			$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
			$data['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active','tbl_three_forms.form_type'=>'Recreation'));
		}
		//echo $this->db->last_query();exit;
		$resultHtml=$this->load->view('search/result-tours',$data, true);
		$searchHtml=$this->load->view('search/search-tours',$data, true);
		
		echo json_encode(array("html"=>$resultHtml,"count"=>count($data['searchData']),"searchHtml"=>$searchHtml));exit;
	}
	
	
	function events()
	{
		$data['searchData']=array();
		$role = $this->input->post("role");
		$minPrice = $this->input->post("minPrice");
		$maxPrice = $this->input->post("maxPrice");
		$category = $this->input->post("category");
        $subcategory = $this->input->post("subcategory");
		$view = $this->input->post("view");
		$data['view']=$view;
		
		//sort
		$order_price = $this->input->post("order_price");
		$order_star = $this->input->post("order_star");
		$order_user = $this->input->post("order_user");
		$order_name = $this->input->post("order_name");
		
		$destination = $this->input->get("destination");

		
		$result=array();
		
		if($category!='') {
			$categories=explode(',',$category);
			$catcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($categories as $cat)
			{
				if($catcounter==1)
				{$whereParam.="tbl_event_master.event_category_id='".$cat."'";}
				else
				{$whereParam.=" OR tbl_event_master.event_category_id='".$cat."'";}
				$catcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($subcategory!='') {
			$subcategories=explode(',',$subcategory);
			$subcatcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($subcategories as $subcat)
			{
				if($subcatcounter==1)
				{$whereParam.="tbl_event_master.event_subcategory_id='".$subcat."'";}
				else
				{$whereParam.=" OR tbl_event_master.event_subcategory_id='".$subcat."'";}
				$subcatcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($destination!='')
		{
			$this->db->where('tbl_event_master.event_location', $destination);
		}
		if($order_name)
		{
			$this->db->order_by('tbl_event_master.event_title',$order_name);
		}
		
		$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_event_master.hosts_id');
		$data['searchData']=$this->master_model->getRecords('tbl_event_master',array('tbl_event_master.event_status'=>'active'));
		//echo $this->db->last_query();exit;
		$resultHtml=$this->load->view('search/result-events',$data, true);
		$searchHtml=$this->load->view('search/search-events',$data, true);
		
		echo json_encode(array("html"=>$resultHtml,"count"=>count($data['searchData']),"searchHtml"=>$searchHtml));exit;
	
	}
	
	
	function transportation()
	{
		$data['searchData']=array();
		$role = $this->input->post("role");
		$minPrice = $this->input->post("minPrice");
		$maxPrice = $this->input->post("maxPrice");
		$category = $this->input->post("category");
        $subcategory = $this->input->post("subcategory");
		$view = $this->input->post("view");
		$data['view']=$view;
		
		//sort
		$order_price = $this->input->post("order_price");
		$order_star = $this->input->post("order_star");
		$order_user = $this->input->post("order_user");
		$order_name = $this->input->post("order_name");
		
		$pickup_location = $this->input->get("pickup_location");
		$drop_location = $this->input->get("drop_location");
		
		$result=array();
		
		if($category!='') {
			$categories=explode(',',$category);
			$catcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($categories as $cat)
			{
				if($catcounter==1)
				{$whereParam.="tbl_transportation_master.transportation_category_id='".$cat."'";}
				else
				{$whereParam.=" OR tbl_transportation_master.transportation_category_id='".$cat."'";}
				$catcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		if($subcategory!='') {
			$subcategories=explode(',',$subcategory);
			$subcatcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($subcategories as $subcat)
			{
				if($subcatcounter==1)
				{$whereParam.="tbl_transportation_master.transportation_subcategory_id='".$subcat."'";}
				else
				{$whereParam.=" OR tbl_transportation_master.transportation_subcategory_id='".$subcat."'";}
				$subcatcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		
		
		if($pickup_location!='')
		{
			$this->db->where('tbl_transportation_master.transportation_pickup_location', $pickup_location);
		}
		if($drop_location!='')
		{
			$this->db->where('tbl_transportation_master.transportation_drop_location', $drop_location);
		}
		if($order_name)
		{
			$this->db->order_by('tbl_transportation_master.transportation_title',$order_name);
		}
		
		$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_transportation_master.hosts_id');
		$data['searchData']=$this->master_model->getRecords('tbl_transportation_master',array('tbl_transportation_master.transportation_status'=>'active'));
		//echo $this->db->last_query();exit;
		$resultHtml=$this->load->view('search/result-transportation',$data, true);
		$searchHtml=$this->load->view('search/search-transportation',$data, true);
		
		echo json_encode(array("html"=>$resultHtml,"count"=>count($data['searchData']),"searchHtml"=>$searchHtml));exit;
	
	}
}
