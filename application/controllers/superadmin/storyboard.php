<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Storyboard extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	}
	public function manageStoryboard()
	{
	   $data['pagetitle']='Rehla ticket | Story Board';
	   if(isset($_POST['multiple_delete']))
	   {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_story_board','storyID',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/storyboard/manageStoryboard');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/storyboard/manageStoryboard');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/storyboard/manageStoryboard');
			}
		}
	  if(isset($_POST['blockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_story_board',array('story_status'=>$stat),array('storyID'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/storyboard/manageStoryboard');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/storyboard/manageStoryboard');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/storyboard/manageStoryboard');
			}
			
		}
		if(isset($_POST['unblockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_story_board',array('story_status'=>$stat),array('storyID'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/storyboard/manageStoryboard');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/storyboard/manageStoryboard');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/storyboard/manageStoryboard');
			}
		}
	   $data['stories']=$this->master_model->getRecords('tbl_story_board','','',array('storyID'=>'DESC'));
	  $data['middle_content']='manage-story-board';
	  $this->load->view('admin/common-file',$data);
	}
	public function statusStoryBoard($categorystatus,$category_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('story_status'=>$categorystatus);
		if($this->master_model->updateRecord('tbl_story_board',$input_array,array('storyID'=>$category_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/storyboard/manageStoryboard/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/storyboard/manageStoryboard/');
		}
	}
	public function deleteStoryBoard($category_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_story_board','storyID',$category_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/storyboard/manageStoryboard/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/storyboard/manageStoryboard/');
	    }
	}
	public function addStoryBoard()
	{
		$this->load->library('upload');
		   $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Story board';
		  $data['fetch_category']=$this->master_model->getRecords('tbl_category_story',array('category_status'=>'active'));
		  if(isset($_POST['btn_storyboard']))
		  {
			  $this->form_validation->set_rules('categoryID','Category Name','required|xss_clean');
			  $this->form_validation->set_rules('story_title','Title','required|xss_clean');
			  $this->form_validation->set_rules('story_description','Description','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  	$_POST['userID']='0';
					$_POST['dateAdded']=date('Y-m-d H:i:s'); 
					$_POST['story_status']='active'; 
				   if($this->master_model->insertRecord('tbl_story_board',$_POST))
				  { 
				  	$storyID=$this->db->insert_id();
					 $files = $_FILES;
					 if(isset($files['story_images']['name']) && count($files['story_images']['name'])>0 && $files['story_images']['name'][0]!='')
					 {
							$gallry_img=count($files['story_images']['name']); 
							$config=array('upload_path'=>'uploads/story/', 
										  'file_ext_tolower' =>TRUE,
										  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
										  'file_name'=>time().rand(1,9999)
										  );
							$this->upload->initialize($config);
							$flag=1; 		
							for($i=0;$i<$gallry_img;$i++)
							{  
								if($files['story_images']['name'][$i]!='')
								{
									$_FILES['story_images']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['story_images']['name'][$i]));
									$_FILES['story_images']['type']	= $files['story_images']['type'][$i];
									$_FILES['story_images']['tmp_name']= $files['story_images']['tmp_name'][$i];
									$_FILES['story_images']['error']	= $files['story_images']['error'][$i];
									$_FILES['story_images']['size']	= $files['story_images']['size'][$i];
									if($this->upload->do_upload('story_images'))
									{
										$dt=$this->upload->data();
										$file_name=$dt['file_name'];
										$_POST['storyID']=$storyID;
										$_POST['story_images']=$file_name;
										$_POST['dateAdded']=date('Y-m-d H:i:s');
										$this->master_model->insertRecord('tbl_story_images',$_POST);
									}
								}
							 }
					  }
					  $this->session->set_flashdata('success','Record Added successfully.');
					  redirect(base_url().'superadmin/storyboard/addStoryBoard/');
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Error while Adding record.'); 
					  redirect(base_url().'superadmin/storyboard/addStoryBoard/');
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='add-story-board';
		  $this->load->view('admin/common-file',$data);
	}
	public function updateStoryBoard()
	{
		$this->load->library('upload');
		   $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Edit Story Board';
		  $storyID=$this->uri->segment('4');
		   $data['stories']=$this->master_model->getRecords('tbl_story_board',array('storyID'=>$storyID));
		   $data['fetch_story_img']=$this->master_model->getRecords('tbl_story_images',array('storyID'=>$storyID));
		   $data['fetch_category']=$this->master_model->getRecords('tbl_category_story',array('category_status'=>'active'));
		  if(isset($_POST['btn_storyboard']))
		  {
			  $this->form_validation->set_rules('categoryID','Category Name','required|xss_clean');
			  $this->form_validation->set_rules('story_title','Title','required|xss_clean');
			  $this->form_validation->set_rules('story_description','Description','required|xss_clean');
			  if($this->form_validation->run())
			  {
				if($this->master_model->updateRecordf('tbl_story_board',$_POST,array('storyID'=>$storyID)))
				{ 
				$storyID=$storyID;
				 $files = $_FILES;
				 if(isset($files['story_images']['name']) && count($files['story_images']['name'])>0 && $files['story_images']['name'][0]!='')
				 {
						$gallry_img=count($files['story_images']['name']); 
						$config=array('upload_path'=>'uploads/story/', 
									  'file_ext_tolower' =>TRUE,
									  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
									  'file_name'=>time().rand(1,9999)
									  );
						$this->upload->initialize($config);
						$flag=1; 		
						for($i=0;$i<$gallry_img;$i++)
						{  
							if($files['story_images']['name'][$i]!='')
							{
								$_FILES['story_images']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['story_images']['name'][$i]));
								$_FILES['story_images']['type']	= $files['story_images']['type'][$i];
								$_FILES['story_images']['tmp_name']= $files['story_images']['tmp_name'][$i];
								$_FILES['story_images']['error']	= $files['story_images']['error'][$i];
								$_FILES['story_images']['size']	= $files['story_images']['size'][$i];
								if($this->upload->do_upload('story_images'))
								{
									$dt=$this->upload->data();
									$file_name=$dt['file_name'];
									$_POST['storyID']=$storyID;
									$_POST['story_images']=$file_name;
									$this->master_model->insertRecord('tbl_story_images',$_POST);
								}
							}
						 }
				  }
					$this->session->set_flashdata('success','Record Added successfully.');
					redirect(base_url().'superadmin/storyboard/updateStoryBoard/'.$storyID);
				}
				else
				{
					$this->session->set_flashdata('error','Error while Adding record.'); 
					redirect(base_url().'superadmin/storyboard/updateStoryBoard/'.$storyID);
				}
				  
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-story-board';
		  $this->load->view('admin/common-file',$data);
	}
	
}