<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Host extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  

	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | user';
	  $data['pageLable']='Hosts Provider';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_hosts_master','hosts_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_hosts_master',array('hosts_status'=>$stat),array('hosts_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_hosts_master',array('hosts_status'=>$stat),array('hosts_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_hosts_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$hosts_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('hosts_status'=>$status);
		if($this->master_model->updateRecord('tbl_hosts_master',$input_array,array('hosts_id'=>$hosts_id)))
		{
		   $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($hosts_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_hosts_master','hosts_id',$hosts_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}

	public function edit($hosts_id)
	{

		

		$data['pagetitle']='Rehla ticket | user';
	  	$data['pageLable']='Hosts Provider';

	  	if(isset($_POST['btn_edit_host']))	
	  	{

	  		
	  		$this->form_validation->set_rules('full_name','Full Name','required|xss_clean');
	  		$this->form_validation->set_rules('company_name','Company Name','required|xss_clean');
	  		$this->form_validation->set_rules('description','Description','required|xss_clean');
	  		$this->form_validation->set_rules('address','Address','required|xss_clean');
	  		$this->form_validation->set_rules('website','Website','required|xss_clean');
	  		$this->form_validation->set_rules('contact_no','Contact Number','required|xss_clean');
	  		$this->form_validation->set_rules('position','Position','required|xss_clean');
	  		$this->form_validation->set_rules('mobile_no','Mobile Number','required|xss_clean');
	  		$this->form_validation->set_rules('payment_type','Preferred Payment','required|xss_clean');
	  		$this->form_validation->set_rules('currency_type','Preferred Currency','required|xss_clean');

	  		if($this->form_validation->run())
	  		{

	  			$full_name      = $this->input->post('full_name',true);
	  			$company_name   = $this->input->post('company_name',true);
	  			$description    = $this->input->post('description',true);
	  			$address        = $this->input->post('address',true);
	  			$website        = $this->input->post('website',true);
	  			$contact_no     = $this->input->post('contact_no',true);
	  			$position       = $this->input->post('position',true);
	  			$mobile_no      = $this->input->post('mobile_no',true);
	  			$payment_type   = $this->input->post('payment_type',true);
	  			$currency_type  = $this->input->post('currency_type',true);

	  			$mainategory_id_arr = $this->input->post('mainategory_id');
	  			$mainategory_id     = implode(',',$mainategory_id_arr);

	  			$old_profile_image = $this->input->post('old_profile_image');

	  			if(isset($_FILES['profile_image']['name']) && $_FILES['profile_image']['name']!="")
	  			{

	  				$config['upload_path']          = 'uploads/profile/';
	  				$config['file_name']            = uniqid();
	                $config['allowed_types']        = 'gif|jpg|png';
	              

	                $this->load->library('upload', $config);

	                if (!$this->upload->do_upload('profile_image'))
	                {
	                    
	                    $this->session->set_flashdata('error',$this->upload->display_errors()); 
		  				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/edit/'.$hosts_id);
	                  
	                }
	                else
	                {
	                      $upload_data   = $this->upload->data();
	                      $profile_image = $upload_data['file_name'];

	                }
	  			}
	  			else{ $profile_image = $old_profile_image; }


	  			$update_arr = array('hosts_name'           => $full_name,
	  								'hosts_company'        => $company_name,
	  								'host_profile'         => $profile_image,
	  								'hosts_mainategory_id' => $mainategory_id,
	  								'hosts_description'    => $description,
	  								'hosts_address'        => $address,
	  								'hosts_website'        => $website,
	  								'hosts_contact'        => $contact_no,
	  								'hosts_position'       => $position,
	  								'hosts_mobile'         => $mobile_no,
	  								'hosts_preferred_payment' => $payment_type,
	  								'hosts_preferred_currency'=>$currency_type);

	  			$update_result = $this->master_model->updateRecord('tbl_hosts_master',$update_arr,array('hosts_id'=>$hosts_id));
	  			if($update_result)
	  			{

	  				$this->session->set_flashdata('success','Record updated successfully.');
		  			redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	  			}
	  			else
	  			{

	  				$this->session->set_flashdata('error','Error while updating Record.'); 
		  			redirect(base_url().'superadmin/'.$this->router->fetch_class().'/edit/'.$hosts_id);
	  			}

	  		}
	  		else
	  		{
	  			$this->session->set_flashdata('error',$this->form_validation->error_string());
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/edit/'.$hosts_id);
			}	

	  	}
	  	$data['categories'] = $this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active'));
		$data['host_arr']   = $this->master_model->getRecords('tbl_hosts_master',array('hosts_id'=>$hosts_id));
	  	$data['middle_content']='edit-'.$this->router->fetch_class();
	  	$this->load->view('admin/common-file',$data);
		

	}

	public function verfication($hosts_id)
	{

		if($hosts_id!="")	
		{

			$user_exist = $this->master_model->getRecords('tbl_hosts_master',array('hosts_id'=>$hosts_id));
			if(count($user_exist)>0)
			{

				$host_result = $this->master_model->updateRecord('tbl_hosts_master',array('hosts_email_verify'=>'yes'),array('hosts_id'=>$hosts_id));
				if($host_result)
				{

					$this->session->set_flashdata('success','Verfication status updated successfully.');
		  			redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
				}
				else
				{

					$this->session->set_flashdata('error','Error while updating status.'); 
		  			redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
				}
			}
			else
			{

				$this->session->set_flashdata('error','Invalid Host.'); 
		  		redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
			}
		}
	}

}