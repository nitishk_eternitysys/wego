<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Requirement extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Requirement';
	  $data['pageLable']='Requirement';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_requirement_master','requirement_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_requirement_master',array('requirement_status'=>$stat),array('requirement_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_requirement_master',array('requirement_status'=>$stat),array('requirement_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_requirement_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$requirement_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('requirement_status'=>$status);
		if($this->master_model->updateRecord('tbl_requirement_master',$input_array,array('requirement_id'=>$requirement_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($requirement_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_requirement_master','requirement_id',$requirement_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Requirement';
		  $data['pageLable']='Requirement';
		  if(isset($_POST['btn_add']))
		  {
			  $this->form_validation->set_rules('requirement_name_eng','Facilities english','required|xss_clean|is_unqie');
			  $this->form_validation->set_rules('requirement_name_arb','Facilities Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				 
				  $requirement_name_eng=$this->input->post('requirement_name_eng',true);
				  $requirement_name_arb=$this->input->post('requirement_name_arb',true);
				  $requirement_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_requirement_master',array('requirement_name_eng'=>$requirement_name_eng,'requirement_name_arb'=>$requirement_name_arb));
				  if($checkDub==0)
				  {
					  $input_array=array('requirement_name_eng'=>addslashes($requirement_name_eng),'requirement_name_arb'=>$requirement_name_arb,'requirement_status'=>$requirement_status,'requirement_font'=>$requirement_font);
					  if($this->master_model->insertRecord('tbl_requirement_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					  $data['error']='Requirement already exist !';  
				  }
			}
			else
			{
			  $data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($requirement_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Requirement';
		  $data['pageLable']='Requirement';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_requirement_master',array('requirement_id'=>$requirement_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('requirement_name_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('requirement_name_arb','Testimonial Description Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $requirement_name_eng=$this->input->post('requirement_name_eng',true);
				  $requirement_name_arb=$this->input->post('requirement_name_arb',true);
				  $requirement_font=$this->input->post('requirement_font',true);
				  $checkDub=$this->master_model->getRecordCount('tbl_requirement_master',array('requirement_name_eng'=>$requirement_name_eng,'requirement_name_arb'=>$requirement_name_arb,'requirement_id !='=>$requirement_id));
				  if($checkDub==0)
				  {
					  $input_array=array('requirement_name_eng'=>addslashes($requirement_name_eng),'requirement_name_arb'=>$requirement_name_arb,'requirement_font'=>$requirement_font);
					  if($this->master_model->updateRecord('tbl_requirement_master',$input_array,array('requirement_id'=>$requirement_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$requirement_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$requirement_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$requirement_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
}