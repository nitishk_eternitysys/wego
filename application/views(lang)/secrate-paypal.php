<?php
	$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
	$paypal_id='sriniv_1293527277_biz@inbox.com'; // Business email ID

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<!-- To check javascript-->
<noscript>
    <meta http-equiv="refresh" content="0; url=<?php echo base_url();?>" />
</noscript>
<script language="javascript">
document.onmousedown=disableclick;
status="Go Back...";
function disableclick(event)
{
  if(event.button==2)
   {
	 alert(status);
	 return false;
   }
}
</script>
<script>

function load()
{
	document.forms['frm_palpal'].submit();
}
</script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Wego - Redirections</title>
</head>

<body class="demo" oncontextmenu="return false" onload="javascript:return load();" >
<!-- Demo content -->
<div>
  <center>
    <h1>Redirects to Paypal.com ....</h1>
    <img src="<?php echo base_url().'front/images/loader.gif'?>"  height="180" width="180"/>
  </center>
</div>
<!-- /Demo content -->

<div style="display:none;">
  <form action="<?php echo $paypal_url ?>" method="post" name="frm_palpal" id="frm_palpal">
    <input type="hidden" name="business" value="<?php echo $paypal_id ?>">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="item_name" value="Wego Booking">
    <input type="hidden" name="item_number" value="1">
    <input type="hidden" name="amount" value="<?php echo $this->session->userdata('price'); ?>">
    <input type="hidden" name="cpp_header_image" value="http://www.phpgang.com/wp-content/uploads/gang.jpg">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="handling" value="0">
    <input type="hidden" name="cancel_return" value="<?php echo base_url().'booking/cancel_payment'?>">
    <input type="hidden" name="return" value="<?php echo base_url().'booking/return_payment'?>">
  </form>
</div>
</body>
</html>
