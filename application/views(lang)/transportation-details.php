<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>
<!-- START: PAGE TITLE -->
<div class="row page-title page-title3">
	<div class="container clear-padding text-center">
		<h3><?php echo $fetch_array_details[0]['transportation_title']; ?></h3>
		<h4>
			<i class="fa fa-certificate"></i>
			<?php echo $fetch_array_details[0]['transportation_car_brand']; ?>
		</h4>
		<p><i class="fa fa-car"></i> <?php echo $fetch_array_details[0]['transportation_car']; ?></p>
	</div>
</div>
<!-- END: PAGE TITLE -->
<!-- START: ROOM GALLERY -->
<?php $image_array=$this->master_model->getRecords('tbl_transportation_images',array('transportation_id'=>$fetch_array_details[0]['transportation_id'])); ?>
<div class="row hotel-detail">
	<div class="container">
		<div class="main-content col-md-8">
			<div id="room-gallery" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php
						if(count($image_array)){
							$i=0;
						foreach($image_array as $row)
						{
						?>
						<li data-target="#room-gallery" data-slide-to="<?php echo $i; ?>" <?php if ($i==0): ?> class="active" <?php endif; ?>></li>
										 <?php
						$i++;}
						} ?>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php if(count($image_array)){
						$i=0;
					foreach($image_array as $row)
					{ ?>
					<div class="item <?php if ($i==0): ?>active<?php endif; ?>">
						<img src="<?php echo base_url().'uploads/transportation/'.$row['transportation_image_name']; ?>" alt="<?php echo $row['transportation_image_name']; ?>" />
					</div>
				<?php	$i++;}
					} ?>
				</div>
				<a class="left carousel-control" href="#room-gallery" role="button" data-slide="prev">
					<span class="fa fa-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#room-gallery" role="button" data-slide="next">
					<span class="fa fa-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>

			<div class="room-complete-detail">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#overview"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
					<li><a data-toggle="tab" href="#review"><i class="fa fa-comments"></i> <span>Reviews</span></a></li>
					<li><a data-toggle="tab" href="#write-review"><i class="fa fa-edit"></i> <span>Write Review</span></a></li>
				</ul>
				<div class="tab-content">
					<div id="overview" class="tab-pane active in fade">
						<h4 class="tab-heading">OVERVIEW</h4>

						<div class="car-overview col-md-2 col-sm-4 col-xs-6"><i class="fa fa-dashboard"></i><?php echo $fetch_array_details[0]['transportation_car_model']; ?> Model</div>
						<div class="car-overview col-md-2 col-sm-4 col-xs-6"><i class="fa fa-users"></i><?php echo $fetch_array_details[0]['transportation_max_passanger']; ?> Passanger</div>
						<div class="car-overview col-md-2 col-sm-4 col-xs-6"><i class="fa fa-taxi"></i><?php echo $fetch_array_details[0]['transportation_car_size']; ?> Size</div>
						<div class="car-overview col-md-2 col-sm-4 col-xs-6"><i class="fa fa-suitcase"></i><?php echo $fetch_array_details[0]['transportation_car_brand']; ?> Brand</div>
						<div class="car-overview col-md-2 col-sm-4 col-xs-6"><i class="fa fa-eye"></i><?php echo $fetch_array_details[0]['transportation_car']; ?> Car</div>

						<div class="clearfix"></div>
						<h4 class="tab-heading">Brief Description of <?php echo $fetch_array_details[0]['transportation_title']; ?></h4>
						<p><?php echo $fetch_array_details[0]['transportation_description']; ?></p>
					</div>
					<div id="review" class="tab-pane fade">
						<h4 class="tab-heading">REVIEWS</h4>
						<div class="review-header">
							<div class="col-md-6 col-sm6 text-center">
								<h2>4.0/5.0</h2>
								<h5>Based on 128 Reviews</h5>
							</div>
							<div class="col-md-6 col-sm-6">
								<table class="table">
									<tr>
										<td>Value for Money</td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
									<tr>
										<td>Atmosphere in hotel</td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
									<tr>
										<td>Quality of food</td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
										</td>
									</tr>
									<tr>
										<td>Staff behaviour</td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
									<tr>
										<td>Restaurant Quality</td>
										<td>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star"></i>
											<i class="fa fa-star-o"></i>
											<i class="fa fa-star-o"></i>
										</td>
									</tr>
								</table>
							</div>
							<div class="clearfix"></div>
							<div class="guest-review">
								<div class="individual-review dark-review">
									<h4>Best Place to Stay, Awesome <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user1.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span>Lenore, USA</span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review">
									<h4>Best Place to Stay, Awesome <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span>Lenore, USA</span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review dark-review">
									<h4>Best Place to Stay, Awesome <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user1.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span>Lenore, USA</span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review">
									<h4>Best Place to Stay, Awesome <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span>Lenore, USA</span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="individual-review dark-review">
									<h4>Best Place to Stay, Awesome <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
									<div class="col-md-md-1 col-sm-1 col-xs-2">
										<img src="assets/images/user1.jpg" alt="cruise">
									</div>
									<div class="col-md-md-3 col-sm-3 col-xs-3">
										<span>Lenore, USA</span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="load-more text-center">
									<a href="#">Load More</a>
								</div>
							</div>
						</div>
					</div>
					<div id="write-review" class="tab-pane fade">
						<h4 class="tab-heading">Write A Review</h4>
						<form >
							<label>Review Title</label>
							<input type="text" class="form-control" name="review-titile" required>
							<label for="comment">Comment</label>
							<textarea class="form-control" rows="5" id="comment"></textarea>
							<label>Value for Money (Rate out of 5)</label>
							<input type="text" class="form-control" name="value-for-money">
							<label>Hotel Atmosphere (Rate out of 5)</label>
							<input type="text" class="form-control" name="atmosphere">
							<label>Staff Behaviour (Rate out of 5)</label>
							<input type="text" class="form-control" name="staff-beh">
							<label>Food Quality (Rate out of 5)</label>
							<input type="text" class="form-control" name="food-quality">
							<label>Rooms (Rate out of 5)</label>
							<input type="text" class="form-control" name="room">
							<div class="text-center">
								<button type="submit" class="btn btn-default submit-review">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 hotel-detail-sidebar">
			<div class="col-md-12 sidebar-wrapper clear-padding">
				<div class="contact sidebar-item">
					<h4><i class="fa fa-bookmark"></i> Package Summary</h4>
					<div class="sidebar-item-body">
						<h5><i class="fa fa-heart"></i>Title</h5>
						<p><?php echo $fetch_array_details[0]['transportation_title']; ?></p>
						<h5><i class="fa fa-map-marker"></i>Pick up Location</h5>
						<p><?php echo $fetch_array_details[0]['transportation_pickup_location']; ?></p>
						<h5><i class="fa fa-map-marker"></i>Drop Location</h5>
						<p><?php echo $fetch_array_details[0]['transportation_drop_location']; ?></p>
						<h5><i class="fa fa-calendar"></i>Available Time</h5>
						<p><?php echo date('d M Y h:i A',strtotime($fetch_array_details[0]['transportation_available_time'])); ?></p>
						<h5><i class="fa fa-calendar"></i>Pickup Time</h5>
						<p><?php echo date('d M Y h:i A',strtotime($fetch_array_details[0]['transportation_pickuptime'])); ?></p>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END: ROOM GALLERY -->
<script type="text/javascript">
var map;
var markersArray = [];
		function initialize() {
				var myLatlng = new google.maps.LatLng(<?php echo $fetch_array_details[0]['accom_latitude']; ?>,<?php echo $fetch_array_details[0]['accom_longitude']; ?>);
				var myOptions = {
						zoom:7,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map"), myOptions);

				var marker = new google.maps.Marker({
							position: new google.maps.LatLng(<?php echo $fetch_array_details[0]['accom_latitude']; ?>,<?php echo $fetch_array_details[0]['accom_longitude']; ?>),
							map: map
					 });
					 markersArray.push(marker);
}

window.onload = function () { initialize() };
</script>
