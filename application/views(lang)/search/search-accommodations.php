<?php $acco_rooms=$this->master_model->getRecords('tbl_accommodations_master',array('accom_status'=>'active'),'MAX(accom_beds) as rooms,MAX(accom_guest_max) as members'); ?>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Name / Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text"  placeholder="Name / Location" class="form-control" name="name" id="accomdationname" value="<?php echo $_GET['name']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
        <div id="accomresults"></div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Check-In</label>
        <div class="input-group margin-bottom-sm">
            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkin" id="checkin" value="<?php echo $_GET['checkin']; ?>">
            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Check-Out</label>
        <div class="input-group margin-bottom-sm">
            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkout" id="checkout"  value="<?php echo $_GET['checkout']; ?>">
            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
    <label>Rooms</label>
    	<div class="input-group margin-bottom-sm">
            <select class="form-control" name="rooms" id="rooms" >
                <option value="">Select Rooms</option>
                <?php if(count($acco_rooms)>0){ 
                        for($r=1;$r<=$acco_rooms[0]['rooms'];$r++){
                ?>
                <option value="<?php echo $r; ?>" <?php if($_GET['rooms']==$r){ echo 'selected="selected"';} ?>><?php echo $r; ?></option>
                <?php } } ?>
            </select>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
    <label>Members</label>
    	<div class="input-group margin-bottom-sm">
            <select class="form-control" name="members" id="members" >
                <option value="">Select Members</option>
                <?php if(count($acco_rooms)>0){ 
						for($m=1;$m<=$acco_rooms[0]['members'];$m++){
				?>
				<option value="<?php echo $m; ?>" <?php if($_GET['members']==$m){ echo 'selected="selected"';} ?>><?php echo $m; ?></option>
				<?php } } ?>
            </select>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(e) {
     jQuery('#checkin').datetimepicker({
	  format:'d-m-Y',
	  minDate:'<?php echo date('Y-m-d'); ?>',
	  timepicker: false,
	 
 });
	jQuery('#checkout').datetimepicker({
	  format:'d-m-Y',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		minDate:jQuery('#checkin').val()?jQuery('#checkin').val():false
	   })
	  },
	});
});

</script>