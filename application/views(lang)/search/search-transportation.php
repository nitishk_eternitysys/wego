<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Type</label>
        <select class="form-control"  name="category" id="category5" rel="5" >
          <option value=""><?php echo 'Select category'; ?></option>
            <?php
			$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'5','parent_id'=>'0','category_status'=>'active')); 
                    if(count($category_accom)>0)
                    {
                        foreach($category_accom as $row)
                        {
							$selected='';
							if(isset($_GET['category']))
							{
								if($row['category_id']==$_GET['category'])
								{
									$selected="selected='selected'";
								}
							}
							?>
                             <option value="<?php echo $row['category_id']; ?>" <?php echo $selected;?>><?php echo $row['category_name_eng']; ?></option>
                            <?php
						}
					}
			?>
        </select>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Pickup Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="pickup_location" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['pickup_location']; ?>" id="transportationfrom">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
        <div id="transfromresults"></div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Drop Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="drop_location" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['drop_location']; ?>" id="transportationto">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
        <div id="transtoresults"></div>
    </div>
</div>
