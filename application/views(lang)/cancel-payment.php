<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3>Oopps !! payment Cancel</h3>
			<h4 class="thank"><i class="fa fa-thumbs-o-down"></i> Sorry your payment has been canceled!</h4>
			<!--<span><i class="fa fa-plane"></i> New York <i class="fa fa-long-arrow-right"></i> New Delhi <i class="fa fa-calendar"></i> SAT, 22 JUL</span>-->
		</div>
	</div>
	<!-- END: PAGE TITLE -->
	
	<!-- START: BOOKING DETAILS -->
	<div class="row">
		<div class="container clear-padding">
			<div>
				<div class="col-md-8 col-sm-8">
					<div class=" confirmation-detail">
						<h3>Booking Cancled</h3>
						<p>Sorry your payment has been cancel.</p>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 booking-sidebar">
					<div class="sidebar-item contact-box">
						<h4><i class="fa fa-phone"></i>Need Help?</h4>
						<div class="sidebar-body text-center">
							<p>Need Help? Call us or drop a message. Our agents will be in touch shortly.</p>
							<h2>+91 1234567890</h2>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<!-- END: BOOKING DETAILS -->
