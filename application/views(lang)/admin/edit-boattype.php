<!-- BEGIN Page Title -->
<div class="page-title">
    <div style="clear:both !important;">
        <h1><i class="fa fa-plus"></i><?php echo $pageLable; ?></h1>
        <h4>Update <?php echo $pageLable; ?></h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><?php echo $pageLable; ?></li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i> <?php echo $pageLable; ?></h3>
                <div class="box-tool">
                	<a class="show-tooltip" title="" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>">
<i class="fa fa-chevron-up"></i></a>
                   <!-- <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>-->
                </div>
            </div>
            <div class="box-content">
				<?php 
                  if($error!=''){  ?>
                    <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php }  if($this->session->flashdata('error')!=''){?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php
				 }
                  if($this->session->flashdata('success')!=''){?>	
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <form class="form-horizontal" id="validation-form" method="post" enctype="multipart/form-data">
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Name (English) </label>
                        <div class="col-md-6">
             			 <input type="text" class="form-control" name="boattype_name_eng" id="boattype_name_eng" placeholder="Boat type in english" value="<?php echo $fetch_single_arr[0]['boattype_name_eng']; ?>" data-rule-required="true" />
                  <?php echo form_error('boattype_name_eng'); ?>
                    <div class="error_msg" id="error_boattype_name_eng" style="display:none;"></div>
                   </div>
                  </div>
                     <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Name (Arabic) </label>
                        <div class="col-md-6">
                          <input type="text" class="form-control" name="boattype_name_arb" id="boattype_name_arb" placeholder="Boat type in arabic" value="<?php echo $fetch_single_arr[0]['boattype_name_arb']; ?>" dir="rtl" data-rule-required="true" />
                          <?php echo form_error('boattype_name_arb'); ?>
                          <div class="error_msg" id="error_boattype_name_arb" style="display:none;"></div>
                        </div>
                      </div>
                     <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                          <input type="submit" value="Submit" class="btn btn-primary" name="btn_submit" id="btn_submit">
                        </div>
                      </div>
                </form>
            </div>
        </div>
    </div>
</div>

