                <!-- BEGIN Page Title -->
                <div class="page-title">
                    <div>
                        <h1><i class="fa fa-file-o"></i> Dashboard</h1>
                        <h4>Overview, stats, chat and more</h4>
                    </div>
                </div>
                <!-- END Page Title -->

                <!-- BEGIN Breadcrumb -->
                <div id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="active"><i class="fa fa-home"></i> Home</li>
                    </ul>
                </div>
                <!-- END Breadcrumb -->


                <!-- BEGIN Tiles -->
                <div class="row">
                    <div class="col-md-7">
                        <div class="row">
                        
                        <div class="col-md-3 tile-active">
                                        <a class="tile tile-light-blue" data-stop="3000" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <div class="img img-center">
                                                <img src="<?php echo base_url(); ?>img/demo/wp-logo.png" />
                                            </div>
                                            <p class="title text-center">Visit FLATY wp</p>
                                        </a>

                                        <a class="tile tile-light-sky" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <p>FLATY wp is new custom theme designed for the Wordpress admin.</p>
                                        </a>
                                    </div>
                                    
                        <div class="col-md-3 tile-active">
                      <div class="tile tile-blue">
                                            <div class="img img-center">
                                                <i class="fa fa-desktop"></i>
                                            </div>
                                            <p class="title text-center">FLATY Admin</p>
                                        </div>

                      <div class="tile tile-sml-blue">
                                            <p class="title">FLATY Admin</p>
                                            <p>FLATY is the new premium and fully responsive admin dashboard template.</p>
                                            <div class="img img-bottom">
                                                <i class="fa fa-desktop"></i>
                                            </div>
                                        </div>      
                       </div>
                       
                       <div class="col-md-3 tile-active">
                                        <a class="tile tile-red" data-stop="3000" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <div class="img img-center">
                                                <img src="<?php echo base_url(); ?>img/demo/wp-logo.png" />
                                            </div>
                                            <p class="title text-center">Visit FLATY wp</p>
                                        </a>

                                        <a class="tile tile-light-red" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <p>FLATY wp is new custom theme designed for the Wordpress admin.</p>
                                        </a>
                                    </div>
                       
                        <div class="col-md-3 tile-active">
                                       
                                <div class="tile tile-green" data-stop="3500" style="background-image: url(<?php echo base_url(); ?>img/demo/gallery/5.jpg);">
                                    <p class="title">Gallery</p>
                                </div>

                                <a class="tile tile-light-green" data-stop="5000" href="gallery.html">
                                    <p class="title">Gallery page</p>
                                    <p>Click on this tile block to see our amazing gallery page. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div class="img img-bottom">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                </a>
                                    </div>
                                    
                        
                        
                        <div class="col-md-3 tile-active">
                      <div class="tile tile-magenta">
                                            <div class="img img-center">
                                                <i class="fa fa-desktop"></i>
                                            </div>
                                            <p class="title text-center">FLATY Admin</p>
                                        </div>

                      <div class="tile tile-light-magento">
                                            <p class="title">FLATY Admin</p>
                                            <p>FLATY is the new premium and fully responsive admin dashboard template.</p>
                                            <div class="img img-bottom">
                                                <i class="fa fa-desktop"></i>
                                            </div>
                                        </div>      
                       </div>
                       
                        <div class="col-md-3 tile-active">
                                        <a class="tile tile-pink" data-stop="3000" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <div class="img img-center">
                                                <img src="<?php echo base_url(); ?>img/demo/wp-logo.png" />
                                            </div>
                                            <p class="title text-center">Visit FLATY wp</p>
                                        </a>

                                        <a class="tile tile-light-pink" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <p>FLATY wp is new custom theme designed for the Wordpress admin.</p>
                                        </a>
                                    </div>                                  
                        
                       
                        <div class="col-md-3 tile-active">
                                       
                                <div class="tile tile-img" data-stop="3500" style="background-image: url(<?php echo base_url(); ?>img/demo/gallery/5.jpg);">
                                    <p class="title">Gallery</p>
                                </div>
                                

                                <a class="tile tile-light-yellow" data-stop="5000" href="gallery.html">
                                    <p class="title">Gallery page</p>
                                    <p>Click on this tile block to see our amazing gallery page. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div class="img img-bottom">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                </a>
                                    </div>
                                    
                        <div class="col-md-3 tile-active">
                                        <a class="tile tile-dark-magento" data-stop="3000" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <div class="img img-center">
                                                <img src="<?php echo base_url(); ?>img/demo/wp-logo.png" />
                                            </div>
                                            <p class="title text-center">Visit FLATY wp</p>
                                        </a>

                                        <a class="tile tile-lgt-magento" href="http://codecanyon.net/item/flaty-wp-premium-wordpress-flat-admin-template/5329999">
                                            <p>FLATY wp is new custom theme designed for the Wordpress admin.</p>
                                        </a>
                                    </div>            
                           
                     
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tile tile-orange">
                                    <div class="img">
                                        <i class="fa fa-comments"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">128</p>
                                        <p class="title">Comments</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="tile tile-dark-blue">
                                    <div class="img">
                                        <i class="fa fa-download"></i>
                                    </div>
                                    <div class="content">
                                        <p class="big">+160</p>
                                        <p class="title">Downloads</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 tile-active">
                                <div class="tile tile-img" data-stop="3500" style="background-image: url(<?php echo base_url(); ?>img/demo/gallery/5.jpg);">
                                    <p class="title">Gallery</p>
                                </div>
                                <a class="tile tile-lime" data-stop="5000" href="gallery.html">
                                    <p class="title">Gallery page</p>
                                    <p>Click on this tile block to see our amazing gallery page. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <div class="img img-bottom">
                                        <i class="fa fa-picture-o"></i>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- END Tiles -->


                <!-- BEGIN Main Content -->
                <div class="row">
                    <div class="col-md-7">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Visitors Chart</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <div id="visitors-chart" style="margin-top:20px; position:relative; height: 290px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="box">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Weekly Visitors Stat</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="weekly-stats">
                                    <li>
                                        <span class="inline-sparkline">134,178,264,196,307,259,287</span>
                                        Visits: <span class="value">376</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">89,124,197,138,235,169,186</span>
                                        Unique Visitors: <span class="value">238</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">625,517,586,638,669,698,763</span>
                                        Page Views: <span class="value">514</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">1.34,2.98,0.76,1.29,1.86,1.68,1.92</span>
                                        Pages / Visit: <span class="value">1.43</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">2.34,2.67,1.47,1.97,2.25,2.47,1.27</span>
                                        Avg. Visit Time: <span class="value">00:02:34</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">70.34,67.41,59.45,65.43,78.42,75.92,74.29</span>
                                        Bounce Rate: <span class="value">73.56%</span>
                                    </li>
                                    <li>
                                        <span class="inline-sparkline">78.12,74.52,81.25,89.23,86.15,91.82,85.18</span>
                                        % New Visits: <span class="value">82.65%</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        <div class="box box-black">
                            <div class="box-title">
                                <h3><i class="fa fa-retweet"></i> Thing To Do</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="things-to-do">
                                    <li>
                                        <p>
                                            <i class="fa fa-user"></i>
                                            <span class="value">4</span>
                                            Accept User Registration
                                            <a class="btn btn-success" href="#">Go</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-comments"></i>
                                            <span class="value">14</span>
                                            Review Comments
                                            <a class="btn btn-success" href="#">Go</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-shopping-cart blue"></i>
                                            <span class="value">7</span>
                                            Pending Orders
                                            <a class="btn btn-success" href="#">Go</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-file-text-o"></i>
                                            <span class="value">4</span>
                                            New Invoice
                                            <a class="btn btn-success" href="#">Go</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-gear"></i>
                                            <span class="value">3</span>
                                            Settings To Change
                                            <a class="btn btn-success" href="#">Go</a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="box box-orange">
                            <div class="box-title">
                                <h3><i class="fa fa-bar-chart-o"></i> Weekly Changes</h3>
                                <div class="box-tool">
                                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                            <div class="box-content">
                                <ul class="weekly-changes">
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-up light-green"></i>
                                            <span class="light-green">186</span>
                                            New Comments
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-minus light-blue"></i>
                                            <span class="light-blue">53</span>
                                            New Users
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-down light-red"></i>
                                            <span class="light-red">17</span>
                                            New Articles
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-up light-green"></i>
                                            <span class="light-green">75</span>
                                            New Tickets
                                        </p>
                                    </li>
                                    <li>
                                        <p>
                                            <i class="fa fa-arrow-down light-red"></i>
                                            <span class="light-red">74</span>
                                            New Orders
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
           
