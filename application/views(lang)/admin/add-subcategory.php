<!-- BEGIN Page Title -->

<div class="page-title">
  <div style="clear:both !important;">
    <h1><i class="fa fa-book"></i>SubCategory</h1>
    <h4>Add SubCategory</h4>
  </div>
</div>

<!-- END Page Title --> 

<!-- BEGIN Breadcrumb -->

<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li> <a href="<?php echo base_url().'superadmin/subcategory/manage/'; ?>">Manage subcategory</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li class="active">subcategory</li>
  </ul>
</div>

<!-- END Breadcrumb --> 

<!-- BEGIN Main Content -->

<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-title">
        <h3><i class="fa fa-bars"></i>Add subcategory</h3>
        <div class="box-tool"> <a  class="show-tooltip" href="<?php echo base_url().'superadmin/subcategory/manage/';?>" title="Back"><i class="fa fa-chevron-up"></i></a> 
          
          <!-- <a data-action="close" href="#"><i class="fa fa-times"></i></a>--> 
          
        </div>
      </div>
      <div class="box-content">
        <form method="post" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
          <div class="form-group">
            <div class="col-sm-12">
              <?php 

                      if($error!=''){  ?>
              <div class="alert alert-danger"><?php echo $error; ?></div>
              <?php } 

                      if($this->session->flashdata('success')!=''){?>
              <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Main category Name</label>
            <div class="col-md-6">
              <select  id="maincategory_id" name="maincategory_id" class="form-control">
                  <option value="">select main category</option>
                 <?php 
				 $category=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active')); 
				 if(count($category))
				 {
					 foreach($category as $row)
					 {
				 ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_eng']; ?></option>
                <?php
					 }
				 } 
				 ?>
              </select>
              <?php echo form_error('maincategory_id'); ?>
              <div class="error_msg" id="error_maincategory_id" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">category Name</label>
            <div class="col-md-6">
              <select  id="category_id" name="category_id" class="form-control">
                  <option value="">select category</option>
                 <?php 
				 $category=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>'0')); 
				 if(count($category))
				 {
					 foreach($category as $row)
					 {
				 ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_eng'].'('.$row['category_name_arb'].')'; ?></option>
                <?php
					 }
				 } 
				 ?>
              </select>
              <?php echo form_error('category_name_eng'); ?>
              <div class="error_msg" id="error_category_name_eng" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Name (English) </label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="category_name_eng" id="category_name_eng" placeholder="sub-Category Name in english" value="<?php echo set_value('category_name_eng'); ?>" data-rule-required="true" />
              <?php echo form_error('category_name_eng'); ?>
              <div class="error_msg" id="error_category_name_eng" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Name (Arabic) </label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="category_name_arb" id="category_name_arb" placeholder="sub-Category Name in arabic" value="<?php echo set_value('category_name_arb'); ?>" dir="rtl" data-rule-required="true" />
              <?php echo form_error('category_name_arb'); ?>
              <div class="error_msg" id="error_category_name_arb" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Status</label>
            <div class="col-sm-9 col-lg-10 controls">
              <label class="radio-inline">
                <input type="radio" value="1" name="category_status" checked="checked">
                Active </label>
              <label class="radio-inline">
                <input type="radio" value="0" name="category_status">
                Block </label>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
              <input type="submit" value="Submit" class="btn btn-primary" name="btn_category" id="btn_category">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
