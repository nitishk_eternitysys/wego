<script type="text/javascript" language="javascript" src="<?php echo base_url(); ?>js/admin-validation.js"></script>
<!-- BEGIN Page Title -->
<div class="page-title">
    <div>
       <h1><i class="fa fa-book"></i><?php echo $pageLable; ?></h1>
       <h4><?php echo $pageLable; ?></h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><?php echo $pageLable; ?></li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<form name="frm-manage-newsletter" id="frm-manage-newsletter" action="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/';?>"  method="post">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <?php if($this->session->flashdata('error')!=''){  ?>
			<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
		    <?php } if($this->session->flashdata('success')!=''){?>	
			<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
			<?php } ?>
            <div class="box-title">
                <h3><i class="fa fa-table"></i> <?php echo $pageLable; ?></h3>
                <div class="box-tool">
                   <!--<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>-->
                </div>
            </div>
            <div class="box-content">
                <div class="btn-toolbar pull-right clearfix">
                    <div class="btn-group">
                       <!--<a class="btn btn-circle show-tooltip" title="Add Newsletter" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/add/';?>"><i class="fa fa-plus"></i></a>-->
                       <button type="submit" name="multiple_delete" id="multiple_delete"  class="btn btn-circle btn-fill btn-bordered btn-primary"><i class="fa fa-trash-o"></i></button>
                    </div>
                    <div class="btn-group">
                       <button type="submit" name="blockmultiple" id="blockmultiple" class="btn btn-circle btn-fill btn-bordered btn-primary"><i class="fa fa-unlock-alt"></i></button>
                    </div>
                    <div class="btn-group">
                       <button type="submit" name="unblockmultiple" id="unblockmultiple"  class="btn btn-circle btn-fill btn-bordered btn-primary"><i class="fa fa-unlock"></i></button>
                    </div>
                    <div class="btn-group">
                        <a class="btn btn-circle show-tooltip" title="Refresh" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/'.$this->router->fetch_class().'/';?>"><i class="fa fa-repeat"></i></a>
                    </div>
                </div>
                <br/><br/>
                <div class="clearfix"></div>
                <div class="table-responsive" style="border:0">
                   <table class="table table-advance" <?php if(count($fetch_arr)>0){?> id="table1" <?php } ?>>
                       <thead>
                            <tr>
                               <th style="width:18px">
                                <input type="checkbox" name="mult_change" id="mult_change" value="delete" /></th>
                               <th>Boat Name</th>
                               <th>Package Name</th>
                               <th>Boat Owner</th>
                               <th>User Name</th>
                               <th>Rating</th>
                               <th>title</th>
                               <th>Description</th>
                               <th>Status</th>
                               <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                         <?php 
						 if(count($fetch_arr)>0)
						 {
						   foreach($fetch_arr as $row)
						   {  
						    // $boatImage=$this->master_model->getRecords('tbl_boat_image',array('pakage_id'=>$row['package_id']));	
						 ?>
                             <tr>
                                <td style="width:18px">
                                  <input type="checkbox" name="checkbox_del[]" id="checkbox_del[]" value="<?php echo $row['rateID']; ?>"/>
                                </td>
                                <td><?php echo stripslashes($row['boat_name']); ?></td>
                                <td><a href="<?php echo base_url().'boat/details/'.base64_encode($row['package_id']); ?>" target="new"><?php echo  stripslashes($row['package_title']); ?></a></td>
                                <td><?php echo stripslashes($row['owner_first_name']).' '.stripslashes($row['owner_last_name']); ?></td>
                                <td><?php echo stripslashes($row['firstName']).' '.$row['lastName']; ?></td>
                                <td><?php echo stripslashes($row['rating']); ?></td>
                                <td><?php echo stripslashes($row['review_title']); ?></td>
                                <td><?php echo stripslashes($row['review_desc']); ?></td>
                               <?php /*<td>
                                 if(!isset($boatImage[0]['boat_image_name'])){ ?>
                                <img src="<?php echo base_url().'front/20120315120949!Placeholder.jpg'; ?>" width="100" height="100">
                                <?php }else{ ?>
                                 <img src="<?php echo base_url(); ?>uploads/boats/<?php echo $boatImage[0]['boat_image_name']; ?>" width="100" height="100">
                                <?php  }
                                </td>  */ ?>
                               <!-- <td><input type='checkbox' name='chk_set_front' id='<?php echo $row["boat_id"];?>' class='set_front_product' <?php if($row["set_front"]=="yes"){ echo 'checked="checked"';}?>  data-ref="<?php if($row["set_front"]=="yes") { echo 'no';}else{echo 'yes';}?>"></td>-->
                                <td>
                                <?php 
								if($row['review_status']=='active')
								{ ?>
								<a href="<?php echo base_url('superadmin/'.$this->router->fetch_class().'/ratingstatus/block/'.$row['rateID']);?>" title="Click here for Block"><i class="fa fa-unlock" style="font-size: 20px;"></i></a>
								<?php } else{ ?>
								<a href="<?php echo base_url('superadmin/'.$this->router->fetch_class().'/ratingstatus/active/'.$row['rateID']);?>" title="Click here for Active"><i class="fa fa-unlock-alt" style="font-size: 20px;"></i></a>
								<?php } ?>
                                </td>
                                <td>
                                <a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/ratingdelete/'.$row['rateID'];?>" onclick="javascript : return deletconfirm();" title="Click here for Delete" ><i class="fa fa-trash-o"  style="font-size: 18px;"></i></a>
                                <!--<a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/details/'.$row['rateID'];?>" title="Click here for Details" ><i class="fa fa-eye"  style="font-size: 18px;"></i></a>-->
                                </td>
                             </tr>
                        <?php 
						   }
						 }
						 else
						 { ?>
                        <tr>
                          <td colspan="10"><div class="alert alert-danger" style="text-align:center;">No Data Found.</div></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<!-- END Main Content -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/data-tables/bootstrap3/dataTables.bootstrap.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/data-tables/bootstrap3/dataTables.bootstrap.js"></script>