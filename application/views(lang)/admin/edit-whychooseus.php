<!-- BEGIN Page Title -->
<div class="page-title">
    <div style="clear:both !important;">
        <h1><i class="fa fa-plus"></i><?php echo $pageLable; ?></h1>
        <h4>Update <?php echo $pageLable; ?></h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><?php echo $pageLable; ?></li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i> <?php echo $pageLable; ?></h3>
                <div class="box-tool">
                	<a class="show-tooltip" title="" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>">
<i class="fa fa-chevron-up"></i></a>
                   <!-- <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>-->
                </div>
            </div>
            <div class="box-content">
				<?php 
                  if($error!=''){  ?>
                    <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php }  if($this->session->flashdata('error')!=''){?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php
				 }
                  if($this->session->flashdata('success')!=''){?>	
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <form class="form-horizontal" id="validation-form" method="post" enctype="multipart/form-data">
                    <div class="row">
                       <div class="col-md-6 ">
                          <!-- BEGIN Left Side -->
                          <h4>English</h4>
                            <div class="form-group">
                                <label class="col-xs-3 col-lg-2 control-label" for="meta_desc_eng">Title</label>
                                <div class="col-sm-9 col-lg-10 controls">
                                   <input type="text" class="form-control" id="whychoose_title_eng" name="whychoose_title_eng" data-rule-required="true" value="<?php echo $fetch_single_arr[0]['whychoose_title_eng']; ?>">                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 col-lg-2 control-label" for="meta_desc_eng"> Short Description</label>
                                <div class="col-sm-9 col-lg-10 controls">
                                   <textarea class="form-control" rows="5" id="whychoose_short_desc_eng" name="whychoose_short_desc_eng" data-rule-required="true"><?php echo $fetch_single_arr[0]['whychoose_short_desc_eng']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-3 col-lg-2 control-label" for="page_description_arb">Description</label>
                                <div class="col-sm-9 col-lg-10 controls">
                                    <textarea class="form-control ckeditor" rows="5" id="whychoose_desc_eng" name="whychoose_desc_eng" data-rule-required="true"><?php echo $fetch_single_arr[0]['whychoose_desc_eng']; ?></textarea>
                                </div>
                            </div>
                          <!-- END Left Side -->
                       </div>
                       <div class="col-md-6 ">
                          <!-- BEGIN Left Side -->
                          <h4>Arabic</h4>
                          <div class="form-group">
                                <label class="col-xs-3 col-lg-2 control-label" for="meta_desc_eng">Title</label>
                                <div class="col-sm-9 col-lg-10 controls">
                                   <input type="text" class="form-control" id="whychoose_title_arb" name="whychoose_title_arb" data-rule-required="true" value="<?php echo $fetch_single_arr[0]['whychoose_title_arb']; ?>" dir="rtl">                                
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="col-xs-3 col-lg-2 control-label" for="page_description_arb">Short Description</label>
                                <div class="col-sm-9 col-lg-10 controls">
                                    <textarea class="form-control" rows="5" id="whychoose_short_desc_arb" name="whychoose_short_desc_arb" data-rule-required="true" dir="rtl"><?php echo $fetch_single_arr[0]['whychoose_short_desc_arb']; ?></textarea>
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="col-xs-3 col-lg-2 control-label" for="page_description_arb">Description</label>
                                <div class="col-sm-9 col-lg-10 controls">
                                    <textarea class="form-control ckeditor" rows="5" id="whychoose_desc_arb" name="whychoose_desc_arb" data-rule-required="true" dir="rtl"><?php echo $fetch_single_arr[0]['whychoose_desc_arb']; ?></textarea>
                                </div>
                            </div>
                           <!-- END Left Side -->
                       </div>
                       <div class="clr"></div>
                       <div class="clr"></div>
                       <div class="col-md-4">
                           <div class="form-group">
                                <label class="col-xs-1 col-lg-3 control-label" for="page_description_arb">&nbsp;</label>
                                <div class="col-sm-9 ">
                                  <input type="submit" value="Submit" class="btn btn-primary" name="btn_submit" id="btn_submit">
                                </div>
                            </div>
                       </div>
                     </div>
                 </form>
            </div>
        </div>
    </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script> 
<script type="text/javascript">
CKEDITOR.replace( 'whychoose_desc_arb',
{
	language: 'ar'
});
</script>
