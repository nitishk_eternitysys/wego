<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class app extends CI_Controller{
	public function __construct()
    {
	  parent::__construct();
	}
	public function userLogin()
	{
		$resp['result'] ='';
		$resp['user_message']='';
		if(isset($_REQUEST))
		{
			if($_REQUEST['user_type']=='user')
			{
			   $email=$_REQUEST['email'];
			   $password=$_REQUEST['password'];
			   $valid=$this->master_model->getRecords('tbl_user_master',array('user_email'=>$email,'user_password'=>$password));
			   if(count($valid)>0)
			   {
					if($valid[0]['user_email_verify']=='yes')
					{
						if($valid[0]['user_status']=='active')
						{
						         $resp['data']=array('userID'=>$valid[0]['user_id'],
								                     'userEmail'=>$valid[0]['user_email'],
												     'userName'=>$valid[0]['user_name'],
												     'userType'=>'user');
								 $resp['result']='success';
								 $resp['user_message']='Login successfully';	 	
						}
						else
						{
							$resp['result']='fail';
							$resp['user_message']='Sorry! you are block by admin.';
						}
					}
					else
					{
						$resp['result']='fail';
						$resp['user_message']='Your account is not verified yet. Please verify your account from your email address with confirmation link';
					}
				}
			   else
			   {
				    $resp['result']='fail';
					$resp['user_message']='Invalid credentials.';
			   }
			}
			if($_REQUEST['user_type']=='owner')
			{
				
				   $email=$_REQUEST['hosts_email'];
				   $password=$_REQUEST['hosts_password'];
				   $valid=$this->master_model->getRecords('tbl_hosts_master',array('hosts_email'=>$email,'hosts_password'=>$password));
					if(count($valid)>0)
					{
						if($valid[0]['hosts_email_verify']!='no')
						{
							if($valid[0]['hosts_status']!='block')
							{
								
								$resp['data']=array('userID'=>$valid[0]['hosts_id'],
												    'userEmail'=>$valid[0]['hosts_email'],
												    'userFName'=>$valid[0]['hosts_name'],
												    'userType'=>'owner'
												    );			  
								$resp['result'] ='success';
		                        $resp['user_message']='owner login successfully !';				  
							    
							}
							else
							{
							  $resp['result'] ='fail';
		                      $resp['user_message']='Sorry! you are block by admin.';		
							}
						}
						else
						{
						   	$resp['result'] ='fail';
		                    $resp['user_message']='Sorry! You are not verify. Please verify your email id.';		
						}
					}
					else
					{
						$resp['result'] ='fail';
		                $resp['user_message']='Invalid credentials.';
						
					}
			}
		    echo str_replace('\/','/',json_encode($resp));
		}
	}
	public function userReg()
	{
		$this->load->library('upload');	
		$resp['result'] ='';
		$resp['user_message']='';
		if(isset($_POST['user_email']))
		{
				$firstname=$_POST['user_name'];
				$email=$_POST['user_email'];
				$password=$_POST['user_password'];
				$config1=array('upload_path'=>'uploads/profile/',
							   'allowed_types'=>'jpg|jpeg|gif|png|ico',
							   'file_name'=>rand(1,9999),'max_size'=>0);
				$this->upload->initialize($config1);
				if($_FILES['user_id_info']['name']!='')
				{
					if($this->upload->do_upload('user_id_info'))
					{
					  $dt=$this->upload->data();
					  $_POST['user_id_info']=$dt['file_name'];
					}
					else
					{
						$_POST['user_id_info']='';
						$res['error']=$this->upload->display_errors();
					}
				}
				if($this->master_model->insertRecord('tbl_user_master',$_REQUEST))
				{
					$userId=$this->db->insert_id();
					$whr=array('id'=>'1');
					$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
					$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$email,'subject'=>'User Successfully Registered','view'=>'user-registration');
					$other_info=array('firstname'=>$firstname,'email'=>$email,'password'=>$password,'userId'=>$userId);
					$this->email_sending->sendmail($info_arr,$other_info);
					
					$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
					$other_info1=array('firstname'=>$firstname,'email'=>$email,'lastname'=>'');
					$this->email_sending->sendmail($info_arr1,$other_info1);
					
					$resp['result'] ='success';
					$resp['user_message']='You are Register successfully. Please login to continue.';
				}
				else
				{
					$resp['result'] ='fail';
					$resp['user_message']='Oops ! something wrong. Please try sometimes leter.';
				}
		}
		$resp['currency']=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
		$resp['language']=$this->master_model->getRecords('tbl_language_master',array('language_status'=>'active'));
		$this->db->group_by('country');
		$resp['country']=$this->master_model->getRecords('tbl_cities_master');
		echo str_replace('\/','/',json_encode($resp));
	}
	public function ownerReg()
	{
		$this->load->library('upload');	
		$resp['result'] ='';
		$resp['data']=array();
		$resp['user_message']='';
		if(isset($_REQUEST))
		{
		
		  $this->form_validation->set_rules('owner_first_name','First Name','required|xss_clean');
		  $this->form_validation->set_rules('owner_last_name','Last Name','required|xss_clean');
		  $this->form_validation->set_rules('owner_email','Email','required|xss_clean|is_unique[tbl_boat_owner.owner_email]');
		  $this->form_validation->set_rules('password','Password','required|xss_clean');
		  $this->form_validation->set_rules('confirm-password','Confirm Password','required|xss_clean');
		  $this->form_validation->set_rules('company_name','company name','required|xss_clean');
		  if($this->form_validation->run())
		  {
			  $owner_first_name=$this->input->post('owner_first_name',true);
			  $owner_last_name=$this->input->post('owner_last_name',true);
			  $owner_email=$this->input->post('owner_email',true);
			  $password=$this->input->post('password',true);
			  $company_name=$this->input->post('company_name',true);
			  $today=date('Y-m-d h:i:s');
			    $owner_upload_license='';
				$owner_logo='';
			    $config=array('upload_path'=>'uploads/logo/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				if($_FILES['owner_logo']['name']!='')
				{
					if($this->upload->do_upload('owner_logo'))
					{
					  $dt=$this->upload->data();
					  $owner_logo=$dt['file_name'];
					}
					else
					{
					  $resp['result'] ='fail';
					  $resp['user_message']=$this->upload->display_errors();
					}
				}
				$config=array('upload_path'=>'uploads/license/',
					          'allowed_types'=>'*',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				if($_FILES['owner_upload_license']['name']!='')
				{
					if($this->upload->do_upload('owner_upload_license'))
					{
					  $dt=$this->upload->data();
					  $owner_upload_license=$dt['file_name'];
					}
					else
					{
					  $resp['result'] ='fail';
					  $resp['user_message']=$this->upload->display_errors();
					}
				}
			    if($owner_upload_license!='' && $owner_logo!='')	
			    {
				   $input_array=array('owner_first_name'=>$owner_first_name,'owner_last_name'=>$owner_last_name,'owner_email'=>$owner_email,'owner_password'=>$password,'owner_company_name'=>$company_name,'owner_reg_date'=>$today,'owner_upload_license'=>$owner_upload_license,'owner_logo'=>$owner_logo);
				   if($owner_id=$this->master_model->insertRecord('tbl_boat_owner',$input_array,true))
				   {
						
						$whr=array('id'=>'1');
						$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
						$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$owner_email,'subject'=>'Boat owner Successfully Registered','view'=>'boat-owner-registration');
						$other_info=array('firstname'=>$owner_first_name,'email'=>$owner_email,'lastname'=>$owner_last_name,'password'=>$password,'userId'=>$owner_id);
						$this->email_sending->sendmail($info_arr,$other_info);
						
						$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
						$other_info1=array('firstname'=>$owner_first_name,'email'=>$owner_email,'lastname'=>$owner_last_name);
						$this->email_sending->sendmail($info_arr1,$other_info1);
						
						$resp['result'] ='success';
					    $resp['user_message']='You are registered successfully.';
						
						
					}
				   else
				   {
					   $resp['result'] ='fail';
					   $resp['user_message']='Oops ! something wrong. Please try sometimes leter.';
				   }
			    }
				else
				{
				    $resp['result'] ='fail';
					$resp['user_message']='Please upload Logo or license';	
				}
		   }
		  else
		  {
			$resp['result'] ='fail';
			$resp['user_message']=$this->form_validation->error_string();
		   }
		   
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function forgotpassword()
	{
		$resp['result'] ='';
		$resp['user_message']="";
		$this->load->model('email_sending');
		if(isset($_REQUEST))
		{
			$email=$this->input->post('email',TRUE);
			$this->db->select('email,firstName,lastName');
			$userInfo=$this->master_model->getRecords('tbl_user_master',array('email'=>$email));
			$admin_email=$this->master_model->getRecords('tbl_admin_login',array('id'=>'1'));
			if(count($userInfo)!=0)
			{
				    $length = 6;
					$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
					$this->db->set('password',$randomString);
					$this->db->where('email',$email);
					$get_final=$this->db->update('tbl_user_master');
					if($get_final)
					{
						$info_arr=array('from'=>$admin_email[0]['admin_email'],
							            'to'=>$email,
							            'base_url'=>base_url(),
							            'subject'=>'Reset Password On Rehlaticket',
							            'view'=>'user-forgotpassword'
							           );
					    $other_info=array(
						                'base_url'=>base_url(),
							            'randomString' => $randomString,
							            'username'     => $userInfo[0]['firstName'].' '.$userInfo[0]['lastName'],
										'email'        => $email,
						                );
						$this->email_sending->sendmail($info_arr,$other_info);
						$resp['result']='success';
						$resp['user_message']="Password reset successful ! You will receive an email with your new password shortly. Don't forget to check your spam box!";
				    }
				}
			    else
			    {
				  $resp['result']='fail';
				  $resp['user_message']='Please enter your registered email address to receive an alternate password.';
			    }
		 }
		echo str_replace('\/','/',json_encode($resp));
	}
	public function profile()
	{
		  $resp['result'] ='';
		  $resp['data_user']='';
		  $resp['data_country']='';
		  $resp['user_message']="";
		  $fetch_array=$this->master_model->getRecords('tbl_user_master',array('userID'=>$_REQUEST['userID']));
		  $country_array=$this->master_model->getRecords('tbl_countries','','',array('name'=>'ASC'));
		  $resp['data_user']= $fetch_array;
		  $resp['data_country']=$country_array;
		  if(isset($_REQUEST))
		  {
			  $this->form_validation->set_rules('firstName','First Name','required|xss_clean');
			  $this->form_validation->set_rules('lastName','Last Name','required|xss_clean');
			  $this->form_validation->set_rules('email','Email','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $firstname=$this->input->post('firstName',true);
				  $lastname=$this->input->post('lastName',true);
				  $email=$this->input->post('email',true);
				  $contactNumber=$this->input->post('contactNumber',true);
				  $day=$this->input->post('day',true);
				  $month=$this->input->post('month',true);
				  $year=$this->input->post('year',true);
				  $dob=$year.'-'.$month.'-'.$day;
				  $address=$this->input->post('address',true);
				  $zipCode=$this->input->post('zipCode',true);
				  $city=$this->input->post('city',true);
				  $country=$this->input->post('country',true);
				  $state=$this->input->post('state',true);
				  $oldimg=$this->input->post('oldimg',true);
				  $this->load->library('upload'); 
				  $config=array('upload_path'=>'uploads/profile/',
								  'allowed_types'=>'jpg|jpeg|gif|png',
								  'file_name'=>rand(1,9999),'max_size'=>0);
					$this->upload->initialize($config);
					
					if($_FILES['userImage']['name']!='')
					{
						if($this->upload->do_upload('userImage'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						}
						else
						{
							$file=$oldimg;
							$data['error']=$this->upload->display_errors();
						}
					}
					else
					{
						$file=$oldimg;
					}
	                 $input_array=array('firstName'=>$firstname,'lastName'=>$lastname,'email'=>$email,'dob'=>$dob,'address'=>$address,'zipCode'=>$zipCode,'city'=>$city,'countryId'=>$country,'state'=>$state,'userImage'=>$file,'contactNumber'=>$contactNumber);
					 if($this->master_model->updateRecord('tbl_user_master',$input_array,array('userID'=>$_REQUEST['userID'])))
					 {	
						$user_data=array('profile_pic'=>$file);
						$resp['result']='success';
					    $resp['user_message']='Your profile update successfully.';
					 }
					 else
					 {
						$resp['result']='fail';
					    $resp['user_message']='Oops ! something wrong. Please try sometimes leter.';
					 }
				}
			  else
			  {
			      $resp['result']='fail';
			      $resp['user_message']=$this->form_validation->error_string();
			  }
		  }
		  echo str_replace('\/','/',json_encode($resp));
	}
	public function userchangepassword()
	{
		$resp['result'] ='';
		$resp['data']=array();
		$resp['user_message']="";
		if(isset($_REQUEST))
		{
		   $this->form_validation->set_rules('oldPassword','Old password','required|xss_clean');
		   $this->form_validation->set_rules('newPassword','New Password','required|xss_clean');
		   $this->form_validation->set_rules('confirm_password','Confirm Password','required|xss_clean|matches[newPassword]|max_length[6]');
		   if($this->form_validation->run())
		   {
				$current_pass=$this->input->post('oldPassword',true);
				$new_pass=$this->input->post('newPassword',true);
				$row=$this->master_model->getRecordCount('tbl_user_master',array('userID'=>$_REQUEST['userID'],'password'=>$current_pass));
				if($row==0)
				{
					$resp['result']='fail';
					$resp['user_message']="Current Password is Wrong.";
				}
				else
				{
					$input_array=array('password'=>$new_pass);
					$this->master_model->updateRecord('tbl_user_master',$input_array,array('userID'=>$_REQUEST['userID']));
					$resp['result']='success';
					$resp['user_message']="Your Password Updated Successfully.";
				}
		   }
		   else
		   {
			  $resp['result']='fail';
			  $resp['user_message']=$this->form_validation->error_string();;
		   }	
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function searchboat()
	{
		$resp['result'] ='success';
		$resp['data']=array();
		$resp['user_message']="";
		if(!empty($_REQUEST['budget'])) 
		{
				$budget=$_REQUEST['budget'];
				$budgetcounter=1;
				$whereParambd="";
				$whereParambd.="(";
				foreach($budget as $bd)
				{
					$range=explode('-',$bd);
					//print_r($range);exit;
					if($budgetcounter==1)
					{
						if($range[1]=='10000')
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					} 
					else
					{
						if($range[1]=='10000')
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					}
					$budgetcounter++;
				}
				$whereParambd.=")";
				$this->db->where($whereParambd);
			}
		if(!empty($_REQUEST['type'])) 
		{
			$type=$_REQUEST['type'];
			$typecounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($type as $t)
			{
				if($typecounter==1)
				{$whereParam.="tbl_boat_master.boat_type_id=".$t;}
				else
				{$whereParam.=" OR tbl_boat_master.boat_type_id=".$t;}
				$typecounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['cabins'])) 
		{
			$cabins=$_REQUEST['cabins'];
			$cabinscounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($cabins as $c)
			{
				if($cabinscounter==1)
				{$whereParam.="tbl_boat_master.boat_cabins_id=".$c;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_cabins_id=".$c;}
				$cabinscounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['crew'])) 
		{
			$crew=$_REQUEST['crew'];
			$crewcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($crew as $cr)
			{
				if($crewcounter==1)
				{$whereParam.="tbl_boat_master.boat_crewtype_id=".$cr;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_crewtype_id=".$cr;}
				$crewcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['category'])) 
		{
			$category=$_REQUEST['category'];
			$categorycounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($category as $ca)
			{
				if($categorycounter==1)
				{$whereParam.="tbl_boat_master.boat_category_id=".$ca;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_category_id=".$ca;}
				$categorycounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['boat_location'])) 
		{
			$this->db->where('tbl_boatpackage_master.boat_location_to',trim($_REQUEST['boat_location']));
		}
		if(!empty($_REQUEST['boat_location_from'])) 
		{
			$this->db->where('tbl_boatpackage_master.boat_location',trim($_REQUEST['boat_location_from']));
		}
		if(!empty($_REQUEST['boat_location'])) 
		{
			$this->db->where('boat_location_to',trim($_REQUEST['boat_location']));
		}
		if(!empty($_REQUEST['boat_location_from'])) 
		{
			$this->db->where('boat_location',trim($_REQUEST['boat_location_from']));
		}
		$sort=array('tbl_boat_master.boat_id'=>'desc');
		if(!empty($_REQUEST['priceSort'])) 
		{
			$sort=array('tbl_boatpackage_master.adult_price'=>$_REQUEST['priceSort']);
		}
		if(!empty($_REQUEST['boatName'])) 
		{
			$sort=array('tbl_boatpackage_master.package_title'=>$_REQUEST['boatName']);
		}
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$resp['searchData']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boat_master.boat_status'=>'active'),'',$sort);
		echo str_replace('\/','/',json_encode($resp));
	}
}