<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Owner extends CI_Controller {
	
	public function __construct()
    {
	   parent::__construct();
	   	
	   if($this->session->userdata('lang')=='')
	   {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	 
	}
	public function regitration()
	{
	  $this->load->library('upload');	
      $data['error']='';		
	  $data['pagetitle']='Regitration owner'; 
	  $this->load->model('email_sending');
	  if(isset($_POST['btn_register']))
	  {
		  $this->form_validation->set_rules('owner_first_name','First Name','required|xss_clean');
		  $this->form_validation->set_rules('company_name','Company Name','required|xss_clean');
		  $this->form_validation->set_rules('owner_email','Email','required|xss_clean|is_unique[tbl_boat_owner.owner_email]');
		  $this->form_validation->set_rules('password','Password','required|xss_clean');
		  $this->form_validation->set_rules('confirm-password','Confirm Password','required|xss_clean');
		  $this->form_validation->set_rules('owner_national_id_number','National Id number','required|xss_clean');
		  $this->form_validation->set_rules('owner_country','Country','required|xss_clean');
		  $this->form_validation->set_rules('owner_city','City','required|xss_clean');
		  $this->form_validation->set_rules('paypal_id','Paypal','required|xss_clean');
		  $this->form_validation->set_rules('bank_name','Bank Name','required|xss_clean');
		  $this->form_validation->set_rules('iban_number','IBAN number','required|xss_clean');
		  if($this->form_validation->run())
		  {
			  $owner_first_name=$this->input->post('owner_first_name',true);
			  $company_name=$this->input->post('company_name',true);
			  $owner_email=$this->input->post('owner_email',true);
			  $password=$this->input->post('password',true);
			  $company_name=$this->input->post('company_name',true);
			  $owner_national_id_number = $this->input->post('owner_national_id_number',true);
			  $owner_country = $this->input->post('owner_country',true);
			  $owner_city = $this->input->post('owner_city',true);
			  $paypal_id = $this->input->post('paypal_id',true);
			  $bank_name = $this->input->post('bank_name',true);
			  $iban_number = $this->input->post('iban_number',true);
			  $today=date('Y-m-d h:i:s');
			    $owner_upload_license='';
				$config=array('upload_path'=>'uploads/license/',
					          'allowed_types'=>'*',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				if($_FILES['owner_upload_license']['name']!='')
				{
					if($this->upload->do_upload('owner_upload_license'))
					{
					  $dt=$this->upload->data();
					  $owner_upload_license=$dt['file_name'];
					}
					else
					{
					  $this->session->set_flashdata('error_owner_reg',$this->upload->display_errors());
					  redirect(base_url().'owner/regitration');
					}
				}
			    if($owner_upload_license!='')	
			    {
				   $input_array=array('owner_first_name'=>$owner_first_name,'owner_company_name'=>$company_name,'owner_email'=>$owner_email,'owner_password'=>$password,'owner_national_id_number'=>$owner_national_id_number,'owner_country'=>$owner_country,'owner_city'=>$owner_city,'paypal_id'=>$paypal_id,'owner_upload_license'=>$owner_upload_license,'bank_name'=>$bank_name,'iban_number'=>$iban_number);
				   if($owner_id=$this->master_model->insertRecord('tbl_boat_owner',$input_array,true))
				   {
						
						$whr=array('id'=>'1');
						$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
						$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$owner_email,'subject'=>'Boat owner Successfully Registered','view'=>'boat-owner-registration');
						$other_info=array('firstname'=>$owner_first_name,'email'=>$owner_email,'lastname'=>$owner_last_name,'password'=>$password,'userId'=>$owner_id);
						$this->email_sending->sendmail($info_arr,$other_info);
						
						$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
						$other_info1=array('firstname'=>$owner_first_name,'email'=>$owner_email,'lastname'=>$owner_last_name);
						$this->email_sending->sendmail($info_arr1,$other_info1);
						
						$this->session->set_flashdata('success_owner_reg','You are registered successfully.');
						redirect(base_url().'owner/regitration');
					}
				   else
				   {
					 $this->session->set_flashdata('error_owner_reg','Oops ! something wrong. Please try sometimes leter.');
					 redirect(base_url().'owner/regitration');
				   }
			    }
		   }
		  else
		  {
			$this->session->set_flashdata('error_owner_reg',$this->form_validation->error_string());
			redirect(base_url().'owner/regitration');
		  }
	  }
	  if(isset($_POST['btn_register_owner']))
	  {
		  $this->form_validation->set_rules('hosts_name','Full Name','required|xss_clean');
		  $this->form_validation->set_rules('hosts_company','Company Name','required|xss_clean');
		  $this->form_validation->set_rules('hosts_email','Email','required|xss_clean|is_unique[tbl_hosts_master.hosts_email]');
		  $this->form_validation->set_rules('hosts_password','Password','required|xss_clean');
		  $this->form_validation->set_rules('confirm-password','Confirm Password','required|xss_clean');
		  
		  if($this->form_validation->run())
		  {
			  $hosts_name=$this->input->post('hosts_name',true);
			  $hosts_company=$this->input->post('hosts_company',true);
			  $categories_id=$this->input->post('hosts_mainategory_id',true);
			  $hosts_description=$this->input->post('hosts_description',true);
			  $hosts_address=$this->input->post('hosts_address',true);
			  $hosts_website = $this->input->post('hosts_website',true);
			  $hosts_contact = $this->input->post('hosts_contact',true);
			  $hosts_position = $this->input->post('hosts_position',true);
			  $hosts_mobile = $this->input->post('hosts_mobile',true);
			  $hosts_email = $this->input->post('hosts_email',true);
			  $hosts_password = $this->input->post('hosts_password',true);
			  $hosts_preferred_payment = $this->input->post('hosts_preferred_payment',true);
			  $hosts_preferred_currency = $this->input->post('hosts_preferred_currency',true);
			  $latitude='';
			  $longitude='';
			  $hosts_mainategory_id='';
			  if($categories_id)
			  {
				  $hosts_mainategory_id=implode(',',$categories_id);
			  }
             if($hosts_address!='')
			 {
				  $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($hosts_address).'&sensor=false');
					// We convert the JSON to an array
					$geo = json_decode($geo, true);
					// If everything is cool
					if ($geo['status'] = 'OK') {
					  // We set our values
					  $latitude = $geo['results'][0]['geometry']['location']['lat'];
					  $longitude = $geo['results'][0]['geometry']['location']['lng'];
					}
				}
				
			     $dateAdded=date('Y-m-d h:i:s');
			     $input_array=array('hosts_name'=>$hosts_name,'hosts_company'=>$hosts_company,'hosts_mainategory_id'=>$hosts_mainategory_id,'hosts_description'=>$hosts_description,'hosts_address'=>$hosts_address,'hosts_longitude'=>$longitude,'hosts_latitude'=>$latitude,'hosts_website'=>$hosts_website,'hosts_contact'=>$hosts_contact,'hosts_position'=>$hosts_position,'hosts_mobile'=>$hosts_mobile,'hosts_email'=>$hosts_email,'hosts_password'=>$hosts_password,'hosts_preferred_payment'=>$hosts_preferred_payment,'hosts_preferred_currency'=>$hosts_preferred_currency,'dateAdded'=>$dateAdded);
				   if($this->master_model->insertRecord('tbl_hosts_master',$input_array))
				   {
						$owner_id=$this->db->insert_id();
						$whr=array('id'=>'1');
						$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
						$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$hosts_email,'subject'=>'Host Successfully Registered','view'=>'boat-owner-registration');
						$other_info=array('firstname'=>$hosts_name,'email'=>$hosts_email,'lastname'=>'','password'=>$hosts_password,'userId'=>$owner_id);
						$this->email_sending->sendmail($info_arr,$other_info);
						
						$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
						$other_info1=array('firstname'=>$hosts_name,'email'=>$hosts_email,'lastname'=>'');
						$this->email_sending->sendmail($info_arr1,$other_info1);
						
						$this->session->set_flashdata('success_user_reg','You are registered successfully.');
						redirect(base_url().'owner/regitration');
					}
				   else
				   {
					 $data['error_reg']='Oops ! something wrong. Please try sometimes leter.';
				   }
			
		   }
		  else
		  {
			$data['error_reg']=$this->form_validation->error_string();
		  }
	  }
	  $data['categories']=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active'));
      $data['middle_content']='login';
	  $this->load->view('common-file',$data);	
	}
	public function login()
	{
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	 
      $data['error']='';	
	  $data['pagetitle']='Owner login'; 
	  if(isset($_POST['btn_login_owner']))
	  {
		    $this->form_validation->set_rules('hosts_email','Email','required|xss_clean');
		    $this->form_validation->set_rules('hosts_password','Password','required|xss_clean');
			if($this->form_validation->run())
			{
			   $email=$this->input->post('hosts_email',true);
			   $password=$this->input->post('hosts_password',true);
			   $valid=$this->master_model->getRecords('tbl_hosts_master',array('hosts_email'=>$email,'hosts_password'=>$password));
				if(count($valid)>0)
				{
					if($valid[0]['hosts_email_verify']!='no')
					{
						if($valid[0]['hosts_status']!='block')
						{
							$user_data=array('userID'=>$valid[0]['hosts_id'],
											 'userEmail'=>$valid[0]['hosts_email'],
											 'userFName'=>$valid[0]['hosts_name'],
											 'userType'=>'owner'
											  );
							$this->session->set_userdata($user_data);
							redirect(base_url('owner/profile'));
						}
						else
						{
						  $this->session->set_flashdata('error_owner_login','Sorry! you are block by admin.');
						  redirect(base_url().'owner/regitration');
						}
					}
					else
					{
					  $this->session->set_flashdata('error_owner_login','Sorry! You are not verify. Please verify your email id.');
					  redirect(base_url().'owner/regitration');
					}
				}
				else
				{
					$this->session->set_flashdata('error_owner_login','Invalid credentials.');
					redirect(base_url().'owner/regitration');
				}
			}
			else
			{
				$this->session->set_flashdata('error_owner_login',$this->form_validation->error_string());
				redirect(base_url().'owner/regitration');
			}
	  }
	  //$data['middle_content']='owner-login';
	  $data['middle_content']='login';
	  $this->load->view('common-file',$data);	
	}
	public function verify()
	{
		$owner_id=base64_decode($this->uri->segment(3));
		if($owner_id)
		{
			$input_array=array('hosts_email_verify'=>'yes');
			$this->master_model->updateRecord('tbl_hosts_master',$input_array,array('hosts_id'=>$owner_id)); 
			$this->session->set_flashdata('success','Thank you. you are verified.');
			redirect(base_url().'owner/login');
		}
		else
		{
			redirect(base_url());
		}
	}
	public function profile()
	{
	  if($this->session->userdata('userID')=='')
	  {redirect(base_url());}
	  $this->load->library('upload');		
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Owner account'; 
	  $data['error']='';
	  if(isset($_POST['btn_profile']))
	  {
		  $this->form_validation->set_rules('hosts_name','Full Name','required|xss_clean');
		  $this->form_validation->set_rules('hosts_company','Company Name','required|xss_clean');
		  $this->form_validation->set_rules('hosts_email','Email','required|xss_clean');
		  
		  if($this->form_validation->run())
		  {
			  $hosts_name=$this->input->post('hosts_name',true);
			  $hosts_company=$this->input->post('hosts_company',true);
			  $categories_id=$this->input->post('hosts_mainategory_id',true);
			  $hosts_description=$this->input->post('hosts_description',true);
			  $hosts_address=$this->input->post('hosts_address',true);
			  $hosts_website = $this->input->post('hosts_website',true);
			  $hosts_contact = $this->input->post('hosts_contact',true);
			  $hosts_position = $this->input->post('hosts_position',true);
			  $hosts_mobile = $this->input->post('hosts_mobile',true);
			  $hosts_email = $this->input->post('hosts_email',true);
			  $hosts_preferred_payment = $this->input->post('hosts_preferred_payment',true);
			  $hosts_preferred_currency = $this->input->post('hosts_preferred_currency',true);
			  $latitude='';
			  $longitude='';
			  $hosts_mainategory_id='';
			  if($categories_id)
			  {
				  $hosts_mainategory_id=implode(',',$categories_id);
			  }

			if($hosts_address!='')
			{
			  $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($hosts_address).'&sensor=false');
				// We convert the JSON to an array
				$geo = json_decode($geo, true);
				// If everything is cool
				if ($geo['status'] = 'OK') {
				  // We set our values
				  $latitude = $geo['results'][0]['geometry']['location']['lat'];
				  $longitude = $geo['results'][0]['geometry']['location']['lng'];
				}
			}
			   
				   $input_array=array('hosts_name'=>$hosts_name,'hosts_company'=>$hosts_company,'hosts_mainategory_id'=>$hosts_mainategory_id,'hosts_description'=>$hosts_description,'hosts_address'=>$hosts_address,'hosts_longitude'=>$longitude,'hosts_latitude'=>$latitude,'hosts_website'=>$hosts_website,'hosts_contact'=>$hosts_contact,'hosts_position'=>$hosts_position,'hosts_mobile'=>$hosts_mobile,'hosts_preferred_payment'=>$hosts_preferred_payment,'hosts_preferred_currency'=>$hosts_preferred_currency);
				   if($this->master_model->updateRecord('tbl_hosts_master',$input_array,array('hosts_id'=>$this->session->userdata('userID'))))
				   {	
						$this->session->set_flashdata('success','Profile updated successfully.');
						redirect(base_url().'owner/profile');
					}
				   else
				   {
					 $data['error']='Oops ! something wrong. Please try sometimes leter.';
				   }
			
		   }
		  else
		  {
			$data['error']=$this->form_validation->error_string();
		  }
	  }
	  if(isset($_POST['btn_change_pass']))
	  {
		  
		  $this->form_validation->set_rules('old-password','Old Password','required|xss_clean');
		  $this->form_validation->set_rules('new-password','New password','required|xss_clean');
		  $this->form_validation->set_rules('confirm-password','confirm password','required|xss_clean');
		  
		  if($this->form_validation->run())
		  {
			  $old_password=$this->input->post('old-password',true);
			  $new_password=$this->input->post('new-password',true);
			  $confirm_password=$this->input->post('confirm-password',true);
			 $valid=$this->master_model->getRecords('tbl_hosts_master',array('hosts_password'=>$old_password,'hosts_id'=>$this->session->userdata('userID')));
			   if(count($valid)>0){
				   $input_array=array('hosts_password'=>$new_password);
				   if($this->master_model->updateRecord('tbl_hosts_master',$input_array,array('hosts_id'=>$this->session->userdata('userID'))))
				   {	
						$this->session->set_flashdata('success','Password change successfully.');
						redirect(base_url().'owner/profile');
					}
				   else
				   {
					 $data['error']='Oops ! something wrong. Please try sometimes leter.';
				   }
			   }
			   else
			   {
				   $data['error']='Old password is incorrect';
			   }
			
		   }
		  else
		  {
			$data['error']=$this->form_validation->error_string();
		  }
	  
	  }
	  if(isset($_POST['accommodationssubmit']))
	  {
		 $_POST['accom_added_date']=date('Y-m-d'); 
		 $_POST['hosts_id']=$this->session->userdata('userID');
		 if($this->master_model->insertRecord('tbl_accommodations_master',$_POST))
		 {
			 $accom_id=$this->db->insert_id();
		     $files = $_FILES;
			 if(isset($files['accom_image']['name']) && count($files['accom_image']['name'])>0 && $files['accom_image']['name'][0]!='')
			 {
					$gallry_img=count($files['accom_image']['name']); 
					$config=array('upload_path'=>'uploads/accom/', 
								  'file_ext_tolower' =>TRUE,
								  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
								  'file_name'=>time().rand(1,9999)
								  );
					$this->upload->initialize($config);
					$flag=1; 		
					for($i=0;$i<$gallry_img;$i++)
					{  
						if($files['accom_image']['name'][$i]!='')
						{
							$_FILES['accom_image']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['accom_image']['name'][$i]));
							$_FILES['accom_image']['type']	= $files['accom_image']['type'][$i];
							$_FILES['accom_image']['tmp_name']= $files['accom_image']['tmp_name'][$i];
							$_FILES['accom_image']['error']	= $files['accom_image']['error'][$i];
							$_FILES['accom_image']['size']	= $files['accom_image']['size'][$i];
							if($this->upload->do_upload('accom_image'))
							{
								$dt=$this->upload->data();
								$file_name=$dt['file_name'];
								$_POST['accom_id']=$accom_id;
								$_POST['accom_image_name']=$file_name;
								$this->master_model->insertRecord('tbl_accommodations_images',$_POST);
							}
						}
					 }
			  }
			  $this->session->set_flashdata('success','accommodation added successfully.');
		      redirect(base_url().'owner/profile');
		 }
	  }
	  //tourssubmit
	  if(isset($_POST['tourssubmit']))
	  {
		 
		 $_POST['form_added_date']=date('Y-m-d'); 
		 $_POST['form_type']='Tours';
		 $config=array('upload_path'=>'uploads/forms/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['form_image']['name']!='')
			{
				if($this->upload->do_upload('form_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['form_image']=$dt['file_name'];
				}
				else
				{
					//$_POST['form_image']=$_POST['old_pageImage'];
					$data['error']=$this->upload->display_errors();
				}
			}
			else
			{
				$_POST['form_image']='';
			} 
			
		 $_POST['hosts_id']=$this->session->userdata('userID');
		 if($this->master_model->insertRecord('tbl_three_forms',$_POST))
		 {
			    $insert_id=$this->db->insert_id();
				if(count($_POST['form_available_day'])>0)
				{
					for($i=0;$i<count($_POST['form_available_day']);$i++)
					{
						$dtf=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_availalbe_from_time_'.$_POST['form_available_day'][$i]]));
						$dtt=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_availalbe_to_time_'.$_POST['form_available_day'][$i]]));
						$this->master_model->insertRecord('tbl_available_days',array('hosts_id'=>$_POST['hosts_id'],'formID'=>$insert_id,'day'=>$_POST['form_available_day'][$i],'form_available_from_time'=>$dtf,'form_available_to_time'=>$dtt,'available_form_type'=>'Tours'));
					}
				}
			    $files = $_FILES;
			    if(isset($files['form_images_name']['name']) && count($files['form_images_name']['name'])>0 && $files['form_images_name']['name'][0]!='')
			    {
					$gallry_img=count($files['form_images_name']['name']); 
					$config=array('upload_path'=>'uploads/form/', 
								  'file_ext_tolower' =>TRUE,
								  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
								  'file_name'=>time().rand(1,9999)
								  );
					$this->upload->initialize($config);
					$flag=1; 		
					for($i=0;$i<$gallry_img;$i++)
					{  
						if($files['form_images_name']['name'][$i]!='')
						{
							$_FILES['form_images_name']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['form_images_name']['name'][$i]));
							$_FILES['form_images_name']['type']	= $files['form_images_name']['type'][$i];
							$_FILES['form_images_name']['tmp_name']= $files['form_images_name']['tmp_name'][$i];
							$_FILES['form_images_name']['error']	= $files['form_images_name']['error'][$i];
							$_FILES['form_images_name']['size']	= $files['form_images_name']['size'][$i];
							if($this->upload->do_upload('form_images_name'))
							{
								$dt=$this->upload->data();
								$file_name=$dt['file_name'];
								$_POST['form_id']=$insert_id;
								$_POST['form_images_name']=$file_name;
								$this->master_model->insertRecord('tbl_threeforms_images',$_POST);
							}
						}
					 }
			    }
			    $this->session->set_flashdata('success','Form added successfully.');
		        redirect(base_url().'owner/profile');
		 }
	  }
	  //adventuresubmit
	  if(isset($_POST['adventuresubmit']))
	  {
		 $_POST['form_added_date']=date('Y-m-d'); 
		 $_POST['form_type']='Adventure'; 
		 $_POST['hosts_id']=$this->session->userdata('userID');
		 $config=array('upload_path'=>'uploads/forms/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['form_image']['name']!='')
			{
				if($this->upload->do_upload('form_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['form_image']=$dt['file_name'];
				}
				else
				{
					//$_POST['form_image']=$_POST['old_pageImage'];
					$data['error']=$this->upload->display_errors();
				}
			}
			else
			{
				$_POST['form_image']='';
			}
			
		 if($this->master_model->insertRecord('tbl_three_forms',$_POST))
		 {
			 $insert_id=$this->db->insert_id();
			 if(count($_POST['form_available_day'])>0)
			 {
					for($i=0;$i<count($_POST['form_available_day']);$i++)
					{
						
						$dtf=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_availalbe_from_time_'.$_POST['form_available_day'][$i]]));
						$dtt=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_availalbe_to_time_'.$_POST['form_available_day'][$i]]));
						$this->master_model->insertRecord('tbl_available_days',array('hosts_id'=>$_POST['hosts_id'],'formID'=>$insert_id,'day'=>$_POST['form_available_day'][$i],'form_available_from_time'=>$dtf,'form_available_to_time'=>$dtt,'available_form_type'=>'Adventure'));
					}
				}
			 $files = $_FILES;
			 if(isset($files['form_images_name']['name']) && count($files['form_images_name']['name'])>0 && $files['form_images_name']['name'][0]!='')
			 { 
				$gallry_img=count($files['form_images_name']['name']); 
				$config=array('upload_path'=>'uploads/form/', 
							  'file_ext_tolower' =>TRUE,
							  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
							  'file_name'=>time().rand(1,9999)
							  );
				$this->upload->initialize($config);
				$flag=1; 		
				for($i=0;$i<$gallry_img;$i++)
				{  
					if($files['form_images_name']['name'][$i]!='')
					{
						$_FILES['form_images_name']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['form_images_name']['name'][$i]));
						$_FILES['form_images_name']['type']	= $files['form_images_name']['type'][$i];
						$_FILES['form_images_name']['tmp_name']= $files['form_images_name']['tmp_name'][$i];
						$_FILES['form_images_name']['error']	= $files['form_images_name']['error'][$i];
						$_FILES['form_images_name']['size']	= $files['form_images_name']['size'][$i];
						if($this->upload->do_upload('form_images_name'))
						{
							$dt=$this->upload->data();
							$file_name=$dt['file_name'];
							$_POST['form_id']=$insert_id;
							$_POST['form_images_name']=$file_name;
							$this->master_model->insertRecord('tbl_threeforms_images',$_POST);
						}
					}
				 }
			 }
		     $this->session->set_flashdata('success','Form added successfully.');
		     redirect(base_url().'owner/profile');
		 }
	  }
	  //recreationsubmit
	  if(isset($_POST['recreationsubmit']))
	  {
		 $_POST['form_added_date']=date('Y-m-d'); 
		 $_POST['form_type']='Recreation'; 
		 $_POST['hosts_id']=$this->session->userdata('userID');
		 $config=array('upload_path'=>'uploads/forms/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['form_image']['name']!='')
			{
				if($this->upload->do_upload('form_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['form_image']=$dt['file_name'];
				}
				else
				{
					//$_POST['form_image']=$_POST['old_pageImage'];
					$data['error']=$this->upload->display_errors();
				}
			}
			else
			{
				$_POST['form_image']='';
			}
			
		 if($this->master_model->insertRecord('tbl_three_forms',$_POST))
		 {
			 $insert_id=$this->db->insert_id();
			 if(count($_POST['form_available_day'])>0)
			 {
					for($i=0;$i<count($_POST['form_available_day']);$i++)
					{
						$dtf=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_availalbe_from_time_'.$_POST['form_available_day'][$i]]));
						$dtt=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_availalbe_to_time_'.$_POST['form_available_day'][$i]]));
						$this->master_model->insertRecord('tbl_available_days',array('hosts_id'=>$_POST['hosts_id'],'formID'=>$insert_id,'day'=>$_POST['form_available_day'][$i],'form_available_from_time'=>$dtf,'form_available_to_time'=>$dtt,'available_form_type'=>'Recreation'));
					}
				}
			 $files = $_FILES;
			 if(isset($files['form_images_name']['name']) && count($files['form_images_name']['name'])>0 && $files['form_images_name']['name'][0]!='')
			 { 
				$gallry_img=count($files['form_images_name']['name']); 
				$config=array('upload_path'=>'uploads/form/', 
							  'file_ext_tolower' =>TRUE,
							  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
							  'file_name'=>time().rand(1,9999)
							  );
				$this->upload->initialize($config);
				$flag=1; 		
				for($i=0;$i<$gallry_img;$i++)
				{  
					if($files['form_images_name']['name'][$i]!='')
					{
						$_FILES['form_images_name']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['form_images_name']['name'][$i]));
						$_FILES['form_images_name']['type']	= $files['form_images_name']['type'][$i];
						$_FILES['form_images_name']['tmp_name']= $files['form_images_name']['tmp_name'][$i];
						$_FILES['form_images_name']['error']	= $files['form_images_name']['error'][$i];
						$_FILES['form_images_name']['size']	= $files['form_images_name']['size'][$i];
						if($this->upload->do_upload('form_images_name'))
						{
							$dt=$this->upload->data();
							$file_name=$dt['file_name'];
							$_POST['form_id']=$insert_id;
							$_POST['form_images_name']=$file_name;
							$this->master_model->insertRecord('tbl_threeforms_images',$_POST);
						}
					}
				 }
			 }
		     $this->session->set_flashdata('success','Form added successfully.');
		     redirect(base_url().'owner/profile');
		 }
	  }
	  if(isset($_POST['couponsubmit']))
	  {
		 $_POST['coupon_added_date']=date('Y-m-d'); 
		 $_POST['hosts_id']=$this->session->userdata('userID');
		 if($this->master_model->insertRecord('tbl_coupon_master',$_POST))
		 {
			 $coupon_id=$this->db->insert_id();
		     $files = $_FILES;
			 if(isset($files['coupon_images']['name']) && count($files['coupon_images']['name'])>0 && $files['coupon_images']['name'][0]!='')
			 {
					$gallry_img=count($files['coupon_images']['name']); 
					$config=array('upload_path'=>'uploads/coupon/', 
								  'file_ext_tolower' =>TRUE,
								  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
								  'file_name'=>time().rand(1,9999)
								  );
					$this->upload->initialize($config);
					$flag=1; 		
					for($i=0;$i<$gallry_img;$i++)
					{  
						if($files['coupon_images']['name'][$i]!='')
						{
							$_FILES['coupon_images']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['coupon_images']['name'][$i]));
							$_FILES['coupon_images']['type']	= $files['coupon_images']['type'][$i];
							$_FILES['coupon_images']['tmp_name']= $files['coupon_images']['tmp_name'][$i];
							$_FILES['coupon_images']['error']	= $files['coupon_images']['error'][$i];
							$_FILES['coupon_images']['size']	= $files['coupon_images']['size'][$i];
							if($this->upload->do_upload('coupon_images'))
							{
								$dt=$this->upload->data();
								$file_name=$dt['file_name'];
								$_POST['coupon_id']=$coupon_id;
								$_POST['coupon_images_name']=$file_name;
								$this->master_model->insertRecord('tbl_coupon_images',$_POST);
							}
						}
					 }
			  }	 
			 $this->session->set_flashdata('success','Coupon added successfully.');
		     redirect(base_url().'owner/profile');
		 }
	  }
	  if(isset($_POST['eventsubmit']))
	  {
		 $_POST['event_added_date']=date('Y-m-d'); 
		 $_POST['hosts_id']=$this->session->userdata('userID');
		 $config=array('upload_path'=>'uploads/event/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['event_image']['name']!='')
			{
				if($this->upload->do_upload('event_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['event_image']=$dt['file_name'];
				}
				else
				{
					//$_POST['form_image']=$_POST['old_pageImage'];
					$data['error']=$this->upload->display_errors();
				}
			}
			else
			{
				$_POST['event_image']='';
			}
			if($this->master_model->insertRecord('tbl_event_master',$_POST))
		    {
				 $event_id=$this->db->insert_id();
				 $files = $_FILES;
				 if(isset($files['event_images_name']['name']) && count($files['event_images_name']['name'])>0 && $files['event_images_name']['name'][0]!='')
				 {
						$gallry_img=count($files['event_images_name']['name']); 
						$config=array('upload_path'=>'uploads/event/', 
									  'file_ext_tolower' =>TRUE,
									  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
									  'file_name'=>time().rand(1,9999)
									  );
						$this->upload->initialize($config);
						$flag=1; 		
						for($i=0;$i<$gallry_img;$i++)
						{  
							if($files['event_images_name']['name'][$i]!='')
							{
								$_FILES['event_images_name']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['event_images_name']['name'][$i]));
								$_FILES['event_images_name']['type']	= $files['event_images_name']['type'][$i];
								$_FILES['event_images_name']['tmp_name']= $files['event_images_name']['tmp_name'][$i];
								$_FILES['event_images_name']['error']	= $files['event_images_name']['error'][$i];
								$_FILES['event_images_name']['size']	= $files['event_images_name']['size'][$i];
								if($this->upload->do_upload('event_images_name'))
								{
									$dt=$this->upload->data();
									$file_name=$dt['file_name'];
									$_POST['event_id']=$event_id;
									$_POST['event_images_name']=$file_name;
									$this->master_model->insertRecord('tbl_coupon_images',$_POST);
								}
							}
						 }
				  }	 
		          $this->session->set_flashdata('success','Event added successfully.');
		          redirect(base_url().'owner/profile');
		    }
	  }
	  if(isset($_POST['transportationsubmit']))
	  {
		 $_POST['transportation_added_date']=date('Y-m-d'); 
		 $_POST['hosts_id']=$this->session->userdata('userID');
		  $config=array('upload_path'=>'uploads/transportation/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['transportation_image']['name']!='')
			{
				if($this->upload->do_upload('transportation_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['transportation_image']=$dt['file_name'];
				}
				else
				{
					//$_POST['form_image']=$_POST['old_pageImage'];
					$data['error']=$this->upload->display_errors();
				}
			}
			else
			{
				$_POST['transportation_image']='';
			}
		 if($this->master_model->insertRecord('tbl_transportation_master',$_POST))
		 {
		     $transportation_id=$this->db->insert_id();
			 $files = $_FILES;
			 if(isset($files['transportation_image_name']['name']) && count($files['transportation_image_name']['name'])>0 && $files['transportation_image_name']['name'][0]!='')
			 {
					$gallry_img=count($files['transportation_image_name']['name']); 
					$config=array('upload_path'=>'uploads/transportation/', 
								  'file_ext_tolower' =>TRUE,
								  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
								  'file_name'=>time().rand(1,9999)
								  );
					$this->upload->initialize($config);
					$flag=1; 		
					for($i=0;$i<$gallry_img;$i++)
					{  
						if($files['transportation_image_name']['name'][$i]!='')
						{
							$_FILES['transportation_image_name']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['transportation_image_name']['name'][$i]));
							$_FILES['transportation_image_name']['type']	= $files['transportation_image_name']['type'][$i];
							$_FILES['transportation_image_name']['tmp_name']= $files['transportation_image_name']['tmp_name'][$i];
							$_FILES['transportation_image_name']['error']	= $files['transportation_image_name']['error'][$i];
							$_FILES['transportation_image_name']['size']	= $files['transportation_image_name']['size'][$i];
							if($this->upload->do_upload('transportation_image_name'))
							{
								$dt=$this->upload->data();
								$file_name=$dt['file_name'];
								$_POST['transportation_id']=$event_id;
								$_POST['transportation_image_name']=$file_name;
								$this->master_model->insertRecord('tbl_coupon_images',$_POST);
							}
						}
					}
			  }	 	 
		      $this->session->set_flashdata('success','Transportation added successfully.');
		      redirect(base_url().'owner/profile');
		 }
	  }
	  $data['fetch_array']=$this->master_model->getRecords('tbl_hosts_master',array('hosts_id'=>$this->session->userdata('userID')));
	  $data['categories']=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active'));
	  $data['middle_content']='owner-account';
	  $this->load->view('common-file',$data);		
	}
	public function updateaccom($accom_id)
	{
		
		if($this->master_model->updateRecordf('tbl_accommodations_master',$_POST,array('hosts_id'=>$this->session->userdata('userID'),'accom_id'=>$accom_id)))
		{	
			$this->session->set_flashdata('success','Accomodation update successfully.');
			redirect(base_url().'owner/profile');
		}
		else
		{
			$this->session->set_flashdata('error','Oops ! something wrong. Please try sometimes leter.');
			redirect(base_url().'owner/profile');
		}
	}
	public function updateform($common_id)
	{
		$this->load->library('upload');	
		if(isset($_POST['accommodationssubmit']))
	    {
			if($this->master_model->updateRecordf('tbl_accommodations_master',$_POST,array('hosts_id'=>$this->session->userdata('userID'),'accom_id'=>$common_id))){$this->session->set_flashdata('success','Accomodation update successfully.');}
			else
			{$this->session->set_flashdata('error','Oops ! something wrong. Please try sometimes leter.');}
		}
		if(isset($_POST['couponsubmit']))
		{
		   if($this->master_model->updateRecordf('tbl_coupon_master',$_POST,array('hosts_id'=>$this->session->userdata('userID'),'coupon_id'=>$common_id))){$this->session->set_flashdata('success','Coupon update successfully.');}
		   else
		   {$this->session->set_flashdata('error','Oops ! something wrong. Please try sometimes leter.');}	
		}
		if(isset($_POST['eventsubmit']))
	    {
		  $userInfo=$this->master_model->getRecords('tbl_event_master',array('event_id'=>$common_id));
		  $config=array('upload_path'=>'uploads/event/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			$_POST['event_image']=$userInfo[0]['event_image'];
			if($_FILES['event_image']['name']!='')
			{
				if($this->upload->do_upload('event_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['event_image']=$dt['file_name'];
				}
			}
			if($this->master_model->updateRecordf('tbl_event_master',$_POST,array('hosts_id'=>$this->session->userdata('userID'),'coupon_id'=>$common_id)))
		    {$this->session->set_flashdata('success','Event update successfully.');}
			else
			{$this->session->set_flashdata('error','Oops ! something wrong. Please try sometimes leter.');}
		}
	    if(isset($_POST['transportationsubmit']))
	    {
		  $transportationsubmitInfo=$this->master_model->getRecords('tbl_transportation_master',array('transportation_id'=>$common_id));
		  $config=array('upload_path'=>'uploads/transportation/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			$_POST['transportation_image']=$transportationsubmitInfo[0]['transportation_image'];
			if($_FILES['transportation_image']['name']!='')
			{
				if($this->upload->do_upload('transportation_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['transportation_image']=$dt['file_name'];
				}
				
			}
			if($this->master_model->updateRecordf('tbl_transportation_master',$_POST,array('hosts_id'=>$this->session->userdata('userID'),'transportation_id'=>$common_id)))
		    {$this->session->set_flashdata('success','Transportation update successfully.');}
			else
			{$this->session->set_flashdata('error','Oops ! something wrong. Please try sometimes leter.');}
		}
		redirect(base_url().'owner/profile');
	}
	public function updatetours($formsID)
	{
		$this->load->library('upload');
		$config=array('upload_path'=>'uploads/forms/',
					  'allowed_types'=>'jpg|jpeg|gif|png',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['form_image']['name']!='')
			{
				if($this->upload->do_upload('form_image'))
				{
				  $dt=$this->upload->data();
				  $_POST['form_image']=$dt['file_name'];
				}
				else
				{
					$_POST['form_image']=$_POST['old_img'];
					$data['error']=$this->upload->display_errors();
				}
			}
			else
			{
				$_POST['form_image']=$_POST['old_img'];
			} 
			//print_r($_POST);exit;
		if($this->master_model->updateRecordf('tbl_three_forms',$_POST,array('hosts_id'=>$this->session->userdata('userID'),'formsID'=>$formsID)))
		{	
		$form_type=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID),'form_type');
		$this->master_model->deleteRecord('tbl_available_days','formID',$formsID);
			for($i=0;$i<count($_POST['form_available_day']);$i++)
			{
				$dtf=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_available_from_time_'.$_POST['form_available_day'][$i]]));
				$dtt=date('Y-m-d H:i:s',strtotime('01-08-2016 '.$_POST['form_available_to_time_'.$_POST['form_available_day'][$i]]));
				$this->master_model->insertRecord('tbl_available_days',array('hosts_id'=>$this->session->userdata('userID'),'formID'=>$formsID,'day'=>$_POST['form_available_day'][$i],'form_available_from_time'=>$dtf,'form_available_to_time'=>$dtt,'available_form_type'=>$form_type[0]['form_type']));
			}
			$this->session->set_flashdata('success','form update successfully.');
			redirect(base_url().'owner/profile');
		}
		else
		{
			$this->session->set_flashdata('error','Oops ! something wrong. Please try sometimes leter.');
			redirect(base_url().'owner/profile');
		}
	}
}