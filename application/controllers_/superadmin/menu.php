<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	}
	
	
	public function manage()
	{
	  $data['pagetitle']='Main Menu';
	  $data['error']='';
	    if(isset($_POST['action']) && $_POST['action']=='2')
	    {
			if(count($_POST['checkbox_del'])!= 0)
			{
				$cnt_checkbox_del=count($_POST['checkbox_del']); 
				for($i=0;$i<$cnt_checkbox_del;$i++)
				{
					$this->master_model->deleteRecord('tbl_menu_master','menu_id',$_POST['checkbox_del'][$i]);
				}
				$this->session->set_flashdata('success','Record(s) delete Successfully.');
				redirect(base_url().'superadmin/menu/manage');
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/menu/manage');
			}
	    }
	    if(isset($_POST['action']) && $_POST['action']=='0')
	    {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='0';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_menu_master',array('menu_status'=>$stat),array('menu_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/menu/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
				    redirect(base_url().'superadmin/menu/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/menu/manage');
			}
			
		} 
		if(isset($_POST['action'])  && $_POST['action']=='1')
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='1';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_menu_master',array('menu_status'=>$stat),array('menu_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/menu/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/menu/manage');;
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/menu/manage');
			}
		}
	   $this->db->join('tbl_front_page','tbl_front_page.front_id=tbl_menu_master.page_id','left');
	   $data['fetch_menu']=$this->master_model->getRecords('tbl_menu_master');
	   $data['middle_content']='manage-menu';
	   $this->load->view('admin/common-file',$data);
	}
	public function statusmenu($menu_id,$menustatus)
	{
		$data['success']=$data['error']='';
		$input_array = array('menu_status'=>$menustatus);
		if($this->master_model->updateRecord('tbl_menu_master',$input_array,array('menu_id'=>$menu_id)))
		{
		   $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/menu/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/menu/manage/');
		}
	}
	public function delete($menu_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_menu_master','menu_id',$menu_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/menu/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/menu/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Main Menu';
		  $data['pages']=$this->master_model->getRecords('tbl_front_page',array('front_status'=>'1'));
		  if(isset($_POST['btn_menu']))
		  {
			  $this->form_validation->set_rules('menu_name_eng','menu Name English','required|xss_clean|is_unique[tbl_menu_master.menu_name_eng]');
			  $this->form_validation->set_rules('menu_name_arb','menu Name Arabic','required|xss_clean|is_unique[tbl_menu_master.menu_name_arb]');
			  //$this->form_validation->set_rules('page_id','Page','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $menu_name_eng=$this->input->post('menu_name_eng',true);
				  @$menu_name_arb=$this->input->post('menu_name_arb',true);
				  $page_id=$this->input->post('page_id',true);
				  
				  $menu_slug=$this->master_model->create_slug($menu_name_eng,'tbl_menu_master','menu_slug');
				  $input_array=array('menu_name_eng'=>addslashes($menu_name_eng),'menu_status'=>'1','menu_slug'=>$menu_slug,'page_id'=>$page_id,'menu_name_arb'=>$menu_name_arb);
				   if($this->master_model->insertRecord('tbl_menu_master',$input_array))
				  { 
					  $this->session->set_flashdata('success','Record Added successfully.');
					  redirect(base_url().'superadmin/menu/add/');
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Error while Adding record.'); 
					  redirect(base_url().'superadmin/menu/add/');
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='add-menu';
		  $this->load->view('admin/common-file',$data);
	}
	public function update()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Main Menu';
		  $menu_id=$this->uri->segment('4');
		  $data['pages']=$this->master_model->getRecords('tbl_front_page',array('front_status'=>'1'));
		  $data['categories']=$this->master_model->getRecords('tbl_menu_master',array('menu_id'=>$menu_id));
		
		  if(isset($_POST['btn_menu']))
		  {
			  $this->form_validation->set_rules('menu_name_eng','menu Name English','required|xss_clean');
			  $this->form_validation->set_rules('menu_name_arb','menu Name Arabic','required|xss_clean');
			  //$this->form_validation->set_rules('page_id','Page','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $menu_name_eng=$this->input->post('menu_name_eng',true);
				  $menu_name_arb=$this->input->post('menu_name_arb',true);
				  $page_id=$this->input->post('page_id',true);
				  
				 
				  $menu_slug=$this->master_model->create_slug($menu_name_eng,'tbl_menu_master','menu_slug','menu_id',$menu_id);
				  $input_array=array('menu_name_eng'=>addslashes($menu_name_eng),'menu_status'=>'1','menu_slug'=>$menu_slug,'page_id'=>$page_id,'menu_name_arb'=>$menu_name_arb);
				  $num_rows_array=array('menu_name_eng'=>$menu_name_eng,'page_id'=>$page_id);
				  $this->db->where('menu_id !=', $menu_id); 
				  $getRecords=$this->master_model->getRecords('tbl_menu_master',$num_rows_array);
				  if(count($getRecords)==0)
				  {
				      if($this->master_model->updateRecord('tbl_menu_master',$input_array,array('menu_id'=>$menu_id)))
					  { 
						 $this->session->set_flashdata('success','Record Added successfully.');
						 redirect(base_url().'superadmin/menu/update/'.$menu_id);
					  }
					  else
					  {
						 $this->session->set_flashdata('error','Error while Adding record.'); 
						 redirect(base_url().'superadmin/menu/update/'.$menu_id);
					  }
				  }
				  else
				  {
					 $data['error']='Record already exist !'; 
					 //redirect(base_url().'superadmin/menu/update/'.$menu_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-menu';
		  $this->load->view('admin/common-file',$data);
	}
	
}