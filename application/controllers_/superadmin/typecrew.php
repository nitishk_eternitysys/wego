<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Typecrew extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Type crew';
	  $data['pageLable']='Type crew';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_typecrew_master','crew_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_typecrew_master',array('crew_status'=>$stat),array('crew_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_typecrew_master',array('crew_status'=>$stat),array('crew_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_typecrew_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$crew_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('crew_status'=>$status);
		if($this->master_model->updateRecord('tbl_typecrew_master',$input_array,array('crew_id'=>$crew_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($crew_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_typecrew_master','crew_id',$crew_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Type crew';
		  $data['pageLable']='Type crew';
		  if(isset($_POST['btn_add']))
		  {
			  $this->form_validation->set_rules('crew_name_eng','Type crew english','required|xss_clean|is_unqie');
			  $this->form_validation->set_rules('crew_name_arb','Type crew Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $testimonial_image='';
				  $crew_name_eng=$this->input->post('crew_name_eng',true);
				  $crew_name_arb=$this->input->post('crew_name_arb',true);
				 
				  $crew_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_typecrew_master',array('crew_name_eng'=>$crew_name_eng,'crew_name_arb'=>$crew_name_arb));
				  if($checkDub==0)
				  {
					  $crew_slug=$this->master_model->create_slug($crew_name_eng,'tbl_typecrew_master','crew_name_eng');
					  $input_array=array('crew_name_eng'=>addslashes($crew_name_eng),'crew_name_arb'=>$crew_name_arb,'crew_status'=>$crew_status,'crew_slug'=>$crew_slug);
					  if($this->master_model->insertRecord('tbl_typecrew_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					  $data['error']='Type crew already exist !';  
				  }
				  
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($crew_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Type crew';
		  $data['pageLable']='Type crew';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_typecrew_master',array('crew_id'=>$crew_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('crew_name_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('crew_name_arb','Testimonial Description Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $crew_name_eng=$this->input->post('crew_name_eng',true);
				  $crew_name_arb=$this->input->post('crew_name_arb',true);
				  $crew_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_typecrew_master',array('crew_name_eng'=>$crew_name_eng,'crew_name_arb'=>$crew_name_arb,'crew_id !='=>$crew_id));
				  if($checkDub==0)
				  {
					  $crew_slug=$this->master_model->create_slug($crew_name_eng,'tbl_typecrew_master','crew_name_eng');
					  $input_array=array('crew_name_eng'=>addslashes($crew_name_eng),'crew_name_arb'=>$crew_name_arb,'crew_status'=>$crew_status,'crew_slug'=>$crew_status);
					  if($this->master_model->updateRecord('tbl_typecrew_master',$input_array,array('crew_id'=>$crew_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$crew_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$crew_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$crew_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
}