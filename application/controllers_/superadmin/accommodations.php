<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Accommodations extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Accommodations';
	  $data['pageLable']='Accommodations';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_accommodations_master','accom_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_accommodations_master',array('accom_status'=>$stat),array('accom_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
				      $this->master_model->updateRecord('tbl_accommodations_master',array('accom_status'=>$stat),array('accom_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	    $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_accommodations_master.hosts_id');	
	    $data['fetch_arr']=$this->master_model->getRecords('tbl_accommodations_master');
	    $data['middle_content']='manage-'.$this->router->fetch_class();
	    $this->load->view('admin/common-file',$data);
	}
	public function status($status,$accom_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('accom_status'=>$status);
		if($this->master_model->updateRecord('tbl_accommodations_master',$input_array,array('accom_id'=>$accom_id)))
		{
		   $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($accom_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_accommodations_master','accom_id',$accom_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function details($accom_id='')
	{
	  $data['success']=$data['error']='';
	  $data['pagetitle']='Rehla ticket | '.$this->router->fetch_class().'';
	  $data['pageLable']='Manage '.$this->router->fetch_class().'';	
	  $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_accommodations_master.hosts_id');	
	  $data['arr_details']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$accom_id));
	  $data['middle_content']='details-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
}