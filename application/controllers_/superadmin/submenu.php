<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Submenu extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	}
	
	
	public function manage()
	{
	  $data['pagetitle']='Submenu';
	  $data['error']='';
	    if(isset($_POST['action']) && $_POST['action']=='2')
	    {
			if(count($_POST['checkbox_del'])!= 0)
			{
				$cnt_checkbox_del=count($_POST['checkbox_del']); 
				for($i=0;$i<$cnt_checkbox_del;$i++)
				{
					$this->master_model->deleteRecord('tbl_submenu_master','submenu_id',$_POST['checkbox_del'][$i]);
				}
				$this->session->set_flashdata('success','Record(s) delete Successfully.');
				redirect(base_url().'superadmin/submenu/manage');
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/submenu/manage');
			}
	    }
	    if(isset($_POST['action']) && $_POST['action']=='0')
	    {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='0';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_submenu_master',array('submenu_status'=>$stat),array('submenu_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/submenu/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
				    redirect(base_url().'superadmin/submenu/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/submenu/manage');
			}
			
		} 
		if(isset($_POST['action'])  && $_POST['action']=='1')
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='1';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_submenu_master',array('submenu_status'=>$stat),array('submenu_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/submenu/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/submenu/manage');;
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/submenu/manage');
			}
		}
	   $this->db->join('tbl_front_page','tbl_front_page.front_id=tbl_submenu_master.page_id','left');
	   $this->db->join('tbl_menu_master','tbl_menu_master.menu_id=tbl_submenu_master.menu_id');
	   $data['fetch_menu']=$this->master_model->getRecords('tbl_submenu_master');
	   $data['middle_content']='manage-submenu';
	   $this->load->view('admin/common-file',$data);
	}
	public function statusmenu($submenu_id,$menustatus)
	{
		$data['success']=$data['error']='';
		$input_array = array('submenu_status'=>$menustatus);
		if($this->master_model->updateRecord('tbl_submenu_master',$input_array,array('submenu_id'=>$submenu_id)))
		{
		   $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/submenu/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/submenu/manage/');
		}
	}
	public function delete($submenu_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_submenu_master','submenu_id',$submenu_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/submenu/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/submenu/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Main Submenu';
		  $data['pages']=$this->master_model->getRecords('tbl_front_page',array('front_status'=>'1'));
		  $data['menu']=$this->master_model->getRecords('tbl_menu_master',array('menu_status'=>'1'));
		  if(isset($_POST['btn_menu']))
		  {
			  $this->form_validation->set_rules('submenu_name_eng','menu Name English','required|xss_clean|is_unique[tbl_submenu_master.submenu_name_eng]');
			  $this->form_validation->set_rules('submenu_name_arb','menu Name Arabic','required|xss_clean|is_unique[tbl_submenu_master.submenu_name_arb]');
			  $this->form_validation->set_rules('menu_id','Main Menu','required|xss_clean');
			  //$this->form_validation->set_rules('page_id','Page','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $submenu_name_eng=$this->input->post('submenu_name_eng',true);
				  @$submenu_name_arb=$this->input->post('submenu_name_arb',true);
				  $page_id=$this->input->post('page_id',true);
				  $menu_id=$this->input->post('menu_id',true);
				  
				  $menu_slug=$this->master_model->create_slug($submenu_name_eng,'tbl_submenu_master','submenu_slug');
				  $input_array=array('submenu_name_eng'=>addslashes($submenu_name_eng),'submenu_status'=>'1','submenu_slug'=>$menu_slug,'page_id'=>$page_id,'menu_id'=>$menu_id,'submenu_name_arb'=>$submenu_name_arb);
				   if($this->master_model->insertRecord('tbl_submenu_master',$input_array))
				  { 
					  $this->session->set_flashdata('success','Record Added successfully.');
					  redirect(base_url().'superadmin/submenu/add/');
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Error while Adding record.'); 
					  redirect(base_url().'superadmin/submenu/add/');
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='add-submenu';
		  $this->load->view('admin/common-file',$data);
	}
	public function update()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Submenu';
		 	$subcategory_id=$this->uri->segment('4');
		   $data['submenu']=$this->master_model->getRecords('tbl_submenu_master',array('submenu_id'=>$subcategory_id));
		   $data['pages']=$this->master_model->getRecords('tbl_front_page',array('front_status'=>'1'));
		   $data['menu']=$this->master_model->getRecords('tbl_menu_master',array('menu_status'=>'1'));
		
		  if(isset($_POST['btn_menu']))
		  {
			  $this->form_validation->set_rules('submenu_name_eng','menu Name English','required|xss_clean');
			  $this->form_validation->set_rules('submenu_name_arb','menu Name Arabic','required|xss_clean');
			  $this->form_validation->set_rules('menu_id','Main Menu','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $submenu_name_eng=$this->input->post('submenu_name_eng',true);
				  $submenu_name_arb=$this->input->post('submenu_name_arb',true);
				  $page_id=$this->input->post('page_id',true);
				  $menu_id=$this->input->post('menu_id',true);
				 
				  $menu_slug=$this->master_model->create_slug($submenu_name_eng,'tbl_submenu_master','submenu_slug','submenu_id',$submenu_id);
				  $input_array=array('submenu_name_eng'=>addslashes($submenu_name_eng),'submenu_status'=>'1','submenu_slug'=>$menu_slug,'page_id'=>$page_id,'submenu_name_arb'=>$submenu_name_arb);
				  $num_rows_array=array('submenu_name_eng'=>$submenu_name_eng,'page_id'=>$page_id);
				  $this->db->where('submenu_id !=', $submenu_id); 
				  $getRecords=$this->master_model->getRecords('tbl_submenu_master',$num_rows_array);
				  if(count($getRecords)==0)
				  {
				      if($this->master_model->updateRecord('tbl_submenu_master',$input_array,array('submenu_id'=>$submenu_id)))
					  { 
						 $this->session->set_flashdata('success','Record Added successfully.');
						 redirect(base_url().'superadmin/submenu/update/'.$submenu_id);
					  }
					  else
					  {
						 $this->session->set_flashdata('error','Error while Adding record.'); 
						 redirect(base_url().'superadmin/submenu/update/'.$submenu_id);
					  }
				  }
				  else
				  {
					 $data['error']='Record already exist !'; 
					 //redirect(base_url().'superadmin/submenu/update/'.$submenu_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-menu';
		  $this->load->view('admin/common-file',$data);
	}
	
}