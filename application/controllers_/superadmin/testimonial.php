<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Testimonial extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Testimonial';
	  $data['pageLable']='Testimonial';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_testimonial_master','testimonial_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_testimonial_master',array('testimonial_status'=>$stat),array('testimonial_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_testimonial_master',array('testimonial_status'=>$stat),array('testimonial_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/testimonial/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/testimonial/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/testimonial/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_testimonial_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$testimonial_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('testimonial_status'=>$status);
		if($this->master_model->updateRecord('tbl_testimonial_master',$input_array,array('testimonial_id'=>$testimonial_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($testimonial_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_testimonial_master','testimonial_id',$testimonial_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Testimonial';
		  $data['pageLable']='Testimonial';
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('testimonial_desc_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('testimonial_desc_arb','Testimonial Description Arbic','required|xss_clean');
			  $this->form_validation->set_rules('testimonial_name','Name','required|xss_clean|is_unique[tbl_testimonial_master.testimonial_name]');
			  if($this->form_validation->run())
			  {
				  $testimonial_image='';
				  $testimonial_desc_eng=$this->input->post('testimonial_desc_eng',true);
				  $testimonial_desc_arb=$this->input->post('testimonial_desc_arb',true);
				  $testimonial_name=$this->input->post('testimonial_name',true);
				  $testimonial_status='active';
				  $testimonial_slug=$this->master_model->create_slug($testimonial_desc_eng,'tbl_testimonial_master','testimonial_desc_eng');
				  $config=array('upload_path'=>'./uploads/testimonial/',
					            'allowed_types'=>'jpg|jpeg|gif|png',
					            'file_name'=>rand(1,9999),'max_size'=>0);
				  $this->upload->initialize($config);	
				  if($_FILES['testimonial_image']['name']!='')
				  {
						if($this->upload->do_upload('testimonial_image'))
						{
						  $dt=$this->upload->data();
						  $testimonial_image=$dt['file_name'];
						}
						else
						{
						   $this->session->set_flashdata('error',$this->upload->display_errors()); 
						   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
						}
				  }
				  $input_array=array('testimonial_desc_eng'=>addslashes($testimonial_desc_eng),'testimonial_desc_arb'=>$testimonial_desc_arb,'testimonial_status'=>$testimonial_status,'testimonial_slug'=>$testimonial_slug,'testimonial_name'=>$testimonial_name,'testimonial_image'=>$testimonial_image);
				  if($this->master_model->insertRecord('tbl_testimonial_master',$input_array))
				  { 
				      $this->session->set_flashdata('success','Record Added successfully.');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Error while Adding record.'); 
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($testimonial_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Testimonial';
		  $data['pageLable']='Testimonial';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_testimonial_master',array('testimonial_id'=>$testimonial_id));
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('testimonial_desc_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('testimonial_desc_arb','Testimonial Description Arbic','required|xss_clean');
			  $this->form_validation->set_rules('testimonial_name','Name','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $testimonial_desc_eng=$this->input->post('testimonial_desc_eng',true);
				  $testimonial_desc_arb=$this->input->post('testimonial_desc_arb',true);
				  $testimonial_name=$this->input->post('testimonial_name',true);
				  $testimonial_status='active';
				  $testimonial_image=$data['fetch_single_arr'][0]['testimonial_image'];
				  $checkDub=$this->master_model->getRecordCount('tbl_testimonial_master',array('testimonial_name'=>$testimonial_name,'testimonial_id !='=>$testimonial_id));
				  if($checkDub==0)
				  {
					  $testimonial_slug=$this->master_model->create_slug($testimonial_desc_eng,'tbl_testimonial_master','testimonial_desc_eng');
					  $config=array('upload_path'=>'./uploads/testimonial/',
									'allowed_types'=>'jpg|jpeg|gif|png',
									'file_name'=>rand(1,9999),'max_size'=>0);
					  $this->upload->initialize($config);	
					  if($_FILES['testimonial_image']['name']!='')
					  {
							if($this->upload->do_upload('testimonial_image'))
							{
							  $dt=$this->upload->data();
							  $testimonial_image=$dt['file_name'];
							}
							else
							{
							   $this->session->set_flashdata('error',$this->upload->display_errors()); 
							   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
							}
					  }
					  $input_array=array('testimonial_desc_eng'=>addslashes($testimonial_desc_eng),'testimonial_desc_arb'=>$testimonial_desc_arb,'testimonial_status'=>$testimonial_status,'testimonial_slug'=>$testimonial_slug,'testimonial_name'=>$testimonial_name,'testimonial_image'=>$testimonial_image);
					  if($this->master_model->updateRecord('tbl_testimonial_master',$input_array,array('testimonial_id'=>$testimonial_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$testimonial_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$testimonial_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$testimonial_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
}