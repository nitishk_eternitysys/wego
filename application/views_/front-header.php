<!DOCTYPE html>
<html class="load-full-screen"><head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="LimpidThemes">
	<title>Rehla Ticket</title>
	<base href="<?php echo base_url(); ?>">
    <!-- STYLES -->
	<link href="front/css/animate.min.css" rel="stylesheet">
	<link href="front/css/bootstrap-select.min.css" rel="stylesheet">
	<link href="front/css/owl.carousel.css" rel="stylesheet">
	<link href="front/css/owl-carousel-theme.css" rel="stylesheet">
    <link href="front/css/flexslider.css" rel="stylesheet" media="screen">
	<?php if($this->session->userdata('lang')=='eng'){ ?>
    <link href="front/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="front/css/style.css" rel="stylesheet" media="screen">
    <?php }else{ ?> 
    <link href="front/css/bootstrap.min-arb.css" rel="stylesheet">
    <link href="front/css/style-arb.css" rel="stylesheet" media="screen">
    <?php } ?>
	<!-- LIGHT -->
	<link rel="stylesheet" type="text/css" href="front/css/dummy.html" id="select-style">
	<link href="front/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="front/css/light.css" rel="stylesheet" media="screen">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600' rel='stylesheet' type='text/css'>
    <link href="css/star-rating.css" rel="stylesheet" media="screen">
    <script src="front/js/jquery.js"></script>
    <script src="js/star-rating.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
    <script type="text/javascript">
	var siteurl='<?php echo base_url(); ?>';
	$(document).ready(function(){
		   $('#input-2').rating();
		});
	</script>
    <script src="<?php echo base_url(); ?>js/markerclusterer_packed.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places"></script> 
    <style type="text/css">
	.error{color:#F00;}
	</style>
</head>
<body class="load-full-screen">
<!-- BEGIN: PRELOADER -->
<div id="loader" class="load-full-screen">
	<div class="loading-animation">
		<span><i class="fa fa-plane"></i></span>
		<span><i class="fa fa-bed"></i></span>
		<span><i class="fa fa-ship"></i></span>
		<span><i class="fa fa-suitcase"></i></span>
	</div>
</div>
<!-- END: PRELOADER -->
<?php $adminLogin=$this->master_model->getRecords('tbl_admin_login',array('id'=>'1')) ?>
<!-- BEGIN: SITE-WRAPPER -->
<div class="site-wrapper">
<div class="row transparent-menu-top">
		<div class="container clear-padding">
			<div class="navbar-contact">
				<div class="col-md-7 col-sm-6 clear-padding">
                    <a href="javascript:void(0);" class="transition-effect"><i class="fa fa-phone"></i> <?php echo $adminLogin[0]['admin_phone']; ?></a>
					<a href="mailTo:<?php echo $adminLogin[0]['admin_email']; ?>" class="transition-effect"><i class="fa fa-envelope-o"></i> <?php echo $adminLogin[0]['admin_email']; ?></a>
				</div>
				<div class="col-md-5 col-sm-6 clear-padding search-box">
<!--
					<div class="col-md-6 col-xs-5 clear-padding">
						<form >
							<div class="input-group">
								<input type="text" name="search" class="form-control" required placeholder="Search">
								<span class="input-group-addon"><i class="fa fa-search fa-fw"></i></span>
							</div>
						</form>
					</div>
-->
                    <?php if($this->session->userdata('userID')==TRUE){ ?>
					<div class="clear-padding user-logged"> 
                       <div class="flag-box">
                            <?php  if($this->session->userdata('lang')!='arb'){ ?><!--class="flag-lag"-->
                            <div ><a href="<?php echo base_url().'home/langchange/arb'; ?>"><!--<img src="<?php echo base_url(); ?>front/images/sa-flag.png" alt="flag">-->Arbic</a></div>
                            <?php  }else{ ?><!--class="flag-lag"-->
                            <div ><a href="<?php echo base_url().'home/langchange/eng'; ?>"><!--<img src="<?php echo base_url(); ?>front/images/uk-flag.png" alt="flag">-->English</a></div>
                           <?php } ?> 
                       </div>
                       <a href="<?php echo base_url().$this->session->userdata('userType'); ?>/profile" class="transition-effect">
                        <?php if($this->session->userdata('profile_pic')!=''){ ?>
							<img src="uploads/profile/<?php echo $this->session->userdata('profile_pic'); ?>" alt="<?php echo $this->session->userdata('userFName'); ?>"><?php } ?>Hi, <?php echo $this->session->userdata('userFName'); ?>
						</a>
						<a href="<?php echo base_url(); ?>home/logout" class="transition-effect">
							<i class="fa fa-sign-out"></i>Sign Out
						</a>
					</div>
                    <?php }else{ ?>
                    <div class="col-md-6 col-xs-7 clear-padding user-logged">
                     <div class="flag-box">
                            <?php  if($this->session->userdata('lang')!='arb'){ ?>
                            <div class="flag-lag"><a href="<?php echo base_url().'home/langchange/arb'; ?>"><img src="<?php echo base_url(); ?>front/images/sa-flag.png" alt="flag"></a></div>
                            <?php  }else{ ?>
                            <div class="flag-lag"><a href="<?php echo base_url().'home/langchange/eng'; ?>"><img src="<?php echo base_url(); ?>front/images/uk-flag.png" alt="flag"></a></div>
                           <?php } ?> 
                      </div>
					   <a href="<?php echo base_url(); ?>home/login" class="transition-effect login-link"><i class="fa fa-sign-out"></i>Login</a>
					</div>
                    <?php } ?>
				</div>
			</div>
		</div>
	</div>
<div class="clearfix"></div>
<div class="row transparent-menu">
		<div class="container clear-padding">
			<!-- BEGIN: HEADER -->
			<div class="navbar-wrapper">
				<div class="navbar navbar-default" role="navigation">
					<!-- BEGIN: NAV-CONTAINER -->
					<div class="nav-container">
						<div class="navbar-header">
							<!-- BEGIN: TOGGLE BUTTON (RESPONSIVE)-->
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<!-- BEGIN: LOGO -->
							<a class="navbar-brand logo" href="<?php echo base_url(); ?>">Rehla	Ticket</a>
						</div>
						
						<!-- BEGIN: NAVIGATION -->       
						<div class="navbar-collapse collapse">
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown"><a class="" href="<?php echo base_url();?>" data-toggle=""><i class="fa fa-home"></i><?php echo $this->lang->line('home');?>  </a></li>
								<li class="dropdown mega">
									<a class="dropdown-toggle" href="javascript:void(0);" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->lang->line('about_us');?>  <i class="fa fa-caret-down"></i></a>
									<ul class="dropdown-menu mega-menu">
										<!--<li class="col-md-3 col-sm-3 desc">
											<h4>CARS</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
										</li>-->
                                        <?php 
										 $menu_arr=$this->master_model->getRecords('tbl_menu_master',array('menu_status'=>'1')); 
										 if(count($menu_arr))
										 {
											foreach($menu_arr as $row)
										    {
										   ?>
                                            <li class="col-md-3 col-sm-4 links">
                                                <h5><?php echo $row['menu_name_'.$this->session->userdata('lang')]; ?></h5>
                                                <ul>
                                                   <?php 
                                                   $fetchsubmenu=$this->master_model->getRecords('tbl_submenu_master',array('menu_id'=>$row['menu_id']));
                                                   if(count($fetchsubmenu))
                                                   { 
                                                     foreach($fetchsubmenu as $sub)
                                                     {
                                                   ?>
                                                      <li><a href="<?php echo base_url().'home/page/'.$sub['page_id'] ?>"><?php echo $sub['submenu_name_'.$this->session->userdata('lang')] ?></a></li>
                                                    <?php
                                                     }
                                                   }
                                                   ?>
                                                </ul>
										    </li>
										<?php 
											 }
										} ?>
                                        <!--<li class="col-md-3 col-sm-4 links">
											<h5>TOP REGION</h5>
											<ul>
												<li><a href="#">ASIA</a></li>
												<li><a href="#">AFRICA</a></li>
												<li><a href="#">EAST ASIA</a></li>
												<li><a href="#">WESTERN EUROPE</a></li>
												<li><a href="#">SOUTH AMERICA</a></li>
											</ul>
										</li>
										<li class="col-md-3 col-sm-4 links">
											<div class="img-div clear-top">
												<img src="front/images/tour3.jpg" alt="cruise">
												<div class="overlay">
													<h5>DUBAI</h5>
													<p>Starting From $599</p>
													<a href="#">KNOW MORE</a>
												</div>
											</div>
										</li>-->
									</ul>
									<div class="clearfix"></div>
								</li>
								<!--<li class="dropdown mega">
									<a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-tag"></i> SPECIAL OFFERS <i class="fa fa-caret-down"></i></a>
									<ul class="dropdown-menu mega-menu">
										<li class="col-md-3 col-sm-3 desc">
											<h4>CRUISES</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
										</li>
										<li class="col-md-3 col-sm-4 links">
											<h5>PAGES</h5>
											<ul>
												<li><a href="cruise.html">CRUISE SEARCH</a></li>
												<li><a href="cruise-list.html">CRUISE LIST</a></li>
												<li><a href="cruise-detail-2.html">CRUISE DETAIL</a></li>
												<li><a href="cruise-detail.html">CRUISE DETAIL 2</a></li>
											</ul>
										</li>
										<li class="col-md-3 col-sm-4 links">
											<h5>TOP REGION</h5>
											<ul>
												<li><a href="#">ASIA</a></li>
												<li><a href="#">AFRICA</a></li>
												<li><a href="#">EAST ASIA</a></li>
												<li><a href="#">WESTERN EUROPE</a></li>
												<li><a href="#">SOUTH AMERICA</a></li>
											</ul>
										</li>
										<li class="col-md-3 col-sm-4 links">
											<div class="img-div clear-top">
												<img src="front/images/tour3.jpg" alt="cruise">
												<div class="overlay">
													<h5>DUBAI</h5>
													<p>Starting From $599</p>
													<a href="#">KNOW MORE</a>
												</div>
											</div>
										</li>
									</ul>
									<div class="clearfix"></div>
								</li> --->
                              
                                <li class="dropdown mega">
									<a class="dropdown-toggle" href="#" data-toggle="dropdown"><i class="fa fa-tag"></i> SPECIAL OFFERS <i class="fa fa-caret-down"></i></a>
									<ul class="dropdown-menu mega-menu">
                                       <li class="col-md-3 col-sm-3 links">
                                        <h5>Fishing</h5>
                                        <div class="img-div">
                                            <img alt="cruise" src="http://tng-me.com/rehlaticket/uploads/boats/1563.jpg">
                                            <div class="overlay">
                                                <!--<h5>20% OFF</h5>-->
                                                <a href="http://tng-me.com/rehlaticket/boat/details/MQ==">KNOW MORE</a>
                                            </div>
                                         </div>
                                      </li>
									</ul>
									<div class="clearfix"></div>
								</li>
                             
								<li><a class="" href="<?php echo base_url().'home/contactus/'; ?>" data-toggle=""><i class="fa fa-phone"></i> <?php echo $this->lang->line('contact_us');?></a></li>
							</ul>
						</div>
						<!-- END: NAVIGATION -->
					</div>
					<!--END: NAV-CONTAINER -->
				</div>
			</div>
			<!-- END: HEADER -->
		</div>
	</div>
