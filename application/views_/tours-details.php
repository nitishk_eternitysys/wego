<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>
<!-- START: PAGE TITLE -->
<div class="row page-title page-title3">
	<div class="container clear-padding text-center">
		<h3><?php echo $fetch_array_details[0]['form_title'.$this->session->userdata('form_lang').'']; ?></h3>
		<h5>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star"></i>
			<i class="fa fa-star-o"></i>
		</h5>
		<p><i class="fa fa-map-marker"></i> <?php echo $fetch_array_details[0]['form_address']; ?></p>
	</div>
</div>

<?php $image_array=$this->master_model->getRecords('tbl_threeforms_images',array('form_id'=>$fetch_array_details[0]['formsID'])); ?>
<div class="row hotel-detail">
	<div class="container">
		<div class="main-content col-md-8">
			<div id="room-gallery" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<?php
						if(count($image_array)){
							$i=0;
						foreach($image_array as $row)
						{
						?>
						<li data-target="#room-gallery" data-slide-to="<?php echo $i; ?>" <?php if ($i==0): ?> class="active" <?php endif; ?>></li>
						<?php
						$i++;}
						} ?>
				</ol>
				<div class="carousel-inner" role="listbox">
					<?php if(count($image_array)){
						$i=0;
					foreach($image_array as $row)
					{ ?>
					<div class="item <?php if ($i==0): ?>active<?php endif; ?>">
						<img src="<?php echo base_url().'uploads/form/'.$row['form_images_name']; ?>" alt="<?php echo $row['form_images_name']; ?>" />
					</div>
				<?php	$i++;}
					} ?>
				</div>
				<a class="left carousel-control" href="#room-gallery" role="button" data-slide="prev">
					<span class="fa fa-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#room-gallery" role="button" data-slide="next">
					<span class="fa fa-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<div class="room-complete-detail">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#overview"><i class="fa fa-calendar"></i> <span>Availalbility</span></a></li>
					<li><a data-toggle="tab" href="#available"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
					<li><a data-toggle="tab" href="#review"><i class="fa fa-comments"></i> <span>Reviews</span></a></li>
					<li><a data-toggle="tab" href="#write-review"><i class="fa fa-edit"></i> <span>Write Review</span></a></li>
                    <li><a data-toggle="tab" href="#passanger-list"><i class="fa fa-edit"></i> <span>Passanger List</span></a></li>
				</ul>
                
				<div class="tab-content">
					<div id="overview" class="tab-pane active in fade">
                       <div id="calendar" class="has-toolbar"></div>
					</div>
					<div id="available" class="tab-pane fade">
						<!--<h4 class="tab-heading">Available Dates</h4>-->
                        <h4 class="tab-heading">About <?php echo $fetch_array_details[0]['form_title'.$this->session->userdata('form_lang').'']; ?></h4>
						<p><?php echo $fetch_array_details[0]['form_desc'.$this->session->userdata('form_lang').'']; ?></p>
                        <br>
						 <?php if($fetch_array_details[0]['form_refundable']=='yes'){ ?>
                            <h4 class="tab-heading">Cancellation Policy</h4>
                            <p>Cancellation Price</p>
                            <p><?php echo $fetch_array_details[0]['form_cancel_before'].'%'; ?></p>
                            <br/>
                            <p>Before hours </p>
                            <p><?php echo $fetch_array_details[0]['form_not_cancel_hrs']; ?></p>
                         <?php } ?>	
                        <?php /*
						<?php if($fetch_array_details[0]['form_availalbe_for']=='daily'){?>
								<div class="car-overview col-md-6 col-sm-6 col-xs-6"><strong>From</strong> - <?php echo date('d M Y h:i A',strtotime($fetch_array_details[0]['form_available_from_date']));?></div>

									<div class="car-overview col-md-6 col-sm-6 col-xs-6"><strong>To</strong> - <?php echo date('d M Y h:i A',strtotime($fetch_array_details[0]['form_availalbe_to_date'])); ?></div>
							<?php	} else { ?>

								<?php $available_days=$this->master_model->getRecords('tbl_available_days',array('formID'=>$fetch_array_details[0]['formsID'])); ?>
								<?php if (count($available_days)>0):
										foreach ($available_days as $avail) {?>

								<div class="daily-schedule">
								<div class="title">
									<p><span><?php echo $avail['day']; ?></span></p>
								</div>
								<div class="daily-schedule-body">
									<div class="col-md-12 col-sm-12">
										<div class="car-overview col-md-6 col-sm-6 col-xs-6"><strong>From</strong> - <?php echo date('h:i A',strtotime($avail['form_from_time']));?></div>

											<div class="car-overview col-md-6 col-sm-6 col-xs-6"><strong>To</strong> - <?php echo date('h:i A',strtotime($avail['form_to_time'])); ?></div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
								<?php } endif;  ?>

						<?php }  */?>
                         
					</div>
					<div id="review" class="tab-pane fade">
							<div class="review-header">
								<div class="guest-review">
                                  <?php
								  $this->db->join('tbl_user_master','tbl_user_master.user_id=tbl_rating_master.fk_user_id');
								  $ratingdetails=$this->master_model->getRecords('tbl_rating_master',array('tbl_rating_master.fk_form_id'=>base64_decode($this->uri->segment(3)),'tbl_rating_master.form_type'=>$this->uri->segment(4)));                  
								  if(count($ratingdetails))
								  {
									  foreach($ratingdetails as $rating)
									  {
										  $dark='';
										  if($rating['rating']==5)
										  {
											  $dark='dark-review';
										  }
										  $approx_avg1=$rating['rating'];
										  switch($approx_avg1)
										  {
												case (round($approx_avg1)==0) :
													$star='<i class="fa fa-star-o" id="1"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=1) :
													$star='<i class="fa fa-star" id="1"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=2) :
													$star= '<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=3) :
													$star='<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=4) :
													$star='<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=5) :
													$star= '<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
													break;	
											}
								       ?>
									   <div class="individual-review <?php echo $dark; ?>">
										<h4><?php echo $rating['review_title']; ?> <?php echo $star; ?> </h4>
										<p><?php echo $rating['review_desc']; ?></p>
										<div class="col-md-md-1 col-sm-1 col-xs-2">
										   <img src="assets/images/user1.jpg" alt="<?php echo $rating['user_name']; ?>">
                                        </div>
										<div class="col-md-md-3 col-sm-3 col-xs-3">
											<span><?php echo $rating['user_name']; ?>, <?php echo $rating['user_city'];?></span>
										</div>
									</div>
									<div class="clearfix"></div>
								  <?php
									  }
								  }
								  else
								  { ?>
                                   <div class="alert alert-danger">No any review add of this <?php echo $this->uri->segment(4); ?> !</div> 
                                  <?php	 
								  }
								  ?>
                                </div>
							</div>
						</div>
					<div id="write-review" class="tab-pane fade">
							<div class="alert alert-danger" id="alerterror" style="display:none;"></div>
				            <div class="alert alert-success" id="alertsuccess" style="display:none;"></div>
					        <h4 class="tab-heading">Write A Review</h4>
                            <?php 
							if($this->session->userdata('userID')!='' && $this->session->userdata('userType')=='user'){ ?>
							<form id="reviewform" name="reviewform">
                                <input type="hidden" name="fk_form_id" id="fk_form_id" value="<?php echo $this->uri->segment(3); ?>">
                                <input type="hidden" name="form_type" id="form_type" value="<?php echo $this->uri->segment(4); ?>">
                                <div class="rating">
                                 <input id="input-2" value="1" class="rating rating-loading" name="rating" data-min="0" data-max="5" data-step="1">
                                 </div>
								<label>Review Title</label>
								<input type="text" class="form-control" name="review_title" id="review_title"  data-rule-required="true">
								<label for="comment">Comment</label>
								<textarea class="form-control" name="review_desc" rows="5" id="review_desc"  data-rule-required="true"></textarea>
                                <div class="text-center">
								  <button type="submit" id='btn_review' name="btn_review" class="btn btn-default submit-review">Submit</button>
								</div>
							</form>
                            <?php }else{ 
							  if($this->session->userdata('userType')=='')
							  {
								  ?>
                                <form method="post" id="loginfrm_user_rating">
                                   <label>Email</label>
                                    <div class="input-group">
                                        <input name="email" type="text" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                    </div>
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input name="password" type="password" class="form-control" placeholder="Password" data-rule-required="true">
                                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                                    </div>					
                                    <button type="submit" name="btn_login" id="btn_login" value="1" class="btn btn-default submit-review">LOGIN <i class="fa fa-sign-in"></i></button>
                                </form>
							 <?php
							   }
							   else
							   {
								   echo 'Please login with the normal customer and give the rating of this '.$this->uri->segment(4).'. ';
							   }
							 } ?>
						</div>
                    <div id="passanger-list" class="tab-pane fade">
                          <div class="col-md-12 col-sm-12">
                                <div class="most-recent-booking">
                                    <h4>Passangers list</h4>
                                    
                                    <div class="field-entry">
                                        <div class="col-md-1 col-sm-1 col-xs-1 clear-padding">
                                            <p><strong>No.</strong></p>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                            <p><strong>Passenger Name</strong></p>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                            <p><strong>Gender</strong></p>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 clear-padding">
                                            <p><strong>Age</strong></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                     <?php 
									  $this->db->join('tbl_passenger_list','tbl_passenger_list.tmp_id=tbl_transaction_master.rowId');
									  $passangerList=$this->master_model->getRecords('tbl_transaction_master',array('tbl_transaction_master.booking_form_id'=>$fetch_array_details[0]['formsID'],'tbl_transaction_master.type !='=>'accom'));
									  if(count($passangerList))
									  {
										  $i=1;	
										  foreach($passangerList as $row)
										  {
									  ?>
                                              <div class="field-entry">
                                                <div class="col-md-1 col-sm-1 col-xs-1 clear-padding">
                                                    <p><strong><?php echo $i; ?>.</strong></p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                                    <p><?php if($row['passanger_type']=='public'){echo ucfirst($row['passenger_name']);}else{echo substr($row['passenger_name'], 0, 1).'------';} ?></p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                                    <p><?php echo $row['passenger_gender']; ?></p>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3 clear-padding">
                                                    <p><?php echo $row['passanger_age']; ?></p>
                                                </div>
                                            </div>
                                    <?php 
									     $i++;
									    }
									  }
									  ?>
                              </div>
                          </div>
                     </div>
				</div>
			</div>
		</div>
		<div class="col-md-4 package-detail-sidebar">
			<div class="col-md-12 sidebar-wrapper clear-padding">
				<div class="package-summary sidebar-item">
					<h4><i class="fa fa-bookmark"></i> Package Summary</h4>
					<div class="package-summary-body">
						<h5><i class="fa fa-heart"></i>Title</h5>
						<p><?php echo $fetch_array_details[0]['form_title'.$this->session->userdata('form_lang').'']; ?></p>
						<h5><i class="fa fa-map-marker"></i>Departure</h5>
						<p><?php echo $fetch_array_details[0]['form_departure']; ?></p>
						<h5><i class="fa fa-calendar"></i>Departure Time</h5>
						<p><?php echo $fetch_array_details[0]['form_departure_time']; ?></p>
					</div>
					<div class="package-summary-footer text-center">
						<div class="col-md-6 col-sm-6 col-xs-6 price">
							<h5>Starting From</h5>
							<h5><strong>$<?php echo $fetch_array_details[0]['form_price']; ?>/Person</strong></h5>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 book">
                          <?php if($fetch_array_details[0]['formsID']==$this->session->userdata('formID')){ ?>
                          <a href="<?php echo base_url().'booking/tours/'.base64_encode($fetch_array_details[0]['formsID']);?>" >BOOK NOW</a>
                          <?php }else{ ?>
                          <a href="javascript:void(0)"  onClick="return alert('Please select date and time !');">BOOK NOW</a>
                          <?php } ?>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 sidebar-wrapper clear-padding">
				<div class="package-summary sidebar-item">
					<h5><i class="fa fa-map-marker"></i> <?php echo $fetch_array_details[0]['form_address']; ?></h5>
						<div id="map" style="height:200px; width:100%;"></div>
				</div>
				<?php
				$similarAccom=$this->master_model->getRecords('tbl_three_forms',array('subcategoryID'=>$fetch_array_details[0]['subcategoryID'],'form_status'=>'active','form_type'=>'Tours','formsID !='=>$fetch_array_details[0]['formsID']));
				 if(count($similarAccom)>0){
				?>
				<div class="similar-hotel sidebar-item">
					<h4><i class="fa fa-bed"></i> Similar <?php echo $fetch_array_details[0]['form_type']; ?></h4>
					<div class="sidebar-item-body">

						<?php
									foreach ($similarAccom as $same) {
							?>
						<div class="similar-hotel-box">
							<a href="<?php echo base_url().'details/toursDetails/'.base64_encode($same['formsID']).'/'.$fetch_array_details[0]['form_type'];?>">
								<div class="col-md-5 col-sm-5 col-xs-5 clear-padding">
									<?php if($same['form_image']!=''){ ?>
						<img src="<?php echo $this->master_model->resize($same['form_image'],150,120,'uploads/forms/');?>" alt="<?php echo $same['form_title']; ?>">
										<?php } else { ?>
										<img src="<?php echo $this->master_model->resize('default.gif',150,120,'front/images/');?>" alt="<?php echo $same['form_title']; ?>">
										<?php } ?>
								</div>
								<div class="col-md-7 col-sm-7 col-xs-7">
									<h5><?php echo $same['form_title'.$this->session->userdata('form_lang').'']; ?></h5>
									<h5><i class="fa fa-map-marker"></i> <?php echo $same['form_address']; ?></h5>
									<span>$<?php echo $same['form_price']; ?>/Person</span>
								</div>
							</a>
						</div>
					<?php }  ?>

					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/jquery-ui/jquery-ui.min.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/fullcalendar.css" />

<script src="<?php echo base_url(); ?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script language="javascript">
//-----------------------------Calanedar--------------------------------//
$(document).ready(function(){
if (jQuery().fullCalendar){

              	var date = new Date();

				var d = date.getDate();

				var m = date.getMonth();

				var y = date.getFullYear();

				var h = {};
                if ($(window).width() <= 480) {

					h = {

						left: 'title, prev,next',

						center: '',

					   // right: 'month,agendaWeek,agendaDay'

					};

				} else {

					h = {

						left: 'title',

						center: '',

						//right: 'prev,next,today,month,agendaWeek,agendaDay'

					};

				}

		

				var initDrag = function (el) {

					// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)

					// it doesn't need to have a start or end

					var eventObject = {

						title: $.trim(el.text()) // use the element's text as the event title

					};

					// store the Event Object in the DOM element so we can get to it later

					el.data('eventObject', eventObject);

					// make the event draggable using jQuery UI

					el.draggable({

						zIndex: 999,

						revert: true, // will cause the event to go back to its

						revertDuration: 0 //  original position after the drag

					});

				}

				var addEvent = function (title, priority) { 

					title = title.length == 0 ? "Untitled Event" : title;

					priority = priority.length == 0 ? "default" : priority;

		

					var html = $('<div data-class="label label-' + priority + '" class="external-event label label-' + priority + '">' + title + '</div>');

					jQuery('#event_box').append(html);

					initDrag(html);

				}

		

				$('#external-events div.external-event').each(function () {

					initDrag($(this))

				});

		

				$('#event_add').click(function () {  

					var title = $('#event_title').val(); 

					var priority = $('#event_priority').val();

				});

			   /*course start date*/

			   $('#calendar').fullCalendar({ 

				    header: h,

					editable: false,

					droppable: false, // this allows things to be dropped onto the calendar !!!

					drop: function (date, allDay) { // this function is called when something is dropped

						//retrieve the dropped element's stored Event Object

						var originalEventObject = $(this).data('eventObject');

						//we need to copy it, so that multiple events don't have a reference to the same object

						var copiedEventObject = $.extend({}, originalEventObject);

						//assign it the date that was reported

						copiedEventObject.start = date;

						copiedEventObject.allDay = allDay;

						copiedEventObject.className = $(this).attr("data-class");

						//render the event on the calendar

						// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)

						$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

						// is the "remove after drop" checkbox checked?

						if ($('#drop-remove').is(':checked')) {

							// if so, remove the element from the "Draggable Events" list

							$(this).remove();

						}

					},
                events: [
				     <?php 
					  $getDays=array();
					  $this->db->group_by('tbl_available_days.form_from_time');
					  $getresponse=$this->master_model->getRecords('tbl_available_days',array('tbl_available_days.formID'=>$fetch_array_details[0]['formsID'])); 
					  if(count($getresponse)>0)
					  { 
					     $lastMonth=array();
						  foreach($getresponse as $row)
						  {
							  $getDays[]=$row['day'];
						  }
						  $date     = date('Y-m-d',strtotime($fetch_array_details[0]['form_available_from_date']));
						  $end_date = date('Y-m-d',strtotime($fetch_array_details[0]['form_availalbe_to_date']));
						  while (strtotime($date) <= strtotime($end_date)) 
						  {
							
							 $date1 = date ("l",strtotime($date));
							 if(in_array($date1,$getDays))
							 {
								$y=date ("Y",strtotime($date));
								$m=date ("m",strtotime($date));
								$d=date ("d",strtotime($date));
							   ?>
							    {
								 title: '<?php echo $fetch_array_details[0]['form_title'.$this->session->userdata('form_lang').'']; ?>',
								 start: new Date(<?php echo $y; ?>, <?php echo $m-1; ?>,<?php echo $d; ?>),
								 url: '<?php echo base_url().'details/tourstime/'.$date1.'/'.base64_encode($fetch_array_details[0]['formsID']).'/'.$fetch_array_details[0]['form_type']; ?>/<?php echo  $date; ?>',
								 className: 'label label-default fancybox fancybox.ajax',
							    },
							  <?php	
							 }
							 $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
						  } 
					   }?>
					   ]
               });
           //Replace buttons style
           $('.fc-button').addClass('btn');
        }	
 });
 </script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.fancybox.js?v=2.1.5"></script> 
<!-- END: ROOM GALLERY -->
<script type="text/javascript">
$('.fancybox').fancybox();
$.fancybox.close();
var map;
var markersArray = [];
		function initialize() {
				var myLatlng = new google.maps.LatLng(<?php echo $fetch_array_details[0]['form_location']; ?>);
				var myOptions = {
						zoom:7,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map"), myOptions);

				var marker = new google.maps.Marker({
							position: new google.maps.LatLng(<?php echo $fetch_array_details[0]['form_location']; ?>),
							map: map
					 });
					 markersArray.push(marker);
}

window.onload = function () { initialize() };
</script>
