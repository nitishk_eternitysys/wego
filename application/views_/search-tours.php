<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Where to go</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="destination" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['destination']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Adult</label>
        <div class="input-group margin-bottom-sm">
           <span class="ui-spinner">
            <input class="form-control quantity-padding" value="<?php echo $_GET['adult']; ?>" name="adult" id="adult_count" aria-valuemin="1" aria-valuenow="1" autocomplete="off" role="spinbutton"  ></span>
        </div>
    </div>
</div>
<div class="col-md-2 col-sm-6 col-xs-6">
    <div class="form-gp">
        <label>Child</label>
        <div class="input-group margin-bottom-sm">
           <span class="ui-spinner">
            <input class="form-control quantity-padding" value="<?php echo $_GET['child']; ?>" name="child" id="child_count" aria-valuemin="1" aria-valuenow="1" autocomplete="off" role="spinbutton" ></span>
        </div>
    </div>
</div>