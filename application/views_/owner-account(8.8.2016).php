<!-- START: USER PROFILE -->
<style>
/*form styles*/
.msform {
margin:20px auto 0;
text-align: center;
position: relative;
}
.msform fieldset {
border: 0 none;
/*background: white;    
border-radius: 3px;
box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);*/
padding:20px 30px 0;
box-sizing: border-box;
width: 100%;
margin: 0 auto;
/*stacking fieldsets above each other
position: absolute;*/
}
/*Hide all except first fieldset*/
.msform fieldset:not(:first-of-type) {
display: none;
}
/*inputs*/

.msform input, .msform textarea {
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 0px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display:inline-block;
    font-size: 14px;
    height:40px;
    line-height: 1.42857;
    padding: 6px 12px;
    margin-bottom:15px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
}
    
    .user-personal-info .form-control, .user-change-password .form-control, .user-preference .form-control {

    border-radius: 0px;
    height:36px;
    margin-bottom: 15px;
    padding: 8px;
}
/*buttons*/
.msform .action-button {
width: 100px;
background: #00adef;
font-weight: bold;
color: white;
border: 0 none;
border-radius: 1px;
cursor: pointer;
padding: 10px 5px;
margin:30px 10px 20px;
}
.msform .action-button:hover, .msform .action-button:focus {
/*box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;*/
}
/*headings*/
.fs-title {
font-size: 20px;
text-transform: uppercase;
color: #2C3E50;
margin:0 0 25px;
}
.fs-subtitle {
font-weight: normal;
font-size: 13px;
color: #666;
margin-bottom: 20px;
}
/*progressbar*/
.progressbar {
margin:0px;
padding:0px;
overflow: hidden;
/*CSS counters to number the steps*/
counter-reset: step;
} 
.progressbar li {
list-style-type: none;
text-transform: none;
font-size: 13px;
width: 12.33%;
float: left;
position: relative;
}
.progressbar li.active {
    color: #00adef;
}
.progressbar li:before {
content: counter(step);
counter-increment: step;
width: 50px;
height:50px;    
line-height: 48px;
display: block;
font-size: 21px;
color: #333;
background: #e6e6e6;
border-radius: 30px;
margin: 0 auto 5px auto;
}
/*progressbar connectors*/
.progressbar li:after {
content: '';
width: 100%;
height: 2px;
background: white;
position: absolute;
left: -50%;
top: 9px;
z-index: -1; /*put it behind the numbers*/
}
.progressbar li:first-child:after {
/*connector not needed before the first step*/
content: none;
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
.progressbar li.active:before, .progressbar li.active:after {
background: #00adef;
color: white;
}

label.btn span {vertical-align: top;
  font-size: 1.5em ;
}

label input[type="radio"] ~ i.fa.fa-circle-o{
    color: #c8c8c8;    display: inline;
}
label input[type="radio"] ~ i.fa.fa-check-circle-o{
    display: none;
}
label input[type="radio"]:checked ~ i.fa.fa-circle-o{
    display: none;
}
label input[type="radio"]:checked ~ i.fa.fa-check-circle-o{
    color: #7AA3CC;    display: inline;
}
label:hover input[type="radio"] ~ i.fa {
color: #7AA3CC;
}

label input[type="checkbox"] ~ i.fa.fa-square-o{
    color: #c8c8c8;    display: inline;
}
label input[type="checkbox"] ~ i.fa.fa-check-square-o{
    display: none;
}
label input[type="checkbox"]:checked ~ i.fa.fa-square-o{
    display: none;
}
label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{
    color: #7AA3CC;    display: inline;
}
label:hover input[type="checkbox"] ~ i.fa {
color: #7AA3CC;
}

div[data-toggle="buttons"] label.active{
    color: #7AA3CC;
}

div[data-toggle="buttons"] label {
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: normal;
line-height: 2em;
text-align: left;
white-space: nowrap;
vertical-align: top;
cursor: pointer;
background-color: none;
border: 0px solid 
#c8c8c8;
border-radius: 3px;
color: #c8c8c8;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
-o-user-select: none;
user-select: none;
}

div[data-toggle="buttons"] label:hover {
color: #7AA3CC;
}

div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
-webkit-box-shadow: none;
box-shadow: none;
}
    .switch-but{background:#ccc;border-radius:20px;padding:5px;}    
</style>
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3>Welcome, <?php echo $fetch_array[0]['hosts_name']; ?></h3>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#profile-overview" class="text-center"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
						<li><a data-toggle="tab" href="#profile" class="text-center"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                        <?php if(count($categories)>0){ 
								foreach($categories as $cat){
								$owner_cat=explode(',',$fetch_array[0]['hosts_mainategory_id']);
								if(in_array($cat['category_id'],$owner_cat))
								{
						?>
						<li><a data-toggle="tab" href="#<?php echo $cat['category_slug'] ?>" class="text-center"><i class="fa fa-edit"></i> <span><?php echo $cat['category_name_'.$this->session->userdata('lang')] ?></span></a></li>
                        <?php } } } ?>
					</ul>
				</div>
			</div>
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
					<div id="profile-overview" class="tab-pane fade in active">
						<div class="col-md-6">
							<div class="brief-info">
								<!--<div class="col-md-2 col-sm-2 clear-padding">
									<img src="assets/images/user1.jpg" alt="cruise">
								</div>-->
								<div class="col-md-10 col-sm-10">
									<h3><?php echo ucfirst($fetch_array[0]['hosts_name']); ?></h3>
									<h5><i class="fa fa-envelope-o"></i><?php echo $fetch_array[0]['hosts_email']; ?></h5>
									<h5><i class="fa fa-phone"></i>+<?php echo $fetch_array[0]['hosts_contact']; ?></h5>
									<h5><i class="fa fa-map-marker"></i><?php echo $fetch_array[0]['hosts_address']; ?></h5>
								</div>
								<div class="clearfix"></div>
								<div class="brief-info-footer">
									<a data-toggle="tab" href="#profile" class="text-center" id="profileDiv"><i class="fa fa-edit"></i>Edit Profile</a>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="most-recent-booking">
								<h4>Recent Booking</h4>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-profile-offer">
								<h4>Offers For You</h4>
								<div class="offer-body">
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>20% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 20% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>30% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 30% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>10% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 10% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="user-notification">
									<h4>Notification</h4>
									<div class="notification-body">
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bolt"></i> 50% off on all items <span class="pull-right">08h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-sun-o"></i> New Year special offer <span class="pull-right">1d ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div id="booking" class="tab-pane fade in">
						<div class="col-md-3 col-sm-3 col-xs-6">
							<form>
								<select class="form-control">
									<option>All Bookings</option>
									<option>Hotel</option>
									<option>Flight</option>
									<option>Holiday</option>
									<option>Cruise</option>
								</select>
							</form>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-12">
							<div class="item-entry">
								<span>Order ID: CR1234</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer1.jpg" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
											<p>Check In: 22 Aug </p>
											<p>Check Out: 25 Aug</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Cancel</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Date:</strong> 20 Aug 2015<strong>Order Total:</strong> $566</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span>Order ID: CR1568</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/airline/vistara-2x.png" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>New Delhi <i class="fa fa-long-arrow-right"></i> New York</h4>
											<p>Take Off: 22 Aug </p>
											<p>Landing: 22 Aug</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Cancel</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Date:</strong> 20 Aug 2015<strong>Order Total:</strong> $566</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span>Order ID: CR9880</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer1.jpg" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>Wonderful Europe</h4>
											<p>Start: 22 Aug </p>
											<p>End: 25 Aug</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p class="failed"><i class="fa fa-times"></i>Payment Failed</p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Date:</strong> 20 Aug 2015<strong>Order Total:</strong> $566</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span>Order ID: CR1234</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer2.jpg" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
											<p>Check In: 22 Aug </p>
											<p>Check Out: 25 Aug</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Cancel</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Date:</strong> 20 Aug 2015<strong>Order Total:</strong> $566</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry completed">
								<span>Order ID: CR1568</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/airline/vistara-2x.png" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>New Delhi <i class="fa fa-long-arrow-right"></i> New York</h4>
											<p>Take Off: 22 Aug </p>
											<p>Landing: 22 Aug</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p class="confirmed"><i class="fa fa-check"></i>Completed</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Submit Review</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Date:</strong> 20 Aug 2015<strong>Order Total:</strong> $566</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry completed">
								<span>Order ID: CR9880</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer3.jpg" alt="cruise">
										</div>
										<div class="col-md-4 col-sm-4">
											<h4>Wonderful Europe</h4>
											<p>Start: 22 Aug </p>
											<p>End: 25 Aug</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p class="confirmed"><i class="fa fa-check"></i>Completed</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Submit Review</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Date:</strong> 20 Aug 2015<strong>Order Total:</strong> $566</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="text-center load-more">
								<a href="#">LOAD MORE</a>
							</div>
						</div>
					</div>
					<div id="profile" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Personal Information</h4>
								<div class="user-info-body">
									 <form method="post" id="userProfile" enctype="multipart/form-data">
										<div class="col-md-12">
											<label>Name</label>
											<input name="hosts_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true"  value="<?php echo $fetch_array[0]['hosts_name']; ?>">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12">
											<label>Email-ID</label>
											 <input name="hosts_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_email']; ?>" data-rule-email="true" readonly>
										</div>
										<div class="col-md-12">
									   <label>Company Name</label>
									   <input name="hosts_company" type="text" class="form-control" placeholder="Company Name" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_company']; ?>"  >	</div>
									   <div class="col-md-12">
										<label>Types</label>
									      <select multiple="multiple" class="form-control" data-rule-required="true" style="height:auto !important;" name="hosts_mainategory_id[]">
											<?php if(count($categories)>0){ 
                                                    foreach($categories as $cat){
														$expload_cat=explode(',',$fetch_array[0]['hosts_mainategory_id']);
														?>
                                            <option value="<?php echo $cat['category_id'] ?>" <?php if(in_array($cat['category_id'],$expload_cat)){ ?> selected="selected" <?php } ?>><?php echo $cat['category_name_'.$this->session->userdata('lang')] ?></option>
                                            <?php } } ?>
                                        </select>
										</div>
										<div class="col-md-12">
                                         <label>Description</label>
                                         <textarea name="hosts_description" id="hosts_description" class="form-control" data-rule-required="true"><?php echo $fetch_array[0]['hosts_description']; ?></textarea>
										</div>	
										<div class="col-md-12">
											<label>Address</label>
											<textarea name="hosts_address" id="hosts_address" class="form-control" data-rule-required="true"><?php echo $fetch_array[0]['hosts_address']; ?></textarea>
										</div>
                                        <div class="col-md-12">
											<label>Website</label>
										    <input name="hosts_website" type="text" class="form-control" placeholder="Website" value="<?php echo $fetch_array[0]['hosts_website']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Contact Number</label>
										  <input name="hosts_contact" type="text" class="form-control" placeholder="Contact Number" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_contact']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Position</label>
										  <input name="hosts_position" type="text" class="form-control" placeholder="Position" value="<?php echo $fetch_array[0]['hosts_position']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Mobile Number</label>
										  <input name="hosts_mobile" type="text" class="form-control" placeholder="Mobile Number" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_mobile']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Preferred Payment</label>
										  <select class="form-control" data-rule-required="true" name="hosts_preferred_payment">
                                            <option value="">Select Payment</option>
                                            <option value="Paypal" <?php if($fetch_array[0]['hosts_preferred_payment']=='Paypal'){ echo 'selected="selected"'; } ?> >Paypal</option>
                                            <option value="Bank" <?php if($fetch_array[0]['hosts_preferred_payment']=='Bank'){ echo 'selected="selected"'; } ?>>Bank</option>
                                          </select>
										</div>
                                        <div class="col-md-12">
										  <label>Preferred Currency</label>
										  <select class="form-control" data-rule-required="true" name="hosts_preferred_currency">
                                            <option value="">Select Currency</option>
                                            <option value="USD" <?php if($fetch_array[0]['hosts_preferred_currency']=='USD'){ echo 'selected="selected"'; } ?> >USD</option>
                                            <option value="AED" <?php if($fetch_array[0]['hosts_preferred_currency']=='AED'){ echo 'selected="selected"'; } ?>>AED</option>
                                          </select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <button type="submit"  name="btn_profile" id="btn_profile">SAVE CHANGES</button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <a href="<?php echo base_url('owner/profile'); ?>">CANCEL</a>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Change Password</h4>
								<div class="change-password-body">
									<form method="post" id="passwordForm" enctype="multipart/form-data">
										<div class="col-md-12">
											<label>Old Password</label>
											<input type="password" placeholder="Old Password" class="form-control" name="old-password" id="old-password" data-rule-required="true">
										</div>
										<div class="col-md-12">
											<label>New Password</label>
											<input type="password" placeholder="New Password" class="form-control" name="new-password" id="new-password" data-rule-required="true" data-rule-minlength="6">
										</div>
										<div class="col-md-12">
											<label>Confirm Password</label>
											<input type="password" placeholder="Confirm Password" class="form-control" name="confirm-password" id="confirm-password" data-rule-required="true" data-rule-minlength="6" data-rule-equalto="#new-password">
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit" name="btn_change_pass" id="btn_change_pass" value="1">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#flight-preference" aria-expanded="false" aria-controls="flight-preference">
										<i class="fa fa-plane"></i> Flight Preference <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="flight-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label>Price Range</label>
											<select class="form-control" name="flight-price-range">
												<option>Upto $199</option>
												<option>Upto $250</option>
												<option>Upto $499</option>
												<option>Upto $599</option>
												<option>Upto $1000</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Food Preference</label>
											<select class="form-control" name="flight-food">
												<option>Indian</option>
												<option>Chineese</option>
												<option>Sea Food</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Airline</label>
											<select class="form-control" name="flight-airline">
												<option>Indigo</option>
												<option>Vistara</option>
												<option>Spicejet</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#hotel-preference" aria-expanded="false" aria-controls="hotel-preference">
										<i class="fa fa-bed"></i> Hotel Preference <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="hotel-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label>Price Range</label>
											<select class="form-control" name="hotel-price-range">
												<option>Upto $199</option>
												<option>Upto $250</option>
												<option>Upto $499</option>
												<option>Upto $599</option>
												<option>Upto $1000</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Food Preference</label>
											<select class="form-control" name="hotel-food">
												<option>Indian</option>
												<option>Chineese</option>
												<option>Sea Food</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Facilities</label>
											<select class="form-control" name="hotel-facilities">
												<option>WiFi</option>
												<option>Bar</option>
												<option>Restaurant</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Rating</label>
											<select class="form-control" name="hotel-facilities">
												<option>5</option>
												<option>4</option>
												<option>3</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
                    <!-- Accoumadation form create--->
                    <div id="accommodations" class="tab-pane fade in">
                        <div id="accommodations_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addaccom" data-id="accomodations_add">Add Accommodations</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_accommodations_master',array('hosts_id'=>$this->session->userdata('userID'))); 
								
							    if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['accom_id']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['accom_title']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['accom_category_id'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['accom_subcategory_id'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Rooms  :</strong> <?php echo $manage['accom_bed_rooms'] ?> </p>
                                            <p class="col-sm-5"><strong>Beds :</strong> <?php echo $manage['accom_beds'] ?> </p>
                                            <p class="col-sm-5"><strong>Bathrooms :</strong> <?php echo $manage['accom_bathrooms'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['accom_description']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['accom_id']; ?>" data-action="accomedit"data-id="accomodations_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="<?php echo $manage['accom_id']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['accom_user_name']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['accom_position']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['accom_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['accom_email']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="accomodations_add" class="commancls" style="display:none;">
                            <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
                               <p><a href="javascript:void(0);"  class="addcls" rel="accommodations_manage">Manage Accommodations</a></p>
                            </div> 
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="user-personal-info">
                                    <h4>Accommodations Information</h4>
                                    <div class="user-info-body">
                                      <form class="msform" id="accommodationsform" name="accommodationsform" method="post"> 
                                            <!-- progressbar -->
                                            <ul id="progressbar" class="progressbar">
                                                <li class="active ">Basic info</li>
                                                <li>Rooms &amp; beds</li>
                                                <li>Amenities</li>
                                                <li>Rules</li>
                                                <li>Other rules</li>
                                                <li>Pricing</li>
                                                <li>Cancellation and refund policy:</li>
                                                <li>Contact info:</li>
                                            </ul>
                                            <!-- fieldsets -->
                                            <fieldset>
                                            <h2 class="fs-title">Basic info</h2>
                                                <input type="text" name="accom_title" placeholder="Titile" id="accom_title" class="form-control" data-rule-required='true'/>
                                                <select name="accom_category_id" class="form-control" id="accom_category_id">
                                                   <option selected="selected">Select category</option>
                                                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'1','category_status'=>'active')); 
                                                   if(count($category_accom)>0)
                                                   {
                                                       foreach($category_accom as $row)
                                                       {
                                                      ?>
                                                       <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                                                      <?php 
                                                       }
                                                  } ?>
                                                </select>
                                                <select name="accom_subcategory_id" class="form-control" id="accom_subcategory_id">
                                                  <option selected="selected">Select subcategory</option>
                                                </select>
                                                <textarea name="accom_location" id="accom_location" class="form-control" placeholder="location"></textarea>
                                                <textarea name="accom_description" id="accom_description" class="form-control" placeholder="Description"></textarea>
                                                <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                            <h2 class="fs-title">Rooms &amp; beds</h2>
                                            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
                                            <input type="text" onkeypress="return OnlyNumericKeys(event);"  name="accom_bed_rooms" id="accom_bed_rooms" placeholder="Bed Rooms" class="col-md-6 col-sm-12" />
                                            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_beds" id="accom_beds" placeholder="Beds" class="col-md-6 col-sm-12" />
                                            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_bathrooms" id="accom_bathrooms" placeholder="Bathrooms" />
                                            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_swimming_pool" id="accom_swimming_pool" placeholder="swimming pool" />
                                            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_play_yards" id="accom_play_yards" placeholder="play yards" />
                                             <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_balcony" id="accom_balcony" placeholder="Balcony" />
                                              <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_garden" id="accom_garden" placeholder="garden" />
                                              <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_roof" id="accom_roof" placeholder="Roof" />
                                              
                                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                                            <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                            <h2 class="fs-title">Amenities</h2>
                                             <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">TV</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                   <div class="switch-but">
                                                    <label class="btn active amenities">
                                                      <input type="radio"  value="yes"  name='accom_tv' id="accom_tv"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio"  value="no"  name='accom_tv' id="accom_tv1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                      </div>
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_tv_yes" style="display:none;"><input type="text" name="accom_tv_number" id="accom_tv_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Play station</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn amenities active">
                                                      <input type="radio" value="yes" class="amenities"  name='accom_playstation' id="accom_playstation"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio" value="no" class="amenities"  name='accom_playstation' id="accom_playstation1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                    
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_playstation_yes" style="display:none;"><input type="text" name="accom_playstation_number" id="accom_playstation_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">WIFI</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn amenities active">
                                                      <input type="radio" class="amenities" value="yes" rel="number" name='accom_wifi' id="accom_wifi"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio" class="amenities" value="no" rel="number" name='accom_wifi' id="accom_wifi1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                    
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_wifi_yes" style="display:none;"><input type="text" name="accom_wifi_number" id="accom_wifi_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">DVD</div>
                                                  <div class="col-xs-4 " data-toggle="buttons">
                                                    <label class="btn amenities active">
                                                      <input type="radio" class="amenities" value="yes" rel="number" name='accom_dvd' id="accom_dvd"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio" class="amenities" value="no" rel="number" name='accom_dvd' id="accom_dvd1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                    
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_dvd_yes" style="display:none;"><input type="text" name="accom_dvd_number" id="accom_dvd_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
                                                </div>
                                              </div>
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">GYM</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn amenities active">
                                                      <input type="radio" class="amenities" value="yes" name='accom_gym' id="accom_gym"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio" class="amenities" value="no"  name='accom_gym' id="accom_gym1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                    
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_gym_yes" style="display:none;"><input type="text" name="accom_gym_number" id="accom_gym_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
                                                </div>
                                              </div>
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">kitchen</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn amenities active">
                                                      <input type="radio" class="amenities" value="yes"  name='accom_kitchen' id="accom_kitchen"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio" class="amenities" value="no"  name='accom_kitchen' id="accom_kitchen1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                    
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_kitchen_yes" style="display:none;"><input type="text" name="accom_kitchen_number" onkeypress="return OnlyNumericKeys(event);" id="accom_kitchen_number" class="form-control"></div>
                                                </div>
                                              </div>
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Washing Machine</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn amenities active">
                                                      <input type="radio" id="accom_washing_machine"  value="yes" rel="machine_numbe" name='accom_washing_machine'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn amenities">
                                                      <input type="radio" id="accom_washing_machine1" value="no" rel="machine_numbe" name='accom_washing_machine' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                    
                                                  </div>
                                                  <div class="col-xs-4 clsdevnumber" id="accom_washing_machine_yes" style="display:none;"><input type="text" name="accom_washing_machine_number" id="accom_washing_machine_number" onkeypress="return OnlyNumericKeys(event);" class="form-control"></div>
                                                </div>
                                              </div>
                                             <input type="button" name="previous" class="previous action-button" value="Previous" />
                                             <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                            <h2 class="fs-title">Rules</h2>
                                           <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                                            <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Check In</div>
                                                  <div class="col-xs-6">
                                                      <input type="text" name="accom_check_in" id="accom_check_in" placeholder="Select check in time" />
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Check Out</div>
                                                  <div class="col-xs-6">
                                                      <input type="text" name="accom_check_out" id="accom_check_out" placeholder="Select check in time" />
                                                  </div>
                                                </div>
                                             </div> 
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Max gusts</div>
                                                  <div class="col-xs-6">
                                                      <input type="text" name="accom_guest_max" id="accom_guest_max" placeholder="Max guests" onkeypress="return OnlyNumericKeys(event);" />
                                                  </div>
                                                </div>
                                             </div> 
                                             <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Pets</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn active">
                                                      <input type="radio" id="accom_pets"  value="yes" name='accom_pets'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn">
                                                      <input type="radio" id="accom_pets1" value="no"  name='accom_pets' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                   </div>
                                                </div>
                                              </div> 
                                              <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Smoking</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn  active">
                                                      <input type="radio" id="accom_smoking"  value="yes" name='accom_smoking'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn ">
                                                      <input type="radio" id="accom_smoking1" value="no"  name='accom_smoking' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                  </div>
                                                 </div>
                                              </div> 
                                               <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Cooking</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn  active">
                                                      <input type="radio" id="accom_cooking"  value="yes" name='accom_cooking'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn ">
                                                      <input type="radio" id="accom_cooking1" value="no"  name='accom_cooking' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                  </div>
                                                 </div>
                                              </div> 
                                              <input type="button" name="previous" class="previous action-button" value="Previous" />
                                              <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                                 <h2 class="fs-title">Other Rules</h2>
                                                 <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                                                 <textarea name="accom_gustes_allowed" id="accom_gustes_allowed" placeholder="Add Descripcription gustes allowed" ></textarea>
                                                 <input type="button" name="previous" class="previous action-button" value="Previous" />
                                                 <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                            <h2 class="fs-title">Pricing- discount </h2>
                                            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                                            <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Price</div>
                                                  <div class="col-xs-6">
                                                      <input type="text" name="accom_price" id="accom_price" placeholder="Price"  onkeypress="return OnlyNumericKeys(event);" />
                                                  </div>
                                                </div>
                                             </div> 
                                            <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Discount</div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn discount active">
                                                      <input type="radio" id="accom_discount" value="on" name='accom_discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
                                                    </label>
                                                    <label class="btn discount">
                                                      <input type="radio" id="accom_discount1" value="off"  name='accom_discount' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
                                                    </label>
                                                  </div>
                                                  <div class="col-xs-4" id="accom_discount_on" style="display:none;">
                                                     <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="accom_precentage_discount" id="accom_precentage_discount" value="" placeholder="discount price in percantage">
                                                  </div>
                                                 </div>
                                              </div> 
                                            <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Bird discount</div>
                                                  <div class="col-xs-6" data-toggle="buttons">
                                                    <input type="text" name="accom_bird_discount" id="accom_bird_discount" placeholder="Bird discount" /> 
                                                  </div>	
                                                 </div>
                                             </div>
                                            <input type="button" name="previous" class="previous action-button" value="Previous" />
                                            <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                              <h2 class="fs-title">Cancellation and refund policy:</h2>
                                              <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                                              <div class="row">
                                                <div class="col-xs-10">
                                                  <div class="col-xs-4">Cancellation and refund policy: </div>
                                                  <div class="col-xs-4" data-toggle="buttons">
                                                    <label class="btn cancellation active">
                                                      <input type="radio" id="accom_cancel_policy" value="yes" name='accom_cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                                                    </label>
                                                    <label class="btn cancellation">
                                                      <input type="radio" id="accom_cancel_policy1" value="no"  name='accom_cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                                                    </label>
                                                  </div>
                                                  <div class="col-xs-4" id="accom_cancel_policy_yes" style="display:none;">
                                                     <input type="input" class="form-control" name="accom_cancel_policy_price" id="accom_cancel_policy_price" value="">
                                                  </div>
                                                 </div>
                                             </div>
                                              <input type="button" name="previous" class="previous action-button" value="Previous" />
                                              <input type="button" name="next" class="next action-button" value="Next" />
                                            </fieldset>
                                            <fieldset>
                                                <h2 class="fs-title">Contact info</h2>
                                                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                                                <input type="text" name="accom_user_name" id="accom_user_name" placeholder="Name" />
                                                <input type="text" name="accom_position" id="accom_position" placeholder="position" />
                                                <input type="text" name="accom_email"  id="accom_email" placeholder="Email" />
                                                <input type="text" name="accom_mobile"  id="accom_mobile" placeholder="mobile number" />
                                                <input type="button" name="previous" class="previous action-button" value="Previous" />
                                                <input type="submit" id="accommodations" name="accommodationssubmit" class="submit action-button" value="Submit" />
                                           </fieldset>
                                       </form> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Accoumadation form create--->
                    <!-- Tours form create--->
                    <div id="tours" class="tab-pane fade in">
                        <div id="tours_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addatours" data-id="tours_add">Add Tours</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_three_forms',array('form_type'=>'Tours','hosts_id'=>$this->session->userdata('userID'))); 
								if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['formsID']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['form_title']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['categoryID'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['subcategoryID'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Price  :</strong> <?php echo $manage['form_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Child Price :</strong> <?php echo $manage['form_child_age'] ?> </p>
                                            <p class="col-sm-5"><strong>Group Price :</strong> <?php echo $manage['form_grp_price'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['form_desc']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['formsID']; ?>" data-action="toursedit"data-id="tours_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="<?php echo $manage['formsID']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['form_name']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['form_position']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['form_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['form_mobile']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="tours_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- Tours form create--->
                    <!-- adventure form create--->
                    <div id="adventure" class="tab-pane fade in">
                        <div id="adventure_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addadventure" data-id="adventure_add">Add Adventure</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_three_forms',array('form_type'=>'Adventure','hosts_id'=>$this->session->userdata('userID'))); 
								if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['formsID']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['form_title']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['categoryID'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['subcategoryID'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Price  :</strong> <?php echo $manage['form_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Child Price :</strong> <?php echo $manage['form_child_age'] ?> </p>
                                            <p class="col-sm-5"><strong>Group Price :</strong> <?php echo $manage['form_grp_price'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['form_desc']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['formsID']; ?>" data-action="adventureedit"data-id="adventure_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="<?php echo $manage['formsID']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['form_name']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['form_position']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['form_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['form_mobile']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="adventure_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- adventure form create--->
                     <!-- recreations form create--->
                    <div id="recreations" class="tab-pane fade in">
                        <div id="recreations_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addrecreations" data-id="recreations_add">Add Recreations</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_three_forms',array('form_type'=>'Recreation','hosts_id'=>$this->session->userdata('userID'))); 
								if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['formsID']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['form_title']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['categoryID'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['subcategoryID'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Price  :</strong> <?php echo $manage['form_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Child Price :</strong> <?php echo $manage['form_child_age'] ?> </p>
                                            <p class="col-sm-5"><strong>Group Price :</strong> <?php echo $manage['form_grp_price'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['form_desc']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['formsID']; ?>" data-action="adventureedit"data-id="adventure_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="<?php echo $manage['formsID']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['form_name']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['form_position']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['form_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['form_mobile']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="recreations_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- recreations form create--->
                    <div id="transportation" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Personal Information</h4>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Change Password</h4>
								
							</div>
							
						</div>
					</div>
                    <!-- Coupon form create--->
                    <div id="coupon" class="tab-pane fade in">
                        <div id="coupon_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addcoupon" data-id="coupon_add">Add Coupon</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $managecoupon=$this->master_model->getRecords('tbl_coupon_master',array('hosts_id'=>$this->session->userdata('userID'))); 
								
							    if(count($managecoupon))
								{ 	
								  foreach($managecoupon as $coupon)
								  {
					             ?>
                                 <div class="item-entry" id="remove_coupon_<?php echo $coupon['coupon_id']; ?>">
                                    <!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
                                    <div class="item-content">
                                        <div class="item-body">
                                             <div class="col-md-9 col-sm-10">
                                                <h4><?php echo $coupon['coupon_title']; ?></h4>
                                                <?php $couponcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$coupon['category_id'])); ?>
                                                <p class="col-md-5"><strong>Category :</strong><?php echo $couponcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                                <?php $couponsubcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$coupon['subcategory_id'])); ?>
                                                <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $couponsubcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                                <p class="col-sm-5"><strong>Price :</strong> <?php echo $coupon['price'] ?> </p>
                                                <p class="col-sm-5"><strong>Address :</strong> <?php echo $coupon['coupon_address'] ?> </p>
                                                <p class="col-sm-5"><strong>Description  :</strong> <?php echo $coupon['coupon_description']; ?> </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <p><a href="javascript:void(0)" class="editclass" rel="<?php echo $coupon['coupon_id']; ?>" data-action="couponedit" data-id="accomodations_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="<?php echo $coupon['coupon_id']; ?>" class="removeaccom">Remove</a></p>
                                            </div>
                                        </div>
                                        <div class="item-footer"> 
                                            <h5><?php echo 'User Info'; ?></h5>
                                            <p class="col-md-5"><strong>Name  :</strong> <?php echo $coupon['coupon_user_name']; ?> </p>
                                            <p class="col-md-5"><strong>Position  :</strong> <?php echo $coupon['coupon_person_postion']; ?> </p>
                                            <p class="col-md-5"><strong>Email  :</strong> <?php echo $coupon['coupon_email']; ?> </p>
                                            <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $coupon['coupon_mobile']; ?> </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                  <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="coupon_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- Coupon form create--->
                    <div id="events" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Personal Information</h4>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Change Password</h4>
								
							</div>
							
						</div>
					</div>
                    <div id="restaurants" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Personal Information</h4>
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Change Password</h4>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END: USER PROFILE -->
    
<!-- END: USER PROFILE -->
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		 jQuery('#userstory').validate({});
         jQuery('#passwordForm').validate({});
		 jQuery('#userProfile').validate({});
	});
</script>