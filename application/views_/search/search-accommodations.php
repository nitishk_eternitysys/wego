<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Check-In</label>
        <div class="input-group margin-bottom-sm">
            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkin" id="checkin" value="<?php echo $_GET['checkin']; ?>">
            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Check-Out</label>
        <div class="input-group margin-bottom-sm">
            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkout" id="checkout"  value="<?php echo $_GET['checkout']; ?>">
            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(e) {
     jQuery('#checkin').datetimepicker({
	  format:'d-m-Y',
	  minDate:'<?php echo date('Y-m-d'); ?>',
	  timepicker: false,
	 
 });
	jQuery('#checkout').datetimepicker({
	  format:'d-m-Y',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		minDate:jQuery('#checkin').val()?jQuery('#checkin').val():false
	   })
	  },
	});
});

</script>