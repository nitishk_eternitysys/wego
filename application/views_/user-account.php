<!-- START: USER PROFILE -->
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3>Welcome, <?php echo $fetch_array[0]['user_name']; ?></h3>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#profile-overview" class="text-center"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
						
						<li><a data-toggle="tab" href="#profile" class="text-center"><i class="fa fa-user"></i> <span>Profile</span></a></li>
						<li><a data-toggle="tab" href="#booking" class="text-center"><i class="fa fa-history"></i> <span>Bookings</span></a></li>	
                        <!--<li><a data-toggle="tab" href="#wishlist" class="text-center"><i class="fa fa-heart-o"></i> <span>Wishlist</span></a></li>
						<li><a data-toggle="tab" href="#cards" class="text-center"><i class="fa fa-credit-card"></i> <span>My Cards</span></a></li>
						<li><a data-toggle="tab" href="#complaint" class="text-center"><i class="fa fa-edit"></i> <span>Complaints</span></a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
					<div id="profile-overview" class="tab-pane fade in active">
						<div class="col-md-6">
							<div class="brief-info">
								<div class="col-md-2 col-sm-2 clear-padding">
									<img src="assets/images/user1.jpg" alt="cruise">
								</div>
								<div class="col-md-10 col-sm-10">
									<h3><?php echo ucfirst($fetch_array[0]['user_name']); ?></h3>
									<h5><i class="fa fa-envelope-o"></i><?php echo $fetch_array[0]['user_email']; ?></h5>
									<h5><i class="fa fa-phone"></i>+<?php echo $fetch_array[0]['user_mobile']; ?></h5>
									<h5><i class="fa fa-map-marker"></i><?php echo $fetch_array[0]['user_address']; ?></h5>
								</div>
								<div class="clearfix"></div>
								<div class="brief-info-footer">
									<a data-toggle="tab" href="#profile" class="text-center" id="profileDiv"><i class="fa fa-edit"></i>Edit Profile</a>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="most-recent-booking">
								<h4>Recent Booking</h4>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-profile-offer">
								<h4>Offers For You</h4>
								<div class="offer-body">
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>20% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 20% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>30% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 30% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>10% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 10% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="user-notification">
									<h4>Notification</h4>
									<div class="notification-body">
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bolt"></i> 50% off on all items <span class="pull-right">08h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-sun-o"></i> New Year special offer <span class="pull-right">1d ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div id="booking" class="tab-pane fade in">
					   <div class="col-md-12">
                           <div class="text-center" role="tabpanel">
                                <!-- BEGIN: CATEGORY TAB -->
                                <ul id="hotDeal" role="tablist" class="nav nav-tabs">
                                    <li class="active text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab1" href="#tab1">
                                            <i class="fa fa-bed"></i> 
                                            <span>Accommodations</span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab2" href="#tab2">
                                            <i class="fa fa-plane"></i> 
                                            <span>Tours</span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab3" href="#tab3">
                                            <i class="fa fa-suitcase"></i> 
                                            <span>Adventure</span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab4" href="#tab4">
                                            <i class="fa fa-taxi"></i> 
                                            <span>Recreations</span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- END: CATEGORY TAB -->
                                <div class="clearfix"></div>
                                <!-- BEGIN: TAB PANELS -->
                                <div class="tab-content">
                                    <!-- BEGIN: FLIGHT SEARCH -->
                                     <div id="tab1" class="tab-pane active fade in" >
                                     <?php
                                    $this->db->join('tbl_accommodations_master','tbl_accommodations_master.accom_id=tbl_transaction_master.booking_form_id');
                                    $this->db->select('tbl_transaction_master.*,tbl_accommodations_master.accom_title AS title,tbl_accommodations_master.accom_image AS image,tbl_accommodations_master.accom_cancel_policy');
								     $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'accom'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php  } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <?php 
													if($order['accom_cancel_policy']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 }
													 else if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													} ?>
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab2" class="tab-pane fade" >
                                     <?php
									 $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image,tbl_three_forms.form_refundable,tbl_three_forms.form_departure_time,tbl_three_forms.form_departure_address');
									 $this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_transaction_master.booking_form_id');
									 $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'Tours'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                        <p><strong>Report Address</strong>: <?php echo ucfirst($order['form_departure_time']); ?></p>
                                                        <p><strong>Reporting Time</strong>: <?php echo ucfirst($order['form_departure_address']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                     <?php 
													if($order['form_refundable']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 } }
													 if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													 ?>
                                                  </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab3" class="tab-pane fade" >
                                     <?php
									   $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image,tbl_three_forms.form_refundable,tbl_three_forms.form_departure_time,tbl_three_forms.form_departure_address');
									  $this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_transaction_master.booking_form_id');
									  $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'Adventure'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                        <p><strong>Report Address</strong>: <?php echo ucfirst($order['form_departure_time']); ?></p>
                                                        <p><strong>Reporting Time</strong>: <?php echo ucfirst($order['form_departure_address']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <?php 
													if($order['form_refundable']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 } }
													 if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													 ?>
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else{ ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab4" class="tab-pane fade" >
                                     <?php
									 $this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_transaction_master.booking_form_id');
									 $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image,tbl_three_forms.form_refundable,tbl_three_forms.form_departure_time,tbl_three_forms.form_departure_address');
                                     $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'Recreation'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                              if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                        <p><strong>Report Address</strong>: <?php echo ucfirst($order['form_departure_time']); ?></p>
                                                        <p><strong>Reporting Time</strong>: <?php echo ucfirst($order['form_departure_address']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <?php 
													if($order['form_refundable']!='no'){ 
													 if($order['status']=='pending')
													 {
													?>
                                                      <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a></p>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
    											   </div>
                                                    </form>
                                                    <?php 
													 } }
													 if($order['status']=='cancel')
													 {
													  ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                      <form method="post" action="<?php echo base_url().'user/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                    <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
        												<div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
    											   </div>
                                                    </form>   
                                                      <?php
													 }
													 else if($order['status']=='approve')
													 {
													  ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
													  }
													 ?>
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>user/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>  
                                </div>
                           </div>
			           </div>
					</div>
					<div id="profile" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Personal Information</h4>
								<div class="user-info-body">
									 <form method="post" id="userProfile" enctype="multipart/form-data">
										<div class="col-md-12">
											<label>User Name</label>
											<input name="user_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true"  value="<?php echo $fetch_array[0]['user_name']; ?>">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12">
											<label>Email-ID</label>
											 <input name="user_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" value="<?php echo $fetch_array[0]['user_email']; ?>" data-rule-email="true" readonly>
										</div>
										<div class="col-md-12">
									   <label>Birth day</label>
									   <input name="user_birth_day" type="text" class="form-control" placeholder="Birth day" data-rule-required="true" value="<?php echo $fetch_array[0]['user_birth_day']; ?>"  >	</div>
									   <div class="col-md-12">
										<label>Gender</label>
									      <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="male" <?php if($fetch_array[0]['user_gender']=='male'){echo 'checked="checked"'; } ?> > Male
                                          <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="female" <?php if($fetch_array[0]['user_gender']=='female'){echo 'checked="checked"'; } ?> >Female
										</div>
										<div class="col-md-12">
                                         <label>Mobile</label>
                                         <input name="user_mobile" type="text" class="form-control" placeholder="mobile" data-rule-required="true" value="<?php echo $fetch_array[0]['user_mobile']; ?>" >
										</div>	
										<div class="col-md-12">
											<label>Address</label>
											<input name="user_address" type="text" class="form-control" placeholder="Address" data-rule-required="true" value="<?php echo $fetch_array[0]['user_address']; ?>" >
										</div>
                                        <div class="col-md-12">
											<label>Country</label>
                                            
										   <!--<input name="user_country" type="text" class="form-control" placeholder="Country" data-rule-required="true" value="<?php //echo $fetch_array[0]['user_country']; ?>" >-->
										</div>
                                        <div class="col-md-12">
										  <label>City</label>
										  <input name="user_city" type="text" class="form-control" placeholder="City" data-rule-required="true" value="<?php echo $fetch_array[0]['user_city']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Id Proof</label>
										  <input name="user_id_info" type="file"  placeholder="City"   >
                                          <a href="<?php echo base_url().'uploads/profile/'.$fetch_array[0]['user_id_info'] ?>" download>Download</a>
										</div>
                                        <div class="col-md-12">
										  <label>payment method</label>
                                           <select class="form-control" data-rule-required="true" name="user_payment_method" id="user_payment_method">
                                               <option value="">Select Payment</option>
                                               <option value="Paypal" <?php if($fetch_array[0]['user_payment_method']=='Paypal'){echo "selected='selected'";} ?>>Paypal</option>
                                               <option value="Bank" <?php if($fetch_array[0]['user_payment_method']=='Bank'){echo "selected='selected'";} ?>>Bank</option>
                                          </select>
										  <!--<input name="user_payment_method" type="text" class="form-control" placeholder="payment method " data-rule-required="true" value="<?php //echo $fetch_array[0]['user_payment_method']; ?>" >-->
										</div>
                                        <div class="col-md-12">
										  <label>Preferred currency</label>
                                             <select class="form-control" data-rule-required="true" name="user_preferred_currency" id="user_preferred_currency">
                                                <option value="">Select currency</option>
                                                 <?php
                                                 $lang=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
                                                 if(count($lang))
                                                 {
                                                    foreach($lang as $row)
                                                    {
														$sel='';
														if($row['currency_id']==$fetch_array[0]['user_preferred_currency'])
														{
															$sel='selected="selected"';
														}
                                                    ?>
                                                     <option value="<?php echo $row['currency_id']; ?>" <?php echo $sel; ?>><?php echo $row['currency_'.$this->session->userdata('lang')]; ?></option>
                                                   <?php
                                                   }
                                                 }
                                                ?>
                                      </select>
										<!-- <input name="user_preferred_currency" type="text" class="form-control" placeholder="Preferred currency" data-rule-required="true" value="<?php //echo $fetch_array[0]['user_preferred_currency']; ?>"  >-->
										</div>
                                        <div class="col-md-12">
										  <label>Language</label>
										   <select class="form-control" data-rule-required="true" name="user_preferred_language" id="user_preferred_language">
                                                <option value="">Select Language</option>
                                                 <?php
                                                 $lang=$this->master_model->getRecords('tbl_language_master',array('language_status'=>'active'));
                                                 if(count($lang))
                                                 {
                                                    foreach($lang as $row)
                                                    {
														$sel='';
														if($row['language_id']==$fetch_array[0]['user_preferred_language'])
														{
															$sel='selected="selected"';
														}
                                                    ?>
                                                     <option value="<?php echo $row['language_id']; ?>" <?php echo $sel; ?>><?php echo $row['language_'.$this->session->userdata('lang')]; ?></option>
                                                   <?php
                                                   }
                                                 }
                                                ?>
                           					</select>
                                           
                                          <!-- <input name="user_preferred_language" type="text" class="form-control" placeholder="Language" data-rule-required="true" value="<?php //echo $fetch_array[0]['user_preferred_language']; ?>" >-->
										</div>
                                        <div class="col-md-12">
										  <label>Website</label>
										  <input name="user_website_here" type="text" class="form-control" placeholder="Website" data-rule-required="true" value="<?php echo $fetch_array[0]['user_website_here']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Interests</label>
										  <textarea name="user_interests" id="user_interests" class="form-control" data-rule-required="true"><?php echo $fetch_array[0]['user_interests']; ?></textarea>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <button type="submit"  name="btn_profile" id="btn_profile">SAVE CHANGES</button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <a href="<?php echo base_url('user/profile'); ?>">CANCEL</a>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Change Password</h4>
								<div class="change-password-body">
									<form >
										<div class="col-md-12">
											<label>Old Password</label>
											<input type="password" placeholder="Old Password" class="form-control" name="old-password">
										</div>
										<div class="col-md-12">
											<label>New Password</label>
											<input type="password" placeholder="New Password" class="form-control" name="new-password">
										</div>
										<div class="col-md-12">
											<label>Confirm Password</label>
											<input type="password" placeholder="Confirm Password" class="form-control" name="confirm-password">
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#flight-preference" aria-expanded="false" aria-controls="flight-preference">
										<i class="fa fa-plane"></i> Flight Preference <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="flight-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label>Price Range</label>
											<select class="form-control" name="flight-price-range">
												<option>Upto $199</option>
												<option>Upto $250</option>
												<option>Upto $499</option>
												<option>Upto $599</option>
												<option>Upto $1000</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Food Preference</label>
											<select class="form-control" name="flight-food">
												<option>Indian</option>
												<option>Chineese</option>
												<option>Sea Food</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Airline</label>
											<select class="form-control" name="flight-airline">
												<option>Indigo</option>
												<option>Vistara</option>
												<option>Spicejet</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#hotel-preference" aria-expanded="false" aria-controls="hotel-preference">
										<i class="fa fa-bed"></i> Hotel Preference <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="hotel-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label>Price Range</label>
											<select class="form-control" name="hotel-price-range">
												<option>Upto $199</option>
												<option>Upto $250</option>
												<option>Upto $499</option>
												<option>Upto $599</option>
												<option>Upto $1000</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Food Preference</label>
											<select class="form-control" name="hotel-food">
												<option>Indian</option>
												<option>Chineese</option>
												<option>Sea Food</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Facilities</label>
											<select class="form-control" name="hotel-facilities">
												<option>WiFi</option>
												<option>Bar</option>
												<option>Restaurant</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Rating</label>
											<select class="form-control" name="hotel-facilities">
												<option>5</option>
												<option>4</option>
												<option>3</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div id="wishlist" class="tab-pane fade in">
						<div class="col-md-12">
							<div class="item-entry">
								<span><i class="fa fa-hotel"></i> Hotel</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer1.jpg" alt="cruise">
										</div>
										<div class="col-md-7 col-sm-7">
											<h4>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Remove</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Total:</strong> $566 <a href="#">Book Now</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span><i class="fa fa-hotel"></i> Hotel</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer2.jpg" alt="cruise">
										</div>
										<div class="col-md-7 col-sm-7">
											<h4>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Remove</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Total:</strong> $566 <a href="#">Book Now</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span><i class="fa fa-hotel"></i> Hotel</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer3.jpg" alt="cruise">
										</div>
										<div class="col-md-7 col-sm-7">
											<h4>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Remove</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Total:</strong> $566 <a href="#">Book Now</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span><i class="fa fa-plane"></i> Flight</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer2.jpg" alt="cruise">
										</div>
										<div class="col-md-7 col-sm-7">
											<h4>New York <i class="fa fa-long-arrow"></i> New Delhi</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Remove</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Total:</strong> $566 <a href="#">Book Now</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="item-entry">
								<span><i class="fa fa-suitcase"></i> Tour</span>
								<div class="item-content">
									<div class="item-body">
										<div class="col-md-2 col-sm-2 clear-padding text-center">
											<img src="assets/images/offer1.jpg" alt="cruise">
										</div>
										<div class="col-md-7 col-sm-7">
											<h4>Wonderful Europe</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="#">Remove</a></p>
										</div>
									</div>
									<div class="item-footer">
										<p><strong>Order Total:</strong> $566 <a href="#">Book Now</a></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="load-more text-center">
								<a href="#">Load More</a>
							</div>
						</div>
					</div>
					<div id="cards" class="tab-pane fade in">
						<div class="col-md-12">
							<div class="col-md-6">
								<div class="card-entry">
									<div class="pull-right">
										<p><a href="#"><i class="fa fa-pencil"></i></a><a href="#"><i class="fa fa-times"></i></a></p>
									</div>
									<h3>XXXX XXXX XXXX 1234</h3>
									<p>Valid Thru 06/17</p>
									<div class="clearfix"></div>
									<div class="card-type">
										<p>Name On Card</p>
										<div class="pull-left">
											<h3>Lenore</h3>
										</div>
										<div class="pull-right">
											<img src="assets/images/card/mastercard.png" alt="cruise">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card-entry">
									<div class="pull-right">
										<p><a href="#"><i class="fa fa-pencil"></i></a><a href="#"><i class="fa fa-times"></i></a></p>
									</div>
									<h3>XXXX XXXX XXXX 2345</h3>
									<p>Valid Thru 06/17</p>
									<div class="clearfix"></div>
									<div class="card-type">
										<p>Name On Card</p>
										<div class="pull-left">
											<h3>Lenore</h3>
										</div>
										<div class="pull-right">
											<img src="assets/images/card/visa.png" alt="cruise">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-6">
								<div class="card-entry primary-card">
									<div class="pull-left">
										<span>Primary</span>
									</div>
									<div class="pull-right">
										<p><a href="#"><i class="fa fa-pencil"></i></a><a href="#"><i class="fa fa-times"></i></a></p>
									</div>
									<div class="clearfix"></div>
									<h3>XXXX XXXX XXXX 1234</h3>
									<p>Valid Thru 06/17</p>
									<div class="clearfix"></div>
									<div class="card-type">
										<p>Name On Card</p>
										<div class="pull-left">
											<h3>Lenore</h3>
										</div>
										<div class="pull-right">
											<img src="assets/images/card/mastercard.png" alt="cruise">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="user-add-card">
									<form >
										<input class="form-control" name="card-number" type="text" required placeholder="Card Number">
										<input class="form-control" name="cardholder-name" type="text" required placeholder="Cardholder Name">
										<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
											<input class="form-control" name="valid-month" type="text" required placeholder="Expiry Month">
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											<input class="form-control" name="valid-year" type="text" required placeholder="Expiry Year">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-4 clear-padding">
											<input class="form-control" name="cvv" type="password" required placeholder="CVV">
										</div>
										<div class="clearfix"></div>
										<div>
											 <button type="submit">ADD CARD</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div id="complaint" class="tab-pane fade in">
						<div class="col-md-12">
							<div class="recent-complaint">
								<h3>Service Requests</h3>
								<div class="complaint-tabs">
									<ul class="nav nav-tabs">
										<li class="active"><a data-toggle="tab" href="#active-complaint" class="text-center"><i class="fa fa-bolt"></i> Active (6)</a></li>
										<li><a data-toggle="tab" href="#resolved-complaint" class="text-center"><i class="fa fa-history"></i> Resolved (4)</a></li>	
									</ul>
								</div>
								<div class="tab-content">
									<div id="active-complaint" class="tab-pane fade in active">
										<p><a href="#"><span>Ticket #123456:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #113443:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #113443:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #123456:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #113443:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #113443:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
									</div>
									<div id="resolved-complaint" class="tab-pane fade in">
										<p><a href="#"><span>Ticket #123456:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #113443:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #113443:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
										<p><a href="#"><span>Ticket #123456:</span> My last booking was failed but ammount has been dedicted from my account.</a></p>
									</div>
								</div>
								<h3>New Requests</h3>
								<div class="submit-complaint">
									<form >
										<div class="col-md-6 col-sm-6 col-xs-6">
											<label>Category</label>
											<select class="form-control" name="category">
												<option>Flight</option>
												<option>Hotel</option>
												<option>Cruise</option>
												<option>Holiday</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6">
											<label>Sub Category</label>
											<select class="form-control" name="sub-category">
												<option>Flight</option>
												<option>Hotel</option>
												<option>Cruise</option>
												<option>Holiday</option>
											</select>
										</div>
										<div class="col-md-12">
											<label>Booking ID</label>
											<input type="text" class="form-control" name="booking-id" placeholder="e.g. CR12567">
										</div>
										<div class="col-md-12">
											<label>Subject</label>
											<input type="text" class="form-control" name="subject" placeholder="Problem Subject">
										</div>
										<div class="col-md-12">
											<label>Problem Description</label>
											<textarea class="form-control" rows="5" name="problem" placeholder="Your Issue"></textarea>
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit">SUBMIT REQUEST</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END: USER PROFILE -->
<!-- END: USER PROFILE -->
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		 jQuery('.alert').show(); 
		 jQuery('#userstory').validate({});
         jQuery('#passwordForm').validate({});
		 jQuery('#userProfile').validate({});
	});
</script>