<link rel="stylesheet" href="<?php echo base_url(); ?>assets/prettyPhoto/css/prettyPhoto.css">
<!-- BEGIN Page Title -->
<div class="page-title">
  <div>
    <h1><i class="fa fa-book"></i><?php echo $pageLable;?></h1>
    <h4><?php echo $pageLable;?></h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li><a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>"><?php echo 'Manage accommodations';?></a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
    <li class="active"><?php echo $pageLable;?></li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class='col-md-12'>
    <div class="box">
        <div class="box-title">
            <h3><i class="fa fa-bars"></i> <?php echo $arr_details[0]['accom_title'];?></h3>	
            <div class="box-tool">
              <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
             <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Accommodations Title </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_title'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Provider Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['hosts_name'];?>
                  </div>
               </div>
               <div class="form-group">
                 <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['accom_category_id'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">Category Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($categoryName[0]['category_name_eng'])){echo $categoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>   
               <div class="form-group">
               <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['accom_subcategory_id'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">SubCategory Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     :  <?php if(isset($subcategoryName[0]['category_name_eng'])){echo $subcategoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Location </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_location']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Description </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_description']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Bed Rooms </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_bed_rooms']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Beds  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_beds']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Bathrooms  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_bathrooms']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Swimming Pool</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_swimming_pool']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Play Yards</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_play_yards']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Balcony</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_balcony']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Garden</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_garden']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Roof</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_roof']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Tv</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_tv']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_tv']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of Tv</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_tv_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Playstation</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_playstation']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_playstation']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of Playstation</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_playstation_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Wifi</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_wifi']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_wifi']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of Wifi</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_wifi_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">DVD</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_dvd']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_dvd']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of DVD</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_dvd_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">GYM</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_gym']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_gym']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of GYM</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_gym_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Kitchen</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_kitchen']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_kitchen']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of kitchen</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_kitchen_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">washing machine</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_washing_machine ']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['accom_washing_machine ']=='yes'){ ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Number of washing machine<</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_washing_machine_number']; ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Check In<</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_check_in']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Check Out<</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_check_out']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Guest Max <</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_guest_max']; ?>
                  </div>
               </div>
              <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Pets </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_pets']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">smoking </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_smoking']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">cooking </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_cooking']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Gustes Allowed </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_gustes_allowed']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_price']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Discount </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_discount']; ?>
                  </div>
               </div>	
              <?php if($arr_details[0]['accom_discount']=='on'){?>
			  <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Discount </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_percentage_discount']; ?>
                  </div>
               </div>	
			   <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Bird Discount  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_bird_discount']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">User name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_user_name']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Position </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_position']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Email </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_email']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Mobile </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['accom_mobile']; ?>
                  </div>
               </div>
              </form>
          </div>
     </div>
  </div>      
</div>
<!-- END Main Content -->