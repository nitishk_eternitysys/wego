<link rel="stylesheet" href="<?php echo base_url(); ?>assets/prettyPhoto/css/prettyPhoto.css">
<!-- BEGIN Page Title -->
<div class="page-title">
  <div>
    <h1><i class="fa fa-book"></i><?php echo $pageLable;?></h1>
    <h4><?php echo $pageLable;?></h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li><a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>"><?php echo 'Manage Event';?></a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
    <li class="active"><?php echo $pageLable;?></li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class='col-md-12'>
    <div class="box">
        <div class="box-title">
            <h3><i class="fa fa-bars"></i> <?php echo $arr_details[0]['event_title'];?></h3>	
            <div class="box-tool">
              <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
             <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Event Title </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_title'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Provider Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['hosts_name'];?>
                  </div>
               </div>
               <div class="form-group">
                 <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['event_category_id'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">Category Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($categoryName[0]['category_name_eng'])){echo $categoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>   
               <div class="form-group">
               <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['event_subcategory_id'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">SubCategory Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     :  <?php if(isset($subcategoryName[0]['category_name_eng'])){echo $subcategoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Location </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_location']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Description </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_description']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Duration </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_duration']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Address  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_address']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_user_name']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Position</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_position']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Email</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_email']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Mobile</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['event_mobile']; ?>
                  </div>
               </div>
               
              </form>
          </div>
     </div>
  </div>      
</div>
<!-- END Main Content -->