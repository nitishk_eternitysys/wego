<link rel="stylesheet" href="<?php echo base_url(); ?>assets/prettyPhoto/css/prettyPhoto.css">
<!-- BEGIN Page Title -->
<div class="page-title">
  <div>
    <h1><i class="fa fa-book"></i><?php echo $pageLable;?></h1>
    <h4><?php echo $pageLable;?></h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li><a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>"><?php echo 'Manage tours';?></a> <span class="divider"><i class="fa fa-angle-right"></i></span></li>
    <li class="active"><?php echo $pageLable;?></li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class='col-md-12'>
    <div class="box">
        <div class="box-title">
            <h3><i class="fa fa-bars"></i> <?php echo $arr_details[0]['form_title'];?></h3>	
            <div class="box-tool">
              <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
             <form action="#" class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Title </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_title'];?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Provider Name </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['hosts_name'];?>
                  </div>
               </div>
               <div class="form-group">
                 <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['categoryID'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">Category Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php if(isset($categoryName[0]['category_name_eng'])){echo $categoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>   
               <div class="form-group">
               <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$arr_details[0]['subcategoryID'])); ?>
                  <label class="col-sm-3 col-lg-2 control-label">SubCategory Name  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     :  <?php if(isset($subcategoryName[0]['category_name_eng'])){echo $subcategoryName[0]['category_name_eng'];} ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Duration </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_duration']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Description </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_desc']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Why you </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_why_you']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Location  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_location']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Address  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_address']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Name</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_name']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Position</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_position']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Email</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_email']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Mobile</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_mobile']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_price']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Minimum Age</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_min_age']; ?>
                  </div>
               </div>
              
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Max Age</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_max_age']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Less than age</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_lessthan_age']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Child price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_child_price']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Child age</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_child_age']; ?>
                  </div>
               </div>
              
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Min member</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_min_member']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Max member</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_max_member']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Group price</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_grp_price']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Gender</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_gender']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Age</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_age']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Weight</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_weight']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">What allowed </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_what_allowed']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">What not allowed </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_not_allowed']; ?>
                  </div>
               </div>
              
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Additional Info</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_additional_info']; ?>
                  </div>
               </div>
               
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Refundable</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_refundable']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['form_refundable']=='yes'){?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">% chargeable if cancelled before </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_cancel_before']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Nonrefundable if cancelled before  </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_not_cancel'].''.$arr_details[0]['form_not_cancel_hrs']; ?>
                  </div>
               </div>
               <?php } ?>
              <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Available </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_availalbe_for']; ?>
                  </div>
               </div>
               <?php if($arr_details[0]['form_availalbe_for']=='daily'){?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Availability date from </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_available_from_date']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Availability date to </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_availalbe_to_date']; ?>
                  </div>
               </div>
                <?php } else { ?>
                <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Availability </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                  <div class="table-responsive" style="border:0">
                   <table class="table table-advance">
                       <thead>
                            <tr>
                               
                               <th>Day</th>
                               <th>From time</th>
                               <th>To time</th>
                              
                            </tr>
                        </thead>
                        <tbody>
                         <?php
						   $this->db->join('tbl_three_forms','tbl_three_forms.formsID=tbl_available_days.formID');	
							$days=$this->master_model->getRecords('tbl_available_days',array('formID'=>$arr_details[0]['formsID']));
							if(count($days))
							{
								foreach($days as $day)
								{
			              ?>
                             <tr>
                                <td><?php echo $day['day']; ?></td>
                                <td><?php echo date('H:i:s',strtotime($day['form_from_time'])); ?></td>
                                <td><?php echo date('H:i:s',strtotime($day['form_to_time'])); ?></td>
                             </tr>
                        <?php 
						   }
						 }
						 else
						 { ?>
                        <tr>
                          <td colspan="3"><div class="alert alert-danger" style="text-align:center;">No Data Found.</div></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                  </div>
                </div>
               <?php } ?>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Departure </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_departure']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Departure Address</label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_departure_address']; ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Departure Time </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo $arr_details[0]['form_departure_time']; ?>
                  </div>
               </div>	
              
			  <div class="form-group">
                  <label class="col-sm-3 col-lg-2 control-label">Added Date </label>
                  <div class="col-sm-9 col-lg-10 controls" style="padding-top:7px;">
                     : <?php echo date('d-m-Y',strtotime($arr_details[0]['form_added_date'])); ?>
                  </div>
               </div>	
			  
              </form>
          </div>
     </div>
  </div>      
</div>
<!-- END Main Content -->