<!-- START: USER PROFILE -->
<style>
/*form styles*/
.msform {
margin:20px auto 0;

position: relative;
}
.msform fieldset {
border: 0 none;
/*background: white;    
border-radius: 3px;
box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);*/
padding:20px 30px 0;
box-sizing: border-box;
width: 100%;
margin: 0 auto;
/*stacking fieldsets above each other
position: absolute;*/
}
/*Hide all except first fieldset*/
.msform fieldset:not(:first-of-type) {
display: none;
}
/*inputs*/

.msform input, .msform textarea {
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 0px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
    color: #555;
    display:inline-block;
    font-size: 14px;
    height:40px;
    line-height: 1.42857;
    padding: 6px 12px;
    margin-bottom:20px;
    transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
    width: 100%;
}
    
    .user-personal-info .form-control, .user-change-password .form-control, .user-preference .form-control {

    border-radius: 0px;
    height:40px;
    margin-bottom: 15px;
    padding: 8px;
}
.user-personal-info, .user-change-password, .user-preference, .card-entry, .user-add-card{margin-top:20px;}
.user-personal-info label.error {
    bottom: -1px;
    display: block;
    font-size: 13px;
    font-weight: normal;
    left: 16px;
    margin-bottom: 2px;
    width: 100%;
    position: absolute;
    text-align: left;
    color: #f00;
}    
    
/*buttons*/
.msform .action-button {
width: 100px;
background: #00adef;
font-weight: bold;
color: white;
border: 0 none;
border-radius: 1px;
cursor: pointer;
padding: 10px 5px;
margin:30px 10px 20px;
}
.msform .action-button:hover, .msform .action-button:focus {
/*box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;*/
}
/*headings*/
.fs-title {
font-size: 20px;
text-transform: uppercase;
color: #2C3E50;
margin:0 0 25px;
}
.fs-subtitle {
font-weight: normal;
font-size: 13px;
color: #666;
margin-bottom: 20px;
}
/*progressbar*/
.progressbar {
margin:0px;
padding:0px;
overflow: hidden;
text-align: center;
/*CSS counters to number the steps*/
counter-reset: step;
} 
.progressbar li.big-text{width: 12%;}    
#accommodations .progressbar li{float:left;}   
#tours .progressbar li{float:none; display: inline-block;width: 13.4%;vertical-align: top;}   
#adventure .progressbar li{float:none; display: inline-block; width:13.5%;}   
#recreations .progressbar li{float:none; display: inline-block; width:13.5%;}   
#transportation .progressbar li{float:none; display: inline-block; width:20%;}   
#events .progressbar li{float:none; display: inline-block; width:20%;}   
#coupon .progressbar li{float:none; display: inline-block; width:16%;}   
    
.progressbar li {
list-style-type: none;
text-transform: none;
font-size: 13px;
width:10.6%;
float: left;
position: relative;
vertical-align: top;
}
.progressbar li.active {
    color: #00adef;
}
.progressbar li:before {
content: counter(step);
counter-increment: step;
width: 50px;
height:50px;    
line-height: 48px;
display: block;
font-size: 21px;
color: #333;
background: #e6e6e6;
border-radius: 30px;
margin: 0 auto 5px auto;
}
/*progressbar connectors*/
.progressbar li:after {
content: '';
width: 100%;
height: 2px;
background: white;
position: absolute;
left: -50%;
top: 9px;
z-index: -1; /*put it behind the numbers*/
}
.progressbar li:first-child:after {
/*connector not needed before the first step*/
content: none;
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
.progressbar li.active:before, .progressbar li.active:after {
background: #00adef;
color: white;
}

label.btn span {vertical-align: top;
  font-size: 1.5em ;
}

label input[type="radio"] ~ i.fa.fa-circle-o{
    color: #c8c8c8;    display: inline;
}
label input[type="radio"] ~ i.fa.fa-check-circle-o{
    display: none;
}
label input[type="radio"]:checked ~ i.fa.fa-circle-o{
    display: none;
}
label input[type="radio"]:checked ~ i.fa.fa-check-circle-o{
    color: #7AA3CC;    display: inline;
}
label:hover input[type="radio"] ~ i.fa {
color: #7AA3CC;
}

label input[type="checkbox"] ~ i.fa.fa-square-o{
    color: #c8c8c8;    display: inline;
}
label input[type="checkbox"] ~ i.fa.fa-check-square-o{
    display: none;
}
label input[type="checkbox"]:checked ~ i.fa.fa-square-o{
    display: none;
}
label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{
    color: #7AA3CC;    display: inline;
}
label:hover input[type="checkbox"] ~ i.fa {
color: #7AA3CC;
}

div[data-toggle="buttons"] label.active{
    color: #7AA3CC;
}

div[data-toggle="buttons"] label {
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: normal;
line-height: 2em;
text-align: left;
white-space: nowrap;
vertical-align: top;
cursor: pointer;
background-color: none;
border: 0px solid #c8c8c8;
border-radius: 3px;
color: #c8c8c8;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
-o-user-select: none;
user-select: none;
position: relative; 
width: auto;
}

div[data-toggle="buttons"] label:hover {
color: #7AA3CC;
}

div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
-webkit-box-shadow: none;
box-shadow: none;
}
.switch-but{background:#ccc;border-radius:20px;padding:5px;}    
</style>
<?php 
if($error!=''){  ?>
<div class="alert alert-danger"><?php echo $error; ?></div>
<?php } 
if($this->session->flashdata('success')!=''){?>	
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<!-- START: USER PROFILE -->
<div class="row user-profile">
		<div class="container">
			<div class="col-md-12 user-name">
				<h3>Welcome, <?php echo $fetch_array[0]['hosts_name']; ?></h3>
			</div>
			<div class="col-md-2 col-sm-2">
				<div class="user-profile-tabs">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#profile-overview" class="text-center"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
                        <li><a data-toggle="tab" href="#booking" class="text-center"><i class="fa fa-history"></i> <span>Bookings</span></a></li>	
                    
						<li><a data-toggle="tab" href="#profile" class="text-center"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                        <?php if(count($categories)>0){ 
								foreach($categories as $cat){
								$owner_cat=explode(',',$fetch_array[0]['hosts_mainategory_id']);
								if(in_array($cat['category_id'],$owner_cat))
								{
						?>
						<li><a data-toggle="tab" href="#<?php echo $cat['category_slug'] ?>" class="text-center"><i class="<?php echo $cat['category_icon'];  ?>"></i> <span><?php echo $cat['category_name_'.$this->session->userdata('lang')] ?></span></a></li>
                        <?php } } } ?>
					 
                    </ul>
				</div>
			</div>
			<div class="col-md-10 col-sm-10">
				<div class="tab-content">
					<div id="profile-overview" class="tab-pane fade in active">
						<div class="col-md-6">
							<div class="brief-info">
								<!--<div class="col-md-2 col-sm-2 clear-padding">
									<img src="assets/images/user1.jpg" alt="cruise">
								</div>-->
								<div class="col-md-10 col-sm-10">
									<h3><?php echo ucfirst($fetch_array[0]['hosts_name']); ?></h3>
									<h5><i class="fa fa-envelope-o"></i><?php echo $fetch_array[0]['hosts_email']; ?></h5>
									<h5><i class="fa fa-phone"></i>+<?php echo $fetch_array[0]['hosts_contact']; ?></h5>
									<h5><i class="fa fa-map-marker"></i><?php echo $fetch_array[0]['hosts_address']; ?></h5>
								</div>
								<div class="clearfix"></div>
								<div class="brief-info-footer">
									<a data-toggle="tab" href="#profile" class="text-center" id="profileDiv"><i class="fa fa-edit"></i>Edit Profile</a>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="most-recent-booking">
								<h4>Recent Booking</h4>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-plane"></i>New York<i class="fa fa-long-arrow-right"></i>New Delhi</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-bed"></i>Grand Lilly <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="confirmed"><i class="fa fa-check"></i>Confirmed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="field-entry">
									<div class="col-md-6 col-sm-4 col-xs-4 clear-padding">
										<p><i class="fa fa-suitcase"></i>Wonderful Europe</p>
									</div>
									<div class="col-md-4 col-sm-6 col-xs-6">
										<p class="failed"><i class="fa fa-times"></i>failed</p>
									</div>
									<div class="col-md-2 col-sm-2 col-xs-2 text-center clear-padding">
										<a href="#">Detail</a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-profile-offer">
								<h4>Offers For You</h4>
								<div class="offer-body">
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>20% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 20% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>30% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 30% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="offer-entry">
										<div class="col-md-4 col-sm-4 col-xs-4 offer-left text-center">	
											<p>10% OFF</p>
										</div>
										<div class="col-md-8 col-sm-8 col-xs-8 offer-right">
											<p>Get 10% OFF on flights when you pay with your Bank of America Credit Card. <a href="#">Book Now</a></p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="user-notification">
									<h4>Notification</h4>
									<div class="notification-body">
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bolt"></i> 50% off on all items <span class="pull-right">08h ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-sun-o"></i> New Year special offer <span class="pull-right">1d ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-plane"></i> Domestic Flights Starting from $199 <span class="pull-right">1m ago</span></p>
										</div>
										<div class="notification-entry">
											<p><i class="fa fa-bed"></i> 20% Cashback on hotel booking <span class="pull-right">1h ago</span></p>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div id="booking" class="tab-pane fade in">
					   <div class="col-md-12">
                           <div class="text-center" role="tabpanel">
                                <!-- BEGIN: CATEGORY TAB -->
                                <ul id="hotDeal" role="tablist" class="nav nav-tabs">
                                    <li class="active text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab1" href="#tab1">
                                            <i class="fa fa-bed"></i> 
                                            <span>Accommodations</span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab2" href="#tab2">
                                            <i class="fa fa-plane"></i> 
                                            <span>Tours</span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab3" href="#tab3">
                                            <i class="fa fa-suitcase"></i> 
                                            <span>Adventure</span>
                                        </a>
                                    </li>
                                    <li class="text-center" role="presentation">
                                        <a data-toggle="tab" role="tab" aria-controls="tab4" href="#tab4">
                                            <i class="fa fa-taxi"></i> 
                                            <span>Recreations</span>
                                        </a>
                                    </li>
                                </ul>
                                <!-- END: CATEGORY TAB -->
                                <div class="clearfix"></div>
                                <!-- BEGIN: TAB PANELS -->
                                <div class="tab-content">
                                     <div id="tab1" class="tab-pane active fade in" >
                                     <?php
									 $this->db->join('tbl_accommodations_master','tbl_transaction_master.booking_form_id=tbl_accommodations_master.accom_id');
                                     $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('tbl_accommodations_master.hosts_id'=>$this->session->userdata('userID'),'tbl_transaction_master.type'=>'accom'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['accom_image']!=''){  ?>
                                                            <img src="<?php echo $this->master_model->resize($order['accom_image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['accom_title']; ?>">
                                                         <?php } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['accom_title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['accom_title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-2 col-sm-3">
                                                        <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <p>
                                                     <?php 
                                                     if($order['status']=='pending')
                                                     {
                                                    ?>
                                                      <div class="col-md-2 col-sm-2">
                                                        <a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a>
                                                      </div>
                                                      <div class="col-md-2 col-sm-3">
                                                       <a style="margin-left:05px;" href="javascript:void(0);" rel="modal-approve-<?php echo $order['orderID']; ?>" class="bookingapprove" >Approve</a>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'owner/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                      <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form>
                                                      <form method="post" action="<?php echo base_url().'ajax/approve/'.$order['orderID'].''; ?>" name="form-submit" id="approve-<?php echo $order['orderID']; ?>">
                                                      <div id="modal-approve-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="reson" id="reson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtnapprove">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form> 
                                                    <?php 
                                                     }
                                                     else if($order['status']=='cancel')
                                                     {
                                                      ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                       <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
                                                   </div>
                                                      <?php
                                                     }
                                                     else if($order['status']=='approve')
                                                     {
                                                      ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
                                                      }
                                                     ?>
                                                    </p>  
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>owner/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab2" class="tab-pane fade" >
                                     <?php
									  $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image');
									  $this->db->join('tbl_three_forms','tbl_transaction_master.booking_form_id=tbl_three_forms.formsID');
									  $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('tbl_three_forms.hosts_id'=>$this->session->userdata('userID'),'tbl_transaction_master.type'=>'Tours'));
									  if(count($fetch_order)>0)
                                      { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <p>
                                                     <?php 
                                                     if($order['status']=='pending')
                                                     {
                                                    ?>
                                                      <div class="col-md-2 col-sm-2">
                                                        <a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a>
                                                      </div>
                                                      <div class="col-md-2 col-sm-3">
                                                       <a style="margin-left:05px;" href="javascript:void(0);" rel="modal-approve-<?php echo $order['orderID']; ?>" class="bookingapprove" >Approve</a>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'owner/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                      <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form>
                                                      <form method="post" action="<?php echo base_url().'ajax/approve/'.$order['orderID'].''; ?>" name="form-submit" id="approve-<?php echo $order['orderID']; ?>">
                                                      <div id="modal-approve-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="reson" id="reson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtnapprove">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form> 
                                                    <?php 
                                                     }
                                                     else if($order['status']=='cancel')
                                                     {
                                                      ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                       <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
                                                   </div>
                                                      <?php
                                                     }
                                                     else if($order['status']=='approve')
                                                     {
                                                      ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
                                                      }
                                                     ?>
                                                    </p>
                                                    
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>owner/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab3" class="tab-pane fade" >
                                     <?php
									 $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image');
									 $this->db->join('tbl_three_forms','tbl_transaction_master.booking_form_id=tbl_three_forms.formsID');
                                     $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('tbl_three_forms.hosts_id'=>$this->session->userdata('userID'),'tbl_transaction_master.type'=>'Adventure'));
                                    // $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>''));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <p>
                                                     <?php 
                                                     if($order['status']=='pending')
                                                     {
                                                    ?>
                                                      <div class="col-md-2 col-sm-2">
                                                        <a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a>
                                                      </div>
                                                      <div class="col-md-2 col-sm-3">
                                                       <a style="margin-left:05px;" href="javascript:void(0);" rel="modal-approve-<?php echo $order['orderID']; ?>" class="bookingapprove" >Approve</a>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'owner/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                      <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form>
                                                      <form method="post" action="<?php echo base_url().'ajax/approve/'.$order['orderID'].''; ?>" name="form-submit" id="approve-<?php echo $order['orderID']; ?>">
                                                      <div id="modal-approve-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="reson" id="reson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtnapprove">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form> 
                                                    <?php 
                                                     }
                                                     else if($order['status']=='cancel')
                                                     {
                                                      ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                       <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
                                                   </div>
                                                      <?php
                                                     }
                                                     else if($order['status']=='approve')
                                                     {
                                                      ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
                                                      }
                                                     ?>
                                                    </p>
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>owner/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>
                                     <div id="tab4" class="tab-pane fade" >
                                     <?php
									 $this->db->select('tbl_transaction_master.*,tbl_three_forms.form_title AS title,tbl_three_forms.form_image AS image'); 
									 $this->db->join('tbl_three_forms','tbl_transaction_master.booking_form_id=tbl_three_forms.formsID');
                                     $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('tbl_three_forms.hosts_id'=>$this->session->userdata('userID'),'tbl_transaction_master.type'=>'Recreation'));
									
                                    // $fetch_order=$this->master_model->getRecords('tbl_transaction_master',array('truserID'=>$this->session->userdata('userID'),'type'=>'Recreation'));
                                     if(count($fetch_order)>0)
                                     { 
                                        foreach($fetch_order as $order)
                                        {
                                         ?>
                                          <div class="item-entry">
                                            <span>Order ID: <?php echo $order['orderID']; ?></span>
                                            <div class="item-content">
                                                <div class="item-body">
                                                    <div class="col-md-2 col-sm-2 clear-padding text-center">
                                                        <?php if($order['image']!=''){ 
                                                                if($order['type']=='Tours' || $order['type']=='Adventure' || $order['type']=='Recreation'){
                                                        ?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/forms/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } else {?>
                                                            <img src="<?php echo $this->master_model->resize($order['image'],500,350,'uploads/accom/');?>" alt="<?php echo $order['title']; ?>">
                                                         <?php } } else { ?>
                                                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $order['title']; ?>">
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4">
                                                        <h4><?php echo ucfirst($order['title']); ?></h4>
                                                        <p><strong>Booking Type</strong>: <?php echo ucfirst($order['booking_type']); ?> </p>
                                                        <p><strong>Payment by</strong>: <?php echo ucfirst($order['payType']); ?></p>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                    <?php if($order['status']=='pending'){ ?>
                                                        <p class="failed"><i class="fa fa-minus"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='approve'){ ?>
                                                        <p class="confirmed"><i class="fa fa-check"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else if($order['status']=='cancel'){ ?>
                                                        <p class="failed"><i class="fa fa-remove"></i><?php echo ucfirst($order['status']); ?></p>
                                                        <?php } else {} ?>
                                                    </div>
                                                    <p>
                                                     <?php 
                                                     if($order['status']=='pending')
                                                     {
                                                    ?>
                                                      <div class="col-md-2 col-sm-2">
                                                        <a href="javascript:void(0);" class="btn_cancel" rel="modal-<?php echo $order['orderID']; ?>" >Cancel</a>
                                                      </div>
                                                      <div class="col-md-2 col-sm-3">
                                                       <a style="margin-left:05px;" href="javascript:void(0);" rel="modal-approve-<?php echo $order['orderID']; ?>" class="bookingapprove" >Approve</a>
                                                    </div>
                                                      <form method="post" action="<?php echo base_url().'owner/cancelOrder/'.$order['orderID'];?>" name="form-submit" id="<?php echo $order['orderID']; ?>">
                                                      <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="cancelreson" id="cancelreson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtn">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form>
                                                      <form method="post" action="<?php echo base_url().'ajax/approve/'.$order['orderID'].''; ?>" name="form-submit" id="approve-<?php echo $order['orderID']; ?>">
                                                      <div id="modal-approve-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <textarea name="reson" id="reson" class="form-control" ></textarea>  
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                                                                    <button class="btn btn-primary savebtnapprove">Save changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   </div>
                                                    </form> 
                                                    <?php 
                                                     }
                                                     else if($order['status']=='cancel')
                                                     {
                                                      ?>
                                                       <div class="col-md-3 col-sm-3">
                                                        <p><a href="javascript:void(0);" class="btn_cancel_view" rel="modal-<?php echo $order['orderID']; ?>" >Cancel View</a></p>
                                                      </div>
                                                       <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                    <h3 id="myModalLabel">Cancel Reson</h3>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <?php echo  $order['transReson']; ?>
                                                                </div>
                                                             </div>
                                                        </div>
                                                   </div>
                                                      <?php
                                                     }
                                                     else if($order['status']=='approve')
                                                     {
                                                      ?>
                                                         <div class="col-md-3 col-sm-3">
                                                            <p><a href="javascript:void(0);" class="btn_approve_view" rel="modal-<?php echo $order['orderID']; ?>" >Approve View</a></p>
                                                          </div>
                                                         <div id="modal-<?php echo $order['orderID']; ?>" class="modal fade" style="display: none;" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                                                                        <h3 id="myModalLabel">Note</h3>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                       <?php echo  $order['transReson']; ?>
                                                                    </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                   <?php
                                                      }
                                                     ?>
                                                    </p>
                                                    
                                                </div>
                                                <div class="item-footer">
                                                    <p><strong>Order Date:</strong> <?php echo date('d M Y',strtotime($order['tranDate'])); ?><strong>Order Total:</strong> $<?php echo $order['tranAmount']; ?> <?php if($order['payType']=='bank'){ ?><strong><a href="<?php echo base_url(); ?>uploads/payment/<?php echo $order['doc']; ?>" download>Attachment</a></strong><?php } ?> <a href="<?php echo base_url(); ?>owner/orderDetails/<?php echo base64_encode($order['id']); ?>">View details</a></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php 
                                        } 
                                      }else { ?>
                                        <div class="item-entry">
                                          <div class="item-content">
                                                <div class="item-body">
                                                  <div class="alert alert-warning">No Booking Available !</div>
                                                </div>
                                            </div>
                                         </div>
                                      <?php } ?>
                                      </div>  	
                                </div>
                           </div>
			           </div>
					</div>
					<div id="profile" class="tab-pane fade in">
						<div class="col-md-6">
							<div class="user-personal-info">
								<h4>Personal Information</h4>
								<div class="user-info-body">
									 <form method="post" id="userProfile" enctype="multipart/form-data">
										<div class="col-md-12">
											<label>Name</label>
											<input name="hosts_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true"  value="<?php echo $fetch_array[0]['hosts_name']; ?>">
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12">
											<label>Email-ID</label>
											 <input name="hosts_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_email']; ?>" data-rule-email="true" readonly>
										</div>
										<div class="col-md-12">
									   <label>Company Name</label>
									   <input name="hosts_company" type="text" class="form-control" placeholder="Company Name" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_company']; ?>"  >	</div>
                                       <div class="col-md-12">
									   <label>Profile Image</label>
									   <input name="host_profile" type="file" id="host_profile">	
                                        <div class="col-md-6">
                                          <?php if($fetch_array[0]['host_profile']!=''){ ?>
                                          <img src="<?php echo base_url().'uploads/profile/'.$fetch_array[0]['host_profile']; ?>" width="200" height="200">
                                          <?php } ?>
                                        </div>
                                       </div>
									   <div class="col-md-12">
										<label>Types</label>
									      <select multiple="multiple" class="form-control" data-rule-required="true" style="height:auto !important;" name="hosts_mainategory_id[]">
											<?php if(count($categories)>0){ 
                                                    foreach($categories as $cat){
														$expload_cat=explode(',',$fetch_array[0]['hosts_mainategory_id']);
														?>
                                            <option value="<?php echo $cat['category_id'] ?>" <?php if(in_array($cat['category_id'],$expload_cat)){ ?> selected="selected" <?php } ?>><?php echo $cat['category_name_'.$this->session->userdata('lang')] ?></option>
                                            <?php } } ?>
                                        </select>
										</div>
										<div class="col-md-12">
                                         <label>Description</label>
                                         <textarea name="hosts_description" id="hosts_description" class="form-control" data-rule-required="true"><?php echo $fetch_array[0]['hosts_description']; ?></textarea>
										</div>	
										<div class="col-md-12">
											<label>Address</label>
											<textarea name="hosts_address" id="hosts_address" class="form-control" data-rule-required="true"><?php echo $fetch_array[0]['hosts_address']; ?></textarea>
										</div>
                                        <div class="col-md-12">
											<label>Website</label>
										    <input name="hosts_website" type="text" class="form-control" placeholder="Website" value="<?php echo $fetch_array[0]['hosts_website']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Contact Number</label>
										  <input name="hosts_contact" type="text" class="form-control" placeholder="Contact Number" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_contact']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Position</label>
										  <input name="hosts_position" type="text" class="form-control" placeholder="Position" value="<?php echo $fetch_array[0]['hosts_position']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Mobile Number</label>
										  <input name="hosts_mobile" type="text" class="form-control" placeholder="Mobile Number" data-rule-required="true" value="<?php echo $fetch_array[0]['hosts_mobile']; ?>" >
										</div>
                                        <div class="col-md-12">
										  <label>Preferred Payment</label>
										  <select class="form-control" data-rule-required="true" name="hosts_preferred_payment">
                                            <option value="">Select Payment</option>
                                            <option value="Paypal" <?php if($fetch_array[0]['hosts_preferred_payment']=='Paypal'){ echo 'selected="selected"'; } ?> >Paypal</option>
                                            <option value="Bank" <?php if($fetch_array[0]['hosts_preferred_payment']=='Bank'){ echo 'selected="selected"'; } ?>>Bank</option>
                                          </select>
										</div>
                                        <div class="col-md-12">
										  <label>Preferred Currency</label>
										  <select class="form-control" data-rule-required="true" name="hosts_preferred_currency">
                                            <option value="">Select Currency</option>
                                            <option value="USD" <?php if($fetch_array[0]['hosts_preferred_currency']=='USD'){ echo 'selected="selected"'; } ?> >USD</option>
                                            <option value="AED" <?php if($fetch_array[0]['hosts_preferred_currency']=='AED'){ echo 'selected="selected"'; } ?>>AED</option>
                                          </select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <button type="submit"  name="btn_profile" id="btn_profile">SAVE CHANGES</button>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-6 text-center">
										  <a href="<?php echo base_url('owner/profile'); ?>">CANCEL</a>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="user-change-password">
								<h4>Change Password</h4>
								<div class="change-password-body">
									<form method="post" id="passwordForm" enctype="multipart/form-data">
										<div class="col-md-12">
											<label>Old Password</label>
											<input type="password" placeholder="Old Password" class="form-control" name="old-password" id="old-password" data-rule-required="true">
										</div>
										<div class="col-md-12">
											<label>New Password</label>
											<input type="password" placeholder="New Password" class="form-control" name="new-password" id="new-password" data-rule-required="true" data-rule-minlength="6">
										</div>
										<div class="col-md-12">
											<label>Confirm Password</label>
											<input type="password" placeholder="Confirm Password" class="form-control" name="confirm-password" id="confirm-password" data-rule-required="true" data-rule-minlength="6" data-rule-equalto="#new-password">
										</div>
										<div class="col-md-12 text-center">
											 <button type="submit" name="btn_change_pass" id="btn_change_pass" value="1">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#flight-preference" aria-expanded="false" aria-controls="flight-preference">
										<i class="fa fa-plane"></i> Flight Preference <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="flight-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label>Price Range</label>
											<select class="form-control" name="flight-price-range">
												<option>Upto $199</option>
												<option>Upto $250</option>
												<option>Upto $499</option>
												<option>Upto $599</option>
												<option>Upto $1000</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Food Preference</label>
											<select class="form-control" name="flight-food">
												<option>Indian</option>
												<option>Chineese</option>
												<option>Sea Food</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Airline</label>
											<select class="form-control" name="flight-airline">
												<option>Indigo</option>
												<option>Vistara</option>
												<option>Spicejet</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
							<div class="user-preference">
								<h4 data-toggle="collapse" data-target="#hotel-preference" aria-expanded="false" aria-controls="hotel-preference">
										<i class="fa fa-bed"></i> Hotel Preference <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
								</h4>
								<div class="collapse" id="hotel-preference">
									<form >
										<div class="col-md-6 col-sm-6">
											<label>Price Range</label>
											<select class="form-control" name="hotel-price-range">
												<option>Upto $199</option>
												<option>Upto $250</option>
												<option>Upto $499</option>
												<option>Upto $599</option>
												<option>Upto $1000</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Food Preference</label>
											<select class="form-control" name="hotel-food">
												<option>Indian</option>
												<option>Chineese</option>
												<option>Sea Food</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Facilities</label>
											<select class="form-control" name="hotel-facilities">
												<option>WiFi</option>
												<option>Bar</option>
												<option>Restaurant</option>
											</select>
										</div>
										<div class="col-md-6 col-sm-6">
											<label>Rating</label>
											<select class="form-control" name="hotel-facilities">
												<option>5</option>
												<option>4</option>
												<option>3</option>
											</select>
										</div>
										<div class="clearfix"></div>
										<div class="col-md-12 text-center">
											 <button type="submit">SAVE CHANGES</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
                    <!-- Accoumadation form create--->
                    <div id="accommodations" class="tab-pane fade in">
                        <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
                        <div id="accommodations_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addaccom" data-id="accomodations_add">Add Accommodations</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_accommodations_master',array('hosts_id'=>$this->session->userdata('userID'))); 
								
							    if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="accomodations_<?php echo $manage['accom_id']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['accom_title'.$this->session->userdata('form_lang').'']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['accom_category_id'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['accom_subcategory_id'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Rooms  :</strong> <?php echo $manage['accom_bed_rooms'] ?> </p>
                                            <p class="col-sm-5"><strong>Beds :</strong> <?php echo $manage['accom_beds'] ?> </p>
                                            <p class="col-sm-5"><strong>Bathrooms :</strong> <?php echo $manage['accom_bathrooms'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['accom_description'.$this->session->userdata('form_lang').'']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['accom_id']; ?>" data-action="accomedit"data-id="accomodations_add">Edit</a> &nbsp;<a href="javascript:void(0)";  rel="accomodations" data-id="<?php echo $manage['accom_id']; ?>"class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['accom_user_name'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['accom_position'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['accom_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['accom_email']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="accomodations_add" class="commancls" style="display:none;">
                           
                        </div>
                    </div>
                    <!-- Accoumadation form create--->
                    <!-- Tours form create--->
                    <div id="tours" class="tab-pane fade in">
                        <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
                        <div id="tours_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addatours" data-id="tours_add">Add Tours</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_three_forms',array('form_type'=>'Tours','hosts_id'=>$this->session->userdata('userID'))); 
								if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['formsID']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['form_title'.$this->session->userdata('form_lang').'']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['categoryID'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['subcategoryID'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Price  :</strong> <?php echo $manage['form_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Child Price :</strong> <?php echo $manage['form_lessthan_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Group Price :</strong> <?php echo $manage['form_grp_price'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['form_desc'.$this->session->userdata('form_lang').'']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['formsID']; ?>" data-action="toursedit"data-id="tours_add">Edit</a> &nbsp;<a href="javascript:void(0)";  rel="form" data-id="<?php echo $manage['formsID']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['form_name'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['form_position'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['form_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['form_mobile']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="tours_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- Tours form create--->
                    <!-- adventure form create--->
                    <div id="adventure" class="tab-pane fade in">
                        <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
                        <div id="adventure_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addadventure" data-id="adventure_add">Add Adventure</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_three_forms',array('form_type'=>'Adventure','hosts_id'=>$this->session->userdata('userID'))); 
								if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['formsID']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['form_title'.$this->session->userdata('form_lang').'']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['categoryID'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['subcategoryID'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Price  :</strong> <?php echo $manage['form_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Child Price :</strong> <?php echo $manage['form_lessthan_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Group Price :</strong> <?php echo $manage['form_grp_price'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['form_desc'.$this->session->userdata('form_lang').'']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['formsID']; ?>" data-action="adventureedit" data-id="adventure_add">Edit</a> &nbsp;<a href="javascript:void(0)";  rel="form" data-id="<?php echo $manage['formsID']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['form_name'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['form_position'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['form_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['form_mobile']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="adventure_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- adventure form create--->
                    <!-- recreations form create--->
                    <div id="recreations" class="tab-pane fade in">
                      <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
                        <div id="recreations_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addrecreations" data-id="recreations_add">Add Recreations</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageaccoumatation=$this->master_model->getRecords('tbl_three_forms',array('form_type'=>'Recreation','hosts_id'=>$this->session->userdata('userID'))); 
								if(count($manageaccoumatation))
								{ 	
								  foreach($manageaccoumatation as $manage)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $manage['formsID']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $manage['form_title'.$this->session->userdata('form_lang').'']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['categoryID'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$manage['subcategoryID'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Price  :</strong> <?php echo $manage['form_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Child Price :</strong> <?php echo $manage['form_lessthan_price'] ?> </p>
                                            <p class="col-sm-5"><strong>Group Price :</strong> <?php echo $manage['form_grp_price'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $manage['form_desc'.$this->session->userdata('form_lang').'']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $manage['formsID']; ?>" data-action="recreationsedit" data-id="recreations_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="form" data-id="<?php echo $manage['formsID']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'User Info'; ?></h5>
                                        <p class="col-md-5"><strong>Name  :</strong> <?php echo $manage['form_name'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Position  :</strong> <?php echo $manage['form_position'.$this->session->userdata('form_lang').'']; ?> </p>
                                        <p class="col-md-5"><strong>Email  :</strong> <?php echo $manage['form_email']; ?> </p>
                                        <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $manage['form_mobile']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="recreations_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- recreations form create--->
                    <!-- Transportation form create--->
                    <div id="transportation" class="tab-pane fade in">
                       <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
						<div id="transportation_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addtransportation" data-id="transportation_add">Add Transportation</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $managetranse=$this->master_model->getRecords('tbl_transportation_master',array('hosts_id'=>$this->session->userdata('userID'))); 
								if(count($managetranse))
								{ 	
								  foreach($managetranse as $trans)
								  {
					             ?>
                               <div class="item-entry" id="remove_<?php echo $trans['transportation_id']; ?>">
								<!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
								<div class="item-content">
									<div class="item-body">
										 <div class="col-md-9 col-sm-10">
											<h4><?php echo $trans['transportation_title']; ?></h4>
                                            <?php $categoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$trans['transportation_category_id'])); ?>
                                            <p class="col-md-5"><strong>Category :</strong><?php echo $categoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <?php $subcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$trans['transportation_subcategory_id'])); ?>
                                            <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $subcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                            <p class="col-md-5"><strong>Car Size  :</strong> <?php echo $trans['transportation_car_size'] ?> </p>
                                            <p class="col-md-5"><strong>Car Model  :</strong> <?php echo $trans['transportation_car_model'] ?> </p>
                                            <p class="col-md-5"><strong>Car Brand  :</strong> <?php echo $trans['transportation_car_brand'] ?> </p>
                                            <p class="col-sm-5"><strong>Car :</strong> <?php echo $trans['transportation_car'] ?> </p>
                                            <p class="col-sm-5"><strong>max passanger :</strong> <?php echo $trans['transportation_max_passanger'] ?> </p>
										    <p class="col-sm-5"><strong>Description  :</strong> <?php echo $trans['transportation_description']; ?> </p>
										</div>
										<div class="col-md-3 col-sm-3">
											<p><a href="javascript:void(0)" class="editclass" rel="<?php echo $trans['transportation_id']; ?>" data-action="transportationedit" data-id="transportation_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="transportation" data-id="<?php echo $trans['transportation_id']; ?>" class="removeaccom">Remove</a></p>
                                        </div>
									</div>
									<div class="item-footer"> 
                                        <h5><?php echo 'Available time'; ?></h5>
                                        <p class="col-md-5"><strong>Available Time  :</strong> <?php echo $trans['transportation_available_time']; ?> </p>
                                        <p class="col-md-5"><strong>Pickuptime  :</strong> <?php echo $trans['transportation_pickuptime']; ?> </p>
                                        <p class="col-md-5"><strong>Pickup Location  :</strong> <?php echo $trans['transportation_pickup_location']; ?> </p>
                                        <p class="col-md-5"><strong>Remarks  :</strong> <?php echo $trans['transportation_pickup_remarks']; ?> </p>
                                        <p class="col-md-5"><strong>Drop Location  :</strong> <?php echo $trans['transportation_drop_location']; ?> </p>
                                        <p class="col-md-5"><strong>Drop Remark  :</strong> <?php echo $trans['transportation_drop_remark']; ?> </p>
                                    </div>
									<div class="clearfix"></div>
								</div>
							</div>
                             <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="transportation_add" class="commancls" style="display:none;">
                        </div>
					</div>
                    <!-- Transportation form create--->
                    <!-- Coupon form create--->
                    <div id="coupon" class="tab-pane fade in">
                        <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
                        <div id="coupon_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addcoupon" data-id="coupon_add">Add Coupon</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $managecoupon=$this->master_model->getRecords('tbl_coupon_master',array('hosts_id'=>$this->session->userdata('userID'))); 
								
							    if(count($managecoupon))
								{ 	
								  foreach($managecoupon as $coupon)
								  {
					             ?>
                                 <div class="item-entry" id="remove_coupon_<?php echo $coupon['coupon_id']; ?>">
                                    <!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
                                    <div class="item-content">
                                        <div class="item-body">
                                             <div class="col-md-9 col-sm-10">
                                                <h4><?php echo $coupon['coupon_title']; ?></h4>
                                                <?php 
												$couponcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$coupon['category_id'])); ?>
                                                <p class="col-md-5"><strong>Category :</strong><?php if(isset($couponcategoryName[0]['category_name_'.$this->session->userdata('lang')])){echo $couponcategoryName[0]['category_name_'.$this->session->userdata('lang')];}?> </p>
                                                <?php $couponsubcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$coupon['subcategory_id'])); ?>
                                                <p class="col-md-5"><strong>Subcategory :</strong> <?php if(isset($couponsubcategoryName[0]['category_name_'.$this->session->userdata('lang')])){ echo $couponsubcategoryName[0]['category_name_'.$this->session->userdata('lang')];} ?> </p>
                                                <p class="col-sm-5"><strong>Price :</strong> <?php echo $coupon['price'] ?> </p>
                                                <p class="col-sm-5"><strong>Address :</strong> <?php echo $coupon['coupon_address'] ?> </p>
                                                <p class="col-sm-5"><strong>Description  :</strong> <?php echo $coupon['coupon_description']; ?> </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <p><a href="javascript:void(0)" class="editclass" rel="<?php echo $coupon['coupon_id']; ?>" data-action="couponedit" data-id="coupon_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="coupon" data-id="<?php echo $coupon['coupon_id']; ?>" class="removeaccom">Remove</a></p>
                                            </div>
                                        </div>
                                        <div class="item-footer"> 
                                            <h5><?php echo 'User Info'; ?></h5>
                                            <p class="col-md-5"><strong>Name  :</strong> <?php echo $coupon['coupon_user_name']; ?> </p>
                                            <p class="col-md-5"><strong>Position  :</strong> <?php echo $coupon['coupon_person_postion']; ?> </p>
                                            <p class="col-md-5"><strong>Email  :</strong> <?php echo $coupon['coupon_email']; ?> </p>
                                            <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $coupon['coupon_mobile']; ?> </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                  <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="coupon_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- Coupon form create--->
                    <!-- Event form create--->
                    <div id="events" class="tab-pane fade in">
                       <div class="user-waitt" style="display:none;"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
						<div id="events_manage" class="commancls">
                        	<div class="col-md-3 col-sm-3 col-xs-6 footer-about-box">
							  <p><a href="javascript:void(0);" class="addcls" rel="" data-action="addevents" data-id="events_add">Add Events</a></p>
						    </div>
							<div class="clearfix"></div>
							<div class="col-md-12">
                            <?php 
							    $manageevent=$this->master_model->getRecords('tbl_event_master',array('hosts_id'=>$this->session->userdata('userID'))); 
								
							    if(count($manageevent))
								{ 	
								  foreach($manageevent as $event)
								  {
					             ?>
                                 <div class="item-entry" id="remove_coupon_<?php echo $event['event_id']; ?>">
                                    <!--<span><i class="fa fa-hotel"></i> Hotel</span>-->
                                    <div class="item-content">
                                        <div class="item-body">
                                             <div class="col-md-9 col-sm-10">
                                                <h4><?php echo $event['event_title']; ?></h4>
                                                <?php $couponcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$event['event_category_id'])); ?>
                                                <p class="col-md-5"><strong>Category :</strong><?php echo $couponcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                                <?php $couponsubcategoryName=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$event['event_subcategory_id'])); ?>
                                                <p class="col-md-5"><strong>Subcategory :</strong> <?php echo $couponsubcategoryName[0]['category_name_'.$this->session->userdata('lang')] ?> </p>
                                                <p class="col-sm-5"><strong>Event Duration :</strong> <?php echo $event['event_duration'] ?> </p>
                                                <p class="col-sm-5"><strong>Address :</strong> <?php echo $event['event_address'] ?> </p>
                                                <p class="col-sm-5"><strong>Description  :</strong> <?php echo $event['event_description']; ?> </p>
                                            </div>
                                            <div class="col-md-3 col-sm-3">
                                                <p><a href="javascript:void(0)" class="editclass" rel="<?php echo $event['event_id']; ?>" data-action="eventedit" data-id="events_add">Edit</a> &nbsp;<a href="javascript:void(0)"; rel="event" data-id="<?php echo $event['event_id']; ?>" class="removeaccom">Remove</a></p>
                                            </div>
                                        </div>
                                        <div class="item-footer"> 
                                            <h5><?php echo 'User Info'; ?></h5>
                                            <p class="col-md-5"><strong>Name  :</strong> <?php echo $event['event_user_name']; ?> </p>
                                            <p class="col-md-5"><strong>Position  :</strong> <?php echo $event['event_position']; ?> </p>
                                            <p class="col-md-5"><strong>Email  :</strong> <?php echo $event['event_email']; ?> </p>
                                            <p class="col-md-5"><strong>Mobile  :</strong> <?php echo $event['event_mobile']; ?> </p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                  <?php 
								  }
							   }else{ ?>
                               <div class="item-entry">
                                    <div class="item-content">
                                        <div class="item-body">
                                           <div class="danger">No records found !</div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                               <?php } ?>
                            </div>			
                        </div>
					    <div id="events_add" class="commancls" style="display:none;">
                        </div>
                    </div>
                    <!-- Event form create--->
                    <!-- restaurants form create--->
                    <div id="restaurants" class="tab-pane fade in">
					   Not all fields in restaurants
                    </div>
                    <!-- restaurants form create--->
				</div>
			</div>
		</div>
	</div>
<!-- END: USER PROFILE -->
<!-- END: USER PROFILE -->
<script type="text/javascript">
	jQuery(document).ready(function(e) {
		 jQuery('#userstory').validate({});
         jQuery('#passwordForm').validate({});
		 jQuery('#userProfile').validate({});
	});
</script>