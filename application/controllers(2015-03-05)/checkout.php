<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Checkout extends CI_Controller {
	
	
	public function cart()
	{
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['error']='';
		$data['pagetitle']='Checkout';
		if(isset($_POST['btn_pay']))
		{
			$user_data=array('shipping_firstname'=>$_POST['firstname'],
			 'shipping_lastname'=>$_POST['lastname'],
			 'shipping_contactNumber'=>$_POST['contactNumber'],
			 'note'=>$_POST['note']
			  );
			$this->session->set_userdata($user_data);
			redirect(base_url().'checkout/secure_payment');
		}
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$this->db->join('tbl_boattype_master','tbl_boattype_master.boattype_id=tbl_boat_master.boat_type_id');
		$data['boatDetails']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boatpackage_master.package_id'=>$this->session->userdata('package_id')));
		$facilities_amt=0;
		if($this->session->userdata('facilities')!='')
		{
			$facilities=explode(',',$this->session->userdata('facilities')); 
			if(count($facilities))
			{
				for($i=0;$i<count($facilities);$i++)
				{
					$getfac=$this->master_model->getRecords('tbl_boat_facilities',array('btfacID'=>$facilities[$i]));
					if(count($getfac))
					{
						$facilities_amt +=$getfac[0]['facilities_amt'];
					}
				}
			}
		}
		$data['facilities_amt']=$facilities_amt;
		$data['userDetails']=$this->master_model->getRecords('tbl_user_master',array('userID'=>$this->session->userdata('userID')));
		$data['middle_content']='cart';
	  	$this->load->view('common-file',$data);
	}
	
	public function bank()
	{
		if(isset($_POST['btn_pay']))
		{
			$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
			$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
			$details=$this->master_model->getRecords('tbl_boat_master',array('tbl_boatpackage_master.package_id'=>$this->session->userdata('package_id'),'tbl_boat_master.boat_status'=>'active'));
			//$details=$this->master_model->getRecords('tbl_boat_master',array('boat_id'=>$this->session->userdata('boat_id')));
			$tran_date=date('Y-m-d h:i:s');
			$this->load->library('upload'); 
			$config=array('upload_path'=>'uploads/documents/',
					  'allowed_types'=>'jpg|jpeg|gif|png|pdf',
					  'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			
			if($_FILES['doc']['name']!='')
			{
				if($this->upload->do_upload('doc'))
				{
					$facilities_amt=0;
					if($this->session->userdata('facilities')!='')
					{
						$facilities=explode(',',$this->session->userdata('facilities')); 
						if(count($facilities))
						{
							for($i=0;$i<count($facilities);$i++)
							{
								$getfac=$this->master_model->getRecords('tbl_boat_facilities',array('btfacID'=>$facilities[$i]));
								if(count($getfac))
								{
									$facilities_amt +=$getfac[0]['facilities_amt'];
								}
							}
						}
					}
				  $dt=$this->upload->data();
				  $file=$dt['file_name'];
				  $insert_array=array(
					'tranAmount'=>($details[0]['adult_price'] * $this->session->userdata('qty')) + ($this->session->userdata('child_qty') * $details[0]['child_price']) + ($facilities_amt),
					'trpackage_id'=>$this->session->userdata('package_id'),
					'truserID'=>$this->session->userdata('userID'),
					'shipping_firstname'=>$this->input->post('firstname',true),
					'shipping_lastname'=>$this->input->post('lastname',true),
					'shipping_contactNumber'=>$this->input->post('contactNumber',true),
					'note'=>$this->input->post('note'),
					'qty'=>$this->session->userdata('qty'),
					'child_qty'=>$this->session->userdata('child_qty'),
					'facilities'=>$this->session->userdata('facilities'),
					'doc'=>$file,
					'tranDate'=>$tran_date
				);
					if($this->master_model->insertRecord('tbl_banking_temp',$insert_array))
					{
						$id=$this->db->insert_id();
						$this->db->where('package_id',$this->session->userdata('package_id'));
						$this->db->set('adult_qty','adult_qty-'.$this->session->userdata('qty'),FALSE);
						$this->db->set('child_price','child_price-'.$this->session->userdata('child_qty'),FALSE);
						$this->db->update('tbl_boatpackage_master');
						redirect(base_url().'checkout/thankyoubank/'.base64_encode($id));
					}
					else
					{
						$this->session->set_flashdata('error','You are registered successfully.');
						redirect(base_url().'checkout/cart');
					}
				}
				else
				{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(base_url().'checkout/cart');
				}
			}
			else
			{
				redirect(base_url());
			}
			
		}
		else
		{
			redirect(base_url());
		}
	}
	
	public function secure_payment()
	{
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$data['boatDetails']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boatpackage_master.package_id'=>$this->session->userdata('package_id'),'boat_status'=>'active'));
		$facilities_amt=0;
		if($this->session->userdata('facilities')!='')
		{
			$facilities=explode(',',$this->session->userdata('facilities')); 
			if(count($facilities))
			{
				for($i=0;$i<count($facilities);$i++)
				{
					$getfac=$this->master_model->getRecords('tbl_boat_facilities',array('btfacID'=>$facilities[$i]));
					if(count($getfac))
					{
						$facilities_amt +=$getfac[0]['facilities_amt'];
					}
				}
			}
		}
		$data['facilities_amt']=$facilities_amt;
		$this->load->view('secrate-paypal',$data);
	}
	
	public function return_payment()
	{
		
		if($this->session->userdata('package_id')==TRUE)
		{
			$tx=$_GET['tx'];
			$st=$_GET['st'];
			$amt=$_GET['amt'];
			$tran_date=date('Y-m-d h:i:s');
			
			$insert_array=array(
				'transactionID'=>$tx,
				'tranAmount'=>$amt,
				'tranStatus'=>$st,
				'trpackage_id'=>$this->session->userdata('package_id'),
				'truserID'=>$this->session->userdata('userID'),
				'shipping_firstname'=>$this->session->userdata('shipping_firstname'),
				'shipping_lastname'=>$this->session->userdata('shipping_lastname'),
				'shipping_contactNumber'=>$this->session->userdata('shipping_contactNumber'),
				'note'=>$this->session->userdata('note'),
				'qty'=>$this->session->userdata('qty'),
				'facilities'=>$this->session->userdata('facilities'),
				'child_qty'=>$this->session->userdata('child_qty'),
				'tranDate'=>$tran_date
			);
			$complete=$this->db->insert('tbl_transaction_master',$insert_array,true);
			$id=$this->db->insert_id();
			$this->db->where('package_id',$this->session->userdata('package_id'));
			$this->db->set('adult_qty','adult_qty-'.$this->session->userdata('qty'),FALSE);
			$this->db->set('child_price','child_price-'.$this->session->userdata('child_qty'),FALSE);
			$this->db->update('tbl_boatpackage_master');
			$array_items = array('package_id' => '','shipping_firstname'=>'','shipping_lastname'=>'','shipping_contactNumber'=>'','note'=>'','child_qty'=>'','adult_qty'=>'','redirectTo'=>'','facilities'=>'');
			$this->session->unset_userdata($array_items);
			redirect(base_url().'checkout/thanks/'.base64_encode($id));
		}
		else
		{
			redirect(show_404());
		}
	}
	
	public function thanks($txnID)
	{
		$txnID=base64_decode($txnID);
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['error']='';
		$data['pagetitle']='Thank you';
		$data['details']=$this->master_model->getRecords('tbl_transaction_master',array('txnID'=>$txnID));
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$data['boatDetails']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boatpackage_master.package_id'=>$data['details'][0]['trpackage_id'],'boat_status'=>'active'));
		$data['middle_content']='thankyou';
	  	$this->load->view('common-file',$data);
	}
	
	public function thankyoubank($txnID)
	{
		$txnID=base64_decode($txnID);
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['error']='';
		$data['pagetitle']='Thank you';
		$data['details']=$this->master_model->getRecords('tbl_banking_temp',array('btID'=>$txnID));
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$data['boatDetails']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boatpackage_master.package_id'=>$data['details'][0]['trpackage_id'],'boat_status'=>'active'));
		$data['middle_content']='thankyoubank';
	  	$this->load->view('common-file',$data);
	}
	
	public function cancel_payment()
	{
		$array_items = array('package_id' => '','shipping_firstname'=>'','shipping_lastname'=>'','shipping_contactNumber'=>'','note'=>'','child_qty'=>'','adult_qty'=>'','redirectTo'=>'','facilities'=>'');
		$this->session->unset_userdata($array_items);
		$this->load->view('cancel-payment');
	}
	
}