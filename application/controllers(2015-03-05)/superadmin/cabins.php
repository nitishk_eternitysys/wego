<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cabins extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Cabins';
	  $data['pageLable']='Cabins';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_cabins_master','cabins_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_cabins_master',array('cabins_status'=>$stat),array('cabins_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_cabins_master',array('cabins_status'=>$stat),array('cabins_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_cabins_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$cabins_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('cabins_status'=>$status);
		if($this->master_model->updateRecord('tbl_cabins_master',$input_array,array('cabins_id'=>$cabins_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($cabins_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_cabins_master','cabins_id',$cabins_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Cabins';
		  $data['pageLable']='Cabins';
		  if(isset($_POST['btn_add']))
		  {
			  $this->form_validation->set_rules('cabins_name_eng','Cabins english','required|xss_clean');
			  $this->form_validation->set_rules('cabins_name_arb','Cabins Arbic Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $testimonial_image='';
				  $cabins_name_eng=$this->input->post('cabins_name_eng',true);
				  $cabins_name_arb=$this->input->post('cabins_name_arb',true);
				  $cabins_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_cabins_master',array('cabins_name_eng'=>$cabins_name_eng,'cabins_name_arb'=>$cabins_name_arb));
				  if($checkDub==0)
				  {
					  $cabins_slug=$this->master_model->create_slug($cabins_name_eng,'tbl_cabins_master','cabins_name_eng');
					  $input_array=array('cabins_name_eng'=>addslashes($cabins_name_eng),'cabins_name_arb'=>$cabins_name_arb,'cabins_status'=>$cabins_status,'cabins_slug'=>$cabins_slug);
					  if($this->master_model->insertRecord('tbl_cabins_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					  $data['error']='Cabins already exist !';  
				  }
				  
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($cabins_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Cabins';
		  $data['pageLable']='Cabins';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_cabins_master',array('cabins_id'=>$cabins_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('cabins_name_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('cabins_name_arb','Testimonial Description Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $cabins_name_eng=$this->input->post('cabins_name_eng',true);
				  $cabins_name_arb=$this->input->post('cabins_name_arb',true);
				  $cabins_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_cabins_master',array('cabins_name_eng'=>$cabins_name_eng,'cabins_name_arb'=>$cabins_name_arb,'cabins_id !='=>$cabins_id));
				  if($checkDub==0)
				  {
					  $cabins_slug=$this->master_model->create_slug($cabins_name_eng,'tbl_cabins_master','cabins_name_eng');
					  $input_array=array('cabins_name_eng'=>addslashes($cabins_name_eng),'cabins_name_arb'=>$cabins_name_arb,'cabins_status'=>$cabins_status,'cabins_slug'=>$cabins_status);
					  if($this->master_model->updateRecord('tbl_cabins_master',$input_array,array('cabins_id'=>$cabins_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$cabins_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$cabins_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$cabins_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
}