<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subcategory extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	}
	public function manage()
	{
	   $data['pagetitle']='Rehla ticket | subCategories';
	   if(isset($_POST['multiple_delete']))
	   {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_category_master','category_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	    if(isset($_POST['blockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='0';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_category_master',array('category_status'=>$stat),array('category_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
		if(isset($_POST['unblockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='1';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_category_master',array('category_status'=>$stat),array('category_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $this->db->where('parent_id !=','0');
	  $data['categories']=$this->master_model->getRecords('tbl_category_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class().'';
	  $this->load->view('admin/common-file',$data);
	}
	public function status($categorystatus,$category_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('category_status'=>$categorystatus);
		if($this->master_model->updateRecord('tbl_category_master',$input_array,array('category_id'=>$category_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($category_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_category_master','category_id',$category_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | sub-Categories';
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('maincategory_id','Main Category select','required|xss_clean');
			  $this->form_validation->set_rules('category_id','Category select','required|xss_clean');
			  $this->form_validation->set_rules('category_name_eng','Sub Category Name English','required|xss_clean');
			  $this->form_validation->set_rules('category_name_arb','Sub Category Name Arbic','required|xss_clean');
			  $this->form_validation->set_rules('category_status','Status','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $maincategory_id=$this->input->post('maincategory_id');
				  $parent_id=$this->input->post('category_id');
				  $category_name_eng=$this->input->post('category_name_eng',true);
				  $category_name_arb=$this->input->post('category_name_arb',true);
				  $category_status=$this->input->post('category_status',true);
				  
				  $checkdublication=$this->master_model->getRecords('tbl_category_master',array('category_name_eng'=>addslashes($category_name_eng),'category_name_arb'=>addslashes($category_name_arb),'parent_id'=>$parent_id));
				  if(count($checkdublication)==0)
				  {
					  $category_slug=$this->master_model->create_slug($category_name_eng,'tbl_category_master','category_slug');
				      $input_array=array('category_name_eng'=>addslashes($category_name_eng),'category_name_arb'=>addslashes($category_name_arb),'category_status'=>$category_status,'category_slug'=>$category_slug,'parent_id'=>$parent_id,'maincategory_id'=>$maincategory_id);

					  if($this->master_model->insertRecord('tbl_category_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					 $this->session->set_flashdata('error','subcategory already exist of this category !'); 
				     redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');  
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | sub-Categories';
		  $category_id=$this->uri->segment('4');
		  $data['categories']=$this->master_model->getRecords('tbl_category_master',array('category_id'=>$category_id));
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('maincategory_id','Main Category select','required|xss_clean');
			  $this->form_validation->set_rules('category_id','Category select','required|xss_clean');
			  $this->form_validation->set_rules('category_name_eng','Category Name English','required|xss_clean');
			  $this->form_validation->set_rules('category_name_arb','Category Name Arbic','required|xss_clean');
			  $this->form_validation->set_rules('category_status','Status','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $maincategory_id=$this->input->post('maincategory_id');
				  $parent_id=$this->input->post('category_id');
				  $category_name_eng=$this->input->post('category_name_eng',true);
				  $category_name_arb=$this->input->post('category_name_arb',true);
				  $category_status=$this->input->post('category_status',true);
				  
				  $this->db->where('category_id !=',$category_id);
				  $checkdublication=$this->master_model->getRecords('tbl_category_master',array('category_name_eng'=>addslashes($category_name_eng),'category_name_arb'=>addslashes($category_name_arb),'parent_id'=>$parent_id));
				  if(count($checkdublication)==0)
				  {
					  $category_slug=$this->master_model->create_slug($category_name_eng,'tbl_category_master','category_slug','category_id',$category_id);
					  $input_array=array('category_name_eng'=>addslashes($category_name_eng),'category_name_arb'=>addslashes($category_name_arb),'category_status'=>$category_status,'category_slug'=>$category_slug,'parent_id'=>$parent_id,'maincategory_id'=>$maincategory_id);
					   if($this->master_model->updateRecord('tbl_category_master',$input_array,array('category_id'=>$category_id,)))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$category_id);
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$category_id);
					  }
				  }
				  else
				  {
					 $this->session->set_flashdata('error','subcategory already exist of this category !'); 
				     redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$category_id);  
				  }
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
}