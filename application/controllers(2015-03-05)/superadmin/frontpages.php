<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Frontpages extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	}
	
	public function managefrontpage()
	{
		$data['success']=$data['error']='';
	  	$data['pagetitle']='Rehla Ticket | Manage Frontpage';
		if(isset($_POST['multiple_delete']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_front_page','front_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/frontpages/managefrontpage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/frontpages/managefrontpage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/frontpages/managefrontpage');
			}
		}
	  if(isset($_POST['blockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='0';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_front_page',array('front_status'=>$stat),array('front_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/frontpages/managefrontpage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/frontpages/managefrontpage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/frontpages/managefrontpage');
			}
			
		}
		if(isset($_POST['unblockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='1';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_front_page',array('front_status'=>$stat),array('front_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/frontpages/managefrontpage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/frontpages/managefrontpage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/frontpages/managefrontpage');
			}
		}
		$data['fetch_manage_frontpage']=$this->master_model->getRecords('tbl_front_page');
		$data['middle_content']='manage-frontpage';
		$this->load->view('admin/common-file',$data);
	}
	
	public function addfrontpage()
	{
		$data['success']=$data['error']='';
	  	$data['pagetitle']='Rehla Ticket | Add Frontpage';
			if(isset($_POST['brm_frontpage']))
			{
				  $this->form_validation->set_rules('page_name_eng','Page Name English','required|xss_clean|is_unique[tbl_front_page.front_page_name_eng]');
				  $this->form_validation->set_rules('page_title_eng','Page Title English','required|xss_clean|is_unique[tbl_front_page.front_page_title_eng]');
				  $this->form_validation->set_rules('meta_title_eng','Meta Title English','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_eng','Meta Keywords English','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_eng','Meta Description English','required|xss_clean');
				  $this->form_validation->set_rules('page_description_eng','Page Description English','required');
				  
				  $this->form_validation->set_rules('page_name_arb','Page Name Arabic','required|xss_clean|is_unique[tbl_front_page.front_page_name_arb]');
				  $this->form_validation->set_rules('page_title_arb','Page Title Arabic','required|xss_clean|is_unique[tbl_front_page.front_page_title_arb]');
				  $this->form_validation->set_rules('meta_title_arb','Meta Title Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_arb','Meta Keywords Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_arb','Meta Description Arabic','required|xss_clean');
				  $this->form_validation->set_rules('page_description_arb','Page Description Arabic','required');
				  
				  	if($this->form_validation->run())
			  		{
						  $page_name_eng=$this->input->post('page_name_eng',true);
						  $page_title_eng=$this->input->post('page_title_eng',true);
						  $meta_title_eng=$this->input->post('meta_title_eng',true);
						  $meta_keyword_eng=$this->input->post('meta_keyword_eng',true);
						  $meta_desc_eng=$this->input->post('meta_desc_eng',true);
						  $page_description_eng=$this->input->post('page_description_eng');
						  
						  $page_name_arb=$this->input->post('page_name_arb',true);
						  $page_title_arb=$this->input->post('page_title_arb',true);
						  $meta_title_arb=$this->input->post('meta_title_arb',true);
						  $meta_keyword_arb=$this->input->post('meta_keyword_arb',true);
						  $meta_desc_arb=$this->input->post('meta_desc_arb',true);
						  $page_description_arb=$this->input->post('page_description_arb');
						  
						  $slug=$this->master_model->create_slug($page_name_eng,'tbl_front_page','page_slug');
						  
							$input_array=array('front_page_name_eng'=>$page_name_eng,'front_page_title_eng'=>$page_title_eng,'front_page_description_eng'=>$page_description_eng,'meta_title_eng'=>$meta_title_eng,'meta_keywords_eng'=>$meta_keyword_eng,'meta_desc_eng'=>$meta_desc_eng,'front_page_name_arb'=>$page_name_arb,'front_page_title_arb'=>$page_title_arb,'front_page_description_arb'=>$page_description_arb,'meta_title_arb'=>$meta_title_arb,'meta_keywords_arb'=>$meta_keyword_arb,'meta_desc_arb'=>$meta_desc_arb,'page_slug'=>$slug);
							
							if($this->master_model->insertRecord('tbl_front_page',$input_array))
							{ 
								$this->session->set_flashdata('success','Pages Added Successfully.');
								redirect(base_url().'superadmin/frontpages/addfrontpage');
							}
							else
							{
								$data['error']='Error while adding Page.';
							}
					}
					else
					{
						$data['error']=$this->form_validation->error_string();
					}
			}
		$data['middle_content']='add-frontpage';
		$this->load->view('admin/common-file',$data);
	}
	
	public function frontstatus($id,$status)
	{
		$input_array = array('front_status'=>$status);
		if($this->master_model->updateRecord('tbl_front_page',$input_array,array('front_id'=>$id)))
		{
	       $this->session->set_flashdata('success','Record(s) status updated successfully.');
		   redirect(base_url().'superadmin/frontpages/managefrontpage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/frontpages/managefrontpage');
		}
	}
	
	public function deletefront($front_id)
	{
		if($this->master_model->deleteRecord('tbl_front_page','front_id',$front_id)) 
		{
			$this->session->set_flashdata('success','Record deleted successfully.');
		   	redirect(base_url().'superadmin/frontpages/managefrontpage/');
		}
		else
		{
			$this->session->set_flashdata('error','Error while deleting record.'); 
		   	redirect(base_url().'superadmin/frontpages/managefrontpage');
		}
	}
	
	
	
	public function frontupdate()
	{
		$data['success']=$data['error']='';
	  	$data['pagetitle']='Rehla Ticket | Update Frontpage';
		
		 $front_id=$this->uri->segment('4');
		 $data['frontcontent']=$this->master_model->getRecords('tbl_front_page',array('front_id'=>$front_id));
		
			if(isset($_POST['brm_frontpage']))
			{
				  $this->form_validation->set_rules('page_name_eng','Page Name English','required|xss_clean');
				  $this->form_validation->set_rules('page_title_eng','Page Title English','required|xss_clean');
				  $this->form_validation->set_rules('meta_title_eng','Meta Title English','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_eng','Meta Keywords English','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_eng','Meta Description English','required|xss_clean');
				  $this->form_validation->set_rules('page_description_eng','Page Description English','required');
				  
				  $this->form_validation->set_rules('page_name_arb','Page Name Arabic','required|xss_clean');
				  $this->form_validation->set_rules('page_title_arb','Page Title Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_title_arb','Meta Title Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_arb','Meta Keywords Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_arb','Meta Description Arabic','required|xss_clean');
				  $this->form_validation->set_rules('page_description_arb','Page Description Arabic','required');
				  
				  	if($this->form_validation->run())
			  		{
						  $page_name_eng=$this->input->post('page_name_eng',true);
						  $page_title_eng=$this->input->post('page_title_eng',true);
						  $meta_title_eng=$this->input->post('meta_title_eng',true);
						  $meta_keyword_eng=$this->input->post('meta_keyword_eng',true);
						  $meta_desc_eng=$this->input->post('meta_desc_eng',true);
						  $page_description_eng=$this->input->post('page_description_eng');
						  
						  $page_name_arb=$this->input->post('page_name_arb',true);
						  $page_title_arb=$this->input->post('page_title_arb',true);
						  $meta_title_arb=$this->input->post('meta_title_arb',true);
						  $meta_keyword_arb=$this->input->post('meta_keyword_arb',true);
						  $meta_desc_arb=$this->input->post('meta_desc_arb',true);
						  $page_description_arb=$this->input->post('page_description_arb');
						  
						  $slug=$this->master_model->create_slug($page_name_eng,'tbl_front_page','page_slug','front_id',$front_id);
						  
							$input_array=array('front_page_name_eng'=>$page_name_eng,'front_page_title_eng'=>$page_title_eng,'front_page_description_eng'=>$page_description_eng,'meta_title_eng'=>$meta_title_eng,'meta_keywords_eng'=>$meta_keyword_eng,'meta_desc_eng'=>$meta_desc_eng,'front_page_name_arb'=>$page_name_arb,'front_page_title_arb'=>$page_title_arb,'front_page_description_arb'=>$page_description_arb,'meta_title_arb'=>$meta_title_arb,'meta_keywords_arb'=>$meta_keyword_arb,'meta_desc_arb'=>$meta_desc_arb,'page_slug'=>$slug);
							
							if($this->master_model->updateRecord('tbl_front_page',$input_array,array('front_id'=>$front_id)))
							{ 
								$this->session->set_flashdata('success','Pages Added Successfully.');
								redirect(base_url().'superadmin/frontpages/frontupdate/'.$front_id);
							}
							else
							{
								$data['error']='Error while adding Page.';
							}
					}
					else
					{
						$data['error']=$this->form_validation->error_string();
					}
			}
			
		$data['middle_content']='edit-frontpage';
		$this->load->view('admin/common-file',$data);
	}
	
	
	
	public function managecontactinquiry()
	{
		$data['success']=$data['error']='';
		$data['pagetitle']='Rehla Ticket | Contact Inquiry';
		$data['fetch_manage_contactinquiry']=$this->master_model->getRecords('tbl_contact_inqury');
		$data['middle_content']='manage-contactinquiry';
		$this->load->view('admin/common-file',$data);
	}
	
	public function deletecontactinquiry($contact_id)
	{
		if($this->master_model->deleteRecord('tbl_contact_inqury','con_id',$contact_id)) 
		{
			$this->session->set_flashdata('success','Record deleted successfully.');
		   	redirect(base_url().'superadmin/frontpages/managecontactinquiry/');
		}
		else
		{
			$this->session->set_flashdata('error','Error while deleting record.'); 
		   	redirect(base_url().'superadmin/frontpages/managecontactinquiry');
		}
	}
	
	public function deletemultiplecontactinq()
	{
		if(isset($_REQUEST['checkbox_del'])!="")
		{
		   $checkbox_del_count = count($_POST['checkbox_del']);
		   for($i=0;$i<$checkbox_del_count;$i++)
		   {
			 $this->master_model->deleteRecord('tbl_contact_inqury','con_id',$_REQUEST['checkbox_del'][$i]);
		   }
		   $this->session->set_flashdata('success',"Records deleted successfully.");	
		   redirect(base_url().'superadmin/frontpages/managecontactinquiry');
		}
	}
public function manageseo()
	{
		$data['success']=$data['error']='';
	  	$data['pagetitle']='Rehla Ticket | Manage SEO';
		$data['fetch_seo']=$this->master_model->getRecords('tbl_seo_master');
		$data['middle_content']='manage-seo';
		$this->load->view('admin/common-file',$data);
	}
	
	public function addseo()
	{
		$data['success']=$data['error']='';
	  	$data['pagetitle']='Rehla Ticket | Add SEO';
			if(isset($_POST['brm_seo']))
			{
				  $this->form_validation->set_rules('page_link','Page Link ','required');
				  $this->form_validation->set_rules('page_title','Page Title','required|xss_clean');
				  $this->form_validation->set_rules('meta_title_eng','Meta Title English','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_eng','Meta Keywords English','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_eng','Meta Description English','required|xss_clean');
				  $this->form_validation->set_rules('meta_title_arb','Meta Title Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_arb','Meta Keywords Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_arb','Meta Description Arabic','required|xss_clean');
				  
				  	if($this->form_validation->run())
			  		{
						  $page_link=$this->input->post('page_link',true);
						  $page_title=$this->input->post('page_title',true);
						  $meta_title_eng=$this->input->post('meta_title_eng',true);
						  $meta_keyword_eng=$this->input->post('meta_keyword_eng',true);
						  $meta_desc_eng=$this->input->post('meta_desc_eng',true);
						  
						  $meta_title_arb=$this->input->post('meta_title_arb',true);
						  $meta_keyword_arb=$this->input->post('meta_keyword_arb',true);
						  $meta_desc_arb=$this->input->post('meta_desc_arb',true);
						  
						  
							$input_array=array('page_link'=>$page_link,'page_title'=>$page_title,'meta_title_eng'=>$meta_title_eng,'meta_keywords_eng'=>$meta_keyword_eng,'meta_desc_eng'=>$meta_desc_eng,'meta_title_arb'=>$meta_title_arb,'meta_keywords_arb'=>$meta_keyword_arb,'meta_desc_arb'=>$meta_desc_arb);
							
							if($this->master_model->insertRecord('tbl_seo_master',$input_array))
							{ 
								$this->session->set_flashdata('success','Seo Added Successfully.');
								redirect(base_url().'superadmin/frontpages/addseo');
							}
							else
							{
								$data['error']='Error while adding seo.';
							}
					}
					else
					{
						$data['error']=$this->form_validation->error_string();
					}
			}
		$data['middle_content']='add-seo';
		$this->load->view('admin/common-file',$data);
	}
	
	public function seoupdate()
	{
		$data['success']=$data['error']='';
	  	$data['pagetitle']='Rehla Ticket | Update SEO';
		
		 $seo_id=$this->uri->segment('4');
		 $data['seocontent']=$this->master_model->getRecords('tbl_seo_master',array('seo_id'=>$seo_id));
		
			if(isset($_POST['brn_seo']))
			{
				  $this->form_validation->set_rules('page_link','Page Link ','required');
				  $this->form_validation->set_rules('page_title','Page Title','required|xss_clean');
				  $this->form_validation->set_rules('meta_title_eng','Meta Title English','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_eng','Meta Keywords English','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_eng','Meta Description English','required|xss_clean');
				  $this->form_validation->set_rules('meta_title_arb','Meta Title Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_keyword_arb','Meta Keywords Arabic','required|xss_clean');
				  $this->form_validation->set_rules('meta_desc_arb','Meta Description Arabic','required|xss_clean');
				  
				  	if($this->form_validation->run())
			  		{
						 $page_link=$this->input->post('page_link',true);
						  $page_title=$this->input->post('page_title',true);
						  $meta_title_eng=$this->input->post('meta_title_eng',true);
						  $meta_keyword_eng=$this->input->post('meta_keyword_eng',true);
						  $meta_desc_eng=$this->input->post('meta_desc_eng',true);
						  
						  $meta_title_arb=$this->input->post('meta_title_arb',true);
						  $meta_keyword_arb=$this->input->post('meta_keyword_arb',true);
						  $meta_desc_arb=$this->input->post('meta_desc_arb',true);
						  
							$input_array=array('page_link'=>$page_link,'page_title'=>$page_title,'meta_title_eng'=>$meta_title_eng,'meta_keywords_eng'=>$meta_keyword_eng,'meta_desc_eng'=>$meta_desc_eng,'meta_title_arb'=>$meta_title_arb,'meta_keywords_arb'=>$meta_keyword_arb,'meta_desc_arb'=>$meta_desc_arb);
							
							if($this->master_model->updateRecord('tbl_seo_master',$input_array,array('seo_id'=>$seo_id)))
							{ 
								$this->session->set_flashdata('success','SEO updated Successfully.');
								redirect(base_url().'superadmin/frontpages/seoupdate/'.$seo_id);
							}
							else
							{
								$data['error']='Error while adding seo.';
							}
					}
					else
					{
						$data['error']=$this->form_validation->error_string();
					}
			}
			
		$data['middle_content']='edit-seo';
		$this->load->view('admin/common-file',$data);
	}
	
		
}