<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Booking extends CI_Controller {

	public function accom($accom_id='')
	{
	  $accom=base64_decode($accom_id);
	  $data['pagetitle']='Booking Details';
	  if($this->session->userdata('userType')=='user' && $this->session->userdata('userID')!='')
	  {
		  if($this->uri->segment(4)=='')
		  {
		    redirect(base_url().'booking/accom/'.$accom_id.'/passanger/');
		  }
	  }
	  if(isset($_POST['btn_login']))
	  {
		   $this->form_validation->set_rules('email','Email','required|xss_clean');
		   $this->form_validation->set_rules('password','Password','required|xss_clean');
		   if($this->form_validation->run())
		   {
			    $email=$this->input->post('email',true);
			    $password=$this->input->post('password',true);
			    $valid=$this->master_model->getRecords('tbl_user_master',array('user_email'=>$email,'user_password'=>$password));
				if(count($valid)>0)
				{
					if($valid[0]['user_email_verify']=='yes')
					{
						if($valid[0]['user_status']=='active')
						{
							$user_data=array('userID'=>$valid[0]['user_id'],
									 		'userEmail'=>$valid[0]['user_email'],
											'userFName'=>$valid[0]['user_name'],
											'userType'=>'user'
											 );
							$this->session->set_userdata($user_data);
							redirect(base_url().'booking/accom/'.$accom_id.'/passanger');
                        }
						else
						{
						   $data['error']='Sorry! you are block by admin.';
						}
					}
					else
					{
						$data['error']='Your account is not verified yet. Please verify your account from your email address with confirmation link';
					}
				}
				else
				{
					$data['error']='Invalid credentials.';
				}
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
	  }
	  if(isset($_POST['btn_booking']))
	  {
		  if(count($_POST['user_name']))
		  {
			  
              $passngerArray=array('booking_form_id'=>$accom,'type'=>'accom','booking_user_id'=>$this->session->userdata('userID'));
			  $checkInsert=$this->master_model->getRecords('tbl_tmp_booking',$passngerArray);
			  if(count($checkInsert))
			  {
				 $passngerArray=array('booking_form_id'=>$accom,'type'=>'accom','booking_user_id'=>$this->session->userdata('userID')); 
				 $this->db->set($passngerArray);
				 $this->db->where('booking_id',$checkInsert[0]['booking_id']);
			 	 $success=$this->db->update('tbl_tmp_booking');  
				 $booking_id=$checkInsert[0]['booking_id'];
			  }
			  else
			  {
				  $passngerArray=array('booking_form_id'=>$accom,'type'=>'accom','booking_user_id'=>$this->session->userdata('userID'));
				  $this->master_model->insertRecord('tbl_tmp_booking',$passngerArray);   
				  $booking_id=$this->db->insert_id(); 
			  }
			  if($booking_id)
			  {
				 
				 if($this->session->userdata('rowId')!=$this->session->userdata('session_id').'_'.$booking_id)
				 {
				   $rowId=$this->session->userdata('session_id').'_'.$booking_id;
			       $this->session->set_userdata('rowId',$rowId);
				   $update=array('tmp_id'=>$rowId);
				 }
				 else
				 {
				   $rowId=$this->session->userdata('session_id').'_'.$booking_id; 
				 }
				 $this->db->set('tmp_id',$rowId);
				 $this->db->where('booking_id',$booking_id);
			 	 $success=$this->db->update('tbl_tmp_booking');
				 
                 if(count($_POST['user_name']))
				 {
					 $this->master_model->deleteRecord('tbl_passenger_list','tmp_booking_id',$booking_id);
					 for($i=0;$i<count($_POST['user_name']);$i++)
					 {
						 if($_POST['user_name'][$i]!='')
						 {
							$inserArray=array('tmp_booking_id'=>$booking_id,'passenger_name'=>$_POST['user_name'][$i],'passenger_gender'=>$_POST['gender'][$i],'passanger_age'=>$_POST['age'][$i],'tmp_id'=>$rowId);
							$this->master_model->insertRecord('tbl_passenger_list',$inserArray);
						}
					 }
                     redirect(base_url().'booking/accom/'.$accom_id.'/billing');
				  }
			  }
			}
			else
			{
				redirect(base_url().'booking/accom/'.$accom_id.'/passanger');
			}
      }
	  if(isset($_POST['btn_paypal']))
	  {
		$price=$_POST['price'];
		$passanger_type = $_POST['passanger_type'];
		$user_data=array('price'=>$price,'passanger_type'=>$passanger_type);
		$this->session->set_userdata($user_data);
		redirect(base_url().'booking/secure_payment/');
	  }
	  if(isset($_POST['btn_bank']))
	  {
		  
			$this->load->library('upload');
			$screen_shot='';
		    $config=array('upload_path'=>'uploads/payment/',
								'allowed_types'=>'*',
								'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			if($_FILES['screen_shot']['name']!='')
			{
				if($this->upload->do_upload('screen_shot'))
				{
					$dt=$this->upload->data();
					$screen_shot=$dt['file_name'];
				}
				else
				{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(base_url().'booking/accom/'.$accom_id.'/billing');
				}
			}
			else
			{
			  redirect(base_url().'booking/accom/'.$accom_id.'/billing');
			}
            if($screen_shot!='')
			{
				$st='completed';
				$amt=$_POST['price'];
				$passanger_type = $_POST['passanger_type'];
				$tran_date=date('Y-m-d h:i:s');
				$orderID = rand().time();

				$bookingTbl=$this->master_model->getRecords('tbl_tmp_booking',array('tmp_id'=>$this->session->userdata('rowId')));
				$insert_array=array(
					'orderID'=>$orderID,
					'passanger_type'=>$passanger_type,
					'tranAmount'=>$amt,
					'tranStatus'=>$st,
					'rowId'=>$this->session->userdata('rowId'),
					'truserID'=>$this->session->userdata('userID'),
					'booking_id'=>$bookingTbl[0]['booking_id'],
					'booking_type'=>$bookingTbl[0]['booking_type'],
					'booking_form_id'=>$bookingTbl[0]['booking_form_id'],
					'type'=>$bookingTbl[0]['type'],
					'tmp_id'=>$bookingTbl[0]['tmp_id'],
					'booking_user_id'=>$bookingTbl[0]['booking_user_id'],
                    'booking_time'=>$this->session->userdata('availability_check'), 
					'payType'=>'bank',
					'type'=>'accom',
					'accom_from_date'=>date('Y-m-d H:i:s',strtotime($this->session->userdata('accom_session_from_date'))),
					'accom_to_date'=>date('Y-m-d H:i:s',strtotime($this->session->userdata('accom_session_to_date'))),
					'doc'=>$screen_shot,
					'tranDate'=>$tran_date
				);
				$complete=$this->db->insert('tbl_transaction_master',$insert_array,true);
				$id=$this->db->insert_id();
                $rowId = $this->session->userdata('rowId');
				$this->master_model->deleteRecord('tbl_tmp_booking','tmp_id',$this->session->userdata('rowId'));

				$array_items = array('rowId'=>'','price'=>'','accom_session_from_date'=>'','accom_session_to_date'=>'');
				$this->session->unset_userdata($array_items);
				redirect(base_url().'booking/thanks/'.base64_encode($rowId));
			}
		}
	
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$accom));
	  $data['middle_content']='accom-booking';
	  $this->load->view('common-file',$data);
	}
	public function bookingDate($accom_id)
	{
		if($this->input->post('txt_from_date')!='' && $this->input->post('txt_to_date')!='')
		{
		    $accomdation_sel_from_date=$this->input->post('txt_from_date');
			$accomdation_sel_to_date=$this->input->post('txt_to_date');
			$session_array=array('accom_session_from_date'=>$accomdation_sel_from_date,'accom_session_to_date'=>$accomdation_sel_to_date,'accom_session_id'=>$accom_id);
			$this->session->set_userdata($session_array);
			if($this->session->userdata('userType')=='user' && $this->session->userdata('userID')!='')
			{
			  if($this->uri->segment(4)=='')
			  {
			    echo base_url().'booking/accom/'.$accom_id.'/passanger/';
			  }
			}
			else
			{
			  echo base_url().'booking/accom/'.$accom_id.'/';
			}
			
		}
		else
		{
			echo 'error';
		}
	}
	public function tours($three_id='',$type='')
	{
	  $formsID =base64_decode($three_id);
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
	  $data['pagetitle']='Booking Details';
	  if($this->session->userdata('userType')=='customer' && $this->session->userdata('userID')!='')
	  {   
	    if($this->uri->segment(4)=='')
	    {redirect(base_url().'booking/tours/'.$three_id.'/passanger/');}
	  }
	  if(isset($_POST['btn_login']))
	  {
		   $this->form_validation->set_rules('email','Email','required|xss_clean');
		   $this->form_validation->set_rules('password','Password','required|xss_clean');
		   if($this->form_validation->run())
		   {
			   $email=$this->input->post('email',true);
			   $password=$this->input->post('password',true);
			   $valid=$this->master_model->getRecords('tbl_user_master',array('user_email'=>$email,'user_password'=>$password));
				if(count($valid)>0)
				{
					if($valid[0]['user_email_verify']=='yes')
					{
						if($valid[0]['user_status']=='active')
						{
							$user_data=array('userID'=>$valid[0]['user_id'],
									 		 'userEmail'=>$valid[0]['user_email'],
											 'userFName'=>$valid[0]['user_name'],
											 'userType'=>'user'
											);
							$this->session->set_userdata($user_data);
                            redirect(base_url().'booking/tours/'.$three_id.'/passanger/');
						}
						else
						{
							$data['error']='Sorry! you are block by admin.';
						}
					}
					else
					{
						$data['error']='Your account is not verified yet. Please verify your account from your email address with confirmation link';
					}
				}
				else
				{
					$data['error']='Invalid credentials.';
				}
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
	  }
	  if(isset($_POST['btn_booking']))
	  {
		
		  if(count($_POST['user_name']))
		  {
              $passngerArray=array('booking_form_id'=>$formsID,'type'=>$data['fetch_array_details'][0]['form_type'],'booking_user_id'=>$this->session->userdata('userID'));
			  $checkInsert=$this->master_model->getRecords('tbl_tmp_booking',$passngerArray);
			  if(count($checkInsert))
			  {
			     $passngerArray=array('booking_type'=>$_POST['check_type'],'booking_form_id'=>$formsID,'type'=>$data['fetch_array_details'][0]['form_type'],'booking_user_id'=>$this->session->userdata('userID')); 
				 $this->db->set($passngerArray);
				 $this->db->where('booking_id',$checkInsert[0]['booking_id']);
			 	 $success=$this->db->update('tbl_tmp_booking');  
				 $booking_id=$checkInsert[0]['booking_id'];
			  }
			  else
			  {
				  
				  $passngerArray=array('booking_type'=>$_POST['check_type'],'booking_form_id'=>$formsID,'type'=>$data['fetch_array_details'][0]['form_type'],'booking_user_id'=>$this->session->userdata('userID'));
			      $this->master_model->insertRecord('tbl_tmp_booking',$passngerArray);
			      $booking_id=$this->db->insert_id();
			  }
			  if($booking_id!='')
			  {
				 if($this->session->userdata('rowId')!=$this->session->userdata('session_id').'_'.$booking_id)
				 {
				   $rowId=$this->session->userdata('session_id').'_'.$booking_id;
				   $this->session->set_userdata('rowId',$rowId);
				   $update=array('tmp_id'=>$rowId);
				 }
				 else
				 {
				   $rowId=$this->session->userdata('session_id').'_'.$booking_id; 
				 }
				 $this->db->set('tmp_id',$rowId);
				 $this->db->where('booking_id',$booking_id);
			 	 $success=$this->db->update('tbl_tmp_booking');
                 if(count($_POST['user_name']))
				 {
					 $this->master_model->deleteRecord('tbl_passenger_list','tmp_booking_id',$booking_id);
					 for($i=0;$i<count($_POST['user_name']);$i++)
					 {
						 if($_POST['user_name'][$i]!='')
						 {
							$passanger_adult_type = $_POST['check_human_type'.$i]; 
							$inserArray=array('tmp_booking_id'=>$booking_id,'passenger_name'=>$_POST['user_name'][$i],'passenger_gender'=>$_POST['gender'][$i],'passanger_age'=>$_POST['age'][$i],'tmp_id'=>$rowId,'passanger_adult_type'=>$passanger_adult_type);
							$this->master_model->insertRecord('tbl_passenger_list',$inserArray);
						}
					 }
                      redirect(base_url().'booking/tours/'.$three_id.'/billing');
				  }
			  }
			}
		  else
		  {redirect(base_url().'booking/tours/'.$three_id.'/passanger');}

	  }
      if(isset($_POST['btn_paypal']))
	  {
			$price=$_POST['price'];
			$passanger_type = $_POST['passanger_type'];
			$user_data=array('price'=>$price,'passanger_type'=>$passanger_type);
			$this->session->set_userdata($user_data);
			redirect(base_url().'booking/secure_payment/');
	  }
	  if(isset($_POST['btn_bank']))
	  {
			$this->load->library('upload');
			$screen_shot='';
		    $config=array('upload_path'=>'uploads/payment/',
								'allowed_types'=>'*',
								'file_name'=>rand(1,9999),'max_size'=>0);
			$this->upload->initialize($config);
			if($_FILES['screen_shot']['name']!='')
			{
				if($this->upload->do_upload('screen_shot'))
				{
					$dt=$this->upload->data();
					$screen_shot=$dt['file_name'];
				}
				else
				{
					$this->session->set_flashdata('error',$this->upload->display_errors());
					redirect(base_url().'booking/tours/'.$three_id.'/billing');
				}
			}
			else
			{
				redirect(base_url().'booking/tours/'.$three_id.'/billing');
			}
           if($screen_shot!='')
			{
				$st='completed';
				$amt=$_POST['price'];
				$tran_date=date('Y-m-d h:i:s');
				$orderID = rand().time();
                $passanger_type = $_POST['passanger_type'];
				$bookingTbl=$this->master_model->getRecords('tbl_tmp_booking',array('tmp_id'=>$this->session->userdata('rowId')));
                $insert_array=array(
				    'orderID'=>$orderID,
					'passanger_type' =>$passanger_type,
					'tranAmount'=>$amt,
					'tranStatus'=>$st,
					'rowId'=>$this->session->userdata('rowId'),
					'truserID'=>$this->session->userdata('userID'),
					'booking_id'=>$bookingTbl[0]['booking_id'],
					'booking_type'=>$bookingTbl[0]['booking_type'],
					'booking_form_id'=>$bookingTbl[0]['booking_form_id'],
					'type'=>$bookingTbl[0]['type'],
					'tmp_id'=>$bookingTbl[0]['tmp_id'],
					'booking_user_id'=>$bookingTbl[0]['booking_user_id'],
					'payType'=>'bank',
					'booking_time'=>$this->session->userdata('availability_check'),
					'doc'=>$screen_shot,
					'tranDate'=>$tran_date
				);
				$complete=$this->db->insert('tbl_transaction_master',$insert_array,true);	
				$id=$this->db->insert_id();
				$rowId=$this->session->userdata('rowId');
                $this->master_model->deleteRecord('tbl_tmp_booking','tmp_id',$this->session->userdata('rowId'));
                $array_items = array('rowId' => '','price'=>'');
				$this->session->unset_userdata($array_items);
				redirect(base_url().'booking/thanks/'.base64_encode($rowId));
			}
		}
      $data['middle_content']='three-form-booking';
	  $this->load->view('common-file',$data);
	}
    public function secure_payment()
	{
		$data['pagetitle']='Booking Details';
		$this->load->view('secrate-paypal',$data);
	}
    public function return_payment()
	{
        if($this->session->userdata('rowId')==TRUE)
		{

			$tx=$_GET['tx'];
			$st=$_GET['st'];
			$amt=$_GET['amt'];
			$tran_date=date('Y-m-d H:i:s');
			$orderID = rand().time();

			$bookingTbl=$this->master_model->getRecords('tbl_tmp_booking',array('tmp_id'=>$this->session->userdata('rowId')));

			$insert_array=array(
				'transactionID'=>$tx,
				'orderID'=>$orderID,
				'tranAmount'=>$amt,
				'tranStatus'=>$st,
				'passanger_type' =>$this->session->userdata('passanger_type'),
				'rowId'=>$this->session->userdata('rowId'),
				'truserID'=>$this->session->userdata('userID'),
				'booking_id'=>$bookingTbl[0]['booking_id'],
				'booking_type'=>$bookingTbl[0]['booking_type'],
				'booking_form_id'=>$bookingTbl[0]['booking_form_id'],
				'type'=>$bookingTbl[0]['type'],
				'tmp_id'=>$bookingTbl[0]['tmp_id'],
				'booking_user_id'=>$bookingTbl[0]['booking_user_id'],
				'booking_time'=>$this->session->userdata('availability_check'),
				'accom_from_date'=>date('Y-m-d H:i:s',strtotime($this->session->userdata('accom_session_from_date'))),
				'accom_to_date'=>date('Y-m-d H:i:s',strtotime($this->session->userdata('accom_session_to_date'))),
				'payType'=>'paypal',
				'tranDate'=>$tran_date
			);
			$complete=$this->db->insert('tbl_transaction_master',$insert_array,true);
			$id=$this->db->insert_id();
            $rowId=$this->session->userdata('rowId');
			$this->master_model->deleteRecord('tbl_tmp_booking','tmp_id',$this->session->userdata('rowId'));

			$array_items = array('rowId' => '','price'=>'','passanger_type'=>'','accom_session_from_date'=>'','accom_session_to_date'=>'');
			$this->session->unset_userdata($array_items);
			redirect(base_url().'booking/thanks/'.base64_encode($rowId));
		}
		else
		{
			redirect(show_404());
		}
	}

	public function thanks($rowId)
	{
		$rowId=base64_decode($rowId);
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}
		$data['error']='';
		$data['pagetitle']='Thank you';
		$data['details']=$details=$this->master_model->getRecords('tbl_transaction_master',array('rowId'=>$rowId));
		if($details[0]['type']=='Tours' || $details[0]['type']=='Adventure' || $details[0]['type']=='Recreation' || $details[0]['type']=='packages')
		{
		   $data['boatDetails']=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$details[0]['booking_form_id']),'form_title AS title');
		}
		else if($details[0]['type']=='accom')
		{
		   $data['boatDetails']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$details[0]['booking_form_id']),'accom_title AS title');
		}
		else {
			$data['boatDetails']=array();
		}
        $data['middle_content']='thankyou';
	  	$this->load->view('common-file',$data);
	}

	public function cancel_payment()
	{
		$array_items = array('rowId' => '','price'=>'');
		$this->session->unset_userdata($array_items);
		$this->load->view('cancel-payment');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
