<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class app extends CI_Controller{
	public function __construct()
    {
	  parent::__construct();
	}
	public function userLogin()
	{
		$resp['result'] ='';
		$resp['user_message']='';
		if(isset($_REQUEST))
		{
			if($_REQUEST['user_type']=='user')
			{
			   $email=$_REQUEST['email'];
			   $password=$_REQUEST['password'];
			   $valid=$this->master_model->getRecords('tbl_user_master',array('user_email'=>$email,'user_password'=>$password));
			   if(count($valid)>0)
			   {
					if($valid[0]['user_email_verify']=='yes')
					{
						if($valid[0]['user_status']=='active')
						{
						         $resp['data']=array('userID'=>$valid[0]['user_id'],
								                     'userEmail'=>$valid[0]['user_email'],
												     'userName'=>$valid[0]['user_name'],
												     'userType'=>'user');
								 $resp['result']='success';
								 $resp['user_message']='Login successfully';	 	
						}
						else
						{
							$resp['result']='fail';
							$resp['user_message']='Sorry! you are block by admin.';
						}
					}
					else
					{
						$resp['result']='fail';
						$resp['user_message']='Your account is not verified yet. Please verify your account from your email address with confirmation link';
					}
				}
			   else
			   {
				    $resp['result']='fail';
					$resp['user_message']='Invalid credentials.';
			   }
			}
			if($_REQUEST['user_type']=='owner')
			{
				
				   //$email=$_REQUEST['hosts_email'];
				   //$password=$_REQUEST['hosts_password'];
				   if(isset($_REQUEST['hosts_email']))
				   {$email=$_REQUEST['hosts_email'];}
				   else
				   {$email=$_REQUEST['email'];}
				   if(isset($_REQUEST['hosts_password']))
				   {$password=$_REQUEST['hosts_password'];}
				   else
				   {$password=$_REQUEST['password'];}
				   
				   $valid=$this->master_model->getRecords('tbl_hosts_master',array('hosts_email'=>$email,'hosts_password'=>$password));
					if(count($valid)>0)
					{
						if($valid[0]['hosts_email_verify']!='no')
						{
							if($valid[0]['hosts_status']!='block')
							{
								
								$resp['data']=array('userID'=>$valid[0]['hosts_id'],
												    'userEmail'=>$valid[0]['hosts_email'],
												    'userFName'=>$valid[0]['hosts_name'],
												    'userType'=>'owner'
												    );			  
								$resp['result'] ='success';
		                        $resp['user_message']='owner login successfully !';				  
							    
							}
							else
							{
							  $resp['result'] ='fail';
		                      $resp['user_message']='Sorry! you are block by admin.';		
							}
						}
						else
						{
						   	$resp['result'] ='fail';
		                    $resp['user_message']='Sorry! You are not verify. Please verify your email id.';		
						}
					}
					else
					{
						$resp['result'] ='fail';
		                $resp['user_message']='Invalid credentials.';
						
					}
			}
		    echo str_replace('\/','/',json_encode($resp));
		}
	}
	public function userReg()
	{
		$this->load->library('upload');	
		$resp['result'] ='';
		$resp['user_message']='';
		if(isset($_REQUEST['user_email']))
		{
			
				$firstname=$_REQUEST['user_name'];
				$email=$_REQUEST['user_email'];
				$password=$_REQUEST['user_password'];
				$config1=array('upload_path'=>'uploads/profile/',
							   'allowed_types'=>'jpg|jpeg|gif|png|ico',
							   'file_name'=>rand(1,9999),'max_size'=>0);
				$this->upload->initialize($config1);
				if($_FILES['user_id_info']['name']!='')
				{
					if($this->upload->do_upload('user_id_info'))
					{
					  $dt=$this->upload->data();
					  $_REQUEST['user_id_info']=$dt['file_name'];
					}
					else
					{
						$_REQUEST['user_id_info']='';
						$res['error']=$this->upload->display_errors();
					}
				}
				
				$getResponse=$this->master_model->getRecords('tbl_user_master',array('user_email'=>$email));
				if(count($getResponse)==0)
				{
					if($this->master_model->insertRecord('tbl_user_master',$_REQUEST))
					{
						$userId=$this->db->insert_id();
						/*$whr=array('id'=>'1');
						$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
						$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$email,'subject'=>'User Successfully Registered','view'=>'user-registration');
						$other_info=array('firstname'=>$firstname,'email'=>$email,'password'=>$password,'userId'=>$userId);
						$this->email_sending->sendmail($info_arr,$other_info);
						
						$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
						$other_info1=array('firstname'=>$firstname,'email'=>$email,'lastname'=>'');
						$this->email_sending->sendmail($info_arr1,$other_info1); */
						
						$resp['result'] ='success';
						$resp['user_message']='You are Register successfully. Please login to continue.';
					}
					else
					{
						$resp['result'] ='fail';
						$resp['user_message']='Oops ! something wrong. Please try sometimes leter.';
					}
				}
				else
				{
					$resp['result'] ='fail';
					$resp['user_message']='Email Id already exists please try another email ID';	
				}
		}
		$resp['currency']=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
		$resp['language']=$this->master_model->getRecords('tbl_language_master',array('language_status'=>'active'));
		$this->db->group_by('country');
		$resp['country']=$this->master_model->getRecords('tbl_cities_master');
		echo str_replace('\/','/',json_encode($resp));
	}
	public function ownerReg()
	{
		$this->load->library('upload');	
		$resp['result'] ='';
		$resp['data']=array();
		$resp['user_message']='';
		
		if(isset($_REQUEST))
		{
			    
		        $hosts_name=$_REQUEST['hosts_name'];
			    $hosts_company=$_REQUEST['hosts_company'];
			    $categories_id=$_REQUEST['hosts_mainategory_id'];
			    $hosts_description=$_REQUEST['hosts_description'];
			    $hosts_address=$_REQUEST['hosts_address'];
			    $hosts_website = $_REQUEST['hosts_website'];
			    $hosts_contact = $_REQUEST['hosts_contact'];
			    $hosts_position = $_REQUEST['hosts_position'];
			    $hosts_mobile = $_REQUEST['hosts_mobile'];
			    $hosts_email=$_REQUEST['hosts_email'];
			    $hosts_preferred_payment = $_REQUEST['hosts_preferred_payment'];
			    $hosts_preferred_currency = $_REQUEST['hosts_preferred_currency'];
			    $latitude='';
			    $longitude='';
			    $hosts_mainategory_id='';
				if($categories_id)
				{
				  $_REQUEST['hosts_mainategory_id']=implode(',',$categories_id);
				}
				if($hosts_address!='')
				{
					  $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($hosts_address).'&sensor=false');
						// We convert the JSON to an array
						$geo = json_decode($geo, true);
						// If everything is cool
						if ($geo['status'] = 'OK') {
						  // We set our values
						  $_REQUEST['hosts_latitude']  = $geo['results'][0]['geometry']['location']['lat'];
						  $_REQUEST['hosts_longitude']  = $geo['results'][0]['geometry']['location']['lng'];
						}
				 }
				if($hosts_email!='')
				{   
			        $getResponse=$this->master_model->getRecords('tbl_hosts_master',array('hosts_email'=>$hosts_email));
				    if(count($getResponse)==0)
					{
						$dateAdded=date('Y-m-d h:i:s');
						if($this->master_model->insertRecord('tbl_hosts_master',$_REQUEST))
						{
							$owner_id=$this->db->insert_id();
							
							/*$whr=array('id'=>'1');
							$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
							$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$hosts_email,'subject'=>'Host Successfully Registered','view'=>'boat-owner-registration');
							$other_info=array('firstname'=>$hosts_name,'email'=>$hosts_email,'lastname'=>'','password'=>$hosts_password,'userId'=>$owner_id);
							$this->email_sending->sendmail($info_arr,$other_info);
							
							$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
							$other_info1=array('firstname'=>$hosts_name,'email'=>$hosts_email,'lastname'=>'');
							$this->email_sending->sendmail($info_arr1,$other_info1); */
							
							$resp['result'] ='success';
							$resp['user_message']='You are Register successfully. Please login to continue.';
						}
						else
						{
							$resp['result'] ='fail';
							$resp['user_message']='Oops ! something wrong. Please try sometimes leter.';
						}
					}
					else
					{
					    $resp['result'] ='fail';
						$resp['user_message']='Email Id already exists please try another email ID';	
					}
				}
		 }
		$resp['currency']=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active')); 
		$resp['category']=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active'));   
		echo str_replace('\/','/',json_encode($resp));
	}
	public function homepage()
	{
		$resp['result'] ='';
		$this->db->select('accom_id,accom_title,accom_price,accom_percentage_discount,accom_added_date,accom_image');
	    $resp['accommodations']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_status'=>'active')); 
		$this->db->select('formsID,form_title,form_image,form_duration,form_price,form_bird_discount,form_added_date,form_image');
		$resp['tours']=$this->master_model->getRecords('tbl_three_forms',array('form_status'=>'active','form_type'=>'Tours'));;
		$this->db->select('formsID,form_title,form_image,form_duration,form_price,form_bird_discount,form_added_date,form_image'); 
		$resp['adventure']=$this->master_model->getRecords('tbl_three_forms',array('form_status'=>'active','form_type'=>'Adventure')); 
		$this->db->select('formsID,form_title,form_image,form_duration,form_price,form_bird_discount,form_added_date,form_image');
		$resp['recreation']=$this->master_model->getRecords('tbl_three_forms',array('form_status'=>'active','form_type'=>'Recreation'));; 
		echo str_replace('\/','/',json_encode($resp));
	}
	public function topdestination()
	{
	   	 $resp['result'] ='';
		 $this->db->select('accom_id AS id,accom_title AS title,accom_price AS price,accom_percentage_discount AS discount,accom_added_date AS addeddate,accom_image as image');
		 $topaccommodations=$this->master_model->getRecords('tbl_accommodations_master',array('top_destination_show'=>'yes'));
		 $this->db->select('formsID AS id,form_title AS title,form_image as image,form_duration,form_price AS price,form_bird_discount AS discount,form_added_date AS addeddate');
		 $topthree=$this->master_model->getRecords('tbl_three_forms',array('top_destination_show'=>'yes'));;
		 $resp['topdestination'] = array_merge($topaccommodations,$topthree);
		 echo str_replace('\/','/',json_encode($resp));
	}
	public function details()
	{
		$resp['result'] ='';
		$resp['detail']=array();
		$resp['message_type']='fails';
		$resp['message']='some thing is wrong';
		$type = $_REQUEST['type'];
		$id   = $_REQUEST['id'];
		if($id!='')
		{
			switch($type)
			{
				case 'accomdation' :
				  $resp['message_type']='success';
				  $resp['message']='';
				  $resp['detail']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$id));
				  $resp['image_array']=$this->master_model->getRecords('tbl_accommodations_images',array('accom_id'=>$id));
				  if($resp['detail']['accom_image']!='')
				  {array_push($resp['image_array'],$resp['detail']['accom_image']);}
				  $this->db->select('accom_id AS id,accom_title AS title,accom_price AS price,accom_percentage_discount AS discount,accom_added_date AS addeddate,accom_image as image');
				  $resp['similar_post']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_category_id'=>$resp['detail'][0]['accom_category_id'],'accom_status'=>'active'));
				  $resp['review']=$this->master_model->getRecords('tbl_rating_master',array('fk_form_id'=>$id,'form_type'=>'accom'));
				break;  
				case 'transport' :
				  $resp['message_type']='success';
				  $resp['message']='';
				  $resp['detail']=$this->master_model->getRecords('tbl_transportation_master',array('transportation_id'=>$id),array(''));
				break;  
				case 'event' :
				   $resp['message_type']='success';
				   $resp['message']='';
				   $resp['detail']=$this->master_model->getRecords('tbl_event_master',array('event_id'=>$id));
				break;  
				default :
				   $resp['message_type']='success';
				   $resp['message']='';
				   $resp['detail']=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$id));
				   $resp['image_array']=$this->master_model->getRecords('tbl_threeforms_images',array('form_id'=>$id));
				   if($resp['detail']['form_image']!='')
				   {array_push($resp['image_array'],$resp['detail']['form_image']);}
				   $this->db->where('form_type',$type);
				   $resp['review']=$this->master_model->getRecords('tbl_rating_master',array('fk_form_id'=>$id));
				    $this->db->select('formsID,form_title,form_image,form_duration,form_price,form_bird_discount,form_added_date');
				   $resp['similar_post']=$this->master_model->getRecords('tbl_three_forms',array('categoryID'=>$resp['detail'][0]['categoryID']));
				break;
				
			}
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function searching()
	{
		$resp['result'] ='';
		$resp['data']=array();
		$resp['message_type']='fails';
		$resp['message']='some thing is wrong';
		$type=$_REQUEST['type'];
		if($type!='')
		{
			switch($type)
			{
				case 'accomdation' :
				  $resp['message_type']='success';
				  $resp['message']='';
				  $categoryID='1';
				
		          $minPrice = $_REQUEST["minPrice"];
		          $maxPrice = $_REQUEST["maxPrice"];
		          $category = $_REQUEST["category"];
                  $subcategory = $_REQUEST["subcategory"];
		          //sort
		          $order_price = $_REQUEST["order_price"];
		          $order_star = $_REQUEST["order_star"];
		          $order_user = $_REQUEST["order_user"];
		          $order_name = $_REQUEST["order_name"];
		
		          $checkin = $_REQUEST["checkin"];
		          $checkout = $_REQUEST["checkout"];
		          $adult = $_REQUEST["adult"];
		          $child = $_REQUEST["child"];
		          if($category!='') {
						$categories=explode(',',$category);
						$catcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($categories as $cat)
						{
							if($catcounter==1)
							{$whereParam.="tbl_accommodations_master.accom_category_id='".$cat."'";}
							else
							{$whereParam.=" OR tbl_accommodations_master.accom_category_id='".$cat."'";}
							$catcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
					if($subcategory!='') {
						$subcategories=explode(',',$subcategory);
						$subcatcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($subcategories as $subcat)
						{
							if($subcatcounter==1)
							{$whereParam.="tbl_accommodations_master.accom_subcategory_id='".$subcat."'";}
							else
							{$whereParam.=" OR tbl_accommodations_master.accom_subcategory_id='".$subcat."'";}
							$subcatcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
					if($checkin!='')
					{
						$this->db->where('DATE(tbl_accommodations_master.accom_check_in)', date('Y-m-d',strtotime($checkin)));
					}
					if($checkout!='')
					{
						$this->db->where('DATE(tbl_accommodations_master.accom_check_out)', date('Y-m-d',strtotime($checkout)));
					}
					if($minPrice!='' && $maxPrice!='' )
					{
						$this->db->where('tbl_accommodations_master.accom_price >=', $minPrice);
						$this->db->where('tbl_accommodations_master.accom_price <=', $maxPrice);
					}
					if($order_price)
					{
						$this->db->order_by('tbl_accommodations_master.accom_price',$order_price);
					}
					if($order_name)
					{
						$this->db->order_by('tbl_accommodations_master.accom_title',$order_name);
					}
				   $this->db->select('accom_id AS id,accom_title AS title,accom_price AS price,accom_percentage_discount AS discount,accom_added_date AS addeddate,accom_image as image,(CASE WHEN (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="accom") IS NULL THEN 0 ELSE (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="accom") END ) AS rating');	
				   $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_accommodations_master.hosts_id');
		           $resp['searchData']=$this->master_model->getRecords('tbl_accommodations_master',array('tbl_accommodations_master.accom_status'=>'active'));
				   $resp['category']=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
				break;  
				case 'transport' :
				  $resp['message_type']='success';
				  $resp['message']='';
				  $categoryID='5';
				            $data['searchData']=array();
							$minPrice = $_REQUEST["minPrice"];
							$maxPrice = $_REQUEST["maxPrice"];
							$category = $_REQUEST["category"];
							$subcategory = $_REQUEST["subcategory"];
							//sort
							$order_price = $_REQUEST["order_price"];
							$order_star = $_REQUEST["order_star"];
							$order_user = $_REQUEST["order_user"];
							$order_name = $_REQUEST["order_name"];
					
							$pickup_location =$_REQUEST["pickup_location"];
							$drop_location = $_REQUEST["drop_location"];
					
							$result=array();
					
							if($category!='') 
							{
								$categories=explode(',',$category);
								$catcounter=1;
								$whereParam="";
								$whereParam.="(";
								foreach($categories as $cat)
								{
									if($catcounter==1)
									{$whereParam.="tbl_transportation_master.transportation_category_id='".$cat."'";}
									else
									{$whereParam.=" OR tbl_transportation_master.transportation_category_id='".$cat."'";}
									$catcounter++;
								}
								$whereParam.=")";
								$this->db->where($whereParam);
							}
							if($subcategory!='') 
							{
								$subcategories=explode(',',$subcategory);
								$subcatcounter=1;
								$whereParam="";
								$whereParam.="(";
								foreach($subcategories as $subcat)
								{
									if($subcatcounter==1)
									{$whereParam.="tbl_transportation_master.transportation_subcategory_id='".$subcat."'";}
									else
									{$whereParam.=" OR tbl_transportation_master.transportation_subcategory_id='".$subcat."'";}
									$subcatcounter++;
								}
								$whereParam.=")";
								$this->db->where($whereParam);
							}
					        if($pickup_location!='')
							{
								$this->db->where('tbl_transportation_master.transportation_pickup_location', $pickup_location);
							}
							if($drop_location!='')
							{
								$this->db->where('tbl_transportation_master.transportation_drop_location', $drop_location);
							}
							if($order_name)
							{
								$this->db->order_by('tbl_transportation_master.transportation_title',$order_name);
							}
					$this->db->select('tbl_transportation_master.*,(CASE WHEN (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=transportation_id AND form_type="transport") IS NULL THEN 0 ELSE (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=transportation_id AND form_type="transport") END ) AS rating');	
					$this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_transportation_master.hosts_id');
					$resp['searchData']=$this->master_model->getRecords('tbl_transportation_master',array('tbl_transportation_master.transportation_status'=>'active'));
				    $resp['category']=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
				break;  
				case 'event' :
				   $resp['message_type']='success';
				   $resp['message']='';
				   $categoryID='6';
				   
				    $data['searchData']=array();
					$minPrice = $_REQUEST["minPrice"];
					$maxPrice = $_REQUEST["maxPrice"];
					$category = $_REQUEST["category"];
					$subcategory = $_REQUEST["subcategory"];
					
					//sort
					$order_price = $_REQUEST["order_price"];
					$order_star = $_REQUEST["order_star"];
					$order_user = $_REQUEST["order_user"];
					$order_name = $_REQUEST["order_name"];
		            $destination = $_REQUEST["destination"];

		
		            $result=array();
					if($category!='') {
						$categories=explode(',',$category);
						$catcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($categories as $cat)
						{
							if($catcounter==1)
							{$whereParam.="tbl_event_master.event_category_id='".$cat."'";}
							else
							{$whereParam.=" OR tbl_event_master.event_category_id='".$cat."'";}
							$catcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
					
					if($subcategory!='') {
						$subcategories=explode(',',$subcategory);
						$subcatcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($subcategories as $subcat)
						{
							if($subcatcounter==1)
							{$whereParam.="tbl_event_master.event_subcategory_id='".$subcat."'";}
							else
							{$whereParam.=" OR tbl_event_master.event_subcategory_id='".$subcat."'";}
							$subcatcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
					
					if($destination!='')
					{
						$this->db->where('tbl_event_master.event_location', $destination);
					}
					if($order_name)
					{
						$this->db->order_by('tbl_event_master.event_title',$order_name);
					}
				   $this->db->select('tbl_event_master.*,(CASE WHEN (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=event_id AND form_type="event") IS NULL THEN 0 ELSE (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=event_id AND form_type="event") END ) AS rating');	
		           $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_event_master.hosts_id');
				   $resp['searchData']=$this->master_model->getRecords('tbl_event_master',array('tbl_event_master.event_status'=>'active'));
		           $resp['category']=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
				break;  
				case 'tours' :
				   $resp['message_type']='success';
				   $resp['message']='';
				   $categoryID='2';
				   $data['searchData']=array();
					
					$minPrice = $_REQUEST["minPrice"];
					$maxPrice = $_REQUEST["maxPrice"];
					$category = $_REQUEST["category"];
					$subcategory = $_REQUEST["subcategory"];
					
					//sort
					$order_price = $_REQUEST["order_price"];
					$order_star = $_REQUEST["order_star"];
					$order_user = $_REQUEST["order_user"];
					$order_name = $_REQUEST["order_name"];
		            $destination = $_REQUEST["destination"];

					$adult = $_REQUEST["adult"];
					$child = $_REQUEST["child"];
					
					$result=array();
		            if($category!='') {
						$categories=explode(',',$category);
						$catcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($categories as $cat)
						{
							if($catcounter==1)
							{$whereParam.="tbl_three_forms.categoryID='".$cat."'";}
							else
							{$whereParam.=" OR tbl_three_forms.categoryID='".$cat."'";}
							$catcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
		
					if($subcategory!='') {
						$subcategories=explode(',',$subcategory);
						$subcatcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($subcategories as $subcat)
						{
							if($subcatcounter==1)
							{$whereParam.="tbl_three_forms.subcategoryID='".$subcat."'";}
							else
							{$whereParam.=" OR tbl_three_forms.subcategoryID='".$subcat."'";}
							$subcatcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
		
					if($destination!='')
					{
						$this->db->where('tbl_three_forms.form_departure_address', $destination);
					}
					if($minPrice!='' && $maxPrice!='' )
					{
						$this->db->where('tbl_three_forms.form_price >=', $minPrice);
						$this->db->where('tbl_three_forms.form_price <=', $maxPrice);
					}
					if($order_price)
					{
						$this->db->order_by('tbl_three_forms.form_price',$order_price);
					}
					if($order_name)
					{
						$this->db->order_by('tbl_three_forms.form_title',$order_name);
					}
					$this->db->select('formsID AS id,form_title AS title,form_image as image,form_duration,form_price AS price,form_bird_discount AS discount,form_added_date AS addeddate,(CASE WHEN (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND 	form_type="tours") IS NULL THEN 0 ELSE (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="tours") END ) AS rating');
				    $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
			        $resp['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active','tbl_three_forms.form_type'=>'Tours'));
					$resp['category']=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
				break;
				case 'adventure' :
				   $resp['message_type']='success';
				   $resp['message']='';
				   $categoryID='3';
				   $data['searchData']=array();
					
					$minPrice = $_REQUEST["minPrice"];
					$maxPrice = $_REQUEST["maxPrice"];
					$category = $_REQUEST["category"];
					$subcategory = $_REQUEST["subcategory"];
					
					//sort
					$order_price = $_REQUEST["order_price"];
					$order_star = $_REQUEST["order_star"];
					$order_user = $_REQUEST["order_user"];
					$order_name = $_REQUEST["order_name"];
		            $destination = $_REQUEST["destination"];

					$adult = $_REQUEST["adult"];
					$child = $_REQUEST["child"];
					
					$result=array();
		            if($category!='') {
						$categories=explode(',',$category);
						$catcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($categories as $cat)
						{
							if($catcounter==1)
							{$whereParam.="tbl_three_forms.categoryID='".$cat."'";}
							else
							{$whereParam.=" OR tbl_three_forms.categoryID='".$cat."'";}
							$catcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
		
					if($subcategory!='') {
						$subcategories=explode(',',$subcategory);
						$subcatcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($subcategories as $subcat)
						{
							if($subcatcounter==1)
							{$whereParam.="tbl_three_forms.subcategoryID='".$subcat."'";}
							else
							{$whereParam.=" OR tbl_three_forms.subcategoryID='".$subcat."'";}
							$subcatcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
		
					if($destination!='')
					{
						$this->db->where('tbl_three_forms.form_departure_address', $destination);
					}
					if($minPrice!='' && $maxPrice!='' )
					{
						$this->db->where('tbl_three_forms.form_price >=', $minPrice);
						$this->db->where('tbl_three_forms.form_price <=', $maxPrice);
					}
					if($order_price)
					{
						$this->db->order_by('tbl_three_forms.form_price',$order_price);
					}
					if($order_name)
					{
						$this->db->order_by('tbl_three_forms.form_title',$order_name);
					}
					$this->db->select('formsID AS id,form_title AS title,form_image as image,form_duration,form_price AS price,form_bird_discount AS discount,form_added_date AS addeddate,(CASE WHEN (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="adventure") IS NULL THEN 0 ELSE (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="adventure") END ) AS rating');
				    $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
				    $resp['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active','tbl_three_forms.form_type'=>'Adventure'));
                    $resp['category']=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
				break;
				case 'recreations' :
				   $resp['message_type']='success';
				   $resp['message']='';
				   $categoryID='4';
				    $data['searchData']=array();
					
					$minPrice = $_REQUEST["minPrice"];
					$maxPrice = $_REQUEST["maxPrice"];
					$category = $_REQUEST["category"];
					$subcategory = $_REQUEST["subcategory"];
					
					//sort
					$order_price = $_REQUEST["order_price"];
					$order_star = $_REQUEST["order_star"];
					$order_user = $_REQUEST["order_user"];
					$order_name = $_REQUEST["order_name"];
		            $destination = $_REQUEST["destination"];

					$adult = $_REQUEST["adult"];
					$child = $_REQUEST["child"];
					
					$result=array();
		            if($category!='') {
						$categories=explode(',',$category);
						$catcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($categories as $cat)
						{
							if($catcounter==1)
							{$whereParam.="tbl_three_forms.categoryID='".$cat."'";}
							else
							{$whereParam.=" OR tbl_three_forms.categoryID='".$cat."'";}
							$catcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
		
					if($subcategory!='') {
						$subcategories=explode(',',$subcategory);
						$subcatcounter=1;
						$whereParam="";
						$whereParam.="(";
						foreach($subcategories as $subcat)
						{
							if($subcatcounter==1)
							{$whereParam.="tbl_three_forms.subcategoryID='".$subcat."'";}
							else
							{$whereParam.=" OR tbl_three_forms.subcategoryID='".$subcat."'";}
							$subcatcounter++;
						}
						$whereParam.=")";
						$this->db->where($whereParam);
					}
		
					if($destination!='')
					{
						$this->db->where('tbl_three_forms.form_departure_address', $destination);
					}
					if($minPrice!='' && $maxPrice!='' )
					{
						$this->db->where('tbl_three_forms.form_price >=', $minPrice);
						$this->db->where('tbl_three_forms.form_price <=', $maxPrice);
					}
					if($order_price)
					{
						$this->db->order_by('tbl_three_forms.form_price',$order_price);
					}
					if($order_name)
					{
						$this->db->order_by('tbl_three_forms.form_title',$order_name);
					}
					$this->db->select('formsID AS id,form_title AS title,form_image as image,form_duration,form_price AS price,form_bird_discount AS discount,form_added_date AS addeddate,(CASE WHEN (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="tours") IS NULL THEN 0 ELSE (SELECT avg(rating) FROM tbl_rating_master WHERE tbl_rating_master.fk_form_id=id AND form_type="tours") END ) AS rating');
				    $this->db->join('tbl_hosts_master','tbl_hosts_master.hosts_id=tbl_three_forms.hosts_id');
				    $resp['searchData']=$this->master_model->getRecords('tbl_three_forms',array('tbl_three_forms.form_status'=>'active','tbl_three_forms.form_type'=>'Recreation'));
				    $resp['category']=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
				break;
			}
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function subcategory()
	{
		$resp['result'] ='';
		$resp['data']=array();
		$resp['message_type']='fails';
		$resp['message']='some thing is wrong';
		$type = $_REQUEST['type'];
		$categoryID   = $_REQUEST['id'];
		$subcategory=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$categoryID,'category_status'=>'active','parent_id !='=>'0'));
		if(count($subcategory))
		{
			$resp['message_type']='success';
			$resp['message']='';
			$resp['subcategory']=$subcategory;
		}
		else
		{
		    $resp['message_type']='fails';
			$resp['message']='no records found !';
			$resp['subcategory']=$subcategory;	
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function forgotpassword()
	{
		$resp['result'] ='';
		$resp['user_message']="";
		$this->load->model('email_sending');
		if(isset($_REQUEST))
		{
			$email=$this->input->post('email',TRUE);
			$this->db->select('email,firstName,lastName');
			$userInfo=$this->master_model->getRecords('tbl_user_master',array('email'=>$email));
			$admin_email=$this->master_model->getRecords('tbl_admin_login',array('id'=>'1'));
			if(count($userInfo)!=0)
			{
				    $length = 6;
					$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
					$this->db->set('password',$randomString);
					$this->db->where('email',$email);
					$get_final=$this->db->update('tbl_user_master');
					if($get_final)
					{
						$info_arr=array('from'=>$admin_email[0]['admin_email'],
							            'to'=>$email,
							            'base_url'=>base_url(),
							            'subject'=>'Reset Password On Rehlaticket',
							            'view'=>'user-forgotpassword'
							           );
					    $other_info=array(
						                'base_url'=>base_url(),
							            'randomString' => $randomString,
							            'username'     => $userInfo[0]['firstName'].' '.$userInfo[0]['lastName'],
										'email'        => $email,
						                );
						$this->email_sending->sendmail($info_arr,$other_info);
						$resp['result']='success';
						$resp['user_message']="Password reset successful ! You will receive an email with your new password shortly. Don't forget to check your spam box!";
				    }
				}
			    else
			    {
				  $resp['result']='fail';
				  $resp['user_message']='Please enter your registered email address to receive an alternate password.';
			    }
		 }
		echo str_replace('\/','/',json_encode($resp));
	}
	public function profile()
	{
		  $resp['result'] ='';
		  $resp['data_user']='';
		  $resp['data_country']='';
		  $resp['user_message']="";
		  $fetch_array=$this->master_model->getRecords('tbl_user_master',array('userID'=>$_REQUEST['userID']));
		  $country_array=$this->master_model->getRecords('tbl_countries','','',array('name'=>'ASC'));
		  $resp['data_user']= $fetch_array;
		  $resp['data_country']=$country_array;
		  if(isset($_REQUEST))
		  {
			  $this->form_validation->set_rules('firstName','First Name','required|xss_clean');
			  $this->form_validation->set_rules('lastName','Last Name','required|xss_clean');
			  $this->form_validation->set_rules('email','Email','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $firstname=$this->input->post('firstName',true);
				  $lastname=$this->input->post('lastName',true);
				  $email=$this->input->post('email',true);
				  $contactNumber=$this->input->post('contactNumber',true);
				  $day=$this->input->post('day',true);
				  $month=$this->input->post('month',true);
				  $year=$this->input->post('year',true);
				  $dob=$year.'-'.$month.'-'.$day;
				  $address=$this->input->post('address',true);
				  $zipCode=$this->input->post('zipCode',true);
				  $city=$this->input->post('city',true);
				  $country=$this->input->post('country',true);
				  $state=$this->input->post('state',true);
				  $oldimg=$this->input->post('oldimg',true);
				  $this->load->library('upload'); 
				  $config=array('upload_path'=>'uploads/profile/',
								  'allowed_types'=>'jpg|jpeg|gif|png',
								  'file_name'=>rand(1,9999),'max_size'=>0);
					$this->upload->initialize($config);
					
					if($_FILES['userImage']['name']!='')
					{
						if($this->upload->do_upload('userImage'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						}
						else
						{
							$file=$oldimg;
							$data['error']=$this->upload->display_errors();
						}
					}
					else
					{
						$file=$oldimg;
					}
	                 $input_array=array('firstName'=>$firstname,'lastName'=>$lastname,'email'=>$email,'dob'=>$dob,'address'=>$address,'zipCode'=>$zipCode,'city'=>$city,'countryId'=>$country,'state'=>$state,'userImage'=>$file,'contactNumber'=>$contactNumber);
					 if($this->master_model->updateRecord('tbl_user_master',$input_array,array('userID'=>$_REQUEST['userID'])))
					 {	
						$user_data=array('profile_pic'=>$file);
						$resp['result']='success';
					    $resp['user_message']='Your profile update successfully.';
					 }
					 else
					 {
						$resp['result']='fail';
					    $resp['user_message']='Oops ! something wrong. Please try sometimes leter.';
					 }
				}
			  else
			  {
			      $resp['result']='fail';
			      $resp['user_message']=$this->form_validation->error_string();
			  }
		  }
		  echo str_replace('\/','/',json_encode($resp));
	}
	public function userchangepassword()
	{
		$resp['result'] ='';
		$resp['data']=array();
		$resp['user_message']="";
		if(isset($_REQUEST))
		{
		   $this->form_validation->set_rules('oldPassword','Old password','required|xss_clean');
		   $this->form_validation->set_rules('newPassword','New Password','required|xss_clean');
		   $this->form_validation->set_rules('confirm_password','Confirm Password','required|xss_clean|matches[newPassword]|max_length[6]');
		   if($this->form_validation->run())
		   {
				$current_pass=$this->input->post('oldPassword',true);
				$new_pass=$this->input->post('newPassword',true);
				$row=$this->master_model->getRecordCount('tbl_user_master',array('userID'=>$_REQUEST['userID'],'password'=>$current_pass));
				if($row==0)
				{
					$resp['result']='fail';
					$resp['user_message']="Current Password is Wrong.";
				}
				else
				{
					$input_array=array('password'=>$new_pass);
					$this->master_model->updateRecord('tbl_user_master',$input_array,array('userID'=>$_REQUEST['userID']));
					$resp['result']='success';
					$resp['user_message']="Your Password Updated Successfully.";
				}
		   }
		   else
		   {
			  $resp['result']='fail';
			  $resp['user_message']=$this->form_validation->error_string();;
		   }	
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function searchboat()
	{
		$resp['result'] ='success';
		$resp['data']=array();
		$resp['user_message']="";
		if(!empty($_REQUEST['budget'])) 
		{
				$budget=$_REQUEST['budget'];
				$budgetcounter=1;
				$whereParambd="";
				$whereParambd.="(";
				foreach($budget as $bd)
				{
					$range=explode('-',$bd);
					//print_r($range);exit;
					if($budgetcounter==1)
					{
						if($range[1]=='10000')
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					} 
					else
					{
						if($range[1]=='10000')
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					}
					$budgetcounter++;
				}
				$whereParambd.=")";
				$this->db->where($whereParambd);
			}
		if(!empty($_REQUEST['type'])) 
		{
			$type=$_REQUEST['type'];
			$typecounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($type as $t)
			{
				if($typecounter==1)
				{$whereParam.="tbl_boat_master.boat_type_id=".$t;}
				else
				{$whereParam.=" OR tbl_boat_master.boat_type_id=".$t;}
				$typecounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['cabins'])) 
		{
			$cabins=$_REQUEST['cabins'];
			$cabinscounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($cabins as $c)
			{
				if($cabinscounter==1)
				{$whereParam.="tbl_boat_master.boat_cabins_id=".$c;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_cabins_id=".$c;}
				$cabinscounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['crew'])) 
		{
			$crew=$_REQUEST['crew'];
			$crewcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($crew as $cr)
			{
				if($crewcounter==1)
				{$whereParam.="tbl_boat_master.boat_crewtype_id=".$cr;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_crewtype_id=".$cr;}
				$crewcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['category'])) 
		{
			$category=$_REQUEST['category'];
			$categorycounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($category as $ca)
			{
				if($categorycounter==1)
				{$whereParam.="tbl_boat_master.boat_category_id=".$ca;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_category_id=".$ca;}
				$categorycounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_REQUEST['boat_location'])) 
		{
			$this->db->where('tbl_boatpackage_master.boat_location_to',trim($_REQUEST['boat_location']));
		}
		if(!empty($_REQUEST['boat_location_from'])) 
		{
			$this->db->where('tbl_boatpackage_master.boat_location',trim($_REQUEST['boat_location_from']));
		}
		if(!empty($_REQUEST['boat_location'])) 
		{
			$this->db->where('boat_location_to',trim($_REQUEST['boat_location']));
		}
		if(!empty($_REQUEST['boat_location_from'])) 
		{
			$this->db->where('boat_location',trim($_REQUEST['boat_location_from']));
		}
		$sort=array('tbl_boat_master.boat_id'=>'desc');
		if(!empty($_REQUEST['priceSort'])) 
		{
			$sort=array('tbl_boatpackage_master.adult_price'=>$_REQUEST['priceSort']);
		}
		if(!empty($_REQUEST['boatName'])) 
		{
			$sort=array('tbl_boatpackage_master.package_title'=>$_REQUEST['boatName']);
		}
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$resp['searchData']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boat_master.boat_status'=>'active'),'',$sort);
		echo str_replace('\/','/',json_encode($resp));
	}
}