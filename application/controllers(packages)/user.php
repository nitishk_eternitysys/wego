<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	
	public function __construct()
    {
	   parent::__construct();
	   
	   if($this->session->userdata('userID')=='')
		{
			redirect(base_url());
		}
	}
	public function profile()
	{
	  $this->load->library('upload');		
	  $data['error']='';
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['fetch_array']=$this->master_model->getRecords('tbl_user_master',array('user_id'=>$this->session->userdata('userID')));
	  $this->db->join('tbl_category_story','tbl_category_story.category_id=tbl_story_board.categoryID');
	  $data['fetch_story']=$this->master_model->getRecords('tbl_story_board',array('userID'=>$this->session->userdata('userID')));
	  $data['pagetitle']='Profile';
	  if(isset($_POST['btn_profile']))
	  {
		        $config1=array('upload_path'=>'uploads/profile/',
						       'allowed_types'=>'jpg|jpeg|gif|png|ico',
						       'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config1);
				if($_FILES['user_id_info']['name']!='')
				{
					if($this->upload->do_upload('user_id_info'))
					{
					  $dt=$this->upload->data();
					  $_POST['user_id_info']=$dt['file_name'];
					}
					else
					{
						$_POST['user_id_info']=$data['fetch_array'][0]['user_id_info'];	;
						$data['error']=$this->upload->display_errors();
					}
				}
				else
				{
				  $_POST['user_id_info']=$data['fetch_array'][0]['user_id_info'];	
				}
				if($this->master_model->updateRecordf('tbl_user_master',$_POST,array('user_id'=>$this->session->userdata('userID'))))
				{	
					
					$this->session->set_flashdata('success','Your profile update successfully.');
					redirect(base_url().'user/profile');
				}
				else
				{
					$data['error']='Oops ! something wrong. Please try sometimes leter.';
				}
			
	  }
	  if(isset($_POST['btn_password']))
	  {
		$this->form_validation->set_rules('oldPassword','Old password','required|xss_clean');
		$this->form_validation->set_rules('newPassword','New Password','required|xss_clean');
		$this->form_validation->set_rules('confirm_password','Confirm Password','required|xss_clean|matches[newPassword]|max_length[6]');
		  if($this->form_validation->run())
		  {
				$current_pass=$this->input->post('oldPassword',true);
				$new_pass=$this->input->post('newPassword',true);
				$row=$this->master_model->getRecordCount('tbl_user_master',array('user_id'=>$this->session->userdata('userID'),'password'=>$current_pass));
				if($row==0)
				{
					$data['error']="Current Password is Wrong.";
				}
				else
				{
					$input_array=array('password'=>$new_pass);
					$this->master_model->updateRecord('tbl_user_master',$input_array,array('user_id'=>$this->session->userdata('userID')));
					$this->session->set_flashdata('success','Your Password Updated Successfully.');
					redirect(base_url().'user/profile');
				}
		  }
		  else
		  {
			  $data['error']=$this->form_validation->error_string();
		  }
	  }
	  
	  /*stroy added 
	  if(isset($_POST['btn_story']))
	  {
		$this->form_validation->set_rules('story_category_id','Story cateory','required|xss_clean');
		$this->form_validation->set_rules('story_description','Story description','required|xss_clean');
	    if($this->form_validation->run())
		{
			    $_POST['story_user_id']=$this->session->userdata('userID');
				foreach($_POST as $key=>$val)
				{
					if($key!='btn_story')
					{
				     $array_key[]=$key;
				     $array_val[]=$val;   
					}
				}
				$input_array=array_combine($array_key, $array_val);
				if($story_id=$this->master_model->insertRecord('tbl_story_master',$input_array,true))
				{
				    $files = $_FILES;
					if(isset($files['main_image']['name']) && count($files['main_image']['name'])>0 && $files['main_image']['name'][0]!='')
					{
							$gallry_img=count($files['main_image']['name']); 
							$config=array('upload_path'=>'uploads/story/', 
										  'allowed_types'=>'jpg|jpeg|gif|png',
										  'file_name'=>rand(1,9999)
										  );
							$this->upload->initialize($config);
							$flag=1; 		
							for($i=0;$i<$gallry_img;$i++)
							{  
								if($files['main_image']['name'][$i]!='')
								{
									$_FILES['main_image']['name']	= rand(0,999999).str_replace(' ','',$files['main_image']['name'][$i]);
									$_FILES['main_image']['type']	= $files['main_image']['type'][$i];
									$_FILES['main_image']['tmp_name']= $files['main_image']['tmp_name'][$i];
									$_FILES['main_image']['error']	= $files['main_image']['error'][$i];
									$_FILES['main_image']['size']	= $files['main_image']['size'][$i];
									if($this->upload->do_upload('main_image'))
									{
										$dt=$this->upload->data();
										$file_name=$dt['file_name'];
									    $this->master_model->insertRecord('tbl_story_images',array('story_id'=>$story_id,'image_name'=>$file_name,'user_id'=>$this->session->userdata('userID')));
								    }
								}
							}
					   } 
					$this->session->set_flashdata('success','Record insert successfully.');
			       redirect(base_url().''.$this->router->fetch_class().'/profile/');	
				}
				
		}
		else
		{
		  $data['error']=$this->form_validation->error_string();
		}
	  }
	  $this->db->join('tbl_category_story','tbl_category_story.category_id=tbl_story_master.story_category_id');
	  $data['story_fetch']=$this->master_model->getRecords('tbl_story_master',array('tbl_story_master.story_user_id'=>$this->session->userdata('userID')),'',array('tbl_story_master.story_id'=>'desc')); 
	  /*story added */
	  $data['middle_content']='user-account';
	  $this->load->view('common-file',$data);
	}
	public function wishlistremove($package_id='')
	{
	    $package_id=base64_decode($package_id);
		if($package_id)
		{
			if($this->master_model->deleteRecord('tbl_wishlist_master','package_id',$package_id)) 
			{
			  $this->session->set_flashdata('success','Record deleted successfully.');
			  redirect(base_url().''.$this->router->fetch_class().'/profile/');	
			}
		}
	}
	public function storyremove($story_id ='')
	{
	    $story_id =base64_decode($story_id );
		if($story_id)
		{
			if($this->master_model->deleteRecord('tbl_story_board','storyID',$story_id)) 
			{
			  $this->master_model->deleteRecord('tbl_story_images','storyID',$story_id);
			  $this->session->set_flashdata('success','Record deleted successfully.');
			  redirect(base_url().''.$this->router->fetch_class().'/profile/');	
			}
		}
	}
	
	public function paymentApprove($id)
	{
		$id=base64_decode($id);
		$input_array=array('status'=>'approve');
		$this->master_model->updateRecord('tbl_transaction_master',$input_array,array('id'=>$id)); 
		$this->session->set_flashdata('success','Order approve successfully.');
		redirect(base_url().''.$this->router->fetch_class().'/profile/');	
	}
	
	public function orderDetails($id)
	{
		$id=base64_decode($id);
		$data['error']='';
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['fetch_array']=$this->master_model->getRecords('tbl_user_master',array('user_id'=>$this->session->userdata('userID')));
		$data['order']=$this->master_model->getRecords('tbl_transaction_master',array('id'=>$id));
		$data['pagetitle']='Order Details';
		
		$data['middle_content']='order-details';
	  	$this->load->view('common-file',$data);
	}
	
	
	/* This is for cancelation of order */
	public function cancelOrder()
	{
		$orderID=$this->uri->segment(3);
		$orderInfo=$this->master_model->getRecords('tbl_transaction_master',array('orderID'=>$orderID));
		if(count($orderInfo)>0)
		{
		   $cancel_reson = $this->input->post('textValue');	
		   if($orderInfo[0]['type']=='accom')
		   {	
		      $fetchInfo=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$orderInfo[0]['booking_form_id']));
		      $tranAmount  = $orderInfo[0]['tranAmount'] - ($orderInfo[0]['tranAmount'] * ($fetchInfo[0]['accom_cancel_policy_price'] / 100));
			  $policy_cancel= $fetchInfo[0]['accom_cancel_policy_price'];
		   }
		   else
		   {
			  $fetchInfo     = $this->master_model->getRecords('tbl_three_forms',array('formsID'=>$orderInfo[0]['booking_form_id']));
			  $tranAmount    = $orderInfo[0]['tranAmount'] - ($orderInfo[0]['tranAmount'] * ($fetchInfo[0]['form_cancel_before'] / 100));
			  $policy_cancel = $fetchInfo[0]['form_cancel_before'];
		   }
		   $this->master_model->updateRecordf('tbl_transaction_master',array('transReson'=>$cancel_reson,'transCancelAmount'=>$tranAmount,'transCancelPercentage'=>$policy_cancel,'status'=>'cancel'),array('orderID'=>$orderID));
		   echo 'success';
		}
		else
		{
			echo 'error';
		}
	}
	
	public function addStory()
	{
		$this->load->library('upload');
		$data['error']='';
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['fetch_array']=$this->master_model->getRecords('tbl_user_master',array('user_id'=>$this->session->userdata('userID')));
		$data['fetch_category']=$this->master_model->getRecords('tbl_category_story',array('category_status'=>'active'));
		
		if(isset($_POST['btn_story']))
		{
			$_POST['userID']=$this->session->userdata('userID');
			$_POST['dateAdded']=date('Y-m-d H:i:s'); 
			$_POST['story_status']='active'; 
			if($this->master_model->insertRecord('tbl_story_board',$_POST))
			{	
				$storyID=$this->db->insert_id();
				 $files = $_FILES;
				 if(isset($files['story_images']['name']) && count($files['story_images']['name'])>0 && $files['story_images']['name'][0]!='')
				 {
						$gallry_img=count($files['story_images']['name']); 
						$config=array('upload_path'=>'uploads/story/', 
									  'file_ext_tolower' =>TRUE,
									  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
									  'file_name'=>time().rand(1,9999)
									  );
						$this->upload->initialize($config);
						$flag=1; 		
						for($i=0;$i<$gallry_img;$i++)
						{  
							if($files['story_images']['name'][$i]!='')
							{
								$_FILES['story_images']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['story_images']['name'][$i]));
								$_FILES['story_images']['type']	= $files['story_images']['type'][$i];
								$_FILES['story_images']['tmp_name']= $files['story_images']['tmp_name'][$i];
								$_FILES['story_images']['error']	= $files['story_images']['error'][$i];
								$_FILES['story_images']['size']	= $files['story_images']['size'][$i];
								if($this->upload->do_upload('story_images'))
								{
									$dt=$this->upload->data();
									$file_name=$dt['file_name'];
									$_POST['storyID']=$storyID;
									$_POST['story_images']=$file_name;
									$_POST['dateAdded']=date('Y-m-d H:i:s');
									$this->master_model->insertRecord('tbl_story_images',$_POST);
								}
							}
						 }
				  }
				$this->session->set_flashdata('success','Your story added successfully.');
				redirect(base_url().'user/profile');
			}
			else
			{
				$data['error']='Oops ! something wrong. Please try sometimes leter.';
			}
		}
		$data['pagetitle']='Add Story Board';
		$data['middle_content']='add-story';
	  	$this->load->view('common-file',$data);
	}
	
	public function editStory($id)
	{
		$id=base64_decode($id);
		$this->load->library('upload');
		$data['error']='';
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['fetch_array']=$this->master_model->getRecords('tbl_user_master',array('user_id'=>$this->session->userdata('userID')));
		$data['fetch_story']=$this->master_model->getRecords('tbl_story_board',array('storyID'=>$id));
		$data['fetch_story_img']=$this->master_model->getRecords('tbl_story_images',array('storyID'=>$id));
		$data['fetch_category']=$this->master_model->getRecords('tbl_category_story',array('category_status'=>'active'));
		if(isset($_POST['btn_story']))
		{
			$_POST['userID']=$this->session->userdata('userID'); 
			$_POST['story_status']='active'; 
			if($this->master_model->updateRecordf('tbl_story_board',$_POST,array('storyID'=>$id)))
			{	
				$storyID=$id;
				 $files = $_FILES;
				 if(isset($files['story_images']['name']) && count($files['story_images']['name'])>0 && $files['story_images']['name'][0]!='')
				 {
						$gallry_img=count($files['story_images']['name']); 
						$config=array('upload_path'=>'uploads/story/', 
									  'file_ext_tolower' =>TRUE,
									  'allowed_types'=>'*', //jpg|jpeg|gif|png|JPG|PNG|JPEG
									  'file_name'=>time().rand(1,9999)
									  );
						$this->upload->initialize($config);
						$flag=1; 		
						for($i=0;$i<$gallry_img;$i++)
						{  
							if($files['story_images']['name'][$i]!='')
							{
								$_FILES['story_images']['name']	= rand(0,999999).str_replace(' ','',strtolower($files['story_images']['name'][$i]));
								$_FILES['story_images']['type']	= $files['story_images']['type'][$i];
								$_FILES['story_images']['tmp_name']= $files['story_images']['tmp_name'][$i];
								$_FILES['story_images']['error']	= $files['story_images']['error'][$i];
								$_FILES['story_images']['size']	= $files['story_images']['size'][$i];
								if($this->upload->do_upload('story_images'))
								{
									$dt=$this->upload->data();
									$file_name=$dt['file_name'];
									$_POST['storyID']=$storyID;
									$_POST['story_images']=$file_name;
									$this->master_model->insertRecord('tbl_story_images',$_POST);
								}
							}
						 }
				  }
				$this->session->set_flashdata('success','Your story added successfully.');
				redirect(base_url().'user/editStory/'.base64_encode($id));
			}
			else
			{
				$data['error']='Oops ! something wrong. Please try sometimes leter.';
			}
		}
		$data['pagetitle']='Edit Story Board';
		$data['middle_content']='edit-story';
	  	$this->load->view('common-file',$data);
	}
	
	public function deleteimage()
    {
		$dataId=$this->input->post('delete_id');
		  if($this->master_model->deleteRecord('tbl_story_images','storyImgID',$dataId))
		  {
			  echo 'success';
		  }
		  else
		  {
			  echo 'error';
		  }
	}
	
}