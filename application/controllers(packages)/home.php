<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	public function index()
	{
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Home';
	  if(isset($_POST['findTours']))
	  {
		  $mainCat=$this->input->post('mainCat',true);
		  $category=$this->input->post('category',true);
		  $subcategory=$this->input->post('subcategory',true);
		  $destination=$this->input->post('destination',true);
		  $adult=$this->input->post('adult',true);
		  $child=$this->input->post('child',true);
		  redirect(base_url().'search/result/'.$mainCat.'?category='.$category.'&subcategory='.$subcategory.'&destination='.$destination.'&adult='.$adult.'&child='.$child);
	  }
	  $data['acco_rooms']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_status'=>'active'),'MAX(accom_beds) as rooms,MAX(accom_guest_max) as members');
	  
	  $data['middle_content']='home';
	  $this->load->view('common-file',$data);
	}
	public function page($page_id='')
	{
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Home';
	  $data['middle_content']='pages';
	  $data['fetch_array']=$this->master_model->getRecords('tbl_front_page',array('front_id'=>$page_id));
	  $this->load->view('common-file',$data);
	}
	public function login()
	{
      $this->load->library('upload');	
	  $data['error']=$data['error_reg']='';
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Login/Registration';
	  $data['middle_content']='login';
	  $this->load->model('email_sending');
	  if(isset($_POST['btn_register']))
	  {
		
		  	    $firstname=$_POST['user_name'];
				$email=$_POST['user_email'];
				$password=$_POST['user_password'];
				
				$config1=array('upload_path'=>'uploads/profile/',
						       'allowed_types'=>'jpg|jpeg|gif|png|ico',
						       'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config1);
				if($_FILES['user_id_info']['name']!='')
				{
					if($this->upload->do_upload('user_id_info'))
					{
					  $dt=$this->upload->data();
					  $_POST['user_id_info']=$dt['file_name'];
					}
					else
					{
						$_POST['user_id_info']='';
						$data['error']=$this->upload->display_errors();
					}
				}

				if($this->master_model->insertRecord('tbl_user_master',$_POST))
				{
					$userId=$this->db->insert_id();
					$whr=array('id'=>'1');
					$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
					$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$email,'subject'=>'User Successfully Registered','view'=>'user-registration');
					$other_info=array('firstname'=>$firstname,'email'=>$email,'password'=>$password,'userId'=>$userId);
					$this->email_sending->sendmail($info_arr,$other_info);
					
					$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
					$other_info1=array('firstname'=>$firstname,'email'=>$email,'lastname'=>'');
					$this->email_sending->sendmail($info_arr1,$other_info1);
					
					$this->session->set_flashdata('success','You are Register successfully. Please login to continue.');
					redirect(base_url().'home/login');
				}
				else
				{
					$data['error_reg']='Oops ! something wrong. Please try sometimes leter.';
				}
			
	  }
	  if(isset($_POST['btn_login']))
	  {
		  
		  $this->form_validation->set_rules('email','Email','required|xss_clean');
		  $this->form_validation->set_rules('password','Password','required|xss_clean');
			if($this->form_validation->run())
			{
			   $email=$this->input->post('email',true);
			   $password=$this->input->post('password',true);
			   $valid=$this->master_model->getRecords('tbl_user_master',array('user_email'=>$email,'user_password'=>$password));
				if(count($valid)>0)
				{
					if($valid[0]['user_email_verify']=='yes')
					{
						if($valid[0]['user_status']=='active')
						{
							$user_data=array('userID'=>$valid[0]['user_id'],
									 		'userEmail'=>$valid[0]['user_email'],
											'userFName'=>$valid[0]['user_name'],
											'userType'=>'user'
											 );
							$this->session->set_userdata($user_data);
							if($this->session->userdata('redirectTo')!='')
							{
								redirect(base_url($this->session->userdata('redirectTo')));
							}
							else
							{
								redirect(base_url('user/profile'));
							}
						}
						else
						{
							$data['error']='Sorry! you are block by admin.';
						}
					}
					else
					{
						$data['error']='Your account is not verified yet. Please verify your account from your email address with confirmation link';
					}
				}
				else
				{
					$data['error']='Invalid credentials.';
				}
			}
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
	  }
	  $data['categories']=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active'));
	  $this->load->view('common-file',$data);
	}
	public function registration()
	{
	  $this->load->library('upload');	
	  $data['error']=$data['error_reg']='';
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Login/Registration';
	  $data['middle_content']='registration';
	  $this->load->model('email_sending');
	  if(isset($_POST['btn_register']))
	  {
		
		  	    $firstname=$_POST['user_name'];
				$email=$_POST['user_email'];
				$password=$_POST['user_password'];
				
				$config1=array('upload_path'=>'uploads/profile/',
						       'allowed_types'=>'jpg|jpeg|gif|png|ico',
						       'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config1);
				if($_FILES['user_id_info']['name']!='')
				{
					if($this->upload->do_upload('user_id_info'))
					{
					  $dt=$this->upload->data();
					  $_POST['user_id_info']=$dt['file_name'];
					}
					else
					{
						$_POST['user_id_info']='';
						$data['error']=$this->upload->display_errors();
					}
				}

				if($this->master_model->insertRecord('tbl_user_master',$_POST))
				{
					$userId=$this->db->insert_id();
					$whr=array('id'=>'1');
					$info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
					$info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$email,'subject'=>'User Successfully Registered','view'=>'user-registration');
					$other_info=array('firstname'=>$firstname,'email'=>$email,'password'=>$password,'userId'=>$userId);
					$this->email_sending->sendmail($info_arr,$other_info);
					
					$info_arr1=array('from'=>$info_mail[0]['admin_email'],'to'=>$info_mail[0]['admin_email'],'subject'=>'New User Successfully Registered','view'=>'user-registration-to-admin');
					$other_info1=array('firstname'=>$firstname,'email'=>$email,'lastname'=>'');
					$this->email_sending->sendmail($info_arr1,$other_info1);
					
					$this->session->set_flashdata('success','You are Register successfully. Please login to continue.');
					redirect(base_url().'home/registration');
				}
				else
				{
					$data['error_reg']='Oops ! something wrong. Please try sometimes leter.';
				}
			
	  }
	  $data['categories']=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active'));
	  $this->load->view('common-file',$data);	
	}
	public function verify()
	{
		$userID=base64_decode($this->uri->segment(3));
		if($userID)
		{
			$input_array=array('user_email_verify'=>'yes');
			$this->master_model->updateRecord('tbl_user_master',$input_array,array('user_id'=>$userID)); 
			$this->session->set_flashdata('success','Thank you. you are verified.');
			redirect(base_url().'home/login');
		}
		else
		{
			redirect(base_url());
		}
	}
	public function logout()
	{
		$this->session->unset_userdata('userID');
		$this->session->unset_userdata('userEmail');
		$this->session->unset_userdata('userFName');
		$this->session->unset_userdata('userLName');
		$this->session->unset_userdata('profile_pic');
	    redirect(base_url(), 'refresh');
	}
	public function contactus()
	{
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Contact us';
	  $data['middle_content']='contact-us';
	  $this->load->view('common-file',$data);
	}
	public function langchange($lang='')
	{
		if($lang=='eng')
		{$this->session->set_userdata('site_lang','english');$this->session->set_userdata('form_lang',''); }
		else
	    {$this->session->set_userdata('site_lang','arbic');$this->session->set_userdata('form_lang','_arb'); }
		$this->load->library('user_agent');
		if($lang!='')
		{
			$this->session->set_userdata('lang',$lang);
			if($this->agent->is_referral())
			{redirect($this->agent->referrer());}
			else 
			{redirect(base_url());}
	    }
	}
	public function whychoose($chooseid='')
	{
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['pagetitle']='Why choose us';
	  $data['middle_content']='why-choose';
	  $data['fetch_array']=$this->master_model->getRecords('tbl_whychooseus_master',array('whychoose_id'=>$chooseid));
	  $this->load->view('common-file',$data);	
	}
	public function details($chooseid='')
	{
		
	  $data['pagetitle']='Accommodation details';
	  $data['fetch_array_details']=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>35));	
	  $data['middle_content']='accomm-details';
	  $this->load->view('common-file',$data);	
	}
	public function datecheck()
	{
		 // Start date
		 $date = '2009-12-06';
		 $end_date = '2009-12-13';
		 $getDays=array('Monday','Sunday');
		 while (strtotime($date) <= strtotime($end_date)) 
		 {
		   echo $date1 = date ("l",strtotime($date));
		    if(in_array($date1,$getDays))
			{
				 echo  date ("d",strtotime($date));
			}
		   $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
		 }
	}
	public function storyBoard($catID='')
	{
		
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	   $this->db->join('tbl_category_story','tbl_category_story.category_id=tbl_story_board.categoryID'); 
		if($catID!='')
		{
			$catID=base64_decode($catID);
			$this->db->where('categoryID',$catID);
		}
	  $data['fetch_story']=$this->master_model->getRecords('tbl_story_board',array('story_status'=>'active'),'',array('storyID'=>'DESC'));
	  $data['fetch_category']=$this->master_model->getRecords('tbl_category_story',array('category_status'=>'active'),'',array('category_id'=>'DESC'));
	  $data['pagetitle']='Story Board';
	  $data['middle_content']='story-board';
	  $this->load->view('common-file',$data);
	}
	public function storyDetails($id)
	{
		$id=base64_decode($id);
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$this->db->join('tbl_category_story','tbl_category_story.category_id=tbl_story_board.categoryID');
		$data['fetch_story']=$this->master_model->getRecords('tbl_story_board',array('story_status'=>'active','storyID'=>$id));
		$data['pagetitle']='Story Board Details';
		$data['middle_content']='story-board-details';
		$this->load->view('common-file',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */