<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Storycategory extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	}
	public function manageStorycategory()
	{
	   $data['pagetitle']='Rehla ticket | Categories';
	   if(isset($_POST['multiple_delete']))
	   {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_category_story','category_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/storycategory/manageStorycategory');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/storycategory/manageStorycategory');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/storycategory/manageStorycategory');
			}
		}
	  if(isset($_POST['blockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='0';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_category_story',array('category_status'=>$stat),array('category_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/storycategory/manageStorycategory');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/storycategory/manageStorycategory');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/storycategory/manageStorycategory');
			}
			
		}
		if(isset($_POST['unblockmultiple']))
		{
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='1';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_category_story',array('category_status'=>$stat),array('category_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/storycategory/manageStorycategory');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/storycategory/manageStorycategory');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/storycategory/manageStorycategory');
			}
		}
	   $data['categories']=$this->master_model->getRecords('tbl_category_story');
	  $data['middle_content']='manage-story-category';
	  $this->load->view('admin/common-file',$data);
	}
	public function statusStoryCategory($categorystatus,$category_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('category_status'=>$categorystatus);
		if($this->master_model->updateRecord('tbl_category_story',$input_array,array('category_id'=>$category_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/storycategory/manageStorycategory/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/storycategory/manageStorycategory/');
		}
	}
	public function deleteStoryCategory($category_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_category_story','category_id',$category_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/storycategory/manageStorycategory/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/storycategory/manageStorycategory/');
	    }
	}
	public function addStoryCategory()
	{
		   $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | story Categories';
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('category_name_eng','Category Name English','required|xss_clean|is_unique[tbl_category_story.category_name_eng]');
			  $this->form_validation->set_rules('category_name_arb','Category Name Arbic','required|xss_clean|is_unique[tbl_category_story.category_name_arb]');
			  $this->form_validation->set_rules('category_status','Status','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $category_name_eng=$this->input->post('category_name_eng',true);
				  $category_name_arb=$this->input->post('category_name_arb',true);
				  $category_status=$this->input->post('category_status',true);
				  $category_slug=$this->master_model->create_slug($category_name_eng,'tbl_category_story','category_slug');
				  $input_array=array('category_name_eng'=>addslashes($category_name_eng),'category_name_arb'=>addslashes($category_name_arb),'category_status'=>$category_status,'category_slug'=>$category_slug);
				   if($this->master_model->insertRecord('tbl_category_story',$input_array))
				  { 
					  $this->session->set_flashdata('success','Record Added successfully.');
					  redirect(base_url().'superadmin/storycategory/addStoryCategory/');
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Error while Adding record.'); 
					  redirect(base_url().'superadmin/storycategory/addStoryCategory/');
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='add-story-category';
		  $this->load->view('admin/common-file',$data);
	}
	public function updateStoryCategory()
	{
		   $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Categories';
		  $category_id=$this->uri->segment('4');
		   $data['categories']=$this->master_model->getRecords('tbl_category_story',array('category_id'=>$category_id));
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('category_name_eng','Category Name English','required|xss_clean');
			  $this->form_validation->set_rules('category_name_arb','Category Name Arbic','required|xss_clean');
			  $this->form_validation->set_rules('category_status','Status','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $category_name_eng=$this->input->post('category_name_eng',true);
				  $category_name_arb=$this->input->post('category_name_arb',true);
				  $category_status=$this->input->post('category_status',true);
				  $categorycheck=$this->master_model->getRecords('tbl_category_story',array('category_name_eng'=>$category_name_eng,'category_name_arb'=>$category_name_arb,'category_id !='=>$category_id));
				  if(count($categorycheck)==0)
				  {
					  $category_slug=$this->master_model->create_slug($category_name_eng,'tbl_category_story','category_slug','category_id',$category_id);
					  $input_array=array('category_name_eng'=>addslashes($category_name_eng),'category_name_arb'=>addslashes($category_name_arb),'category_status'=>$category_status,'category_slug'=>$category_slug);
					   if($this->master_model->updateRecord('tbl_category_story',$input_array,array('category_id'=>$category_id)))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/storycategory/updateStoryCategory/'.$category_id);
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/storycategory/updateStoryCategory/'.$category_id);
					  }
				  }
				  else
				  {
					 $this->session->set_flashdata('error','This category name already exist !'); 
					 redirect(base_url().'superadmin/storycategory/updateStoryCategory/'.$category_id);  
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-story-category';
		  $this->load->view('admin/common-file',$data);
	}
	
}