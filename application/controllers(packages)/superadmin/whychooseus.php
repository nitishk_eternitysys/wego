<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Whychooseus extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Testimonial';
	  $data['pageLable']='Why choose us';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_whychooseus_master','whychoose_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_whychooseus_master',array('whychoose_status'=>$stat),array('whychoose_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_whychooseus_master',array('whychoose_status'=>$stat),array('whychoose_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/testimonial/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/testimonial/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/testimonial/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_whychooseus_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$whychoose_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('whychoose_status'=>$status);
		if($this->master_model->updateRecord('tbl_whychooseus_master',$input_array,array('whychoose_id'=>$whychoose_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($whychoose_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_whychooseus_master','whychoose_id',$whychoose_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Testimonial';
		  $data['pageLable']='Why choose us';
		  if(isset($_POST['btn_category']))
		  {
			  $this->form_validation->set_rules('whychoose_title_eng','Why choose us  english title','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_title_arb','why choose us  arbic title','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_short_desc_eng','why choose us english short description','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_short_desc_arb','why choose us arbic short description','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_desc_eng','why choose us english description','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_desc_arb','why choose us arbic description','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  
				  $whychoose_title_eng=$this->input->post('whychoose_title_eng',true);
				  $whychoose_title_arb=$this->input->post('whychoose_title_arb',true);
				  $whychoose_short_desc_eng=$this->input->post('whychoose_short_desc_eng',true);
				  $whychoose_short_desc_arb=$this->input->post('whychoose_short_desc_arb',true);
				  $whychoose_desc_eng=$this->input->post('whychoose_desc_eng',true);
				  $whychoose_desc_arb=$this->input->post('whychoose_desc_arb',true);
				  
				  $whychoose_status='active';
				  $input_array=array('whychoose_title_eng'=>addslashes($whychoose_title_eng),'whychoose_title_arb'=>$whychoose_title_arb,'whychoose_short_desc_eng'=>$whychoose_short_desc_eng,'whychoose_short_desc_arb'=>$whychoose_short_desc_arb,'whychoose_desc_eng'=>$whychoose_desc_eng,'whychoose_desc_arb'=>$whychoose_desc_arb,'whychoose_status'=>$whychoose_status);
				  if($this->master_model->insertRecord('tbl_whychooseus_master',$input_array))
				  { 
				      $this->session->set_flashdata('success','Record Added successfully.');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Error while Adding record.'); 
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($whychoose_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Testimonial';
		  $data['pageLable']='Why choose us';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_whychooseus_master',array('whychoose_id'=>$whychoose_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('whychoose_title_eng','Why choose us  english title','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_title_arb','why choose us  arbic title','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_short_desc_eng','why choose us english short description','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_short_desc_arb','why choose us arbic short description','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_desc_eng','why choose us english description','required|xss_clean');
			  $this->form_validation->set_rules('whychoose_desc_arb','why choose us arbic description','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  
				  $whychoose_title_eng=$this->input->post('whychoose_title_eng',true);
				  $whychoose_title_arb=$this->input->post('whychoose_title_arb',true);
				  $whychoose_short_desc_eng=$this->input->post('whychoose_short_desc_eng',true);
				  $whychoose_short_desc_arb=$this->input->post('whychoose_short_desc_arb',true);
				  $whychoose_desc_eng=$this->input->post('whychoose_desc_eng',true);
				  $whychoose_desc_arb=$this->input->post('whychoose_desc_arb',true);
				  $whychoose_status='active';
				  $input_array=array('whychoose_title_eng'=>addslashes($whychoose_title_eng),'whychoose_title_arb'=>$whychoose_title_arb,'whychoose_short_desc_eng'=>$whychoose_short_desc_eng,'whychoose_short_desc_arb'=>$whychoose_short_desc_arb,'whychoose_desc_eng'=>$whychoose_desc_eng,'whychoose_desc_arb'=>$whychoose_desc_arb,'whychoose_status'=>$whychoose_status);
				  if($this->master_model->updateRecord('tbl_whychooseus_master',$input_array,array('whychoose_id'=>$whychoose_id)))
				  { 
					$this->session->set_flashdata('success','Record updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$whychoose_id);
				  }
				  else
				  {
					$this->session->set_flashdata('error','Error while Adding record.'); 
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$whychoose_id);
				  }
			 }
			 else
			 {
				$data['error']=$this->form_validation->error_string();
			 }
	   }
	   $data['middle_content']='edit-'.$this->router->fetch_class();
	   $this->load->view('admin/common-file',$data);
	}
}