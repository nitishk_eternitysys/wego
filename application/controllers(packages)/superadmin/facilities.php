<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Facilities extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Facilities';
	  $data['pageLable']='Facilities';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_facilities_master','facilities_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_facilities_master',array('facilities_status'=>$stat),array('facilities_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_facilities_master',array('facilities_status'=>$stat),array('facilities_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_facilities_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$facilities_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('facilities_status'=>$status);
		if($this->master_model->updateRecord('tbl_facilities_master',$input_array,array('facilities_id'=>$facilities_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($facilities_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_facilities_master','facilities_id',$facilities_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Facilities';
		  $data['pageLable']='Facilities';
		  if(isset($_POST['btn_add']))
		  {
			  $this->form_validation->set_rules('facilities_name_eng','Facilities english','required|xss_clean|is_unqie');
			  $this->form_validation->set_rules('facilities_name_arb','Facilities Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				 
				  $facilities_name_eng=$this->input->post('facilities_name_eng',true);
				  $facilities_name_arb=$this->input->post('facilities_name_arb',true);
				  $facilities_font='';
				  
				  if($this->input->post('facilities_allowed'))
				  {$facilities_allowed=$this->input->post('facilities_allowed');}
				  else
				  {$facilities_allowed='no'; }
				  
				  if($this->input->post('facilities_not_allowed'))
				  {$facilities_not_allowed=$this->input->post('facilities_not_allowed');}
				  else
				  {$facilities_not_allowed='no';}
				  
				  $facilities_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_facilities_master',array('facilities_name_eng'=>$facilities_name_eng,'facilities_name_arb'=>$facilities_name_arb));
				  if($checkDub==0)
				  {
					 $input_array=array('facilities_name_eng'=>addslashes($facilities_name_eng),'facilities_name_arb'=>$facilities_name_arb,'facilities_status'=>$facilities_status,'facilities_font'=>$facilities_font,'facilities_not_allowed'=>$facilities_not_allowed,'facilities_allowed'=>$facilities_allowed);
					  if($this->master_model->insertRecord('tbl_facilities_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					  $data['error']='Facilities already exist !';  
				  }
				  
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($facilities_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Facilities';
		  $data['pageLable']='Facilities';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_facilities_master',array('facilities_id'=>$facilities_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('facilities_name_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('facilities_name_arb','Testimonial Description Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $facilities_name_eng=$this->input->post('facilities_name_eng',true);
				  $facilities_name_arb=$this->input->post('facilities_name_arb',true);
				  $facilities_font=$this->input->post('facilities_font',true);
				  $facilities_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_facilities_master',array('facilities_name_eng'=>$facilities_name_eng,'facilities_name_arb'=>$facilities_name_arb,'facilities_id !='=>$facilities_id));
				  if($checkDub==0)
				  {
					  $facilities_font='';
				      if($this->input->post('facilities_allowed'))
					  {$facilities_allowed=$this->input->post('facilities_allowed');}
					  else
					  {$facilities_allowed='no'; }
					  
					  if($this->input->post('facilities_not_allowed'))
					  {$facilities_not_allowed=$this->input->post('facilities_not_allowed');}
					  else
					  {$facilities_not_allowed='no';}
					 
					  $input_array=array('facilities_name_eng'=>addslashes($facilities_name_eng),'facilities_name_arb'=>$facilities_name_arb,'facilities_status'=>$facilities_status,'facilities_font'=>$facilities_font,'facilities_allowed'=>$facilities_allowed,'facilities_not_allowed'=>$facilities_not_allowed);
					  if($this->master_model->updateRecord('tbl_facilities_master',$input_array,array('facilities_id'=>$facilities_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$facilities_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$facilities_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$facilities_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
}