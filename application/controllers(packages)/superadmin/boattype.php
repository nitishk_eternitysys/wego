<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Boattype extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Boat type';
	  $data['pageLable']='Boat type';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_boattype_master','boattype_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boattype_master',array('boattype_status'=>$stat),array('boattype_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boattype_master',array('boattype_status'=>$stat),array('boattype_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_boattype_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$boattype_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('boattype_status'=>$status);
		if($this->master_model->updateRecord('tbl_boattype_master',$input_array,array('boattype_id'=>$boattype_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($boattype_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_boattype_master','boattype_id',$boattype_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Boat type';
		  $data['pageLable']='Boat type';
		  if(isset($_POST['btn_add']))
		  {
			  $this->form_validation->set_rules('boattype_name_eng','boat type english','required|xss_clean|is_unqie');
			  $this->form_validation->set_rules('boattype_name_arb','boat type Arbic Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $testimonial_image='';
				  $boattype_name_eng=$this->input->post('boattype_name_eng',true);
				  $boattype_name_arb=$this->input->post('boattype_name_arb',true);
				 
				  $boattype_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_boattype_master',array('boattype_name_eng'=>$boattype_name_eng,'boattype_name_arb'=>$boattype_name_arb));
				  if($checkDub==0)
				  {
					  $boattype_slug=$this->master_model->create_slug($boattype_name_eng,'tbl_boattype_master','boattype_name_eng');
					  $input_array=array('boattype_name_eng'=>addslashes($boattype_name_eng),'boattype_name_arb'=>$boattype_name_arb,'boattype_status'=>$boattype_status,'boattype_slug'=>$boattype_slug);
					  if($this->master_model->insertRecord('tbl_boattype_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					  $data['error']='Boat type already exist !';  
				  }
				  
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($boattype_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Boat type';
		  $data['pageLable']='Boat type';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_boattype_master',array('boattype_id'=>$boattype_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('boattype_name_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('boattype_name_arb','Testimonial Description Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $boattype_name_eng=$this->input->post('boattype_name_eng',true);
				  $boattype_name_arb=$this->input->post('boattype_name_arb',true);
				  $boattype_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_boattype_master',array('boattype_name_eng'=>$boattype_name_eng,'boattype_name_arb'=>$boattype_name_arb,'boattype_id !='=>$boattype_id));
				  if($checkDub==0)
				  {
					  $boattype_slug=$this->master_model->create_slug($boattype_name_eng,'tbl_boattype_master','boattype_name_eng');
					  $input_array=array('boattype_name_eng'=>addslashes($boattype_name_eng),'boattype_name_arb'=>$boattype_name_arb,'boattype_status'=>$boattype_status,'boattype_slug'=>$boattype_status);
					  if($this->master_model->updateRecord('tbl_boattype_master',$input_array,array('boattype_id'=>$boattype_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$boattype_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$boattype_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$boattype_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
}