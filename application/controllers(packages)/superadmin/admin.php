<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();
	   $this->load->library('upload');  
	   $this->load->model('email_sending');	  
	   if( ! ini_get('date.timezone') )
		{
		   date_default_timezone_set('GMT');
		} 
	}
    public function login()
	{ 
	     $data['pagetitle']='Rehlaticket | Login';
		 if(isset($_POST['btn_login']))
		 {
			$this->form_validation->set_rules('username','','required|xss_clean');
			$this->form_validation->set_rules('password','','required|xss_clean');
			if($this->form_validation->run())
			{
				$username=$this->input->post('username',true);
				$password=$this->input->post('password',true);
				$input_array=array('admin_username'=>$username,'admin_password'=>$password);
				$user_info=$this->master_model->getRecords('tbl_admin_login',$input_array);
				if(count($user_info)>0)
				{ 
				
					$mysqltime=date("H:i:s");
					$user_data=array('admin_username'=>$user_info[0]['admin_username'],
									 'admin_id'=>$user_info[0]['id'],
									 'admin_img'=>$user_info[0]['admin_img'],
									 'timer'=>base64_encode($mysqltime));
					$this->session->set_userdata($user_data);
					redirect(base_url().'superadmin/admin/dashboard/');			
				}
				else
				{
					 $data['error']='Invalid username or password !';
				}
			}
		  }
	      $this->load->view('admin/login',$data);
	}
	
	public function forgotpassword()
	{
	  $data['pagetitle']='Rehlaticket | Forgotpassword';
	  $data['error']=$data['success']='';	
	  if(isset($_POST['btn_recovery']))
	  {
	    $this->form_validation->set_rules('email','','required|valid_email');
		if($this->form_validation->run())
		{
		  $admin_email=$this->input->post('email',true);
		  $result=$this->master_model->getRecords('tbl_admin_login',array('tbl_admin_login.admin_email'=>$admin_email),'tbl_admin_login.*');
		  if(count($result)>0)
		  {
			 $whr=array('id'=>'1');
			 $info_mail=$this->master_model->getRecords('tbl_admin_login',$whr,'*');
			 $info_arr=array('from'=>$info_mail[0]['admin_email'],'to'=>$admin_email,'subject'=>'Password Recovery','view'=>'admin-forgot-password');
			 $other_info=array('name'=>$result[0]['admin_username'],'email_id'=>base64_encode($info_mail[0]['id']));
			 if($this->email_sending->sendmail($info_arr,$other_info))
			 {
				$change_password=array('password_status'=>'0');	
				$this->master_model->updateRecord('tbl_admin_login',$change_password,array('id'=>'1'));	 
				$this->session->set_flashdata('success','Mail send successfully.');
				redirect(base_url().'superadmin/admin/forgotpassword/');	
			 }
			 else
			 {
				$this->session->set_flashdata('error','While error for sending mail');
				redirect(base_url().'superadmin/admin/forgotpassword/');	
			 }
		  }
		  else
		  {
			 $this->session->set_flashdata('error','Your email was not found.');
			 redirect(base_url().'superadmin/admin/forgotpassword/'); 
		  }
		}
	  }
	  $this->load->view('admin/login',$data);
	}
	/*
	  Function   :changepassword
	  Developer  :Dhananjay
	  Routes File:'academymaster/changepassword' used by 'academymaster/admin/changepassword'
	  Description:Admin can change the password by forgot password.    
	*/
	public function changepassword()
	{
	  $data['pagetitle']='Rehlaticket | Changepassword';
	  $data['error']=$data['success']=$data['error_alreay']='';
	  $result=$this->master_model->getRecords('tbl_admin_login',array('tbl_admin_login.id'=>1,'tbl_admin_login.password_status'=>'0'),'tbl_admin_login.*'); 
	  if(count($result)>0)
	  {
		  if(isset($_POST['btn_password']))
		  {
			 $this->form_validation->set_rules('password','','required');
			 $this->form_validation->set_rules('confirm_password','','required');
			 if($this->form_validation->run())
			 {
				$password=$this->input->post('password',true);
				$confirm_password=$this->input->post('confirm_password',true);
				$update_password=array('admin_password'=>$confirm_password);
				if($this->master_model->updateRecord('tbl_admin_login',$update_password,array('id'=>'1')))
				{
					$change_password=array('password_status'=>'1');	
				    $this->master_model->updateRecord('tbl_admin_login',$change_password,array('id'=>'1'));	
				    $data['success']='Password Change successfully'; 
				}
			 }
		  }
	  }
	  else
	  {
		$data['error_alreay']='Looks like you have already changed your password!';   
	  }
	  $this->load->view('admin/login',$data);
	}
	/*
	  Function   : logout
	  Developer  : Dhananjay
	  Description: Admin can logout session unset by admin.    
	*/
	public function logout()
	{
	  $this->session->unset_userdata('admin_id');
	  $this->session->unset_userdata('admin_username');
	   $this->session->unset_userdata('timer');
	  redirect(base_url().'superadmin/admin/login/');
	}
	/*
	  Function   : dashboard
	  Developer  : Dhananjay (show dashboard)
	  Routes File: 'academymaster/dashboard' used by 'academymaster/admin/dashboard'
	  Description: Admin can login and come to Dashboard.    
	*/
	public function dashboard()
	{
	  $data['success']=$data['error']='';	
	  $data['pagetitle']='Rehlaticket | Dashboard';
	  $data['middle_content']='dashboard';
	  $this->load->view('admin/common-file',$data);
	}
	/*
	  Function   : accountsetting
	  Developer  : Rushiraj
	  Routes File: 'academymaster/accountsetting' used by 'academymaster/admin/accountsetting'
	  Description: Admin can change email and site on off functionality.    
	*/
	public function accountsetting()
	{
	  $data['success']=$data['error']=$data['success1']=$data['error1']=$data['success2']=$data['error2']='';
	  $data['pagetitle']='Rehlaticket | Account Setting';	
	  if(isset($_POST['btn_account']))
	  {
		  $this->form_validation->set_rules('username','','required|xss_clean');
		  $this->form_validation->set_rules('email','','required|xss_clean|valid_email');
		   $this->form_validation->set_rules('phone','','required|xss_clean');
		  if($this->form_validation->run())
		  {
			 	$username=$this->input->post('username',true);
				$email=$this->input->post('email',true);
				$phone=$this->input->post('phone',true);
				$fax=$this->input->post('fax',true);
				$address=$this->input->post('address',true);
				$old_image=$this->input->post('old_image',true);
				$config=array('upload_path'=>'uploads/admin/',
					          'allowed_types'=>'jpg|jpeg|gif|png',
					          'file_name'=>rand(1,9999),'max_size'=>0);
			    $this->upload->initialize($config);
				
					if($_FILES['file_upload']['name']!='')
					{
						if($this->upload->do_upload('file_upload'))
						{
						  $dt=$this->upload->data();
						  $file=$dt['file_name'];
						}
						else
						{
							$file=$old_image;
							$data['error']=$this->upload->display_errors();
						}
					}
					else
					{
						$file=$old_image;
					}
					$input_array=array('admin_username'=>$username,'admin_email'=>$email,'admin_img'=>$file,'admin_phone'=>$phone,'admin_fax'=>$fax,'admin_address'=>$address);
					$this->master_model->updateRecord('tbl_admin_login',$input_array,array('id'=>'1'));
					if($_FILES['file_upload']['name']!='')
					{
						@unlink('uploads/admin/'.$old_image);
					}
					$user_data=array('admin_img'=>$file);
					$this->session->set_userdata($user_data);
					$data['success']='Record Updated Successfully.';
		  }
		  else
		  {
			$data['error']=$this->form_validation->error_string();
		  }
	  }
	  if(isset($_POST['btn_password']))
	  {
		$this->form_validation->set_rules('current_pass','','required|xss_clean');
		$this->form_validation->set_rules('new_pass','New Password','required|xss_clean');
		$this->form_validation->set_rules('confirm_pass','Confirm Password','required|xss_clean|matches[new_pass]|max_length[6]');
		  if($this->form_validation->run())
		  {
				$current_pass=$this->input->post('current_pass',true);
				$new_pass=$this->input->post('new_pass',true);
				$row=$this->master_model->getRecordCount('tbl_admin_login',array('admin_password'=>$current_pass));
				if($row==0)
				{
					$data['error1']="Current Password is Wrong.";
				}
				else
				{
					$input_array=array('admin_password'=>$new_pass);
					$this->master_model->updateRecord('tbl_admin_login',$input_array,array('id'=>'1'));
					$data['success1']='Password Updated Successfully.'; 
				}
		  }
		  else
		  {
			  $data['error1']=$this->form_validation->error_string();
		  }
	  }
	  if(isset($_POST['btn_status']))
	  {
		$site_status=$this->input->post('site_status',true);
		$input_array1=array('site_status'=>$site_status);
		$this->master_model->updateRecord('tbl_site_status',$input_array1,array('site_id'=>'1'));
	    $data['success2']='Site Status Changed Successfully.'; 
	  }
	  $data['result']=$this->master_model->getRecords('tbl_admin_login',array('tbl_admin_login.id'=>1),'tbl_admin_login.*');
	  $data['status']=$this->master_model->getRecords('tbl_site_status',array('site_id'=>1),'*');
	  $data['middle_content']='accountsetting';
	  $this->load->view('admin/common-file',$data);
	}
	/*
	  Function   : sociallink
	  Developer  : Dhananjay
	  Routes File: 'academymaster/sociallink' used by 'academymaster/admin/sociallink'
	  Description: Admin can update social link.    
	*/
	public function sociallink()
	{
	  $data['success']=$data['error']='';
	  $data['pagetitle']='Rehlaticket | Sociallink';	
	  $data['social_link']=$this->master_model->getRecords('tbl_social',array('tbl_social.social_id'=>1),'tbl_social.*');
	  if(isset($_POST['btn_social']))
	  {
		 $this->form_validation->set_rules('facebook_link','','required|xss_clean|valid_url');
		 $this->form_validation->set_rules('twitter_link','','required|xss_clean|valid_url'); 
		 $this->form_validation->set_rules('instagram','','required|xss_clean|valid_url');
		  $this->form_validation->set_rules('google_plus','','required|xss_clean|valid_url');
		 if($this->form_validation->run())
		 {
			 $facebook_link=$this->input->post('facebook_link',true);
			 $twitter_link=$this->input->post('twitter_link',true);
			 $instagram=$this->input->post('instagram',true);
			  $google_plus=$this->input->post('google_plus',true);
			 $update_link=array('facebook'=>$facebook_link,'instagram'=>$instagram,'twitter'=>$twitter_link,'google_plus'=>$google_plus);
		     if($this->master_model->updateRecord('tbl_social',$update_link,array('social_id'=>'1')))
			 {
				$this->session->set_flashdata('success','Social links updated successfully.'); 
				redirect(base_url().'superadmin/admin/sociallink/');
			 }
			 else
			 {
				$this->session->set_flashdata('error','Error while updating social link.'); 
				redirect(base_url().'superadmin/admin/sociallink/'); 
			 }
		}
	  }
	  $data['middle_content']='sociallink';
	  $this->load->view('admin/common-file',$data);
	}
	
	public function banner()
	{
		//upload first banner
		if(isset($_POST['btn_add_banner1']))
		{
			$this->form_validation->set_rules('banner_url1','Banner Url','required|xss_clean|valid_url');
			if($this->form_validation->run())
			{
			 	$banner_url1=$this->input->post('banner_url1',true);
			 	$old_image1=$this->input->post('old_image1',true);
				
				if($_FILES['file_upload1']['name']!="" && $_FILES['file_upload1']['error']==0)
				{
					$logo_config=array('file_name'=>uniqid(),
					'allowed_types'=>'jpg|jpeg|gif|png',
					'upload_path'=>'uploads/banner_image/');
					$this->upload->initialize($logo_config);
					if($this->upload->do_upload('file_upload1'))
					{ 
						$upload_data=$this->upload->data();
						$file_name=$upload_data['file_name'];
						$this->master_model->createThumb($file_name,'uploads/banner_image/',200,250);
						if($old_image1!="")
						{
							@unlink('uploads/banner_image/'.$old_logo);
							@unlink('uploads/banner_image/thumb/'.$old_logo);
						}
					}
				}
				else
				{$file_name=$old_image1;}
				$update_array=array('banner_url'=>$banner_url1,'banner_image'=>$file_name);
				
				if($this->master_model->updateRecord('tbl_banner_master',$update_array,array('banner_id'=>'1')))
				{
				$this->session->set_flashdata('success','Banner Updated successfully.'); 
				redirect(base_url().'superadmin/admin/banner/');
				}
			}
		}
		
		//upload second banner
		if(isset($_POST['btn_add_banner2']))
		{
			 	$this->form_validation->set_rules('banner_url2','Banner Ur2','required|xss_clean|valid_url');
			if($this->form_validation->run())
			{
			 	$banner_url2=$this->input->post('banner_url2',true);
			 	$old_image2=$this->input->post('old_image2',true);
				
				if($_FILES['file_upload2']['name']!="" && $_FILES['file_upload2']['error']==0)
				{
					$logo_config=array('file_name'=>uniqid(),
					'allowed_types'=>'jpg|jpeg|gif|png',
					'upload_path'=>'uploads/banner_image/');
					$this->upload->initialize($logo_config);
					if($this->upload->do_upload('file_upload2'))
					{ 
						$upload_data=$this->upload->data();
						$file_name=$upload_data['file_name'];
						$this->master_model->createThumb($file_name,'uploads/banner_image/',200,250);
						if($old_image2!="")
						{
							@unlink('uploads/banner_image/'.$old_logo);
							@unlink('uploads/banner_image/thumb/'.$old_logo);
						}
					}
				}
				else
				{$file_name=$old_image2;}
				$update_array=array('banner_url'=>$banner_url2,'banner_image'=>$file_name);
				if($this->master_model->updateRecord('tbl_banner_master',$update_array,array('banner_id'=>'2')))
				{
				$this->session->set_flashdata('success','Banner Updated successfully.'); 
				redirect(base_url().'superadmin/admin/banner/');
				}	 
			}
		}
		if(isset($_POST['btn_add_banner3']))
		{
			 	$this->form_validation->set_rules('banner_url3','Banner Url','required|xss_clean|valid_url');
			if($this->form_validation->run())
			{
			 	$banner_url3=$this->input->post('banner_url3',true);
			 	$old_image3=$this->input->post('old_image3',true);
				
				if($_FILES['file_upload3']['name']!="" && $_FILES['file_upload3']['error']==0)
				{
					$logo_config=array('file_name'=>uniqid(),
					'allowed_types'=>'jpg|jpeg|gif|png',
					'upload_path'=>'uploads/banner_image/');
					$this->upload->initialize($logo_config);
					if($this->upload->do_upload('file_upload3'))
					{ 
						$upload_data=$this->upload->data();
						$file_name=$upload_data['file_name'];
						$this->master_model->createThumb($file_name,'uploads/banner_image/',200,250);
						if($old_image3!="")
						{
							@unlink('uploads/banner_image/'.$old_logo);
							@unlink('uploads/banner_image/thumb/'.$old_logo);
						}
					}
				}
				else
				{$file_name=$old_image3;}
				$update_array=array('banner_url'=>$banner_url3,'banner_image'=>$file_name);
				if($this->master_model->updateRecord('tbl_banner_master',$update_array,array('banner_id'=>'3')))
				{
				$this->session->set_flashdata('success','Banner Updated successfully.'); 
				redirect(base_url().'superadmin/admin/banner/');
				}	 
			}
		}
		
		$data['banner_info']=$this->master_model->getRecords('tbl_banner_master');		
		$data['success']=$data['error']='';
		$data['pagetitle']='Rehlaticket | Banner';
		$data['middle_content']='add-banner';
		$this->load->view('admin/common-file',$data);	
	}
}