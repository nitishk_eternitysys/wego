<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Boat extends CI_Controller {
    public function __construct()
    {
	   parent::__construct();  
	   $this->load->library('upload');  
	}
	public function manage()
	{
	  $data['pagetitle']='Rehla ticket | Manage Boat';
	  $data['pageLable']='Manage Boat';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->deleteRecord('tbl_boat_master','boat_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
	  if(isset($_POST['blockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boat_master',array('boat_status'=>$stat),array('boat_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
			
		}
	  if(isset($_POST['unblockmultiple']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boat_master',array('boat_status'=>$stat),array('boat_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage');
			}
		}
		
	  $this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');	
	  $this->db->join('tbl_boattype_master','tbl_boattype_master.boattype_id=tbl_boat_master.boat_type_id');
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_boat_master');
	  $data['middle_content']='manage-'.$this->router->fetch_class();
	  $this->load->view('admin/common-file',$data);
	}
	public function status($status,$boat_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('boat_status'=>$status);
		if($this->master_model->updateRecord('tbl_boat_master',$input_array,array('boat_id'=>$boat_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
		}
	}
	public function delete($boat_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_boat_master','boat_id',$boat_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/manage/');
	    }
	}
	public function add()
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Manage Boat';
		  $data['pageLable']='Manage Boat';
		  if(isset($_POST['btn_add']))
		  {
			  $this->form_validation->set_rules('boattype_name_eng','Manage Boat english','required|xss_clean|is_unqie');
			  $this->form_validation->set_rules('boattype_name_arb','Manage Boat Arbic Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $testimonial_image='';
				  $boattype_name_eng=$this->input->post('boattype_name_eng',true);
				  $boattype_name_arb=$this->input->post('boattype_name_arb',true);
				 
				  $boat_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_boat_master',array('boattype_name_eng'=>$boattype_name_eng,'boattype_name_arb'=>$boattype_name_arb));
				  if($checkDub==0)
				  {
					  $boattype_slug=$this->master_model->create_slug($boattype_name_eng,'tbl_boat_master','boattype_name_eng');
					  $input_array=array('boattype_name_eng'=>addslashes($boattype_name_eng),'boattype_name_arb'=>$boattype_name_arb,'boat_status'=>$boat_status,'boattype_slug'=>$boattype_slug);
					  if($this->master_model->insertRecord('tbl_boat_master',$input_array))
					  { 
						  $this->session->set_flashdata('success','Record Added successfully.');
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
					  else
					  {
						  $this->session->set_flashdata('error','Error while Adding record.'); 
						  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/add/');
					  }
				  }
				  else
				  {
					  $data['error']='Manage Boat already exist !';  
				  }
				  
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
		  }
		  $data['middle_content']='add-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	public function update($boat_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Manage Boat';
		  $data['pageLable']='Manage Boat';
		  $data['fetch_single_arr']=$this->master_model->getRecords('tbl_boat_master',array('boat_id'=>$boat_id));
		  if(isset($_POST['btn_submit']))
		  {
			  $this->form_validation->set_rules('boattype_name_eng','Testimonial Description English','required|xss_clean');
			  $this->form_validation->set_rules('boattype_name_arb','Testimonial Description Arbic','required|xss_clean');
			  if($this->form_validation->run())
			  {
				  $boattype_name_eng=$this->input->post('boattype_name_eng',true);
				  $boattype_name_arb=$this->input->post('boattype_name_arb',true);
				  $boat_status='active';
				  $checkDub=$this->master_model->getRecordCount('tbl_boat_master',array('boattype_name_eng'=>$boattype_name_eng,'boattype_name_arb'=>$boattype_name_arb,'boat_id !='=>$boat_id));
				  if($checkDub==0)
				  {
					  $boattype_slug=$this->master_model->create_slug($boattype_name_eng,'tbl_boat_master','boattype_name_eng');
					  $input_array=array('boattype_name_eng'=>addslashes($boattype_name_eng),'boattype_name_arb'=>$boattype_name_arb,'boat_status'=>$boat_status,'boattype_slug'=>$boat_status);
					  if($this->master_model->updateRecord('tbl_boat_master',$input_array,array('boat_id'=>$boat_id)))
					  { 
						$this->session->set_flashdata('success','Record updated successfully.');
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$boat_id);
					  }
					  else
					  {
						$this->session->set_flashdata('error','Error while Adding record.'); 
						redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$boat_id);
					  }
				  }
				  else
				  {
					  $this->session->set_flashdata('error','Testimonial name is already exists !');
					  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/update/'.$boat_id);
				  }
			  }
			else
			{
				$data['error']=$this->form_validation->error_string();
			}
			  
		  }
		  $data['middle_content']='edit-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
	public function details($boat_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Manage Boat';
		  $data['pageLable']='Manage Boat';	
		  $this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');	
	      $data['arr_details']=$this->master_model->getRecords('tbl_boat_master',array('boat_id'=>$boat_id));
		  $data['middle_content']='details-'.$this->router->fetch_class();
		  $this->load->view('admin/common-file',$data);
	}
	
	public function packagedetails($package_id='')
	{
		  $data['success']=$data['error']='';
		  $data['pagetitle']='Rehla ticket | Manage Boat';
		  $data['pageLable']='Manage Boat';	
		  $this->db->select('tbl_boatpackage_master.*,tbl_boat_owner.*,tbl_boat_master.boat_name');	
		  $this->db->join('tbl_boat_master','tbl_boat_master.boat_id=tbl_boatpackage_master.boat_id');
		  $this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		  $data['arr_details']=$this->master_model->getRecords('tbl_boatpackage_master',array('tbl_boatpackage_master.package_id'=>$package_id));
		  $data['middle_content']='details-package';
		  $this->load->view('admin/common-file',$data);
	}
	public function package()
	{	
	  $data['pagetitle']='Rehla ticket | Manage Package';
	  $data['pageLable']='Manage Boat';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
					  $this->master_model->deleteRecord('tbl_boatpackage_master','package_id',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
			}
		}
	    if(isset($_POST['blockmultiple']))
	    {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boatpackage_master',array('status'=>$stat),array('package_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
			}
			
		}
	   if(isset($_POST['unblockmultiple']))
	   {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boatpackage_master',array('status'=>$stat),array('package_id'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package');
			}
	  }
	  $this->db->select('tbl_boatpackage_master.*,tbl_boat_owner.*,tbl_boat_master.boat_name');	
      $this->db->join('tbl_boat_master','tbl_boat_master.boat_id=tbl_boatpackage_master.boat_id');
	  $this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_boatpackage_master');
	  $data['middle_content']='manage-package';
	  $this->load->view('admin/common-file',$data);
	}
	public function packagestatus($status,$package_id)
	{
		$data['success']=$data['error']='';
		$input_array = array('status'=>$status);
		if($this->master_model->updateRecord('tbl_boatpackage_master',$input_array,array('package_id'=>$package_id)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package/');
		}
	
	}
	public function packagedelete($package_id)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_boatpackage_master','package_id',$package_id)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/package/');
	    }
	}
	public function rating()
	{	
	  $data['pagetitle']='Rehla ticket | Manage Rating';
	  $data['pageLable']='Manage Boat';
	  if(isset($_POST['multiple_delete']))
	  {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
					  $this->master_model->deleteRecord('tbl_boat_rating','rateID',$_POST['checkbox_del'][$i]);
					}
					$this->session->set_flashdata('success','Record(s) delete Successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
				}
				else
				{
					$this->session->set_flashdata('error','Select Record(s) to delete.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select Record(s) to delete.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
			}
		}
	    if(isset($_POST['blockmultiple']))
	    {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='block';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boat_rating',array('review_status'=>$stat),array('rateID'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to block.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to block.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
			}
			
		}
	   if(isset($_POST['unblockmultiple']))
	   {
			if(isset($_POST['checkbox_del']))
			{
				if(count($_POST['checkbox_del'])!= 0)
				{
					$cnt_checkbox_del=count($_POST['checkbox_del']); 
					$stat='active';
					for($i=0;$i<$cnt_checkbox_del;$i++)
					{
						$this->master_model->updateRecord('tbl_boat_rating',array('review_status'=>$stat),array('rateID'=>$_POST['checkbox_del'][$i]));
					}
					$this->session->set_flashdata('success','Record(s) status updated successfully.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
				}
				else
				{
					$this->session->set_flashdata('error','Select record(s) to unblock.');
					redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
				}
			}
			else
			{
				$this->session->set_flashdata('error','Select record(s) to unblock.');
				redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating');
			}
	  }
	  $this->db->select('tbl_boatpackage_master.*,tbl_boat_owner.*,tbl_boat_master.boat_name,tbl_boat_rating.*,tbl_user_master.firstName,tbl_user_master.lastName');	
      $this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.package_id=tbl_boat_rating.fk_package_id');
	  $this->db->join('tbl_boat_master','tbl_boat_master.boat_id=tbl_boatpackage_master.boat_id');
	  $this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
	  $this->db->join('tbl_user_master','tbl_user_master.userID=tbl_boat_rating.fk_user_id');
	  $data['fetch_arr']=$this->master_model->getRecords('tbl_boat_rating');
	  $data['middle_content']='manage-rating';
	  $this->load->view('admin/common-file',$data);
	}
	public function ratingstatus($status,$rateID)
	{
		$data['success']=$data['error']='';
		$input_array = array('review_status'=>$status);
		if($this->master_model->updateRecord('tbl_boat_rating',$input_array,array('rateID'=>$rateID)))
		{
	       $this->session->set_flashdata('success','Record status updated successfully.');
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating/');
		}
		else
		{
		   $this->session->set_flashdata('error','Error while updating status.'); 
		   redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating/');
		}
	}
	public function ratingdelete($rateID)
	{
		$data['success']=$data['error']='';
	  	if($this->master_model->deleteRecord('tbl_boat_rating','rateID',$rateID)) 
	  	{
		  $this->session->set_flashdata('success','Record deleted successfully.');
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating/');
 	    }
	 	else
	  	{
		  $this->session->set_flashdata('error','Error while deleting Record.'); 
		  redirect(base_url().'superadmin/'.$this->router->fetch_class().'/rating/');
	    }
	}
	
}