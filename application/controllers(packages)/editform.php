<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Editform extends CI_Controller {
	
	public function addaccom()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="accommodations_manage">Manage Accommodations</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Accommodations Information</h4>
          <div class="user-info-body">
        <form class="msform" id="accommodationsform" name="accommodationsform" method="post" enctype="multipart/form-data"> 
        <!-- progressbar -->
        <ul id="progressbaraccom" class="progressbar">
            <li class="active">Basic info</li>
            <li>Rooms &amp; beds</li>
            <li>Amenities</li>
            <li>Rules</li>
            <li>Location </li>
            <li class="big-text">Pricing & Cancelation</li>
            <li>Gallery :</li>
            <li>Contact info:</li>
        </ul>
        <!-- fieldsets -->
        <fieldset id="accom1" class="progressbaraccom">
        <h2 class="fs-title">Basic info</h2>
        <div class="col-md-6 col-sm-6"> 
           <input type="text" name="accom_title" placeholder="Titile" id="accom_title"  data-rule-required='true'/>
        </div>
        <div class="col-md-6 col-sm-6"> 
           <input type="text" name="accom_title_arb" placeholder="Arbic Titile" id="accom_title_arb"  data-rule-required='true' dir="rtl" />
        </div>
        <div class="col-md-6 col-sm-6 acc-input-file"> 
          <input type="file" name="accom_image1" placeholder="Titile" id="accom_image1"  data-rule-required='true' />
        </div>
        <div class="col-md-6 col-sm-6">
            <span class="allowed-span">Type</span> 
           <label class="labelcheck"><input name="accom_type" id="" value="accommodation" type="radio" checked="checked">Accoumadation </label>
           <label class="labelcheck"><input name="accom_type" id="" value="hall" type="radio">Hall </label>
        </div>
         <div style="clear:both;"></div>
        <div class="col-md-6 col-sm-6"> 
        <select name="accom_category_id" class="form-control" id="accom_category_id">
        <option selected="selected" value="">Select category</option>
        <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'1','parent_id'=>'0','category_status'=>'active')); 
        if(count($category_accom)>0)
        {
        foreach($category_accom as $row)
        {
        ?>
        <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
        <?php 
        }
        } ?>
        </select>
            </div>
        <div class="col-md-6 col-sm-6">    
        <select name="accom_subcategory_id" class="form-control" id="accom_subcategory_id">
        <option selected="selected" value="">Select subcategory</option>
        </select>
        </div>
       <!-- <div class="col-md-6 col-sm-6"> 
           <textarea name="accom_location" id="accom_location" class="form-control" placeholder="location"></textarea>
        </div>-->
        <div class="col-md-6 col-sm-6"> 
          <textarea name="accom_description" id="accom_description" class="form-control" placeholder="Description"></textarea>
        </div>
        <div class="col-md-6 col-sm-6"> 
         <textarea name="accom_description_arb" id="accom_description_arb" class="form-control" placeholder="Arabic Description" dir="rtl"></textarea>
        </div>
        <div style="clear:both;"></div>
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom2" class="progressbaraccom">
        <h2 class="fs-title">Rooms &amp; beds</h2>
        <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
        <div class="col-md-6 col-sm-6">  <input type="text" onkeypress="return OnlyNumericKeys(event);"  name="accom_bed_rooms" id="accom_bed_rooms" placeholder="Bed Rooms" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_beds" id="accom_beds" placeholder="Beds" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_bathrooms" id="accom_bathrooms" placeholder="Bathrooms" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_swimming_pool" id="accom_swimming_pool" placeholder="swimming pool" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_play_yards" id="accom_play_yards" placeholder="play yards" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_balcony" id="accom_balcony" placeholder="Balcony" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_garden" id="accom_garden" placeholder="garden" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_roof" id="accom_roof" placeholder="Roof" /></div>
        
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom3" class="progressbaraccom">
        <h2 class="fs-title">Amenities</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">TV</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn active amenities">
        <input type="radio"  value="yes"  name='accom_tv' id="accom_tv"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio"  value="no"  name='accom_tv' id="accom_tv1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_tv_yes" style="display:none;"><input type="text" name="accom_tv_number" id="accom_tv_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Play station</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" value="yes" class="amenities"  name='accom_playstation' id="accom_playstation"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" value="no" class="amenities"  name='accom_playstation' id="accom_playstation1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_playstation_yes" style="display:none;"><input type="text" name="accom_playstation_number" id="accom_playstation_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">WIFI</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes" rel="number" name='accom_wifi' id="accom_wifi"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no" rel="number" name='accom_wifi' id="accom_wifi1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_wifi_yes" style="display:none;"><input type="text" name="accom_wifi_number" id="accom_wifi_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">DVD</div>
        <div class="col-xs-4 " data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes" rel="number" name='accom_dvd' id="accom_dvd"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no" rel="number" name='accom_dvd' id="accom_dvd1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_dvd_yes" style="display:none;"><input type="text" name="accom_dvd_number" id="accom_dvd_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">GYM</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes" name='accom_gym' id="accom_gym"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no"  name='accom_gym' id="accom_gym1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_gym_yes" style="display:none;"><input type="text" name="accom_gym_number" id="accom_gym_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">kitchen</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes"  name='accom_kitchen' id="accom_kitchen"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no"  name='accom_kitchen' id="accom_kitchen1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_kitchen_yes" style="display:none;"><input type="text" name="accom_kitchen_number" onkeypress="return OnlyNumericKeys(event);" id="accom_kitchen_number" class="form-control"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Washing Machine</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" id="accom_washing_machine"  value="yes" rel="machine_numbe" name='accom_washing_machine'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" id="accom_washing_machine1" value="no" rel="machine_numbe" name='accom_washing_machine' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_washing_machine_yes" style="display:none;"><input type="text" name="accom_washing_machine_number" id="accom_washing_machine_number" onkeypress="return OnlyNumericKeys(event);" class="form-control"></div>
        </div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom4" class="progressbaraccom">
        <h2 class="fs-title">Rules</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Max gusts</div>
        <div class="col-xs-6">
        <input type="text" name="accom_guest_max" id="accom_guest_max" placeholder="Max guests" onkeypress="return OnlyNumericKeys(event);" />
        </div>
        </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
                <div class="col-xs-4">Check In</div>
                <div class="col-xs-6">
                   <input type="text" name="accom_check_in" id="accom_check_in" placeholder="Select check in time" />
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
          <div class="col-xs-4">Check Out</div>
           <div class="col-xs-6">
             <input type="text" name="accom_check_out" id="accom_check_out" placeholder="Select check in time" />
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
          <div class="col-xs-4">Reporting Time </div>
           <div class="col-xs-6">
             <input type="text" name="accom_reporting_time" id="accom_reporting_time" placeholder="Before the departure/Booked time" />
            </div>
          </div>
        </div> 
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">Allowed</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="accom_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
            <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">Not Allowed </span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="accom_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
        </div>    
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">What Included</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="accom_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
               <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">What not Included</span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="accom_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
              <span class="allowed-span">Requirement </span>
                   <?php 
				   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="accoum_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom8" class="progressbaraccom">
            <h2 class="fs-title">Location</h2>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location_address" id="accom_location_address_accom" placeholder="Location Address" />
            </div>
            <div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="accom_location_map" id="accom_location_accom" placeholder="Location" /></div>
            <div class="col-md-12 col-sm-12"> <div id="gmap_accom" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom5" class="progressbaraccom">
        <h2 class="fs-title">Pricing- discount </h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Price</div>
        <div class="col-xs-6">
        <input type="text" name="accom_price" id="accom_price" placeholder="Price"  onkeypress="return OnlyNumericKeys(event);" />
        </div>
        </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Reservation Amount </div>
                <div class="col-xs-6">
                <input type="text" name="accom_reservation_price" id="accom_reservation_price" placeholder="Reservation Amount"  onkeypress="return OnlyNumericKeys(event);" />
                </div>
           </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
                <div class="col-xs-4">Bird discount</div>
                <div class="col-xs-3" data-toggle="buttons">
                   <input type="text" name="accom_bird_days_before" id="accom_bird_days_before" placeholder="Bird days befor" /> 
               </div>	
               <div data-toggle="buttons" class="col-xs-3">
                   <input type="text" placeholder="Bird discount" id="accom_bird_discount" name="accom_bird_discount"> 
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
         <div class="col-xs-4">Discount</div>
         <div class="col-xs-4" data-toggle="buttons">
            <label class="btn discount active">
            <input type="radio" id="accom_discount" value="on" name='accom_discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
            </label>
            <label class="btn discount">
            <input type="radio" id="accom_discount1" value="off"  name='accom_discount' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
            </label>
        </div>
        </div>
        </div> 
        <div class="row" id="accom_discount_on" style="display:none;">
             <div class="col-xs-10">
                <div class="col-xs-4">Discount Price</div>
                <div class="col-sm-4">
                   <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="accom_precentage_discount" id="accom_precentage_discount" value="" placeholder="discount price in percantage">
                </div>
            </div>  
            <div class="col-xs-10">
                <div class="col-xs-4">Discount From date</div>
                <div class="col-sm-6">
                   <input type="text"  class="form-control" name="accom_discount_from_expiry" id="accom_discount_from_expiry" value="" placeholder="From date">
                </div>
            </div>
            <div class="col-xs-10">
                <div class="col-xs-4">Discount To date</div>
                <div class="col-sm-6">
                   <input type="text"   class="form-control" name="accom_discount_to_expiry" id="accom_discount_to_expiry" value="" placeholder="From date">
                </div>
            </div>    
       </div>
        <div class="row">
        <div class="col-xs-10">
         <div class="col-xs-4">Cancellation and refund policy: </div>
          <div class="col-xs-4" data-toggle="buttons">
           <label class="btn cancellation active">
             <input type="radio" id="accom_cancel_policy" value="yes" name='accom_cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
           </label>
           <label class="btn cancellation">
              <input type="radio" id="accom_cancel_policy1" value="no"  name='accom_cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
          </label>
        </div>
       </div>
        <div class="col-md-12">
              <div  id="accom_cancel_policy_yes" style="display:none;">
                  <div class="col-xs-3">chargeable if cancelled before: </div>
                  <div class="col-xs-8">
                    <div class="col-xs-5">
                     <input type="input" placeholder="% chargeable if cancelled before " class="form-control" name="accom_cancel_policy_price" id="accom_cancel_policy_price" value="">
                    </div>
                    <div class="col-xs-4">
                        <input type="input" placeholder="hrs" id="accom_cancel_hrs" name="accom_cancel_hrs" class="form-control">
                    </div>
                  </div>
                 <div class="clr"></div>
                 <div class="col-md-12">
                      <div class="col-xs-3"></div>
                       <div class="col-xs-6" style="margin-left:10px;"> 
                          <input type="input" placeholder="non refundable price" id="accom_not_cancel" name="accom_not_cancel" class="form-control">                   
                       </div>                   
                    </div>
                </div>
           </div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom6" class="progressbaraccom">
          <h2 class="fs-title">Gallery</h2>
          <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="accom_image[]" id="accom_image" multiple placeholder="Image" /></div>
           <input type="button" name="previous" class="previous action-button" value="Previous" />
           <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom7" class="progressbaraccom">
        <h2 class="fs-title">Contact info</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_user_name" id="accom_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_user_name_arb" id="accom_user_name_arb" placeholder="Arabic Name" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_position" id="accom_position" placeholder="position" /></div>
             <div class="col-md-6 col-sm-6"><input type="text" name="accom_position_arb" id="accom_position_arb" placeholder="Arabic position" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_email"  id="accom_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_mobile"  id="accom_mobile" placeholder="mobile number"   onkeypress="return OnlyNumericKeys(event);"/></div>
            <div class="col-md-6 col-sm-6"><input type="button" name="previous" class="previous action-button" value="Previous" /></div>
            <div class="col-md-6 col-sm-6"><input type="submit" id="accommodations" name="accommodationssubmit" class="submit action-button" value="Submit" /></div>
        </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
		jQuery(document).ready(function(e) {
			 jQuery('#accom_check_in').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#accom_check_out').val()?jQuery('#accom_check_out').val():false
				   })
				  },
			 
			 });
			jQuery('#accom_check_out').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#accom_check_in').val()?jQuery('#accom_check_in').val():false
				   })
				  },
			});
		   jQuery('#accom_discount_from_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#accom_discount_to_expiry').val()?jQuery('#accom_discount_to_expiry').val():false
				   })
				  },
			 
			 });
			jQuery('#accom_discount_to_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#accom_discount_to_expiry').val()?jQuery('#accom_discount_to_expiry').val():false
				   })
				  },
			});
	});
	function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_accom'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('form_location_address_accom');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1);;
				document.getElementById("form_location_accom").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("accom_location_address_accom").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
      <?php	
	}
	public function accomedit($accom_edit='')
	{
	  $accom_id = $this->input->post('accom_id');	
	  $accom_info=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$accom_id));		
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="accommodations_manage">Manage Accommodations</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Accommodations Information</h4>
          <div class="user-info-body">
        <form class="msform" id="accommodationsform" name="accommodationsform"  action="<?php echo base_url().'owner/updateaccom/'.$accom_id.'' ?>" method="post" enctype="multipart/form-data"> 
        <!-- progressbar -->
        <ul id="progressbaraccom" class="progressbar">
            <li class="active">Basic info</li>
            <li>Rooms &amp; beds</li>
            <li>Amenities</li>
            <li>Rules</li>
            <li>Location </li>
            <li class="big-text">Pricing & Cancelation</li>
            <li>Gallery :</li>
            <li>Contact info:</li>
        </ul>
         <input type="hidden" id="checkupdate_accoum" value="update">
        <!-- fieldsets -->
        <fieldset id="accom1" class="progressbaraccom">
        <h2 class="fs-title">Basic info</h2>
        <div class="col-md-6 col-sm-6"> 
           <input type="text" name="accom_title" placeholder="Titile" id="accom_title"  data-rule-required='true' value="<?php echo $accom_info[0]['accom_title'];  ?>"/>
        </div>
        <div class="col-md-6 col-sm-6"> 
           <input type="text" name="accom_title_arb" placeholder="Arbic Titile" id="accom_title_arb" value="<?php echo $accom_info[0]['accom_title_arb']; ?>"  data-rule-required='true' dir="rtl" />
        </div>
        <div class="col-md-6 col-sm-6 acc-input-file"> 
         <input type="file" name="accom_image1" placeholder="Titile" id="accom_image1"  data-rule-required='true'/>
          <img src="<?php echo base_url().'uploads/accom/'.$accom_info[0]['accom_image']; ?>" width="50" height="50">
        </div>
        <div class="col-md-6 col-sm-6">
            <span class="allowed-span">Type</span> 
           <label class="labelcheck"><input name="accom_type" <?php if($accom_info[0]['accom_type']=='accommodation') {echo 'checked="checked"';	} ?>  id="" value="accommodation" type="radio" >Accoumadation </label>
           <label class="labelcheck"><input name="accom_type" <?php if($accom_info[0]['accom_type']=='hall') {echo 'checked="checked"';	} ?>  id="" value="hall" type="radio">Hall </label>
        </div>
        <div style="clear:both;"></div>
        <div class="col-md-6 col-sm-6"> 
        <select name="accom_category_id" class="form-control" id="accom_category_id">
        <option selected="selected" value="">Select category</option>
        <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'1','parent_id'=>'0','category_status'=>'active')); 
        if(count($category_accom)>0)
        {
			foreach($category_accom as $row)
			{
				 $sel='';
				 if($accom_info[0]['accom_category_id']==$row['category_id'])
				 {
					 $sel="selected='selected'";	
				 }
        ?>
       		 <option value="<?php echo $row['category_id']; ?>" <?php echo $sel; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
        <?php 
       		 }
        } ?>
        </select>
       </div>
       <div class="col-md-6 col-sm-6">    
        <select name="accom_subcategory_id" class="form-control" id="accom_subcategory_id">
          <option  value="">Select subcategory</option>
           <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$accom_info[0]['accom_category_id'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
			            if($sub['category_id']==$accom_info[0]['accom_subcategory_id'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $sub['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
        </select>
        </div>
        <div class="col-md-6 col-sm-6"> 
          <textarea name="accom_description" id="accom_description" class="form-control" placeholder="Description"><?php echo $accom_info[0]['accom_description'];  ?></textarea>
        </div>
        <div class="col-md-6 col-sm-6"> 
         <textarea name="accom_description_arb" id="accom_description_arb" class="form-control" placeholder="Arabic Description" dir="rtl"><?php echo $accom_info[0]['accom_description_arb'];  ?></textarea>
        </div>
        <div style="clear:both;"></div>
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom2" class="progressbaraccom">
        <h2 class="fs-title">Rooms &amp; beds</h2>
        <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
        <div class="col-md-6 col-sm-6">  <input type="text" onkeypress="return OnlyNumericKeys(event);"  name="accom_bed_rooms" id="accom_bed_rooms" placeholder="Bed Rooms" value="<?php echo $accom_info[0]['accom_bed_rooms'];  ?>" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_beds" id="accom_beds" placeholder="Beds" value="<?php echo $accom_info[0]['accom_beds'];  ?>" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_bathrooms" id="accom_bathrooms" placeholder="Bathrooms" value="<?php echo $accom_info[0]['accom_bathrooms'];  ?>" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_swimming_pool" id="accom_swimming_pool" placeholder="swimming pool"  value="<?php echo $accom_info[0]['accom_swimming_pool'];  ?>"  /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_play_yards" id="accom_play_yards" placeholder="play yards" value="<?php echo $accom_info[0]['accom_play_yards'];  ?>" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_balcony" id="accom_balcony" placeholder="Balcony" value="<?php echo $accom_info[0]['accom_balcony'];  ?>"  /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_garden" id="accom_garden" placeholder="garden" value="<?php echo $accom_info[0]['accom_garden'];  ?>" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_roof" id="accom_roof" placeholder="Roof" value="<?php echo $accom_info[0]['accom_roof'];?>" /></div>
        
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom3" class="progressbaraccom">
            <h2 class="fs-title">Amenities</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">TV</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn active amenities">
            <input type="radio"  value="yes" <?php if($accom_info[0]['accom_tv']=='yes'){echo "checked";} ?>   name='accom_tv' id="accom_tv"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio"  value="no"  <?php if($accom_info[0]['accom_tv']=='no'){echo "checked";} ?> name='accom_tv' id="accom_tv1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_tv_yes" <?php if($accom_info[0]['accom_tv']=='no'){ ?> style="display:none;" <?php }  ?>><input type="text" name="accom_tv_number" id="accom_tv_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_tv_number']; ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Play station</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" value="yes" class="amenities"  <?php if($accom_info[0]['accom_playstation']=='yes'){echo "checked";} ?>  name='accom_playstation' id="accom_playstation"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" value="no" class="amenities" <?php if($accom_info[0]['accom_playstation']=='no'){echo "checked";} ?> name='accom_playstation' id="accom_playstation1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_playstation_yes" <?php if($accom_info[0]['accom_playstation']=='no'){ ?> style="display:none; <?php } ?>"><input type="text" name="accom_playstation_number" id="accom_playstation_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_playstation_number']; ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">WIFI</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" value="yes" <?php if($accom_info[0]['accom_wifi']=='yes'){echo "checked";} ?>  rel="number" name='accom_wifi' id="accom_wifi"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_wifi']=='no'){echo "checked";} ?> rel="number" name='accom_wifi' id="accom_wifi1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_wifi_yes" <?php if($accom_info[0]['accom_wifi']=='no'){ ?> style="display:none; <?php } ?>"><input type="text" name="accom_wifi_number" id="accom_wifi_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_wifi_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">DVD</div>
            <div class="col-xs-4 " data-toggle="buttons">

            <label class="btn amenities active">
            <input type="radio" class="amenities" <?php if($accom_info[0]['accom_dvd']=='yes'){echo "checked";} ?> value="yes" rel="number" name='accom_dvd' id="accom_dvd"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_dvd']=='no'){echo "checked";} ?> rel="number" name='accom_dvd' id="accom_dvd1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_dvd_yes"  <?php if($accom_info[0]['accom_dvd']=='no'){echo ' style="display:none;"';} ?>><input type="text" name="accom_dvd_number" id="accom_dvd_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_dvd_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">GYM</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" <?php if($accom_info[0]['accom_gym']=='yes'){echo "checked";} ?> value="yes" name='accom_gym' id="accom_gym"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_gym']=='no'){echo "checked";} ?>  name='accom_gym' id="accom_gym1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_gym_yes" <?php if($accom_info[0]['accom_gym']=='no'){echo 'style="display:none;"';} ?>><input type="text" name="accom_gym_number" id="accom_gym_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"  value="<?php echo $accom_info[0]['accom_gym_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">kitchen</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" value="yes" <?php if($accom_info[0]['accom_kitchen']=='yes'){echo "checked";} ?>  name='accom_kitchen' id="accom_kitchen"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_kitchen']=='no'){echo "checked";} ?>  name='accom_kitchen' id="accom_kitchen1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_kitchen_yes" <?php if($accom_info[0]['accom_kitchen']=='no'){echo 'style="display:none;"';} ?> ><input type="text" name="accom_kitchen_number" onkeypress="return OnlyNumericKeys(event);" id="accom_kitchen_number" class="form-control" value="<?php echo $accom_info[0]['accom_kitchen_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Washing Machine</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" id="accom_washing_machine" <?php if($accom_info[0]['accom_washing_machine']=='yes'){echo "checked";} ?>  value="yes" rel="machine_numbe" name='accom_washing_machine'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" id="accom_washing_machine1" value="no" <?php if($accom_info[0]['accom_washing_machine']=='no'){echo "checked";} ?> rel="machine_numbe" name='accom_washing_machine' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_washing_machine_yes" <?php if($accom_info[0]['accom_washing_machine']=='no'){echo 'style="display:none;"';} ?>><input type="text" name="accom_washing_machine_number" id="accom_washing_machine_number" onkeypress="return OnlyNumericKeys(event);" class="form-control" value="<?php echo $accom_info[0]['accom_washing_machine_number'];  ?>"></div>
            </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom4" class="progressbaraccom">
        <h2 class="fs-title">Rules</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Max gusts</div>
        <div class="col-xs-6">
        <input type="text" name="accom_guest_max" id="accom_guest_max" placeholder="Max guests" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_guest_max'];  ?>"  />
        </div>
        </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
                <div class="col-xs-4">Check In</div>
                <div class="col-xs-6">
                   <input type="text" name="accom_check_in" id="accom_check_in" placeholder="Select check in time" value="<?php echo date('Y-m-d H:i:s',strtotime($accom_info[0]['accom_check_in']));  ?>"  />
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
          <div class="col-xs-4">Check Out</div>
           <div class="col-xs-6">
             <input type="text" name="accom_check_out" id="accom_check_out" placeholder="Select check in time" value="<?php echo date('Y-m-d H:i:s',strtotime($accom_info[0]['accom_check_out']));  ?>" />
            </div>
          </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
          <div class="col-xs-4">Reporting Time </div>
           <div class="col-xs-6">
             <input type="text" name="accom_reporting_time" id="accom_reporting_time" placeholder="Before the departure/Booked time"  value="<?php echo $accom_info[0]['accom_reporting_time'];  ?>"  />
            </div>
          </div>
        </div> 
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">Allowed</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox='';
						   if($accom_info[0]['accom_what_allowed'])
						   {
							   $whatallowed=explode(',',$accom_info[0]['accom_what_allowed']);
							   if(in_array($row['facilities_id'],$whatallowed))
							   {
								$checkbox='checked="checked"';
							   }
						   }
                   ?>
                        <label class="labelcheck"><input  <?php echo $checkbox; ?> type="checkbox" name="accom_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
            <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">Not Allowed </span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox1='';
						   if($accom_info[0]['accom_what_allowed'])
						   {
							   $whatallowed=explode(',',$accom_info[0]['accom_not_allowed']);
							   if(in_array($row['facilities_id'],$whatallowed))
							   {
								$checkbox1='checked="checked"';
							   }
						   }
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox1; ?> type="checkbox" name="accom_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
        </div>    
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">What Included</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox2='';
						   if($accom_info[0]['accom_what_allowed'])
						   {
							   $what_included=explode(',',$accom_info[0]['accom_what_included']);
							   if(in_array($row['included_id'],$what_included))
							   {
								$checkbox2='checked="checked"';
							   }
						   }   
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox2; ?> type="checkbox" name="accom_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
               <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">What not Included</span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox3='';
						   if($accom_info[0]['accom_what_allowed'])
						   {
							   $what_included=explode(',',$accom_info[0]['accom_not_included']);
							   if(in_array($row['included_id'],$what_included))
							   {
								$checkbox3='checked="checked"';
							   }
						   } 
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox3; ?> type="checkbox" name="accom_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
              <span class="allowed-span">Requirement </span>
                   <?php 
				   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox4='';
						   if($accom_info[0]['accoum_requirement'])
						   {
							   $whatincluded=explode(',',$accom_info[0]['accoum_requirement']);
							   if(in_array($row['requirement_id'],$whatincluded))
							   {
								 $checkbox4='checked="checked"';
							   }
						   } 
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox4; ?> type="checkbox" name="accoum_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom8" class="progressbaraccom">
            <h2 class="fs-title">Location</h2>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location_address" id="accom_location_address_accom" placeholder="Location Address" value="<?php echo $accom_info[0]['form_location_address']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="accom_location_map" id="accom_location_accom" placeholder="Location" value="<?php echo $accom_info[0]['accom_location_map']; ?>"  /></div>
            <div class="col-md-12 col-sm-12"> <div id="gmap_accom" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom5" class="progressbaraccom">
        <h2 class="fs-title">Pricing- discount </h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Price</div>
        <div class="col-xs-6">
          <input type="text" name="accom_price" id="accom_price" placeholder="Price"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_price']; ?>" />
        </div>
        </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Reservation Amount </div>
                <div class="col-xs-6">
                <input type="text" name="accom_reservation_price" id="accom_reservation_price" placeholder="Reservation Amount"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_reservation_price'];  ?>" />
                </div>
           </div>
        </div>
        <div class="row">
            <div class="col-xs-10">
                <div class="col-xs-4">Bird discount</div>
                <div class="col-xs-3" data-toggle="buttons">
                   <input type="text" name="accom_bird_days_before" id="accom_bird_days_before" placeholder="Bird days befor" value="<?php echo $accom_info[0]['accom_bird_days_before'];  ?>" /> 
               </div>	
               <div data-toggle="buttons" class="col-xs-3">
                   <input type="text" placeholder="Bird discount" id="accom_bird_discount" name="accom_bird_discount" value="<?php echo $accom_info[0]['accom_bird_discount'];  ?>" > 
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
         <div class="col-xs-4">Discount</div>
         <div class="col-xs-4" data-toggle="buttons">
            <label class="btn discount <?php if($accom_info[0]['accom_discount']=='on'){ ?> active <?php } ?>">
             <input type="radio"  <?php if($accom_info[0]['accom_discount']=='on'){ ?> checked='checked' <?php } ?> id="accom_discount" value="on" name='accom_discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
            </label>
            <label class="btn discount <?php if($accom_info[0]['accom_discount']=='off'){ ?> active <?php } ?>">
            <input type="radio" id="accom_discount1" value="off"  name='accom_discount' <?php if($accom_info[0]['accom_discount']=='off'){ ?> checked='checked' <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
            </label>
        </div>
        </div>
        </div> 
        <div class="row" id="accom_discount_on" style=" <?php if($accom_info[0]['accom_discount']=='on'){ echo 'display:show;'; }else{ echo 'display:none;';} ?>">
             <div class="col-xs-10">
                <div class="col-xs-4">Discount Price</div>
                <div class="col-sm-4">
                   <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="accom_precentage_discount" id="accom_precentage_discount" placeholder="discount price in percantage" value="<?php echo $accom_info[0]['accom_precentage_discount']; ?>">
                </div>
            </div>  
            <div class="col-xs-10">
                <div class="col-xs-4">Discount From date</div>
                <div class="col-sm-6">
                   <input type="text"  class="form-control" value="<?php echo date('Y-m-d H:i:s',strtotime($accom_info[0]['accom_discount_from_expiry']));  ?>" name="accom_discount_from_expiry" id="accom_discount_from_expiry" placeholder="From date">
                </div>
            </div>
            <div class="col-xs-10">
                <div class="col-xs-4">Discount To date</div>
                <div class="col-sm-6">
                   <input type="text"   class="form-control" name="accom_discount_to_expiry" id="accom_discount_to_expiry" value="<?php echo date('Y-m-d H:i:s',strtotime($accom_info[0]['accom_discount_to_expiry']));  ?>" placeholder="From date">
                </div>
            </div>    
       </div>
        <div class="row">
        <div class="col-xs-10">
         <div class="col-xs-4">Cancellation and refund policy: </div>
          <div class="col-xs-4" data-toggle="buttons">
           <label class="btn cancellation active">
             <input type="radio" id="accom_cancel_policy"  <?php if($accom_info[0]['accom_cancel_policy']=='yes'){echo 'checked';} ?> value="yes" name='accom_cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
           </label>
           <label class="btn cancellation">
              <input type="radio" id="accom_cancel_policy1" value="no" <?php if($accom_info[0]['accom_cancel_policy']=='no'){echo 'checked';} ?>   name='accom_cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
          </label>
        </div>
       </div>
        <div class="col-md-12">
              <div  id="accom_cancel_policy_yes" <?php if($accom_info[0]['accom_cancel_policy']=='no'){echo 'style="display:none;"';} ?> >
                  <div class="col-xs-3">chargeable if cancelled before: </div>
                  <div class="col-xs-8">
                    <div class="col-xs-5">
                     <input type="input" placeholder="% chargeable if cancelled before " class="form-control" name="accom_cancel_policy_price" id="accom_cancel_policy_price" value="<?php echo $accom_info[0]['accom_cancel_policy_price']; ?>">
                    </div>
                    <div class="col-xs-4">
                        <input type="input" placeholder="hrs" id="accom_cancel_hrs" name="accom_cancel_hrs" class="form-control" value="<?php echo $accom_info[0]['accom_cancel_hrs']; ?>">
                    </div>
                  </div>
                 <div class="clr"></div>
                 <div class="col-md-12">
                      <div class="col-xs-3"></div>
                       <div class="col-xs-6" style="margin-left:10px;"> 
                          <input type="input" placeholder="non refundable price" id="accom_not_cancel" name="accom_not_cancel" class="form-control" value="<?php echo $accom_info[0]['accom_not_cancel']; ?>">                   
                       </div>                   
                    </div>
                </div>
           </div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom6" class="progressbaraccom">
              <h2 class="fs-title">Gallery</h2>
              <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="accom_image[]" id="accom_image" multiple placeholder="Image" /></div>
              <div class="clearfix"></div>
              <div class="image-set gallery-box">
                      <?php $accoun_image=$this->master_model->getRecords('tbl_accommodations_images',array('accom_id'=>$accom_id)); 
                      if(count($accoun_image))
                      {
                          foreach($accoun_image as $row)
                          {
                        ?>
                          <div class="col-md-3 col-sm-4">
                              <div class="image-wrapper">
                            <img src="<?php  echo base_url().'uploads/accom/'.$row['accom_image_name']; ?>" width="175" height="144">
                            <div class="img-caption">
                                <div class="link">
                                    <a href="javascript:void(0);" class="deletelink" rel="accommodations" data-id="<?php echo $row['accom_image_id'];?>"><i class="fa fa-times"></i></a>
                                </div>
                            </div>      
                              </div>
                              </div>
                      <?php
                          }
                      }
                        
                      ?>
                    </div>
              <div class="clearfix"></div>
              <input type="button" name="previous" class="previous action-button" value="Previous" />
              <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="accom7" class="progressbaraccom">
        <h2 class="fs-title">Contact info</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_user_name" id="accom_user_name" placeholder="Name" value="<?php echo $accom_info[0]['accom_user_name'];  ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_user_name_arb" id="accom_user_name_arb" placeholder="Arabic Name" value="<?php echo $accom_info[0]['accom_user_name_arb'];  ?>" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_position" id="accom_position" placeholder="position" value="<?php echo $accom_info[0]['accom_position'];  ?>" /></div>
             <div class="col-md-6 col-sm-6"><input type="text" name="accom_position_arb" id="accom_position_arb" placeholder="Arabic position" dir="rtl" value="<?php echo $accom_info[0]['accom_position_arb'];  ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_email"  id="accom_email" placeholder="Email" value="<?php echo $accom_info[0]['accom_email'];  ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_mobile"  id="accom_mobile" placeholder="mobile number" value="<?php echo $accom_info[0]['accom_mobile'];  ?>"    onkeypress="return OnlyNumericKeys(event);"/></div>
            <div class="col-md-6 col-sm-6"><input type="button" name="previous" class="previous action-button" value="Previous" /></div>
            <div class="col-md-6 col-sm-6"><input type="submit" id="accommodations" name="accommodationssubmit" class="submit action-button" value="Submit" /></div>
        </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
		jQuery(document).ready(function(e) {
			 jQuery('#accom_check_in').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#accom_check_out').val()?jQuery('#accom_check_out').val():false
				   })
				  },
			 
			 });
			jQuery('#accom_check_out').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#accom_check_in').val()?jQuery('#accom_check_in').val():false
				   })
				  },
			});
		   jQuery('#accom_discount_from_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#accom_discount_to_expiry').val()?jQuery('#accom_discount_to_expiry').val():false
				   })
				  },
			 
			 });
			jQuery('#accom_discount_to_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#accom_discount_to_expiry').val()?jQuery('#accom_discount_to_expiry').val():false
				   })
				  },
			});
	});
	function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_accom'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('form_location_address_accom');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1);;
				document.getElementById("form_location_accom").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("accom_location_address_accom").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
      <?php	
	}
	public function addcoupon()
	{
      ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
        <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="coupon_manage">Manage Coupon</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Coupon</h4>
          <div class="user-info-body">
        <form class="msform" id="couponform" name="couponform" method="post" enctype="multipart/form-data"> 
        <!-- progressbar -->
        <ul id="progressbarcoupon" class="progressbar">
            <li class="active ">Basic info</li>
            <li>Rules </li>
            <li>Location  </li>
            <li>Pricing </li>
            <!--<li>Chargeable /free items </li>-->
            <li class="big-text">Cancel &amp; Refund</li>
            <li>Gallery :</li>
            <li>Contact info:</li>
        </ul>
        <!-- fieldsets -->
        <fieldset id="coupon1" class="progressbarcoupon">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_title" placeholder="Title" id="coupon_title"  data-rule-required='true'/></div>
           
            
               <div class="col-md-6 col-sm-6"> 
            <input type="text" name="coupon_title_arb" placeholder="Arabic Title" id="coupon_title_arb"  data-rule-required='true' value="" dir="rtl"/>
            </div>
            
            <div class="col-md-6 col-sm-6"> 
              <input type="file" name="coupon_image" placeholder="Image" id="coupon_image"  data-rule-required='true'/>
            </div>
            
            <div class="col-md-6 col-sm-6">
            <span class="allowed-span">Type</span> 
           <label class="labelcheck"><input name="coupon_type"  checked="checked" id="" value="tickets" type="radio" >Tickets </label>
           <label class="labelcheck"><input name="coupon_type"   id="" value="coupon" type="radio">Coupon </label>
        </div>
        
         <div style="clear:both;"></div>
            
            <div class="col-md-6 col-sm-6">
            <select name="category_id" class="form-control" id="coupon_category_id">
                <option selected="selected" value="">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'7','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategory_id" class="form-control" id="subcategory_id">
               <option selected="selected" value="">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"> <textarea name="coupon_location" id="coupon_location" class="form-control" placeholder="location"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="coupon_address" id="coupon_address" class="form-control" placeholder="Address"></textarea></div>
            <div class="col-md-6 col-sm-6"> <textarea name="coupon_description" id="coupon_description" class="form-control" placeholder="Description"></textarea></div>
             <div class="col-md-6 col-sm-6"> <textarea name="coupon_description_arb" id="coupon_description_arb" class="form-control" placeholder="Arabic Description" dir="rtl"></textarea></div>
             
              <div class="col-md-6 col-sm-6"> <textarea name="coupon_why_you" id="coupon_why_you" class="form-control" placeholder="Why You"></textarea></div>
              
               <div class="col-md-6 col-sm-6"> <textarea name="coupon_why_you_arb" id="coupon_why_you_arb" class="form-control" placeholder="Arabic Why You" dir="rtl"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon2"  class="progressbarcoupon">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
           <div class="col-md-12 col-sm-12">
               <select name="gender" class="form-control" id="coupon_gender">
               <option selected="selected" value="">none</option>
               <option value="none">none</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">  <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="age" id="age" placeholder="Age" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="weight" id="weight" placeholder="Weight" /></div>
            <!-- code adding from padmaja-->
             <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">Allowed</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="coupon_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
            <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">Not Allowed </span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="coupon_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?></div> 
        </div>    
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">What Included</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="coupon_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                </div>
               <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">What not Included</span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="coupon_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
              </div> 
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
              <span class="allowed-span">Requirement </span>
                   <?php 
				   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="coupon_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
        </div>
        <!-- code adding from padmaja-->
        
           <!-- <div class="col-md-6 col-sm-6">  <textarea name="coupon_what_allowed" id="coupon_what_allowed" class="form-control" placeholder="what allowed"></textarea></div>
            <div class="col-md-6 col-sm-6">  <textarea name="coupon_not_allowed" id="coupon_not_allowed" class="form-control" placeholder="Not Allowed"></textarea></div>-->
            <div class="col-md-12 col-sm-12"> <textarea name="additiona_info" id="additiona_info" class="form-control" placeholder="Other Rules" data-rule-required='true'></textarea></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        
        <fieldset id="coupon7" class="progressbarcoupon">
               <h2 class="fs-title">Location</h2>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location_address" id="form_location_address_coupon" placeholder="Location Address" />
            </div>
            <div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="coupon_location_map" id="coupon_location_coupon" placeholder="Location" /></div>
            <div class="col-md-12 col-sm-12"> <div id="gmap_coupon" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        
        <fieldset id="coupon3"  class="progressbarcoupon">
        <h2 class="fs-title">Pricing </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                    <input type="text" name="price" id="price" placeholder="Price"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                <div class="col-xs-4">Reservation Amount </div>
                    <div class="col-xs-6">
                    <input type="text" name="reservation_price" id="reservation_price" placeholder="Reservation Amount"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                    <div class="col-xs-4">Bird discount</div>
                    <div class="col-xs-3" data-toggle="buttons">
                       <input type="text" name="bird_days_before" id="bird_days_before" placeholder="Bird days befor" /> 
                   </div>	
                   <div data-toggle="buttons" class="col-xs-3">
                       <input type="text" placeholder="Bird discount" id="bird_discount" name="bird_discount"> 
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
             <div class="col-xs-4">Discount</div>
             <div class="col-xs-4" data-toggle="buttons">
                <label class="btn discount active">
                <input type="radio" id="discount" value="on" name='discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
                </label>
                <label class="btn discount">
                <input type="radio" id="discount1" value="off"  name='discount' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
                </label>
            </div>
            </div>
            </div> 
            <div class="row" id="discount_on" style="display:none;">
                 <div class="col-xs-10">
                    <div class="col-xs-4">Discount Price</div>
                    <div class="col-sm-4">
                       <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="precentage_discount" id="precentage_discount" value="" placeholder="discount price in percantage">
                    </div>
                </div>  
                <div class="col-xs-10">
                    <div class="col-xs-4">Discount From date</div>
                    <div class="col-sm-6">
                       <input type="text"  class="form-control" name="discount_from_expiry" id="discount_from_expiry" value="" placeholder="From date">
                    </div>
                </div>
                <div class="col-xs-10">
                    <div class="col-xs-4">Discount To date</div>
                    <div class="col-sm-6">
                       <input type="text"   class="form-control" name="discount_to_expiry" id="discount_to_expiry" value="" placeholder="To date">
                    </div>
                </div>    
           </div>
            
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset  id="coupon4" class="progressbarcoupon">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn couponcancel active">
                        <input type="radio" id="cancel_policy" value="yes" name='cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel">
                        <input type="radio" id="cancel_policy" value="no"  name='cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="cancel_policy_yes" style="display:none;">
                      <input type="input" class="form-control" name="cancel_policy_price" id="cancel_policy_price" value="">
                    </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon5" class="progressbarcoupon">
             <h2 class="fs-title">Gallery</h2>
               <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
               <div class="col-md-6 col-sm-6"> <input type="file" name="coupon_images[]" id="coupon_images" placeholder="Name" multiple/></div>
               <input type="button" name="previous" class="previous action-button" value="Previous" />
               <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon6" class="progressbarcoupon">
                <h2 class="fs-title">Contact info</h2>
                
                 <div class="col-md-6 col-sm-6"><input type="text" name="coupon_user_name" id="coupon_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_user_name_arb" id="coupon_user_name_arb" placeholder="Arabic Name" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_person_postion" id="coupon_person_postion" placeholder="position" /></div>
             <div class="col-md-6 col-sm-6"><input type="text" name="coupon_person_postion_arb" id="coupon_person_postion_arb" placeholder="Arabic position" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_email"  id="coupon_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_mobile"  id="coupon_mobile" placeholder="mobile number"   onkeypress="return OnlyNumericKeys(event);"/></div>
            <div class="col-md-6 col-sm-6"><input type="button" name="previous" class="previous action-button" value="Previous" /></div>
            <div class="col-md-6 col-sm-6"><input type="submit" id="couponsubmit" name="couponsubmit" class="submit action-button" value="Submit" /></div>
            
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
           <!-- <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_user_name" id="coupon_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_person_postion" id="coupon_person_postion" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_email"  id="coupon_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_mobile"  id="coupon_mobile" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="couponform" name="couponsubmit" class="submit action-button" value="Submit" />-->
          </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	  		  jQuery(document).ready(function(e) {
			jQuery('#discount_from_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						maxDate:jQuery('#discount_to_expiry').val()?jQuery('#discount_to_expiry').val():false
					   })
					  },
				 
				 });
			jQuery('#discount_to_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						minDate:jQuery('#discount_from_expiry').val()?jQuery('#discount_from_expiry').val():false
					   })
					  },
				});
		 });
	function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_coupon'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('form_location_address_coupon');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1);;
				
				document.getElementById("coupon_location_coupon").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  alert(results[0].formatted_address);
				document.getElementById("coupon_location_address_coupon").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
      <?php	
	
	}
    public function addatours()
	{
	 ?>
		  <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
		  <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
		  <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
		  <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script>     
		 <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
		  <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
			<p><a href="javascript:void(0);"  class="addcls" rel="tours_manage">Manage Things to do</a></p>
		  </div> 
		  <div class="clearfix"></div>
		  <div class="col-md-12">
			 <div class="user-personal-info">
			  <h4>Things to do</h4>
			  <div class="user-info-body">
			<form class="msform" id="toursform" name="toursform" method="post" enctype="multipart/form-data"> 
			<!-- progressbar -->
			<ul id="progressbartours" class="progressbar">
                <li class="active">Basic info</li>
                <li>Contact info:</li>
                <li>Rules </li>
                <li>Location </li>
                <li class="big-text">pricing & cancellation</li>
                <li>Schedule</li>
                <li>Gallery</li>
		   </ul>
			<!-- fieldsets -->
			<fieldset id="tours1" class="progressbartours">
				<h2 class="fs-title">Basic info</h2>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_tours"  data-rule-required='true'/></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" id="form_title_arb_tours"  placeholder="Arabic Title"  data-rule-required='true' dir="rtl"/></div>
				<div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_image" placeholder="Titile" id="form_image_tours" /></div>
				<div class="col-md-6 col-sm-6">
                  <select name="main_categoryID" class="form-control" id="categoryID_things_to_do">
                        <option selected="selected" value="">Select main category</option>
                        <?php 
						$this->db->where('category_id IN(2,3,4,9,10) !=0');
						$category_main=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active')); 
                        if(count($category_main)>0)
                        {
                            foreach($category_main as $row)
                            {
                            ?>
                            <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                            <?php 
                            }
                        } ?>
                    </select>
				</div>
                <div style="clear:both"></div>
                <div class="col-md-6 col-sm-6">
				<select name="categoryID" class="form-control" id="categoryID_tours">
					<option selected="selected" value="">Select category</option>
				</select>
				</div>
				<div class="col-md-6 col-sm-6">
				<select name="subcategoryID" class="form-control" id="subcategoryID_tours">
				   <option selected="selected" value="">Select subcategory</option>
				</select>
				</div>
				<div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_tours" value="" placeholder="duration"></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_tours" class="form-control" placeholder="Description"></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_tours" class="form-control" placeholder="Arbic Description" dir="rtl"></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_tours" class="form-control" placeholder="Why you"></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_tours" class="form-control" placeholder="arbic Why you" dir="rtl" ></textarea></div>
				<!--<div class="col-md-12 col-sm-12">  <textarea name="form_address" id="form_address_tours" class="form-control" placeholder="Address"></textarea></div>-->
				<div style="clear:both;"></div>
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="tours2"  class="progressbartours">
					<h2 class="fs-title">Contact info</h2>
					<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_tours" placeholder="Name" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name_arb" id="form_name_arb_tours" placeholder="Arbic Name" dir="rtl"  /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_tours" placeholder="position" /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position_arb" id="form_position_arb_tours" placeholder="Arbic position" dir="rtl" /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_tours" placeholder="Email" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_tours" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);" /></div>
					<input type="button" name="previous" class="previous action-button" value="Previous" />
					<input type="button" name="next" class="next action-button" value="Next" />
			   </fieldset>
			<fieldset id="tours3" class="progressbartours">
				<h2 class="fs-title">Rules</h2>
				<!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
				<div class="col-md-6 col-sm-6">
				<select name="form_gender" class="form-control" id="form_gender_tours">
				   <option selected="selected" value="">Select gender</option>
				   <option value="none">none</option>
				   <option value="Male">Male</option>
				   <option value="Female">Female</option>
				</select>
				</div>
				<div class="col-md-6 col-sm-6"><input type="text" onkeypress="return OnlyNumericKeys(event);"  name="form_age" id="form_age_tours" placeholder="Age" /></div>
				<div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);" name="form_weight" id="form_weight_tours" placeholder="Weight" /></div>
				<div class="col-md-6 col-sm-6">
				<span class="allowed-span">Allowed</span> 
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
				   ?>
						<label class="labelcheck"><input type="checkbox" name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
				</div>
				<div class="col-md-6 col-sm-6">
				   <span class="allowed-span">Not Allowed </span>
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
				   ?>
						<label class="labelcheck"><input type="checkbox" name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				<!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>-->
				</div>
				<div class="row">
				<div class="col-md-6 col-sm-6">
					<span class="allowed-span">What Included</span> 
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
					   ?>
							<label class="labelcheck"><input type="checkbox" name="form_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
					</div>
				   <div class="col-md-6 col-sm-6">
					   <span class="allowed-span">What not Included</span>
					   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
					   ?>
							<label class="labelcheck"><input type="checkbox" name="form_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
				</div>
				<div class="row">
			  <div class="col-md-6 col-sm-6">
				  <span class="allowed-span">Requirement </span>
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
					   ?>
							<label class="labelcheck"><input type="checkbox" name="form_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
			</div>
				<div class="col-md-6 col-sm-6"><textarea name="form_additional_info" id="form_additional_info_tours" class="form-control" placeholder="Additional info"></textarea></div>
				 <div class="col-md-6 col-sm-6"><textarea name="form_additional_info_arb" id="form_additional_info_arb_tours" class="form-control" placeholder="Arbic Additional info" dir="rtl"></textarea></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>   
			<fieldset id="tours7" class="progressbartours">
				<h2 class="fs-title">Location</h2>
				<div class="col-md-12 col-sm-12">
				<input type="text" name="form_location_address" id="form_location_address_tours" placeholder="Location Address" />
				</div>
				<div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="form_location" id="form_location_tours" placeholder="Location" /></div>
				<div class="col-md-12 col-sm-12"> <div id="gmap_tours" style="width:100%;height:200px;"></div></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset  id="tours4" class="progressbartours">
			   <h2 class="fs-title">Pricing & cancellation policy </h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="row">
				  <!-- New Form Add HereNed here-->
				  <div class="col-xs-10">
						  <div class="col-xs-4">Family</div>
						   <div class="col-xs-4">
								<label class="yeslabel">
								   <input type="checkbox" class="chk_click" id="form_family_member_tours" rel='form_family_member_toursdiv' value="yes" name='form_family'>
									<span class="yeslabeltext"> yes</span>
								</label>
						   </div>
					  </div>
				  <div class="col-xs-10" id="form_family_member_toursdiv" style="display:none;">
				   <div class="col-xs-4">&nbsp;</div>
					<div class="col-xs-3">
					 <input type="text" id="form_family_member_tours" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" />
				   </div>
				   <div class="col-xs-3">
					 <input type="text" id="form_family_price_tours" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" />
				   </div>
				  </div>
				  <div class="col-xs-10">
					<div class="col-xs-4">Children Less Than</div>
					<div class="col-xs-3">
					  <select id="form_lessthan_age_tours" class="form-control" name="form_lessthan_age">
						<?php for($i=0;$i<=18;$i++){ ?>
						<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					  </select>
					</div>
					<div class="col-xs-3">
					 <input type="text" id="form_lessthan_price_tours" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Price</div>
					<div class="col-xs-6">
					  <input type="text" name="form_member_price" id="form_member_price_tours" placeholder="Price" onkeypress="return OnlyNumericKeys(event);" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Group</div>
					<div class="col-xs-3">
					  <input type="text" name="form_min_member" id="form_min_member_tours" placeholder="Min"  onkeypress="return OnlyNumericKeys(event);" />
					</div>
					<div class="col-xs-3">
					  <input type="text" name="form_max_member" id="form_max_member_tours" placeholder="Max"  onkeypress="return OnlyNumericKeys(event);" />
					</div>
				   </div> 
				   <div class="col-xs-10">
						<div class="col-xs-4">Group Price</div>
						<div class="col-xs-6">
						  <input type="text" name="form_grp_price" id="form_grp_price_tours" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" />
						</div>
				   </div>   
				   
				  <div class="clearfix"></div>
				</div>
				<div class="row">
				  <div class="col-xs-10">
					<div class="col-xs-4">Bird discount</div>
						<div class="col-xs-3" data-toggle="buttons">
						   <input name="form_bird_days_before" id="form_bird_days_tours" value="" placeholder="Bird days befor" type="text"> 
					   </div>	
					   <div data-toggle="buttons" class="col-xs-3">
						   <input placeholder="Bird discount" id="form_bird_tours" name="form_bird_discount" value="" type="text"> 
						</div>
					</div>
				</div>
				<div class="row">
				   <div class="col-xs-10">
					 <div class="col-xs-4">Discount</div>
					 <div class="col-xs-4" data-toggle="buttons">
						<label class="btn discountthree active">
						<input id="tours_discount" value="on" class="tours_discount" name="form_discount" checked="checked" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
						</label>
						<label class="btn discountthree">
						<input id="tours_discount1" value="off" class="tours_discount" name="form_discount" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
						</label>
					</div>
				   </div>
				</div>
				<div class="row" id="tours_discount_on" style="display: none;">
				   <div class="col-xs-10">
						<div class="col-xs-4">Discount Price</div>
						<div class="col-sm-4">
						   <input onkeypress="return OnlyNumericKeys(event);" class="form-control" name="form_pecentage_discount" id="tours_recreation" value="" placeholder="discount price in percantage" type="text">
						</div>
					</div>  
					<div class="col-xs-10">
						<div class="col-xs-4">Discount From date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_from_expiry" id="tours_discount_from_expiry" value="" placeholder="From date" type="text">
						</div>
					</div>
					<div class="col-xs-10">
						<div class="col-xs-4">Discount To date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_to_expiry" id="tours_discount_to_expiry" value="" placeholder="From date" type="text">
						</div>
					</div>    	
				</div>
				<div class="row">
				   <div class="col-xs-10">
						<div class="col-xs-4">Cancellation and refund policy: </div>
						<div class="col-xs-4" data-toggle="buttons">
							<label class="btn amenitiesform active">
							<input type="radio" id="form_refundable_tours" rel='form_refundable_tours' value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
							</label>
							<label class="btn amenitiesform">
							<input type="radio" id="form_refundable_tours1" rel='form_refundable_tours' value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
							</label>
						</div>
					</div>
					<div class="col-md-12">
					  <div id="form_refundable_tours_yes" style="display:none;">
						  <div class="col-xs-3">chargeable if cancelled before: </div>
						  <div class="col-xs-8">
							<div class="col-xs-5">
						   <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_tours" value="" placeholder="% chargeable if cancelled before "></div>
						   <div class="col-xs-4">
							<input type="input" placeholder="hrs" value="" id="form_not_cancel_tours" name="form_not_cancel_hrs" class="form-control">
							</div>
						  </div>
						 <div class="clr"></div>
						 <div class="col-md-12">
							  <div class="col-xs-3"></div>
							   <div class="col-xs-6" style="margin-left:10px;">                    
								<input type="input" placeholder="non refundable price" value="" id="form_not_cancel_tours" name="form_not_cancel" class="form-control">
							  </div>                   
							</div>
						</div>
					</div>
				</div>
			   
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="tours5" class="progressbartours">
			<h2 class="fs-title">Schedule</h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div id="weekly_tours">
					   <input type="hidden" id="form_availalbe_to_tours"  rel="tours" value="weekly"  name="form_availalbe_for">
					   <div class="row">
					<div class="col-xs-10">
					  <div class="col-xs-4">From Date</div>
					  <div class="col-xs-6">
						<input type="text" name="form_available_from_date" id="form_available_from_date_tours" placeholder="Select available time" />
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">To Date</div>
					   <div class="col-xs-6">
					   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_tours"  placeholder="Select available time" />
					 </div>
					</div>
				</div>
					  <?php 
					  $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
					  $cntarr=count($days_arr);
					  if(count($days_arr)>0)
					  {
						for($i=0;$i<$cntarr;$i++)
						{
					   ?>
						<div class="row">
						  <div class="col-xs-10">
							<div class="col-xs-4"><?php if($i==0) {?>Select Days <?php }else{echo ' ';} ?></div>
							<div class="col-xs-3">
							  <label style="margin-bottom:-10px;">
								<div class="col-xs-1">
									<input type="checkbox" class="clsday" name="form_available_day[]" id="form_available_day_tours_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>">
								</div>
								<div class="col-xs-3" style="margin-top:10px;"><?php echo ucfirst($days_arr[$i]);?></div>
							  </label>
							 </div>
						  </div> 
						</div>
						<div class="row" id="<?php echo ucfirst($days_arr[$i]);?>" style="display:none;">
						   <div class="col-xs-10">
							  <div class="col-xs-4"><?php echo ' ';?></div>
							  <div class="col-xs-2"><input type="text" name="form_from_time_<?php echo $days_arr[$i]?>[]" id="form_from_time_tours<?php echo $i; ?>" class="form_availalbe_from_time_tours" placeholder="From time" /></div>
							  <div class="col-xs-2"><input type="text" name="form_to_time_<?php echo $days_arr[$i]?>[]" id="form_to_time_tours<?php echo $i; ?>" class="form_availalbe_to_time_tours" placeholder="To time" /> </div>
							  <div class="col-xs-2"><input type="text" name="form_day_duration_<?php echo $days_arr[$i]?>[]" id="form_duration_tours<?php echo $i; ?>" placeholder="Duration" /> </div>
							  <div class="col-xs-2" style="margin-top:-20px;"><a href="javascript:void(0);" class="addcheck" rel='tours'>Add</a></div>
							</div>
						</div> 
						<?php
						}
					  }
					  ?>
				 </div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point</div>
					   <div class="col-xs-6">
						<textarea name="form_departure" id="form_departure_tours" class="form-control" placeholder="Departure Point"></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point address</div>
					   <div class="col-xs-6">
						<textarea name="form_departure_address" id="form_departure_address_tours" class="form-control" placeholder="Address"></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_departure_time" id="form_departure_time_tours"  placeholder="Departure Time" />
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Reporting Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_reporting_time" id="form_reporting_time_tours"  placeholder="Departure Time" />
					 </div>
					</div>
				</div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="tours6" class="progressbartours">
				<h2 class="fs-title">Gallery</h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="row">
					<div class="col-xs-10">
						<input type="file" style="padding:0px;" multiple name="form_images_name[]" id="form_images_name" class="form-control" >                      
					</div>
				</div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="submit" id="submit_tours" name="tourssubmit" class="submit action-button" value="Submit" />   
			</fieldset>
			</form> 
		  <script type="text/javascript">
		  jQuery(document).ready(function(e) {
			 jQuery('#form_available_from_date_tours').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#form_availalbe_to_date_tours').val()?jQuery('#form_availalbe_to_date_tours').val():false
				   })
				  },
			 
			 });
			jQuery('#form_availalbe_to_date_tours').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#form_available_from_date_tours').val()?jQuery('#form_available_from_date_tours').val():false
				   })
				  },
			});
			jQuery('.form_availalbe_from_time_tours').datetimepicker({
				  formatTime:'H:i:s',
				  format:	'H:i:s',
				  datepicker: false,
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('.form_availalbe_to_time_tours').val()?jQuery('.form_availalbe_to_time_tours').val():false
				   })
				  },
			 
			 });
			jQuery('.form_availalbe_to_time_tours').datetimepicker({
				  formatTime:'H:i:s',
				  format:	'H:i:s',
				  datepicker: false,
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('.form_available_from_time_tours').val()?jQuery('.form_available_from_time_tours').val():false
				   })
				  },
			});
			jQuery('#tours_discount_from_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						maxDate:jQuery('#tours_discount_to_expiry').val()?jQuery('#tours_discount_to_expiry').val():false
					   })
					  },
				 
				 });
			jQuery('#tours_discount_to_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						minDate:jQuery('#tours_discount_from_expiry').val()?jQuery('#tours_discount_from_expiry').val():false
					   })
					  },
				});
		 });
			function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_tours'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('form_location_address_tours');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1);;
				document.getElementById("form_location_tours").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("form_location_address_tours").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
			</div>
			</div>
		  </div>
		  <?php	
			
	}
	public function addpackage()
    {
	 
	 ?>
		 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
		  <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
		  <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
		  <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script>     
		 <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
		  <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
			<p><a href="javascript:void(0);"  class="addcls" rel="package_manage">Manage Packages</a></p>
		  </div> 
		  <div class="clearfix"></div>
		  <div class="col-md-12">
			 <div class="user-personal-info">
			  <h4>Packages</h4>
			  <div class="user-info-body">
			<form class="msform" id="toursform" name="toursform" method="post" enctype="multipart/form-data"> 
			<!-- progressbar -->
			<ul id="progressbarpackage" class="progressbar">
                <li class="active">Basic info</li>
                <li>Contact info:</li>
                <li>Rules </li>
                <li>Location </li>
                <li class="big-text">pricing & cancellation</li>
                <li>Schedule</li>
                <li>Gallery</li>
		   </ul>
			<!-- fieldsets -->
			<fieldset id="package1" class="progressbarpackage">
               <input type="hidden" name="main_categoryID" value="9" />
				<h2 class="fs-title">Basic info</h2>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_package"  data-rule-required='true'/></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" id="form_title_arb_package"  placeholder="Arabic Title"  data-rule-required='true' dir="rtl"/></div>
				<div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_image" placeholder="Titile" id="form_image_package" /></div>
				<div style="clear:both"></div>
                <div class="col-md-6 col-sm-6">
				<select name="categoryID" class="form-control" id="categoryID_package">
                  <option selected="selected" value="">Select category</option>
                     <?php 
					   $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>9,'parent_id'=>'0','category_status'=>'active')); 
						if(count($category_accom)>0)
						{
							foreach($category_accom as $row)
							{
							?>
							<option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
							<?php 
							}
						} ?>
				
				</select>
				</div>
				<div class="col-md-6 col-sm-6">
				<select name="subcategoryID" class="form-control" id="subcategoryID_package">
				   <option selected="selected" value="">Select subcategory</option>
				</select>
				</div>
				<div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_package" value="" placeholder="duration"></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_package" class="form-control" placeholder="Description"></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_package" class="form-control" placeholder="Arbic Description" dir="rtl"></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_package" class="form-control" placeholder="Why you"></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_package" class="form-control" placeholder="arbic Why you" dir="rtl" ></textarea></div>
				<!--<div class="col-md-12 col-sm-12">  <textarea name="form_address" id="form_address_package" class="form-control" placeholder="Address"></textarea></div>-->
				<div style="clear:both;"></div>
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="package2"  class="progressbarpackage">
					<h2 class="fs-title">Contact info</h2>
					<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_package" placeholder="Name" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name_arb" id="form_name_arb_package" placeholder="Arbic Name" dir="rtl"  /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_package" placeholder="position" /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position_arb" id="form_position_arb_package" placeholder="Arbic position" dir="rtl" /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_package" placeholder="Email" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_package" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);" /></div>
					<input type="button" name="previous" class="previous action-button" value="Previous" />
					<input type="button" name="next" class="next action-button" value="Next" />
			   </fieldset>
			<fieldset id="package3" class="progressbarpackage">
				<h2 class="fs-title">Rules</h2>
				<!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
				<div class="col-md-6 col-sm-6">
				<select name="form_gender" class="form-control" id="form_gender_package">
				   <option selected="selected" value="">Select gender</option>
				   <option value="none">none</option>
				   <option value="Male">Male</option>
				   <option value="Female">Female</option>
				</select>
				</div>
				<div class="col-md-6 col-sm-6"><input type="text" onkeypress="return OnlyNumericKeys(event);"  name="form_age" id="form_age_package" placeholder="Age" /></div>
				<div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);" name="form_weight" id="form_weight_package" placeholder="Weight" /></div>
				<div class="col-md-6 col-sm-6">
				<span class="allowed-span">Allowed</span> 
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
				   ?>
						<label class="labelcheck"><input type="checkbox" name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				   <!--<textarea name="form_what_allowed" id="form_what_allowed_package" class="form-control" placeholder="What allowed"></textarea>-->
				</div>
				<div class="col-md-6 col-sm-6">
				   <span class="allowed-span">Not Allowed </span>
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
				   ?>
						<label class="labelcheck"><input type="checkbox" name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				<!--<textarea name="form_not_allowed" id="form_not_allowed_package" class="form-control" placeholder="What not allowed"></textarea>-->
				</div>
				<div class="row">
				<div class="col-md-6 col-sm-6">
					<span class="allowed-span">What Included</span> 

					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
					   ?>
							<label class="labelcheck"><input type="checkbox" name="form_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					   <!--<textarea name="form_what_allowed" id="form_what_allowed_package" class="form-control" placeholder="What allowed"></textarea>-->
					</div>
				   <div class="col-md-6 col-sm-6">
					   <span class="allowed-span">What not Included</span>
					   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
					   ?>
							<label class="labelcheck"><input type="checkbox" name="form_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_package" class="form-control" placeholder="What not allowed"></textarea>--></div> 
				</div>
				<div class="row">
			  <div class="col-md-6 col-sm-6">
				  <span class="allowed-span">Requirement </span>
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
					   ?>
							<label class="labelcheck"><input type="checkbox" name="form_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_package" class="form-control" placeholder="What not allowed"></textarea>--></div>
			</div>
				<div class="col-md-6 col-sm-6"><textarea name="form_additional_info" id="form_additional_info_package" class="form-control" placeholder="Additional info"></textarea></div>
				 <div class="col-md-6 col-sm-6"><textarea name="form_additional_info_arb" id="form_additional_info_arb_package" class="form-control" placeholder="Arbic Additional info" dir="rtl"></textarea></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>   
			<fieldset id="package7" class="progressbarpackage">
				<h2 class="fs-title">Location</h2>
				<div class="col-md-12 col-sm-12">
				<input type="text" name="form_location_address" id="form_location_address_package" placeholder="Location Address" />
				</div>
				<div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="form_location" id="form_location_package" placeholder="Location" /></div>
				<div class="col-md-12 col-sm-12"> <div id="gmap_package" style="width:100%;height:200px;"></div></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset  id="package4" class="progressbarpackage">
			   <h2 class="fs-title">Pricing & cancellation policy </h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="row">
				  <!-- New Form Add HereNed here-->
				  <div class="col-xs-10">
						  <div class="col-xs-4">Family</div>
						   <div class="col-xs-4">
								<label class="yeslabel">
								   <input type="checkbox" class="chk_click" id="form_family_member_package" rel='form_family_member_packagediv' value="yes" name='form_family'>
									<span class="yeslabeltext"> yes</span>
								</label>
						   </div>
					  </div>
				  <div class="col-xs-10" id="form_family_member_packagediv" style="display:none;">
				   <div class="col-xs-4">&nbsp;</div>
					<div class="col-xs-3">
					 <input type="text" id="form_family_member_package" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" />
				   </div>
				   <div class="col-xs-3">
					 <input type="text" id="form_family_price_package" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" />
				   </div>
				  </div>
				  <div class="col-xs-10">
					<div class="col-xs-4">Children Less Than</div>
					<div class="col-xs-3">
					  <select id="form_lessthan_age_package" class="form-control" name="form_lessthan_age">
						<?php for($i=0;$i<=18;$i++){ ?>
						<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					  </select>
					</div>
					<div class="col-xs-3">
					 <input type="text" id="form_lessthan_price_package" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Price</div>
					<div class="col-xs-6">
					  <input type="text" name="form_member_price" id="form_member_price_package" placeholder="Price" onkeypress="return OnlyNumericKeys(event);" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Group</div>
					<div class="col-xs-3">
					  <input type="text" name="form_min_member" id="form_min_member_package" placeholder="Min"  onkeypress="return OnlyNumericKeys(event);" />
					</div>
					<div class="col-xs-3">
					  <input type="text" name="form_max_member" id="form_max_member_package" placeholder="Max"  onkeypress="return OnlyNumericKeys(event);" />
					</div>
				   </div> 
				   <div class="col-xs-10">
						<div class="col-xs-4">Group Price</div>
						<div class="col-xs-6">
						  <input type="text" name="form_grp_price" id="form_grp_price_package" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" />
						</div>
				   </div>   
				   
				  <div class="clearfix"></div>
				</div>
				<div class="row">
				  <div class="col-xs-10">
					<div class="col-xs-4">Bird discount</div>
						<div class="col-xs-3" data-toggle="buttons">
						   <input name="form_bird_days_before" id="form_bird_days_package" value="" placeholder="Bird days befor" type="text"> 
					   </div>	
					   <div data-toggle="buttons" class="col-xs-3">
						   <input placeholder="Bird discount" id="form_bird_package" name="form_bird_discount" value="" type="text"> 
						</div>
					</div>
				</div>
				<div class="row">
				   <div class="col-xs-10">
					 <div class="col-xs-4">Discount</div>
					 <div class="col-xs-4" data-toggle="buttons">
						<label class="btn discountthree active">
						<input id="tours_discount" value="on" class="tours_discount" name="form_discount" checked="checked" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
						</label>
						<label class="btn discountthree">
						<input id="tours_discount1" value="off" class="tours_discount" name="form_discount" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
						</label>
					</div>
				   </div>
				</div>
				<div class="row" id="tours_discount_on" style="display: none;">
				   <div class="col-xs-10">
						<div class="col-xs-4">Discount Price</div>
						<div class="col-sm-4">
						   <input onkeypress="return OnlyNumericKeys(event);" class="form-control" name="form_pecentage_discount" id="tours_recreation" value="" placeholder="discount price in percantage" type="text">
						</div>
					</div>  
					<div class="col-xs-10">
						<div class="col-xs-4">Discount From date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_from_expiry" id="tours_discount_from_expiry" value="" placeholder="From date" type="text">
						</div>
					</div>
					<div class="col-xs-10">
						<div class="col-xs-4">Discount To date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_to_expiry" id="tours_discount_to_expiry" value="" placeholder="From date" type="text">
						</div>
					</div>    	
				</div>
				<div class="row">
				   <div class="col-xs-10">
						<div class="col-xs-4">Cancellation and refund policy: </div>
						<div class="col-xs-4" data-toggle="buttons">
							<label class="btn amenitiesform active">
							<input type="radio" id="form_refundable_package" rel='form_refundable_package' value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
							</label>
							<label class="btn amenitiesform">
							<input type="radio" id="form_refundable_package1" rel='form_refundable_package' value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
							</label>
						</div>
					</div>
					<div class="col-md-12">
					  <div id="form_refundable_package_yes" style="display:none;">
						  <div class="col-xs-3">chargeable if cancelled before: </div>
						  <div class="col-xs-8">
							<div class="col-xs-5">
						   <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_package" value="" placeholder="% chargeable if cancelled before "></div>
						   <div class="col-xs-4">
							<input type="input" placeholder="hrs" value="" id="form_not_cancel_package" name="form_not_cancel_hrs" class="form-control">
							</div>
						  </div>
						 <div class="clr"></div>
						 <div class="col-md-12">
							  <div class="col-xs-3"></div>
							   <div class="col-xs-6" style="margin-left:10px;">                    
								<input type="input" placeholder="non refundable price" value="" id="form_not_cancel_package" name="form_not_cancel" class="form-control">
							  </div>                   
							</div>
						</div>
					</div>
				</div>
			   
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="package5" class="progressbarpackage">
			<h2 class="fs-title">Schedule</h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div id="weekly_package">
					   <input type="hidden" id="form_availalbe_to_package"  rel="tours" value="weekly"  name="form_availalbe_for">
					   <div class="row">
					<div class="col-xs-10">
					  <div class="col-xs-4">From Date</div>
					  <div class="col-xs-6">
						<input type="text" name="form_available_from_date" id="form_available_from_date_package" placeholder="Select available time" />
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">To Date</div>
					   <div class="col-xs-6">
					   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_package"  placeholder="Select available time" />
					 </div>
					</div>
				</div>
					  <?php 
					  $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
					  $cntarr=count($days_arr);
					  if(count($days_arr)>0)
					  {
						for($i=0;$i<$cntarr;$i++)
						{
					   ?>
						<div class="row">
						  <div class="col-xs-10">
							<div class="col-xs-4"><?php if($i==0) {?>Select Days <?php }else{echo ' ';} ?></div>
							<div class="col-xs-3">
							  <label style="margin-bottom:-10px;">
								<div class="col-xs-1">
									<input type="checkbox" class="clsday" name="form_available_day[]" id="form_available_day_package_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>">
								</div>
								<div class="col-xs-3" style="margin-top:10px;"><?php echo ucfirst($days_arr[$i]);?></div>
							  </label>
							 </div>
						  </div> 
						</div>
						<div class="row" id="<?php echo ucfirst($days_arr[$i]);?>" style="display:none;">

						   <div class="col-xs-10">
							  <div class="col-xs-4"><?php echo ' ';?></div>
							  <div class="col-xs-2"><input type="text" name="form_from_time_<?php echo $days_arr[$i]?>[]" id="form_from_time_package<?php echo $i; ?>" class="form_availalbe_from_time_package" placeholder="From time" /></div>
							  <div class="col-xs-2"><input type="text" name="form_to_time_<?php echo $days_arr[$i]?>[]" id="form_to_time_package<?php echo $i; ?>" class="form_availalbe_to_time_package" placeholder="To time" /> </div>
							  <div class="col-xs-2"><input type="text" name="form_day_duration_<?php echo $days_arr[$i]?>[]" id="form_duration_package<?php echo $i; ?>" placeholder="Duration" /> </div>
							  <div class="col-xs-2" style="margin-top:-20px;"><a href="javascript:void(0);" class="addcheck" rel='tours'>Add</a></div>
							</div>
						</div> 
						<?php
						}
					  }
					  ?>
				 </div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point</div>
					   <div class="col-xs-6">
						<textarea name="form_departure" id="form_departure_package" class="form-control" placeholder="Departure Point"></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point address</div>
					   <div class="col-xs-6">
						<textarea name="form_departure_address" id="form_departure_address_package" class="form-control" placeholder="Address"></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_departure_time" id="form_departure_time_package"  placeholder="Departure Time" />
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Reporting Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_reporting_time" id="form_reporting_time_package"  placeholder="Departure Time" />
					 </div>
					</div>
				</div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="package6" class="progressbarpackage">
				<h2 class="fs-title">Gallery</h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="row">
					<div class="col-xs-10">
						<input type="file" style="padding:0px;" multiple name="form_images_name[]" id="form_images_name" class="form-control" >                      
					</div>
				</div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="submit" id="submit_package" name="tourssubmit" class="submit action-button" value="Submit" />   
			</fieldset>
			</form> 
			<script type="text/javascript">
                  jQuery(document).ready(function(e) {
                     jQuery('#form_available_from_date_package').datetimepicker({
                          format:'Y-m-d H:i:s',
                          onShow:function( ct ){
                           this.setOptions({
                            maxDate:jQuery('#form_availalbe_to_date_package').val()?jQuery('#form_availalbe_to_date_package').val():false
                           })
                          },
                     
                     });
                    jQuery('#form_availalbe_to_date_package').datetimepicker({
                          format:'Y-m-d H:i:s',
                          onShow:function( ct ){
                           this.setOptions({
                            minDate:jQuery('#form_available_from_date_package').val()?jQuery('#form_available_from_date_package').val():false
                           })
                          },
                    });
                    jQuery('.form_availalbe_from_time_package').datetimepicker({
                          formatTime:'H:i:s',
                          format:	'H:i:s',
                          datepicker: false,
                          onShow:function( ct ){
                           this.setOptions({
                            maxDate:jQuery('.form_availalbe_to_time_package').val()?jQuery('.form_availalbe_to_time_package').val():false
                           })
                          },
                     
                     });
                    jQuery('.form_availalbe_to_time_package').datetimepicker({
                          formatTime:'H:i:s',
                          format:	'H:i:s',
                          datepicker: false,
                          onShow:function( ct ){
                           this.setOptions({
                            minDate:jQuery('.form_available_from_time_package').val()?jQuery('.form_available_from_time_package').val():false
                           })
                          },
                    });
                    jQuery('#tours_discount_from_expiry').datetimepicker({
                              format:'Y-m-d H:i:s',
                              onShow:function( ct ){
                               this.setOptions({
                                maxDate:jQuery('#tours_discount_to_expiry').val()?jQuery('#tours_discount_to_expiry').val():false
                               })
                              },
                         
                         });
                    jQuery('#tours_discount_to_expiry').datetimepicker({
                              format:'Y-m-d H:i:s',
                              onShow:function( ct ){
                               this.setOptions({
                                minDate:jQuery('#tours_discount_from_expiry').val()?jQuery('#tours_discount_from_expiry').val():false
                               })
                              },
                        });
                 });
                    function initAutocomplete() {
                    var map = new google.maps.Map(document.getElementById('gmap_package'), {
                      center: {lat: -33.8688, lng: 151.2195},
                      zoom: 13,
                      mapTypeId: 'roadmap'
                    });
            
                    // Create the search box and link it to the UI element.
                    var input = document.getElementById('form_location_address_package');
                    var searchBox = new google.maps.places.SearchBox(input);
                   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            
                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener('bounds_changed', function() {
                      searchBox.setBounds(map.getBounds());
                    });
            
                    var markers = [];
                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener('places_changed', function() {
                      var places = searchBox.getPlaces();
            
                      if (places.length == 0) {
                        return;
                      }
            
                      // Clear out the old markers.
                      markers.forEach(function(marker) {
                        marker.setMap(null);
                      });
                      markers = [];
            
                      // For each place, get the icon, name and location.
                      var bounds = new google.maps.LatLngBounds();
                      
                      places.forEach(function(place) {
                        if (!place.geometry) {
                          console.log("Returned place contains no geometry");
                          return;
                        }
                        var fisrtStr=place.geometry.location.toString().trim();
                        fisrtStr = fisrtStr.slice(1, -1);;
                        document.getElementById("form_location_package").value = fisrtStr;
                        var icon = {
                          url: place.icon,
                          size: new google.maps.Size(71, 71),
                          origin: new google.maps.Point(0, 0),
                          anchor: new google.maps.Point(17, 34),
                          scaledSize: new google.maps.Size(25, 25)
                        };
                        
                        // Create a marker for each place.
                        markers.push(new google.maps.Marker({
                          map: map,
                          icon: icon,
                          title: place.name,
                          position: place.geometry.location
                        }));
                        
                        
                        if (place.geometry.viewport) {
                          // Only geocodes have viewport.
                          bounds.union(place.geometry.viewport);
                        } else {
                          bounds.extend(place.geometry.location);
                        }
                      });
                      map.fitBounds(bounds);
                    });
                  }
                function clearOverlays() {
                  for (var i = 0; i < markersArray.length; i++ ) {
                    markersArray[i].setMap(null);
                  }
                  markersArray.length = 0;
                }
                function getAddress(latLng) {
                geocoder.geocode( {'latLng': latLng},
                  function(results, status) {
                    if(status == google.maps.GeocoderStatus.OK) {
                      if(results[0]) {
                          //alert(results[0].formatted_address);
                        document.getElementById("form_location_address_package").value = results[0].formatted_address;
                      }
                      else {
                         // alert("No results");
                       // document.getElementById("address").value = "No results";
                      }
                    }
                    else {
                        //alert(status);
                      //document.getElementById("address").value = status;
                    }
                  });
                }
                $(document).on('click','.action-button', function(){
                     initAutocomplete();
                });
                window.onload = function () { initAutocomplete() };
            </script>
			</div>
			</div>
		  </div>
		  <?php	
	}
	public function addadventure()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="adventure_manage">Manage Adventure</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Adventure</h4>
          <div class="user-info-body">
        <form class="msform" id="adventureform" name="adventureform" method="post" enctype="multipart/form-data"> 
            <!-- progressbar -->
            <ul id="progressbaradventure" class="progressbar">
                <li class="active ">Adventure info</li>
                <li>Contact info:</li>
                <li>Rules </li>
                <li>Location </li>
                <li class="big-text">Pricing & cancellation  </li>
                <li>Gallery :</li>
                <li>Schedule</li>
            </ul>
            <!-- fieldsets -->
            <fieldset id="adventure1"  class="progressbaradventure">
            <h2 class="fs-title">Adventure info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_adventure"  data-rule-required='true'/></div>
              <div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" placeholder="Arbic Title" id="form_title_arb_adventure"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6"><input type="file" style="padding:0px;" name="form_image" placeholder="Titile" id="form_image_adventure" /></div>
            <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_adventure">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'3','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
                </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_adventure">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_adventure" value="" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_adventure" class="form-control" placeholder="Description"></textarea></div>
             <div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_adventure" class="form-control" placeholder="Arbic Description" dir="rtl"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_youadventure" class="form-control" placeholder="Why you"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_adventure" class="form-control" placeholder="Arbic Why you" dir="rtl"></textarea></div>
           <!-- <div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_adventure" class="form-control" placeholder="Address"></textarea></div>-->
            <div style="clear:both;"></div>
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure2"  class="progressbaradventure">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_adventure" placeholder="Name" /></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_name_arb" id="form_name_arb_adventure" placeholder="Arbic Name" dir="rtl" /></div>
                <div class="col-md-6 col-sm-6">  <input type="text" name="form_position" id="form_position_adventure" placeholder="position" /></div>
                <div class="col-md-6 col-sm-6">  <input type="text" name="form_position_arb" id="form_position_arb_adventure" placeholder="Arbic position" dir="rtl" /></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_email"  id="form_email_adventure" placeholder="Email" /></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_adventure" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);" /></div>
                    <input type="button" name="previous" class="previous action-button" value="Previous" />
                    <input type="button" name="next" class="next action-button" value="Next" />
               </fieldset>
            <fieldset id="adventure3"  class="progressbaradventure">
                <h2 class="fs-title">Rules</h2>
                <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
                <div class="col-md-6 col-sm-6">
                <select name="form_gender" class="form-control" id="form_gender_adventure">
                   <option  selected="selected" >select gender</option>
                   <option value="none">none</option>
                   <option value="Male">Male</option>
                   <option value="Female">Female</option>
                </select>
                </div>
                <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_adventure" placeholder="Age" /></div>
                <div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_adventure" placeholder="Weight" /></div>
                <div class="col-md-6 col-sm-6">
            <span class="allowed-span">Allowed</span> 
               <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
			   if(count($fetch_array))
			   {
				   foreach($fetch_array as $row)
				   {
			   ?>
                    <label class="labelcheck"><input type="checkbox" name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
               <?php
				   }
			   }
			   ?>
               <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
            </div>
                <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">Not Allowed </span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>-->
                </div>
                <div class="row">
                <div class="col-md-6 col-sm-6">
                    <span class="allowed-span">What Included</span> 
                       <?php 
                       $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
                       if(count($fetch_array))
                       {
                           foreach($fetch_array as $row)
                           {
                       ?>
                            <label class="labelcheck"><input type="checkbox" name="form_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                       <?php
                           }
                       }
                       ?>
                       <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                    </div>
                   <div class="col-md-6 col-sm-6">
                       <span class="allowed-span">What not Included</span>
                       <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
                       if(count($fetch_array))
                       {
                           foreach($fetch_array as $row)
                           {
                       ?>
                            <label class="labelcheck"><input type="checkbox" name="form_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                       <?php
                           }
                       }
                       ?>
                    <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
                </div>
                <div class="row">
              <div class="col-md-6 col-sm-6">
                  <span class="allowed-span">Requirement </span>
                       <?php 
                       $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
                       if(count($fetch_array))
                       {
                           foreach($fetch_array as $row)
                           {
                       ?>
                            <label class="labelcheck"><input type="checkbox" name="form_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
                       <?php
                           }
                       }
                       ?>
                    <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
            </div>
                <div class="col-md-6 col-sm-6"><textarea name="form_additional_info" id="form_additional_info_adventure" class="form-control" placeholder="Additional info"></textarea></div>
                 <div class="col-md-6 col-sm-6"><textarea name="form_additional_info_arb" id="form_additional_info_arb_adventure" class="form-control" placeholder="Arabic Additional info" dir="rtl"></textarea></div>
                
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure8"  class="progressbaradventure">
                <h2 class="fs-title">Location</h2>
                <div class="col-md-12 col-sm-12">
                <input type="text" name="form_location_address" id="form_location_address_adventure" placeholder="Location Address" />
                </div>
                <div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="form_location" id="form_location_adventure" placeholder="Location" /></div>
                <div class="col-md-12 col-sm-12"><div id="gmap_adventure" style="width:100%;height:200px;"></div></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset  id="adventure4" class="progressbaradventure">
                 <h2 class="fs-title">pricing & cancellation policy </h2>
                 <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                 <div class="row">
                  <!-- New Form Add HereNed here-->
                 <?php /*<div class="col-xs-10">
                    <div class="col-xs-4">Children Age</div>
                    <div class="col-xs-3">
                      <select id="form_min_age_adventure" class="form-control" name="form_min_age">
                          <?php for($i=0;$i<=18;$i++){ ?>
                          <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                          <?php } ?>
                      </select>
                    </div>
                     <div class="col-xs-3">
                     <select id="form_max_age_adventure" class="form-control" name="form_max_age">
                           <?php for($i=0;$i<=18;$i++){ ?>
                           <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                           <?php } ?>
                      </select>
                    </div>
                   </div>  
                  <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_price" id="form_price_adventure" placeholder="Price" onkeypress="return OnlyNumericKeys(event);"  />
                    </div>
                   </div>*/ ?>  
                  <div class="col-xs-10">
                      <div class="col-xs-4">Family</div>
                       <div class="col-xs-4">
                            <label >
                               <input type="checkbox" class="chk_click" id="form_family_member_adventure" rel='form_family_member_adventurediv' value="yes" name='form_family'><span> yes</span>
                            </label>
                       </div>
                  </div>
                  <div class="col-xs-10" id="form_family_member_adventurediv" style="display:none;">
                   <div class="col-xs-4">&nbsp;</div>
                    <div class="col-xs-3">
                     <input type="text" id="form_family_member_adventure" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" />
                   </div>
                   <div class="col-xs-3">
                     <input type="text" id="form_family_price_adventure" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" />
                   </div>
                  </div>
                  <div class="col-xs-10">
                    <div class="col-xs-4">Children Less Than</div>
                    <div class="col-xs-3">
                      <select id="form_lessthan_age_adventure" class="form-control" name="form_lessthan_age">
                        <?php for($i=0;$i<=18;$i++){ ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-xs-3">
                     <input type="text" id="form_lessthan_price_adventure" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                  </div> 
                  <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_member_price" id="form_member_price_adventure" placeholder="Price" onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                  </div> 
                  <div class="col-xs-10">
                    <div class="col-xs-4">Group</div>
                    <div class="col-xs-3">
                      <input type="text" name="form_min_member" id="form_min_member_adventure" placeholder="Min"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                    <div class="col-xs-3">
                      <input type="text" name="form_max_member" id="form_max_member_adventure" placeholder="Max"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                   </div>  
                  <div class="col-xs-10">
                    <div class="col-xs-4">Group Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_grp_price" id="form_grp_price_adventure" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                  </div>
                  <div class="clr"></div>
                 <!--
                 <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_price" id="form_price_tours" placeholder="Price" />
                    </div>
                   </div>
                </div>
                <?php /*?><div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Unit</div>
                    <div class="col-xs-6">
                       <select name="price_unit" class="form-control" id="price_unit">
                           <option selected="selected">none</option>
                           <?php for($i=0;$i<20;$i++){ ?>
                           <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                           <?php } ?>
                       </select>
                    </div>
                   </div>
                </div><?php */?>
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Children price demo</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_child_price" id="form_child_price_tours" placeholder="Children price" />
                    </div>
                   </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Children Age</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_child_age" id="form_child_age_tours" placeholder="Children Age" />
                    </div>
                   </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Group price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_grp_price" id="form_grp_price_tours" placeholder="Group price" />
                    </div>
                   </div>
                </div>-->
                </div>
               <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Bird discount</div>
                        <div class="col-xs-3" data-toggle="buttons">
                           <input name="form_bird_days_before" id="form_bird_days_tours" value="" placeholder="Bird days befor" type="text"> 
                       </div>	
                       <div data-toggle="buttons" class="col-xs-3">
                           <input placeholder="Bird discount" id="form_bird_tours" name="form_bird_discount" value="" type="text"> 
                        </div>
                    </div>
            	</div>
               <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Discount</div>
                     <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn discountthree active">
                        <input id="adventure_discount" value="on"  class="adventure_discount" name="form_discount" checked="checked" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
                        </label>
                        <label class="btn discountthree">
                        <input id="adventure_discount1" value="off" class="adventure_discount" name="form_discount" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
                        </label>
                    </div>
       				 </div>
        		</div>
               <div class="row" id="adventure_discount_on" style="display: none;">
                             <div class="col-xs-10">
                                <div class="col-xs-4">Discount Price</div>
                                <div class="col-sm-4">
                                   <input onkeypress="return OnlyNumericKeys(event);" class="form-control" name="form_pecentage_discount" id="adventure_recreation" value="" placeholder="discount price in percantage" type="text">
                                </div>
                            </div>  
                            <div class="col-xs-10">
                                <div class="col-xs-4">Discount From date</div>
                                <div class="col-sm-6">
                                   <input class="form-control" name="form_discount_from_expiry" id="adventure_discount_from_expiry" value="" placeholder="From date" type="text">
                                </div>
                            </div>
                            <div class="col-xs-10">
                                <div class="col-xs-4">Discount To date</div>
                                <div class="col-sm-6">
                                   <input class="form-control" name="form_discount_to_expiry" id="adventure_discount_to_expiry" value="" placeholder="From date" type="text">
                                </div>
                            </div>    
       			   </div>
               <div class="row">
                   <div class="col-xs-10">
                        <div class="col-xs-4">Cancellation and refund policy: </div>
                        <div class="col-xs-4" data-toggle="buttons">
                            <label class="btn amenitiesform active">
                            <input type="radio" id="form_refundable_adventure" rel='form_refundable_adventure' value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                            </label>
                            <label class="btn amenitiesform">
                            <input type="radio" id="form_refundable_adventure1" rel='form_refundable_adventure' value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                            </label>
                        </div>
                  </div>
                   <div class="col-md-12">
                   <div id="form_refundable_adventure_yes" style="display:none;">
                      <div class="col-xs-3">chargeable if cancelled before: </div>
                      <div class="col-xs-8">
                        <div class="col-xs-5">
                        <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_adventure" value="" placeholder="% chargeable if cancelled before "></div>
                       <div class="col-xs-4">
                         <input type="input" placeholder="hrs" value="" id="form_not_cancel_adventure" name="form_not_cancel_hrs" class="form-control">
                        </div>
                      </div>
                     <div class="clr"></div>
                     <div class="col-md-12">
                          <div class="col-xs-3"></div>
                           <div class="col-xs-6" style="margin-left:10px;">                    
                             <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_adventure" value="" placeholder="non refundable price">
                          </div>                   
                        </div>
                    </div>
                  </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
              </fieldset>
            <fieldset id="adventure6" class="progressbaradventure">
            <h2 class="fs-title">Schedule</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div id="weekly_adventure">
                   <input type="hidden" id="form_availalbe_for_adventure" rel="adventure" value="weekly"  name='form_availalbe_for' >
                   <div class="row">
                    <div class="col-xs-10">
                      <div class="col-xs-4">From Date</div>
                      <div class="col-xs-6">
                         <input type="text" name="form_available_from_date" id="form_available_from_date_adventure" placeholder="Select available time" />
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">TO date</div>
                       <div class="col-xs-6">
                        <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_adventure"  placeholder="Select available time" />
                     </div>
                    </div>
                </div> 
                  <?php 
				  $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  $cntarr=count($days_arr);
				  if(count($days_arr)>0)
				  {
					for($i=0;$i<$cntarr;$i++)
					{
				   ?>
                    <div class="row">
                      <div class="col-xs-10">
                        <div class="col-xs-4"><?php if($i==0) {?>Select Days <?php }else{echo ' ';} ?></div>
                        <div class="col-xs-3">
                          <label style="margin-bottom:-10px;">
                            <div class="col-xs-1">
                                <input type="checkbox" class="clsday" name="form_available_day[]" id="form_available_day_adventure_<?php echo $days_arr[$i]?>" value="<?php echo 'adventure_'.$days_arr[$i]?>">
                            </div>
                            <div class="col-xs-3" style="margin-top:10px;"><?php echo ucfirst($days_arr[$i]);?></div>
                          </label>
                         </div>
                      </div> 
                    </div>
                    <div class="row" id="<?php echo 'adventure_'.ucfirst($days_arr[$i]);?>" style="display:none;">
                       <div class="col-xs-10">
                          <div class="col-xs-4"><?php echo ' ';?></div>
                          <div class="col-xs-2"><input type="text" name="form_from_time_adventure_<?php echo $days_arr[$i]?>[]" id="form_from_time_adventure<?php echo $i; ?>" class="form_availalbe_from_time_adventure" placeholder="From time" /></div>
                          <div class="col-xs-2"><input type="text" name="form_to_time_adventure_<?php echo $days_arr[$i]?>[]" id="form_to_time_adventure<?php echo $i; ?>" class="form_availalbe_to_time_adventure" placeholder="To time" /> </div>
                          <div class="col-xs-2"><input type="text" name="form_day_duration_adventure_<?php echo $days_arr[$i]?>[]" id="form_duration_adventure<?php echo $i; ?>" placeholder="Duration" /> </div>
                          <div class="col-xs-2" style="margin-top:-20px;"><a href="javascript:void(0);" class="addcheck" rel='adventure'>Add</a></div>
                        </div>
                    </div> 
                    <?php
					}
				  }
				  ?>
            </div>   
            	<div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Departure Point</div>
                       <div class="col-xs-6">
                        <textarea name="form_departure" id="form_departure_adventure" class="form-control" placeholder="Departure Point"></textarea>
                     </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Departure Point address</div>
                       <div class="col-xs-6">
                        <textarea name="form_departure_address" id="form_departure_address_adventure" class="form-control" placeholder="Address"></textarea>
                     </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Departure Time</div>
                       <div class="col-xs-6">
                        <input type="text" name="form_departure_time" id="form_departure_time_adventure"  placeholder="Departure Time" />
                     </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Reporting Time</div>
                       <div class="col-xs-6">
                        <input type="text" name="form_reporting_time" id="form_reporting_time_adventure"  placeholder="Departure Time" />
                     </div>
                    </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure7" class="progressbaradventure">
            <h2 class="fs-title">Gallery</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                    <input type="file" style="padding:0px;" multiple name="form_images_name[]" id="form_images_adventure" class="form-control" >                      
                </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_adventure" name="adventuresubmit" class="submit action-button" value="Submit" />
        </fieldset>
        </form> 
        <script type="text/javascript">
	 jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_adventure').datetimepicker({
			  format:'Y-m-d',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_adventure').val()?jQuery('#form_availalbe_to_date_adventure').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_adventure').datetimepicker({
			  format:'Y-m-d',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_adventure').val()?jQuery('#form_available_from_date_adventure').val():false
			   })
			  },
		});
		jQuery('#adventure_discount_from_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#adventure_discount_to_expiry').val()?jQuery('#adventure_discount_to_expiry').val():false
				   })
				  },
			 
			 });
		jQuery('#adventure_discount_to_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#adventure_discount_from_expiry').val()?jQuery('#adventure_discount_from_expiry').val():false
				   })
				  },
			});
		jQuery('.form_availalbe_from_time_adventure').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_adventure').val()?jQuery('.form_availalbe_to_time_adventure').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_adventure').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_adventure').val()?jQuery('.form_available_from_time_adventure').val():false
			   })
			  },
		    });
          });
			function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('gmap_adventure'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('form_location_address_adventure');
        var searchBox = new google.maps.places.SearchBox(input);
       // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
		  
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
			var fisrtStr=place.geometry.location.toString().trim();
			fisrtStr = fisrtStr.slice(1, -1);;
			document.getElementById("form_location_adventure").value = fisrtStr;
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };
			
            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));
			
			
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }  
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("form_location_address_adventure").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
				$(document).on('click','.action-button', function(){
					 initAutocomplete();
				});
				window.onload = function () { initAutocomplete() };
         </script>
        </div>
        </div>
      </div>
      <?php		
	}
    public function addrecreations()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="recreations_manage">Manage Recreation</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Recreation</h4>
          <div class="user-info-body">
        <form class="msform" id="recreationform" name="recreationform" method="post" enctype="multipart/form-data"> 
        <!-- progressbar -->
            <ul id="progressbarrecreation" class="progressbar">
                <li class="active ">Recreations info</li>
                <li>Contact info:</li>
                <li>Rules </li>
                <li>Location </li>
                <li class="big-text">Pricing & cancellation </li>
                <li>Schedule</li>
                <li>Gallery :</li>
            </ul>
        <!-- fieldsets -->
        <fieldset id="recreation1" class="progressbarrecreation">
              <h2 class="fs-title">Recreations info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_recreation"  data-rule-required='true'/></div>
             <div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" placeholder="Arbic Title" id="form_title_arb_recreation"  data-rule-required='true' dir="rtl"/></div>
            <div class="col-md-6 col-sm-6"> <input type="file" style="padding:0px;" name="form_image" placeholder="Titile" id="form_image_recreation" /></div>
            <div class="col-md-6 col-sm-6">
             <select name="categoryID" class="form-control" id="categoryID_recreation">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'4','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_recreation">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_recreation" value="" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_recreation" class="form-control" placeholder="Description"></textarea></div>
             <div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_recreation" class="form-control" placeholder="Arbic Description" dir="rtl"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_recreation" class="form-control" placeholder="Why you"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_recreation" class="form-control" placeholder="arbic Why you" dir="rtl"></textarea></div>
           <!--<div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_recreation" class="form-control" placeholder="Address"></textarea></div>-->
            <div style="clear:both;"></div>  
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation2"  class="progressbarrecreation">
                <h2 class="fs-title">Contact info</h2>
                <h3 class="fs-subtitle">We will never sell it</h3>
                <div class="col-md-6 col-sm-6"><input type="text" name="form_name" id="form_name_recreation" placeholder="Name" /></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="form_name_arb" id="form_name_arb_recreation" placeholder="Arbic Name" dir="rtl" /></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_recreation" placeholder="position" /></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="form_position_arb" id="form_position_arb_recreation" placeholder="Arbic position" dir="rtl" /></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_recreation" placeholder="Email" /></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="form_mobile"  id="form_mobile_recreation" placeholder="mobile number"   onkeypress="return OnlyNumericKeys(event);"/></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
           </fieldset>
        <fieldset id="recreation3"  class="progressbarrecreation">
            <h2 class="fs-title">Rules</h2>
            <h3 class="fs-subtitle">Your presence on the social network</h3>
            <div class="col-md-6 col-sm-6">
            <select name="form_gender" class="form-control" id="form_gender_recreation">
               <option selected="selected" value="">Select gender</option>
               <option value="none">none</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_recreation" placeholder="Age" /></div>
            <div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_recreation" placeholder="Weight" /></div>
            <div class="col-md-6 col-sm-6">
              <span class="allowed-span">Allowed</span> 
               <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
			   if(count($fetch_array))
			   {
				   foreach($fetch_array as $row)
				   {
			   ?>
                    <label class="labelcheck"><input type="checkbox" name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
               <?php
				   }
			   }
			   ?>
               <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
            </div>
            <div class="col-md-6 col-sm-6">
               <span class="allowed-span">Not Allowed </span>
               <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
               if(count($fetch_array))
               {
                   foreach($fetch_array as $row)
                   {
               ?>
                    <label class="labelcheck"><input type="checkbox" name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
               <?php
                   }
               }
               ?>
            <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>-->
            </div>
            <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">What Included</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="form_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
               <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">What not Included</span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                   ?>
                        <label class="labelcheck"><input type="checkbox" name="form_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
            </div>
            <div class="row">
             <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">Requirement </span>
				   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
                    ?>
                       <label class="labelcheck"><input type="checkbox" name="form_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>-->
                </div>
            </div>
            <div class="col-md-12 col-sm-12"><textarea name="form_additional_info" id="form_additional_info_recreation" class="form-control" placeholder="Additional Info"></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_additional_info_arb" id="form_additional_info_arb_recreation" class="form-control" placeholder="Arbic Additional Info" dir="rtl"></textarea></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation7"  class="progressbarrecreation">
            <h2 class="fs-title">Location</h2>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location_address" id="form_location_address_recreation" placeholder="Location Address" />
            </div>
            <div class="col-md-12 col-sm-12"> <input type="text" name="form_location" id="form_location_recreation" placeholder="Location" /></div>
            <div class="col-md-12 col-sm-12"><div id="gmap_recreation" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset  id="recreation4"  class="progressbarrecreation">
                 <h2 class="fs-title">Price </h2>
                 <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                  <div class="col-xs-10">
                      <div class="col-xs-4">Family</div>
                       <div class="col-xs-4">
                            <label >
                               <input type="checkbox" class="chk_click" id="form_family_member_recreation" rel='form_family_member_recreationdiv' value="yes" name='form_family'><span> yes</span>
                            </label>
                       </div>
                  </div>
                  <div class="col-xs-10" id="form_family_member_recreationdiv" style="display:none;">
                   <div class="col-xs-4">&nbsp;</div>
                    <div class="col-xs-3">
                     <input type="text" id="form_family_member_recreation" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" />
                   </div>
                   <div class="col-xs-3">
                     <input type="text" id="form_family_price_recreation" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" />
                   </div>
                  </div>
                  <div class="col-xs-10">
                    <div class="col-xs-4">Children Less Than</div>
                    <div class="col-xs-3">
                      <select id="form_lessthan_age_recreation" class="form-control" name="form_lessthan_age">
                        <?php for($i=0;$i<=18;$i++){ ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="col-xs-3">
                     <input type="text" id="form_lessthan_price_recreation" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                  </div> 
                  <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_member_price" id="form_member_price_recreation" placeholder="Price" onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                  </div> 
                  <div class="col-xs-10">
                    <div class="col-xs-4">Group</div>
                    <div class="col-xs-3">
                      <input type="text" name="form_min_member" id="form_min_member_recreation" placeholder="Min"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                    <div class="col-xs-3">
                      <input type="text" name="form_max_member" id="form_max_member_recreation" placeholder="Max"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                   </div>  
                  <div class="col-xs-10">
                    <div class="col-xs-4">Group Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_grp_price" id="form_grp_price_recreation" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Bird discount</div>
                        <div class="col-xs-3" data-toggle="buttons">
                           <input name="form_bird_days_before" id="form_bird_days_tours" value="" placeholder="Bird days befor" type="text"> 
                       </div>	
                       <div data-toggle="buttons" class="col-xs-3">
                           <input placeholder="Bird discount" id="form_bird_tours" name="form_bird_discount" value="" type="text"> 
                        </div>
                    </div>
            	</div>
                  <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Discount</div>
                     <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn discountthree active">
                        <input id="recreation_discount" value="on"  class="recreation_discount" name="form_discount" checked="checked" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
                        </label>
                        <label class="btn discountthree">
                        <input id="recreation_discount1" class="recreation_discount" value="off" name="form_discount" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
                        </label>
                    </div>
       				 </div>
        		</div>
                  <div class="row" id="recreation_discount_on" style="display: none;">
                             <div class="col-xs-10">
                                <div class="col-xs-4">Discount Price</div>
                                <div class="col-sm-4">
                                   <input onkeypress="return OnlyNumericKeys(event);" class="form-control" name="form_pecentage_discount" id="ecentage_recreation" value="" placeholder="discount price in percantage" type="text">
                                </div>
                            </div>  
                            <div class="col-xs-10">
                                <div class="col-xs-4">Discount From date</div>
                                <div class="col-sm-6">
                                   <input class="form-control" name="form_discount_from_expiry" id="recreation_discount_from_expiry" value="" placeholder="From date" type="text">
                                </div>
                            </div>
                            <div class="col-xs-10">
                                <div class="col-xs-4">Discount To date</div>
                                <div class="col-sm-6">
                                   <input class="form-control" name="form_discount_to_expiry" id="recreation_discount_to_expiry" value="" placeholder="From date" type="text">
                                </div>
                            </div>    
       			   </div>
                  <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenitiesform active">
                          <input type="radio" id="form_refundable_recreation" rel="form_refundable_recreation" value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn amenitiesform">
                           <input type="radio" id="form_refundable_recreation1" rel="form_refundable_recreation" value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
               </div>
               <div class="col-md-12">
                  <div  id="form_refundable_recreation_yes" style="display:none;">
                      <div class="col-xs-3">chargeable if cancelled before: </div>
                      <div class="col-xs-8">
                        <div class="col-xs-5">
                         <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_recreation" value="" placeholder="% chargeable if cancelled before "></div>
                       <div class="col-xs-4">
                         <input type="input" placeholder="hrs" value="" id="form_not_cancel_recreation" name="form_not_cancel_hrs" class="form-control">
                        </div>
                      </div>
                     <div class="clr"></div>
                     <div class="col-md-12">
                          <div class="col-xs-3"></div>
                           <div class="col-xs-6" style="margin-left:10px;">                    
                             <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_recreation" value="" placeholder="non refundable price">
                          </div>                   
                        </div>
                    </div>
               </div>
            </div>
                 <input type="button" name="previous" class="previous action-button" value="Previous" />
                 <input type="button" name="next" class="next action-button" value="Next" />
         </fieldset>
        <fieldset id="recreation5" class="progressbarrecreation">
        <h2 class="fs-title">Schedule</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <div id="weekly_recreation" >
                <input type="hidden" id="form_availalbe_for_recreation" rel="recreation" value="weekly"  name='form_availalbe_for' >
                <div class="row">
                    <div class="col-xs-10">
                      <div class="col-xs-4">From date</div>
                      <div class="col-xs-6">
                        <input type="text" name="form_available_from_date" id="form_available_from_date_recreation" placeholder="Select available time" />
                      </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">To date</div>
                       <div class="col-xs-6">
                        <input type="text" name="form_available_from_date" id="form_available_to_date_recreation" placeholder="Select available time" />
                     </div>
                    </div>
                </div> 
                  <?php 
				  $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  $cntarr=count($days_arr);
				  if(count($days_arr)>0)
				  {
					for($i=0;$i<$cntarr;$i++)
					{
				   ?>
                    <div class="row">
                      <div class="col-xs-10">
                        <div class="col-xs-4"><?php if($i==0) {?>Select Days <?php }else{echo ' ';} ?></div>
                        <div class="col-xs-3">
                          <label style="margin-bottom:-10px;">
                            <div class="col-xs-1">
                                <input type="checkbox" class="clsday" name="form_available_day[]" id="form_available_day_recreation_<?php echo $days_arr[$i]?>" value="<?php echo 'recreation_'.$days_arr[$i]?>">
                            </div>
                            <div class="col-xs-3" style="margin-top:10px;"><?php echo ucfirst($days_arr[$i]);?></div>
                          </label>
                         </div>
                      </div> 
                    </div>
                    <div class="row" id="<?php echo 'recreation_'.ucfirst($days_arr[$i]);?>" style="display:none;">
                       <div class="col-xs-10">
                          <div class="col-xs-4"><?php echo ' ';?></div>
                          <div class="col-xs-2"><input type="text" name="form_from_time_recreation_<?php echo $days_arr[$i]?>[]" id="form_from_time_recreation<?php echo $i; ?>" class="form_availalbe_from_time_recreation" placeholder="From time" /></div>
                          <div class="col-xs-2"><input type="text" name="form_to_time_recreation_<?php echo $days_arr[$i]?>[]" id="form_to_time_recreation<?php echo $i; ?>" class="form_availalbe_to_time_recreation" placeholder="To time" /> </div>
                          <div class="col-xs-2"><input type="text" name="form_day_duration_recreation_<?php echo $days_arr[$i]?>[]" id="form_duration_recreation<?php echo $i; ?>" placeholder="Duration" /></div>
                          <div class="col-xs-2" style="margin-top:-20px;"><a href="javascript:void(0);" class="addcheck" rel='recreation'>Add</a></div>
                        </div>
                    </div> 
                    <?php
					}
				  }
				 ?>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_recreation" class="form-control" placeholder="Departure Point"></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_recreation" class="form-control" placeholder="Address"></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_recreation"  placeholder="Departure Time" />
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Reporting Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_reporting_time" id="form_reporting_time_recreation"  placeholder="Departure Time" />
                 </div>
                </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation6" class="progressbarrecreation">
            <h2 class="fs-title">Gallery</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                    <input type="file" style="padding:0px;" multiple name="form_images_name[]" id="form_images_recreation" class="form-control" >                      
                </div>
            </div>
             <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_recreation" name="recreationsubmit" class="submit action-button" value="Submit" />
         </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_recreation').datetimepicker({
			  format:'Y-m-d',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_recreation').val()?jQuery('#form_availalbe_to_date_recreation').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_recreation').datetimepicker({
			  format:'Y-m-d',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_recreation').val()?jQuery('#form_available_from_date_recreation').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_recreation').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_recreation').val()?jQuery('.form_availalbe_to_time_recreation').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_recreation').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_recreation').val()?jQuery('.form_available_from_time_recreation').val():false
			   })
			  },
		});
		jQuery('#recreation_discount_from_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#recreation_discount_to_expiry').val()?jQuery('#recreation_discount_to_expiry').val():false
				   })
				  },
			 
			 });
		jQuery('#recreation_discount_to_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#recreation_discount_from_expiry').val()?jQuery('#recreation_discount_from_expiry').val():false
				   })
				  },
			});
});
function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('gmap_recreation'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('form_location_address_recreation');
        var searchBox = new google.maps.places.SearchBox(input);
       // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
		  
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
			var fisrtStr=place.geometry.location.toString().trim();
			fisrtStr = fisrtStr.slice(1, -1);;
			document.getElementById("form_location_recreation").value = fisrtStr;
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };
			
            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));
			
			
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	function getAddress(latLng) {
    geocoder.geocode( {'latLng': latLng},
      function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          if(results[0]) {
			  //alert(results[0].formatted_address);
            document.getElementById("form_location_address_recreation").value = results[0].formatted_address;
          }
          else {
			 // alert("No results");
           // document.getElementById("address").value = "No results";
          }
        }
        else {
			//alert(status);
          //document.getElementById("address").value = status;
        }
      });
    }
	$(document).on('click','.action-button', function(){
		 initAutocomplete();
	});
    window.onload = function () { initAutocomplete() };
</script>
        </div>
        </div>
      </div>
      <?php	
	}
	public function addevents()
	{
    ?>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
     <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
     <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
     <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="events_manage">Manage Events</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Events</h4>
          <div class="user-info-body">
        <form class="msform" id="eventform" name="eventform" method="post"> 
        <!-- progressbar -->
         <ul id="progressbarevent" class="progressbar">
            <li class="active">Basic info</li>
            <li>Address  </li>
            <li>Gallery :</li>
            <li>Contact info  </li>
        </ul>
        <!-- fieldsets -->
        <fieldset id="event1" class="progressbarevent">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_title" placeholder="Titile" id="event_title"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="event_image" placeholder="Image" id="event_image"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6">
            <select name="event_category_id" class="form-control" id="event_category_id">
                <option selected="selected" value="">Select category</option>
                <?php 
				$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'6','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="event_subcategory_id" class="form-control" id="event_subcategory_id">
               <option selected="selected" value="">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="event_duration" id="event_duration" placeholder="duration"  data-rule-required='true'/></div>
            <div class="col-md-12 col-sm-12"><textarea name="event_description" id="event_description" class="form-control" placeholder="Description"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event3"  class="progressbarevent">
            <h2 class="fs-title">Gallery</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                    <input type="file" style="padding:0px;" multiple name="event_images_name[]" id="event_images_name" class="form-control" >                      
                </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event4" class="progressbarevent">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="event_user_name" id="event_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_position" id="event_position" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_email"  id="event_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_mobile"  id="event_mobile" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);"/></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="eventform" name="eventsubmit" class="submit action-button" value="Submit" />
          </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#event_duration').datetimepicker({
			 datepicker:false,
			  format:'H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				
			   })
			  },
		 
		 });
	 });
     var map;
	 var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_event"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			    // show in input box
                document.getElementById("event_location").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);
            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
    <?php
	}
	public function addtransportation()
	{
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
     <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
     <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
     <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="transportation_manage">Manage Transportation</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Transportation</h4>
          <div class="user-info-body">
        <form class="msform" id="transportationform" name="transportationform" method="post"> 
        <!-- progressbar -->
            <ul id="progressbartransportation" class="progressbar">
                <li class="active">Basic info</li>
                <li>Trans Details</li>
                <li>Gallery :</li>
                <li>Available time </li>
             </ul>
            <!-- fieldsets -->
            <fieldset id="transportation1" class="progressbartransportation">
                <h2 class="fs-title">Basic info</h2>
                
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_title" placeholder="Titile" id="transportation_title"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6"> <input style="padding:0px;" type="file" name="transportation_image" placeholder="Image" id="transportation_image"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6">
                <select name="transportation_category_id" class="form-control" id="transportation_category_id">
                    <option selected="selected" value="">Select category</option>
                    <?php 
                    $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'5','parent_id'=>'0','category_status'=>'active')); 
                    if(count($category_accom)>0)
                    {
                        foreach($category_accom as $row)
                        {
                        ?>
                        <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
                </div>
                <div class="col-md-6 col-sm-6">
                <select name="transportation_subcategory_id" class="form-control" id="transportation_subcategory_id">
                   <option selected="selected" value="">Select subcategory</option>
                </select>
                </div>
                <div class="col-md-12 col-sm-12"><textarea name="transportation_description" id="transportation_description" class="form-control" placeholder="Description"></textarea></div>
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation2"  class="progressbartransportation">
                <h2 class="fs-title">Trans Details</h2>
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_car_size" placeholder="Car size" id="transportation_car_size"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_car_model" placeholder="Car model" id="transportation_car_model"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="transportation_car_brand" placeholder="Car Brand" id="transportation_car_brand"  data-rule-required='true'/></div>
               <div class="col-md-6 col-sm-6"><input type="text" name="transportation_car" placeholder="Car" id="transportation_car"  data-rule-required='true'/></div>
                <div class="col-md-12 col-sm-12"><input type="text" name="transportation_max_passanger" placeholder="max passanger" id="transportation_max_passanger"  data-rule-required='true'/></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation3"  class="progressbartransportation">
                <h2 class="fs-title">Gallery</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="row">
                    <div class="col-xs-10">
                        <input type="file" style="padding:0px;" multiple name="transportation_image_name[]"  id="transportation_image_name" class="form-control" >                    </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation4" class="progressbartransportation">
              <h2 class="fs-title">Available time</h2>
              <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_available_time" id="transportation_available_time" placeholder="Available time" /></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_pickuptime" id="transportation_pickuptime" placeholder="pickuptime" /></div>
                <div class="col-md-12 col-sm-12"><input type="text" name="transportation_pickup_location"  id="transportation_pickup_location" placeholder="Pickup location" /></div>
                <div class="col-md-12 col-sm-12"><textarea name="transportation_pickup_remarks" id="transportation_pickup_remarks" placeholder="pickup remark"></textarea></div>
                <div class="col-md-12 col-sm-12"><input type="text" name="transportation_drop_location"  id="transportation_drop_location" placeholder="Drop location" /></div>
                <div class="col-md-12 col-sm-12"><textarea name="transportation_drop_remark" id="transportation_drop_remark" placeholder="Drop remark"></textarea></div>
                    <input type="button" name="previous" class="previous action-button" value="Previous" />
                    <input type="submit" id="transportationform" name="transportationsubmit" class="submit action-button" value="Submit" />
              </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#transportation_available_time').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#transportation_pickuptime').val()?jQuery('#transportation_pickuptime').val():false
			   })
			  },
		 });
		jQuery('#transportation_pickuptime').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#transportation_pickuptime').val()?jQuery('#transportation_pickuptime').val():false
			   })
			  },
		}); 
		
		});
		</script>
    <?php
	}
	/*editform*/
	public function packageedit()
	{
	   $formsID = $this->input->post('accom_id');	
	   $forms_info=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
	?>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
		  <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
		  <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
		  <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script>     
		 <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
		  <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
			<p><a href="javascript:void(0);"  class="addcls" rel="package_manage">Package</a></p>
		  </div> 
		  <div class="clearfix"></div>
		  <div class="col-md-12">
			 <div class="user-personal-info">
			  <h4>Package</h4>
			  <div class="user-info-body">
			<form class="msform" id="toursform" name="toursform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'owner/updatetours/'.$formsID.'' ?>"> 
			<!-- progressbar -->
			<ul id="progressbarpackage" class="progressbar">
                <li class="active">Basic info</li>
                <li>Contact info:</li>
                <li>Rules </li>
                <li>Location </li>
                <li class="big-text">pricing & cancellation</li>
                <li>Schedule</li>
                <li>Gallery</li>
		   </ul>
           <input type="hidden" id="checkupdate_package" value="update">
           <input type="hidden" name="main_categoryID" value="9" />
           
			<!-- fieldsets -->
			<fieldset id="package1" class="progressbarpackage">
				<h2 class="fs-title">Basic info</h2>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_package"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title']; ?>"/></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" id="form_title_arb_package"  placeholder="Arabic Title"  data-rule-required='true' dir="rtl" value="<?php echo $forms_info[0]['form_title_arb']; ?>"/></div>
                 <div class="col-md-6 col-sm-6 acc-input-file"><input type="file" name="form_image" placeholder="Titile" id="form_image_package"  /><img src="<?php echo base_url().'uploads/forms/'.$forms_info[0]['form_image']; ?>" width="50" height="50">
                  <input type="hidden" name="old_img" value="<?php echo $forms_info[0]['form_image']; ?>" />
                </div>
				<div style="clear:both"></div>
                <div class="col-md-6 col-sm-6">
				<select name="categoryID" class="form-control" id="categoryID_package">
					<option selected="selected" value="">Select category</option>
                     <?php 
					   $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$forms_info[0]['main_categoryID'],'parent_id'=>'0','category_status'=>'active')); 
						if(count($category_accom)>0)
						{
							foreach($category_accom as $row)
							{
							?>
							<option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
							<?php 
							}
						} ?>
				</select>
				</div>
				<div class="col-md-6 col-sm-6">
				<select name="subcategoryID" class="form-control" id="subcategoryID_package">
				   <option selected="selected" value="">Select subcategory</option>
                   <?php 
				   $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$forms_info[0]['categoryID'],'category_status'=>'active')); 
				   if(count($subcategory_accom)>0)
				   {
					   foreach($subcategory_accom as $sub)
					   {
							$sel1='';
							if($sub['category_id']==$forms_info[0]['subcategoryID'])
							{
								$sel1="selected='selected'";
							}
				   ?>
						  <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $sub['category_name_'.$this->session->userdata('lang')]; ?></option>
				   <?php
					   }
				   }
				   ?>
				</select>
				</div>
				<div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_package"  placeholder="duration" value="<?php echo $forms_info[0]['form_duration']; ?>"></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_package" class="form-control" placeholder="Description"><?php echo $forms_info[0]['form_desc']; ?></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_package" class="form-control" placeholder="Arbic Description" dir="rtl"><?php echo $forms_info[0]['form_desc_arb']; ?></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_package" class="form-control" placeholder="Why you"><?php echo $forms_info[0]['form_why_you']; ?></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_package" class="form-control" placeholder="arbic Why you" dir="rtl" ><?php echo $forms_info[0]['form_why_you_arb']; ?></textarea></div>
				<!--<div class="col-md-12 col-sm-12">  <textarea name="form_address" id="form_address_package" class="form-control" placeholder="Address"></textarea></div>-->
				<div style="clear:both;"></div>
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="package2"  class="progressbarpackage">
					<h2 class="fs-title">Contact info</h2>
					<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_package" placeholder="Name" value="<?php echo $forms_info[0]['form_name']; ?>" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name_arb" id="form_name_arb_package" placeholder="Arbic Name" dir="rtl" value="<?php echo $forms_info[0]['form_name_arb']; ?>"   /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_package" placeholder="position" value="<?php echo $forms_info[0]['form_position']; ?>"  /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position_arb" id="form_position_arb_package" placeholder="Arbic position" dir="rtl" value="<?php echo $forms_info[0]['form_position_arb']; ?>" /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_package" placeholder="Email" value="<?php echo $forms_info[0]['form_email']; ?>" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_package" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_mobile']; ?>" /></div>
					<input type="button" name="previous" class="previous action-button" value="Previous" />
					<input type="button" name="next" class="next action-button" value="Next" />
			   </fieldset>
			<fieldset id="package3" class="progressbarpackage">
				<h2 class="fs-title">Rules</h2>
				<!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
				<div class="col-md-6 col-sm-6">
				<select name="form_gender" class="form-control" id="form_gender_package">
				   <option selected="selected" value="">Select gender</option>
				   <option value="none" <?php if($forms_info[0]['form_gender']=='none'){ ?> selected="selected" <?php } ?>>none</option>
				   <option value="Male" <?php if($forms_info[0]['form_gender']=='Male'){ ?> selected="selected" <?php } ?>>Male</option>
				   <option value="Female" <?php if($forms_info[0]['form_gender']=='Female'){ ?> selected="selected" <?php } ?>>Female</option>
				</select>
				</div>
				<div class="col-md-6 col-sm-6"><input type="text" onkeypress="return OnlyNumericKeys(event);"  name="form_age" value="<?php echo $forms_info[0]['form_age']; ?>" id="form_age_package" placeholder="Age" /></div>
				<div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);" name="form_weight" value="<?php echo $forms_info[0]['form_weight']; ?>" id="form_weight_package" placeholder="Weight" /></div>
				<div class="col-md-6 col-sm-6">
				<span class="allowed-span">Allowed</span> 
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
						   $checked ='';
						   if($forms_info[0]['form_what_allowed']!='')
						   {
							   $what_allowed=explode(',',$forms_info[0]['form_what_allowed']);
							   if(in_array($row['facilities_id'],$what_allowed))
							   {
								   $checked = "checked='checked'";
							   }
						   }
				   ?>
						<label class="labelcheck"><input type="checkbox" <?php echo $checked; ?> name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				   <!--<textarea name="form_what_allowed" id="form_what_allowed_package" class="form-control" placeholder="What allowed"></textarea>-->
				</div>
				<div class="col-md-6 col-sm-6">
				   <span class="allowed-span">Not Allowed </span>
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
						   $select ='';
						   if($forms_info[0]['form_not_allowed']!='')
						   {
							   $facilities= explode(',',$forms_info[0]['form_not_allowed']);
							   if(in_array($row['facilities_id'],$facilities))
							   {
								   $select = "checked='checked'";
							   }
						   }
				   ?>
						<label class="labelcheck"><input type="checkbox" <?php echo  $select; ?> name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				<!--<textarea name="form_not_allowed" id="form_not_allowed_package" class="form-control" placeholder="What not allowed"></textarea>-->
				</div>
				<div class="row">
				<div class="col-md-6 col-sm-6">
					<span class="allowed-span">What Included</span> 
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
							   $select1 ='';
							   if($forms_info[0]['form_what_included']!='')
							   {
								   $what_included= explode(',',$forms_info[0]['form_what_included']);
								   if(in_array($row['included_id'],$what_included))
								   {
									   $select1 = "checked='checked'";
								   }
							   }
					   ?>
							<label class="labelcheck"><input <?php echo $select1; ?> type="checkbox" name="form_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					   <!--<textarea name="form_what_allowed" id="form_what_allowed_package" class="form-control" placeholder="What allowed"></textarea>-->
					</div>
				   <div class="col-md-6 col-sm-6">
					   <span class="allowed-span">What not Included</span>
					   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
							   $select2 ='';
							   if($forms_info[0]['form_what_included']!='')
							   {
								   $not_included= explode(',',$forms_info[0]['form_not_included']);
								   if(in_array($row['included_id'],$not_included))
								   {
									   $select2 = "checked='checked'";
								   }
							   }
					   ?>
							<label class="labelcheck"><input <?php echo $select2; ?> type="checkbox" name="form_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_package" class="form-control" placeholder="What not allowed"></textarea>--></div> 
				</div>
				<div class="row">
			  <div class="col-md-6 col-sm-6">
				  <span class="allowed-span">Requirement </span>
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
							   $select3 ='';
							   if($forms_info[0]['form_what_included']!='')
							   {
								   $stringreq= explode(',',$forms_info[0]['form_requirement']);
								   if(in_array($row['requirement_id'],$stringreq))
								   {
									   $select3 = "checked='checked'";
								   }
							   }
					   ?>
							<label class="labelcheck"><input <?php echo $select3;?> type="checkbox" name="form_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_package" class="form-control" placeholder="What not allowed"></textarea>--></div>
			</div>
				<div class="col-md-6 col-sm-6"><textarea name="form_additional_info" id="form_additional_info_package" class="form-control" placeholder="Additional info"><?php echo $forms_info[0]['form_additional_info']; ?></textarea></div>
				 <div class="col-md-6 col-sm-6"><textarea name="form_additional_info_arb" id="form_additional_info_arb_package" class="form-control" placeholder="Arbic Additional info" dir="rtl"><?php echo $forms_info[0]['form_additional_info_arb']; ?></textarea></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>   
			<fieldset id="package7" class="progressbarpackage">
				<h2 class="fs-title">Location</h2>
				<div class="col-md-12 col-sm-12">
				<input type="text" name="form_location_address" id="form_location_address_package" placeholder="Location Address" value="<?php echo $forms_info[0]['form_location_address']; ?>" />
				</div>
				<div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="form_location" id="form_location_package" placeholder="Location" value="<?php echo $forms_info[0]['form_location']; ?>" /></div>
				<div class="col-md-12 col-sm-12"> <div id="gmap_package" style="width:100%;height:200px;"></div></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset  id="package4" class="progressbarpackage">
			   <h2 class="fs-title">Pricing & cancellation policy </h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="row">
				  <!-- New Form Add HereNed here-->
				  <div class="col-xs-10">
						  <div class="col-xs-4">Family</div>
						   <div class="col-xs-4">
								<label class="yeslabel">
								   <input type="checkbox" class="chk_click" <?php if($forms_info[0]['form_family']=='yes'){ echo 'checked="checked"'; } ?> id="form_family_member_package" rel='form_family_member_packagediv' value="yes" name='form_family'>
									<span class="yeslabeltext"> yes</span>
								</label>
						   </div>
					  </div>
				  <div class="col-xs-10" id="form_family_member_packagediv" style=" <?php if($forms_info[0]['form_family']=='yes'){ echo "display:show";}else{ echo "display:none";} ?>">
				   <div class="col-xs-4">&nbsp;</div>
					<div class="col-xs-3">
					 <input type="text" id="form_family_member_package" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_family_member']; ?>"  />
				   </div>
				   <div class="col-xs-3">
					 <input type="text" id="form_family_price_package" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_family_price']; ?>" />
				   </div>
				  </div>
				  <div class="col-xs-10">
					<div class="col-xs-4">Children Less Than</div>
					<div class="col-xs-3">
					  <select id="form_lessthan_age_package" class="form-control" name="form_lessthan_age">
						 <?php for($i=0;$i<=18;$i++){
							  $select='';
							  if($i== $forms_info[0]['form_lessthan_age'])
							  {
								  $select="selected='selected'";
							  }
							?>
					    <option value="<?php echo $i; ?>" <?php echo $select; ?>><?php echo $i; ?></option>
					  <?php } ?>
					  </select>
					</div>
					<div class="col-xs-3">
					 <input type="text" id="form_lessthan_price_package" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_lessthan_price']; ?>" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Price</div>
					<div class="col-xs-6">
					  <input type="text" name="form_member_price" id="form_member_price_package" value="<?php echo $forms_info[0]['form_price']; ?>" placeholder="Price" onkeypress="return OnlyNumericKeys(event);" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Group</div>
					<div class="col-xs-3">
					  <input type="text" name="form_min_member" id="form_min_member_package" placeholder="Min"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_min_member']; ?>"/>
					</div>
					<div class="col-xs-3">
					  <input type="text" name="form_max_member" id="form_max_member_package" placeholder="Max"  onkeypress="return OnlyNumericKeys(event);" />
					</div>
				   </div> 
				   <div class="col-xs-10">
						<div class="col-xs-4">Group Price</div>
						<div class="col-xs-6">
						  <input type="text" name="form_grp_price" id="form_grp_price_package" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_max_member']; ?>" />
						</div>
				   </div>   
				   
				  <div class="clearfix"></div>
				</div>
				<div class="row">
				  <div class="col-xs-10">
					<div class="col-xs-4">Bird discount</div>
						<div class="col-xs-3" data-toggle="buttons">
						   <input name="form_bird_days_before" id="form_bird_days_package" placeholder="Bird days befor" type="text" value="<?php echo $forms_info[0]['form_bird_days_before']; ?>">  
					   </div>	
					   <div data-toggle="buttons" class="col-xs-3">
						   <input placeholder="Bird discount" id="form_bird_package" name="form_bird_discount"  type="text" value="<?php echo $forms_info[0]['form_bird_discount']; ?>"> 
						</div>
					</div>
				</div>
				<div class="row">
				   <div class="col-xs-10">
					 <div class="col-xs-4">Discount</div>
					 <div class="col-xs-4" data-toggle="buttons">
						<label class="btn discountthree <?php if($forms_info[0]['form_discount']=='on'){ ?>active <?php } ?>">
						<input id="tours_discount" value="on" <?php if($forms_info[0]['form_discount']=='on'){ ?>checked="checked" <?php } ?>  class="tours_discount" name="form_discount" checked="checked" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
						</label>
						<label class="btn discountthree  <?php if($forms_info[0]['form_discount']=='off'){ ?>active <?php } ?>">
						<input id="tours_discount1" <?php if($forms_info[0]['form_discount']=='off'){ ?>checked="checked" <?php } ?> value="off" class="tours_discount" name="form_discount" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
						</label>
					</div>
				   </div>
				</div>
				<div class="row" id="tours_discount_on" style=" <?php if($forms_info[0]['form_discount']=='on'){ echo "display:show";}else{ echo "display:none";} ?>">
				   <div class="col-xs-10">
						<div class="col-xs-4">Discount Price</div>
						<div class="col-sm-4">
						   <input onkeypress="return OnlyNumericKeys(event);" class="form-control" name="form_pecentage_discount" id="tours_recreation" placeholder="discount price in percantage" type="text" value="<?php echo $forms_info[0]['form_pecentage_discount']; ?>">
						</div>
					</div>  
					<div class="col-xs-10">
						<div class="col-xs-4">Discount From date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_from_expiry" id="tours_discount_from_expiry" placeholder="From date" type="text" value="<?php echo $forms_info[0]['form_discount_from_expiry']; ?>">
						</div>
					</div>
					<div class="col-xs-10">
						<div class="col-xs-4">Discount To date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_to_expiry" id="tours_discount_to_expiry"  placeholder="From date" type="text" value="<?php echo $forms_info[0]['form_discount_to_expiry']; ?>">
						</div>
					</div>    	
				</div>
				<div class="row">
				   <div class="col-xs-10">
						<div class="col-xs-4">Cancellation and refund policy: </div>
						<div class="col-xs-4" data-toggle="buttons">
							<label class="btn amenitiesform <?php if($forms_info[0]['form_refundable']=='yes'){ ?>active <?php } ?>">
							<input type="radio" id="form_refundable_package" rel='form_refundable_package' value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
							</label>
							<label class="btn amenitiesform <?php if($forms_info[0]['form_refundable']=='no'){ ?>active <?php } ?>">
							<input type="radio" id="form_refundable_package1" rel='form_refundable_package' value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
							</label>
						</div>
					</div>
					<div class="col-md-12">
					  <div id="form_refundable_package_yes" style=" <?php if($forms_info[0]['form_refundable']=='no'){ ?> style="display:none;" <?php } ?>">
						  <div class="col-xs-3">chargeable if cancelled before: </div>
						  <div class="col-xs-8">
							<div class="col-xs-5">
						   <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_package"  value="<?php echo $forms_info[0]['form_cancel_before']; ?>"  placeholder="% chargeable if cancelled before "></div>
						   <div class="col-xs-4">
							<input type="input" placeholder="hrs" value="<?php echo $forms_info[0]['form_not_cancel_hrs']; ?>"id="form_not_cancel_package" name="form_not_cancel_hrs" class="form-control">
							</div>
						  </div>
						 <div class="clr"></div>
						 <div class="col-md-12">
							  <div class="col-xs-3"></div>
							   <div class="col-xs-6" style="margin-left:10px;">                    
								<input type="input" placeholder="non refundable price"  value="<?php echo $forms_info[0]['form_not_cancel']; ?>"  id="form_not_cancel_package" name="form_not_cancel" class="form-control">
							  </div>                   
							</div>
						</div>
					</div>
				</div>
			   
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="package5" class="progressbarpackage">
			<h2 class="fs-title">Schedule</h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div id="weekly_package">
					   <input type="hidden" id="form_availalbe_to_package"  rel="tours" value="weekly"  name="form_availalbe_for">
					   <div class="row">
					<div class="col-xs-10">
					  <div class="col-xs-4">From Date</div>
					  <div class="col-xs-6">
						<input type="text" name="form_available_from_date" id="form_available_from_date_package" placeholder="Select available time" />
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">To Date</div>
					   <div class="col-xs-6">
					   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_package"  placeholder="Select available time" />
					 </div>
					</div>
				</div>
					  <?php 
					  $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
					  $cntarr=count($days_arr);
					  if(count($days_arr)>0)
					  {
						for($i=0;$i<$cntarr;$i++)
						{
					   ?>
						<div class="row">
						  <div class="col-xs-10">
							<div class="col-xs-4"><?php if($i==0) {?>Select Days <?php }else{echo ' ';} ?></div>
							<div class="col-xs-3">
							  <label style="margin-bottom:-10px;">
								<div class="col-xs-1">
									<input type="checkbox" class="clsday" name="form_available_day[]" id="form_available_day_package_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>">
								</div>
								<div class="col-xs-3" style="margin-top:10px;"><?php echo ucfirst($days_arr[$i]);?></div>
							  </label>
							 </div>
						  </div> 
						</div>
						<div class="row" id="<?php echo ucfirst($days_arr[$i]);?>" style="display:none;">
						   <div class="col-xs-10">
							  <div class="col-xs-4"><?php echo ' ';?></div>
							  <div class="col-xs-2"><input type="text" name="form_from_time_<?php echo $days_arr[$i]?>[]" id="form_from_time_package<?php echo $i; ?>" class="form_availalbe_from_time_package" placeholder="From time" /></div>
							  <div class="col-xs-2"><input type="text" name="form_to_time_<?php echo $days_arr[$i]?>[]" id="form_to_time_package<?php echo $i; ?>" class="form_availalbe_to_time_package" placeholder="To time" /> </div>
							  <div class="col-xs-2"><input type="text" name="form_day_duration_<?php echo $days_arr[$i]?>[]" id="form_duration_package<?php echo $i; ?>" placeholder="Duration" /> </div>
							  <div class="col-xs-2" style="margin-top:-20px;"><a href="javascript:void(0);" class="addcheck" rel='tours'>Add</a></div>
							</div>
						</div> 
						<?php
						}
					  }
					  ?>
				 </div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point</div>
					   <div class="col-xs-6">
						<textarea name="form_departure" id="form_departure_package" class="form-control" placeholder="Departure Point"><?php echo $forms_info[0]['form_departure']; ?></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point address</div>
					   <div class="col-xs-6">
						<textarea name="form_departure_address" id="form_departure_address_package" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_departure_address']; ?></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_departure_time" id="form_departure_time_package" value='<?php echo $forms_info[0]['form_departure_time']; ?>'  placeholder="Departure Time" />
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Reporting Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_reporting_time" id="form_reporting_time_package" value='<?php echo $forms_info[0]['form_reporting_time']; ?>'  placeholder="Departure Time" />
					 </div>
					</div>
				</div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="package6" class="progressbarpackage">
				  <h2 class="fs-title">Gallery</h2>
                  <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_images_name[]" id="form_images_adventure" multiple placeholder="Image" /></div>
                  <div class="clearfix"></div>
                  <div class="image-set gallery-box">
                  <?php $accoun_image=$this->master_model->getRecords('tbl_threeforms_images',array('form_id'=>$formsID)); 
                   if(count($accoun_image))
                   {
                      foreach($accoun_image as $row)
                      {
                    ?>
                      <div class="col-md-3 col-sm-4">
                        <div class="image-wrapper">
                          <img src="<?php  echo base_url().'uploads/form/'.$row['form_images_name']; ?>" width="175" height="144">
                          <div class="img-caption">
                            <div class="link">
                                <a href="javascript:void(0);" class="deletelink" rel="forms" data-id="<?php echo $row['form_images_id'];?>"><i class="fa fa-times"></i></a>
                            </div>
                        </div>      
                        </div>
                     </div>
                  <?php
                      }
                  }
                  ?>
                 </div>
				  <input type="button" name="previous" class="previous action-button" value="Previous" />
				  <input type="submit" id="submit_package" name="tourssubmit" class="submit action-button" value="Submit" />   
			</fieldset>
			</form> 
		  <script type="text/javascript">
		  jQuery(document).ready(function(e) {
			 jQuery('#form_available_from_date_package').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#form_availalbe_to_date_package').val()?jQuery('#form_availalbe_to_date_package').val():false
				   })
				  },
			 
			 });
			jQuery('#form_availalbe_to_date_package').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#form_available_from_date_package').val()?jQuery('#form_available_from_date_package').val():false
				   })
				  },
			});
			jQuery('.form_availalbe_from_time_package').datetimepicker({
				  formatTime:'H:i:s',
				  format:	'H:i:s',
				  datepicker: false,
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('.form_availalbe_to_time_package').val()?jQuery('.form_availalbe_to_time_package').val():false
				   })
				  },
			 
			 });
			jQuery('.form_availalbe_to_time_package').datetimepicker({
				  formatTime:'H:i:s',
				  format:	'H:i:s',
				  datepicker: false,
				  onShow:function( ct ){

				   this.setOptions({
					minDate:jQuery('.form_available_from_time_package').val()?jQuery('.form_available_from_time_package').val():false
				   })
				  },
			});
			jQuery('#tours_discount_from_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						maxDate:jQuery('#tours_discount_to_expiry').val()?jQuery('#tours_discount_to_expiry').val():false
					   })
					  },
				 
				 });
			jQuery('#tours_discount_to_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						minDate:jQuery('#tours_discount_from_expiry').val()?jQuery('#tours_discount_from_expiry').val():false
					   })
					  },
				});
		 });
			function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_package'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('form_location_address_package');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1);;
				document.getElementById("form_location_package").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("form_location_address_package").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
			</div>
			</div>
		  </div>
     <?php	
	}
	public function toursedit()
	{
	   $formsID = $this->input->post('accom_id');	
	   $forms_info=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
	?>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
		  <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
		  <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
		  <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script>     
		 <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
		  <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
			<p><a href="javascript:void(0);"  class="addcls" rel="tours_manage">Manage Things to do</a></p>
		  </div> 
		  <div class="clearfix"></div>
		  <div class="col-md-12">
			 <div class="user-personal-info">
			  <h4>Things to do</h4>
			  <div class="user-info-body">
			<form class="msform" id="toursform" name="toursform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'owner/updatetours/'.$formsID.'' ?>"> 
			<!-- progressbar -->
			<ul id="progressbartours" class="progressbar">
                <li class="active">Basic info</li>
                <li>Contact info:</li>
                <li>Rules </li>
                <li>Location </li>
                <li class="big-text">pricing & cancellation</li>
                <li>Schedule</li>
                <li>Gallery</li>
		   </ul><input type="hidden" id="checkupdate_tours" value="update">
			<!-- fieldsets -->
			<fieldset id="tours1" class="progressbartours">
				<h2 class="fs-title">Basic info</h2>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_tours"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title']; ?>"/></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" id="form_title_arb_tours"  placeholder="Arabic Title"  data-rule-required='true' dir="rtl" value="<?php echo $forms_info[0]['form_title_arb']; ?>"/></div>
                 <div class="col-md-6 col-sm-6 acc-input-file"><input type="file" name="form_image" placeholder="Titile" id="form_image_tours"  /><img src="<?php echo base_url().'uploads/forms/'.$forms_info[0]['form_image']; ?>" width="50" height="50">
                  <input type="hidden" name="old_img" value="<?php echo $forms_info[0]['form_image']; ?>" />
                 </div>
				<div class="col-md-6 col-sm-6">
                  <select name="main_categoryID" class="form-control" id="categoryID_things_to_do">
                        <option selected="selected" value="">Select main category</option>
                        <?php 
						$this->db->where('category_id IN(2,3,4,9,10) !=0');
						$category_main=$this->master_model->getRecords('tbl_maincategory_master',array('category_status'=>'active')); 
                        if(count($category_main)>0)
                        {
                            foreach($category_main as $row)
                            {
								$sel='';
								if($row['category_id']==$forms_info[0]['main_categoryID'])
								{
									$sel="selected='selected'";
								}
                            ?>
                            <option value="<?php echo $row['category_id']; ?>" <?php echo $sel; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                            <?php 
                            }
                        } ?>
                    </select>
				</div>
                <div style="clear:both"></div>
                <div class="col-md-6 col-sm-6">
				<select name="categoryID" class="form-control" id="categoryID_tours">
					<option selected="selected" value="">Select category</option>
                     <?php 
					   $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$forms_info[0]['main_categoryID'],'parent_id'=>'0','category_status'=>'active')); 
						if(count($category_accom)>0)
						{
							foreach($category_accom as $row)
							{
							?>
							<option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
							<?php 
							}
						} ?>
				</select>
				</div>
				<div class="col-md-6 col-sm-6">
				<select name="subcategoryID" class="form-control" id="subcategoryID_tours">
				   <option selected="selected" value="">Select subcategory</option>
                   <?php 
				   $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$forms_info[0]['categoryID'],'category_status'=>'active')); 
				   if(count($subcategory_accom)>0)
				   {
					   foreach($subcategory_accom as $sub)
					   {
							$sel1='';
							if($sub['category_id']==$forms_info[0]['subcategoryID'])
							{
								$sel1="selected='selected'";
							}
				   ?>
						  <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $sub['category_name_'.$this->session->userdata('lang')]; ?></option>
				   <?php
					   }
				   }
				   ?>
				</select>
				</div>
				<div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_tours"  placeholder="duration" value="<?php echo $forms_info[0]['form_duration']; ?>"></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_tours" class="form-control" placeholder="Description"><?php echo $forms_info[0]['form_desc']; ?></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_tours" class="form-control" placeholder="Arbic Description" dir="rtl"><?php echo $forms_info[0]['form_desc_arb']; ?></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_tours" class="form-control" placeholder="Why you"><?php echo $forms_info[0]['form_why_you']; ?></textarea></div>
				<div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_tours" class="form-control" placeholder="arbic Why you" dir="rtl" ><?php echo $forms_info[0]['form_why_you_arb']; ?></textarea></div>
				<!--<div class="col-md-12 col-sm-12">  <textarea name="form_address" id="form_address_tours" class="form-control" placeholder="Address"></textarea></div>-->
				<div style="clear:both;"></div>
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="tours2"  class="progressbartours">
					<h2 class="fs-title">Contact info</h2>
					<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_tours" placeholder="Name" value="<?php echo $forms_info[0]['form_name']; ?>" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_name_arb" id="form_name_arb_tours" placeholder="Arbic Name" dir="rtl" value="<?php echo $forms_info[0]['form_name_arb']; ?>"   /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_tours" placeholder="position" value="<?php echo $forms_info[0]['form_position']; ?>"  /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_position_arb" id="form_position_arb_tours" placeholder="Arbic position" dir="rtl" value="<?php echo $forms_info[0]['form_position_arb']; ?>" /></div>
				<div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_tours" placeholder="Email" value="<?php echo $forms_info[0]['form_email']; ?>" /></div>
				<div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_tours" placeholder="mobile number"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_mobile']; ?>" /></div>
					<input type="button" name="previous" class="previous action-button" value="Previous" />
					<input type="button" name="next" class="next action-button" value="Next" />
			   </fieldset>
			<fieldset id="tours3" class="progressbartours">
				<h2 class="fs-title">Rules</h2>
				<!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
				<div class="col-md-6 col-sm-6">
				<select name="form_gender" class="form-control" id="form_gender_tours">
				   <option selected="selected" value="">Select gender</option>
				   <option value="none" <?php if($forms_info[0]['form_gender']=='none'){ ?> selected="selected" <?php } ?>>none</option>
				   <option value="Male" <?php if($forms_info[0]['form_gender']=='Male'){ ?> selected="selected" <?php } ?>>Male</option>
				   <option value="Female" <?php if($forms_info[0]['form_gender']=='Female'){ ?> selected="selected" <?php } ?>>Female</option>
				</select>
				</div>
				<div class="col-md-6 col-sm-6"><input type="text" onkeypress="return OnlyNumericKeys(event);"  name="form_age" value="<?php echo $forms_info[0]['form_age']; ?>" id="form_age_tours" placeholder="Age" /></div>
				<div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);" name="form_weight" value="<?php echo $forms_info[0]['form_weight']; ?>" id="form_weight_tours" placeholder="Weight" /></div>
				<div class="col-md-6 col-sm-6">
				<span class="allowed-span">Allowed</span> 
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
						   $checked ='';
						   if($forms_info[0]['form_what_allowed']!='')
						   {
							   $what_allowed=explode(',',$forms_info[0]['form_what_allowed']);
							   if(in_array($row['facilities_id'],$what_allowed))
							   {
								   $checked = "checked='checked'";
							   }
						   }
				   ?>
						<label class="labelcheck"><input type="checkbox" <?php echo $checked; ?> name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
				</div>
				<div class="col-md-6 col-sm-6">
				   <span class="allowed-span">Not Allowed </span>
				   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
				   if(count($fetch_array))
				   {
					   foreach($fetch_array as $row)
					   {
						   $select ='';
						   if($forms_info[0]['form_not_allowed']!='')
						   {
							   $facilities= explode(',',$forms_info[0]['form_not_allowed']);
							   if(in_array($row['facilities_id'],$facilities))
							   {
								   $select = "checked='checked'";
							   }
						   }
				   ?>
						<label class="labelcheck"><input type="checkbox" <?php echo  $select; ?> name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
				   <?php
					   }
				   }
				   ?>
				<!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>-->
				</div>
				<div class="row">
				<div class="col-md-6 col-sm-6">
					<span class="allowed-span">What Included</span> 
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
							   $select1 ='';
							   if($forms_info[0]['form_what_included']!='')
							   {
								   $what_included= explode(',',$forms_info[0]['form_what_included']);
								   if(in_array($row['included_id'],$what_included))
								   {
									   $select1 = "checked='checked'";
								   }
							   }
					   ?>
							<label class="labelcheck"><input <?php echo $select1; ?> type="checkbox" name="form_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
					</div>
				   <div class="col-md-6 col-sm-6">
					   <span class="allowed-span">What not Included</span>
					   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
							   $select2 ='';
							   if($forms_info[0]['form_what_included']!='')
							   {
								   $not_included= explode(',',$forms_info[0]['form_not_included']);
								   if(in_array($row['included_id'],$not_included))
								   {
									   $select2 = "checked='checked'";
								   }
							   }
					   ?>
							<label class="labelcheck"><input <?php echo $select2; ?> type="checkbox" name="form_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
				</div>
				<div class="row">
			  <div class="col-md-6 col-sm-6">
				  <span class="allowed-span">Requirement </span>
					   <?php 
					   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
					   if(count($fetch_array))
					   {
						   foreach($fetch_array as $row)
						   {
							   $select3 ='';
							   if($forms_info[0]['form_what_included']!='')
							   {
								   $stringreq= explode(',',$forms_info[0]['form_requirement']);
								   if(in_array($row['requirement_id'],$stringreq))
								   {
									   $select3 = "checked='checked'";
								   }
							   }
					   ?>
							<label class="labelcheck"><input <?php echo $select3;?> type="checkbox" name="form_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
					   <?php
						   }
					   }
					   ?>
					<!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
			</div>
				<div class="col-md-6 col-sm-6"><textarea name="form_additional_info" id="form_additional_info_tours" class="form-control" placeholder="Additional info"><?php echo $forms_info[0]['form_additional_info']; ?></textarea></div>
				 <div class="col-md-6 col-sm-6"><textarea name="form_additional_info_arb" id="form_additional_info_arb_tours" class="form-control" placeholder="Arbic Additional info" dir="rtl"><?php echo $forms_info[0]['form_additional_info_arb']; ?></textarea></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>   
			<fieldset id="tours7" class="progressbartours">
				<h2 class="fs-title">Location</h2>
				<div class="col-md-12 col-sm-12">
				<input type="text" name="form_location_address" id="form_location_address_tours" placeholder="Location Address" value="<?php echo $forms_info[0]['form_location_address']; ?>" />
				</div>
				<div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="form_location" id="form_location_tours" placeholder="Location" value="<?php echo $forms_info[0]['form_location']; ?>" /></div>
				<div class="col-md-12 col-sm-12"> <div id="gmap_tours" style="width:100%;height:200px;"></div></div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset  id="tours4" class="progressbartours">
			   <h2 class="fs-title">Pricing & cancellation policy </h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div class="row">
				  <!-- New Form Add HereNed here-->
				  <div class="col-xs-10">
						  <div class="col-xs-4">Family</div>
						   <div class="col-xs-4">
								<label class="yeslabel">
								   <input type="checkbox" class="chk_click" <?php if($forms_info[0]['form_family']=='yes'){ echo 'checked="checked"'; } ?> id="form_family_member_tours" rel='form_family_member_toursdiv' value="yes" name='form_family'>
									<span class="yeslabeltext"> yes</span>
								</label>
						   </div>
					  </div>
				  <div class="col-xs-10" id="form_family_member_toursdiv" style=" <?php if($forms_info[0]['form_family']=='yes'){ echo "display:show";}else{ echo "display:none";} ?>">
				   <div class="col-xs-4">&nbsp;</div>
					<div class="col-xs-3">
					 <input type="text" id="form_family_member_tours" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_family_member']; ?>"  />
				   </div>
				   <div class="col-xs-3">
					 <input type="text" id="form_family_price_tours" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_family_price']; ?>" />
				   </div>
				  </div>
				  <div class="col-xs-10">
					<div class="col-xs-4">Children Less Than</div>
					<div class="col-xs-3">
					  <select id="form_lessthan_age_tours" class="form-control" name="form_lessthan_age">
						 <?php for($i=0;$i<=18;$i++){
							  $select='';
							  if($i== $forms_info[0]['form_lessthan_age'])
							  {
								  $select="selected='selected'";
							  }
							?>
					    <option value="<?php echo $i; ?>" <?php echo $select; ?>><?php echo $i; ?></option>
					  <?php } ?>
					  </select>
					</div>
					<div class="col-xs-3">
					 <input type="text" id="form_lessthan_price_tours" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_lessthan_price']; ?>" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Price</div>
					<div class="col-xs-6">
					  <input type="text" name="form_member_price" id="form_member_price_tours" value="<?php echo $forms_info[0]['form_price']; ?>" placeholder="Price" onkeypress="return OnlyNumericKeys(event);" />
					</div>
				  </div> 
				  <div class="col-xs-10">
					<div class="col-xs-4">Group</div>
					<div class="col-xs-3">
					  <input type="text" name="form_min_member" id="form_min_member_tours" placeholder="Min"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_min_member']; ?>"/>
					</div>
					<div class="col-xs-3">
					  <input type="text" name="form_max_member" id="form_max_member_tours" placeholder="Max"  onkeypress="return OnlyNumericKeys(event);" />
					</div>
				   </div> 
				   <div class="col-xs-10">
						<div class="col-xs-4">Group Price</div>
						<div class="col-xs-6">
						  <input type="text" name="form_grp_price" id="form_grp_price_tours" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_max_member']; ?>" />
						</div>
				   </div>   
				   
				  <div class="clearfix"></div>
				</div>
				<div class="row">
				  <div class="col-xs-10">
					<div class="col-xs-4">Bird discount</div>
						<div class="col-xs-3" data-toggle="buttons">
						   <input name="form_bird_days_before" id="form_bird_days_tours" placeholder="Bird days befor" type="text" value="<?php echo $forms_info[0]['form_bird_days_before']; ?>">  
					   </div>	
					   <div data-toggle="buttons" class="col-xs-3">
						   <input placeholder="Bird discount" id="form_bird_tours" name="form_bird_discount"  type="text" value="<?php echo $forms_info[0]['form_bird_discount']; ?>"> 
						</div>
					</div>
				</div>
				<div class="row">
				   <div class="col-xs-10">
					 <div class="col-xs-4">Discount</div>
					 <div class="col-xs-4" data-toggle="buttons">
						<label class="btn discountthree <?php if($forms_info[0]['form_discount']=='on'){ ?>active <?php } ?>">
						<input id="tours_discount" value="on" <?php if($forms_info[0]['form_discount']=='on'){ ?>checked="checked" <?php } ?>  class="tours_discount" name="form_discount" checked="checked" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
						</label>
						<label class="btn discountthree  <?php if($forms_info[0]['form_discount']=='off'){ ?>active <?php } ?>">
						<input id="tours_discount1" <?php if($forms_info[0]['form_discount']=='off'){ ?>checked="checked" <?php } ?> value="off" class="tours_discount" name="form_discount" type="radio"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
						</label>
					</div>
				   </div>
				</div>
				<div class="row" id="tours_discount_on" style=" <?php if($forms_info[0]['form_discount']=='on'){ echo "display:show";}else{ echo "display:none";} ?>">
				   <div class="col-xs-10">
						<div class="col-xs-4">Discount Price</div>
						<div class="col-sm-4">
						   <input onkeypress="return OnlyNumericKeys(event);" class="form-control" name="form_pecentage_discount" id="tours_recreation" placeholder="discount price in percantage" type="text" value="<?php echo $forms_info[0]['form_pecentage_discount']; ?>">
						</div>
					</div>  
					<div class="col-xs-10">
						<div class="col-xs-4">Discount From date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_from_expiry" id="tours_discount_from_expiry" placeholder="From date" type="text" value="<?php echo $forms_info[0]['form_discount_from_expiry']; ?>">
						</div>
					</div>
					<div class="col-xs-10">
						<div class="col-xs-4">Discount To date</div>
						<div class="col-sm-6">
						   <input class="form-control" name="form_discount_to_expiry" id="tours_discount_to_expiry"  placeholder="From date" type="text" value="<?php echo $forms_info[0]['form_discount_to_expiry']; ?>">
						</div>
					</div>    	
				</div>
				<div class="row">
				   <div class="col-xs-10">
						<div class="col-xs-4">Cancellation and refund policy: </div>
						<div class="col-xs-4" data-toggle="buttons">
							<label class="btn amenitiesform <?php if($forms_info[0]['form_refundable']=='yes'){ ?>active <?php } ?>">
							<input type="radio" id="form_refundable_tours" rel='form_refundable_tours' value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
							</label>
							<label class="btn amenitiesform <?php if($forms_info[0]['form_refundable']=='no'){ ?>active <?php } ?>">
							<input type="radio" id="form_refundable_tours1" rel='form_refundable_tours' value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
							</label>
						</div>
					</div>
					<div class="col-md-12">
					  <div id="form_refundable_tours_yes" style=" <?php if($forms_info[0]['form_refundable']=='no'){ ?> style="display:none;" <?php } ?>">
						  <div class="col-xs-3">chargeable if cancelled before: </div>
						  <div class="col-xs-8">
							<div class="col-xs-5">
						   <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_tours"  value="<?php echo $forms_info[0]['form_cancel_before']; ?>"  placeholder="% chargeable if cancelled before "></div>
						   <div class="col-xs-4">
							<input type="input" placeholder="hrs" value="<?php echo $forms_info[0]['form_not_cancel_hrs']; ?>"id="form_not_cancel_tours" name="form_not_cancel_hrs" class="form-control">
							</div>
						  </div>
						 <div class="clr"></div>
						 <div class="col-md-12">
							  <div class="col-xs-3"></div>
							   <div class="col-xs-6" style="margin-left:10px;">                    
								<input type="input" placeholder="non refundable price"  value="<?php echo $forms_info[0]['form_not_cancel']; ?>"  id="form_not_cancel_tours" name="form_not_cancel" class="form-control">
							  </div>                   
							</div>
						</div>
					</div>
				</div>
			   
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="tours5" class="progressbartours">
			<h2 class="fs-title">Schedule</h2>
				<!--<h3 class="fs-subtitle">We will never sell it</h3>-->
				<div id="weekly_tours">
					   <input type="hidden" id="form_availalbe_to_tours"  rel="tours" value="weekly"  name="form_availalbe_for">
					   <div class="row">
					<div class="col-xs-10">
					  <div class="col-xs-4">From Date</div>
					  <div class="col-xs-6">
						<input type="text" name="form_available_from_date" id="form_available_from_date_tours" placeholder="Select available time" />
					  </div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">To Date</div>
					   <div class="col-xs-6">
					   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_tours"  placeholder="Select available time" />
					 </div>
					</div>
				</div>
					  <?php 
					  $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
					  $cntarr=count($days_arr);
					  if(count($days_arr)>0)
					  {
						for($i=0;$i<$cntarr;$i++)
						{
					   ?>
						<div class="row">
						  <div class="col-xs-10">
							<div class="col-xs-4"><?php if($i==0) {?>Select Days <?php }else{echo ' ';} ?></div>
							<div class="col-xs-3">
							  <label style="margin-bottom:-10px;">
								<div class="col-xs-1">
									<input type="checkbox" class="clsday" name="form_available_day[]" id="form_available_day_tours_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>">
								</div>
								<div class="col-xs-3" style="margin-top:10px;"><?php echo ucfirst($days_arr[$i]);?></div>
							  </label>
							 </div>
						  </div> 
						</div>
						<div class="row" id="<?php echo ucfirst($days_arr[$i]);?>" style="display:none;">
						   <div class="col-xs-10">
							  <div class="col-xs-4"><?php echo ' ';?></div>
							  <div class="col-xs-2"><input type="text" name="form_from_time_<?php echo $days_arr[$i]?>[]" id="form_from_time_tours<?php echo $i; ?>" class="form_availalbe_from_time_tours" placeholder="From time" /></div>
							  <div class="col-xs-2"><input type="text" name="form_to_time_<?php echo $days_arr[$i]?>[]" id="form_to_time_tours<?php echo $i; ?>" class="form_availalbe_to_time_tours" placeholder="To time" /> </div>
							  <div class="col-xs-2"><input type="text" name="form_day_duration_<?php echo $days_arr[$i]?>[]" id="form_duration_tours<?php echo $i; ?>" placeholder="Duration" /> </div>
							  <div class="col-xs-2" style="margin-top:-20px;"><a href="javascript:void(0);" class="addcheck" rel='tours'>Add</a></div>
							</div>
						</div> 
						<?php
						}
					  }
					  ?>
				 </div>
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point</div>
					   <div class="col-xs-6">
						<textarea name="form_departure" id="form_departure_tours" class="form-control" placeholder="Departure Point"><?php echo $forms_info[0]['form_departure']; ?></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Point address</div>
					   <div class="col-xs-6">
						<textarea name="form_departure_address" id="form_departure_address_tours" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_departure_address']; ?></textarea>
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Departure Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_departure_time" id="form_departure_time_tours" value='<?php echo $forms_info[0]['form_departure_time']; ?>'  placeholder="Departure Time" />
					 </div>
					</div>
				</div> 
				<div class="row">
					<div class="col-xs-10">
					 <div class="col-xs-4">Reporting Time</div>
					   <div class="col-xs-6">
						<input type="text" name="form_reporting_time" id="form_reporting_time_tours" value='<?php echo $forms_info[0]['form_reporting_time']; ?>'  placeholder="Departure Time" />
					 </div>
					</div>
				</div>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
			</fieldset>
			<fieldset id="tours6" class="progressbartours">
				  <h2 class="fs-title">Gallery</h2>
                  <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_images_name[]" id="form_images_adventure" multiple placeholder="Image" /></div>
                  <div class="clearfix"></div>
                  <div class="image-set gallery-box">
                  <?php $accoun_image=$this->master_model->getRecords('tbl_threeforms_images',array('form_id'=>$formsID)); 
                   if(count($accoun_image))
                   {
                      foreach($accoun_image as $row)
                      {
                    ?>
                      <div class="col-md-3 col-sm-4">
                        <div class="image-wrapper">
                          <img src="<?php  echo base_url().'uploads/form/'.$row['form_images_name']; ?>" width="175" height="144">
                          <div class="img-caption">
                            <div class="link">
                                <a href="javascript:void(0);" class="deletelink" rel="forms" data-id="<?php echo $row['form_images_id'];?>"><i class="fa fa-times"></i></a>
                            </div>
                        </div>      
                        </div>
                     </div>
                  <?php
                      }
                  }
                  ?>
                 </div>
				  <input type="button" name="previous" class="previous action-button" value="Previous" />
				  <input type="submit" id="submit_tours" name="tourssubmit" class="submit action-button" value="Submit" />   
			</fieldset>
			</form> 
		  <script type="text/javascript">
		  jQuery(document).ready(function(e) {
			 jQuery('#form_available_from_date_tours').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#form_availalbe_to_date_tours').val()?jQuery('#form_availalbe_to_date_tours').val():false
				   })
				  },
			 
			 });
			jQuery('#form_availalbe_to_date_tours').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#form_available_from_date_tours').val()?jQuery('#form_available_from_date_tours').val():false
				   })
				  },
			});
			jQuery('.form_availalbe_from_time_tours').datetimepicker({
				  formatTime:'H:i:s',
				  format:	'H:i:s',
				  datepicker: false,
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('.form_availalbe_to_time_tours').val()?jQuery('.form_availalbe_to_time_tours').val():false
				   })
				  },
			 
			 });
			jQuery('.form_availalbe_to_time_tours').datetimepicker({
				  formatTime:'H:i:s',
				  format:	'H:i:s',
				  datepicker: false,
				  onShow:function( ct ){

				   this.setOptions({
					minDate:jQuery('.form_available_from_time_tours').val()?jQuery('.form_available_from_time_tours').val():false
				   })
				  },
			});
			jQuery('#tours_discount_from_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						maxDate:jQuery('#tours_discount_to_expiry').val()?jQuery('#tours_discount_to_expiry').val():false
					   })
					  },
				 
				 });
			jQuery('#tours_discount_to_expiry').datetimepicker({
					  format:'Y-m-d H:i:s',
					  onShow:function( ct ){
					   this.setOptions({
						minDate:jQuery('#tours_discount_from_expiry').val()?jQuery('#tours_discount_from_expiry').val():false
					   })
					  },
				});
		 });
			function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_tours'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('form_location_address_tours');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1);;
				document.getElementById("form_location_tours").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("form_location_address_tours").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
			</div>
			</div>
		  </div>
     <?php	
	}
	public function recreationsedit()
	{
	   $formsID = $this->input->post('accom_id');	
	   $forms_info=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
	   ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script>     
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
      <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="recreations_manage">Manage Recreations</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Recreations</h4>
          <div class="user-info-body">
        <form class="msform" id="recreationsform" name="recreationsform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'owner/updatetours/'.$formsID.'' ?>"> 
        <!-- progressbar -->
            <ul id="progressbarrecreations" class="progressbar">
                  <li class="active">Recreations info</li>
                  <li>Contact info:</li>
                  <li>Rules </li>
                  <li>Location </li>
                  <li class="big-text">pricing & cancellation</li>
                  <li>Schedule</li>
                  <li>Gallery</li>
                <!--<li>Chargeable /free items </li>-->
            </ul>
        <!-- fieldsets -->
         <input type="hidden" id="checkupdat_recreationse" value="update">
        <fieldset id="recreations1" class="progressbarrecreations">
            <h2 class="fs-title">Recreations info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_recreations"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title']; ?>"/></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title_arb" placeholder="Title" id="form_title_arb_recreations"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title_arb']; ?>" dir="rtl"/></div>
            <div class="col-md-6 col-sm-6 acc-input-file">
            <input type="file" name="form_image" placeholder="Titile" id="form_image_recreations"  /><img src="<?php echo base_url().'uploads/forms/'.$forms_info[0]['form_image']; ?>" width="50" height="50"></div>
            <div class="col-md-6 col-sm-6"><input type="hidden" name="old_img" value="<?php echo $forms_info[0]['form_image']; ?>" /></div>
            <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_recreations">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'4','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_recreations">
               <option selected="selected">Select subcategory</option>
                <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$forms_info[0]['categoryID'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
					     $sel1=''; 
			            if($sub['category_id']==$forms_info[0]['subcategoryID'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <input type="text" name="form_duration" id="form_duration_recreations" value="<?php echo $forms_info[0]['form_duration']; ?>" placeholder="duration">
            </div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_recreations" class="form-control" placeholder="Description"><?php echo $forms_info[0]['form_desc']; ?></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc_arb" id="form_desc_arb_recreations" class="form-control" placeholder="Arbic Description" dir="rtl"><?php echo $forms_info[0]['form_desc_arb']; ?></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_recreations" class="form-control" placeholder="Why you"><?php echo $forms_info[0]['form_why_you']; ?></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you_arb" id="form_why_you_arb_recreations" class="form-control" placeholder="Arbic Why you" dir="rtl"><?php echo $forms_info[0]['form_why_you_arb']; ?></textarea></div>
            <!--<div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_recreations" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_address']; ?></textarea></div>-->
            <div style="clear:both;"></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations2"  class="progressbarrecreations">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="form_name" id="form_name_recreations" placeholder="Name" value="<?php echo $forms_info[0]['form_name']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_name_arb" id="form_name_arb_recreations" placeholder="Arbic Name" value="<?php echo $forms_info[0]['form_name_arb']; ?>" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_recreations" placeholder="position" value="<?php echo $forms_info[0]['form_position']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_position_arb" id="form_position_arb_recreations" placeholder="Arbic position" value="<?php echo $forms_info[0]['form_position_arb']; ?>" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_recreations" placeholder="Email" value="<?php echo $forms_info[0]['form_email']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_mobile"  id="form_mobile_recreations" placeholder="mobile number" value="<?php echo $forms_info[0]['form_mobile']; ?>"  onkeypress="return OnlyNumericKeys(event);" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
          </fieldset>
        <fieldset id="recreations3" class="progressbarrecreations">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
             <div class="col-md-12 col-sm-12">
            <select name="form_gender" class="form-control" id="form_gender_recreations">
               <option  value="">Select gender</option>
               <option value="none" <?php if($forms_info[0]['form_gender']=='none'){ ?> selected="selected" <?php } ?>>None</option>
               <option value="Male" <?php if($forms_info[0]['form_gender']=='Male'){ ?> selected="selected" <?php } ?>>Male</option>
               <option value="Female" <?php if($forms_info[0]['form_gender']=='Female'){ ?> selected="selected" <?php } ?>>Female</option>
            </select>
            </div>
             <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_recreations" placeholder="Age" value="<?php echo $forms_info[0]['form_age']; ?>" />
            </div>
             <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_recreations" placeholder="Weight" value="<?php echo $forms_info[0]['form_weight']; ?>" />
            </div>
             <div class="col-md-6 col-sm-6">
            <span class="allowed-span">Allowed</span> 
               <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
			   if(count($fetch_array))
			   {
				   foreach($fetch_array as $row)
				   {
					   $select ='';
					   if(in_array($row['facilities_id'],$row['form_what_allowed']))
					   {
						   $select = "checked='checked'";
					   }
			   ?>
                    <label class="labelcheck"><input <?php echo $select; ?> type="checkbox" name="form_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
               <?php
				   }
			   }
			   ?>
               <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
            </div>
             <div class="col-md-6 col-sm-6">
             <span class="allowed-span">Not Allowed</span>
               <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
			   if(count($fetch_array))
			   {
				   foreach($fetch_array as $row)
				   {
					    $select ='';
					   if(in_array($row['facilities_id'],$row['form_not_allowed']))
					   {
						   $select = "checked='checked'";
					   }
			   ?>
                    <label class="labelcheck"><input <?php echo $select; ?> type="checkbox" name="form_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
               <?php
				   }
			   }
			   ?>
            <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
             <div class="col-md-6 col-sm-6">
             <textarea name="form_additional_info" id="form_additional_info_recreations" class="form-control" placeholder="Additional info"><?php echo $forms_info[0]['form_additional_info']; ?></textarea>
            </div>
            <div class="col-md-6 col-sm-6">
             <textarea name="form_additional_info_arb" id="form_additional_info_arb_recreations" class="form-control" placeholder="Arbic Additional info" dir="rtl"><?php echo $forms_info[0]['form_additional_info_arb']; ?></textarea>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation7"  class="progressbarrecreation">
            <h2 class="fs-title">Location</h2>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location_address" id="form_location_address_recreation" placeholder="Location Address" value="<?php echo $forms_info[0]['form_location_address']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12"> <input type="text" name="form_location" id="form_location_recreations" placeholder="Location" value="<?php echo $forms_info[0]['form_location']; ?>" /></div>
            <div class="col-md-12 col-sm-12"><div id="gmap_recreations" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset  id="recreations4" class="progressbarrecreations">
            <h2 class="fs-title">Price &  Cancellation policy</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
              <div class="col-xs-10">
                  <div class="col-xs-4">Family</div>
                   <div class="col-xs-4">
                        <label >
                           <input type="checkbox" class="chk_click" id="form_family_member_recreation" rel='form_family_member_recreationdiv' value="yes" name='form_family' <?php if($forms_info[0]['form_family']=='yes'){echo 'checked="checked"';} ?>><span> yes</span>
                        </label>
                   </div>
              </div>
              <div class="col-xs-10" id="form_family_member_recreationdiv" style=" <?php if($forms_info[0]['form_family']=='yes'){ echo "display:show";}else{ echo "display:none";} ?>">
               <div class="col-xs-4">&nbsp;</div>
                <div class="col-xs-3">
                 <input type="text" id="form_family_member_recreation" placeholder="Maximum member"  name="form_family_member" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_family_member']; ?>" />
               </div>
               <div class="col-xs-3">
                 <input type="text" id="form_family_price_recreation" placeholder="Price"  name="form_family_price" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_family_price']; ?>"  />
               </div>
              </div>
              <div class="col-xs-10">
                <div class="col-xs-4">Children Less Than</div>
                <div class="col-xs-3">
                  <select id="form_lessthan_age_recreation" class="form-control" name="form_lessthan_age">
                    <?php for($i=0;$i<=18;$i++){ 
					       $selected='';
					       if($forms_info[0]['form_lessthan_age']==$i)
						   {
							 $selected="selected='selected'";
						   }
					?>
                    <option value="<?php echo $i; ?>" <?php echo  $selected; ?>><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-xs-3">
                 <input type="text" id="form_lessthan_price_recreation" placeholder="Price"  name="form_lessthan_price" onkeypress="return OnlyNumericKeys(event);"  value="<?php echo $forms_info[0]['form_lessthan_price'];  ?>"/>
                </div>
              </div> 
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_member_price" id="form_member_price_recreation" placeholder="Price" onkeypress="return OnlyNumericKeys(event);"  value="<?php echo $forms_info[0]['form_member_price'];  ?>" />
                </div>
              </div> 
              <div class="col-xs-10">
                <div class="col-xs-4">Group</div>
                <div class="col-xs-3">
                  <input type="text" name="form_min_member" id="form_min_member_recreation" placeholder="Min"  value="<?php echo $forms_info[0]['form_min_member'];  ?>"   onkeypress="return OnlyNumericKeys(event);" />
                </div>
                <div class="col-xs-3">
                  <input type="text" name="form_max_member" id="form_max_member_recreation" placeholder="Max" value="<?php echo $forms_info[0]['form_max_member']; ?>"  onkeypress="return OnlyNumericKeys(event);" />
                </div>
               </div>
              <div class="col-xs-10">
                    <div class="col-xs-4">Group Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_grp_price" id="form_grp_price_recreations" placeholder="Form group price"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $forms_info[0]['form_grp_price']; ?>" />
                    </div>
               </div>
               <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenitiesform <?php if($forms_info[0]['form_refundable']=='yes'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_recreations" rel="form_refundable_recreations" value="yes" name='form_refundable' <?php if($forms_info[0]['form_refundable']=='yes'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn amenitiesform <?php if($forms_info[0]['form_refundable']=='no'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_recreations1" rel="form_refundable_recreations" value="no"  name='form_refundable' <?php if($forms_info[0]['form_refundable']=='no'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
               </div>
               <div class="col-md-12">
                  <div id="form_refundable_recreations_yes" <?php if($forms_info[0]['form_refundable']=='no'){ ?> style="display:none;" <?php } ?>>
                      <div class="col-xs-3">chargeable if cancelled before: </div>
                      <div class="col-xs-8">
                        <div class="col-xs-5">
                         <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_recreations" value="<?php echo $forms_info[0]['form_cancel_before']; ?>" placeholder="% chargeable if cancelled before "></div>
                        <div class="col-xs-4">
                          <input type="input" placeholder="hrs"  id="form_not_cancel_recreations" name="form_not_cancel_hrs" value="<?php echo $forms_info[0]['form_not_cancel_hrs']; ?>"  class="form-control">
                        </div>
                      </div>
                     <div class="clr"></div>
                     <div class="col-md-12">
                          <div class="col-xs-3"></div>
                           <div class="col-xs-6" style="margin-left:10px;">                    
                            <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_recreations" placeholder="non refundable price" value="<?php echo $forms_info[0]['form_not_cancel']; ?>">
                          </div>                   
                        </div>
                    </div>
               </div>
             </div>  
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations5" class="progressbarrecreations">
        <h2 class="fs-title">Schedule</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_recreations" rel="recreations" value="daily" name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_recreations" rel="recreations" value="weekly"  name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_recreations" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> style="display:none;" <?php } ?>>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_recreations" placeholder="Select available time" value="<?php echo $forms_info[0]['form_available_from_date']; ?>" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_recreations"  placeholder="Select available time" value="<?php echo $forms_info[0]['form_availalbe_to_date']; ?>" />
                 </div>
                </div>
            </div> 
            </div>
            
            <div id="weekly_recreations" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> style="display:none;" <?php } ?>>
            <?php 
			$formsID = $this->input->post('accom_id');	
	   		$days_info=$this->master_model->getRecords('tbl_available_days',array('formID'=>$formsID));
			$cnt_available_day=count($days_info);
			$available_arr=array();
			$available_arr1=array();
			if($cnt_available_day>0)
			{
				for($d=0;$d<$cnt_available_day;$d++)
				{$available_arr[$d]=$days_info[$d]['day'];}
			}
			else
			{$available_arr=array();}
			
			?>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Days</div>
                  <div class="col-xs-6" data-toggle="buttons">
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
								 $key=-1;
								if(in_array($days_arr[$i],$available_arr))
								{
									$key=array_search($days_arr[$i],$available_arr);
								}
								if($key!=(-1)){ $days_info[$key]['form_from_time'];$style="checked='checked'";$style1="active";}else{$style="";$style1="";}
				   ?>
                   <label class="btn <?php echo $style1; ?>">
                    <input type="checkbox" <?php echo $style; ?> name="form_available_day[]" id="form_available_day_recreations_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                    <input type="text" name="form_from_time_adventure_<?php echo $days_arr[$i];?>" id="form_availalbe_from_time_tours" class="form_availalbe_from_time_recreations"  placeholder="Select available time" value="<?php if($key!=(-1)){ echo date('H:i:s',strtotime($days_info[$key]['form_from_time']));} ?>" />
                    <input class="form_availalbe_to_time_recreations" type="text" name="form_to_time_adventure_<?php echo $days_arr[$i];?>" id="form_availalbe_to_time_tours"  placeholder="Select available time" value="<?php if($key!=(-1)){ echo date('H:i:s',strtotime($days_info[$key]['form_to_time']));} ?>" />
                    <?php }  } ?>
                  </div>
                </div>
            </div>
            
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_recreations" class="form-control" placeholder="Departure Point"><?php echo $forms_info[0]['form_departure']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_recreations" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_departure_address']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_recreations"  placeholder="Departure Time" value="<?php echo $forms_info[0]['form_departure_time']; ?>" />
                 </div>
                </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations6" class="progressbarrecreations">
                  <h2 class="fs-title">Gallery</h2>
                  <div class="col-md-6 col-sm-6"><input type="file" name="form_images_name[]" id="form_images_recreations" multiple placeholder="Image" /></div>
                  <div class="clearfix"></div>
                  <div class="image-set gallery-box">
                  <?php $accoun_image=$this->master_model->getRecords('tbl_threeforms_images',array('form_id'=>$formsID)); 
				   if(count($accoun_image))
				   {
					  foreach($accoun_image as $row)
					  {
				    ?>
                      <div class="col-md-3 col-sm-4">
                        <div class="image-wrapper">
                          <img src="<?php  echo base_url().'uploads/form/'.$row['form_images_name']; ?>" width="175" height="144">
                          <div class="img-caption">
							<div class="link">
								<a href="javascript:void(0);" class="deletelink" rel="forms" data-id="<?php echo $row['form_images_id'];?>"><i class="fa fa-times"></i></a>
							</div>
						</div>      
                        </div>
                     </div>
				  <?php
					  }
                  }
				   ?>
                  </div>
                  <div class="clearfix"></div>
                    <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_recreations" name="recreationssubmit" class="submit action-button" value="Submit" />
                 
        </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_recreations').datetimepicker({
			  format:'Y-m-d',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_recreations').val()?jQuery('#form_availalbe_to_date_recreations').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_recreations').datetimepicker({
			  format:'Y-m-d',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_recreations').val()?jQuery('#form_available_from_date_recreations').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_recreations').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_recreations').val()?jQuery('.form_availalbe_to_time_recreations').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_recreations').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_recreations').val()?jQuery('.form_available_from_time_recreations').val():false
			   })
			  },
		});
});
		function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('gmap_recreation'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('form_location_address_recreation');
        var searchBox = new google.maps.places.SearchBox(input);
       // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
		  
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
			var fisrtStr=place.geometry.location.toString().trim();
			fisrtStr = fisrtStr.slice(1, -1);;
			document.getElementById("form_location_recreation").value = fisrtStr;
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };
			
            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));
			
			
            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }  
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	function getAddress(latLng) {
    geocoder.geocode( {'latLng': latLng},
      function(results, status) {
        if(status == google.maps.GeocoderStatus.OK) {
          if(results[0]) {
			  //alert(results[0].formatted_address);
            document.getElementById("form_location_address_recreation").value = results[0].formatted_address;
          }
          else {
			 // alert("No results");
           // document.getElementById("address").value = "No results";
          }
        }
        else {
			//alert(status);
          //document.getElementById("address").value = status;
        }
      });
    }
	$(document).on('click','.action-button', function(){
		 initAutocomplete();
	});
    window.onload = function () { initAutocomplete() };
</script>
        </div>
        </div>
      </div>
      <?php	
	}
    public function couponedit()
	{
	  $coupon_id = $this->input->post('accom_id');	
	  $fetch_single_info=$this->master_model->getRecords('tbl_coupon_master',array('coupon_id'=>$coupon_id));	 	
	 ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="coupon_manage">Manage Coupon</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Coupon</h4>
          <div class="user-info-body">
        <form class="msform" id="couponform" name="couponform" method="post" action="<?php echo base_url().'owner/updateform/'.$coupon_id.'' ?>" enctype="multipart/form-data"> 
        <!-- progressbar -->
            <ul id="progressbarcoupon" class="progressbar">
                <li class="active">Basic info</li>
                <li>Rules </li>
                <li>Location </li>
                <li>Pricing </li>
                <!--<li>Chargeable /free items </li>-->
                <li class="big-text">Cancel &amp; Refund</li>
                <li>Gallery :</li>
                <li>Contact info:</li>
            </ul>
             <input type="hidden" id="checkupdate_coupon" value="update">
        <!-- fieldsets -->
        <fieldset id="coupon1" class="progressbarcoupon">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"> 
            <input type="text" name="coupon_title" placeholder="Title" id="coupon_title"  data-rule-required='true' value="<?php echo $fetch_single_info[0]['coupon_title']; ?>" />
            </div>
             <div class="col-md-6 col-sm-6"> 
            <input type="text" name="coupon_title_arb" placeholder="Arabic Title" id="coupon_title_arb"   value="<?php echo $fetch_single_info[0]['coupon_title_arb']; ?>" dir="rtl"/>
            </div>
            
            <div class="col-md-6 col-sm-6">  
              <input type="file" name="coupon_image" placeholder="Image" id="coupon_image"  data-rule-required='true'/>
              <img src="<?php  echo base_url().'uploads/coupon/'.$fetch_single_info[0]['coupon_image']; ?>" width="50" height="50">
            </div>
            
            <div class="col-md-6 col-sm-6">
            <span class="allowed-span">Type</span> 
           <label class="labelcheck"><input name="coupon_type" <?php if($fetch_single_info[0]['coupon_type']=='tickets') {echo 'checked="checked"';	} ?>  id="" value="tickets" type="radio" >Tickets </label>
           <label class="labelcheck"><input name="coupon_type" <?php if($fetch_single_info[0]['coupon_type']=='coupon') {echo 'checked="checked"';	} ?>  id="" value="coupon" type="radio">Coupon </label>
        </div>
        
             <div style="clear:both;"></div>
            
            <div class="col-md-6 col-sm-6"> 
            <select name="category_id" class="form-control" id="coupon_category_id">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'7','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
						 $sel='';
						 if(isset($fetch_single_info[0]['category_id']))
						 {
							 if($fetch_single_info[0]['category_id']==$row['category_id'])
							 {
								 $sel="selected='selected'";	
							 }
						 }
                    ?>
                    <option value="<?php echo $row['category_id']; ?>" <?php echo $sel; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            
            <div class="col-md-6 col-sm-6"> 
            <select name="subcategory_id" class="form-control" id="subcategory_id">
               <option selected="selected">Select subcategory</option>
                 <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$fetch_single_info[0]['category_id'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
					    $sel1='';
						if(isset($fetch_single_info[0]['subcategory_id']))
						{
							if($sub['category_id']==$fetch_single_info[0]['subcategory_id'])
							{
								$sel1="selected='selected'";
							}
						}
			        ?>
                     <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                   <?php
				   }
			   }
			   ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"> 
            <textarea name="coupon_location" id="coupon_location" class="form-control" placeholder="location"><?php echo $fetch_single_info[0]['coupon_location']; ?></textarea>
            </div>
            <div class="col-md-6 col-sm-6"> 
            <textarea name="coupon_address" id="coupon_address" class="form-control" placeholder="Address"><?php echo $fetch_single_info[0]['coupon_address']; ?></textarea>
            </div>
           
             <div class="col-md-6 col-sm-6"> <textarea name="coupon_description" id="coupon_description" class="form-control" placeholder="Description"><?php echo $fetch_single_info[0]['coupon_description']; ?></textarea></div>
             
             <div class="col-md-6 col-sm-6"> <textarea name="coupon_description_arb" id="coupon_description_arb" class="form-control" placeholder="Arabic Description" dir="rtl"><?php echo $fetch_single_info[0]['coupon_description_arb']; ?></textarea></div>
             
             
              <div class="col-md-6 col-sm-6"> <textarea name="coupon_why_you" id="coupon_why_you" class="form-control" placeholder="Why You"><?php echo $fetch_single_info[0]['coupon_why_you']; ?></textarea></div>
              
               <div class="col-md-6 col-sm-6"> <textarea name="coupon_why_you_arb" id="coupon_why_you_arb" class="form-control" placeholder="Arabic Why You" dir="rtl"><?php echo $fetch_single_info[0]['coupon_why_you_arb']; ?></textarea></div>
               
            <br/>
            <input type="button" name="next" class="next action-button" value="Next" />
            
        </fieldset>
        <fieldset id="coupon2"  class="progressbarcoupon">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
            <div class="col-md-12 col-sm-12"> 
            <select name="gender" class="form-control" id="coupon_gender">
               <option value="none"<?php if($fetch_single_info[0]['gender']=='none'){echo "selected='selected'";} ?>>none</option>
               <option value="Male" <?php if($fetch_single_info[0]['gender']=='Male'){echo "selected='selected'";} ?>>Male</option>
               <option value="Female" <?php if($fetch_single_info[0]['gender']=='Female'){echo "selected='selected'";} ?>>Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"> 
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="age" id="age" placeholder="Age" value="<?php echo $fetch_single_info[0]['age']; ?>" />
            </div>
             <div class="col-md-6 col-sm-6"> 
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="weight" id="weight" placeholder="Weight" value="<?php echo $fetch_single_info[0]['weight']; ?>" />
            </div>
            
               <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">Allowed</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox='';
						   if($fetch_single_info[0]['coupon_what_allowed'])
						   {
							   $whatallowed=explode(',',$fetch_single_info[0]['coupon_what_allowed']);
							   if(in_array($row['facilities_id'],$whatallowed))
							   {
								$checkbox='checked="checked"';
							   }
						   }
                   ?>
                        <label class="labelcheck"><input  <?php echo $checkbox; ?> type="checkbox" name="coupon_what_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
            <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">Not Allowed </span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_facilities_master',array('facilities_status'=>'active','facilities_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox1='';
						   if($fetch_single_info[0]['coupon_not_allowed'])
						   {
							   $whatallowed=explode(',',$fetch_single_info[0]['coupon_not_allowed']);
							   if(in_array($row['facilities_id'],$whatallowed))
							   {
								$checkbox1='checked="checked"';
							   }
						   }
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox1; ?> type="checkbox" name="coupon_not_allowed[]" id="" value="<?php echo $row['facilities_id'];  ?>"><?php echo $row['facilities_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
        </div>    
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <span class="allowed-span">What Included</span> 
                   <?php 
                   $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox2='';
						   if($fetch_single_info[0]['coupon_what_included'])
						   {
							   $what_included=explode(',',$fetch_single_info[0]['coupon_what_included']);
							   if(in_array($row['included_id'],$what_included))
							   {
								$checkbox2='checked="checked"';
							   }
						   }   
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox2; ?> type="checkbox" name="coupon_what_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                   <!--<textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea>-->
                </div>
               <div class="col-md-6 col-sm-6">
                   <span class="allowed-span">What not Included</span>
                   <?php $fetch_array=$this->master_model->getRecords('tbl_included_master',array('included_status'=>'active','included_not_allowed'=>'yes')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox3='';
						   if($fetch_single_info[0]['coupon_not_included'])
						   {
							   $what_included=explode(',',$fetch_single_info[0]['coupon_not_included']);
							   if(in_array($row['included_id'],$what_included))
							   {
								$checkbox3='checked="checked"';
							   }
						   } 
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox3; ?> type="checkbox" name="coupon_not_included[]" id="" value="<?php echo $row['included_id'];  ?>"><?php echo $row['included_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div> 
        </div>
        <div class="row">
          <div class="col-md-6 col-sm-6">
              <span class="allowed-span">Requirement </span>
                   <?php 
				   $fetch_array=$this->master_model->getRecords('tbl_requirement_master',array('requirement_status'=>'active')); 
                   if(count($fetch_array))
                   {
                       foreach($fetch_array as $row)
                       {
						   $checkbox4='';
						   if($fetch_single_info[0]['coupon_requirement'])
						   {
							   $whatincluded=explode(',',$fetch_single_info[0]['coupon_requirement']);
							   if(in_array($row['requirement_id'],$whatincluded))
							   {
								 $checkbox4='checked="checked"';
							   }
						   } 
                   ?>
                        <label class="labelcheck"><input <?php echo $checkbox4; ?> type="checkbox" name="coupon_requirement[]" id="" value="<?php echo $row['requirement_id'];  ?>"><?php echo $row['requirement_name_eng']; ?></label>
                   <?php
                       }
                   }
                   ?>
                <!--<textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea>--></div>
        </div>
            
            
            <!-- <div class="col-md-6 col-sm-6"> 
            <textarea name="coupon_what_allowed" id="coupon_what_allowed" class="form-control" placeholder="what allowed"><?php //echo $fetch_single_info[0]['coupon_what_allowed']; ?></textarea>
            </div>
             <div class="col-md-6 col-sm-6"> 
            <textarea name="coupon_not_allowed" id="coupon_not_allowed" class="form-control" placeholder="Not Allowed"><?php //echo $fetch_single_info[0]['coupon_not_allowed']; ?></textarea>
            </div>-->
            <div class="col-md-12 col-sm-12"> 
            <textarea name="additiona_info" id="additiona_info" class="form-control" placeholder="Other Rules"><?php echo $fetch_single_info[0]['additiona_info']; ?></textarea>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon7" class="progressbarcoupon">
            <h2 class="fs-title">Location</h2>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="coupon_location_address" id="coupon_location_address_coupon" placeholder="Location Address" value="<?php echo $fetch_single_info[0]['coupon_location_address']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12" style="display:none;"><input type="text" name="coupon_location_map" id="coupon_location_coupon" placeholder="Location" value="<?php echo $fetch_single_info[0]['coupon_location_map']; ?>"  /></div>
            <div class="col-md-12 col-sm-12"> <div id="gmap_coupon" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon3"  class="progressbarcoupon">
        <h2 class="fs-title">Pricing </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                    <input type="text" name="price" id="price" placeholder="Price" value="<?php echo $fetch_single_info[0]['price']; ?>" onkeypress="return OnlyNumericKeys(event);" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                <div class="col-xs-4">Reservation Amount </div>
                    <div class="col-xs-6">
                    <input type="text" name="reservation_price" id="reservation_price" value="<?php echo $fetch_single_info[0]['reservation_price']; ?>" placeholder="Reservation Amount"  onkeypress="return OnlyNumericKeys(event);" />
                    </div>
               </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                    <div class="col-xs-4">Bird discount</div>
                    <div class="col-xs-3" data-toggle="buttons">
                       <input type="text" name="bird_days_before" id="bird_days_before" placeholder="Bird days befor" value="<?php echo $fetch_single_info[0]['bird_days_before']; ?>" /> 
                   </div>	
                   <div data-toggle="buttons" class="col-xs-3">
                       <input type="text" placeholder="Bird discount" id="bird_discount" name="bird_discount" value="<?php echo $fetch_single_info[0]['bird_discount']; ?>"> 
                    </div>
                </div>
            </div>
             
        
            <div class="row">
            <div class="col-xs-10">
             <div class="col-xs-4">Discount</div>
             <div class="col-xs-4" data-toggle="buttons">
                <label class="btn discount  <?php if($fetch_single_info[0]['discount']=='on'){ ?> active <?php } ?>">
                <input type="radio" <?php if($fetch_single_info[0]['discount']=='on'){ ?> checked='checked' <?php } ?> id="discount" value="on" name='discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
                </label>
                <label class="btn discount  <?php if($fetch_single_info[0]['discount']=='off'){ ?> active <?php } ?>">
                <input type="radio" <?php if($fetch_single_info[0]['discount']=='off'){ ?> checked='checked' <?php } ?> id="discount1" value="off"  name='discount' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
                </label>
            </div>
            </div>
            </div> 
            <div class="row" id="discount_on" style=" <?php if($fetch_single_info[0]['discount']=='on'){ echo 'display:block;'; }else{ echo 'display:none;';} ?>">
                 <div class="col-xs-10">
                    <div class="col-xs-4">Discount Price</div>
                    <div class="col-sm-4">
                       <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="precentage_discount" id="precentage_discount" value="<?php echo $fetch_single_info[0]['precentage_discount']; ?>" placeholder="discount price in percantage">
                    </div>
                </div>  
                <div class="col-xs-10">
                    <div class="col-xs-4">Discount From date</div>
                    <div class="col-sm-6">
                       <input type="text"  class="form-control" name="discount_from_expiry" id="discount_from_expiry" value="<?php echo date('Y-m-d H:i:s',strtotime($fetch_single_info[0]['discount_from_expiry'])); ?>" placeholder="From date">
                    </div>
                </div>
                <div class="col-xs-10">
                    <div class="col-xs-4">Discount To date</div>
                    <div class="col-sm-6">
                       <input type="text"   class="form-control" name="discount_to_expiry" id="discount_to_expiry" value="<?php echo date('Y-m-d H:i:s',strtotime($fetch_single_info[0]['discount_to_expiry'])); ?>" placeholder="To date">
                    </div>
                </div>    
           </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset  id="coupon4" class="progressbarcoupon">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn couponcancel active">
                        <input type="radio" id="cancel_policy" value="yes" name='cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel">
                        <input type="radio" id="cancel_policy" value="no"  name='cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="cancel_policy_yes" style="display:none;">
                      <input type="input" class="form-control" name="cancel_policy_price" id="cancel_policy_price" value="<?php echo $fetch_single_info[0]['cancel_policy_price']; ?>">
                    </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon5" class="progressbaraccom">
          <h2 class="fs-title">Gallery</h2>
          <div class="col-md-6 col-sm-6"><input type="file" name="coupon_images_name[]" id="coupon_images_name" multiple placeholder="Image" /></div>
          <div class="clearfix"></div>
          <div class="image-set gallery-box">
              <?php 
			   $image=$this->master_model->getRecords('tbl_coupon_images',array('coupon_id'=>$coupon_id)); 
			  if(count($image))
			  {
				  foreach($image as $row)
				  {
            ?>    <div class="col-md-3 col-sm-4">
                    <div class="image-wrapper">
                      <img src="<?php  echo base_url().'uploads/coupon/'.$row['coupon_images_name']; ?>" width="175" height="144">
                      <div class="img-caption">
                        <div class="link">
                            <a href="javascript:void(0);" class="deletelink" rel="coupon" data-id="<?php echo $row['coupon_images_id'];?>"><i class="fa fa-times"></i></a>
                        </div>
                    </div>      
                    </div>
                 </div>
              <?php
                  }
              }
              ?>
             </div>
            <div class="clearfix"></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
           <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon6" class="progressbarcoupon">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="col-md-6 col-sm-6"><input type="text" name="coupon_user_name" id="coupon_user_name" placeholder="Name" value="<?php echo $fetch_single_info[0]['coupon_user_name'];  ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_user_name_arb" id="coupon_user_name_arb" placeholder="Arabic Name" value="<?php echo $fetch_single_info[0]['coupon_user_name_arb'];  ?>" dir="rtl" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_person_postion" id="coupon_person_postion" placeholder="position" value="<?php echo $fetch_single_info[0]['coupon_person_postion'];  ?>" /></div>
             <div class="col-md-6 col-sm-6"><input type="text" name="coupon_person_postion_arb" id="coupon_person_postion_arb" placeholder="Arabic position" dir="rtl" value="<?php echo $fetch_single_info[0]['coupon_person_postion_arb'];  ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_email"  id="coupon_email" placeholder="Email" value="<?php echo $fetch_single_info[0]['coupon_email'];  ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="coupon_mobile"  id="coupon_mobile" placeholder="mobile number" value="<?php echo $fetch_single_info[0]['coupon_mobile'];  ?>"    onkeypress="return OnlyNumericKeys(event);"/></div>
            <div class="col-md-6 col-sm-6"><input type="button" name="previous" class="previous action-button" value="Previous" /></div>
            <div class="col-md-6 col-sm-6"><input type="submit" id="couponform" name="couponsubmit" class="submit action-button" value="Submit" /></div>
             
                <!-- <div class="col-md-6 col-sm-6"> 
                <input type="text" name="coupon_user_name" id="coupon_user_name" placeholder="Name" value="<?php //echo $fetch_single_info[0]['coupon_user_name']; ?>" />
                </div>
                <div class="col-md-6 col-sm-6"> 
                <input type="text" name="coupon_person_postion" id="coupon_person_postion" placeholder="position" value="<?php //echo $fetch_single_info[0]['coupon_person_postion']; ?>" />
                </div>
                 <div class="col-md-6 col-sm-6"> 
                <input type="text" name="coupon_email"  id="coupon_email" placeholder="Email" value="<?php //echo $fetch_single_info[0]['coupon_email']; ?>" />
               </div>
                <div class="col-md-6 col-sm-6"> 
                <input type="text" name="coupon_mobile"  id="coupon_mobile" placeholder="mobile number" value="<?php //echo $fetch_single_info[0]['coupon_mobile']; ?>"   onkeypress="return OnlyNumericKeys(event);"/>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="couponform" name="couponsubmit" class="submit action-button" value="Submit" />-->
          </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
		jQuery(document).ready(function(e) {
            jQuery('#discount_from_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					maxDate:jQuery('#discount_to_expiry').val()?jQuery('#discount_to_expiry').val():false
				   })
				  },
			 
			 });
			jQuery('#discount_to_expiry').datetimepicker({
				  format:'Y-m-d H:i:s',
				  onShow:function( ct ){
				   this.setOptions({
					minDate:jQuery('#discount_to_expiry').val()?jQuery('#discount_to_expiry').val():false
				   })
				  },
			});
        });
	function initAutocomplete() {
			var map = new google.maps.Map(document.getElementById('gmap_coupon'), {
			  center: {lat: -33.8688, lng: 151.2195},
			  zoom: 13,
			  mapTypeId: 'roadmap'
			});
	
			// Create the search box and link it to the UI element.
			var input = document.getElementById('coupon_location_address_coupon');
			var searchBox = new google.maps.places.SearchBox(input);
		   // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	
			// Bias the SearchBox results towards current map's viewport.
			map.addListener('bounds_changed', function() {
			  searchBox.setBounds(map.getBounds());
			});
	
			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();
	
			  if (places.length == 0) {
				return;
			  }
	
			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];
	
			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var fisrtStr=place.geometry.location.toString().trim();
				fisrtStr = fisrtStr.slice(1, -1); alert(fisrtStr);
				document.getElementById("coupon_location_coupon").value = fisrtStr;
				var icon = {
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};
				
				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: map,
				  icon: icon,
				  title: place.name,
				  position: place.geometry.location
				}));
				
				
				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  });
			  map.fitBounds(bounds);
			});
		  }
		function clearOverlays() {
		  for (var i = 0; i < markersArray.length; i++ ) {
			markersArray[i].setMap(null);
		  }
		  markersArray.length = 0;
		}
		function getAddress(latLng) {
		geocoder.geocode( {'latLng': latLng},
		  function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
			  if(results[0]) {
				  //alert(results[0].formatted_address);
				document.getElementById("coupon_location_address_coupon").value = results[0].formatted_address;
			  }
			  else {
				 // alert("No results");
			   // document.getElementById("address").value = "No results";
			  }
			}
			else {
				//alert(status);
			  //document.getElementById("address").value = status;
			}
		  });
		}
		$(document).on('click','.action-button', function(){
			 initAutocomplete();
		});
		window.onload = function () { initAutocomplete() };
	</script>
      <?php	
	
	
	}
	public function eventedit()
	{
      $event_id = $this->input->post('accom_id');	
	  $fetch_single_info=$this->master_model->getRecords('tbl_event_master',array('event_id'=>$event_id));	
	 ?>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
     <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
     <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
     <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="events_manage">Manage Events</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Events</h4>
          <div class="user-info-body">
        <form class="msform" id="eventform" name="eventform" method="post" action="<?php echo base_url().'owner/updateform/'.$event_id.'' ?>"> 
        <!-- progressbar -->
         <ul id="progressbarevent" class="progressbar">
            <li class="active">Basic info</li>
            <li>Address  </li>
            <li>Gallery :</li>
            <li>Contact info  </li>
        </ul>
         <input type="hidden" id="checkupdate_event" value="update">
        <!-- fieldsets -->
        <fieldset id="event1" class="progressbarevent">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6">   
            
            <input type="text" value="<?php echo $fetch_single_info[0]['event_title']; ?>" name="event_title" placeholder="Titile" id="event_title"  data-rule-required='true'/>
            </div>
            <div class="col-md-6 col-sm-6">
            <input style="padding:0px;" type="file" name="event_image" placeholder="Image" id="event_image"  data-rule-required='true'/>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="event_category_id" class="form-control" id="event_category_id">
                <option value="">Select category</option>
                <?php 
				$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'6','parent_id'=>'0','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
						$selected='';
						if(isset($fetch_single_info[0]['event_category_id']))
                        {
							if($row['category_id']==$fetch_single_info[0]['event_category_id'])
							{
								$selected="selected='selected'";
							}
						}
                    ?>
                    <option value="<?php echo $row['category_id']; ?>" <?php echo $selected; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="event_subcategory_id" class="form-control" id="event_subcategory_id">
               <option value="">Select subcategory</option>
               <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$fetch_single_info[0]['event_category_id'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
			            if($sub['category_id']==$accom_info[0]['event_subcategory_id'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
            </select>
            </div>
            <div class="col-md-12 col-sm-12">
            <input type="text" value="<?php echo $fetch_single_info[0]['event_duration']; ?>" name="event_duration" id="event_duration" placeholder="duration"  data-rule-required='true'/>
            </div>
            <div class="col-md-12 col-sm-12">
            <textarea name="event_description" id="event_description" class="form-control" placeholder="Description"><?php echo $fetch_single_info[0]['event_description']; ?></textarea>
            </div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event2"  class="progressbarevent">
            <h2 class="fs-title">Address</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
             <div class="col-md-12 col-sm-12">
            <textarea name="event_address" id="event_address" class="form-control" placeholder="Address"><?php echo $fetch_single_info[0]['event_address']; ?></textarea>
            </div>
             <div class="col-md-12 col-sm-12">
            <input type="text" name="event_location" id="event_location" placeholder="Location" value="<?php echo $fetch_single_info[0]['event_location']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12"><div id="gmap_event" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event3" class="progressbarevent">
                  <h2 class="fs-title">Gallery</h2>
                  <div class="col-md-6 col-sm-6"><input type="file" name="event_images_name[]" id="event_images_name" multiple placeholder="Image" /></div>
                  <div class="clearfix"></div>
                  <div class="image-set gallery-box">
                      <?php 
                      $image=$this->master_model->getRecords('tbl_event_images',array('event_id'=>$event_id)); 
                      if(count($image))
                      {
                          foreach($image as $row)
                          {
                    ?>    <div class="col-md-3 col-sm-4">
                            <div class="image-wrapper">
                              <img src="<?php  echo base_url().'uploads/event/'.$row['event_images_name']; ?>" width="175" height="144">
                              <div class="img-caption">
                                <div class="link">
                                    <a href="javascript:void(0);" class="deletelink" rel="event" data-id="<?php echo $row['event_images_id'];?>"><i class="fa fa-times"></i></a>
                                </div>
                            </div>      
                            </div>
                         </div>
                      <?php
                          }
                      }
                      ?>
                     </div>
            <div class="clearfix"></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event4" class="progressbarevent">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="event_user_name" id="event_user_name" placeholder="Name" value="<?php echo $fetch_single_info[0]['event_user_name']; ?>"/></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_position" id="event_position" placeholder="position" value="<?php echo $fetch_single_info[0]['event_position']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_email"  id="event_email" placeholder="Email" value="<?php echo $fetch_single_info[0]['event_email']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_mobile"  id="event_mobile" placeholder="mobile number" value="<?php echo $fetch_single_info[0]['event_mobile']; ?>"  onkeypress="return OnlyNumericKeys(event);" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="eventform" name="eventsubmit" class="submit action-button" value="Submit" />
          </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#event_duration').datetimepicker({
			 datepicker:false,
			  format:'H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				
			   })
			  },
		 
		 });
	 });
     var map;
	 var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_event"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			    // show in input box
                document.getElementById("event_location").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);
            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
    <?php
	
	}
	public function transportationedit()
	{
	  $transportation_id = $this->input->post('accom_id');	
	  $fetch_single_info=$this->master_model->getRecords('tbl_transportation_master',array('transportation_id'=>$transportation_id));	 	  	
	?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
     <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
     <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
     <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="transportation_manage">Manage Transportation</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Transportation</h4>
          <div class="user-info-body">
        <form class="msform" id="transportationform" name="transportationform" method="post" action="<?php echo base_url().'owner/updateform/'.$transportation_id.'' ?>"> 
        <!-- progressbar -->
            <ul id="progressbartransportation" class="progressbar">
                <li class="active">Basic info</li>
                <li>Trans Details</li>
                <li>Gallery :</li>
                <li>Available time </li>
             </ul>
              <input type="hidden" id="checkupdate_transportation" value="update">
            <!-- fieldsets -->
            <fieldset id="transportation1" class="progressbartransportation">
                <h2 class="fs-title">Basic info</h2>
                <input type="text" value="<?php echo  $fetch_single_info[0]['transportation_title']; ?>" name="transportation_title" placeholder="Titile" id="transportation_title"  data-rule-required='true'/>
                <input type="file" name="transportation_image" placeholder="Image" id="transportation_image"  data-rule-required='true'/>
                <img src="<?php echo base_url().'uploads/transportation/'.$fetch_single_info[0]['transportation_image'] ?>" width="200" height="200">
                <select name="transportation_category_id" class="form-control" id="transportation_category_id">
                    <option value="">Select category</option>
                    <?php 
                    $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'5','parent_id'=>'0','category_status'=>'active')); 
                    if(count($category_accom)>0)
                    {
                        foreach($category_accom as $row)
                        {
							$selected='';
							if($row['category_id']==$fetch_single_info[0]['transportation_category_id'])
							{
								$selected="selected='selected'";
							}
                        ?>
                        <option value="<?php echo $row['category_id']; ?>" <?php echo $selected;?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
                <select name="transportation_subcategory_id" class="form-control" id="transportation_subcategory_id">
                   <option  value="">Select subcategory</option>
                   <?php 
				   $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$fetch_single_info[0]['transportation_category_id'],'category_status'=>'active')); 
				   if(count($subcategory_accom)>0)
				   {
					   foreach($subcategory_accom as $sub)
					   {
						    $sel1=''; 
							if($sub['category_id']==$fetch_single_info[0]['transportation_subcategory_id'])
							{
								$sel1="selected='selected'";
							}
				   ?>
						  <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $sub['category_name_'.$this->session->userdata('lang')]; ?></option>
				   <?php
					   }
				   }
			   		?>
                </select>
                <textarea name="transportation_description" id="transportation_description" class="form-control" placeholder="Description"><?php echo $fetch_single_info[0]['transportation_description']; ?></textarea>
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation2"  class="progressbartransportation">
                <h2 class="fs-title">Trans Details</h2>
                <input type="text" name="transportation_car_size" placeholder="Car size" id="transportation_car_size" value="<?php echo  $fetch_single_info[0]['transportation_car_size']; ?>"   data-rule-required='true'/>
                <input type="text" name="transportation_car_model" placeholder="Car model" id="transportation_car_model" value="<?php echo  $fetch_single_info[0]['transportation_car_model']; ?>"  data-rule-required='true'/>
                <input type="text" name="transportation_car_brand" placeholder="Car Brand" id="transportation_car_brand" value="<?php echo  $fetch_single_info[0]['transportation_car_brand']; ?>"  data-rule-required='true'/>
                <input type="text" name="transportation_car" placeholder="Car" id="transportation_car" value="<?php echo  $fetch_single_info[0]['transportation_car']; ?>"  data-rule-required='true'/>
                <input type="text" value="<?php echo  $fetch_single_info[0]['transportation_max_passanger']; ?>" name="transportation_max_passanger" placeholder="max passanger" id="transportation_max_passanger"  data-rule-required='true'/>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation3" class="progressbartransportation">
                  <h2 class="fs-title">Gallery</h2>
                  <div class="col-md-6 col-sm-6"><input type="file" name="transportation_image_name[]" id="transportation_image_name" multiple placeholder="Image" /></div>
                  <div class="clearfix"></div>
                  <div class="image-set gallery-box">
                  <?php 
				  $image=$this->master_model->getRecords('tbl_transportation_images',array('transportation_id'=>$transportation_id)); 
				  if(count($image))
				  {
					  foreach($image as $row)
					  {
                       ?>    
                       <div class="col-md-3 col-sm-4">
                            <div class="image-wrapper">
                              <img src="<?php  echo base_url().'uploads/transportation/'.$row['transportation_image_name']; ?>" width="175" height="144">
                              <div class="img-caption">
                                <div class="link">
                                    <a href="javascript:void(0);" class="deletelink" rel="transportation" data-id="<?php echo $row['transportation_images_id'];?>"><i class="fa fa-times"></i></a>
                                </div>
                            </div>      
                            </div>
                         </div>
				      <?php
                      }
                  }
                  ?>
                  </div>
                 <div class="clearfix"></div>
                 <input type="button" name="previous" class="previous action-button" value="Previous" />
                 <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation4" class="progressbartransportation">
                    <h2 class="fs-title">Available time</h2>
                    <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                    <input type="text" name="transportation_available_time" id="transportation_available_time" placeholder="Available time"  value="<?php echo  $fetch_single_info[0]['transportation_available_time']; ?>" />
                    <input type="text" name="transportation_pickuptime" id="transportation_pickuptime" placeholder="pickuptime" value="<?php echo  $fetch_single_info[0]['transportation_pickuptime']; ?>"  />
                    <input type="text" name="transportation_pickup_location"  id="transportation_pickup_location" placeholder="Pickup location" value="<?php echo  $fetch_single_info[0]['transportation_pickup_location']; ?>"  />
                    <textarea name="transportation_pickup_remarks" id="transportation_pickup_remarks" placeholder="pickup remark"><?php echo  $fetch_single_info[0]['transportation_pickup_remarks']; ?></textarea>
                    <input type="text" name="transportation_drop_location"  id="transportation_drop_location" placeholder="Drop location" value="<?php echo  $fetch_single_info[0]['transportation_drop_location']; ?>"  />
                    <textarea name="transportation_drop_remark" id="transportation_drop_remark" placeholder="Drop remark"><?php echo  $fetch_single_info[0]['transportation_drop_remark']; ?></textarea>
                    <input type="button" name="previous" class="previous action-button" value="Previous" />
                    <input type="submit" id="transportationform" name="transportationsubmit" class="submit action-button" value="Submit" />
              </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#transportation_available_time').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#transportation_pickuptime').val()?jQuery('#transportation_pickuptime').val():false
			   })
			  },
		 });
		jQuery('#transportation_pickuptime').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#transportation_pickuptime').val()?jQuery('#transportation_pickuptime').val():false
			   })
			  },
		}); 
		
		});
		</script>
    <?php
	
	}
	/*editform */
	
}