<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends CI_Controller {
	
	public function set_top_destination()
	{
		$package_id=$_GET['get_id'];
		$get_status=$_GET['get_status'];
		$type      =$_GET['type'];
		if($type=='accom')
		{
			$this->db->set('top_destination_show',$get_status);
			$this->db->where('accom_id',$package_id);
			$success=$this->db->update('tbl_accommodations_master');
			if($success)
			{
				echo "done";
				exit;
			}
		}
		if($type=='threeform')
		{
			$this->db->set('top_destination_show',$get_status);
			$this->db->where('formsID',$package_id);
			$success=$this->db->update('tbl_three_forms');
			if($success)
			{
				echo "done";
				exit;
			}
		}
		if($type=='host')
		{
			$this->db->set('hosts_verify',$get_status);
			$this->db->where('hosts_id',$package_id);
			$success=$this->db->update('tbl_hosts_master');
			if($success)
			{
				echo "done";
				exit;
			}
		}
	   echo "error";
       exit;
	}
	public function set_front()
	{
		$package_id=$_GET['boat_id'];
		$get_status=$_GET['get_status'];
		$getCount=$this->master_model->getRecords('tbl_boatpackage_master',array('set_front'=>'yes'));

		if(count($getCount)<= 3)
		{
			$this->db->set('set_front',$get_status);
			$this->db->where('package_id',$package_id);
			$success=$this->db->update('tbl_boatpackage_master');
			if($success)
			{
				echo "done";
				return false;
			}
			else
			{
				echo "error";
				return false;
			}
		}
		else
		{
			$this->db->set('set_front','no');
			$this->db->where('package_id',$package_id);
			$success=$this->db->update('tbl_boatpackage_master');
			echo 'limit';
			return false;
		}
	}
	public function selectState()
	{
		$sat='<option value="">State</option>';
		$sel='';
		$countryID=$this->input->post('countryID',true);
		$countries=$this->master_model->getRecords('tbl_user_master',array('countryId'=>$countryID));
		$states=$this->master_model->getRecords('tbl_states',array('country_id'=>$countryID),'',array('name'=>'ASC'));
		if(count($states)>0){
			foreach($states as $state)
			{
				if(isset($countries[0]['state']))
				{
					if($countries[0]['state']==$state['id'])
					{
						$sel='selected';
					}
					else
					{
						$sel='';
					}
				}
				$sat .='<option value="'.$state['id'].'" '.$sel.'>'.$state['name'].'</option>';
			}
		}

		echo $sat;exit;
	}
	public function getCategory()
	{
		$cat='<option value="">Select Category</option>';
		$categoryID=$this->input->post('category_id',true);
		$categories=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'category_status'=>'active','parent_id'=>'0'));
		if(count($categories)>0){
			foreach($categories as $category)
			{
			  $cat .='<option value="'.$category['category_id'].'" '.@$sel.'>'.$category['category_name_'.$this->session->userdata('lang')].'</option>';
			}
		}

		echo $cat;exit;
	}
	public function getMaincategory()
	{
		$cat='<option value="">Select Category</option>';
	    $categoryID=$this->input->post('category_id',true);
		$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>$categoryID,'parent_id'=>'0','category_status'=>'active')); 
		if(count($category_accom)>0)
		{
			foreach($category_accom as $row)
			{
			 $cat .='<option value="'.$row['category_id'].'">'.$row['category_name_'.$this->session->userdata('lang')].'</option>';
			}
		} 
		echo $cat;exit;
	}
	public function getSubcategory()
	{
		$cat='<option value="">Select Subcategory</option>';
		$categoryID=$this->input->post('category_id',true);
		$categories=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$categoryID,'category_status'=>'active','parent_id !='=>'0'));
		if(count($categories)>0)
		{
			foreach($categories as $category)
			{
			  $cat .='<option value="'.$category['category_id'].'" '.$sel.'>'.$category['category_name_'.$this->session->userdata('lang')].'</option>';
			}
		}
      echo $cat;exit;
	}
	public function setImageFront()
	{
		$image_id=$this->input->post('image_id',true);
		$package_id=$this->input->post('package_id',true);
		$this->master_model->updateRecord('tbl_boat_image',array('show_front'=>'no'),array('package_id'=>$package_id));
		$this->master_model->updateRecord('tbl_boat_image',array('show_front'=>'yes'),array('boat_image_id'=>$image_id));
		echo 'OK';exit;
	}
	public function deleteImage()
	{
		$sel='ERR';
		$image_id=$this->input->post('image_id',true);
		$package_id=$this->input->post('package_id',true);
		$image_name=$this->input->post('image_name',true);
		$image=$this->master_model->getRecords('tbl_boat_image',array('boat_image_id'=>$image_id));
		if($image[0]['show_front']!='yes')
		{
			$this->master_model->deleteRecord('tbl_boat_image','boat_image_id',$image_id);
			@unlink('uploads/boats/'.$image_name);
			$sel='OK';
		}
		else
		{
			$sel='SET';
		}
		echo $sel;exit;
	}
	public function wishlist($user_id='',$boat_id='')
	{
		$arrayInsert=array('user_id'=>$user_id,'package_id'=>$boat_id);
		$getRes=$this->master_model->getRecords('tbl_wishlist_master',$arrayInsert);
		if(count($getRes)==0)
		{
			if($this->master_model->insertRecord('tbl_wishlist_master',$arrayInsert))
			{echo 'ok';}
			else
			{echo 'error';}
		}
		else
		{
			echo 'already';
		}
	}
	public function rating()
    {
		    $review_title=$this->input->post('review_title');
			$review_desc=$this->input->post('review_desc');
			$fk_form_id=base64_decode($this->input->post('fk_form_id'));
			$form_type=$this->input->post('form_type');
			$rating=$this->input->post('rating');
			$getResponse=$this->master_model->getRecords('tbl_rating_master',array('fk_user_id'=>$this->session->userdata('userID'),'fk_form_id'=>$fk_form_id));
			if(count($getResponse)==0)
			{
				$insert_array=array('fk_user_id'=>$this->session->userdata('userID'),
									'fk_form_id'=>$fk_form_id,
									'form_type'=>$form_type,
									'rating'=>$rating,
									'review_desc'=>$review_desc,
									'review_title'=>$review_title);
				$complete=$this->master_model->insertRecord('tbl_rating_master',$insert_array);
				if($complete)
				{
				 // $this->session->set_flashdata('success','Thanks for your ratings.');/*,Admin will check it very soon.*/
				  echo 'success';
				  exit();
				}
			}
			else
			{
			  echo 'you have already rating of this Tours !';
			  exit();
			}
	}
	public function login()
	{
	  $data['error']='';

			   $email=$this->input->post('email',true);
			   $password=$this->input->post('password',true);
			   $valid=$this->master_model->getRecords('tbl_user_master',array('email'=>$email,'password'=>$password));
				if(count($valid)>0)
				{
					if($valid[0]['varifyStatus']!='0')
					{
						if($valid[0]['status']!='0')
						{
							$user_data=array('userID'=>$valid[0]['userID'],
											 'userEmail'=>$valid[0]['email'],
									 		 'userFName'=>$valid[0]['firstName'],
									         'userLName'=>$valid[0]['lastName'],
									         'profile_pic'=>$valid[0]['userImage'],
									         'userType'=>'customer'
									 );
							$this->session->set_userdata($user_data);
							echo 'success';
							exit();
						}
						else
						{
							echo 'Sorry! you are block by admin.';
							exit();
						}
					}
					else
					{
						echo 'Sorry! You are not verify. Please verify your email id.';
						exit();
					}
				}
				else
				{
					echo 'Invalid credentials.';
					exit();
				}
	}
	public function removeaccom()
	{
		  $table_name=$filed_id='';
		  $common_id=$this->input->post('common_id');
		  $type     = $this->input->post('type');
		  if($type=='coupon')
		  {
			$table_name='tbl_coupon_master';
			$filed_id='coupon_id';
		  }
		  if($type=='event')
		  {
			$table_name='tbl_event_master';
			$filed_id='event_id';
		  }
		  if($type=='transportation')
		  {
			$table_name='tbl_transportation_master';
			$filed_id='transportation_id';
		  }
		  if($type=='form')
		  {
			$table_name='tbl_three_forms';
			$filed_id='formsID';
		  }
		  if($type=='accomodations')
		  {
			 $table_name='tbl_accommodations_master';
			 $filed_id='accom_id';
		  }
		  if($table_name!='' && $filed_id!='')
		  {
			if($this->master_model->deleteRecord($table_name,$filed_id,$common_id))
			{echo 'success';}
			else
			{echo 'error';}
		  }
		  else
		  {echo 'error';}
    }
	
	public function approve($order_id)
	{
		$_POST['status']='approve';
		$_POST['transReson']=$this->input->post('textValue');
		if($this->master_model->updateRecordf('tbl_transaction_master',$_POST,array('orderID'=>$order_id)))
		{
		  echo 'success';	
		}
		else
		{
		  echo 'error';
		}
	}
	public function getCity()
	{
		$cat='<option value="">Select City</option>';
		$country=$this->input->post('country',true);
		$city=$this->master_model->getRecords('tbl_cities_master',array('country'=>$country));
		if(count($city)>0)
		{
			foreach($city as $c)
			{
			  $cat .='<option value="'.$c['city'].'">'.$c['city'].'</option>';
			}
		}
      echo $cat;exit;
	}
}
