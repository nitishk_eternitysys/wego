<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Boat extends CI_Controller {
	public function search()
	{
		$viewType=$this->uri->segment(3);
	  if($this->session->userdata('lang')=='')
	  {$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
	  $data['error']='';
		$querystring='';
		$config                	= 	array();
		$config['suffix']		=	'?'.$_SERVER['QUERY_STRING'];
		$config['first_url'] 	= 	base_url().'boat/search/'.$viewType.'?'.$_SERVER['QUERY_STRING'];
		$config['base_url']    	= 	base_url() .'boat/search/'.$viewType;
		
		if(!empty($_GET['budget'])) {
				$budget=$_GET['budget'];
				$budgetcounter=1;
				$whereParambd="";
				$whereParambd.="(";
				
				foreach($budget as $bd)
				{
					$range=explode('-',$bd);
					//print_r($range);exit;
					if($budgetcounter==1)
					{
						if($range[1]=='10000')
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					} 
					else
					{
						if($range[1]=='10000')
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					}
					$budgetcounter++;
				}
				$whereParambd.=")";
				$this->db->where($whereParambd);
			}
			
		if(!empty($_GET['type'])) {
			$type=$_GET['type'];
			$typecounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($type as $t)
			{
				if($typecounter==1)
				{$whereParam.="tbl_boat_master.boat_type_id=".$t;}
				else
				{$whereParam.=" OR tbl_boat_master.boat_type_id=".$t;}
				$typecounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['cabins'])) {
			$cabins=$_GET['cabins'];
			$cabinscounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($cabins as $c)
			{
				if($cabinscounter==1)
				{$whereParam.="tbl_boat_master.boat_cabins_id=".$c;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_cabins_id=".$c;}
				$cabinscounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['crew'])) {
			$crew=$_GET['crew'];
			$crewcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($crew as $cr)
			{
				if($crewcounter==1)
				{$whereParam.="tbl_boat_master.boat_crewtype_id=".$cr;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_crewtype_id=".$cr;}
				$crewcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['category'])) {
			$category=$_GET['category'];
			$categorycounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($category as $ca)
			{
				if($categorycounter==1)
				{$whereParam.="tbl_boat_master.boat_category_id=".$ca;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_category_id=".$ca;}
				$categorycounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['boat_location'])) 
		{
			$this->db->where('tbl_boatpackage_master.boat_location_to',trim($_GET['boat_location']));
		}
		if(!empty($_GET['boat_location_from'])) 
		{
			$this->db->where('tbl_boatpackage_master.boat_location',trim($_GET['boat_location_from']));
		}
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$count=$this->master_model->getRecordCount('tbl_boat_master',array('boat_status'=>'active'),'',array('boat_id'=>'desc'));
	  
		$config['total_rows']  	= 	$count;
		$config['per_page']    	= 	10;
		$config['num_links']   	= 	3;
		$config['uri_segment'] 	= 	3;
		
		$config['full_tag_open'] = '<ul class="pagination pagination-lg">';
		$config['full_tag_close'] = '</ul>'; 
		$config['first_link'] = 'First';
		
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		 
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		 
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		 
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		 
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		
		$querystring='?'.$_SERVER['QUERY_STRING'];
		$this->pagination->initialize($config);
		$page= 	($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		if(!empty($_GET['budget'])) {
				$budget=$_GET['budget'];
				$budgetcounter=1;
				$whereParambd="";
				$whereParambd.="(";
				
				foreach($budget as $bd)
				{
					$range=explode('-',$bd);
					//print_r($range);exit;
					if($budgetcounter==1)
					{
						if($range[1]=='10000')
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.="tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					} 
					else
					{
						if($range[1]=='10000')
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  != 'NULL'";
						}
						else
						{
							$whereParambd.=" OR tbl_boatpackage_master.adult_price  >= ".$range[0]." AND tbl_boatpackage_master.adult_price  <= ".$range[1]."";
						}
					}
					$budgetcounter++;
				}
				$whereParambd.=")";
				$this->db->where($whereParambd);
			}
		if(!empty($_GET['type'])) {
			$type=$_GET['type'];
			$typecounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($type as $t)
			{
				if($typecounter==1)
				{$whereParam.="tbl_boat_master.boat_type_id=".$t;} //'course.courseLanguage',$language
				else
				{$whereParam.=" OR tbl_boat_master.boat_type_id=".$t;}
				$typecounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['cabins'])) {
			$cabins=$_GET['cabins'];
			$cabinscounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($cabins as $c)
			{
				if($cabinscounter==1)
				{$whereParam.="tbl_boat_master.boat_cabins_id=".$c;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_cabins_id=".$c;}
				$cabinscounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['crew'])) {
			$crew=$_GET['crew'];
			$crewcounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($crew as $cr)
			{
				if($crewcounter==1)
				{$whereParam.="tbl_boat_master.boat_crewtype_id=".$cr;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_crewtype_id=".$cr;}
				$crewcounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['category'])) {
			$category=$_GET['category'];
			$categorycounter=1;
			$whereParam="";
			$whereParam.="(";
			foreach($category as $ca)
			{
				if($categorycounter==1)
				{$whereParam.="tbl_boat_master.boat_category_id=".$ca;} 
				else
				{$whereParam.=" OR tbl_boat_master.boat_category_id=".$ca;}
				$categorycounter++;
			}
			$whereParam.=")";
			$this->db->where($whereParam);
		}
		if(!empty($_GET['boat_location'])) 
		{
			$this->db->where('boat_location_to',trim($_GET['boat_location']));
		}
		if(!empty($_GET['boat_location_from'])) 
		{
			$this->db->where('boat_location',trim($_GET['boat_location_from']));
		}
		$sort=array('tbl_boat_master.boat_id'=>'desc');
		if(!empty($_GET['priceSort'])) 
		{
			$sort=array('tbl_boatpackage_master.adult_price'=>$_GET['priceSort']);
		}
		/*if(!empty($_GET['ratingSort'])) 
		{
			$sort=array('tbl_boatpackage_master.adult_price'=>$_GET['priceSort']);
		}*/
		if(!empty($_GET['boatName'])) 
		{
			$sort=array('tbl_boatpackage_master.package_title'=>$_GET['boatName']);
		}
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$data['searchData']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boat_master.boat_status'=>'active'),'',$sort,$page,$config['per_page']);
		//echo $this->db->last_query();exit;
		
		$data['boatType']=$this->master_model->getRecords('tbl_boattype_master',array('boattype_status'=>'active'));
		$data['boatCabins']=$this->master_model->getRecords('tbl_cabins_master',array('cabins_status'=>'active'));
		$data['boatCrew']=$this->master_model->getRecords('tbl_typecrew_master',array('crew_status'=>'active'));
		$data['boatCategory']=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>'0','category_status'=>'1'));
		
		$data['links']=$this->pagination->create_links();
	  
	  $data['pagetitle']='Search Boat';
	  if($viewType=='list')
	  $data['middle_content']='search-boat';
	  else
	  $data['middle_content']='search-boat-grid';
	  $this->load->view('common-file',$data);
	}
	
	public function details($package_id)
	{
		$package_id=base64_decode($package_id);
		if($this->session->userdata('lang')=='')
		{$this->session->set_userdata('lang','eng');$this->session->set_userdata('site_lang','english');}	
		$data['error']='';
		$data['pagetitle']='Boat Details';
		if(isset($_POST['bookBoat']))
		{
			$facilities='';
			if(count($_POST['facilities'])>0)
			{
				$facilities=@implode(',',$_POST['facilities']);
			}
			$user_data=array('qty'=>$_POST['qty'],
			                'child_qty'=>$_POST['child_qty'],
			                'redirectTo'=>'checkout/cart',
			                'package_id'=>$package_id,
							'facilities'=>$facilities
				            );
			$this->session->set_userdata($user_data);
			if($this->session->userdata('userID')!='' && $this->session->userdata('userType')=='customer')
			{
				redirect(base_url().'checkout/cart');
			}
			else
			{
				redirect(base_url().'home/login');
			}
		}
		$this->db->join('tbl_boat_owner','tbl_boat_owner.owner_id=tbl_boat_master.boat_owner_id');
		$this->db->join('tbl_boatpackage_master','tbl_boatpackage_master.boat_id=tbl_boat_master.boat_id');
		$data['boatDetails']=$this->master_model->getRecords('tbl_boat_master',array('tbl_boatpackage_master.package_id'=>$package_id,'tbl_boatpackage_master.status'=>'active'));
		$data['facilities']=$this->master_model->getRecords('tbl_boat_facilities',array('boat_id'=>$data['boatDetails'][0]['boat_id']));
		$data['boatImage']=$this->master_model->getRecords('tbl_boat_image',array('package_id'=>$package_id));
		$data['middle_content']='boat-details';
	  	$this->load->view('common-file',$data);
	}
	
}