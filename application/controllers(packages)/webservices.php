<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Webservices extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		echo "Welcome";
	}
	public function
	public function userRegister()
	{
		$resp['status'] ='fail';
		$resp['msg']='server error';
		$resp['loginID']='';
		
		if(isset($_POST))
		{
			$emailID = $_REQUEST['emailID'];
			$password = $_REQUEST['password'];
			$mobileNumber = $_REQUEST['mobileNumber'];
			$DLNumber = $_REQUEST['DLNumber'];
			$firmName = $_REQUEST['firmName'];
			$ownerName = $_REQUEST['ownerName'];
			$userStatus = 'active';
			$address = $_REQUEST['address'];
			
			$dlCount=$this->master_model->getRecords('tbl_login_master',array('DLNumber'=>$DLNumber));

			if(count($dlCount)==0)
			{
				$ins_array=array('emailID'=>$emailID,'password'=>md5($password),'mobileNumber'=>$mobileNumber,'DLNumber'=>$DLNumber,'firmName'=>$firmName,'ownerName'=>$ownerName,'address'=>$address,'userStatus'=>$userStatus);
								
				if($loginID=$this->master_model->insertRecord('tbl_login_master',$ins_array,TRUE))
				{
					$resp['status'] ='success';
					$resp['msg']='You are registered successfully';	
					$resp['loginID']=$loginID;
				}
			}
			else
			{
				$resp['status']='fail';
				$resp['msg']='DL number can not be duplicate';
			}
			echo str_replace('\/','/',json_encode($resp));
		}
	}
		
	
	public function userLogin()
	{
		$resp['status'] ='fail';
		$resp['msg']='server error';
		$resp['loginID']='';
		$resp['emailID']='';
		if(isset($_POST))
		{
			$DLNumber = $_REQUEST['DLNumber'];
			$password = $_REQUEST['password'];
			
			$userInfo=$this->master_model->getRecords('tbl_login_master',array('DLNumber'=>$DLNumber,'password'=>md5($password)));
			if(count($userInfo) > 0)
			{
				$resp['status'] ='success';
				$resp['msg']='You have entered correct credentials';	
				$resp['loginID']=$userInfo[0]['loginID'];
				$resp['emailID']=$userInfo[0]['emailID'];
			}
			else
			{
				$resp['status']='fail';
				$resp['msg']='Invalid Credential';
			}
			echo str_replace('\/','/',json_encode($resp));
		}
	}
	
	
	public function medicineList()
	{
		$resp['status'] ='fail';
		$resp['msg']='server error';
		$resp['data']='';
		if(isset($_POST))
		{
			$medicineList=$this->master_model->getRecords('tbl_medicine_master',array('medicineStatus'=>'active'));
			if(count($medicineList) > 0)
			{
				$resp['status'] ='success';
				$resp['msg']='Medicine list fetched successfully';	
				$resp['data']=$medicineList;
			}
			else
			{
				$resp['status']='fail';
				$resp['msg']='No medicine available';
			}
			echo str_replace('\/','/',json_encode($resp));
		}	
	}
	
	/* This is to insert into order table and send email to admin and user */
	public function confirmOrder()
	{
		$resp['status'] ='fail';
		$resp['msg']='server error';
		//$resp['data']='';
		if(isset($_POST))
		{
			$loginID=$_REQUEST['loginID']; // login ID
			$medicineList=$_REQUEST['medicineList']; // Medicine ID Comma seprated 6,7,5,4,6
			$quantityList=$_REQUEST['quantityList']; // 4,5,6,8,9
			$medicineArray=explode(',',$medicineList);
			$quantityArray=explode(',',$quantityList);
			$emailID=$_REQUEST['emailID'];
			$orderTotal=0;
			if(count($medicineArray)>0 && count($quantityArray)>0)
			{
				for($i=0;$i<count($medicineArray);$i++)
				{
					$price=$this->master_model->getRecords('tbl_medicine_master',array('medicineID'=>$medicineArray[$i]),'medicinePrice');
					$subTotal=$quantityArray[$i]*$price[0]['medicinePrice'];
					$insert_array=array('loginID'=>$loginID,'medicineID'=>$medicineArray[$i],'quantity'=>$quantityArray[$i],'subTotal'=>$subTotal,'orderDate'=>date('Y-m-d'));
					$this->master_model->insertRecord('tbl_medicine_order',$insert_array);
					$orderTotal+=$subTotal;
				}
				
				$userInfo=$this->master_model->getRecords('tbl_login_master',array('loginID'=>$loginID));
				if(count($userInfo)>0)
				{
					$ownerName=$userInfo[0]['ownerName'];
				}
				/* Email sending to user and admin */
				$admin_email='Bgaurav96@gmail.com';
				
				$info_arr=array('from'=>$admin_email,'to'=>$ownerName,'subject'=>'Medicine order notification','view'=>'order_email_to_user');
				$other_info=array('ownerName'=>$ownerName,'orderTotal'=>$orderTotal);
				$this->email_sending->sendmail($info_arr,$other_info);
				
				/* Email sending to admin */
				
				$info_arr1=array('from'=>$admin_email,'to'=>$admin_email,'subject'=>'Medicine order notification','view'=>'order_email_to_admin');
				$other_info1=array('ownerName'=>$ownerName,'orderTotal'=>$orderTotal);
				$this->email_sending->sendmail($info_arr1,$other_info1);
				
				
				$resp['status'] ='success';
				$resp['msg']='Medicine ordered successfully';
			}
		}
		echo str_replace('\/','/',json_encode($resp));
	}
	public function profileEdit()
	{
		$resp['status'] ='fail';
		$resp['msg']='server error';
		//$resp['data']='';
		if(isset($_POST))
		{
			$loginID=$_REQUEST['loginID']; // login ID
			$emailID=$_REQUEST['emailID'];
			$userInfo=$this->master_model->getRecords('tbl_login_master',array('loginID'=>$loginID));
			if(count($userInfo)!=0)
			{ 
				if($this->master_model->updateRecord('tbl_login_master',array('emailID'=>$emailID), array('loginID'=>$loginID)))
				{
			      $resp['status'] ='success';
				  $resp['msg']='Email change successfully';
				}
			}
			else
			{
			    $resp['status'] ='fail';
				$resp['msg']='something is wrong!';	
			}
		}
		echo str_replace('\/','/',json_encode($resp));
	}


}