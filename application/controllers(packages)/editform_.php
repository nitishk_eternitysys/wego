<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Editform extends CI_Controller {
	public function accomedit($accom_edit='')
	{
	   $accom_id = $this->input->post('accom_id');	
	   $accom_info=$this->master_model->getRecords('tbl_accommodations_master',array('accom_id'=>$accom_id));	
	 ?>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
        <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>

		<script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
        <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
        <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
            <p><a href="javascript:void(0);"  class="addcls" rel="accommodations_manage">Manage Accommodations</a></p>
          </div> 
        <div class="clearfix"></div>
        <div class="col-md-12">
             <div class="user-personal-info">
              <h4>Accommodations Information</h4>
              <div class="user-info-body">
            <form class="msform" id="accommodationsform" name="accommodationsform" method="post" action="<?php echo base_url().'owner/updateaccom/'.$accom_id.'' ?>"> 
            <!-- progressbar -->
            <ul id="progressbar" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Rooms &amp; beds</li>
                <li>Amenities</li>
                <li>Rules</li>
                <li>Other rules</li>
                <li>Pricing</li>
                <li>Cancellation and refund policy:</li>
                <li>Contact info:</li>
            </ul>
            <!-- fieldsets -->
            <fieldset>
            <h2 class="fs-title">Basic info</h2>
                <div class="col-md-12 col-sm-12"> 
            <input type="text" name="accom_title" placeholder="Titile" id="accom_title"  data-rule-required='true' value="<?php echo $accom_info[0]['accom_title'];  ?>"/>
                </div>
             <div class="col-md-6 col-sm-6"> 
            <select name="accom_category_id" class="form-control" id="accom_category_id">
            <option selected="selected">Select category</option>
            <?php 
			$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'1','category_status'=>'active')); 
            if(count($category_accom)>0)
            {
            foreach($category_accom as $row)
            {
				 $sel='';
				 if($accom_info[0]['accom_category_id']==$row['category_id'])
				 {
					 $sel="selected='selected'";	
				 }
            ?>
            <option value="<?php echo $row['category_id']; ?>" <?php echo $sel; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
            <?php 
              }
            } ?>
            </select>
                  </div>
              <div class="col-md-6 col-sm-6">      
            <select name="accom_subcategory_id" class="form-control" id="accom_subcategory_id">
              <option >Select subcategory</option>  
               <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$accom_info[0]['accom_category_id'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
			            if($sub['category_id']==$accom_info[0]['accom_subcategory_id'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
             </select>
                </div>
              <div class="col-md-6 col-sm-6">    
            <textarea name="accom_location" id="accom_location" class="form-control" placeholder="location"><?php echo $accom_info[0]['accom_location'];  ?></textarea>
                </div>
                 <div class="col-md-6 col-sm-6"> 
            <textarea name="accom_description" id="accom_description" class="form-control" placeholder="Description"><?php echo $accom_info[0]['accom_description'];  ?></textarea>
                </div>
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Rooms &amp; beds</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
            
             <div class="col-md-6 col-sm-6">   
                 <input type="text" onkeypress="return OnlyNumericKeys(event);" name="accom_bed_rooms" id="accom_bed_rooms" placeholder="Bed Rooms"  value="<?php echo $accom_info[0]['accom_bed_rooms'];  ?>" /></div>
                
                <div class="col-md-6 col-sm-6">   
                    <input type="text" onkeypress="return OnlyNumericKeys(event);" name="accom_beds" id="accom_beds" placeholder="Beds"  value="<?php echo $accom_info[0]['accom_beds'];  ?>" /></div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_bathrooms" id="accom_bathrooms" placeholder="Bathrooms"  value="<?php echo $accom_info[0]['accom_bathrooms'];  ?>" /></div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_swimming_pool" id="accom_swimming_pool" placeholder="swimming pool"  value="<?php echo $accom_info[0]['accom_swimming_pool'];  ?>" /></div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_play_yards" id="accom_play_yards" placeholder="play yards"  value="<?php echo $accom_info[0]['accom_play_yards'];  ?>" /></div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_balcony" id="accom_balcony" placeholder="Balcony"   value="<?php echo $accom_info[0]['accom_balcony'];  ?>"/></div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_garden" id="accom_garden" placeholder="garden"  value="<?php echo $accom_info[0]['accom_garden'];  ?>" /></div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_roof" id="accom_roof" placeholder="Roof"  value="<?php echo $accom_info[0]['accom_roof'];  ?>" /></div>
        
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Amenities</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">TV</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn active amenities">
            <input type="radio"  value="yes" <?php if($accom_info[0]['accom_tv']=='yes'){echo "checked";} ?>   name='accom_tv' id="accom_tv"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio"  value="no"  <?php if($accom_info[0]['accom_tv']=='no'){echo "checked";} ?> name='accom_tv' id="accom_tv1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_tv_yes" <?php if($accom_info[0]['accom_tv']=='no'){ ?> style="display:none;" <?php }  ?>><input type="text" name="accom_tv_number" id="accom_tv_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_tv_number']; ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Play station</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" value="yes" class="amenities"  <?php if($accom_info[0]['accom_playstation']=='yes'){echo "checked";} ?>  name='accom_playstation' id="accom_playstation"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" value="no" class="amenities" <?php if($accom_info[0]['accom_playstation']=='no'){echo "checked";} ?> name='accom_playstation' id="accom_playstation1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_playstation_yes" <?php if($accom_info[0]['accom_playstation']=='no'){ ?> style="display:none; <?php } ?>"><input type="text" name="accom_playstation_number" id="accom_playstation_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_playstation_number']; ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">WIFI</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" value="yes" <?php if($accom_info[0]['accom_wifi']=='yes'){echo "checked";} ?>  rel="number" name='accom_wifi' id="accom_wifi"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_wifi']=='no'){echo "checked";} ?> rel="number" name='accom_wifi' id="accom_wifi1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_wifi_yes" <?php if($accom_info[0]['accom_wifi']=='no'){ ?> style="display:none; <?php } ?>"><input type="text" name="accom_wifi_number" id="accom_wifi_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_wifi_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">DVD</div>
            <div class="col-xs-4 " data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" <?php if($accom_info[0]['accom_dvd']=='yes'){echo "checked";} ?> value="yes" rel="number" name='accom_dvd' id="accom_dvd"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_dvd']=='no'){echo "checked";} ?> rel="number" name='accom_dvd' id="accom_dvd1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_dvd_yes"  <?php if($accom_info[0]['accom_dvd']=='no'){echo ' style="display:none;"';} ?>><input type="text" name="accom_dvd_number" id="accom_dvd_number" class="form-control" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_dvd_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">GYM</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" <?php if($accom_info[0]['accom_gym']=='yes'){echo "checked";} ?> value="yes" name='accom_gym' id="accom_gym"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_gym']=='no'){echo "checked";} ?>  name='accom_gym' id="accom_gym1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_gym_yes" <?php if($accom_info[0]['accom_gym']=='no'){echo 'style="display:none;"';} ?>><input type="text" name="accom_gym_number" id="accom_gym_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"  value="<?php echo $accom_info[0]['accom_gym_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">kitchen</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" class="amenities" value="yes" <?php if($accom_info[0]['accom_kitchen']=='yes'){echo "checked";} ?>  name='accom_kitchen' id="accom_kitchen"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" class="amenities" value="no" <?php if($accom_info[0]['accom_kitchen']=='no'){echo "checked";} ?>  name='accom_kitchen' id="accom_kitchen1" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_kitchen_yes" <?php if($accom_info[0]['accom_kitchen']=='no'){echo 'style="display:none;"';} ?> ><input type="text" name="accom_kitchen_number" onkeypress="return OnlyNumericKeys(event);" id="accom_kitchen_number" class="form-control" value="<?php echo $accom_info[0]['accom_kitchen_number'];  ?>"></div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Washing Machine</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn amenities active">
            <input type="radio" id="accom_washing_machine" <?php if($accom_info[0]['accom_washing_machine']=='yes'){echo "checked";} ?>  value="yes" rel="machine_numbe" name='accom_washing_machine'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn amenities">
            <input type="radio" id="accom_washing_machine1" value="no" <?php if($accom_info[0]['accom_washing_machine']=='no'){echo "checked";} ?> rel="machine_numbe" name='accom_washing_machine' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            
            </div>
            <div class="col-xs-4 clsdevnumber" id="accom_washing_machine_yes" <?php if($accom_info[0]['accom_washing_machine']=='no'){echo 'style="display:none;"';} ?>><input type="text" name="accom_washing_machine_number" id="accom_washing_machine_number" onkeypress="return OnlyNumericKeys(event);" class="form-control" value="<?php echo $accom_info[0]['accom_washing_machine_number'];  ?>"></div>
            </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Check In</div>
            <div class="col-xs-6">
            <input type="text" name="accom_check_in" id="accom_check_in" placeholder="Select check in time" value="<?php echo date('Y-m-d H:i:s',strtotime($accom_info[0]['accom_check_in']));  ?>" />
            </div>
            </div>
            </div>
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Check Out</div>
            <div class="col-xs-6">
            <input type="text" name="accom_check_out" id="accom_check_out" placeholder="Select check in time" value="<?php echo date('Y-m-d H:i:s',strtotime($accom_info[0]['accom_check_out']));  ?>"  />
            </div>
            </div>
            </div> 
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Max gusts</div>
            <div class="col-xs-6">
            <input type="text" name="accom_guest_max" id="accom_guest_max" placeholder="Max guests" onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_guest_max'];  ?>" />
            </div>
            </div>
            </div> 
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Pets</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn active">
            <input type="radio" id="accom_pets" <?php if($accom_info[0]['accom_pets']=='yes'){echo "checked";} ?>  value="yes" name='accom_pets'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn">
            <input type="radio" id="accom_pets1" <?php if($accom_info[0]['accom_pets']=='no'){echo "checked";} ?> value="no"  name='accom_pets' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            </div>
            </div>
            </div> 
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Smoking</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn  active">
            <input type="radio" id="accom_smoking"  <?php if($accom_info[0]['accom_smoking']=='yes'){echo "checked";} ?> value="yes" name='accom_smoking'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn ">
            <input type="radio" id="accom_smoking1" value="no" <?php if($accom_info[0]['accom_smoking']=='no'){echo "checked";} ?>   name='accom_smoking' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            </div>
            </div>
            </div> 
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Cooking</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn  active">
            <input type="radio" id="accom_cooking" <?php if($accom_info[0]['accom_cooking']=='yes'){echo "checked";} ?>  value="yes" name='accom_cooking'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn ">
            <input type="radio" id="accom_cooking1" value="no" <?php if($accom_info[0]['accom_cooking']=='no'){echo "checked";} ?>  name='accom_cooking' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            </div>
            </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Other Rules</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
             <div class="col-md-12 col-sm-12">
            <textarea name="accom_gustes_allowed" id="accom_gustes_allowed" placeholder="Add Descripcription gustes allowed" ><?php echo $accom_info[0]['accom_gustes_allowed']; ?></textarea>
                </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Pricing- discount </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Price</div>
            <div class="col-xs-6">
            <input type="text" name="accom_price" id="accom_price" placeholder="Price"  onkeypress="return OnlyNumericKeys(event);" value="<?php echo $accom_info[0]['accom_price']; ?>" />
            </div>
            </div>
            </div> 
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Discount</div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn discount active">
            <input type="radio" id="accom_discount" value="on" <?php if($accom_info[0]['accom_discount']=='on'){echo "checked";} ?>  name='accom_discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
            </label>
            <label class="btn discount">
            <input type="radio" id="accom_discount1" value="off" <?php if($accom_info[0]['accom_discount']=='off'){echo "checked";} ?>   name='accom_discount' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
            </label>
            </div>
            <div class="col-xs-4" id="accom_discount_on"  <?php if($accom_info[0]['accom_discount']=='off'){echo 'style="display:none;"';} ?> >
            <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="accom_precentage_discount" id="accom_percentage_discount" placeholder="discount price in percantage" value="<?php echo $accom_info[0]['accom_percentage_discount'];  ?>">
            </div>
            </div>
            </div> 
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Bird discount</div>
            <div class="col-xs-6" data-toggle="buttons">
            <input type="text" name="accom_bird_discount" id="accom_bird_discount" placeholder="Bird discount" value="<?php echo $accom_info[0]['accom_bird_discount'];  ?>" /> 
            </div>	
            </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
            <div class="col-xs-10">
            <div class="col-xs-4">Cancellation and refund policy: </div>
            <div class="col-xs-4" data-toggle="buttons">
            <label class="btn cancellation active">
            <input type="radio" id="accom_cancel_policy" value="yes" <?php if($accom_info[0]['accom_cancel_policy']=='yes'){echo 'checked';} ?> name='accom_cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
            </label>
            <label class="btn cancellation">
            <input type="radio" id="accom_cancel_policy1" value="no"  <?php if($accom_info[0]['accom_cancel_policy']=='no'){echo 'checked';} ?>  name='accom_cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
            </label>
            </div>
            <div class="col-xs-4" id="accom_cancel_policy_yes" <?php if($accom_info[0]['accom_cancel_policy']=='no'){echo 'style="display:none;"';} ?> style="display:none;">
            <input type="input" class="form-control" name="accom_cancel_policy_price" id="accom_cancel_policy_price" value="<?php echo $accom_info[0]['accom_cancel_policy_price']; ?>">
            </div>
            </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset>
            <h2 class="fs-title">Contact info</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <input type="text" name="accom_user_name" id="accom_user_name" placeholder="Name"  value="<?php echo $accom_info[0]['accom_user_name'];  ?>" />
            <input type="text" name="accom_position" id="accom_position" placeholder="position"  value="<?php echo $accom_info[0]['accom_position'];  ?>"  />
            <input type="text" name="accom_email"  id="accom_email" placeholder="Email" value="<?php echo $accom_info[0]['accom_email'];  ?>" />
            <input type="text" name="accom_mobile"  id="accom_mobile" placeholder="mobile number" value="<?php echo $accom_info[0]['accom_mobile'];  ?>" />
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="accommodations" name="accommodationssubmit" class="submit action-button" value="Submit" />
            </fieldset>
            </form> 
            </div>
            </div>
         </div>
		<script type="text/javascript">
       jQuery(document).ready(function(e) {
                 jQuery('#accom_check_in').datetimepicker({
                      format:'Y-m-d H:i:s',
                      onShow:function( ct ){
                       this.setOptions({
                        maxDate:jQuery('#accom_check_out').val()?jQuery('#accom_check_out').val():false
                       })
                      },
                 
                 });
                jQuery('#accom_check_out').datetimepicker({
                      format:'Y-m-d H:i:s',
                      onShow:function( ct ){
                       this.setOptions({
                        minDate:jQuery('#accom_check_in').val()?jQuery('#accom_check_in').val():false
                       })
                      },
                });
        });
		</script>
    <?php
	}
	public function addaccom()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
        <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="accommodations_manage">Manage Accommodations</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Accommodations Information</h4>
          <div class="user-info-body">
        <form class="msform" id="accommodationsform" name="accommodationsform" method="post"> 
        <!-- progressbar -->
        <ul id="progressbar" class="progressbar">
        <li class="active ">Basic info</li>
        <li>Rooms &amp; beds</li>
        <li>Amenities</li>
        <li>Rules</li>
        <li>Other rules</li>
        <li>Pricing</li>
        <li>Cancellation and refund policy:</li>
        <li>Contact info:</li>
        </ul>
        <!-- fieldsets -->
        <fieldset class="progressbar">
        <h2 class="fs-title">Basic info</h2>
        <div class="col-md-12 col-sm-12"> 
        <input type="text" name="accom_title" placeholder="Titile" id="accom_title"  data-rule-required='true'/>
        </div>
        <div class="col-md-6 col-sm-6"> 
        <select name="accom_category_id" class="form-control" id="accom_category_id">
        <option selected="selected">Select category</option>
        <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'1','category_status'=>'active')); 
        if(count($category_accom)>0)
        {
        foreach($category_accom as $row)
        {
        ?>
        <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
        <?php 
        }
        } ?>
        </select>
            </div>
        <div class="col-md-6 col-sm-6">    
        <select name="accom_subcategory_id" class="form-control" id="accom_subcategory_id">
        <option selected="selected">Select subcategory</option>
        </select>
        </div>
        <div class="col-md-6 col-sm-6"> 
        <textarea name="accom_location" id="accom_location" class="form-control" placeholder="location"></textarea>
        </div>
        <div class="col-md-6 col-sm-6"> 
        <textarea name="accom_description" id="accom_description" class="form-control" placeholder="Description"></textarea>
        </div>
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Rooms &amp; beds</h2>
        <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
        <div class="col-md-6 col-sm-6">  <input type="text" onkeypress="return OnlyNumericKeys(event);"  name="accom_bed_rooms" id="accom_bed_rooms" placeholder="Bed Rooms" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_beds" id="accom_beds" placeholder="Beds" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_bathrooms" id="accom_bathrooms" placeholder="Bathrooms" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_swimming_pool" id="accom_swimming_pool" placeholder="swimming pool" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_play_yards" id="accom_play_yards" placeholder="play yards" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_balcony" id="accom_balcony" placeholder="Balcony" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_garden" id="accom_garden" placeholder="garden" /></div>
        <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="accom_roof" id="accom_roof" placeholder="Roof" /></div>
        
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Amenities</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">TV</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn active amenities">
        <input type="radio"  value="yes"  name='accom_tv' id="accom_tv"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio"  value="no"  name='accom_tv' id="accom_tv1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_tv_yes" style="display:none;"><input type="text" name="accom_tv_number" id="accom_tv_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Play station</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" value="yes" class="amenities"  name='accom_playstation' id="accom_playstation"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" value="no" class="amenities"  name='accom_playstation' id="accom_playstation1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_playstation_yes" style="display:none;"><input type="text" name="accom_playstation_number" id="accom_playstation_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">WIFI</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes" rel="number" name='accom_wifi' id="accom_wifi"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no" rel="number" name='accom_wifi' id="accom_wifi1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_wifi_yes" style="display:none;"><input type="text" name="accom_wifi_number" id="accom_wifi_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">DVD</div>
        <div class="col-xs-4 " data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes" rel="number" name='accom_dvd' id="accom_dvd"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no" rel="number" name='accom_dvd' id="accom_dvd1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_dvd_yes" style="display:none;"><input type="text" name="accom_dvd_number" id="accom_dvd_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">GYM</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes" name='accom_gym' id="accom_gym"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no"  name='accom_gym' id="accom_gym1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_gym_yes" style="display:none;"><input type="text" name="accom_gym_number" id="accom_gym_number" class="form-control" onkeypress="return OnlyNumericKeys(event);"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">kitchen</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" class="amenities" value="yes"  name='accom_kitchen' id="accom_kitchen"><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" class="amenities" value="no"  name='accom_kitchen' id="accom_kitchen1" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_kitchen_yes" style="display:none;"><input type="text" name="accom_kitchen_number" onkeypress="return OnlyNumericKeys(event);" id="accom_kitchen_number" class="form-control"></div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Washing Machine</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn amenities active">
        <input type="radio" id="accom_washing_machine"  value="yes" rel="machine_numbe" name='accom_washing_machine'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn amenities">
        <input type="radio" id="accom_washing_machine1" value="no" rel="machine_numbe" name='accom_washing_machine' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        
        </div>
        <div class="col-xs-4 clsdevnumber" id="accom_washing_machine_yes" style="display:none;"><input type="text" name="accom_washing_machine_number" id="accom_washing_machine_number" onkeypress="return OnlyNumericKeys(event);" class="form-control"></div>
        </div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Rules</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Check In</div>
        <div class="col-xs-6">
        <input type="text" name="accom_check_in" id="accom_check_in" placeholder="Select check in time" />
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Check Out</div>
        <div class="col-xs-6">
        <input type="text" name="accom_check_out" id="accom_check_out" placeholder="Select check in time" />
        </div>
        </div>
        </div> 
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Max gusts</div>
        <div class="col-xs-6">
        <input type="text" name="accom_guest_max" id="accom_guest_max" placeholder="Max guests" onkeypress="return OnlyNumericKeys(event);" />
        </div>
        </div>
        </div> 
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Pets</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn active">
        <input type="radio" id="accom_pets"  value="yes" name='accom_pets'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn">
        <input type="radio" id="accom_pets1" value="no"  name='accom_pets' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        </div>
        </div>
        </div> 
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Smoking</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn  active">
        <input type="radio" id="accom_smoking"  value="yes" name='accom_smoking'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn ">
        <input type="radio" id="accom_smoking1" value="no"  name='accom_smoking' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        </div>
        </div>
        </div> 
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Cooking</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn  active">
        <input type="radio" id="accom_cooking"  value="yes" name='accom_cooking'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn ">
        <input type="radio" id="accom_cooking1" value="no"  name='accom_cooking' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        </div>
        </div>
        </div> 
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Other Rules</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="col-md-12 col-sm-12"><textarea name="accom_gustes_allowed" id="accom_gustes_allowed" placeholder="Add Descripcription gustes allowed" ></textarea></div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Pricing- discount </h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Price</div>
        <div class="col-xs-6">
        <input type="text" name="accom_price" id="accom_price" placeholder="Price"  onkeypress="return OnlyNumericKeys(event);" />
        </div>
        </div>
        </div> 
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Discount</div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn discount active">
        <input type="radio" id="accom_discount" value="on" name='accom_discount'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> On</span>
        </label>
        <label class="btn discount">
        <input type="radio" id="accom_discount1" value="off"  name='accom_discount' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Off</span>
        </label>
        </div>
        <div class="col-xs-4" id="accom_discount_on" style="display:none;">
        <input type="text"  onkeypress="return OnlyNumericKeys(event);" class="form-control" name="accom_precentage_discount" id="accom_precentage_discount" value="" placeholder="discount price in percantage">
        </div>
        </div>
        </div> 
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Bird discount</div>
        <div class="col-xs-6" data-toggle="buttons">
        <input type="text" name="accom_bird_discount" id="accom_bird_discount" placeholder="Bird discount" /> 
        </div>	
        </div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Cancellation and refund policy:</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
        <div class="row">
        <div class="col-xs-10">
        <div class="col-xs-4">Cancellation and refund policy: </div>
        <div class="col-xs-4" data-toggle="buttons">
        <label class="btn cancellation active">
        <input type="radio" id="accom_cancel_policy" value="yes" name='accom_cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
        </label>
        <label class="btn cancellation">
        <input type="radio" id="accom_cancel_policy1" value="no"  name='accom_cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
        </label>
        </div>
        <div class="col-xs-4" id="accom_cancel_policy_yes" style="display:none;">
        <input type="input" class="form-control" name="accom_cancel_policy_price" id="accom_cancel_policy_price" value="">
        </div>
        </div>
        </div>
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset class="progressbar">
        <h2 class="fs-title">Contact info</h2>
        <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_user_name" id="accom_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_position" id="accom_position" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_email"  id="accom_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="accom_mobile"  id="accom_mobile" placeholder="mobile number" /></div>
            <div class="col-md-6 col-sm-6"><input type="button" name="previous" class="previous action-button" value="Previous" /></div>
            <div class="col-md-6 col-sm-6"><input type="submit" id="accommodations" name="accommodationssubmit" class="submit action-button" value="Submit" /></div>
        </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#accom_check_in').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#accom_check_out').val()?jQuery('#accom_check_out').val():false
			   })
			  },
		 
		 });
		jQuery('#accom_check_out').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#accom_check_in').val()?jQuery('#accom_check_in').val():false
			   })
			  },
		});
});
</script>
      <?php	
	}
	public function addcoupon()
	{
      ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
        <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="coupon_manage">Manage Coupon</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Coupon</h4>
          <div class="user-info-body">
        <form class="msform" id="couponform" name="couponform" method="post"> 
        <!-- progressbar -->
            <ul id="progressbarcoupon" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Rules </li>
                <li>Pricing </li>
                <!--<li>Chargeable /free items </li>-->
                <li>Cancellation and refund policy:</li>
                <li>Contact info:</li>
            </ul>
        <!-- fieldsets -->
        <fieldset id="coupon1" class="progressbarcoupon">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-12 col-sm-12"> <input type="text" name="coupon_title" placeholder="Titile" id="coupon_title"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6">
            <select name="coupon_category_id" class="form-control" id="coupon_category_id">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'7','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="coupon_subcategory_id" class="form-control" id="subcategory_id">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"> <textarea name="coupon_location" id="coupon_location" class="form-control" placeholder="location"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="coupon_address" id="coupon_address" class="form-control" placeholder="Address"></textarea></div>
            <div class="col-md-12 col-sm-12"> <textarea name="coupon_description" id="coupon_description" class="form-control" placeholder="Description"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon2"  class="progressbarcoupon">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
           <div class="col-md-12 col-sm-12">
               <select name="gender" class="form-control" id="coupon_gender">
               <option selected="selected">none</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">  <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="age" id="age" placeholder="Age" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="weight" id="weight" placeholder="Weight" /></div>
            <div class="col-md-6 col-sm-6">  <textarea name="coupon_what_allowed" id="coupon_what_allowed" class="form-control" placeholder="what allowed"></textarea></div>
            <div class="col-md-6 col-sm-6">  <textarea name="coupon_not_allowed" id="coupon_not_allowed" class="form-control" placeholder="Not Allowed"></textarea></div>
            <div class="col-md-12 col-sm-12"> <textarea name="additiona_info" id="additiona_info" class="form-control" placeholder="Description"></textarea></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon3"  class="progressbarcoupon">
        <h2 class="fs-title">Pricing </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="price" id="price" placeholder="Price" />
                </div>
               </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Unit</div>
                <div class="col-xs-6">
                   <select name="price_unit" class="form-control" id="price_unit">
                       <option selected="selected">none</option>
                       <?php for($i=0;$i<20;$i++){ ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php } ?>
                   </select>
                </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset  id="coupon4" class="progressbarcoupon">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn couponcancel active">
                        <input type="radio" id="cancel_policy" value="yes" name='cancel_policy'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel">
                        <input type="radio" id="cancel_policy" value="no"  name='cancel_policy' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="cancel_policy_yes" style="display:none;">
                      <input type="input" class="form-control" name="cancel_policy_price" id="cancel_policy_price" value="">
                    </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="coupon5" class="progressbarcoupon">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_user_name" id="coupon_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_person_postion" id="coupon_person_postion" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_email"  id="coupon_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="coupon_mobile"  id="coupon_mobile" placeholder="mobile number" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="couponform" name="couponsubmit" class="submit action-button" value="Submit" />
          </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <?php	
	
	}
	public function couponedit()
	{
		echo 'Coupon edit';
	}
	public function addatours()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>    
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="tours_manage">Manage Tours</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Tours</h4>
          <div class="user-info-body">
        <form class="msform" id="toursform" name="toursform" method="post" enctype="multipart/form-data"> 
        <!-- progressbar -->
            <ul id="progressbartours" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Contact info:</li>
                <li>Pricing </li>
                <li>Rules </li>
                <li>Cancellation and refund policy:</li>
                <li>Schedule</li>
                <!--<li>Chargeable /free items </li>-->
            </ul>
        <!-- fieldsets -->
        <fieldset id="tours1" class="progressbartours">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_tours"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_image" placeholder="Titile" id="form_image_tours" /></div>
            <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_tours">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'2','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_tours">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_tours" value="" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_tours" class="form-control" placeholder="Description"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_tours" class="form-control" placeholder="Why you"></textarea></div>
            <div class="col-md-12 col-sm-12">  <textarea name="form_address" id="form_address_tours" class="form-control" placeholder="Address"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours2"  class="progressbartours">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_tours" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_tours" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_tours" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_tours" placeholder="mobile number" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
           </fieldset>
        <fieldset  id="tours3" class="progressbartours">
        <h2 class="fs-title">Price </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_price" id="form_price_tours" placeholder="Price" />
                </div>
               </div>
            </div>
            <?php /*?><div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Unit</div>
                <div class="col-xs-6">
                   <select name="price_unit" class="form-control" id="price_unit">
                       <option selected="selected">none</option>
                       <?php for($i=0;$i<20;$i++){ ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php } ?>
                   </select>
                </div>
               </div>
            </div><?php */?>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_price" id="form_child_price_tours" placeholder="Children price" />
                </div>
               </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children Age</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_age" id="form_child_age_tours" placeholder="Children Age" />
                </div>
               </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Group price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_grp_price" id="form_grp_price_tours" placeholder="Group price" />
                </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours4" class="progressbartours">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
            <div class="col-md-6 col-sm-6">
            <select name="form_gender" class="form-control" id="form_gender_tours">
               <option selected="selected">none</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"><input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_tours" placeholder="Age" /></div>
            <div class="col-md-12 col-sm-12"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_tours" placeholder="Weight" /></div>
            <div class="col-md-6 col-sm-6"> <textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_additional_info" id="form_additional_info_tours" class="form-control" placeholder="Additional info"></textarea></div>
            <div class="col-md-12 col-sm-12"> <input type="text" name="form_location" id="form_location_tours" placeholder="Location" /></div>
            <div class="col-md-12 col-sm-12"> <div id="gmap_tours" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours5"  class="progressbartours">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenities active">
                        <input type="radio" id="form_refundable_tours" value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel">
                        <input type="radio" id="form_refundable_tours" value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="form_refundable_yes" style="display:none;">
                      <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_tours" value="" placeholder="% chargeable if cancelled before ">
                      <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_tours" value="" placeholder="Nonrefundable if cancelled before ">
                    </div>
                   
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours6" class="progressbartours">
        <h2 class="fs-title">Schedule</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn active">
                        <input type="radio" id="form_availalbe_for_tours" rel="tours" value="daily" name="form_availalbe_for" checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn">
                        <input type="radio" id="form_availalbe_for_tours" rel="tours" value="weekly"  name="form_availalbe_for" ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_tours">
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_tours" placeholder="Select available time" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_tours"  placeholder="Select available time" />
                 </div>
                </div>
            </div> 
            </div>
            <div id="weekly_tours" style="display:none;">
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Days</div>
                  <div class="col-xs-6" data-toggle="buttons">
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
				   ?>
                   <label class="btn">
                    <input type="checkbox" name="form_available_day[]" id="form_available_day_tours_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                    <input type="text" name="form_availalbe_from_time" id="form_availalbe_from_time_tours" class="form_availalbe_from_time_tours"  placeholder="Select available time" />
                    <input class="form_availalbe_to_time_tours" type="text" name="form_availalbe_to_time" id="form_availalbe_to_time_tours"  placeholder="Select available time" />
                    <?php }  } ?>
                  </div>
                </div>
            </div>
            
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_tours" class="form-control" placeholder="Departure Point"></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_tours" class="form-control" placeholder="Address"></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_tours"  placeholder="Departure Time" />
                 </div>
                </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_tours" name="tourssubmit" class="submit action-button" value="Submit" />
        </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_tours').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_tours').val()?jQuery('#form_availalbe_to_date_tours').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_tours').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_tours').val()?jQuery('#form_available_from_date_tours').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_tours').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_tours').val()?jQuery('.form_availalbe_to_time_tours').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_tours').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_tours').val()?jQuery('.form_available_from_time_tours').val():false
			   })
			  },
		});
});
		var map;
		var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_tours"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			
                // show in input box
                document.getElementById("form_location_tours").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);
            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
        </div>
        </div>
      </div>
      <?php	
	}
	public function toursedit()
	{
		$formsID = $this->input->post('accom_id');	
	   $forms_info=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
		
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>    
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
      <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="tours_manage">Manage Tours</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
             <div class="user-waitt"><div class="user-loder"><img src="<?php echo base_url(); ?>front/images/loader1.gif" alt="Loader" /> </div></div>
          <h4>Tours</h4>
          <div class="user-info-body">
        <form class="msform" id="toursform" name="toursform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'owner/updatetours/'.$formsID.'' ?>"> 
        <!-- progressbar -->
            <ul id="progressbartours" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Contact info:</li>
                <li>Pricing </li>
                <li>Rules </li>
                <li>Cancellation and refund policy:</li>
                <li>Schedule</li>
                <!--<li>Chargeable /free items </li>-->
            </ul>
        <!-- fieldsets -->
        
        <fieldset id="tours1" class="progressbartours">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_tours"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title']; ?>"/></div>
            <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_image" placeholder="Titile" id="form_image_tours"  /></div>
            <div class="col-md-12 col-sm-12">  <input type="hidden" name="old_img" value="<?php echo $forms_info[0]['form_image']; ?>" /></div>
            <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_tours">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'2','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_tours">
               <option selected="selected">Select subcategory</option>
                <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$forms_info[0]['subcategoryID'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
			            if($sub['category_id']==$forms_info[0]['subcategoryID'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_tours" value="<?php echo $forms_info[0]['form_duration']; ?>" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_tours" class="form-control" placeholder="Description"><?php echo $forms_info[0]['form_desc']; ?></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_tours" class="form-control" placeholder="Why you"><?php echo $forms_info[0]['form_why_you']; ?></textarea></div>
            <div class="col-md-12 col-sm-12"> <textarea name="form_address" id="form_address_tours" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_address']; ?></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours2"  class="progressbartours">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="form_name" id="form_name_tours" placeholder="Name" value="<?php echo $forms_info[0]['form_name']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_tours" placeholder="position" value="<?php echo $forms_info[0]['form_position']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_tours" placeholder="Email" value="<?php echo $forms_info[0]['form_email']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_mobile"  id="form_mobile_tours" placeholder="mobile number" value="<?php echo $forms_info[0]['form_mobile']; ?>" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
          </fieldset>
        <fieldset  id="tours3" class="progressbartours">
        <h2 class="fs-title">Price </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_price" id="form_price_tours" placeholder="Price" value="<?php echo $forms_info[0]['form_price']; ?>" />
                </div>
              </div>
            </div>
            <?php /*?><div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Unit</div>
                <div class="col-xs-6">
                   <select name="price_unit" class="form-control" id="price_unit">
                       <option selected="selected">none</option>
                       <?php for($i=0;$i<20;$i++){ ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php } ?>
                   </select>
                </div>
               </div>
            </div><?php */?>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_price" id="form_child_price_tours" placeholder="Children price" value="<?php echo $forms_info[0]['form_child_price']; ?>" />
                </div>
              </div>
            </div>
          <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children Age</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_age" id="form_child_age_tours" placeholder="Children Age" value="<?php echo $forms_info[0]['form_child_age']; ?>" />
                </div>
            </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Group price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_grp_price" id="form_grp_price_tours" placeholder="Group price" value="<?php echo $forms_info[0]['form_grp_price']; ?>" />
                </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours4" class="progressbartours">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
            <div class="col-md-12 col-sm-12">
            <select name="form_gender" class="form-control" id="form_gender_tours">
               <option selected="selected">none</option>
               <option value="Male" <?php if($forms_info[0]['form_gender']=='Male'){ ?> selected="selected" <?php } ?>>Male</option>
               <option value="Female" <?php if($forms_info[0]['form_gender']=='Female'){ ?> selected="selected" <?php } ?>>Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_tours" placeholder="Age" value="<?php echo $forms_info[0]['form_age']; ?>" />
            </div>
            <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_tours" placeholder="Weight" value="<?php echo $forms_info[0]['form_weight']; ?>" />
            </div>
            <div class="col-md-6 col-sm-6">
            <textarea name="form_what_allowed" id="form_what_allowed_tours" class="form-control" placeholder="What allowed"><?php echo $forms_info[0]['form_what_allowed']; ?></textarea>
            </div>
            <div class="col-md-6 col-sm-6">
            <textarea name="form_not_allowed" id="form_not_allowed_tours" class="form-control" placeholder="What not allowed"><?php echo $forms_info[0]['form_not_allowed']; ?></textarea>
            </div>
            <div class="col-md-12 col-sm-12">
            <textarea name="form_additional_info" id="form_additional_info_tours" class="form-control" placeholder="Additional info"><?php echo $forms_info[0]['form_additional_info']; ?></textarea>
            </div>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location" id="form_location_tours" placeholder="Location"vaccept="<?php echo $forms_info[0]['form_location']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12"><div id="gmap_tours" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours5"  class="progressbartours">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenities <?php if($forms_info[0]['form_refundable']=='yes'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_tours" value="yes" name='form_refundable' <?php if($forms_info[0]['form_refundable']=='yes'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel <?php if($forms_info[0]['form_refundable']=='no'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_tours" value="no"  name='form_refundable' <?php if($forms_info[0]['form_refundable']=='no'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="form_refundable_yes" <?php if($forms_info[0]['form_refundable']=='no'){ ?> style="display:none;" <?php } ?>>
                      <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_tours" value="" placeholder="% chargeable if cancelled before ">
                      <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_tours" value="" placeholder="Nonrefundable if cancelled before ">
                    </div>
                   
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="tours6" class="progressbartours">
        <h2 class="fs-title">Schedule</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-6" data-toggle="buttons">
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_tours" rel="tours" value="daily" name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_tours" rel="tours" value="weekly"  name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_tours" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> style="display:none;" <?php } ?>>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_tours" placeholder="Select available time" value="<?php echo $forms_info[0]['form_available_from_date']; ?>" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_tours"  placeholder="Select available time" value="<?php echo $forms_info[0]['form_availalbe_to_date']; ?>" />
                 </div>
                </div>
            </div> 
            </div>
            
            <div id="weekly_tours" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> style="display:none;" <?php } ?>>
            <div class="row weekly-sadul">
                <div class="col-xs-12">
                  <div class="col-xs-3">Days</div>
                    
                  <div class="col-xs-9" data-toggle="buttons">
                    <div class="row">   
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
				   ?>
                        <div class="col-xs-4">
                   <label class="btn">
                    <input type="checkbox" name="form_available_day[]" id="form_available_day_tours_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                        </div>
                        <div class="col-xs-4">
                    <input type="text" name="form_availalbe_from_time" id="form_availalbe_from_time_tours" class="form_availalbe_from_time_tours"  placeholder="Select available time" />
                        </div>
                        <div class="col-xs-4">
                            <input class="form_availalbe_to_time_tours" type="text" name="form_availalbe_to_time" id="form_availalbe_to_time_tours"  placeholder="Select available time" /></div>
                    <?php }  } ?>
                            
                      
                      </div>    
                  </div>
                    
                </div>
            </div>
            
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_tours" class="form-control" placeholder="Departure Point"><?php echo $forms_info[0]['form_departure']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_tours" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_departure_address']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_tours"  placeholder="Departure Time" value="<?php echo $forms_info[0]['form_departure_time']; ?>" />
                 </div>
                </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_tours" name="tourssubmit" class="submit action-button" value="Submit" />
        </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_tours').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_tours').val()?jQuery('#form_availalbe_to_date_tours').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_tours').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_tours').val()?jQuery('#form_available_from_date_tours').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_tours').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_tours').val()?jQuery('.form_availalbe_to_time_tours').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_tours').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_tours').val()?jQuery('.form_available_from_time_tours').val():false
			   })
			  },
		});
});
		var map;
		var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_tours"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			
                // show in input box
                document.getElementById("form_location_tours").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);

            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
        </div>
        </div>
      </div>
      <?php	
	
	}
	public function addadventure()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="adventure_manage">Manage Adventure</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Adventure</h4>
          <div class="user-info-body">
        <form class="msform" id="adventureform" name="adventureform" method="post" enctype="multipart/form-data"> 
            <!-- progressbar -->
            <ul id="progressbaradventure" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Contact info:</li>
                <li>Pricing </li>
                <li>Rules </li>
                <li>Cancellation and refund policy:</li>
                <li>Schedule</li>
            </ul>
            <!-- fieldsets -->
            <fieldset id="adventure1"  class="progressbaradventure">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_adventure"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6"><input type="file" style="padding:0px;" name="form_image" placeholder="Titile" id="form_image_adventure" /></div>
            <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_adventure">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'3','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
                </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_adventure">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_adventure" value="" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_adventure" class="form-control" placeholder="Description"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_adventure" class="form-control" placeholder="Why you"></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_adventure" class="form-control" placeholder="Address"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure2"  class="progressbaradventure">
                    <h2 class="fs-title">Contact info</h2>
                    <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_name" id="form_name_adventure" placeholder="Name" /></div>
                <div class="col-md-6 col-sm-6">  <input type="text" name="form_position" id="form_position_adventure" placeholder="position" /></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_email"  id="form_email_adventure" placeholder="Email" /></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="form_mobile"  id="form_mobile_adventure" placeholder="mobile number" /></div>
                    <input type="button" name="previous" class="previous action-button" value="Previous" />
                    <input type="button" name="next" class="next action-button" value="Next" />
               </fieldset>
            <fieldset id="adventure3" class="progressbaradventure">
            <h2 class="fs-title">Price </h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_price" id="form_price_adventure" placeholder="Price" />
                    </div>
                   </div>
                </div>
                <?php /*?><div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Unit</div>
                    <div class="col-xs-6">
                       <select name="price_unit" class="form-control" id="price_unit">
                           <option selected="selected">none</option>
                           <?php for($i=0;$i<20;$i++){ ?>
                           <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                           <?php } ?>
                       </select>
                    </div>
                   </div>
                </div><?php */?>
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Children price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_child_price" id="form_child_price_adventure" placeholder="Children price" />
                    </div>
                   </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Children Age</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_child_age" id="form_child_age_adventure" placeholder="Children Age" />
                    </div>
                   </div>
                </div>
                <div class="row">
                  <div class="col-xs-10">
                    <div class="col-xs-4">Group price</div>
                    <div class="col-xs-6">
                      <input type="text" name="form_grp_price" id="form_grp_price_adventure" placeholder="Group price" />
                    </div>
                   </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure4"  class="progressbaradventure">
                <h2 class="fs-title">Rules</h2>
                <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
                <div class="col-md-6 col-sm-6"><select name="form_gender" class="form-control" id="form_gender_adventure">
                   <option selected="selected">none</option>
                   <option value="Male">Male</option>
                   <option value="Female">Female</option>
                </select>
                </div>
                <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_adventure" placeholder="Age" /></div>
                <div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_adventure" placeholder="Weight" /></div>
                <div class="col-md-6 col-sm-6"> <textarea name="form_what_allowed" id="form_what_allowed_adventure" class="form-control" placeholder="What allowed"></textarea></div>
                <div class="col-md-6 col-sm-6"> <textarea name="form_not_allowed" id="form_not_allowed_adventure" class="form-control" placeholder="Not allowed"></textarea></div>
                <div class="col-md-12 col-sm-12"><textarea name="form_additional_info" id="form_additional_info_adventure" class="form-control" placeholder="Additional info"></textarea></div>
                <div class="col-md-12 col-sm-12"><input type="text" name="form_location" id="form_location_adventure" placeholder="Location" /></div>
                <div class="col-md-12 col-sm-12"><div id="gmap_adventure" style="width:100%;height:200px;"></div></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure5"  class="progressbaradventure">
                <h2 class="fs-title">Cancellation and refund policy:</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="row">
                   <div class="col-xs-10">
                    <div class="col-xs-4">Cancellation and refund policy: </div>
                        <div class="col-xs-4" data-toggle="buttons">
                            <label class="btn amenities active">
                            <input type="radio" id="form_refundable_adventure" value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                            </label>
                            <label class="btn couponcancel">
                            <input type="radio" id="form_refundable_adventure" value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                            </label>
                        </div>
                        <div class="col-xs-4" id="form_refundable_yes" style="display:none;">
                          <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_adventure" value=""  placeholder="% chargeable if cancelled before ">
                          <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_adventure" value="" placeholder="Nonrefundable if cancelled before ">
                        </div>
                        
                   </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="adventure6" class="progressbaradventure">
            <h2 class="fs-title">Schedule</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn active">
                        <input type="radio" id="form_availalbe_for_adventure" rel="adventure" value="daily" name='form_availalbe_for' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn">
                        <input type="radio" id="form_availalbe_for_adventure" rel="adventure" value="weekly"  name='form_availalbe_for' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_adventure">
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_adventure" placeholder="Select available time" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_adventure"  placeholder="Select available time" />
                 </div>
                </div>
            </div> 
            </div>
            <div id="weekly_adventure" style="display:none;">
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Days</div>
                  <div class="col-xs-6" data-toggle="buttons">
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
				   ?>
                   <label class="btn">
                    <input type="checkbox" name="form_available_day[]" id="form_available_day_adventure_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                    <input class="form_availalbe_from_time_adventure" type="text" name="form_availalbe_from_time" id="form_availalbe_from_time_adventure"  placeholder="Select available time" />
                    <input class="form_availalbe_to_time_adventure" type="text" name="form_availalbe_to_time" id="form_availalbe_to_time_adventure"  placeholder="Select available time" />
                    <?php }  } ?>
                  </div>
                </div>
            </div>
            
            </div>
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Departure Point</div>
                       <div class="col-xs-6">
                        <textarea name="form_departure" id="form_departure_adventure" class="form-control" placeholder="Departure Point"></textarea>
                     </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Departure Point address</div>
                       <div class="col-xs-6">
                        <textarea name="form_departure_address" id="form_departure_address_adventure" class="form-control" placeholder="Address"></textarea>
                     </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-xs-10">
                     <div class="col-xs-4">Departure Time</div>
                       <div class="col-xs-6">
                        <input type="text" name="form_departure_time" id="form_departure_time_adventure"  placeholder="Departure Time" />
                     </div>
                    </div>
                </div> 
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="submit_adventure" name="adventuresubmit" class="submit action-button" value="Submit" />
            </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_adventure').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_adventure').val()?jQuery('#form_availalbe_to_date_adventure').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_adventure').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_adventure').val()?jQuery('#form_available_from_date_adventure').val():false
			   })
			  },
		});
		
		jQuery('.form_availalbe_from_time_adventure').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_adventure').val()?jQuery('.form_availalbe_to_time_adventure').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_adventure').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_adventure').val()?jQuery('.form_available_from_time_adventure').val():false
			   })
			  },
		});
});
var map;
		var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_adventure"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			
                // show in input box
                document.getElementById("form_location_adventure").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);
            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
        </div>
        </div>
      </div>
      <?php		
	}
	public function adventureedit()
	{
		
		$formsID = $this->input->post('accom_id');	
	   $forms_info=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
		
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>    
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
      <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="adventure_manage">Manage Adventure</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Adventure</h4>
          <div class="user-info-body">
        <form class="msform" id="adventureform" name="adventureform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'owner/updatetours/'.$formsID.'' ?>"> 
        <!-- progressbar -->
            <ul id="progressbaradventure" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Contact info:</li>
                <li>Pricing </li>
                <li>Rules </li>
                <li>Cancellation and refund policy:</li>
                <li>Schedule</li>
                <!--<li>Chargeable /free items </li>-->
            </ul>
        <!-- fieldsets -->
        
        <fieldset id="adventure1" class="progressbaradventure">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_adventure"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title']; ?>"/></div>
                <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_image" placeholder="Titile" id="form_image_adventure"  /></div>
                <div class="col-md-6 col-sm-6"><input type="hidden" name="old_img" value="<?php echo $forms_info[0]['form_image']; ?>" /></div>
                <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_adventure">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'2','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_adventure">
               <option selected="selected">Select subcategory</option>
                <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$forms_info[0]['subcategoryID'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
			            if($sub['category_id']==$forms_info[0]['subcategoryID'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_duration" id="form_duration_adventure" value="<?php echo $forms_info[0]['form_duration']; ?>" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_adventure" class="form-control" placeholder="Description"><?php echo $forms_info[0]['form_desc']; ?></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_adventure" class="form-control" placeholder="Why you"><?php echo $forms_info[0]['form_why_you']; ?></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_adventure" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_address']; ?></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="adventure2"  class="progressbaradventure">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="form_name" id="form_name_adventure" placeholder="Name" value="<?php echo $forms_info[0]['form_name']; ?>" /></div>
            <div class="col-md-6 col-sm-6"> <input type="text" name="form_position" id="form_position_adventure" placeholder="position" value="<?php echo $forms_info[0]['form_position']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_adventure" placeholder="Email" value="<?php echo $forms_info[0]['form_email']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_mobile"  id="form_mobile_adventure" placeholder="mobile number" value="<?php echo $forms_info[0]['form_mobile']; ?>" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
          </fieldset>
        <fieldset  id="adventure3" class="progressbaradventure">
        <h2 class="fs-title">Price </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_price" id="form_price_adventure" placeholder="Price" value="<?php echo $forms_info[0]['form_price']; ?>" />
                </div>
              </div>
            </div>
            <?php /*?><div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Unit</div>
                <div class="col-xs-6">
                   <select name="price_unit" class="form-control" id="price_unit">
                       <option selected="selected">none</option>
                       <?php for($i=0;$i<20;$i++){ ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php } ?>
                   </select>
                </div>
               </div>
            </div><?php */?>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_price" id="form_child_price_adventure" placeholder="Children price" value="<?php echo $forms_info[0]['form_child_price']; ?>" />
                </div>
              </div>
            </div>
          <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children Age</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_age" id="form_child_age_adventure" placeholder="Children Age" value="<?php echo $forms_info[0]['form_child_age']; ?>" />
                </div>
            </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Group price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_grp_price" id="form_grp_price_adventure" placeholder="Group price" value="<?php echo $forms_info[0]['form_grp_price']; ?>" />
                </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="adventure4" class="progressbaradventure">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
            <div class="col-md-12 col-sm-12">
            <select name="form_gender" class="form-control" id="form_gender_adventure">
               <option selected="selected">none</option>
               <option value="Male" <?php if($forms_info[0]['form_gender']=='Male'){ ?> selected="selected" <?php } ?>>Male</option>
               <option value="Female" <?php if($forms_info[0]['form_gender']=='Female'){ ?> selected="selected" <?php } ?>>Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_adventure" placeholder="Age" value="<?php echo $forms_info[0]['form_age']; ?>" />
            </div>
            <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_adventure" placeholder="Weight" value="<?php echo $forms_info[0]['form_weight']; ?>" />
            </div>
            <div class="col-md-6 col-sm-6">
            <textarea name="form_what_allowed" id="form_what_allowed_adventure" class="form-control" placeholder="What allowed"><?php echo $forms_info[0]['form_what_allowed']; ?></textarea>
            </div>
            <div class="col-md-6 col-sm-6">
            <textarea name="form_not_allowed" id="form_not_allowed_adventure" class="form-control" placeholder="What not allowed"><?php echo $forms_info[0]['form_not_allowed']; ?></textarea>
            </div>
            <div class="col-md-12 col-sm-12">
            <textarea name="form_additional_info" id="form_additional_info_adventure" class="form-control" placeholder="Additional info"><?php echo $forms_info[0]['form_additional_info']; ?></textarea>
            </div>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location" id="form_location_adventure" placeholder="Location"vaccept="<?php echo $forms_info[0]['form_location']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12"><div id="gmap_adventure" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="adventure5"  class="progressbaradventure">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenities <?php if($forms_info[0]['form_refundable']=='yes'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_adventure" value="yes" name='form_refundable' <?php if($forms_info[0]['form_refundable']=='yes'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel <?php if($forms_info[0]['form_refundable']=='no'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_adventure" value="no"  name='form_refundable' <?php if($forms_info[0]['form_refundable']=='no'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="form_refundable_yes" <?php if($forms_info[0]['form_refundable']=='no'){ ?> style="display:none;" <?php } ?>>
                      <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_adventure" value="" placeholder="% chargeable if cancelled before ">
                      <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_adventure" value="" placeholder="Nonrefundable if cancelled before ">
                    </div>
                   
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="adventure6" class="progressbaradventure">
        <h2 class="fs-title">Schedule</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_adventure" rel="adventure" value="daily" name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_adventure" rel="adventure" value="weekly"  name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_adventure" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> style="display:none;" <?php } ?>>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_adventure" placeholder="Select available time" value="<?php echo $forms_info[0]['form_available_from_date']; ?>" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_adventure"  placeholder="Select available time" value="<?php echo $forms_info[0]['form_availalbe_to_date']; ?>" />
                 </div>
                </div>
            </div> 
            </div>
            
            <div id="weekly_adventure" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> style="display:none;" <?php } ?>>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Days</div>
                  <div class="col-xs-6" data-toggle="buttons">
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
				   ?>
                   <label class="btn">
                    <input type="checkbox" name="form_available_day[]" id="form_available_day_adventure_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                    <input type="text" name="form_availalbe_from_time" id="form_availalbe_from_time_adventure" class="form_availalbe_from_time_adventure"  placeholder="Select available time" />
                    <input class="form_availalbe_to_time_adventure" type="text" name="form_availalbe_to_time" id="form_availalbe_to_time_adventure"  placeholder="Select available time" />
                    <?php }  } ?>
                  </div>
                </div>
            </div>
            
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_adventure" class="form-control" placeholder="Departure Point"><?php echo $forms_info[0]['form_departure']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_adventure" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_departure_address']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_adventure"  placeholder="Departure Time" value="<?php echo $forms_info[0]['form_departure_time']; ?>" />
                 </div>
                </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_adventure" name="adventuresubmit" class="submit action-button" value="Submit" />
        </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_adventure').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_adventure').val()?jQuery('#form_availalbe_to_date_adventure').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_adventure').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_adventure').val()?jQuery('#form_available_from_date_adventure').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_adventure').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_adventure').val()?jQuery('.form_availalbe_to_time_adventure').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_adventure').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_adventure').val()?jQuery('.form_available_from_time_adventure').val():false
			   })
			  },
		});
});
		var map;
		var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_adventure"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			
                // show in input box
                document.getElementById("form_location_adventure").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);

            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
        </div>
        </div>
      </div>
      <?php	
	
	
	}
	public function addrecreations()
	{
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script> 
      <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="recreations_manage">Manage Recreation</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Recreation</h4>
          <div class="user-info-body">
        <form class="msform" id="recreationform" name="recreationform" method="post" enctype="multipart/form-data"> 
        <!-- progressbar -->
            <ul id="progressbarrecreation" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Contact info:</li>
                <li>Pricing </li>
                <li>Rules </li>
                <li>Cancellation and refund policy:</li>
                <li>Schedule</li>
            </ul>
        <!-- fieldsets -->
        <fieldset id="recreation1" class="progressbarrecreation">
              <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_recreation"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6"> <input type="file" style="padding:0px;" name="form_image" placeholder="Titile" id="form_image_recreation" /></div>
            <div class="col-md-6 col-sm-6">
             <select name="categoryID" class="form-control" id="categoryID_recreation">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'4','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_recreation">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="form_duration" id="form_duration_recreation" value="" placeholder="duration"></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_recreation" class="form-control" placeholder="Description"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_recreation" class="form-control" placeholder="Why you"></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_recreation" class="form-control" placeholder="Address"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation2"  class="progressbarrecreation">
                <h2 class="fs-title">Contact info</h2>
                <h3 class="fs-subtitle">We will never sell it</h3>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_name" id="form_name_recreation" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_recreation" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_recreation" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_mobile"  id="form_mobile_recreation" placeholder="mobile number" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
           </fieldset>
        <fieldset id="recreation3"  class="progressbarrecreation">
        <h2 class="fs-title">Price </h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_price" id="form_price_recreation" placeholder="Price" />
                </div>
               </div>
            </div>
            <?php /* ?><div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Unit</div>
                <div class="col-xs-6">
                   <select name="price_unit" class="form-control" id="price_unit">
                       <option selected="selected">none</option>
                       <?php for($i=0;$i<20;$i++){ ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php } ?>
                   </select>
                </div>
               </div>
            </div><?php */ ?>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_price" id="form_child_price_recreation" placeholder="Children price" />
                </div>
               </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children Age</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_age" id="form_child_age_recreation" placeholder="Children Age" />
                </div>
               </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Group price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_grp_price" id="form_grp_price_recreation" placeholder="Group price" />
                </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation4"  class="progressbarrecreation">
            <h2 class="fs-title">Rules</h2>
            <h3 class="fs-subtitle">Your presence on the social network</h3>
            <div class="col-md-6 col-sm-6">
            <select name="form_gender" class="form-control" id="form_gender_recreation">
               <option selected="selected">none</option>
               <option value="Male">Male</option>
               <option value="Female">Female</option>
            </select>
            </div>
            <div class="col-md-6 col-sm-6"> <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_recreation" placeholder="Age" /></div>
            <div class="col-md-12 col-sm-12"><input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_recreation" placeholder="Weight" /></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_what_allowed" id="form_what_allowed_recreation" class="form-control" placeholder="What allowed"></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_not_allowed" id="form_not_allowed_recreation" class="form-control" placeholder="Not Allowed"></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_additional_info" id="form_additional_info_recreation" class="form-control" placeholder="Additional Info"></textarea></div>
            <div class="col-md-12 col-sm-12"> <input type="text" name="form_location" id="form_location_recreation" placeholder="Location" /></div>
            <div class="col-md-12 col-sm-12"><div id="gmap_recreation" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset  id="recreation5" class="progressbarrecreation">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenities active">
                        <input type="radio" id="form_refundable_recreation" value="yes" name='form_refundable'><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel">
                        <input type="radio" id="form_refundable_recreation" value="no"  name='form_refundable' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="form_refundable_yes" style="display:none;">
                      <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_recreation" value="" placeholder="% chargeable if cancelled before ">
                       <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_recreation" value="" placeholder="Nonrefundable if cancelled before ">
                    </div>
                    
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreation6" class="progressbarrecreation">
        <h2 class="fs-title">Schedule</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn active">
                        <input type="radio" id="form_availalbe_for_recreation" rel="recreation" value="daily" name='form_availalbe_for' checked><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn">
                        <input type="radio" id="form_availalbe_for_recreation" rel="recreation" value="weekly"  name='form_availalbe_for' ><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_recreation">
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_recreation" placeholder="Select available time" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_recreation"  placeholder="Select available time" />
                 </div>
                </div>
            </div> 
            </div>
            <div id="weekly_recreation" style="display:none;">
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Days</div>
                  <div class="col-xs-6" data-toggle="buttons">
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
				   ?>
                   <label class="btn">
                    <input type="checkbox" name="form_available_day[]" id="form_available_day_recreation_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                    <input class="form_availalbe_from_time_recreation" type="text" name="form_availalbe_from_time" id="form_availalbe_from_time_recreation"  placeholder="Select available time" />
                    <input class="form_availalbe_to_time_recreation" type="text" name="form_availalbe_to_time" id="form_availalbe_to_time_recreation"  placeholder="Select available time" />
                    <?php }  } ?>
                  </div>
                </div>
            </div>
            
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_recreation" class="form-control" placeholder="Departure Point"></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_recreation" class="form-control" placeholder="Address"></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_recreation"  placeholder="Departure Time" />
                 </div>
                </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_recreation" name="recreationsubmit" class="submit action-button" value="Submit" />
        </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_recreation').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_recreation').val()?jQuery('#form_availalbe_to_date_recreation').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_recreation').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_recreation').val()?jQuery('#form_available_from_date_recreation').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_recreation').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_recreation').val()?jQuery('.form_availalbe_to_time_recreation').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_recreation').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_recreation').val()?jQuery('.form_available_from_time_recreation').val():false
			   })
			  },
		});
});
var map;
		var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_recreation"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			
                // show in input box
                document.getElementById("form_location_recreation").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);
            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
        </div>
        </div>
      </div>
      <?php	
	}
	public function recreationsedit()
	{
	  
		
		$formsID = $this->input->post('accom_id');	
	   $forms_info=$this->master_model->getRecords('tbl_three_forms',array('formsID'=>$formsID));
		
	  ?>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
      <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
      <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
      <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>    
     <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&sensor=false"></script>-->
      <div class="col-md-3 col-sm-3 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="recreations_manage">Manage Recreations</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Recreations</h4>
          <div class="user-info-body">
        <form class="msform" id="recreationsform" name="recreationsform" method="post" enctype="multipart/form-data" action="<?php echo base_url().'owner/updatetours/'.$formsID.'' ?>"> 
        <!-- progressbar -->
            <ul id="progressbarrecreations" class="progressbar">
                <li class="active ">Basic info</li>
                <li>Contact info:</li>
                <li>Pricing </li>
                <li>Rules </li>
                <li>Cancellation and refund policy:</li>
                <li>Schedule</li>
                <!--<li>Chargeable /free items </li>-->
            </ul>
        <!-- fieldsets -->
        
        <fieldset id="recreations1" class="progressbarrecreations">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_title" placeholder="Title" id="form_title_recreations"  data-rule-required='true' value="<?php echo $forms_info[0]['form_title']; ?>"/></div>
            <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="form_image" placeholder="Titile" id="form_image_recreations"  /></div>
            <div class="col-md-6 col-sm-6"><input type="hidden" name="old_img" value="<?php echo $forms_info[0]['form_image']; ?>" /></div>
            <div class="col-md-6 col-sm-6">
            <select name="categoryID" class="form-control" id="categoryID_recreations">
                <option selected="selected">Select category</option>
                <?php $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'2','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>" <?php if($forms_info[0]['categoryID']==$row['category_id']) {?> selected="selected"<?php } ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="subcategoryID" class="form-control" id="subcategoryID_recreations">
               <option selected="selected">Select subcategory</option>
                <?php 
               $subcategory_accom=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>$forms_info[0]['subcategoryID'],'category_status'=>'active')); 
               if(count($subcategory_accom)>0)
               {
				   foreach($subcategory_accom as $sub)
				   {
			            if($sub['category_id']==$forms_info[0]['subcategoryID'])
					    {
							$sel1="selected='selected'";
						}
			   ?>
                      <option value="<?php echo $sub['category_id']; ?>" <?php echo $sel1; ?>><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
               <?php
				   }
			   }
			   ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <input type="text" name="form_duration" id="form_duration_recreations" value="<?php echo $forms_info[0]['form_duration']; ?>" placeholder="duration">
            </div>
            <div class="col-md-6 col-sm-6"><textarea name="form_desc" id="form_desc_recreations" class="form-control" placeholder="Description"><?php echo $forms_info[0]['form_desc']; ?></textarea></div>
            <div class="col-md-6 col-sm-6"><textarea name="form_why_you" id="form_why_you_recreations" class="form-control" placeholder="Why you"><?php echo $forms_info[0]['form_why_you']; ?></textarea></div>
            <div class="col-md-12 col-sm-12"><textarea name="form_address" id="form_address_recreations" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_address']; ?></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations2"  class="progressbarrecreations">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="form_name" id="form_name_recreations" placeholder="Name" value="<?php echo $forms_info[0]['form_name']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_position" id="form_position_recreations" placeholder="position" value="<?php echo $forms_info[0]['form_position']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_email"  id="form_email_recreations" placeholder="Email" value="<?php echo $forms_info[0]['form_email']; ?>" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="form_mobile"  id="form_mobile_recreations" placeholder="mobile number" value="<?php echo $forms_info[0]['form_mobile']; ?>" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
          </fieldset>
        <fieldset  id="recreations3" class="progressbarrecreations">
        <h2 class="fs-title">Price </h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_price" id="form_price_recreations" placeholder="Price" value="<?php echo $forms_info[0]['form_price']; ?>" />
                </div>
              </div>
            </div>
            <?php /*?><div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Unit</div>
                <div class="col-xs-6">
                   <select name="price_unit" class="form-control" id="price_unit">
                       <option selected="selected">none</option>
                       <?php for($i=0;$i<20;$i++){ ?>
                       <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                       <?php } ?>
                   </select>
                </div>
               </div>
            </div><?php */?>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_price" id="form_child_price_recreations" placeholder="Children price" value="<?php echo $forms_info[0]['form_child_price']; ?>" />
                </div>
              </div>
            </div>
          <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Children Age</div>
                <div class="col-xs-6">
                  <input type="text" name="form_child_age" id="form_child_age_recreations" placeholder="Children Age" value="<?php echo $forms_info[0]['form_child_age']; ?>" />
                </div>
            </div>
            </div>
            <div class="row">
              <div class="col-xs-10">
                <div class="col-xs-4">Group price</div>
                <div class="col-xs-6">
                  <input type="text" name="form_grp_price" id="form_grp_price_recreations" placeholder="Group price" value="<?php echo $forms_info[0]['form_grp_price']; ?>" />
                </div>
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations4" class="progressbarrecreations">
            <h2 class="fs-title">Rules</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
             <div class="col-md-12 col-sm-12">
            <select name="form_gender" class="form-control" id="form_gender_recreations">
               <option selected="selected">none</option>
               <option value="Male" <?php if($forms_info[0]['form_gender']=='Male'){ ?> selected="selected" <?php } ?>>Male</option>
               <option value="Female" <?php if($forms_info[0]['form_gender']=='Female'){ ?> selected="selected" <?php } ?>>Female</option>
            </select>
            </div>
             <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_age" id="form_age_recreations" placeholder="Age" value="<?php echo $forms_info[0]['form_age']; ?>" />
            </div>
             <div class="col-md-6 col-sm-6">
            <input type="text" onkeypress="return OnlyNumericKeys(event);"   name="form_weight" id="form_weight_recreations" placeholder="Weight" value="<?php echo $forms_info[0]['form_weight']; ?>" />
            </div>
             <div class="col-md-6 col-sm-6">
            <textarea name="form_what_allowed" id="form_what_allowed_recreations" class="form-control" placeholder="What allowed"><?php echo $forms_info[0]['form_what_allowed']; ?></textarea>
            </div>
             <div class="col-md-6 col-sm-6">
            <textarea name="form_not_allowed" id="form_not_allowed_recreations" class="form-control" placeholder="What not allowed"><?php echo $forms_info[0]['form_not_allowed']; ?></textarea>
            </div>
             <div class="col-md-12 col-sm-12">
            <textarea name="form_additional_info" id="form_additional_info_recreations" class="form-control" placeholder="Additional info"><?php echo $forms_info[0]['form_additional_info']; ?></textarea>
            </div>
            <div class="col-md-12 col-sm-12">
            <input type="text" name="form_location" id="form_location_recreations" placeholder="Location"vaccept="<?php echo $forms_info[0]['form_location']; ?>" />
            </div>
            <div class="col-md-12 col-sm-12"><div id="gmap_recreations" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations5"  class="progressbarrecreations">
            <h2 class="fs-title">Cancellation and refund policy:</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
               <div class="col-xs-10">
                <div class="col-xs-4">Cancellation and refund policy: </div>
                    <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn amenities <?php if($forms_info[0]['form_refundable']=='yes'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_recreations" value="yes" name='form_refundable' <?php if($forms_info[0]['form_refundable']=='yes'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> yes</span>
                        </label>
                        <label class="btn couponcancel <?php if($forms_info[0]['form_refundable']=='no'){ ?>active <?php } ?>">
                        <input type="radio" id="form_refundable_recreations" value="no"  name='form_refundable' <?php if($forms_info[0]['form_refundable']=='no'){ ?>checked <?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> no</span>
                        </label>
                    </div>
                    <div class="col-xs-4" id="form_refundable_yes" <?php if($forms_info[0]['form_refundable']=='no'){ ?> style="display:none;" <?php } ?>>
                      <input type="input" class="form-control" name="form_cancel_before" id="form_cancel_before_recreations" value="" placeholder="% chargeable if cancelled before ">
                      <input type="input" class="form-control" name="form_not_cancel" id="form_not_cancel_recreations" value="" placeholder="Nonrefundable if cancelled before ">
                    </div>
                   
               </div>
            </div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="recreations6" class="progressbarrecreations">
        <h2 class="fs-title">Schedule</h2>
            <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Available For</div>
                  <div class="col-xs-4" data-toggle="buttons">
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_recreations" rel="recreations" value="daily" name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Daily</span>
                        </label>
                        <label class="btn <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?>active<?php } ?>">
                        <input type="radio" id="form_availalbe_for_recreations" rel="recreations" value="weekly"  name="form_availalbe_for" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> checked<?php } ?>><i class="fa fa-circle-o fa-2x"></i><i class="fa fa-check-circle-o fa-2x"></i><span> Weekly</span>
                        </label>
                    </div>
                </div>
            </div>
            <div id="daily_recreations" <?php if($forms_info[0]['form_availalbe_for']=='weekly'){ ?> style="display:none;" <?php } ?>>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Check In</div>
                  <div class="col-xs-6">
                    <input type="text" name="form_available_from_date" id="form_available_from_date_recreations" placeholder="Select available time" value="<?php echo $forms_info[0]['form_available_from_date']; ?>" />
                  </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Check Out</div>
                   <div class="col-xs-6">
                   <input type="text" name="form_availalbe_to_date" id="form_availalbe_to_date_recreations"  placeholder="Select available time" value="<?php echo $forms_info[0]['form_availalbe_to_date']; ?>" />
                 </div>
                </div>
            </div> 
            </div>
            
            <div id="weekly_recreations" <?php if($forms_info[0]['form_availalbe_for']=='daily'){ ?> style="display:none;" <?php } ?>>
            <div class="row">
                <div class="col-xs-10">
                  <div class="col-xs-4">Days</div>
                  <div class="col-xs-6" data-toggle="buttons">
                  <?php $days_arr=array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
				  		$cntarr=count($days_arr);
						if(count($days_arr)>0)
						{
							for($i=0;$i<$cntarr;$i++)
							{
				   ?>
                   <label class="btn">
                    <input type="checkbox" name="form_available_day[]" id="form_available_day_recreations_<?php echo $days_arr[$i]?>" value="<?php echo $days_arr[$i]?>" /> <i class="fa fa-box-o fa-2x"></i><i class="fa fa-check-box-o fa-2x"></i><span><?php echo ucfirst($days_arr[$i]);?></span></label>
                    <input type="text" name="form_availalbe_from_time" id="form_availalbe_from_time_recreations" class="form_availalbe_from_time_recreations"  placeholder="Select available time" />
                    <input class="form_availalbe_to_time_recreations" type="text" name="form_availalbe_to_time" id="form_availalbe_to_time_recreations"  placeholder="Select available time" />
                    <?php }  } ?>
                  </div>
                </div>
            </div>
            
            </div>
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure" id="form_departure_recreations" class="form-control" placeholder="Departure Point"><?php echo $forms_info[0]['form_departure']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Point address</div>
                   <div class="col-xs-6">
                    <textarea name="form_departure_address" id="form_departure_address_recreations" class="form-control" placeholder="Address"><?php echo $forms_info[0]['form_departure_address']; ?></textarea>
                 </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-xs-10">
                 <div class="col-xs-4">Departure Time</div>
                   <div class="col-xs-6">
                    <input type="text" name="form_departure_time" id="form_departure_time_recreations"  placeholder="Departure Time" value="<?php echo $forms_info[0]['form_departure_time']; ?>" />
                 </div>
                </div>
            </div> 
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="submit" id="submit_recreations" name="recreationssubmit" class="submit action-button" value="Submit" />
        </fieldset>
        </form> 
        <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#form_available_from_date_recreations').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#form_availalbe_to_date_recreations').val()?jQuery('#form_availalbe_to_date_recreations').val():false
			   })
			  },
		 
		 });
		jQuery('#form_availalbe_to_date_recreations').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#form_available_from_date_recreations').val()?jQuery('#form_available_from_date_recreations').val():false
			   })
			  },
		});
		jQuery('.form_availalbe_from_time_recreations').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('.form_availalbe_to_time_recreations').val()?jQuery('.form_availalbe_to_time_recreations').val():false
			   })
			  },
		 
		 });
		jQuery('.form_availalbe_to_time_recreations').datetimepicker({
			  formatTime:'H:i:s',
			  format:	'H:i:s',
			  datepicker: false,
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('.form_available_from_time_recreations').val()?jQuery('.form_available_from_time_recreations').val():false
			   })
			  },
		});
});
		var map;
		var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_recreations"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			
                // show in input box
                document.getElementById("form_location_recreations").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);

            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
        </div>
        </div>
      </div>
      <?php	
	
	
	
	}
	public function addevents()
	{
    ?>
     <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
     <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
     <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
     <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="events_manage">Manage Events</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Events</h4>
          <div class="user-info-body">
        <form class="msform" id="eventform" name="eventform" method="post"> 
        <!-- progressbar -->
         <ul id="progressbarevent" class="progressbar">
            <li class="active">Basic info</li>
            <li>Address  </li>
            <li>Contact info  </li>
        </ul>
        <!-- fieldsets -->
        <fieldset id="event1" class="progressbarevent">
            <h2 class="fs-title">Basic info</h2>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_title" placeholder="Titile" id="event_title"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6"><input style="padding:0px;" type="file" name="event_image" placeholder="Image" id="event_image"  data-rule-required='true'/></div>
            <div class="col-md-6 col-sm-6">
            <select name="event_category_id" class="form-control" id="event_category_id">
                <option selected="selected">Select category</option>
                <?php 
				$category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'6','category_status'=>'active')); 
                if(count($category_accom)>0)
                {
                    foreach($category_accom as $row)
                    {
                    ?>
                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                    <?php 
                    }
                } ?>
            </select>
            </div>
            <div class="col-md-6 col-sm-6">
            <select name="event_subcategory_id" class="form-control" id="event_subcategory_id">
               <option selected="selected">Select subcategory</option>
            </select>
            </div>
            <div class="col-md-12 col-sm-12"><input type="text" name="event_duration" id="event_duration" placeholder="duration"  data-rule-required='true'/></div>
            <div class="col-md-12 col-sm-12"><textarea name="event_description" id="event_description" class="form-control" placeholder="Description"></textarea></div>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event2"  class="progressbarevent">
            <h2 class="fs-title">Address</h2>
            <!--<h3 class="fs-subtitle">Your presence on the social network</h3>-->
            <div class="col-md-12 col-sm-12"> <textarea name="event_address" id="event_address" class="form-control" placeholder="Address"></textarea></div>
            <div class="col-md-12 col-sm-12"><input type="text" name="event_location" id="event_location" placeholder="Location" /></div>
            <div class="col-md-12 col-sm-12"><div id="gmap_event" style="width:100%;height:200px;"></div></div>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset id="event3" class="progressbarevent">
                <h2 class="fs-title">Contact info</h2>
                <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
            <div class="col-md-6 col-sm-6"><input type="text" name="event_user_name" id="event_user_name" placeholder="Name" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_position" id="event_position" placeholder="position" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_email"  id="event_email" placeholder="Email" /></div>
            <div class="col-md-6 col-sm-6"><input type="text" name="event_mobile"  id="event_mobile" placeholder="mobile number" /></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="submit" id="eventform" name="eventsubmit" class="submit action-button" value="Submit" />
          </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#event_duration').datetimepicker({
			 datepicker:false,
			  format:'H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				
			   })
			  },
		 
		 });
	 });
     var map;
	 var markersArray = [];
        function initialize() {
            var myLatlng = new google.maps.LatLng(23.63916,44.30566);
            var myOptions = {
                zoom:7,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            map = new google.maps.Map(document.getElementById("gmap_event"), myOptions);
            

            google.maps.event.addListener(map, "click", function(event) {
                // get lat/lon of click
                var clickLat = event.latLng.lat();
                var clickLon = event.latLng.lng();
			    // show in input box
                document.getElementById("event_location").value = clickLat.toFixed(5)+','+clickLon.toFixed(5);
                //document.getElementById("lon").value = clickLon.toFixed(5);
				clearOverlays();
                  var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(clickLat,clickLon),
                        map: map
                     });
				markersArray.push(marker);
            });
    }   
	function clearOverlays() {
	  for (var i = 0; i < markersArray.length; i++ ) {
		markersArray[i].setMap(null);
	  }
	  markersArray.length = 0;
	}
	$(document).on('click','.action-button', function(){
		 initialize();
	});
    window.onload = function () { initialize() };
</script>
    <?php
	}
	public function eventedit()
	{
	  echo 'Event edit';
	}
	public function addtransportation()
	{
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'front/datepicker';?>/jquery.datetimepicker.css"/>
     <script src="<?php echo base_url().'front/datepicker/';?>build/jquery.datetimepicker.full.js"></script>
     <script src="<?php echo base_url(); ?>js/jquery.easing.min.js" type="text/javascript"></script> 
     <script src="<?php echo base_url(); ?>js/form-step.js" type="text/javascript"></script> 
     <div class="col-md-4 col-sm-4 col-xs-6 footer-about-box" >
        <p><a href="javascript:void(0);"  class="addcls" rel="transportation_manage">Manage Transportation</a></p>
      </div> 
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="user-personal-info">
          <h4>Transportation</h4>
          <div class="user-info-body">
        <form class="msform" id="transportationform" name="transportationform" method="post"> 
        <!-- progressbar -->
            <ul id="progressbartransportation" class="progressbar">
                <li class="active">Basic info</li>
                <li>Trans Details</li>
                <li>Available time </li>
             </ul>
            <!-- fieldsets -->
            <fieldset id="transportation1" class="progressbartransportation">
                <h2 class="fs-title">Basic info</h2>
                
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_title" placeholder="Titile" id="transportation_title"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6"> <input style="padding:0px;" type="file" name="transportation_image" placeholder="Image" id="transportation_image"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6">
                <select name="transportation_category_id" class="form-control" id="transportation_category_id">
                    <option selected="selected">Select category</option>
                    <?php 
                    $category_accom=$this->master_model->getRecords('tbl_category_master',array('maincategory_id'=>'5','category_status'=>'active')); 
                    if(count($category_accom)>0)
                    {
                        foreach($category_accom as $row)
                        {
                        ?>
                        <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name_'.$this->session->userdata('lang')]; ?></option>
                        <?php 
                        }
                    } ?>
                </select>
                </div>
                <div class="col-md-6 col-sm-6">
                <select name="transportation_subcategory_id" class="form-control" id="transportation_subcategory_id">
                   <option selected="selected">Select subcategory</option>
                </select>
                </div>
                <div class="col-md-12 col-sm-12"><textarea name="transportation_description" id="transportation_description" class="form-control" placeholder="Description"></textarea></div>
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation2"  class="progressbartransportation">
                <h2 class="fs-title">Trans Details</h2>
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_car_size" placeholder="Car size" id="transportation_car_size"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_car_model" placeholder="Car model" id="transportation_car_model"  data-rule-required='true'/></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="transportation_car_brand" placeholder="Car Brand" id="transportation_car_brand"  data-rule-required='true'/></div>
               <div class="col-md-6 col-sm-6"><input type="text" name="transportation_car" placeholder="Car" id="transportation_car"  data-rule-required='true'/></div>
                <div class="col-md-12 col-sm-12"><input type="text" name="transportation_max_passanger" placeholder="max passanger" id="transportation_max_passanger"  data-rule-required='true'/></div>
                <input type="button" name="previous" class="previous action-button" value="Previous" />
                <input type="button" name="next" class="next action-button" value="Next" />
            </fieldset>
            <fieldset id="transportation3" class="progressbartransportation">
                    <h2 class="fs-title">Available time</h2>
                    <!--<h3 class="fs-subtitle">We will never sell it</h3>-->
                <div class="col-md-6 col-sm-6"><input type="text" name="transportation_available_time" id="transportation_available_time" placeholder="Available time" /></div>
                <div class="col-md-6 col-sm-6"> <input type="text" name="transportation_pickuptime" id="transportation_pickuptime" placeholder="pickuptime" /></div>
                <div class="col-md-12 col-sm-12"><input type="text" name="transportation_pickup_location"  id="transportation_pickup_location" placeholder="Pickup location" /></div>
                <div class="col-md-12 col-sm-12"> <textarea name="transportation_pickup_remarks" id="transportation_pickup_remarks" placeholder="pickup remark"></textarea></div>
                <div class="col-md-12 col-sm-12"> <input type="text" name="transportation_drop_location"  id="transportation_drop_location" placeholder="Drop location" /></div>
                <div class="col-md-12 col-sm-12"><textarea name="transportation_drop_remark" id="transportation_drop_remark" placeholder="Drop remark"></textarea></div>
                    <input type="button" name="previous" class="previous action-button" value="Previous" />
                    <input type="submit" id="transportationform" name="transportationsubmit" class="submit action-button" value="Submit" />
              </fieldset>
        </form> 
        </div>
        </div>
      </div>
      <script type="text/javascript">
	jQuery(document).ready(function(e) {
         jQuery('#transportation_available_time').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				maxDate:jQuery('#transportation_pickuptime').val()?jQuery('#transportation_pickuptime').val():false
			   })
			  },
		 });
		jQuery('#transportation_pickuptime').datetimepicker({
			  format:'Y-m-d H:i:s',
			  onShow:function( ct ){
			   this.setOptions({
				minDate:jQuery('#transportation_pickuptime').val()?jQuery('#transportation_pickuptime').val():false
			   })
			  },
		}); 
		
		});
		</script>
    <?php
	}
	public function transportationedit()
	{
	  echo 'Transportation edit';
	}
	
}