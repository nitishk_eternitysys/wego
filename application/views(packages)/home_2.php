<section>
    <div class="row full-width-search">
        <video loop="" autoplay="" muted="" poster="<?php echo base_url(); ?>front/video/poster.jpg" class="bgvid" id="bgvid">
			<source type="video/mp4" src="<?php echo base_url(); ?>front/video/video.mp4"></source>
			<source type="video/ogv" src="<?php echo base_url(); ?>front/video/video.ogv"></source>
			<source type="video/webm" src="<?php echo base_url(); ?>front/video/video.webm"></source>
		</video>
        <div class="container clear-padding">
            <div class="search-section">
                <div role="tabpanel">
                    <!-- BEGIN: CATEGORY TAB -->
                    <ul id="searchTab" role="tablist" class="nav nav-tabs search-top">
                        <li class="active text-center" role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="accommodations" rel="1" href="#accommodations">
                                <i class="fa fa-bed"></i> 
                                <span>Accommodations</span>
                            </a>
                        </li>
                        <li class="text-center" role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="tours" href="#tours" rel="2">
                                <i class="fa fa-suitcase"></i> 
                                <span>Things To do</span>
                            </a>
                        </li>
                        <li class="text-center" role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="transportation" href="#transportation" rel="5">
                                <i class="fa fa-bus"></i>
                                <span>Transportation</span>
                            </a>
                        </li>
                        <li class="text-center" role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="events" href="#events" rel="6">
                                <i class="fa fa-calendar"></i>
                                <span>Events</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END: CATEGORY TAB -->
    
                    <!-- BEGIN: TAB PANELS -->
                    <div class="tab-content newdesform">
                            <!-- BEGIN: FLIGHT SEARCH -->
                            <div id="accommodations" class="tab-pane active" role="tabpanel">
                                <form action="<?php echo base_url(); ?>search/result/accommodations" autocomplete="off">
                                    <div class="col-md-12 product-search-title">Rooms</div>
                                    <div class="clearfix"></div>
                                    <!--<div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="category" id="category1" rel="1" >
                                                <option value="">Select Category</option>
                                            </select>
                                        </div>
                                    </div>-->
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Name / Location</label>
                                        <div class="input-group">
                                            <input type="text"  placeholder="Name / Location" class="form-control" name="name" id="name">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
                                        </div>
                                    </div>
                                  <!--<div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Sub Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="subcategory" id="subcategory1" >
                                                <option value="">Select Subcategory</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Check in</label>
                                        <div class="input-group">
                                            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkin" id="checkin">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Check out</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkout" id="checkout">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Rooms</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="rooms" id="rooms" >
                                                <option value="">Select Rooms</option>
                                                <?php if(count($acco_rooms)>0){ 
														for($r=1;$r<=$acco_rooms[0]['rooms'];$r++){
												?>
                                                <option value="<?php echo $r; ?>"><?php echo $r; ?></option>
                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Members</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="members" id="members" >
                                                <option value="">Select Members</option>
                                                <?php if(count($acco_rooms)>0){ 
														for($m=1;$m<=$acco_rooms[0]['members'];$m++){
												?>
                                                <option value="<?php echo $m; ?>"><?php echo $m; ?></option>
                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 search-col-padding">
                                        <button class="search-button btn transition-effect" type="submit">Search Rooms</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <!-- END: FLIGHT SEARCH -->
                            
                            <!-- START: HOTEL SEARCH -->
                            <div id="tours" class="tab-pane" role="tabpanel">
                                <form action="<?php echo base_url(); ?>search/result/thingstodo" autocomplete="off" method="get">
                                    <div class="col-md-12 product-search-title">Things To do </div>
                                    <!--<div class="col-md-12 search-col-padding">
                                        <label class="radio-inline">
                                            <input value="tours" rel="2" id="mainCat" name="mainCat" type="radio" checked="checked"> Tours
                                        </label>
                                        <label class="radio-inline">
                                            <input value="adventure" rel="3" id="mainCat" name="mainCat" type="radio"> Adventure
                                        </label>
                                        <label class="radio-inline">
                                            <input value="recreations" rel="4" id="mainCat" name="mainCat" type="radio"> Recreations
                                        </label>
                                    </div>-->
                                    <div class="clearfix"></div>
                                    <!--<div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="category" id="category2" rel="2" >
                                                <option value="">Select Category</option>
                                            </select>
                                        </div>
                                    </div>-->
                                  <!--  <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Sub Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="subcategory" id="subcategory2" >
                                                <option value="">Select Subcategory</option>
                                            </select>
                                        </div>
                                    </div>-->
                                    
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Location</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="E.g. London" class="form-control" name="location">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Check in</label>
                                        <div class="input-group">
                                            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkin" id="checkin_deals">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Check out</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkout" id="checkout_deals">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                        </div>
                                    </div>
                                    
                                    <!--<div class="col-md-1 col-sm-6 search-col-padding">
                                        <label>Adult</label><br>
                                        <span class="ui-spinner">
                                        <input class="form-control" value="1" name="adult" id="adult_count" aria-valuemin="1" aria-valuenow="1" autocomplete="off" role="spinbutton" readonly></span>
                                    </div>
                                    <div class="col-md-1 col-sm-6 search-col-padding">
                                        <label>Child</label><br>
                                        <span class="ui-spinner">
                                            <input type="text" class="form-control" value="1" name="child" id="child_count" readonly>
                                        </span>
                                    </div>-->
                                    
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 search-col-padding">
                                        <button class="search-button btn transition-effect" name="findTours" type="submit" value="1">Search tours</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <!-- END: HOTEL SEARCH -->
                                
                            
                            
                            <!-- START: CRUISE SEARCH -->
                            <div id="transportation" class="tab-pane" role="tabpanel">
                                <form action="<?php echo base_url(); ?>search/result/transportation" autocomplete="off">
                                    <div class="col-md-12 product-search-title">Transportation</div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="category" id="category5" rel="5" >
                                                <option value="">Select Category</option>
                                            </select>
                                        </div>
                                    </div>
                                  <!-- <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Sub Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="subcategory" id="subcategory5" >
                                                <option value="">Select Subcategory</option>
                                            </select>
                                        </div>
                                    </div>-->
                                    
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Pickup location</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="E.g. London" class="form-control" name="pickup_location">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 search-col-padding">
                                        <label>Drop location</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="E.g. London" class="form-control" name="drop_location">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 search-col-padding">
                                        <button class="search-button btn transition-effect" type="submit">Search</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <!-- END: CRUISE SEARCH -->
                            
                            <!-- START: CRUISE SEARCH -->
                            <div id="events" class="tab-pane" role="tabpanel">
                                <form action="<?php echo base_url(); ?>search/result/events" autocomplete="off">
                                    <div class="col-md-12 product-search-title">Events</div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-sm-6 search-col-padding">
                                        <label>Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="category" id="category6" rel="6" >
                                                <option value="">Select Category</option>
                                            </select>
                                        </div>
                                    </div>
                                   <!-- <div class="col-md-4 col-sm-6 search-col-padding">
                                        <label>Sub Types</label>
                                        <div class="input-group">
                                            <select class="selectpicker" name="subcategory" id="subcategory6" >
                                                <option value="">Select Subcategory</option>
                                            </select>
                                        </div>
                                    </div>-->
                                    
                                    <div class="col-md-4 col-sm-6 search-col-padding">
                                        <label>Location</label>
                                        <div class="input-group">
                                            <input type="text" placeholder="E.g. London" class="form-control" name="destination">
                                            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-md-12 search-col-padding">
                                        <button class="search-button btn transition-effect" type="submit">Search</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <!-- END: CRUISE SEARCH -->
                            
                        </div>
                        <!-- END: TAB PANE -->
                </div>
            </div>
            <!--
            <div class="col-md-4 offer-slider">
                <div id="offer1" class="owl-carousel text-right owl-theme owl-responsive-1000 owl-loaded">
                <div class="owl-stage-outer">
                  <div class="owl-stage">
                          <div class="owl-item cloned">
                                    <div class="item">
                                <h3>Romantic Paris</h3>
                                <h4>Starting From $999/Person</h4>
                                <a href="#">KNOW MORE</a>
                            </div> 
                         </div>
                     </div>
                    </div>
                    <div class="owl-stage-outer">
                    <div class="owl-stage">
                         <div class="owl-item cloned">
                          <div class="item">
                        <h3>Romantic Paris</h3>
                        <h4>Starting From $999/Person</h4>
                        <a href="#">KNOW MORE</a>
                    </div> 
                        </div>
                      </div>
                    </div>
                    <div class="owl-controls">
                            <div class="owl-nav">
                                <div class="owl-prev" style="display: none;">prev</div>
                                <div class="owl-next" style="display: none;">next</div>
                            </div>
                        </div>
                      </div>
            </div>
-->
        </div>
    </div>
    </div>
</section>

<section>
    
      <!--how it works start-->
      <div class="how-it-work-section">
         <div class="container">
            <div class="row">
                 
                     <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="work-rgt">
                           <div class="how-icon"><img src="<?php echo base_url(); ?>front/images/happy-clients.jpg" alt="img"/> </div>
                           <div class="how-head">
                              <a href="#">Happy Clients</a>
                           </div>
                           <div class="how-content">
                             It is a long established fact that a reader will be distracted by the readable content 
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="work-rgt">
                           <div class="how-icon"><img src="<?php echo base_url(); ?>front/images/amazing-tour.jpg" alt="img"/> </div>
                           <div class="how-head">
                              <a href="#">Amazing Tour</a>
                           </div>
                           <div class="how-content">
                              It is a long established fact that a reader will be distracted by the readable content
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="work-rgt">
                           <div class="how-icon"><img src="<?php echo base_url(); ?>front/images/handpicked-hotels.jpg" alt="img"/> </div>
                           <div class="how-head">
                              <a href="#">Handpicked Hotels</a>
                           </div>
                           <div class="how-content">
                             It is a long established fact that a reader will be distracted by the readable content
                           </div>
                        </div>
                     </div>
                 
                  <div class="col-sm-6 col-md-3 col-lg-3">
                        <div class="work-rgt">
                           <div class="how-icon"><img src="<?php echo base_url(); ?>front/images/best-travel-agent.jpg" alt="img"/> </div>
                           <div class="how-head">
                              <a href="#">Best Travel Agent</a>
                           </div>
                           <div class="how-content">
                           It is a long established fact that a reader will be distracted by the readable content
                           </div>
                        </div>
                     </div>
            
            </div>
         </div>
      </div>
      <!--how it works end-->    
    
</section>    

<!--
<section>
	<div class="row hot-deals">
		<div class="clear-padding">
			<div class="text-center" role="tabpanel">
				
				<ul id="hotDeal" role="tablist" class="nav nav-tabs">
					<li class="active text-center" role="presentation">
						<a data-toggle="tab" role="tab" aria-controls="tab1" href="#tab1">
							<i class="fa fa-bed"></i> 
							<span>Accommodations</span>
						</a>
					</li>
					<li class="text-center" role="presentation">
						<a data-toggle="tab" role="tab" aria-controls="tab2" href="#tab2">
							<i class="fa fa-plane"></i> 
							<span>Tours</span>
						</a>
					</li>
					<li class="text-center" role="presentation">
						<a data-toggle="tab" role="tab" aria-controls="tab3" href="#tab3">
							<i class="fa fa-suitcase"></i> 
							<span>Adventure</span>
						</a>
					</li>
					<li class="text-center" role="presentation">
						<a data-toggle="tab" role="tab" aria-controls="tab4" href="#tab4">
							<i class="fa fa-taxi"></i> 
							<span>Recreations</span>
						</a>
					</li>
				
				</ul>
				
				<div class="clearfix"></div>
				
				<div class="tab-content">
					
					<div id="tab1" class="tab-pane active fade in" role="tabpanel">
						<div class="hot-deal-grid">
                            <?php 
							$accoum=$this->master_model->getRecords('tbl_accommodations_master',array('accom_status'=>'active'));
							if(count($accoum)){
							  foreach($accoum as $row)
							  {  
							    $rating=$this->master_model->rating($row['accom_id']); 
							?>
							   <div class="col-sm-3 col-lg-2 item">
								<div class="wrapper">
                                   <?php if($row['accom_image']!=''){ ?>
								   <img alt="Cruise" src="<?php echo $this->master_model->resize($row['accom_image'],500,350,'uploads/accom/');?>">
                                   <?php }else{ ?>
                                   <img alt="Cruise" src="<?php echo base_url(); ?>front/images/tour2.jpg">
                                   <?php } ?>
                                   <div class="tour-caption">
                                     <div class="vertical-align">
                                        <h3 class="hover-it"><?php echo substr($row['accom_title'],0,10); ?></h3>
                                          <div class="rating-box">
                                            <?php echo $rating['star']; ?>
                                           </div>
                                        <h4>from <b>$<?php echo $row['accom_price']; ?></b></h4>  
                                     </div>
                                     <div class="vertical-bottom">
                                       <div class="fl">
                                        <div class="tour-info">
                                            <i aria-hidden="true" class="fa fa-calendar"></i>
                                              <?php //echo date("F j, Y",strtotime( $row['accom_check_in'])); ?>
                                              <span class="font-style-2 color-grey-4"><?php echo date("M j,",strtotime( $row['accom_check_in'])); ?> -  <?php echo date("M j, Y",strtotime( $row['accom_check_out'])); ?></span>
                                        </div>
                                       </div>	
                                        
                                     </div>
                                  </div>        
                                </div>
							</div>
                           <?php 
							  }
						   } ?> 
						 </div>
					</div>
					<div id="tab2" class="tab-pane fade" role="tabpanel">
						<div class="hot-deal-grid">
                          <?php
						    $tours=$this->master_model->getRecords('tbl_three_forms',array('form_status'=>'active','form_type'=>'Tours'));
							if(count($tours))
							{
							   foreach($tours as $row)
							   {  
							     $rating=$this->master_model->rating($row['formsID']); 
							   ?>
								<div class="col-sm-3 col-lg-2 item">
									<div class="wrapper">
                                       <?php if($row['form_image']!=''){ ?>
                                       <img alt="Cruise" src="<?php echo $this->master_model->resize($row['form_image'],560,460,'uploads/forms/');?>">
                                       <?php }else{ ?>
                                       <img alt="Cruise" src="<?php echo base_url(); ?>front/images/tour2.jpg">
                                       <?php } ?>
	                                    <div class="tour-caption">
                                             <div class="vertical-align">
                                                <h3 class="hover-it"><?php echo substr($row['form_title'],0,10); ?></h3>
                                                  <div class="rating-box">
                                                   <?php echo $rating['star']; ?>
                                                   </div>
                                                <h4>from <b>$<?php echo $row['formsID'];?></b></h4>  
                                             </div>
                                             <div class="vertical-bottom">
                                               <div class="fl">
                                                <div class="tour-info">
                                                    <i aria-hidden="true" class="fa fa-calendar"></i>
                                                      <span class="font-style-2 color-grey-4">July<strong class="color-white"> 19th</strong> to July<strong class="color-white"> 26th</strong></span>
                                                </div>
                                               </div>	
                                             </div>
                                          </div>        
									</div>
								</div>
							   <?php 
							   }
							 } 
							 ?>
                        </div>
					</div>
					<div id="tab3" class="tab-pane" role="tabpanel">
						<div class="hot-deal-grid">
							  <?php
						    $Adventure=$this->master_model->getRecords('tbl_three_forms',array('form_status'=>'active','form_type'=>'Adventure'));
							if(count($Adventure))
							{
							   foreach($Adventure as $row)
							   { 
							     $rating=$this->master_model->rating($row['formsID']);  
							   ?>
								<div class="col-sm-3 col-lg-2 item">
									<div class="wrapper">
                                       <?php if($row['form_image']!=''){ ?>
                                       <img alt="Cruise" src="<?php echo $this->master_model->resize($row['form_image'],560,460,'uploads/forms/');?>">
                                       <?php }else{ ?>
                                       <img alt="Cruise" src="<?php echo base_url(); ?>front/images/tour2.jpg">
                                       <?php } ?>
	                                    <div class="tour-caption">
                                             <div class="vertical-align">
                                                <h3 class="hover-it"><?php echo substr($row['form_title'],0,10); ?></h3>
                                                  <div class="rating-box">
                                                    <?php echo $rating['star']; ?>
                                                   </div>
                                                <h4>from <b>$<?php echo $row['form_price'];?></b></h4>  
                                             </div>
                                             <div class="vertical-bottom">
                                               <div class="fl">
                                                <div class="tour-info">
                                                    <i aria-hidden="true" class="fa fa-calendar"></i>
                                                      <span class="font-style-2 color-grey-4">July<strong class="color-white"> 19th</strong> to July<strong class="color-white"> 26th</strong></span>
                                                </div>
                                               </div>	
                                             </div>
                                          </div>        
									</div>
								</div>
							   <?php 
							   }
							 } 
							 ?>
						</div>
					</div>
					<div id="tab4" class="tab-pane" role="tabpanel">
						<div class="hot-deal-grid">
							 <?php
						    $Recreation=$this->master_model->getRecords('tbl_three_forms',array('form_status'=>'active','form_type'=>'Recreation'));
							if(count($Recreation))
							{
							   foreach($Recreation as $row)
							   {  
							     $rating=$this->master_model->rating($row['formsID']); 
							   ?>
								<div class="col-sm-3 col-lg-2 item">
									<div class="wrapper">
                                       <?php if($row['form_image']!=''){ ?>
                                       <img alt="Cruise" src="<?php echo $this->master_model->resize($row['form_image'],560,460,'uploads/forms/');?>">
                                       <?php }else{ ?>
                                       <img alt="Cruise" src="<?php echo base_url(); ?>front/images/tour2.jpg">
                                       <?php } ?>
	                                    <div class="tour-caption">
                                             <div class="vertical-align">
                                                <h3 class="hover-it"><?php echo substr($row['form_title'],0,10);?></h3>
                                                  <div class="rating-box">
                                                   <?php echo $rating['star']; ?>
                                                   </div>
                                                <h4>from <b>$<?php echo $row['form_price'];?></b></h4>  
                                             </div>
                                             <div class="vertical-bottom">
                                               <div class="fl">
                                                <div class="tour-info">
                                                    <i aria-hidden="true" class="fa fa-calendar"></i>
                                                      <span class="font-style-2 color-grey-4">July<strong class="color-white"> 19th</strong> to July<strong class="color-white"> 26th</strong></span>
                                                </div>
                                               </div>	
                                             </div>
                                          </div>        
									</div>
								</div>
							   <?php 
							   }
							 } 
							 ?>
						</div>
					</div>
                </div>
			</div>
		</div>
	</div>
</section>
-->
<!-- BEGIN: TOP DESTINATION -->
<section class="top-destination">
	
			<div class="section-title text-center">
				<h2>TOP DESTINATION</h2>
			</div>
            <?php 
			 $fetchArray =$this->master_model->getRecords('tbl_accommodations_master',array('top_destination_show'=>'yes'));
			 if(count($fetchArray))
			 {
				 foreach($fetchArray  as $row)
				 {
				 ?>
                  <div class="col-md-3 col-sm-6 tour-grid clear-padding wow slideInUp" data-wow-delay="0.1s">
                     <?php if($row['accom_image']!=''){ ?>
                       <img alt="Cruise" src="<?php echo $this->master_model->resize($row['accom_image'],560,460,'uploads/accom/');?>">
                       <?php }else{ ?>
                       <img alt="Cruise" src="<?php echo base_url(); ?>front/images/tour2.jpg">
                       <?php } ?>
                       <div class="tour-brief">
                            <div class="pull-left">
                                <h4><i class="fa fa-map-marker"></i><?php echo $row['accom_location'];  ?></h4>
                            </div>
                            <div class="title">
                                <h4><?php echo $row['accom_title'];  ?></h4>
                            </div> 
                            <div class="pull-right">
                                <h4>$<?php echo $row['accom_price'];  ?></h4>
                            </div>
                        </div>
                        <div class="tour-detail text-center">
                            <p><a href="<?php echo base_url().'details/accodetails/'.base64_encode($row['accom_id']); ?>">DETAIL</a></p>
                        </div>
                    </div>
			     <?php
				 }
			 }
			?>
             <?php 
			 $fetchForm =$this->master_model->getRecords('tbl_three_forms',array('top_destination_show'=>'yes'));
			 if(count($fetchForm))
			 {
				 foreach($fetchForm  as $row)
				 {
				  ?>
                 <div class="col-md-3 col-sm-6 tour-grid clear-padding wow slideInUp" data-wow-delay="0.1s">
					<?php if($row['form_image']!=''){ ?>
                    <img alt="Cruise" src="<?php echo $this->master_model->resize($row['form_image'],560,460,'uploads/forms/');?>">
                    <?php }else{ ?>
                    <img alt="Cruise" src="<?php echo base_url(); ?>front/images/tour2.jpg">
                    <?php } ?>
                    <div class="tour-brief">
                        <div class="pull-left"><h4><i class="fa fa-map-marker"></i><?php echo $row['form_address'];  ?></h4></div>
                        <div class="title"><h4><?php echo substr($row['form_title'],0,10); ?></h4> </div> 
                        <div class="pull-right"><h4>$<?php echo $row['form_price'];?></h4></div>
                    </div>
                    <div class="tour-detail text-center">
                        <p><a href="<?php echo base_url().'details/toursDetails/'.base64_encode($row['formsID']).'/'.$row['form_type'].'/';?>">DETAIL</a></p>
                    </div>
                  </div>
			    <?php
				 }
			 }
			?>
        <div class="clearfix"></div>
</section>
<!-- END: TOP DESTINATION -->
<!-- START: TESTIMONIAL SECTION -->
<section id="customer-testimonial">
	<div class="row">
		<div class="container">
			<div class="section-title text-center">
				<h2>TESTIMONIAL</h2>
				<h4>KIND WORDS</h4>
			</div>
			<div class="owl-carousel" id="review-customer">
              <?php 
			  $testmonial_array=$this->master_model->getRecords('tbl_testimonial_master',array('testimonial_status'=>'active'));
			  if(count($testmonial_array)){ 
			    foreach($testmonial_array as $row)
			    {
			    ?>   
				<div class="individual">
					<div class="col-md-2 col-sm-3 text-center">
						<img src="<?php echo base_url().'uploads/testimonial/'.$row['testimonial_image']; ?>" alt="cruise">
					</div>
					<div class="col-md-10 col-sm-9 customer-word">
						<p class="text-center">
							<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
							<?php echo $row['testimonial_desc_'.$this->session->userdata('lang')] ?>
						</p>
						<h5 class="text-center"><?php echo $row['testimonial_name']; ?></h5>
						<!--<h6 class="text-center">Burbank, USA</h6>-->
					</div>
				</div>
                <?php
			    }
				}
				?>
				<!--<div class="individual">
					<div class="col-md-2 col-sm-3 text-center">
						<img src="front/images/user.jpg" alt="cruise">
					</div>
					<div class="col-md-10 col-sm-9 customer-word">
						<p class="text-center">
							<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
							 Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
						</p>
						<h5 class="text-center">LENORE</h5>
						<h6 class="text-center">Burbank, USA</h6>
					</div>
				</div>-->
			</div>
		</div>
	</div>
</section>
<!-- END: TESTIMONIAL SECTION -->    
<!-- START: WHY CHOOSE US SECTION -->
<!--
<section id="why-choose-us">
	<div class="row choose-us-row">
		<div class="container clear-padding">
			<div class="light-section-title text-center">
				<h2>WHY CHOOSE US?</h2>
                <?php $whychooseus=$this->master_model->getRecords('tbl_whychooseus_master'); ?>
				<h4>REASONS TO TRUST US</h4>
				<div class="space"></div>
				<p>
					Lorem Ipsum is simply dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br>
					Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
				</p>
			</div>
			<div class="col-md-4 col-sm-4 wow slideInLeft">
				<div class="choose-us-item text-center">
					<div class="choose-icon"><i class="fa fa-suitcase"></i></div>
					<h4><?php echo $whychooseus[0]['whychoose_title_'.$this->session->userdata('lang')]; ?></h4>
					<p><?php echo $whychooseus[0]['whychoose_short_desc_'.$this->session->userdata('lang')]; ?></p>
					<a href="<?php echo base_url().'home/whychoose/'.$whychooseus[0]['whychoose_id']; ?>">KNOW MORE</a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 wow slideInUp">
				<div class="choose-us-item text-center">
					<div class="choose-icon"><i class="fa fa-phone"></i></div>
					<h4><?php echo $whychooseus[1]['whychoose_title_'.$this->session->userdata('lang')]; ?></h4>
					<p><?php echo $whychooseus[1]['whychoose_short_desc_'.$this->session->userdata('lang')]; ?></p>
					<a href="<?php echo base_url().'home/whychoose/'.$whychooseus[1]['whychoose_id']; ?>">KNOW MORE</a>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 wow slideInRight">
				<div class="choose-us-item text-center">
					<div class="choose-icon"><i class="fa fa-smile-o"></i></div>
					<h4><?php echo $whychooseus[2]['whychoose_title_'.$this->session->userdata('lang')]; ?></h4>
					<p><?php echo $whychooseus[2]['whychoose_short_desc_'.$this->session->userdata('lang')]; ?></p>
					<a href="<?php echo base_url().'home/whychoose/'.$whychooseus[2]['whychoose_id']; ?>">KNOW MORE</a>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!-- END: WHY CHOOSE US SECTION -->
<script type="text/javascript">


jQuery(document).ready(function(e) {
     jQuery('#checkin_deals').datetimepicker({
	  format:'d-m-Y',
	  minDate:'2017-01-26',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		maxDate:jQuery('#checkout_deals').val()?jQuery('#checkout_deals').val():false
	   })
	  },
 });
	jQuery('#checkout_deals').datetimepicker({
	  format:'d-m-Y',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		minDate:jQuery('#checkin_deals').val()?jQuery('#checkin_deals').val():false
	   })
	  },
	});
});

</script>