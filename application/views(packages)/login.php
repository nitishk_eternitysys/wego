<style >
.reqcls{color:red;}
</style>
<script type="application/javascript">
$(document).ready(function(){
 jQuery('#registration_user').validate({});
 jQuery('#loginfrm_user').validate({});
 jQuery('#registration_owner').validate({});
 jQuery('#loginfrm_owner').validate({});
 jQuery("#user_birth_day").datepicker({ dateFormat: "yy-mm-dd",changeYear: true,changeMonth: true,  maxDate: '0' });
});
</script>
<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3>LOGIN/REGISTER</h3>
			<h4 class="thank">Manage Your Account</h4>
		</div>
	</div>
	<!-- END: PAGE TITLE -->
    <!-- START: LOGIN/REGISTER -->
	<div class="container login-row">
    <div class="col-md-12 element-tab">
        <ul class="nav nav-tabs tab-style1">
            <li <?php if($this->router->fetch_class()=='home'){echo 'class="active"'; } ?> ><a data-toggle="tab" href="#tab1">User</a></li>
            <li <?php if($this->router->fetch_class()=='owner'){echo 'class="active"'; } ?> ><a data-toggle="tab" href="#tab2">Hosts Provider</a></li>
        </ul>
        <div class="tab-content">
            <div id="tab1" class="tab-pane fade <?php if($this->router->fetch_class()=='home'){echo ' in active';} ?>">
                <div class="row">
                 <div class="login-min">
                <div class="col-sm-12 login-form">
				<?php 
                if($error!=''){  ?>
                <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php } 
                if($this->session->flashdata('success')!=''){?>	
                <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <h4>Login</h4>
                <form method="post" id="loginfrm_user" action="<?php echo base_url().'home/login/'; ?>">
                   <label>Email <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="email" type="text" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    </div>
                    <label>Password <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="password" type="password" class="form-control" placeholder="Password" data-rule-required="true">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                    </div>					
                    <button type="submit" name="btn_login" id="btn_login" value="1">LOGIN <i class="fa fa-sign-in"></i></button>
                    <div class="account-text"> Do not have an account? <a href="<?php echo base_url().'home/registration/'; ?>"> Register </a></div>
                </form>
  			  </div>
           <?php /*   <div class="col-sm-6 sign-up-form">
                    <?php if($error_reg!=''){  ?>
                    <div class="alert alert-danger"><?php echo $error_reg; ?></div>
                    <?php } 
                    if($this->session->flashdata('success_user_reg')!=''){?>	
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success_user_reg'); ?></div>
                    <?php } ?>
                    <h4>Sign Up</h4>
                    <form method="post" id="registration_user" action="<?php echo base_url().'home/login/'; ?>">
                        <label>Full name <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true" >
                           
                        </div>
                        <label>Email <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                           
                        </div>
                        <label>Password <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_password" id="user_password" type="password" class="form-control" placeholder="Password" data-rule-required="true" data-rule-minlength="6">
                            
                        </div>	
                        <label>Confirm Password <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="confirm-password" id="password_confirm" type="password" class="form-control" placeholder="Retype Password"data-rule-required="true" data-rule-minlength="6" data-rule-equalto="#user_password">
                           
                        </div>	
                        <label>BIRTHDATE<span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_birth_day" id="user_birth_day" type="text" class="form-control" placeholder="Y-m-d" data-rule-required="true" >
                           
                        </div>
                        <label>Gender <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="male" checked="checked" > Male
                            <input name="user_gender" type="radio" placeholder="Email" data-rule-required="true" value="female" >Female
                            
                        </div>
                        <label>Mobile <span class='reqcls'>*</span></label>
                        <div class="input-group">
                           <input name="user_mobile" type="text" class="form-control" placeholder="mobile" data-rule-required="true" onkeypress="return OnlyNumericKeys(event);" >
                            
                        </div>
                        <label>Address <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_address" type="text" class="form-control" placeholder="Address" data-rule-required="true" >
                            
                        </div>
                        <label>Country <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <select class="form-control" data-rule-required="true" name="user_country" id="user_country">
                            <option value="">Select country</option>
                             <?php
							 $this->db->group_by('country');
							 $country=$this->master_model->getRecords('tbl_cities_master');
							 if(count($country))
							 {
								foreach($country  as $coun)
								{
								?>
                                 <option value="<?php echo $coun['country']; ?>"><?php echo $coun['country']; ?></option>
                               <?php
							   }
							 }
							?>
                           </select>
                           <!--<input name="user_country" type="text" class="form-control" placeholder="Country" data-rule-required="true" >-->
                        </div>
                        <label>City <span class='reqcls'>*</span></label>
                        <div class="input-group">
                           <select class="form-control" data-rule-required="true" name="user_city" id="user_city">
                             <option value="">Select City</option>
                           </select>
                           <!--<input name="user_city" type="text" class="form-control" placeholder="City" data-rule-required="true" >-->
                        </div>
                        <label>Id Proof <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_id_info" type="file"  data-rule-required="true" accept="image/*" >
                            <div class="clr"></div>
                            <span>Note : please upload only jpg,jpeg,gif,png</span>
                           
                        </div>
                        <label> payment method <span class='reqcls'>*</span></label>
                        <div class="input-group">
                          <select class="form-control" data-rule-required="true" name="user_payment_method" id="user_payment_method">
                            <option value="">Select Payment</option>
                            <option value="Paypal">Paypal</option>
                            <option value="Bank">Bank</option>
                          </select>
                          <!--<input name="" type="text" class="form-control" placeholder="payment method " data-rule-required="true" >-->
                        </div>
                        <label>Preferred currency <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <select class="form-control" data-rule-required="true" name="user_preferred_currency" id="user_preferred_currency">
                            <option value="">Select currency</option>
                             <?php
							 $lang=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
							 if(count($lang))
							 {
								foreach($lang as $row)
								{
								?>
                                 <option value="<?php echo $row['currency_id']; ?>"><?php echo $row['currency_'.$this->session->userdata('lang')]; ?></option>
                               <?php
							   }
							 }
							?>
                           </select>
                          <!--<input name="user_preferred_currency" type="text" class="form-control" placeholder="Preferred currency" data-rule-required="true" >-->
                            
                        </div>
                        <label>Language <span class='reqcls'>*</span></label>
                        <div class="input-group">
                           <select class="form-control" data-rule-required="true" name="user_preferred_language" id="user_preferred_language">
                            <option value="">Select Language</option>
                             <?php
							 $lang=$this->master_model->getRecords('tbl_language_master',array('language_status'=>'active'));
							 if(count($lang))
							 {
								foreach($lang as $row)
								{
								?>
                                 <option value="<?php echo $row['language_id']; ?>"><?php echo $row['language_'.$this->session->userdata('lang')]; ?></option>
                               <?php
							   }
							 }
							?>
                           </select>
                              
                            <!--<input name="user_preferred_language" type="text" class="form-control" placeholder="Language" data-rule-required="true" >-->
                           
                        </div>
                        <label>Website <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <input name="user_website_here" type="text" class="form-control" placeholder="Website" data-rule-required="true" >
                           
                        </div>
                        <label>Interests <span class='reqcls'>*</span></label>
                        <div class="input-group">
                            <textarea name="user_interests" id="user_interests" class="form-control" data-rule-required="true"></textarea></div>
                        
                        
                        I agree To <a href="<?php echo base_url().'home/page/2';?>" target="new">Terms & Conditions</a> <input name="tc" type="checkbox" required>
                        <button type="submit" name="btn_register" id="btn_register" value="1">REGISTER ME <i class="fa fa-edit"></i></button>
                    </form>
                </div>
                     <div class="clearfix"></div> */ ?>
                      <div class="clearfix"></div> 
                </div>
                </div>
            </div>
            <div id="tab2" class="tab-pane fade <?php if($this->router->fetch_class()=='owner'){echo ' in active'; } ?> ">
               <div class="row">
                  <div class="login-min"> 
                <div class="col-sm-12 login-form">
                    <?php if($this->session->flashdata('error_owner_login')!=''){  ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error_owner_login'); ?></div>
                    <?php } 
                    if($this->session->flashdata('success_owner_reg')!=''){?>	
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success_owner_reg'); ?></div>
                    <?php } ?>
                    <h4>Login</h4>
                    <form method="post" id="loginfrm_owner" action="<?php echo base_url().'owner/login/'; ?>">
                    <label>Email <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    </div>
                    <label>Password <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_password" type="password" class="form-control" placeholder="Password" data-rule-required="true">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                    </div>					
                    <button type="submit" name="btn_login_owner" id="btn_login_owner" value="1">LOGIN <i class="fa fa-sign-in"></i></button>
                       <div class="account-text"> Do not have an account? <a href="<?php echo base_url().'owner/regitration/'; ?>"> Register  </a></div>
                </form>
                </div>
                <?php /*
                <div class="col-sm-6 sign-up-form">
                    <?php if($this->session->flashdata('error_owner_reg')){  ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error_owner_reg'); ?></div>
                    <?php } 
                    if($this->session->flashdata('success_owner_reg')!=''){?>	
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success_owner_reg'); ?></div>
                    <?php } ?>
                    <h4>Sign Up</h4>
                    <form method="post" id="registration_owner" enctype="multipart/form-data" action="<?php echo base_url().'owner/regitration/'; ?>">
                    <label>Name <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_name" id="hosts_name" type="text" class="form-control" placeholder="Full Name" data-rule-required="true" >
                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    </div>
                    <label>company name <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_company" id="hosts_company" type="text" class="form-control" placeholder="Company name" data-rule-required="true">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                    </div>
                    <label>Types  <span class='reqcls'>*</span></label>
                    <div class="input-group">
                 
                        <select multiple="multiple" class="form-control" data-rule-required="true" style="height:auto !important;" name="hosts_mainategory_id[]">
                            <?php 
							
							if(count($categories)>0){ 
									foreach($categories as $cat){?>
                            <option value="<?php echo $cat['category_id'] ?>"><?php echo $cat['category_name_'.$this->session->userdata('lang')] ?></option>
                            <?php } } ?>
                        </select>
                    </div>
                    <label>Description <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <textarea name="hosts_description" id="hosts_description" class="form-control" data-rule-required="true"  style="height:auto !important;"></textarea>
                    </div>
                    <label>Address <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <textarea name="hosts_address" id="hosts_address" class="form-control" data-rule-required="true" style="height:auto !important;"></textarea>
                    </div>
                    <label>Website </label>
                    <div class="input-group">
                        <input name="hosts_website" id="hosts_website" type="text" class="form-control" placeholder="Website" data-rule-url="true">
                        <span class="input-group-addon"><i class="fa fa-globe fa-fw"></i></span>
                    </div>
                    <label>Contact Number <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_contact" id="hosts_contact" type="text" class="form-control" placeholder="Phone number" data-rule-required="true">
                        <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
                    </div>
                    <label>Position</label>
                    <div class="input-group">
                        <input name="hosts_position" id="hosts_position" type="text" class="form-control" placeholder="Position">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                    </div>
                    <label>Mobile Number</label>
                    <div class="input-group">
                        <input name="hosts_mobile" id="hosts_mobile" type="text" class="form-control" placeholder="Mobile Number">
                        <span class="input-group-addon"><i class="fa fa-mobile fa-fw"></i></span>
                    </div>
                    <label>Email <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_email" id="hosts_email" type="text" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                    </div>
                    <label>Password <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="hosts_password" id="hosts_password" type="password" class="form-control" placeholder="Password" data-rule-required="true" data-rule-minlength="6">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                    </div>	
                    <label>Confirm Password <span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <input name="confirm-password" id="password_confirm1" type="password" class="form-control" placeholder="Retype Password"data-rule-required="true" data-rule-minlength="6" data-rule-equalto="#hosts_password">
                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                    </div>
                    <label>Preferred Payment<span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <select class="form-control" data-rule-required="true" name="hosts_preferred_payment">
                            <option value="">Select Payment</option>
                            <option value="Paypal">Paypal</option>
                            <option value="Bank">Bank</option>
                        </select>
                    </div>
                    <label>Preferred Currency<span class='reqcls'>*</span></label>
                    <div class="input-group">
                        <select class="form-control" data-rule-required="true" name="hosts_preferred_currency">
                            <option value="">Select Currency</option>
                              <?php
							 $lang=$this->master_model->getRecords('tbl_currency_master',array('currency_status'=>'active'));
							 if(count($lang))
							 {
								foreach($lang as $row)
								{
								?>
                                 <option value="<?php echo $row['currency_id']; ?>"><?php echo $row['currency_'.$this->session->userdata('lang')]; ?></option>
                               <?php
							   }
							 }
							?>
                        </select>
                    </div>
                    <input name="tc" type="checkbox" required> I agree To <a href="<?php echo base_url().'home/page/2';?>" target="new">Terms & Conditions</a>
                    <button type="submit" name="btn_register_owner" id="btn_register_owner" value="1">REGISTER ME <i class="fa fa-edit"></i></button>
                </form>
                </div>
				*/ ?>
              <div class="clearfix"></div>     
            </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
   </div>
 </div>
