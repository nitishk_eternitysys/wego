<!-- START: FOOTER -->
<section id="footer">
<?php $adminContact=$this->master_model->getRecords('tbl_admin_login',array('id'=>'1')); ?>
	<footer>
		<div class="row sm-footer">
			<div class="container clear-padding">
				<div class="col-md-3 col-sm-6 footer-about-box">
					<?php 
					$getFooter=$this->master_model->getRecords('tbl_front_page',array('front_id'=>'3')); 
					?>
                    <?php if(count($getFooter)){ ?>
                    <h4><?php echo $getFooter[0]['front_page_title_'.$this->session->userdata('lang')];  ?></h4>
					<?php echo word_limiter($getFooter[0]['front_page_description_'.$this->session->userdata('lang')],25);  ?>
					<a href="<?php echo base_url().'home/page/'.$getFooter[0]['front_id']; ?>">READ MORE</a>
                    <?php } ?>
				</div>
				<div class="col-md-3 col-sm-6 contact-box">
					<h4>CONTACT US</h4>
					<p><i class="fa fa-home"></i> <?php echo $adminContact[0]['admin_address']; ?></p>
					<p><i class="fa fa-envelope-o"></i> <?php echo $adminContact[0]['admin_email']; ?></p>
					<p><i class="fa fa-phone"></i><?php echo $adminContact[0]['admin_phone']; ?></p>
					<p class="social-media">
                       <?php $socialLink=$this->master_model->getRecords('tbl_social',array('social_id'=>'1')); ?>
						<a href="<?php echo $socialLink[0]['facebook']; ?>" target="new"><i class="fa fa-facebook"></i></a>
						<a href="<?php echo $socialLink[0]['twitter']; ?>" target="new"><i class="fa fa-twitter"></i></a>
						<a href="<?php echo $socialLink[0]['google_plus']; ?>" target="new"><i class="fa fa-google-plus"></i></a> 
						<a href="<?php echo $socialLink[0]['instagram']; ?>" target="new"><i class="fa fa-instagram"></i></a>
					</p>
				</div>
				<div class="clearfix visible-sm-block"></div>
				<div class="col-md-3 col-sm-6 footer-gallery">
					<h4>GALLERY</h4>
					<img src="front/images/tour1.jpg" alt="cruise">
					<img src="front/images/tour2.jpg" alt="cruise">
					<img src="front/images/tour3.jpg" alt="cruise">
					<img src="front/images/tour4.jpg" alt="cruise">
					<img src="front/images/tour5.jpg" alt="cruise">
					<img src="front/images/tour6.jpg" alt="cruise">
				</div>
				<div class="col-md-3 col-sm-6 footer-subscribe">
					<h4>SUBSCRIBE</h4>
					<p>Don't miss any deal. Subscribe to get offers alerts.</p>
					  <form >
						<div class="col-md-10 col-sm-10 col-xs-9 clear-padding">
							<input type="email" required class="form-control" placeholder="Enter Your Email">
						</div>
						<div class="col-md-2 col-sm-2 col-xs-3 clear-padding">
							<button type="submit"><i class="fa fa-paper-plane"></i></button>
						</div>
					 </form>	
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="row sm-footer-nav text-center">
			<p>
				<a href="#">HOME</a>
				<a href="<?php echo base_url().'owner/regitration/'; ?>">REGITRATION</a>
				<a href="<?php echo base_url().'owner/login/'; ?>">LOGIN</a>
				<a href="#">GALLERY</a>
			</p>
			<p class="copyright">
				&copy;	<?php echo date('Y') ?> REHLA TICKET ALL RIGHTS RESERVED
			</p>
			<div class="go-up">
				<a href="#"><i class="fa fa-arrow-up"></i></a>
			</div>
		</div>
	</footer>
</section>
<!-- END: FOOTER -->
</div>
<!-- END: SITE-WRAPPER -->
<!-- Load Scripts -->
<script src="front/js/respond.js"></script>
<script src="front/plugins/owl.carousel.min.js"></script>
<script src="front/js/bootstrap.min.js"></script>
<script src="front/js/jquery-ui.min.js"></script>
<script src="front/js/bootstrap-select.min.js"></script>
<script src="front/plugins/wow.min.js"></script>
<script type="text/javascript" src="assets/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="js/front-validation.js"></script>
<script src="front/plugins/jquery.flexslider-min.js"></script>
<script src="assets/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="front/plugins/supersized.3.1.3.min.js"></script>
<script src="front/js/js.js"></script>
<script type="text/javascript">
<?php if($this->router->fetch_method()=='accom' || $this->router->fetch_method()=='thanks' || $this->router->fetch_method()=='tours' || $this->router->fetch_class()=='booking'){ ?>
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});
<?php }?>


jQuery(document).ready(function(e) {
     jQuery('#checkin').datetimepicker({
	  format:'d-m-Y',
	  minDate:'<?php echo date('Y-m-d'); ?>',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		maxDate:jQuery('#checkout').val()?jQuery('#checkout').val():false
	   })
	  },
 });
	jQuery('#checkout').datetimepicker({
	  format:'d-m-Y',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		minDate:jQuery('#checkin').val()?jQuery('#checkin').val():false
	   })
	  },
	});
});

</script>
<?php if($this->router->fetch_class().'/'.$this->router->fetch_method()=='home/index'){ ?>
<script type="text/javascript">  
			jQuery(function($){
				"use strict";
				$.supersized({
				   //Functionality
					slideshow               :   1,		//Slideshow on/off
					autoplay				:	1,		//Slideshow starts playing automatically
					start_slide             :   1,		//Start slide (0 is random)
					random					: 	0,		//Randomize slide order (Ignores start slide)
					slide_interval          :   10000,	//Length between transitions
					transition              :   1, 		//0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
					transition_speed		:	500,	//Speed of transition
					new_window				:	1,		//Image links open in new window/tab
					pause_hover             :   0,		//Pause slideshow on hover
					keyboard_nav            :   0,		//Keyboard navigation on/off
					performance				:	1,		//0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
					image_protect			:	1,		//Disables image dragging and right click with Javascript

					//Size & Position
					min_width		        :   0,		//Min width allowed (in pixels)
					min_height		        :   0,		//Min height allowed (in pixels)
					vertical_center         :   1,		//Vertically center background
					horizontal_center       :   1,		//Horizontally center background
					fit_portrait         	:   1,		//Portrait images will not exceed browser height
					fit_landscape			:   0,		//Landscape images will not exceed browser width
					
					//Components
					navigation              :   1,		//Slideshow controls on/off
					thumbnail_navigation    :   1,		//Thumbnail navigation
					slide_counter           :   1,		//Display slide numbers
					slide_captions          :   1,		//Slide caption (Pull from "title" in slides array)
					slides 					:  	[		//Slideshow Images 
													{image : 'front/images/holiday-slide3.jpg', title : 'Slide 2'},
													{image : 'front/images/holiday-slide4.jpg', title : 'Slide 1'}
												]
												
				}); 
		    });
		    
</script>
<?php } ?>
<script>
$(window).load(function() {
	"use strict";
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 150,
    itemMargin: 5,
    asNavFor: '#slider'
  });
 
  $('#slider').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel"
	  });
	});
</script>
</body>
</html>