<script type="text/javascript" src="<?php echo base_url('js/jquery.quick.pagination.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
        $(".paginationsearch").quickPagination({pageSize:"10"});
});
</script>
<div id="searchpagination" class="paginationsearch">
<?php
if($view=='list'){
if($searchData){
		foreach($searchData as $res){?>
	<div  class="hotel-list-view">
		<div class="wrapper">
			<div class="col-md-4 col-sm-6 switch-img clear-padding">
            	<?php if($res['event_image']!=''){ ?>
				<img src="<?php echo $this->master_model->resize($res['event_image'],500,350,'uploads/event/');?>" alt="<?php echo $res['event_title']; ?>">
                <?php } else { ?>
                <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $res['event_title']; ?>">
                <?php } ?>
			</div>
			<div class="col-md-6 col-sm-6 hotel-info">
				<div>
					<div class="hotel-header">
						<h5><?php echo $res['event_title']; ?> <span><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star-o colored"></i></span></h5>
						<p><i class="fa fa-map-marker"></i><?php echo $res['event_address']; ?> <i class="fa fa-phone"></i><?php echo $res['event_mobile']; ?></p>
					</div>
					<!-- <div class="hotel-facility">
						<p><i class="fa fa-plane" title="Flight Included"></i><i class="fa fa-bed" title="Luxury Hotel"></i><i class="fa fa-taxi" title="Transportation"></i><i class="fa fa-beer" title="Bar"></i><i class="fa fa-cutlery" title="Restaurant"></i></p>
					</div> -->
					<div class="hotel-desc">
						<p><?php echo $res['event_description']; ?></p>
					</div>
				</div>
			</div>
			<div class="clearfix visible-sm-block"></div>
			<div class="col-md-2 rating-price-box text-center clear-padding">
				<div class="rating-box">
					<div class="tripadvisor-rating">
						<img src="<?php echo base_url(); ?>assets/images/tripadvisor.png" alt="cruise"><span>4.5/5.0</span>
					</div>
					<div class="user-rating">
						<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
						<span>128 Guest Reviews.</span>
					</div>
				</div>
				<div class="room-book-box">
					<!--<div class="price">
						<h5>$<?php //echo $res['form_price']; ?>/Person</h5>
					</div>-->
					<div class="book">
						<a href="<?php echo base_url().'details/eventDetails/'.base64_encode($res['event_id']);?>">View</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix visible-sm-block"></div>
<?php } } else { ?>
No record(s) found.
<?php } ?>
<?php } else if($view=='grid'){
	if($searchData){
		foreach($searchData as $res){
	 ?>
	<div class="col-md-4 col-sm-6">
				<div class="holiday-grid-view">
					<div class="holiday-header-wrapper">
						<div class="holiday-header">
							<div class="holiday-img">
							<?php if($res['event_image']!=''){ ?>
                            <img src="<?php echo $this->master_model->resize($res['event_image'],500,350,'uploads/event/');?>" alt="<?php echo $res['event_title']; ?>">
                            <?php } else { ?>
                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $res['event_title']; ?>">
                            <?php } ?>
							</div>
							<!--<div class="holiday-price">
								<h4>$<?php //echo $res['form_price']; ?></h4>
								<h5>/Person</h5>
							</div>-->
							<div class="holiday-title">
								<h3><?php echo $res['event_title']; ?></h3>
								<h5><?php echo $res['event_address']; ?></h5>
							</div>
						</div>
					</div>
					<div class="holiday-details">
						<!-- <div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Theme</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p><i class="fa fa-heart" title="Honeymoon Tour"></i><i class="fa fa-users" title="Group Tour"></i></p>
						</div> -->
						<div class="clearfix"></div>
						<!-- <div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Inclusion</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p>
								<i class="fa fa-plane" title="Flight"></i>
								<i class="fa fa-bed" title="Hotel"></i>
								<i class="fa fa-cutlery" title="Meal"></i>
								<i class="fa fa-taxi" title="Transport"></i>
								<i class="fa fa-eye" title="Sightseeing"></i></p>
						</div> -->
						<div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Highlight</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p class="sm-text"><?php echo $res['event_address']; ?></p>
						</div>
					</div>
					<div class="holiday-footer text-center">
						<div class="col-md-8 col-sm-8 col-xs-8 view-detail">
							<a href="<?php echo base_url().'details/eventDetails/'.base64_encode($res['event_id']);?>">VIEW DETAILS</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 social">
							<i class="fa fa-heart-o"></i>
							<i class="fa fa-share-alt"></i>
						</div>
					</div>
				</div>
			</div>
            <?php } } ?>
<?php } ?>
</div>
