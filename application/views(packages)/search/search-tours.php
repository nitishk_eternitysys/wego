<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="location" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['location']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Check in</label>
        <div class="input-group margin-bottom-sm">
            <input type="text"  placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkin" id="checkin_deals" value="<?php echo $_GET['checkin']; ?>">
            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6 col-xs-6">
    <div class="form-gp">
        <label>Check out</label>
        <div class="input-group margin-bottom-sm">
           
            <input type="text" placeholder="DD/MM/YYYY" class="form-control hasDatepicker" name="checkout" id="checkout_deals" value="<?php echo $_GET['checkout']; ?>">
            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
        </div>
    </div>
</div>
<input type="hidden" value="<?php echo $_GET['mainCat']; ?>" name="mainCat" id="mainCat" />
<script type="text/javascript">


jQuery(document).ready(function(e) {
     jQuery('#checkin_deals').datetimepicker({
	  format:'d-m-Y',
	  minDate:'2017-01-26',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		maxDate:jQuery('#checkout_deals').val()?jQuery('#checkout_deals').val():false
	   })
	  },
 });
	jQuery('#checkout_deals').datetimepicker({
	  format:'d-m-Y',
	  timepicker: false,
	  onShow:function( ct ){
	   this.setOptions({
		minDate:jQuery('#checkin_deals').val()?jQuery('#checkin_deals').val():false
	   })
	  },
	});
});

</script>