<script type="text/javascript">
$(document).on('click','.btnAdd',function(){
   if(parseInt($('.appenddiv').length) < <?php echo $fetch_array_details[0]['accom_guest_max']; ?>)
   {	
   $('.appenddiv:last').after('<div class="appenddiv"><div class="col-md-3 ol-sm-3"><label>Name</label><input type="text" name="user_name[]" placeholder="Name" required class="form-control"></div><div class="col-md-3 ol-sm-3"><label>Gender</label><select name="gender[]" id="gender" class="form-control"><option value="">Select Gender</option><option value="male">Male</option><option value="female">Female</option></select></div><div class="col-md-3 ol-sm-3"><label>Age</label><input type="text" name="age[]" id="age" required class="form-control"></div><div class="paypal-pay col-md-3 ol-sm-3" style="margin-top:15px; lastId"><a href="javascript:void(0);" class="btnremove"> Remove</a></div></div>');
   }
   else
   {
	   alert('you not add more than maximum guest!');
   }
});
$(document).on('click','.btnremove',function(){
	$(this).closest('.appenddiv').remove();
 });
</script>
<!-- START: PAGE TITLE -->
<div class="row page-title">
    <div class="container clear-padding text-center flight-title">
        <h3>YOUR SELECTION</h3>
        <h4><?php echo ucfirst($fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']); ?></h4>
        <span><i class="fa fa-calendar"></i> Check In - <?php echo date('d , M',strtotime($fetch_array_details[0]['accom_check_in'])) ?> <i class="fa fa-calendar"></i> Check Out - <?php echo date('d ,M',strtotime($fetch_array_details[0]['accom_check_out'])) ?>  <i class="fa fa-male"></i> Guest(s) - <?php echo  $fetch_array_details[0]['accom_guest_max'];  ?> Adult </span>
    </div>
</div>
<!-- END: PAGE TITLE -->
<!-- START: BOOKING TAB -->
<div class="row booking-tab">
    <div class="container clear-padding">
        <ul class="nav nav-tabs">
            <li class="<?php if($this->uri->segment(4)==''){ ?>active<?php }?> col-md-4 col-sm-4 col-xs-4"><a data-toggle="tab" href="#review-booking" class="text-center"><i class="fa fa-edit"></i> <span>Review Booking</span></a></li>
            <li class="<?php if($this->uri->segment(4)=='passanger'){ ?>active<?php }?> col-md-4 col-sm-4 col-xs-4 logincheck"><a   class="text-center"><i class="fa fa-male"></i> <span>Passenger Info</span></a></li>	
            <li class="<?php if($this->uri->segment(4)=='billing'){ ?>active<?php }?> col-md-4 col-sm-4 col-xs-4"><a class="text-center"><i class="fa fa-check-square"></i> <span>Billing Info</span></a></li>
        </ul> 
    </div>
</div>
<div class="row booking-detail">
		<div class="container clear-padding">
			<div class="tab-content">
               <?php if($this->uri->segment(4)==''){ ?>
				<div id="review-booking" class="tab-pane fade in active">
					<div class="col-md-8 col-sm-8">
                      <div class="login-box clear-margin">
							<h3>Sign In</h3>
							<div class="booking-form">
                                <div class="col-md-12 col-sm-12">
									 <form method="post" name="form_login" id="form_login">
										<label>Email</label>
										<input class="form-control" type="email" name="email" placeholder="Enter Your Email" required>
										<label>Password</label>
										<input class="form-control" type="password" name="password" placeholder="Enter Password" required>
										<a href="#">Forget Password?</a>
										<button type="submit" name="btn_login" id="btn_login" value="1">Login</button>
									</form>
								</div>
                            </div>
						</div>
                     </div>
					<div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item booking-summary">
							<h4><i class="fa fa-bookmark"></i>Your Selection</h4>
							<div class="sidebar-body">
								<div class="booking-summary-title">
									<div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                       <?php if($fetch_array_details[0]['accom_image']!=''){ ?>
                                        <img src="<?php echo $this->master_model->resize($fetch_array_details[0]['accom_image'],200,200,'uploads/accom/');?>" alt="<?php echo $fetch_array_details[0]['accom_title']; ?>">
                                        <?php } else { ?>
                                        <img src="<?php echo $this->master_model->resize('default.gif',200,200,'front/images/');?>" alt="<?php echo $fetch_array_details[0]['accom_title']; ?>">
                                        <?php } ?>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8">
										<h5><?php echo ucfirst($fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']); ?></h5>
										<h5><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h5>
										<h5 class="loc"><i class="fa fa-map-marker"></i><?php echo ucfirst($fetch_array_details[0]['accom_location']); ?></h5>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
									<h5>Check In</h5>
									<div class="date text-center">
										<h2><?php echo date('d',strtotime($fetch_array_details[0]['accom_check_in'])) ?> </h2>
										<h6><?php echo date('M',strtotime($fetch_array_details[0]['accom_check_in'])) ?></h6>
										<h5><?php echo date('D',strtotime($fetch_array_details[0]['accom_check_in'])) ?></h5>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
									<h5>Check Out</h5>
									<div class="date text-center">
										<h2><?php echo date('d',strtotime($fetch_array_details[0]['accom_check_out'])) ?></h2>
										<h6><?php echo date('M',strtotime($fetch_array_details[0]['accom_check_out'])) ?></h6>
										<h5><?php echo date('D',strtotime($fetch_array_details[0]['accom_check_out'])) ?></h5>
									</div>
								</div>
								<div class="clearfix"></div>
								<table class="table">
									<tr>
										<td>Guest</td>
										<td><?php echo $fetch_array_details[0]['accom_guest_max'];  ?></td>
									</tr>
									<!--<tr>
										<td>Nights</td>
										<td><?php ?></td>
									</tr>
									<tr>
										<td>Room Type</td>
										<td>Deluxe Suite</td>
									</tr>-->
									<tr>
										<td>You Pay</td>
										<td class="total">$<?php echo  $fetch_array_details[0]['accom_price'];?></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="sidebar-item contact-box">
							<h4><i class="fa fa-phone"></i>Need Help?</h4>
							<div class="sidebar-body text-center">
								<p>Need Help? Call us or drop a message. Our agents will be in touch shortly.</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
                <?php } if($this->uri->segment(4)=='passanger'){ ?>
				<div id="passenger-info" class="tab-pane fade active in">
					<div class="col-md-8 col-sm-8">
						<div class="passenger-detail">
							<h3>Guest Details</h3>
							<div class="passenger-detail-body">
								<form method="post" name="frm_accom" id="frm_accom">
                                   <?php 
								   $this->db->join('tbl_tmp_booking','tbl_tmp_booking.booking_id=tbl_passenger_list.tmp_booking_id');
								   $checkcount=$this->master_model->getRecords('tbl_passenger_list',array('tbl_tmp_booking.type'=>'accom','tbl_tmp_booking.booking_user_id'=>$this->session->userdata('userID'),'booking_form_id'=>base64_decode($this->uri->segment(3))));
								    if(count($checkcount))
									{ 
									  $i=0;
									  foreach($checkcount as $row)
									  {
									?>
                                       <div class="appenddiv">
                                        <div class="col-md-3 ol-sm-3">
                                            <label>Name</label>
                                            <input type="text" name="user_name[]" placeholder="Name" value="<?php echo $row['passenger_name']; ?>" required class="form-control">
                                        </div>
                                        <div class="col-md-3 ol-sm-3">
                                            <label>Gender</label>
                                            <select name="gender[]" id="gender" class="form-control">
                                               <option value="">Select Gender</option>
                                               <option value="male" <?php if($row['passenger_gender']=='male'){echo 'selected="selected"';} ?>>Male</option>
                                               <option value="female" <?php if($row['passenger_gender']=='female'){echo 'selected="selected"';} ?>>Female</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 ol-sm-3">
                                            <label>Age</label>
                                            <input type="text" name="age[]" value="<?php echo $row['passanger_age']; ?>" id="age" required class="form-control">
                                        </div>
                                        <?php 
										if($i==0){ 
										?>
                                        <div class="paypal-pay col-md-3 ol-sm-3" style="margin-top:15px; lastId">
                                          <a href="javascript:void(0);" class="btnAdd"> Add</a>
                                        </div>
                                        <?php 
										}else{ ?>
                                        <div class="paypal-pay col-md-3 ol-sm-3" style="margin-top:15px; lastId"><a href="javascript:void(0);" class="btnremove"> Remove</a></div>
                                        <?php } ?>
                                       </div>
                                       <div class="clearfix"></div>
                                    <?php
									   $i++;
									  }
									}
									else
									{
								    ?>
                                    <div class="appenddiv">
                                        <div class="col-md-3 ol-sm-3">
                                            <label>Name</label>
                                            <input type="text" name="user_name[]" placeholder="Name" required class="form-control">
                                        </div>
                                        <div class="col-md-3 ol-sm-3">
                                            <label>Gender</label>
                                            <select name="gender[]" id="gender" class="form-control">
                                               <option value="">Select Gender</option>
                                               <option value="male">Male</option>
                                               <option value="female">Female</option>
                                            </select>
                                        </div>
                                        <div class="col-md-3 ol-sm-3">
                                            <label>Age</label>
                                            <input type="text" name="age[]" id="age" required class="form-control">
                                        </div>
                                        <div class="paypal-pay col-md-3 ol-sm-3" style="margin-top:15px; lastId">
                                          <a href="javascript:void(0);" class="btnAdd"> Add</a>
                                        </div>
                                    </div>
								    <?php 
									} 
									?>
                                    <div class="text-center">
										<button type="submit" id="btn_booking" name="btn_booking">CONTINUE <i class="fa fa-chevron-right"></i></button>
									</div>
                                </form>
                                <div style="clear:both;"></div>
							</div>
						</div>
					</div>
                    <div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item booking-summary">
							<h4><i class="fa fa-bookmark"></i>Your Selection</h4>
							<div class="sidebar-body">
								<div class="booking-summary-title">
									<div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                       <?php if($fetch_array_details[0]['accom_image']!=''){ ?>
                                        <img src="<?php echo $this->master_model->resize($fetch_array_details[0]['accom_image'],200,200,'uploads/accom/');?>" alt="<?php echo $fetch_array_details[0]['accom_title']; ?>">
                                        <?php } else { ?>
                                        <img src="<?php echo $this->master_model->resize('default.gif',200,200,'front/images/');?>" alt="<?php echo $fetch_array_details[0]['accom_title']; ?>">
                                        <?php } ?>
									</div>
									<div class="col-md-8 col-sm-8 col-xs-8">
										<h5><?php echo ucfirst($fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']); ?></h5>
										<h5><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></h5>
										<h5 class="loc"><i class="fa fa-map-marker"></i><?php echo ucfirst($fetch_array_details[0]['accom_location']); ?></h5>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
									<h5>Check In</h5>
									<div class="date text-center">
										<h2><?php echo date('d',strtotime($fetch_array_details[0]['accom_check_in'])) ?> </h2>
										<h6><?php echo date('M',strtotime($fetch_array_details[0]['accom_check_in'])) ?></h6>
										<h5><?php echo date('D',strtotime($fetch_array_details[0]['accom_check_in'])) ?></h5>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6 clear-padding">
									<h5>Check Out</h5>
									<div class="date text-center">
										<h2><?php echo date('d',strtotime($fetch_array_details[0]['accom_check_out'])) ?></h2>
										<h6><?php echo date('M',strtotime($fetch_array_details[0]['accom_check_out'])) ?></h6>
										<h5><?php echo date('D',strtotime($fetch_array_details[0]['accom_check_out'])) ?></h5>
									</div>
								</div>
								<div class="clearfix"></div>
								<table class="table">
									<tr>
										<td>Guest</td>
										<td><?php echo $fetch_array_details[0]['accom_guest_max'];  ?></td>
									</tr>
									<!--<tr>
										<td>Nights</td>
										<td><?php ?></td>
									</tr>
									<tr>
										<td>Room Type</td>
										<td>Deluxe Suite</td>
									</tr>-->
									<tr>
										<td>You Pay</td>
										<td class="total">$<?php echo  $fetch_array_details[0]['accom_price'];?></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="sidebar-item contact-box">
							<h4><i class="fa fa-phone"></i>Need Help?</h4>
							<div class="sidebar-body text-center">
								<p>Need Help? Call us or drop a message. Our agents will be in touch shortly.</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
               <?php } if($this->uri->segment(4)=='billing'){ ?> 
                <div id="billing-info" class="tab-pane fade active in">
				   <div class="col-md-8 col-sm-8">
						<div class="passenger-detail">
							<h3>Total Payment to be made $<?php echo  $fetch_array_details[0]['accom_price'];?></h3>
							<div class="passenger-detail-body">
								<div class="saved-card">
									<form method="post" name="frm_accom" id="frm_accom">
                                        <div class="paypal-pay">
									<h4>Pay Using Paypal</h4>
									<div class="col-md-2 col-sm-2 col-xs-4">
										<i class="fa fa-paypal"></i>
									</div>
									<div class="col-md-10 col-sm-10 col-xs-8">
                                         <input type="hidden" name="price" id="price" value="<?php echo $fetch_array_details[0]['accom_price']; ?>" />
                                         <label ><input type="radio" name="passanger_type" checked value="public" id="type_passanger">Public</label>
                                         <label ><input type="radio" name="passanger_type" value="private" id="type_passanger1">Private</label>
                                         <div class="clearfix"></div>
										 <button type="submit" value="1" name="btn_paypal" id="btn_paypal">CONFIRM BOOKING</button>
									</div>
								</div>
                                    </form>
                                    <form method="post" enctype="multipart/form-data">
                                         <input type="hidden" name="price" id="price" value="<?php echo $fetch_array_details[0]['accom_price']; ?>" />
									    <div class="payment-seperator clearfix"></div>
                                        <label data-toggle="collapse" data-target="#saved-card-1"><input type="radio" name="card"> <span>Bank details</span></label>
										<div id="saved-card-1" class="collapse">
											<div class="col-md-4 col-sm-4">
												<label>Screen shot</label>
												<input type="file" name="screen_shot" id="screen_shot" required>
											</div>
										</div>
										<div class="clearfix"></div>
                                         <label ><input type="radio" name="passanger_type" checked value="public" id="type_passanger">Public</label>
                                         <label ><input type="radio" name="passanger_type" value="private" id="type_passanger1">Private</label>
                                         <div class="clearfix"></div>
										<div>
										 <button type="submit" name="btn_bank" id="btn_bank" value="1">CONFIRM BOOKING <i class="fa fa-chevron-right"></i></button>
                                          <a href="<?php echo base_url().'booking/accom/'.$this->uri->segment(3).'/passanger/'; ?>"> Back</a>
                                      </div>
                                     </form>
									
								</div>	
							</div>
						</div>
					</div>
				   <div class="col-md-4 col-sm-4 booking-sidebar">
						<div class="sidebar-item">
							<h4><i class="fa fa-phone"></i>Need Help?</h4>
							<div class="sidebar-body text-center">
								<p>Need Help? Call us or drop a message. Our agents will be in touch shortly.</p>
								<h2>+91 1234567890</h2>
							</div>
						</div>
					</div>
				</div>
                <?php } ?>
                
			</div>
		</div>
	</div>
<!-- END: BOOKING TAB -->