	<!-- START: PAGE TITLE -->
	<div class="row page-title">
		<div class="container clear-padding text-center flight-title">
			<h3>CONTACT US</h3>
			<h4 class="thank">Let's Get Connected</h4>
		</div>
	</div>
	<!-- END: PAGE TITLE -->
	<!-- START: CONTACT-US -->
	<div class="row contact-address">
		<div class="container clear-padding">
          <?php $adminContact=$this->master_model->getRecords('tbl_admin_login',array('id'=>'1')); ?>
			<div class="text-center">
				<h2>GET IN TOUCH</h2>
				<h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</h5>
				<div class="space"></div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-map-marker"></i>
					<p><?php echo $adminContact[0]['admin_address']; ?></p>
				</div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-envelope-o"></i>
					<p><a href="mailto:<?php echo $adminContact[0]['admin_email']; ?>"><?php echo $adminContact[0]['admin_email']; ?></a></p>
				</div>
				<div class="col-md-4 col-sm-4">
					<i class="fa fa-phone"></i>
					<p><?php echo $adminContact[0]['admin_phone']; ?></p>
				</div>
			</div>
		</div>
	</div>
	<!-- END: CONTACT-US -->
<div class="row flex-row">
			<div class="col-md-6 col-sm-6 flex-item clear-padding">
				<iframe src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJG1usnet4BTkRzQqb_Ys-JOg&amp;key=AIzaSyB6hgZM-ruUqTPVUjXGUR-vv7WRqc4MXjY" class="contact-map"></iframe>
			</div>
			<div class="col-md-6 col-sm-6 contact-form flex-item">
				<div class="col-md-12">
					<h2>DROP A MESSAGE</h2>
					<h5>Drop Us a Message</h5>
				</div>
				<form>
					<div class="col-md-6 col-sm-6">
						<input type="text" placeholder="Your Name" class="form-control" required name="name">
					</div>
					<div class="col-md-6 col-sm-6">
						<input type="email" placeholder="Your Email" class="form-control" required name="email">
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
						<input type="text" placeholder="Message Title" class="form-control" required name="message-title">
					</div>
					<div class="clearfix"></div>
					<div class="col-md-12">
						<textarea placeholder="Your Message" id="comment" rows="5" class="form-control"></textarea>
					</div>
					<div class="clearfix"></div>
					<div class="text-center">
						<button class="btn btn-default submit-review" type="submit">SEND YOUR MESSAGE</button>
					</div>
				</form>
			</div>
	</div>