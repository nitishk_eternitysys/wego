<!-- BEGIN Page Title -->
<div class="page-title">
    <div style="clear:both !important;">
        <h1><i class="fa fa-book"></i>Menu</h1>
        <h4>Add Menu</h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li>
        	<a href="<?php echo base_url().'superadmin/menu/manage/'; ?>">Manage Menu</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active">Menu</li>
    </ul>
</div>

<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i>Add Menu</h3>
                <div class="box-tool">
                    <a  class="show-tooltip" href="<?php echo base_url().'superadmin/menu/manage/';?>" title="Back"><i class="fa fa-chevron-up"></i></a>
                   <!-- <a data-action="close" href="#"><i class="fa fa-times"></i></a>-->
                </div>
            </div>
            <div class="box-content">
              <form method="post" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
                  <div class="form-group">
                    <div class="col-sm-12">
					<?php 
                      if($error!=''){  ?>
                        <div class="alert alert-danger"><?php echo $error; ?></div>
                    <?php } 
                      if($this->session->flashdata('success')!=''){?>	
                        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                    <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Title English</label>
                      <div class="col-md-6">
                       <input type="text" name='menu_name_eng' class="form-control" id="menu_name_eng"  data-rule-required="true" placeholder="main menu">
                      </div>
                   </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Title Arabic </label>
                      <div class="col-md-6">
                        <input type="text" name='menu_name_arb' class="form-control" id="menu_name_arb" dir="rtl" data-rule-required="true" placeholder="main menu">
                         <div class="error_msg" id="error_menu_name_arb" style="display:none;"></div>
                      </div>
                   </div>
                   <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Page </label>
                      <div class="col-md-6">
                         <select  name='page_id' class="form-control" id="page_id">
                           <option value="">Select Page</option>
                          <?php if(count($pages)>0){ 
                                foreach($pages as $page) { ?>
                             <option value="<?php echo $page['front_id']; ?>"><?php echo $page['front_page_name_eng']; ?></option>
                             <?php } } ?>
                           </select>
                         <div class="error_msg" id="error_page_id" style="display:none;"></div>
                      </div>
                   </div>
                  <div class="form-group">
                     <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                        <input type="submit" value="Submit" class="btn btn-primary" name="btn_menu" id="btn_menu">
                     </div>
                   </div>
               </form>
            </div>
        </div>
    </div>
</div>