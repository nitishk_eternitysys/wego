<!-- BEGIN Page Title -->
<div class="page-title">
  <div style="clear:both !important;">
    <h1><i class="fa fa-book"></i>Story board</h1>
    <h4>Add Story board</h4>
  </div>
</div>
<!-- END Page Title --> 
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
  <ul class="breadcrumb">
    <li> <i class="fa fa-home"></i> <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li> <a href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manageStoryboard/'; ?>">Manage Story board</a> <span class="divider"><i class="fa fa-angle-right"></i></span> </li>
    <li class="active">Story board</li>
  </ul>
</div>
<!-- END Breadcrumb --> 
<!-- BEGIN Main Content -->
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-title">
        <h3><i class="fa fa-bars"></i>Add Story board</h3>
        <div class="box-tool"> <a  class="show-tooltip" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manageStoryboard/';?>" title="Back"><i class="fa fa-chevron-up"></i></a> 
          
          <!-- <a data-action="close" href="#"><i class="fa fa-times"></i></a>--> 
          	
        </div>
      </div>
      <div class="box-content">
        <form method="post" class="form-horizontal" id="validation-form" enctype="multipart/form-data">
          <div class="form-group">
            <div class="col-sm-12">
              <?php 

                      if($error!=''){  ?>
              <div class="alert alert-danger"><?php echo $error; ?></div>
              <?php } 

                      if($this->session->flashdata('success')!=''){?>
              <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
              <?php } ?>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Category</label>
            <div class="col-md-6">
              <select class="form-control" name="categoryID" data-rule-required="true">
                <option value="">Select Category</option>
                <?php if(count($fetch_category)>0){ 
                        foreach($fetch_category as $cat){
                ?>
                <option value="<?php echo $cat['category_id']; ?>"><?php echo $cat['category_name_eng']; ?></option>
                <?php } } ?>
            </select>
              <?php echo form_error('categoryID'); ?>
              <div class="error_msg" id="error_categoryID" style="display:none;"></div>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Title</label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="story_title" id="story_title" placeholder="Story Title" value="<?php echo set_value('story_title'); ?>" data-rule-required="true" />
              <?php echo form_error('story_title'); ?>
              <div class="error_msg" id="error_story_title" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Description </label>
            <div class="col-md-6">
              <textarea class="form-control" rows="5" id="story_description" name="story_description" data-rule-required="true"></textarea>
              <?php echo form_error('story_description'); ?>
              <div class="error_msg" id="error_story_description" style="display:none;"></div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 col-lg-2 control-label">Images</label>
            <div class="col-sm-9 col-lg-10 controls">
              <input style="padding:0px;" type="file" name="story_images[]" id="story_images" multiple placeholder="Image" data-rule-required="true" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
              <input type="submit" value="Submit" class="btn btn-primary" name="btn_storyboard" id="btn_storyboard">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
