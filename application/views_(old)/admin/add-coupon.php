<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
 var dateToday = new Date(); 
  $(function() {
	   $( "#from_date" ).datepicker({
		dateFormat: 'dd-mm-yy', 
        minDate: dateToday,
		onClose: function (selectedDate) {
            $("#to_date").datepicker("option", "minDate", selectedDate);
          }
    });
    $("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
   });
</script>
<!-- BEGIN Page Title -->
<div class="page-title">
    <div style="clear:both !important;">
        <h1><i class="fa fa-plus"></i><?php echo $pageLable; ?></h1>
        <h4>Add <?php echo $pageLable; ?></h4>
    </div>
</div>
<!-- END Page Title -->
<!-- BEGIN Breadcrumb -->
<div id="breadcrumbs">
    <ul class="breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url().'superadmin/admin/dashboard/'; ?>">Home</a>
            <span class="divider"><i class="fa fa-angle-right"></i></span>
        </li>
        <li class="active"><?php echo $pageLable; ?></li>
    </ul>
</div>
<!-- END Breadcrumb -->
<!-- BEGIN Main Content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-title">
                <h3><i class="fa fa-bars"></i> <?php echo $pageLable; ?></h3>
                <div class="box-tool">
                	<a class="show-tooltip" title="" href="<?php echo base_url().'superadmin/'.$this->router->fetch_class().'/manage/'; ?>">
<i class="fa fa-chevron-up"></i></a>
                   <!-- <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>-->
                </div>
            </div>
            <div class="box-content">
				<?php 
                  if($error!=''){  ?>
                    <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php }  if($this->session->flashdata('error')!=''){?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php
				 }
                  if($this->session->flashdata('success')!=''){?>	
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <form class="form-horizontal" id="validation-form" method="post" enctype="multipart/form-data">
                 <!--<div class="form-group">
                    <label class="col-sm-3 col-lg-2 control-label">Category</label>
                    <div class="col-md-6">
                      <select name="category_id" id="category_id" class="form-control"  data-rule-required="true" >
                          <option value="">select category</option>
                          <?php 
						   $fetch_category=$this->master_model->getRecords('tbl_category_master',array('parent_id'=>'0')); 
                           if(count($fetch_category)){ 
                              foreach($fetch_category as $cat)
                              {
                             ?>
                             <option value="<?php echo $cat['cat_id']; ?>"><?php echo $cat['cat_name']; ?></option>
                           <?php 
                              }
                           } ?>
                      </select>
                    
              <?php echo form_error('cabins_name_eng'); ?>
                <div class="error_msg" id="error_cabins_name_eng" style="display:none;"></div>
               </div>
                  </div> -->
                 
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Coupon From date</label>
                        <div class="col-md-6">
                          <input type="text" name="from_date" id="from_date" value="" class="form-control" data-rule-required="true">
             			  <?php echo form_error('from_date'); ?>
                          <div class="error_msg" id="error_cabins_name_eng" style="display:none;"></div>
                        </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Coupon To date</label>
                      <div class="col-md-6">
                          <input type="text" name="to_date" id="to_date" value="" class="form-control" data-rule-required="true">
                          <?php echo form_error('to_date'); ?>
                          <div class="error_msg" id="error_cabins_name_eng" style="display:none;"></div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Coupon code</label>
                        <div class="col-md-6">
                          <input type="text" name="promo_code" id="promo_code" value="" class="form-control"  data-rule-required="true"  data-rule-maxlength="5" >
             			  <?php echo form_error('cabins_name_eng'); ?>
                          <div class="error_msg" id="promo_code" style="display:none;"></div>
                        </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">No of coupon</label>
                      <div class="col-md-6">
                          <input type="text" name="no_of_promo" id="no_of_promo" value="" class="form-control" data-rule-required="true">
                          <?php echo form_error('no_of_promo'); ?>
                          <div class="error_msg" id="error_no_of_promo" style="display:none;"></div>
                      </div>
                  </div>
                  <input type="radio" name="payment_type" id="payment_type" value="discount" checked style="display:none;" >
                  <div class="form-group">
                      <label class="col-sm-3 col-lg-2 control-label">Discount</label>
                      <div class="col-md-6">
                          <input type="text" name="code_discount" id="code_discount" value="" class="form-control" data-rule-required="true">
                          <?php echo form_error('code_discount'); ?>
                      </div>
                  </div>
                   <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                      <input type="submit" value="Submit" class="btn btn-primary" name="btn_add" id="btn_add">
                    </div>
                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript">
 $(document).ready(function(){
    $('input[name="payment_type"]').click(function(){
	   var paymenttype=$(this).val();	
	   if(paymenttype=='discount')
	   {
		 $('#maxdiv').show();
	    
	   }
	   else
	   { $('#maxdiv').hide();}
	   
	});
	$('#validation-form').submit(function(){
	  $('#error_max_amount').html('');
	  if($('input[name="payment_type"]:checked').val()=='discount')
	  {
		 if($('#max_amount').val()=='')
		 {
			 $('#error_max_amount').html('Please enter amount !');
			 return false;
		 }
	  }
	});
 });
</script>


