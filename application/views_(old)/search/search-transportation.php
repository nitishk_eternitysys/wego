<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Pickup Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="pickup_location" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['pickup_location']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
    </div>
</div>
<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Drop Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="drop_location" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['drop_location']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
    </div>
</div>
