<script type="text/javascript" src="<?php echo base_url('js/jquery.quick.pagination.js') ?>"></script>
<style type="text/css">
.marker-style {
  border: 3px solid #1396e2;
  text-align: center;
  width: 34px;
  height: 34px;
  border-radius: 50%;
  margin-left: -17px !important;
  margin-top: -46px !important;
}
.marker-style img {
  position: absolute !important;
  top: -1px !important;
  bottom: 0px !important;
  right: 0px;
  left: 0px;
  margin: auto !important;
}
.cluster > div::before {
    background-color: #1396e2;
    border-radius: 50%;
    bottom: 0;
    content: "";
    height: 31px;
    left: 0;
    margin: auto;
    position: absolute;
    right: 0;
    top: 0;
    width: 31px;
    z-index: -1;
}
.cluster > div {
    color: #fff !important;
    text-align: center !important;
    z-index: 3;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
        $(".paginationsearch").quickPagination({pageSize:"10"});
		<?php if($view=='map'){  ?>
		initialize();
		<?php } ?>
		
});
var mapStyles = [{featureType:'water',elementType:'all',stylers:[{hue:'#d7ebef'},{saturation:-5},{lightness:54},{visibility:'on'}]},{featureType:'landscape',elementType:'all',stylers:[{hue:'#eceae6'},{saturation:-49},{lightness:22},{visibility:'on'}]},{featureType:'poi.park',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-81},{lightness:34},{visibility:'on'}]},{featureType:'poi.medical',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-80},{lightness:-2},{visibility:'on'}]},{featureType:'poi.school',elementType:'all',stylers:[{hue:'#c8c6c3'},{saturation:-91},{lightness:-7},{visibility:'on'}]},{featureType:'landscape.natural',elementType:'all',stylers:[{hue:'#c8c6c3'},{saturation:-71},{lightness:-18},{visibility:'on'}]},{featureType:'road.highway',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-92},{lightness:60},{visibility:'on'}]},{featureType:'poi',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-81},{lightness:34},{visibility:'on'}]},{featureType:'road.arterial',elementType:'all',stylers:[{hue:'#dddbd7'},{saturation:-92},{lightness:37},{visibility:'on'}]},{featureType:'transit',elementType:'geometry',stylers:[{hue:'#c8c6c3'},{saturation:4},{lightness:10},{visibility:'on'}]}];
function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap',
		styles: mapStyles
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("gmap"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
	<?php $i=1; if($searchData){
		foreach($searchData as $res){?>
	['<?php echo $res['form_title']; ?>', <?php echo $res['form_location']; ?>]<?php if(count($searchData)!=$i) { ?>,<?php }?>
	<?php $i++; } } ?>	
    ];
                        
    // Info Window Content
    var infoWindowContent = [
	<?php $i=1; if($searchData){
		foreach($searchData as $res){?>
        ['<div class="info_content">' +
        '<h3><?php echo $res['form_title']; ?></h3>' +
        '<p><?php echo $res['form_address']; ?></p>' + '</div>']<?php if(count($searchData)!=$i) { ?>,<?php }?>
		<?php $i++; } } ?>
       
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
	var markersArr = [];
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
			icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=' + ([i+1]) + '|FF776B|000000',
			//labelContent: 'terte',
			labelAnchor: new google.maps.Point(50, 0),
			labelClass: "marker-style"
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));
		markersArr.push(marker);
		

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }
	
	var clusterStyles = [
		{
			url: '<?php echo base_url(); ?>/images/cluster.png',
			height: 37,
			width: 37
		}
        ];
        var markerCluster = new MarkerClusterer(map, markersArr, {styles: clusterStyles, maxZoom: 15});

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    /*var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(7);
        google.maps.event.removeListener(boundsListener);
    });*/
    
}
</script>
<div id="searchpagination" class="paginationsearch">
<?php
if($view=='list'){
if($searchData){
		foreach($searchData as $res){
			$host=$this->master_model->getRecords('tbl_hosts_master',array('hosts_id'=>$res['hosts_id']));
			?>
	<div  class="hotel-list-view">
        <?php if($host[0]['hosts_verify']=='yes'){ ?>
        <div class="certified-box"><img src="<?php echo base_url();?>front/images/certified.png" alt=""></div>
        <?php } ?>
		<div class="wrapper">
			<div class="col-md-4 col-sm-6 switch-img clear-padding">
            	<?php if($res['form_image']!=''){ ?>
				<img src="<?php echo $this->master_model->resize($res['form_image'],500,350,'uploads/forms/');?>" alt="<?php echo $res['form_title']; ?>">
                <?php } else { ?>
                <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $res['form_title']; ?>">
                <?php } ?>
			</div>
			<div class="col-md-6 col-sm-6 hotel-info">
				<div>
					<div class="hotel-header">
						<h5><?php echo $res['form_title'.$this->session->userdata('form_lang').'']; ?> <span><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star colored"></i><i class="fa fa-star-o colored"></i></span></h5>
						<p><i class="fa fa-map-marker"></i><?php echo $res['form_address']; ?></p>
					</div>

                    	<ul class="nav nav-tabs">
                         <li class="active"><a data-toggle="tab" href="#date_<?php echo $res['formsID']; ?>"><i class="fa fa-calendar"></i> 2016</a></li>
                         <!--<li><a data-toggle="tab" href="#date2016_<?php //echo $res['formsID']; ?>"><i class="fa fa-calendar"></i> 2016</a></li>-->
						</ul>
						<div class="tab-content">
							<div id="date_<?php echo $res['formsID']; ?>" class="tab-pane fade in active">
								<table class="table">
               <?php
								 $getDays=array();
							     $this->db->group_by('tbl_available_days.form_from_time');
								 $getresponse=$this->master_model->getRecords('tbl_available_days',array('tbl_available_days.formID'=>$res['formsID']));
								 if(count($getresponse)>0)
								 {

								?>
                                  <tr>
                                    <?php
									  $lastMonth=array();
									  foreach($getresponse as $row)
									  {
										  $getDays[]=$row['day'];
									  }
									  $date     = date('Y-m-d',strtotime($res['form_available_from_date']));
									  $end_date = date('Y-m-d',strtotime($res['form_availalbe_to_date']));

									  $j=0;
									  $i=1;
									  while (strtotime($date) <= strtotime($end_date))
									  {

										 $date1 = date ("l",strtotime($date));
										 if(in_array($date1,$getDays))
										 {
											$last=array_unique($lastMonth);
											echo '<td>'.date ("M",strtotime($date)).'-'.date ("d",strtotime($date)).'</td>';
											//echo '<td>'.date ("d",strtotime($date)).'</td>';
										 }
										 $lastMonth[] = date ("M",strtotime($date));
										 $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
									  }


									  //$fetch_array=array_unique($datemonth);
									 // print_r($fetch_array);
									  //print_r($datemonth);
									?>


                                  </tr>
								<?php

								}else{ ?>
                                <tr>
                                  <td colspan="4">No schedule available !</td>
                                </tr>
                                <?php } ?>
                                </table>
							</div>
						</div>
					<!--<div class="hotel-facility">
						<p><i class="fa fa-plane" title="Flight Included"></i><i class="fa fa-bed" title="Luxury Hotel"></i><i class="fa fa-taxi" title="Transportation"></i><i class="fa fa-beer" title="Bar"></i><i class="fa fa-cutlery" title="Restaurant"></i></p>
					</div>
					<div class="hotel-desc">
						<p><?php echo $res['form_desc'.$this->session->userdata('form_lang').'']; ?></p>
					</div> -->
				</div>
			</div>
			<div class="clearfix visible-sm-block"></div>
			<div class="col-md-2 rating-price-box text-center clear-padding">
				<div class="rating-box">
					<div class="tripadvisor-rating">
						<img src="<?php echo base_url(); ?>assets/images/tripadvisor.png" alt="cruise"><span>4.5/5.0</span>
					</div>
					<div class="user-rating">
						<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
						<span>128 Guest Reviews.</span>
					</div>
				</div>
				<div class="room-book-box">
					<div class="price">
						<h5>$<?php echo $res['form_price']; ?>/Person</h5>
					</div>
					<div class="book">
						<a href="<?php echo base_url().'details/toursDetails/'.base64_encode($res['formsID']);?>/<?php echo $res['form_type']; ?>">BOOK</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix visible-sm-block"></div>
<?php } } else { ?>
No record(s) found.
<?php } ?>
<?php } else if($view=='grid'){
	if($searchData){
		foreach($searchData as $res){
	 ?>
	<div class="col-md-4 col-sm-6">
				<div class="holiday-grid-view">
					<div class="holiday-header-wrapper">
						<div class="holiday-header">
							<div class="holiday-img">
							<?php if($res['form_image']!=''){ ?>
                            <img src="<?php echo $this->master_model->resize($res['form_image'],500,350,'uploads/forms/');?>" alt="<?php echo $res['form_title']; ?>">
                            <?php } else { ?>
                            <img src="<?php echo $this->master_model->resize('default.gif',500,350,'front/images/');?>" alt="<?php echo $res['form_title']; ?>">
                            <?php } ?>
							</div>
							<div class="holiday-price">
								<h4>$<?php echo $res['form_price']; ?></h4>
								<h5>/Person</h5>
							</div>
							<div class="holiday-title">
								<h3><?php echo $res['form_title'.$this->session->userdata('form_lang').'']; ?></h3>
								<h5><?php echo $res['form_address']; ?></h5>
							</div>
						</div>
					</div>
					<div class="holiday-details">
						<!-- <div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Theme</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p><i class="fa fa-heart" title="Honeymoon Tour"></i><i class="fa fa-users" title="Group Tour"></i></p>
						</div> -->
						<div class="clearfix"></div>
						<!-- <div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Inclusion</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p>
								<i class="fa fa-plane" title="Flight"></i>
								<i class="fa fa-bed" title="Hotel"></i>
								<i class="fa fa-cutlery" title="Meal"></i>
								<i class="fa fa-taxi" title="Transport"></i>
								<i class="fa fa-eye" title="Sightseeing"></i></p>
						</div> -->
						<div class="col-md-4 col-sm-4 col-xs-4">
							<h5>Highlight</h5>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
							<p class="sm-text"><?php echo $res['form_address']; ?></p>
						</div>
					</div>
					<div class="holiday-footer text-center">
						<div class="col-md-8 col-sm-8 col-xs-8 view-detail">
							<a href="<?php echo base_url().'details/toursDetails/'.base64_encode($res['formsID']);?>/<?php echo $res['form_type']; ?>">VIEW DETAILS</a>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4 social">
							<i class="fa fa-heart-o"></i>
							<i class="fa fa-share-alt"></i>
						</div>
					</div>
				</div>
			</div>
            <?php } } ?>
<?php } else if($view=='map'){ ?>
<div  class="hotel-list-view">
    <div class="wrapper">
        <div id="gmap" style="width:100%;height:500px;"></div>
    </div>
</div>
<?php } ?>
</div>
