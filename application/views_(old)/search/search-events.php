<div class="col-md-3 col-sm-6">
    <div class="form-gp">
        <label>Location</label>
        <div class="input-group margin-bottom-sm">
            <input type="text" name="destination" class="form-control" placeholder="E.g. London" value="<?php echo $_GET['destination']; ?>">
            <span class="input-group-addon"><i class="fa fa-map-marker fa-fw"></i></span>
        </div>
    </div>
</div>