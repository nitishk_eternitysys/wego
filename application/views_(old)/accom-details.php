<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v5-2uaq_wusHDktM9ILcqIrlPtnZgEk&libraries=places&sensor=false"></script>
<style>
.ammenties-2 i {
    background: #e6e6e6 none repeat scroll 0 0;
    font-size: 31px;
    padding: 45px;
}
</style>
<!-- START: PAGE TITLE -->
<div class="row page-title">
    <div class="container clear-padding text-center">
        <h3><?php echo $fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']; ?></h3>
        <h5>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-o"></i>
        </h5>
        <p><i class="fa fa-map-marker"></i><?php echo $fetch_array_details[0]['accom_location']; ?></p>
    </div>
</div>
<!-- END: PAGE TITLE -->
<!-- START: ROOM GALLERY -->
<?php $image_array=$this->master_model->getRecords('tbl_accommodations_images',array('accom_id'=>$fetch_array_details[0]['accom_id'])); ?>
<div class="row hotel-detail">
		<div class="container">
			<div class="product-brief-info">
				<div class="col-md-8 clear-padding">
					<div id="slider" class="flexslider">
						<ul class="slides">
                            <?php
							if(count($image_array)){
							 foreach($image_array as $row)
							 {
							?>
							 <li><img src="<?php echo base_url().'uploads/accom/'.$row['accom_image_name']; ?>" alt="<?php echo $row['accom_image_name']; ?>" /></li>
                           <?php
							 }
						   } ?>
						</ul>
					</div>
					<div id="carousel" class="flexslider">
						<ul class="slides">
						<?php
							if(count($image_array)){
							 foreach($image_array as $row)
							 {
							?>
							 <li><img src="<?php echo base_url().'uploads/accom/'.$row['accom_image_name']; ?>" alt="<?php echo $row['accom_image_name']; ?>" /></li>
                           <?php
							 }
						   } ?>
						</ul>
					</div>
				</div>
				<div class="col-md-4 package-detail-sidebar">
                      <div class="col-md-12 sidebar-wrapper clear-padding">
                                <div class="package-summary sidebar-item">
                                    <h4><i class="fa fa-bookmark"></i> Package Summary</h4>
                                    <div class="package-summary-body">
                                        <h5><i class="fa fa-heart"></i>Title</h5>
                                        <p><?php echo $fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']; ?></p>
                                        <h5><i class="fa fa-map-marker"></i>Departure</h5>
                                        <p><?php echo $fetch_array_details[0]['accom_location']; ?></p>
                                        <h5><i class="fa fa-calendar"></i>Check in - Check out</h5>
                                        <p><?php echo date('d M Y',strtotime($fetch_array_details[0]['accom_check_in'])); ?> - <?php echo date('d M Y',strtotime($fetch_array_details[0]['accom_check_out'])); ?></p>
                                    </div>
                                    <div class="package-summary-footer text-center">
                                        <div class="col-md-6 col-sm-6 col-xs-6 price">
                                            <h5>Starting From</h5>
                                            <h5><strong>$<?php echo $fetch_array_details[0]['accom_price']; ?>/Person</strong></h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 book">
                                            <a href="<?php echo base_url().'booking/accom/'.base64_encode($fetch_array_details[0]['accom_id']);?>">BOOK NOW</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
				</div>
			</div>
		</div>
</div>
<div class="row product-complete-info">
		<div class="container">
			<div class="col-md-8 main-content">
				<div class="room-complete-detail custom-tabs">
					<ul class="nav nav-tabs">
						<li class="col-md-2 col-sm-2 col-xs-2 active text-center"><a data-toggle="tab" href="#overview"><i class="fa fa-bolt"></i> <span>Overview</span></a></li>
						<!--<li class="col-md-2 col-sm-2 col-xs-2  text-center"><a data-toggle="tab" href="#room-info"><i class="fa fa-info-circle"></i> <span>Rooms</span></a></li>-->
						<li class="col-md-3 col-sm-3 col-xs-2 text-center"><a data-toggle="tab" href="#ammenties"><i class="fa fa-bed"></i> <span>Ammenties</span></a></li>
						<li class="col-md-2 col-sm-2 col-xs-2 text-center"><a data-toggle="tab" href="#review"><i class="fa fa-comments"></i> <span>Reviews</span></a></li>
						<li class="col-md-2 col-sm-3 col-xs-2 text-center"><a data-toggle="tab" href="#write-review"><i class="fa fa-edit"></i> <span>Write Review</span></a></li>
                        <li class="col-md-3 col-sm-3 col-xs-2 text-center"><a data-toggle="tab" href="#passanger-list"><i class="fa fa-edit"></i> <span>Passanger List</span></a></li>
					</ul>
					<div class="tab-content">
						<div id="overview" class="tab-pane fadein active in">
							<h4 class="tab-heading">About <?php echo $fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']; ?></h4>
							<p><?php echo $fetch_array_details[0]['accom_description'.$this->session->userdata('form_lang').'']; ?></p>
                            <br/>
                            <?php if($fetch_array_details[0]['accom_cancel_policy']=='yes'){ ?>
                                <h4 class="tab-heading">Cancellation Policy</h4>
                                <p>Cancellation Price</p>
                                <p><?php echo $fetch_array_details[0]['accom_cancel_policy_price'].'%'; ?></p>
                                <br/>
                                <p>Before hours </p>
                                <p><?php echo $fetch_array_details[0]['accom_cancel_hrs']; ?></p>
							<?php } ?>	  
						</div>
						<div id="ammenties" class="tab-pane fade">
							<h4 class="tab-heading">Ammenties of <?php echo $fetch_array_details[0]['accom_title'.$this->session->userdata('form_lang').'']; ?></h4>
							<div class="ammenties-2">
                                	<p>
						    <?php 
							$accom='no';
							if($fetch_array_details[0]['accom_swimming_pool']>0){ ?>
                            <i class="fa fa-life-ring"  style="color:#00adef;" title="Swimming pool"></i>
                            <?php }else{ ?>
							<i class="fa fa-life-ring" title="Swimming pool"></i>
							<?php }?>
                            <?php if($fetch_array_details[0]['accom_tv']=='yes'){ ?>
                            <i class="fa fa-desktop" style="color:#00adef;" title="Television"></i>
                            <?php }else{?>
							 <i class="fa fa-desktop" title="Television"></i>
							<?php	} ?>
                            <?php if($fetch_array_details[0]['accom_playstation']=='yes'){ ?>
                            <i class="fa fa-gamepad" style="color:#00adef;" title="Playstation"></i>
                            <?php }else{ ?>
                            <i class="fa fa-gamepad" title="Playstation"></i>
                            <?php } ?>
                            <?php if($fetch_array_details[0]['accom_wifi']=='yes'){ ?>
                            <i class="fa fa-wifi"  style="color:#00adef;" title="Wifi"></i>
                            <?php }else{ ?>
                             <i class="fa fa-wifi" title="Wifi"></i>
                            <?php } ?>
                            <?php if($fetch_array_details[0]['accom_dvd']=='yes'){ ?>
                            <i class="fa fa-dot-circle-o" style="color:#00adef;" title="DVD"></i>
                            <?php }else{ ?>
                             <i class="fa fa-dot-circle-o" title="DVD"></i>
                            <?php } ?>
                            <?php if($fetch_array_details[0]['accom_gym']=='yes'){ ?>
                            <i class="fa fa-male" title="gym"></i>
                            <?php }else{ ?>
                            <i class="fa fa-male" style="color:#00adef;" title="gym"></i>
                            <?php } ?>
								<!--<div class="col-md-4 col-sm-6">
									<p><i class="fa fa-coffee"></i>Free Coffee</p>
								</div>
								<div class="col-md-4 col-sm-6">
									<p><i class="fa fa-wheelchair"></i>Wheelchair</p>
								</div>
								<div class="col-md-4 col-sm-6">
									<p><i class="fa fa-paw"></i>Pet Room</p>
								</div>-->
							</div>
						</div>
						<div id="review" class="tab-pane fade">
							<div class="review-header">
								<div class="guest-review">
                                  <?php
								  $this->db->join('tbl_user_master','tbl_user_master.user_id=tbl_rating_master.fk_user_id');
								  $ratingdetails=$this->master_model->getRecords('tbl_rating_master',array('tbl_rating_master.fk_form_id'=>base64_decode($this->uri->segment(3)),'tbl_rating_master.form_type'=>'accom'));                  
								  if(count($ratingdetails))
								  {
									  foreach($ratingdetails as $rating)
									  {
										  $dark='';
										  if($rating['rating']==5)
										  {
											  $dark='dark-review';
										  }
										  $approx_avg1=$rating['rating'];
										  switch($approx_avg1)
										  {
												case (round($approx_avg1)==0) :
													$star='<i class="fa fa-star-o" id="1"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=1) :
													$star='<i class="fa fa-star" id="1"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=2) :
													$star= '<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=3) :
													$star='<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=4) :
													$star='<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>';
													break;
												case (round($approx_avg1)<=5) :
													$star= '<i class="fa fa-star" id="1"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
													break;	
											}
								       ?>
									   <div class="individual-review <?php echo $dark; ?>">
										<h4><?php echo $rating['review_title']; ?> <?php echo $star; ?> </h4>
										<p><?php echo $rating['review_desc']; ?></p>
										<div class="col-md-md-1 col-sm-1 col-xs-2">
										   <img src="assets/images/user1.jpg" alt="<?php echo $rating['user_name']; ?>">
                                        </div>
										<div class="col-md-md-3 col-sm-3 col-xs-3">
											<span><?php echo $rating['user_name']; ?>, <?php echo $rating['user_city'];?></span>
										</div>
									</div>
									<div class="clearfix"></div>
								  <?php
									  }
								  }
								  else
								  { ?>
                                   <div class="alert alert-danger">No any review add of this Accommodations !</div> 
                                  <?php	 
								  }
								  ?>
                                </div>
							</div>
						</div>
					    <div id="write-review" class="tab-pane fade">
							<div class="alert alert-danger" id="alerterror" style="display:none;"></div>
				            <div class="alert alert-success" id="alertsuccess" style="display:none;"></div>
					        <h4 class="tab-heading">Write A Review</h4>
                            <?php 
							if($this->session->userdata('userID')!='' && $this->session->userdata('userType')=='user'){ ?>
							<form id="reviewform" name="reviewform">
                                <input type="hidden" name="fk_form_id" id="fk_form_id" value="<?php echo $this->uri->segment(3); ?>">
                                <input type="hidden" name="form_type" id="form_type" value="<?php echo 'accom'; ?>">
                                <div class="rating">
                                 <input id="input-2" value="1" class="rating rating-loading" name="rating" data-min="0" data-max="5" data-step="1">
                                 </div>
								<label>Review Title</label>
								<input type="text" class="form-control" name="review_title" id="review_title"  data-rule-required="true">
								<label for="comment">Comment</label>
								<textarea class="form-control" name="review_desc" rows="5" id="review_desc"  data-rule-required="true"></textarea>
                                <div class="text-center">
								  <button type="submit" id='btn_review' name="btn_review" class="btn btn-default submit-review">Submit</button>
								</div>
							</form>
                            <?php }else{ 
							  if($this->session->userdata('userType')=='')
							  {
								  ?>
                                <form method="post" id="loginfrm_user_rating">
                                   <label>Email</label>
                                    <div class="input-group">
                                        <input name="email" type="text" class="form-control" placeholder="Email" data-rule-required="true" data-rule-email="true">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                    </div>
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input name="password" type="password" class="form-control" placeholder="Password" data-rule-required="true">
                                        <span class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                                    </div>					
                                    <button type="submit" name="btn_login" id="btn_login" value="1" class="btn btn-default submit-review">LOGIN <i class="fa fa-sign-in"></i></button>
                                </form>
							 <?php
							   }
							   else
							   {
								   echo 'Please login with the normal customer and give the rating of this Accommodations. ';
							   }
							 } ?>
						</div>
                        <div id="passanger-list" class="tab-pane fade">
                          <div class="col-md-12 col-sm-12">
                                <div class="most-recent-booking">
                                    <h4>Passangers list</h4>
                                    
                                    <div class="field-entry">
                                        <div class="col-md-1 col-sm-1 col-xs-1 clear-padding">
                                            <p><strong>No.</strong></p>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                            <p><strong>Passenger Name</strong></p>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                            <p><strong>Gender</strong></p>
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-3 clear-padding">
                                            <p><strong>Age</strong></p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                     <?php 
									  $this->db->join('tbl_passenger_list','tbl_passenger_list.tmp_id=tbl_transaction_master.rowId');
									  $passangerList=$this->master_model->getRecords('tbl_transaction_master',array('tbl_transaction_master.booking_form_id'=>$fetch_array_details[0]['accom_id'],'type'=>'accom'));
									  if(count($passangerList))
									  {
										  $i=1;
										  foreach($passangerList as $row)
										  {
									  ?>
                                              <div class="field-entry">
                                                <div class="col-md-1 col-sm-1 col-xs-1 clear-padding">
                                                    <p><strong><?php echo $i; ?>.</strong></p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                                    <p><?php if($row['passanger_type']=='public'){echo ucfirst($row['passenger_name']);}else{echo substr($row['passenger_name'], 0, 1).'------';} ?></p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 col-xs-4 clear-padding">
                                                    <p><?php echo $row['passenger_gender']; ?></p>
                                                </div>
                                                <div class="col-md-3 col-sm-3 col-xs-3 clear-padding">
                                                    <p><?php echo $row['passanger_age']; ?></p>
                                                </div>
                                            </div>
                                    <?php 
									     $i++;
									    }
									  }
									  ?>
                              </div>
                          </div>
                        </div>
					</div>
				</div>
			</div>
			<div class="col-md-4 hotel-detail-sidebar">
				<div class="col-md-12 sidebar-wrapper clear-padding">
					<div class="map sidebar-item">
						<h5><i class="fa fa-map-marker"></i> <?php echo $fetch_array_details[0]['accom_location']; ?></h5>
							<div id="map" style="height:200px; width:100%;"></div>
					</div>
					<?php
					$similarAccom=$this->master_model->getRecords('tbl_accommodations_master',array('accom_subcategory_id'=>$fetch_array_details[0]['accom_subcategory_id'],'accom_status'=>'active','accom_id !='=>$fetch_array_details[0]['accom_id']));
					?>
					<div class="similar-hotel sidebar-item">
						<h4><i class="fa fa-bed"></i> Similar Accommodation</h4>
						<div class="sidebar-item-body">

							<?php if(count($similarAccom)>0){
										foreach ($similarAccom as $same) {
								?>
							<div class="similar-hotel-box">
								<a href="<?php echo base_url().'details/accodetails/'.base64_encode($same['accom_id']);?>">
									<div class="col-md-5 col-sm-5 col-xs-5 clear-padding">
										<?php if($same['accom_image']!=''){ ?>
							<img src="<?php echo $this->master_model->resize($same['accom_image'],150,120,'uploads/accom/');?>" alt="<?php echo $same['accom_title']; ?>">
											<?php } else { ?>
											<img src="<?php echo $this->master_model->resize('default.gif',150,120,'front/images/');?>" alt="<?php echo $same['accom_title']; ?>">
											<?php } ?>
									</div>
									<div class="col-md-7 col-sm-7 col-xs-7">
										<h5><?php echo $same['accom_title'.$this->session->userdata('form_lang').'']; ?></h5>
										<h5><i class="fa fa-map-marker"></i> <?php echo $same['accom_location']; ?></h5>
										<span>$<?php echo $same['accom_price']; ?>/Person</span>
									</div>
								</a>
							</div>
						<?php } } ?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- END: ROOM GALLERY -->
<script type="text/javascript">
var map;
var markersArray = [];
		function initialize() {
				var myLatlng = new google.maps.LatLng(<?php echo $fetch_array_details[0]['accom_latitude']; ?>,<?php echo $fetch_array_details[0]['accom_longitude']; ?>);
				var myOptions = {
						zoom:7,
						center: myLatlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
				}
				map = new google.maps.Map(document.getElementById("map"), myOptions);

				var marker = new google.maps.Marker({
							position: new google.maps.LatLng(<?php echo $fetch_array_details[0]['accom_latitude']; ?>,<?php echo $fetch_array_details[0]['accom_longitude']; ?>),
							map: map
					 });
					 markersArray.push(marker);
}

window.onload = function () { initialize() };
</script>
